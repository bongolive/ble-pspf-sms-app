<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	require_once(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';
	
	if(isset($_SESSION['user'])){
	
		redirect("index.php");
	}

	$action = $_REQUEST['action'];
	$errorFlag = 0;
	$objUser = new user();
	if($action=='fgtpass'){
		if(emailVlidation($_REQUEST['email'])){
			
			if($_REQUEST['userType']=='SUB'){
				$objUser->email = $_REQUEST['email'];
				$arrUser  = $objUser->getSubUserByEmail();
				if(!$arrUser){
			
				$msg = FORGOT_PSWD_MSG_01;
				$errorFlag = 1;
				}else{
					
					$newPass = str_rand(6,'alphanum');
					$objUser->userId = $arrUser['id'];
					$objUser->password = $newPass;
					if($objUser->updateSubPassByEmail()){

						if($project_vars["smtp_auth"]===true){
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
							->setUsername($project_vars["smtp_username"])
							->setPassword($project_vars["smtp_password"]);
						}else{
						
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
						}
						$mailer = Swift_Mailer::newInstance($transport);
						$smarty->assign('field1','Mobile No.');
						$smarty->assign('field1_val',$arrUser['mob_no']);
						$smarty->assign('field2','Password');
						$smarty->assign('field2_val',$newPass);
						$smarty->assign('name',$arrUser['name']);
						$body = $smarty->fetch('mail_template/mail_forgot_pass.tpl');

						$message = Swift_Message::newInstance('New access for Bongolive')
						->setFrom(array($project_vars["smtp_username"] => 'admin'))
						->setTo(array($arrUser['email']=>$arrUser['name']))
						->setBody($body );
						$message->setContentType("text/html");

						//Send the message
						$result = $mailer->send($message);
						
						if($project_vars['send_message_to_mob']){
smsSendWithCurl($arrUser['mob_no'],'Your Bongo Live! credentials - \n Mobile Number '.$arrUser['mob_no'].'
\n Password '.$newPass,$project_vars['sms_api_senderid']);
							$msg = FORGOT_PSWD_MSG_02;
							$action = "sent";
							$errorFlag = 0;
						}else{
						
							$msg = FORGOT_PSWD_MSG_03;
							$action = "sent";
							$errorFlag = 0; 
						}

					}

				}
			}elseif($_REQUEST['userType']=='VEN'){
				$objUser->email = $_REQUEST['email'];
				$arrUser  = $objUser->getVenUserByEmail();
				if(!$arrUser){
			
				$msg = FORGOT_PSWD_MSG_04;
				$errorFlag = 1;
				}else{
					$newPass = str_rand(6,'alphanum');
					$objUser->userId = $arrUser['id'];
					$objUser->password = $newPass;
					if($objUser->updateVenPassByEmail()){
						
						if($project_vars["smtp_auth"]===true){
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
							->setUsername($project_vars["smtp_username"])
							->setPassword($project_vars["smtp_password"]);
						}else{
						
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
						}

						$mailer = Swift_Mailer::newInstance($transport);
						$smarty->assign('field1','Username');
						$smarty->assign('field1_val',$arrUser['username']);
						$smarty->assign('field2','Password');
						$smarty->assign('field2_val',$newPass);
						$smarty->assign('name',$arrUser['name']);
						$body = $smarty->fetch('mail_template/mail_forgot_pass.tpl');

						$message = Swift_Message::newInstance('New access for Bongolive')
						->setFrom(array($project_vars["smtp_username"] => 'admin'))
						->setTo(array($arrUser['email']=>$arrUser['name']))
						->setBody($body );
						$message->setContentType("text/html");

						//Send the message
						$result = $mailer->send($message);
						if($result){
							$action = "sent";
							$msg = FORGOT_PSWD_MSG_05;
							$errorFlag = 0;
						}

						if($project_vars['send_message_to_mob']){

	smsSendWithCurl($arrUser['mob_no'],"Your Bongo Live! credentials - \n Username  ".$arrUser['username']." \n Password  ".$newPass,$project_vars['sms_api_senderid']);
							$msg = FORGOT_PSWD_MSG_06;
								$action = "sent";
								$errorFlag = 0;
						}else{
							
								$msg = FORGOT_PSWD_MSG_07;
								$action = "sent";
								$errorFlag = 0; 
						}

					}
				}

			}
			

			
		}else{
			$msg = FORGOT_PSWD_MSG_08;
			$errorFlag = 1;

		}
		if($errorFlag ==1){
			$smarty->assign("userType",$_REQUEST['userType']);
			$smarty->assign("email",$_REQUEST['email']);
		}
		
		
	}
	
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("msg",$msg);
	$smarty->assign("action",$action);
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('forgot_pass.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/forgot_pass.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/forgot_pass.tpl');
		}
		else
		{
			$smarty->display('forgot_pass.tpl');
		}
	}

	include 'footer.php';
?>