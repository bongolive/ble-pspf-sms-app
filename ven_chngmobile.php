<?php
/****************************************************************************************************************
*	File : sub_chngmobile.php
*	Purpose: mobile number changing for subscribers
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objUser = new user();
	$objUser->userId = $_SESSION['user']['id'];
	$vendorDetail = $objUser->getVendorDetail();

	switch($_REQUEST['action']){
	
		case'chMob':
			if(phoneValidation(trim($_REQUEST['mob_no']))){
				$objUser->mobNo=trim($_REQUEST['mob_no']);
				$objUser->table="mst_vendors";
				$objUser->mob_conf_msg = str_rand($project_vars["random_string_length"],$project_vars["random_string_type"]); 
				if($objUser-> changeMobile()){
				
					$step = 2;
					$msg = VEN_CHANGE_MOBILE_MSG_1;
					smsSendWithCurl($vendorDetail['mob_no'],$objUser->mob_conf_msg);	
					
					/*
					if($project_vars["smtp_auth"]===true){
						$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
						->setUsername($project_vars["smtp_username"])
						->setPassword($project_vars["smtp_password"]);
					}else{
					
						$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
					}

					$mailer = Swift_Mailer::newInstance($transport);

					//Create a message
					$smarty->assign('username',$vendorDetail['name']);
					$smarty->assign('verificationCode',$objUser->mob_conf_msg);
					$body = $smarty->fetch('mail_template/mail_changemobile.tpl');
					//$body = $objUser->mob_conf_msg;
					$message = Swift_Message::newInstance('Mobilie number confirnamtion')
					->setFrom(array($project_vars["smtp_username"] => 'admin'))
					->setTo(array($vendorDetail['email']=>$vendorDetail['name']))
					->setBody($body );
					$message->setContentType("text/html");

					//Send the message
					$result = $mailer->send($message);
					*/

				
				}
			
			}else{
			
				$msg = VEN_CHANGE_MOBILE_MSG_2;
				$smarty->assign('mob_no',trim($_REQUEST['mob_no']));
				$step = 1;
			}
			
		break;
		case'confMob':
			
			$objUser->mob_conf_msg=trim($_REQUEST['mob_conf']);
			$objUser->table="mst_vendors";
			$status = $objUser->moileConfirmation();
			if($status==2 ){
				
				$msg = VEN_CHANGE_MOBILE_MSG_3;
				$step = 2;
				$smarty->assign('mob_conf',trim($_REQUEST['mob_conf']));

			}elseif($status==1){
				$msg = VEN_CHANGE_MOBILE_MSG_4;
			}elseif($status==0){

				$msg = VEN_CHANGE_MOBILE_MSG_5;
				$step = 2;
				$smarty->assign('mob_conf',trim($_REQUEST['mob_conf']));
			}

		break;
		default:
			$step = 1;
		
	}
	
	$smarty->assign('msg',$msg);
	$smarty->assign('step',$step);
	$smarty->assign('mobMaxLen',$project_vars['mob_no_length']);
	$smarty->display("vendor/ven_changemobile.tpl");

	include 'footer.php';

?>