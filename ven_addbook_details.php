<?php
/****************************************************************************************************************
*	File : ven_addbook_details.php
*	Purpose: listing of contacts details of a addressbook
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(CLASSES.'SmartyPaginate.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	$selfUrl = BASE_URL.'ven_addbook_details.php';
	SmartyPaginate::connect();
    SmartyPaginate::setLimit($project_vars['contact_row_per_page']);
	
	
	$pagingUrl = $selfUrl.'?mob_no='.$_REQUEST['mob_no']; 
	$pagingUrl.= '&bid='.$_REQUEST['bid'];
	$pagingUrl.= '&fname='.$_REQUEST['fname'];  
	$pagingUrl.= '&lname='.$_REQUEST['lname']; 
	$pagingUrl.= '&email='.$_REQUEST['email'];
	$pagingUrl.= '&mob_no2='.$_REQUEST['mob_no2'];
	$pagingUrl.= '&optional_one='.$_REQUEST['optional_one'];
	$pagingUrl.= '&optional_two='.$_REQUEST['optional_two'];
	$pagingUrl.= '&area='.$_REQUEST['area'];
	$pagingUrl.= '&city='.$_REQUEST['city'];
	SmartyPaginate::setUrl($pagingUrl);
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	$objAddressbook->addressbookId = $_REQUEST['bid'];
	$objAddressbook->mob_no = $_REQUEST['mob_no'];
	$objAddressbook->fname = $_REQUEST['fname'];
	$objAddressbook->lname = $_REQUEST['lname'];
	$objAddressbook->email = $_REQUEST['email'];
	$objAddressbook->mob_no2 = $_REQUEST['mob_no2'];
	$objAddressbook->optional_one = $_REQUEST['optional_one'];
	$objAddressbook->optional_two = $_REQUEST['optional_two'];
	$objAddressbook->area = $_REQUEST['area'];
	$objAddressbook->city = $_REQUEST['city'];
	
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='delete'){
		
		$objAddressbook->contactId = $_REQUEST['contactId'];
		if($objAddressbook->deletetContact()){
			$msg = VEN_ADD_BOOK_MSG_1;
		}else{
			$msg = VEN_ADD_BOOK_MSG_2;
		}
	
	}
	$bookName = $objAddressbook->getAddressbookName();
	// addressbook listing
	$arrAddressbookDetails = $objAddressbook->getAddressbookDetails();
    $smarty->assign('msg',$msg);
	$smarty->assign('bookName',$bookName);
	$smarty->assign('mob_no',$_REQUEST['mob_no']);
	$smarty->assign('fname',$_REQUEST['fname']);
	$smarty->assign('lname',$_REQUEST['lname']);
	$smarty->assign('email',$_REQUEST['email']);
	$smarty->assign('mob_no2',$_REQUEST['mob_no2']);
	$smarty->assign('optional_one',$_REQUEST['optional_one']);
	$smarty->assign('optional_two',$_REQUEST['optional_two']);
	$smarty->assign('area',$_REQUEST['area']);
	$smarty->assign('city',$_REQUEST['city']);
	$smarty->assign('manageAddbookUrl',BASE_URL.'ven_addbook_details.php');
	$smarty->assign('editContactUrl',BASE_URL.'ven_addcontact.php');
	$smarty->assign('sendSmsUrl',BASE_URL.'ven_smspush.php');
	$smarty->assign('bid',$_REQUEST['bid']);
	$smarty->assign('arrAddressbookDetails',$arrAddressbookDetails);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_addbook_details.tpl");
	SmartyPaginate::disconnect();
	
	include 'footer.php';
?>