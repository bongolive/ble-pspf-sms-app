<?php
	
	/********************************************
	*	File	: ven_manage_senderid.php				*
	*	Purpose	: Reseller senderid management		*
	*	Author	: Leonard Nyirenda						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'vendors/vendor.class.php');
	require_once(MODEL.'senderid/senderid.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	require_once 'swift/lib/swift_required.php';
	require_once(MODEL.'user.class.php');
	
	
	if($_REQUEST['action']!="confirm"){
		$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	}

	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objSenderid = new senderid();
	$objUser = new user();
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
		case 'confirm':
			$objSenderid->id  = urldecode($_REQUEST['id']);
			$objSenderid->status = urldecode($_REQUEST['status']);
			$vendorId=urldecode($_REQUEST['vendorId']);
			$parentVenId=urldecode($_REQUEST['parentVenId']);
			$senderid=urldecode($_REQUEST['senderid']);
			$newStatus=$_REQUEST['status'];
			
	
			$res1 = $objSenderid->updateSenderid();
			if($res1==1){
				$remotesenderid = createRemoteSernderid($senderid);
				$smarty->assign('status', true);
				$smarty->assign('name', $senderid);
				$smarty->assign('newStatus', $newStatus);
				
				$objUser->userId = $vendorId;
				$vendorDetail = $objUser->getVendorDetail();
				$objUser->parentVenId = $vendorDetail['parent_ven_id'];
				$resellerDetail = $objUser->getResellerDetail();
				$smarty->assign('resellerDetails', $resellerDetail);
			
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
						
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);

							$body = $smarty->fetch('mail_template/mail_change_statusname.tpl');

							//var_dump($body);

							$message = Swift_Message::newInstance('Status of  Sender Name')
							->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
							->setTo(array($vendorDetail['email']=>$vendorDetail['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);
	
			}	
		break;
		case'edit':
			
			$objSenderid->id = $_REQUEST['id'];
			$arrSenderidDetails = $objSenderid->getSenderidDetailsname();
			$smarty->assign('arrSenderidDetails',$arrSenderidDetails);
			$action = "update";
			
		break;
		case'update':
			$objSenderid->id  = $_REQUEST['id'];
			$objSenderid->userid  = $_REQUEST['userId'];
			$objSenderid->vendor_id  = $_REQUEST['vendorId'];
			$objSenderid->sender_id  = $_REQUEST['senderId'];
			$objSenderid->status = $_REQUEST['status'];
			$objSenderid->username  = $_REQUEST['name_broad'];

			$objUser->userId = $_REQUEST['vendorId'];
			$userDetail = $objUser-> getUserDetailVen();
			
			$res1 = $objSenderid->updateSenderid();
			$res2 = $objSenderid->updateSenderName();
			$res3 = $objSenderid->updateSenderId_edit();
			
			if($objSenderid->status=='active'){
				$remotesenderid = createRemoteSernderid($objSenderid->sender_id);
			}

			if($res1 && $res2 && $res3){
				$msg = "Senderid updated ";

				if (($_REQUEST['name_broad']!=$_REQUEST['old_name'] || $_REQUEST['status']!=$_REQUEST['old_status'] || $_REQUEST['senderId']!=$_REQUEST['old_sender_id'])  && $project_vars['mail_send']===true){

					//$smarty->assign('name', $_REQUEST['name_broad']);
					//$smarty->assign('id', $_REQUEST['id']);

					if ($_REQUEST['senderId']!=$_REQUEST['old_sender_id']) {
						$smarty->assign('status_id', true);
					}

					if ($_REQUEST['name_broad']!=$_REQUEST['old_name']) {
						$smarty->assign('status_name', true);
					}

					$smarty->assign('newId', $_REQUEST['senderId']);

					$smarty->assign('name', $_REQUEST['name_broad']);

					if ($_REQUEST['status']!=$_REQUEST['old_status']) {
						$smarty->assign('status', true);
						$smarty->assign('newStatus', $_REQUEST['status']);
					}
					
					
					$objUser->parentVenId = $userDetail['parent_ven_id'];
					$resellerDetail = $objUser->getResellerDetail();
					$smarty->assign('resellerDetails', $resellerDetail);
					

					try{
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
						
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);

							$body = $smarty->fetch('mail_template/mail_change_statusname.tpl');

							//var_dump($body);

							$message = Swift_Message::newInstance('Status of  Sender Name')
							->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
							->setTo(array($userDetail['email']=>$userDetail['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);

					} catch (Exception $e){
					
						$msg = SUB_REGISTER_MSG_08.$e->getMessage();
						$errorFlag = 1;
					}
					try{
						if($project_vars['mob_confirmation']==true){		
							$massege = str_rand($project_vars["random_mob_conf_length"],$project_vars["random_string_type"]);
							smsSendWithCurl($userDetail['mob_no'],str_replace('MESSAGE',$massege,$project_vars['register_message']),$project_vars['sms_api_senderid']);
							$objUser->mob_conf_msg = $massege;
							$objUser->insertSubMobConfMsg();
							}
					}catch(Exception $objE){
					
							$msg = SUB_REGISTER_MSG_09.$objE->getMessage();;
							$errorFlag = 1;
					
					}
				}


			}else{
				$msg = "Senderid not updated !!!";
			}

			$action = "search";			
		break;
		
		default:
			$action = "search";
	
	
	}
	
	$selfUrl = BASE_URL.'ven_manage_senderid.php';
	$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
	$pagingUrl.= '&username='.$_REQUEST['username'];
	$pagingUrl.= '&vendor_type='.$_REQUEST['vendor_type'];
	$pagingUrl.= '&senderid='.$_REQUEST['senderid']; 
	$pagingUrl.= '&status='.$_GET['status']; 
	$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
	$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
	
	SmartyPaginate::setUrl($pagingUrl);
	
	$objSenderid->vendorName	= $_REQUEST['vendorName'];
	$objSenderid->username		= $_REQUEST['username'];
	$objSenderid->vendor_type	= $_REQUEST['vendor_type'];
	$objSenderid->senderid		= $_REQUEST['senderid'];
	$objSenderid->status		= $_GET['status'];
	//$objSenderid->status		= $_GET['status'];
	
	$objSenderid->startDate		= getDateFormat($_REQUEST['startDate'],'Y-m-d');
	$objSenderid->endDate		= getDateFormat($_REQUEST['endDate'],'Y-m-d');
	
	$objSenderid->parentVenId=$_SESSION['user']['id'];
	$arrSenderid = $objSenderid->getSubVenSenderidList();

	$smarty->assign("msg",$msg);
	$smarty->assign("vendorName", $_REQUEST['vendorName']);
	$smarty->assign("username",$_REQUEST['username']);
	$smarty->assign("vendor_type", $_REQUEST['vendor_type']);
	$smarty->assign("senderid",$_REQUEST['senderid']);
	$smarty->assign("status", $_GET['status']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	
	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("selfUrl",BASE_URL.'ven_manage_senderid.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrSenderid",$arrSenderid);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_senderid.tpl");
	SmartyPaginate::disconnect();
	
		
	include 'footer.php';

?>
