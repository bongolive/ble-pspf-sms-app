<?php
	class user{

		public $userId;
		public $mobNo;
		public $password;
		public $oldPassword;
		public $email;
		public $url;
		public $username;
		public $name;
		public $step;
		public $gender;
		public $educationId='';
		public $employmentId='';
		public $phone='';
		public $ageRangeId='';
		public $cityId='';
		public $locationId='';
		public $neighborhoodId='';
		public $occupationId='';
		public $industryId='';
		public $postalAddress='';
		public $physicalAddress='';
		public $organisation='';
		public $organisationCatId='';
		public $categoryId='';
		public $peopleRangeId='';
		public $vendor_id='';
		public $vendorType='';
		public $termCondition='';
		public $mob_conf_msg='';
		public $mob_confirmed='';
		public $table='';
		public $active='';
		public $delete='';
		public $confirmed='';
		public $created='';
		public $modified='';
		public $source='BongoLive!';
		public $unsubscribe;
		public $reminder;
		public $sms_reminder;
		public $email_reminder;
		public $credit_level;
		public $broadcast_reminder;

		function registerSubStep1Insert(){

			global $myDB;

			if($this->getSubUserByMob()){
				return 3;
			}elseif($this->getSubUserByEmail()){

				return 2;
			}else{
				$id = getNewUUID();

				$sql = "INSERT INTO mst_subscribers SET
				id= '$id',
				mob_no='".$this->mobNo."',
				email='".$this->email."',
				password='".$this->password."',
				reg_step = '1',
				created=NOW()
				";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)){
					$this->userId = $id;
					return $id;
				}else{
					return false;
				}
			}

		}

		function registerSubStep2(){
		 	$sql = "UPDATE mst_subscribers SET
			name= '".$this->name."',
			gender='".$this->genderId."',
			age_range_id='".$this->ageRangeId."',
			occupation_id='".$this->occupationId."',
			education_id='".$this->educationId."',
			employment_id='".$this->employmentId."',
			reg_step = '2'
			WHERE id= '".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return $this->userId;
			}else{
				return false;
			}

		}


		function registerSubStep3(){

			global $project_vars;

			if(is_array($this->categoryId)){

				$categoryId = " category_id = '".implode(',',$this->categoryId)."', ";

			}else{

				$categoryId = " ";
			}

			$sql = "UPDATE mst_subscribers SET
			location_id= '".$this->locationId."',
			industry_id='".$this->industryId."',
			$categoryId
			term_conditions='".$this->termCondition."', ";
			if($project_vars['sub_account_activation_email']!==true){
				$sql .= " active = '1',
						  confirmed = '1', ";
			}
			$sql .= " reg_step = '3'
			WHERE id= '".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return $this->userId;
			}else{
				return false;
			}

		}

		function registerSubStep1Upadate(){
			if($this->getSubUserByMob()){
				return 3;
			}elseif($this->getSubUserByEmail()){

				return 2;
			}else{
				$sql = "UPDATE mst_subscribers SET
				mob_no='".$this->mobNo."',
				email='".$this->email."',
				password='".$this->password."' where id = '".$this->userId."'";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result){
					return $this->userId ;
				}else{
					return false;
				}
			}

		}

		function insertSubMobConfMsg(){

			$sql = "UPDATE mst_subscribers SET
			mob_conf_msg='".mysql_real_escape_string($this->mob_conf_msg)."',
			mob_confirmed='0'
			WHERE id='".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return $this->userId;
			}else{
				return false;
			}

		}

		function registerSubStep4Update(){

			$sql = "SELECT mob_conf_msg FROM mst_subscribers WHERE id='".$this->userId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			if($row['mob_conf_msg']==$this->mob_conf_msg){

				$sql = "UPDATE mst_subscribers SET
				mob_confirmed='".$this->mob_confirmed."',
				active = '1',
				confirmed = '1'
				WHERE id='".$this->userId."' ";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result){

					return 1;
				}else{
					return 0;
				}
			}else{
				return 2;
			}

		}

		function Add_sub(){

			global $myDB;

			$id = getNewUUID();

			$errors = array('create_date'=>false,'userId'=>false, 'password'=>false, 'mob_no'=>false, 'email'=>false);

			/*if (trim($this->created)=='' || $this->created==null || $this->created==0) {
				$errors['create_date'] = true;
			} else {
				$created = excel_to_timestamp($this->created);
			}*/

			if (strlen($this->password)<6){
				$errors['password'] = true;
			}

			/*if (strlen($this->mobNo)<9 || strlen($this->mobNo)>10){
				$errors['mob_no'] = true;
			}*/

			$pattern ="^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$";

			if ($this->email=='' || empty($this->email) || ereg($pattern,$this->email)==false){
				$errors['email'] = true;
			}

			if ($this->modified=='' || $this->modified=='null') {
				$modified = 0;
			} else {
				$modified = excel_to_timestamp($this->modified);
			}

			$sql_dubl = "SELECT * FROM mst_subscribers";
				$result_dubl = mysql_query($sql_dubl);
				$kol = mysql_num_rows($result_dubl);
				for ($i=0;$i<$kol;$i++){
					$arr[] = mysql_fetch_assoc($result_dubl);
				}
				$dubl = 0;
				foreach ($arr as $val) {
					if ($val['password'] == md5(trim($this->password)) && $val['mob_no']==$this->mobNo && $val['active']==1 && $val['confirmed']==1 &&($val['email_confirmed']==1 || $val['mob_confirmed']==1)) {
						$dubl = 1;
						break;
					}
				}

			if(empty($this->genderId)){$this->genderId = 'M';}
			if(empty($this->mob_confirmed)){$this->mob_confirmed = '1';}
			if(empty($this->term_conditions)){$this->term_conditions = '1';}
			if(empty($this->email_confirmed)){$this->email_confirmed = '1';}
			if(empty($this->active)){$this->active = '1';}
			if(empty($this->confirmed)){$this->confirmed = '1';}
			if(empty($this->deleted)){$this->deleted = '0';}

			if ($dubl=='0' && $errors['create_date']==false && $errors['password']==false && $errors['mob_no']==false && $errors['email']==false) {

				$sql = "INSERT INTO mst_subscribers SET
				id= '$id',
				mob_no='".$this->mobNo."',
				email='".$this->email."',
				password='".md5(trim($this->password))."',
				reg_step = '".$this->step."',
				name = '".$this->name."',
				gender = '".$this->genderId."',
				education_id = '".$this->educationId."',
				employment_id = '".$this->employmentId."',
				location_id = '".$this->locationId."',
				age_range_id = '".$this->ageRangeId."',
				city_id = '".$this->cityId."',
				neighborhood_id = '".$this->neighborhoodId."',
				occupation_id = '".$this->occupationId."',
				industry_id = '".$this->industryId."',
				category_id = '".$this->categoryId."',
				mob_conf_msg = '".$this->mob_conf_msg."',
				mob_confirmed = '".$this->mob_confirmed."',
				term_conditions = '".$this->termCondition."',
				active = '".$this->active."',
				deleted = '".$this->deleted."',
				confirmed = '".$this->confirmed."',
				created = '".date("Y-m-d H:i")."',
				modified = '".date("Y-m-d H:i")."',
				source = '".$this->source."'
				";
				/*created = '".date("Y-m-d H:i",$created)."',
				modified = '".date("Y-m-d H:i",$modified)."'*/
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)){
					$this->userId = $id;
				}else{
					$errors['userId']=true;
				}


			}

		return array('errors'=>$errors,'dubl'=>$dubl);


		}

		function getUserDetail(){

			global $myDB;
			$sql = "SELECT * FROM  mst_subscribers WHERE id= '".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}

		}

		function getUserDetailVen(){

			global $myDB;
			$sql = "SELECT * FROM  mst_vendors WHERE id= '".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}

		}

		function getSubProfile(){

			global $myDB;
			$sql = "
					SELECT ms . * , l.location, ag.age_range, ed.education_level,em.employment, o.occupation, i.industry
					FROM mst_subscribers AS ms
					LEFT JOIN locations AS l ON l.id = ms.location_id
					LEFT JOIN age_ranges AS ag ON ag.id = ms.age_range_id
					LEFT JOIN educations AS ed ON ed.id = ms.education_id
					LEFT JOIN employments AS em ON em.id = ms.employment_id
					LEFT JOIN occupations AS o ON o.id = ms.occupation_id
					LEFT JOIN industries AS i ON i.id = ms.industry_id
					WHERE ms.id= '".$this->userId."'";
			echo '<pre>';

			echo '</pre>';
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}

		}

		function updateSubProfile(){

			if(is_array($this->categoryId)){

				$categoryId = " category_id = '".implode(',',$this->categoryId)."' ";

			}else{

				$categoryId = " ";
			}

			$sql = "UPDATE mst_subscribers SET
			name= '".$this->name."',
			email='".$this->email."',
			gender='".$this->genderId."',
			age_range_id='".$this->ageRangeId."',
			location_id='".$this->locationId."',
			education_id = '".$this->educationId."',
			employment_id = '".$this->employmentId."',
			occupation_id= '".$this->occupationId."',
			industry_id='".$this->industryId."',
			$categoryId
			WHERE id= '".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){
				//updating Subscription
				if($this->unsubscribe != ''){
					$this->unsubscribe();
				}
				return $this->userId;
			}else{
				return false;
			}

		}

		function registerVenStep1Insert(){

			global $myDB;

			if(!empty($this->userId)){

				$sqlPart = " AND id != '".$this->userId."' ";
			}

			$sql = "SELECT * FROM mst_vendors where active=1 and deleted=0 and username='".$this->username."' ".$sqlPart;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==0){

				$id = getNewUUID();

				$sql = "INSERT INTO mst_vendors SET
				id= '$id',
				vendor_id='".$this->vendor_id."',
				username='".$this->username."',
				password='".md5($this->password)."',
				vendor_type='".$this->vendorType."',
				reg_step = '1',
				created=NOW()
				";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)){
					$this->userId = $id;
					return $id;
				}else{
					return false;
				}
			}else
				return 2;
		}

		function registerVenStep1Upadate(){

			global $myDB;

			if(!empty($this->userId)){

				$sqlPart = " AND id != '".$this->userId."' ";
			}

			$sql = "SELECT * FROM mst_vendors where active=1 and deleted=0 and username='".$this->username."' ".$sqlPart;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==0){

				$sql = "UPDATE mst_vendors SET

				username='".$this->username."',
				password='".md5($this->password)."',
				vendor_type='".$this->vendorType."'
				WHERE id = '".$this->userId."'";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)){

					return $this->userId;
				}else{
					return false;
				}
			}else
				return 2;

		}

		function registerVenStep2Update(){

			global $project_vars;

			$email = $this->getVenUserByEmail();

			if($email===false){
				$sql = "UPDATE mst_vendors SET
				name='".mysql_real_escape_string($this->name)."',
				mob_no='".$this->mobNo."',
				phone='".$this->phone."',
				email='".$this->email."',
				people_range_id='".$this->peopleRangeId."',
				postal_address='".mysql_real_escape_string($this->postalAddress)."',
				physical_address='".mysql_real_escape_string($this->physicalAddress)."',
				organisation='".mysql_real_escape_string($this->organisation)."',
				organisation_id='".$this->organisationCatId."',
				location_id = '".$this->locationId."',
				term_conditions= '".$this->termCondition."', ";
				$this->comfermEmail();
				if($project_vars['ven_account_activation_email']!==true){
					$sql .=" active = '1',
					confirmed = '1', ";
				}
				$sql .=" reg_step='2'
				WHERE id='".$this->userId."' ";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result){

					return $this->userId;
				}else{
					return false;
				}
			}else{
				return 2;
			}

		}


		function insertVenMobConfMsg(){

			$sql = "UPDATE mst_vendors SET
			mob_conf_msg='".mysql_real_escape_string($this->mob_conf_msg)."',
			mob_confirmed='0'
			WHERE id='".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return $this->userId;
			}else{
				return false;
			}

		}

		function comfermEmail(){
			$sql = "UPDATE mst_vendors SET
				mob_confirmed='1',
				active = '1',
				confirmed = '1'
				WHERE id='".$this->userId."' ";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		}
		
		
		function registerVenStep3Update(){

			$sql = "SELECT mob_conf_msg FROM mst_vendors WHERE id='".$this->userId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			if($row['mob_conf_msg']==$this->mob_conf_msg){

				$sql = "UPDATE mst_vendors SET
				mob_confirmed='".$this->mob_confirmed."',
				active = '1',
				confirmed = '1'
				WHERE id='".$this->userId."' ";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result){

					return 1;
				}else{
					return 0;
				}
			}else{
				return 2;
			}

		}


		function getVendorDetail(){

			global $myDB;
			$sql = "SELECT * FROM  mst_vendors WHERE id= '".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}

		}

		function getVendorDetailIDfromUrl(){
			//global $myDB;
			$sql = "SELECT id FROM  mst_vendors WHERE url= '". $this->url ."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){
				return $row = mysql_fetch_array($result);
			}else{
				return false;
			}
		}
		
		function getVendorDetailIDfromVendorMobile(){
			//global $myDB;
			$sql = "SELECT id FROM  mst_vendors WHERE mob_no= '". $this->phone ."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){
				return $row = mysql_fetch_array($result);
			}else{
				return false;
			}
		}
		
		function updateVenProfile(){

			$sql = "UPDATE mst_vendors SET
			name='".mysql_real_escape_string($this->name)."',
			phone='".$this->phone."',
			mob_no='".$this->mobNo."',
			email='".$this->email."',
			url='".$this->url."',
			postal_address='".mysql_real_escape_string($this->postalAddress)."',
			physical_address='".mysql_real_escape_string($this->physicalAddress)."',
			organisation='".mysql_real_escape_string($this->organisation)."',
			organisation_id='".$this->organisationCatId."',
			location_id = '".$this->locationId."',
			broadcast_reminder='".$this->broadcast_reminder."',
			people_range_id = '".$this->peopleRangeId."',
			modified = NOW()
			WHERE id='".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return true;
			}else{
				return false;
			}
		}
		
		
		function admUpdateVenProfile(){

			$sql = "UPDATE mst_vendors SET
			name='".mysql_real_escape_string($this->name)."',
			username='".$this->username."',
			password='".$this->password."',
			phone='".$this->phone."',
			mob_no='".$this->mobNo."',
			email='".$this->email."',
			url='".$this->url."',
			postal_address='".mysql_real_escape_string($this->postalAddress)."',
			physical_address='".mysql_real_escape_string($this->physicalAddress)."',
			organisation='".mysql_real_escape_string($this->organisation)."',
			organisation_id='".$this->organisationCatId."',
			location_id = '".$this->locationId."',
			people_range_id = '".$this->peopleRangeId."',
			active	= '".$this->active."',
			deleted	=	'".$this->delete."',
			confirmed	=	'".$this->confirmed."',
			modified = NOW()
			WHERE id='".$this->userId."' ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){

				return true;
			}else{
				return false;
			}
		}
		
		
		function changePassword(){

			$sql = "SELECT * FROM ".$this->table." WHERE ".$this->table.".password='".md5($this->oldPassword)."' and ".$this->table.".id='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				$sql = "UPDATE ".$this->table." SET
				password= '".md5($this->password)."'
				WHERE ".$this->table.".id= '".$this->userId."' ";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result){

					return 1;
				}else{
					return 0;
				}
			}else{

				return 2;
			}

		}

		function changeMobile(){

				if($this->getSubUserByMob()==false){

					$sql = "UPDATE ".$this->table." SET
					mob_no= '".$this->mobNo."',
					mob_conf_msg= '".$this->mob_conf_msg."',
					mob_confirmed= '0'
					WHERE ".$this->table.".id= '".$this->userId."' ";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result){

						return 1;
					}else{
						return 2;
					}
				}else{
					return 3;
				}

		}


		function moileConfirmation(){

				$sql = "SELECT mob_conf_msg from ".$this->table." WHERE ".$this->table.".id= '".$this->userId."' ";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			 	$row = mysql_fetch_assoc($result);

				if($row['mob_conf_msg']==$this->mob_conf_msg){

					$sql = "UPDATE ".$this->table." SET
					mob_confirmed= '1'
					WHERE ".$this->table.".id= '".$this->userId."' ";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result){

						return 1;
					}else{
						return 0;
					}
				}else{
					return 2;
				}

		}

		function unsubscribe(){		
			$sql = "UPDATE  mst_subscribers SET confirmed='".$this->unsubscribe."' WHERE id='".$this->userId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				return 1;
			}else{
				return 2;				
			}
		}
		
		
		
		function subscribersStatus(){

			$sql = "SELECT SUM(if(active=1,1,0)) AS active,SUM(if(reg_step < 3,1,0)) AS incomplete,SUM(if(confirmed=0 and reg_step >=3,1,0)) AS pending_conf FROM  mst_subscribers ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);

		}
		

		
		
		
		
		function VenderSMSEmailReminder(){
			$sql = "UPDATE  credit_balances SET sms_reminder='".$this->sms_reminder."',email_reminder='".$this->email_reminder."',credit_level='".$this->credit_level."' WHERE user_id='".$this->userId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				return true;
			}else{
				return false;				
			}

		}


		function vendorStatus(){

			$sql = "SELECT SUM(if(active=1 AND vendor_type=1,1,0)) AS active_indivdual,SUM(if(active=1 AND vendor_type=2,1,0)) AS active_organisation,SUM(if(reg_step<2,1,0)) AS incomplete,SUM(if(confirmed=0 and reg_step >= 2  ,1,0)) AS pending_conf FROM  mst_vendors";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);

		}

		function getSubUserId(){

			$sqlCond = " ";

			if(is_array($this->ageRangeId)){

				$sqlCond .=" AND  ms.age_range_id IN ('".implode("','",$this->ageRangeId)."') ";
			}

			if(is_array($this->gender)){

				$sqlCond .=" AND  ms.gender IN ('".implode("','",$this->gender)."') ";
			}

			if(is_array($this->educationId)){

				$sqlCond .=" AND  ms.education_id IN ('".implode("','",$this->educationId)."') ";
			}

			if(is_array($this->employmentId)){

				$sqlCond .=" AND  ms.employment_id IN ('".implode("','",$this->employmentId)."') ";
			}

			if(is_array($this->locationId)){

				$sqlCond .=" AND  ms.location_id IN ('".implode("','",$this->locationId)."') ";
			}
			if(is_array($this->occupationId)){

				$sqlCond .=" AND  ms.occupation_id IN ('".implode("','",$this->occupationId)."') ";
			}


			if(is_array($this->categoryId)){
				$sqlCat='';
				$sqlCond .=" AND  (";
				foreach($this->categoryId as $catId){

					$sqlCat .=" ms.category_id like '%".$catId."%' or";
				}
				$sqlCat = substr($sqlCat,0,-2);
				$sqlCond.= $sqlCat.' ) ';
			}


			$arrSubUser= array();

			$sql = "SELECT id,mob_no,name,email FROM  mst_subscribers  AS ms WHERE ms.deleted=0 AND ms.active=1 AND ms.confirmed=1 $sqlCond ";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){

				while($row = mysql_fetch_assoc($result)){

					array_push($arrSubUser,$row);
				}
				return $arrSubUser;

			}else
				return false;


		}

		function getVenUserId(){

			$sqlCond = " ";


			if(is_array($this->vendorType)){

				$sqlCond .=" AND  mv.vendor_type IN (".implode(",",$this->vendorType).") ";
			}
			if(is_array($this->organisationCatId)){

				$sqlCond .=" AND  mv.organisation_id IN ('".implode("','",$this->organisationCatId)."') ";
			}
			if(is_array($this->peopleRangeId)){

				$sqlCond .=" AND  mv.people_range_id IN ('".implode("','",$this->peopleRangeId)."') ";
			}
			/*
			if(is_array($this->cityId)){

				$sqlCond .=" AND  mv.city_id IN ('".implode("','",$this->cityId)."') ";
			}
			*/
			if(is_array($this->locationId)){

				$sqlCond .=" AND  mv.location_id IN ('".implode("','",$this->locationId)."') ";
			}
			$arrSubUser= array();

			$sql = "SELECT id,mob_no,name,email FROM  mst_vendors  AS mv WHERE mv.deleted=0 AND mv.active=1 AND mv.confirmed=1 $sqlCond ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){

				while($row = mysql_fetch_assoc($result)){

					array_push($arrSubUser,$row);
				}
				return $arrSubUser;

			}else
				return false;

		}
		function activateAccount(){

			$sql = "SELECT confirmed FROM ".$this->table." WHERE id='".$this->userId."' ";
			$result = mysql_query($sql)  or mysql_error_show($sql,__FILE__,__LINE__);

			if(mysql_num_rows($result)>0){
				$row = mysql_fetch_assoc($result);

				if($row['confirmed']=='0'){

					$sql = "UPDATE  ".$this->table." SET confirmed =1, active=1, email_confirmed = 1 WHERE id='".$this->userId."' ";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if(mysql_affected_rows()>0){
						return 1;
					}
				}else{
					return 2;
				}

			}else{

				return 3;
			}

		}


		function getAllMobiles($table){
			$arrMob = array();
			if(is_array($this->userId)){
				$sqlStr = " id  IN ('".implode("','",$this->userId)."') ";

			}
			$sql = "SELECT mob_no FROM ".$table." WHERE ".$sqlStr;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){

				array_push($arrMob,$row['mob_no']);
			}
			return $arrMob;


		}

		function getSubUserByEmail(){

			$sql = "select id,mob_no,email,name from mst_subscribers where active=1 and deleted=0 and  email='".$this->email."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		}

		function getSubUserByMob(){

			$sql = "select id,mob_no,email,name from mst_subscribers where active='1' and deleted='0' and  mob_no='".$this->mobNo."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		}

		function getVenUserByEmail(){

			$sql = "select id,mob_no,username,email,name from mst_vendors where active=1 and deleted=0 and email='".$this->email."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);

			if(mysql_num_rows($result)>0){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		}

		function updateSubPassByEmail(){

			$sql = "update mst_subscribers set password='".md5($this->password)."' where id='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){

				return true;

			}else{
				return false;
			}
		}
		
		
		function MobileAddSubscriber(){
			global $myDB;
			$id = getNewUUID();
			
			$sql="INSERT INTO mst_subscribers SET 
			id='$id',
			password = '".$this->password."',
			mob_no = '".$this->mobNo."',
			name = '".$this->username."',
			deleted='0',
			reg_step='1',
			mob_confirmed=1,
			term_conditions=1,
			active=1,
			confirmed=1,
			created=NOW(),
			source='Mobile'
			";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)){
					$this->userId = $id;
					return $id;
				}else{
					return false;
				}
		
		}
		
		
		function MobileUpdateSubscriber(){
			$sql1="select categories.id from categories";
			$result1 = mysql_query($sql1) ;
			//$result1 = mysql_fetch_array($result1);
			while($row = mysql_fetch_assoc($result1)){
				$arrSender[]=$row['id'];
			}
			
			if(is_array($arrSender)){
				$categoryId = " category_id = '".implode(',',$arrSender)."', ";
			}
			
			$sql="update mst_subscribers set
			email='".$this->email."',
			gender='".$this->gender."',
			location_id='".$this->locationId."',
			occupation_id='".$this->occupationId."',
			reg_step='2',
			$categoryId
			age_range_id='".$this->ageRangeId."' where id='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){

				return true;

			}else{
				return false;
			}
		
		}
		
		function getLastBroadcastDetails(){
			$sql="select * from mst_vendors where active=1 and deleted=0 and confirmed=1 and broadcast_reminder=1 and broadcast_reminder_sent=0 limit 100";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				while($row=mysql_fetch_assoc($result)){
					$arrVendors[]=$row;
				}
				return $arrVendors;
			}else{
				return false;
			}
		}
		
		function updateVenPassByEmail(){

			$sql = "update mst_vendors set password='".md5($this->password)."' where id='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){

				return true;

			}else{
				return false;
			}
		}

		function createDefaultSenderName(){

			$sql = "SELECT mob_no FROM  mst_vendors WHERE id='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);

			if(isset($row['mob_no'])){

				$sql = "INSERT INTO senderids (id,senderid,vendor_id,status,created) VALUES(UUID(),'".formatMobno($row['mob_no'])."','".$this->userId."','active',NOW())";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);

			}

		}

		function createDefaultAddbook(){

			$sql = "INSERT INTO addressbooks (id,addressbook,vendor_id,created) values(UUID(),'Default','".$this->userId."',NOW())";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);


		}



//add
function getVenCredit($id = -1 ){
		if($id == -1)
			return false;
		$sql = "SELECT 	credit_balance FROM  credit_balances WHERE user_id='$id'";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if( mysql_num_rows($result)!=0 )
		{
			while($item = mysql_fetch_array($result) )
			return $item;
		}
		return false;//die();
		}
		
		function checkVenDuplicateUrl(){
			$sql = "SELECT 	* FROM  mst_vendors WHERE id != '".$this->userId."' and url= '".$this->url."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if( mysql_num_rows($result) > 0 )
			{
				return true;
			}else{
				return false;
			}
		}

	}
?>