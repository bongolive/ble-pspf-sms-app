<?php 
include_once ('bootstrap.php');
require_once(LIB_DIR.'inc.php');
include(MODEL.'sms/sms.class.php');

$objSms = new sms();

// read raw POST data 
$postData = file_get_contents("php://input"); 

// extract XML structure from it using PHP�s DOMDocument Document Object Model parser 
$dom = new DOMDocument(); 
$dom->loadXML($postData); 

// create new XPath object for quering XML elements (nodes) 
$xPath = new domxpath($dom); 

// query �message� element 
$reports = $xPath->query("/DeliveryReport/message"); 

// write out attributes of each �message� element 
foreach ($reports as $node) { 
	$objSms->smsId = $node->getAttribute('id');
	$objSms->status =$node->getAttribute('status');
	$objSms->updateDeliveryReport();
} 
?>