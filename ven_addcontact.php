<?php
/****************************************************************************************************************
*	File : ven_addcontact.php
*	Purpose: listing of addressbook, adding new contact
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');
	
	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	

	//if(isset($_REQUEST['action']) && $_REQUEST['action']=='add'){

	switch($_REQUEST['action']){
	
		case'add':

			$postedDate=$_REQUEST['year']."/".$_REQUEST['month']."/".$_REQUEST['day'];
			$objAddressbook->addressbookId = $_REQUEST['addbookId'];
			$objAddressbook->title = cleanQuery($_REQUEST['title']);
			$objAddressbook->fname = cleanQuery($_REQUEST['fname']);
			$objAddressbook->lname = cleanQuery($_REQUEST['lname']);
			$objAddressbook->email = cleanQuery($_REQUEST['email']);
			$objAddressbook->mob_no = CutMobileNumber(cleanQuery(trim($_REQUEST['mob_no'])));
			//$objAddressbook->customs = cleanQuery($_REQUEST['customs']);
			
			$objAddressbook->mob_no2 = CutMobileNumber(cleanQuery($_REQUEST['mob_no2']));
			$objAddressbook->gender = cleanQuery($_REQUEST['gender']);
			$objAddressbook->birth_date = date("Y-m-d", strtotime(trim($postedDate)));
			$objAddressbook->address = cleanQuery($_REQUEST['address']);
			$objAddressbook->optional_one = cleanQuery($_REQUEST['optional_one']);
			$objAddressbook->optional_two = cleanQuery($_REQUEST['optional_two']);
			$objAddressbook->area = cleanQuery($_REQUEST['area']);
			$objAddressbook->city = cleanQuery($_REQUEST['city']);
			$objAddressbook->country = cleanQuery($_REQUEST['country']);
			if($objAddressbook->addContact()){
				
				$msg = VEN_ADD_CONTACT_MSG_1;
				foreach($_REQUEST['addbookId'] as $addbookId){
				
					$objAddressbook->updateCountAddressbook($addbookId);
				
				}

			}else{
				$msg = VEN_ADD_CONTACT_MSG_2;
				$errorFlag=1;
			}
		break;
		case'edit':
			$objAddressbook->contactId = $_REQUEST['cid'];
			$arrContactDetails = $objAddressbook->getContactDetail();
		
			if($arrContactDetails){
			
				$smarty->assign('arrContactDetails',$arrContactDetails);
				$timestamp = strtotime($arrContactDetails['birth_date']);
				$smarty->assign('day',date("d",$timestamp));
				$smarty->assign('month',date("m",$timestamp));
				$smarty->assign('year',date("Y",$timestamp));
				$smarty->assign('cid',$_REQUEST['cid']);
				$action = 'update';

			}
		break;
		case'update':
			echo $_REQUEST['areaSSS'];
			 $postedDate=$_REQUEST['year']."/".$_REQUEST['month']."/".$_REQUEST['day'];
			 $postedDate  = date("Y-m-d", strtotime(trim($postedDate)));
			
			$objAddressbook->contactId = $_REQUEST['cid'];
			$objAddressbook->title = $_REQUEST['title'];
			$objAddressbook->fname = $_REQUEST['fname'];
			$objAddressbook->lname = $_REQUEST['lname'];
			$objAddressbook->email = $_REQUEST['email'];
			$objAddressbook->mob_no = CutMobileNumber($_REQUEST['mob_no']);
			//$objAddressbook->customs = $_REQUEST['customs'];
			$objAddressbook->mob_no2 = CutMobileNumber(cleanQuery($_REQUEST['mob_no2']));
			$objAddressbook->gender = cleanQuery($_REQUEST['gender']);
			$objAddressbook->birth_date =$postedDate;
			$objAddressbook->address = cleanQuery($_REQUEST['address']);
			$objAddressbook->optional_one = cleanQuery($_REQUEST['optional_one']);
			$objAddressbook->optional_two = cleanQuery($_REQUEST['optional_two']);
			$objAddressbook->area = cleanQuery($_REQUEST['area']);
			$objAddressbook->city = cleanQuery($_REQUEST['city']);
			$objAddressbook->country = cleanQuery($_REQUEST['country']);
			$status = $objAddressbook->updateContact();
			if($status){
				redirect('ven_addbook_details.php?bid='.$_REQUEST['bid']);
			
			}else{
				$objAddressbook->contactId = $_REQUEST['cid'];
				$arrContactDetails = $objAddressbook->getContactDetail();
		
				if($arrContactDetails){
					$smarty->assign('arrContactDetails',$arrContactDetails);
					$timestamp = strtotime($arrContactDetails['birth_date']);
					$smarty->assign('day',date("d",$timestamp));
					$smarty->assign('month',date("m",$timestamp));
					$smarty->assign('year',date("Y",$timestamp));
					$smarty->assign('cid',$_REQUEST['cid']);
					$action = 'update';
				}
			}
		break;
		default:
			$action ="add";
	
	}

	// addressbook listing
	$arrAddressbook = $objAddressbook->getAddressbookList(false);
	$smarty->assign('arrCountry',getArray('countries','country_code','country'));
	$smarty->assign('currentYear',date("Y")+1);
	$smarty->assign('action',$action);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('gender',$_REQUEST['gender']);
	$smarty->assign('country',$_REQUEST['country']);
	$smarty->assign('msg',$msg);
	$smarty->assign('mob_no_length',$project_vars['mob_no_length']);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->display("vendor/ven_addcontact.tpl");
	

	include 'footer.php';
?>