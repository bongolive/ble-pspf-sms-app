<?php
/********************************************
	*	File	: shell.send_sesponse.php				*
	*	Purpose	: Send response to user and updating text_in_log table*
	*	Author	: leonard nyirenda						*
*********************************************/
include_once ('bootstrap.php');
include_once(LIB_DIR . "shell.inc.php");
include(MODEL.'incoming.class.php');
include_once(LIB_DIR.'smsProcessLib.php');

function sendSMStoBongoliveGateway($mob,$sms){
			define ("URL_API_DOMAIN", $project_vars['prem_gw_url']);
			$senderid = urlencode("15357");
			$key = urlencode($project_vars['prem_gw_key']);
			$mob=$mob;
			$destnum = urlencode($mob);
			$message = urlencode($sms);
			$posturl = URL_API_DOMAIN."key=$key&type=text&contacts=$mob&senderid=$senderid&msg=$message&price=150";
    		$proxy_ip = '192.168.0.2'; //proxy IP here
			$proxy_port = 8080; //proxy port from your proxy list
    		$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $posturl);
			curl_setopt($ch, CURLOPT_HEADER, 0); // no headers in the output
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$response = curl_exec($ch);
			return $response;
	}
$debug = false;

// Infinite loop starts
while(true) {
	//echo "nipo ndani<br />";
	$obj = new incoming();
	$obj->status='response to be sent';
	$arrIncoming = $obj->returnSmsByStatus();

	
	if($arrIncoming != false){
		//echo "<pre>Data found, Now Processing....<br />";
		foreach($arrIncoming as $row){
			$obj->sms_in_id = $row['sms_in_id'];
			$obj->textMessage = cleanQuery($row['outbound_response']);
			$checkSms=$obj->checkSentResponses();
			
			if($row['system_error']==0){
				$status="response sent";
			}else if($row['system_error']==1){
				$status="retry";
			}

			//if($checkSms==false){
			
				define ("URL_API_DOMAIN", $project_vars['prem_gw_url']);
				$senderid = urlencode("15357");
				$key = urlencode($project_vars['prem_gw_key']);
				$mob=$row['phone'];
				$destnum = urlencode($mob);
				$message = urlencode(trim($row['outbound_response'])); 
				$posturl = URL_API_DOMAIN."key=$key&type=text&contacts=$mob&senderid=$senderid&msg=$message&price=150";
				$proxy_ip = '192.168.0.2'; //proxy IP here
				$proxy_port = 8080; //proxy port from your proxy list
				
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL,$posturl);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 60);
				//curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
				//curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
				//curl_setopt($ch, CURLOPT_PROXY,$proxy_ip);
				$output = curl_exec($ch);
				
				//$output = sendSMStoBongoliveGateway($row['phone'],$row['outbound_response']);
				$split =explode(":",$output);
				$response = $split[0];
			//}				
				$obj->sms_in_id = $row['sms_in_id'];
			    $obj->status = $status;
				$obj->textMessage = cleanQuery($row['outbound_response']);
				$obj->phone = $row['phone'];
			    $obj->system_error='0';
			if(trim($response) == "SMS SUBMITTED"){
				//updating the database
			    $obj->setProccesed();
				$obj->insertOutboundResponses();
			}else{
				$obj->status="Sending fail";
				if($checkSms==false){
					$obj->insertOutboundResponses();
				}
			}
		}
	}else{
		echo "<pre>No data found, Going to sleep..<br /><br />";
	}	
	// Infinite loop starts		
} 
include_once(LIB_DIR . "close.php");
?>