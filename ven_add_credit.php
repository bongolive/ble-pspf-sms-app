<?php
	/********************************************
	*	File	: ven_add_credit.php				*
	*	Purpose	: Vendor add credit to sub account *
	*	Author	: Leonard Nyirenda						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'credit/credit.class.php');
	include(MODEL.'vendors/vendor.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	require_once 'swift/lib/swift_required.php';


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	if($_SESSION['user']['parent_ven_id']==''){
		(int) $remoteCredit = getRemoteCreditBalance();
		(int) $resCredit = getTotalCreditOfAllVendors();
		$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
	} 

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	if($_SESSION['user']['id'] != ''){
		$resellerDetail = $objUser->getResellerDetail();
	}else{
		$resellerDetail['name'] ='Admin';
		$resellerDetail['email'] ='pspf@pspf-tz.org';
	}
	$smarty->assign('resellerDetails', $resellerDetail);
	$smarty->assign("parentVenId", $_SESSION['user']['id']);
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	
	$objCredit = new credit();
	$objCredit->parentVenId=$_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'edit':
			$objCredit->creditRequestId = $_GET['id'];
			$arrCreditDetails = $objCredit->getRequestedCreditDetails();
			$objUser->userId = $_GET['id'];
			$arrAddCredit = $objUser->getUserDetailVen();
			
			$smarty->assign('arrCreditScheme',getArray('credit_schemes','id','rate',' ORDER BY rate ASC'));
			
		break;
		case'add':

			$objCredit->creditRate = $_REQUEST['crdtSchmId'];

			$tmp_rate = $objCredit->getCrdtSchm();

			if ($tmp_rate){
			
				$objCredit->crdtSchmId 	= $tmp_rate['id'];	

			}

			else {

				$bon = $objCredit->getCrdtBon();
				
				if($bon){

					$objCredit->crdtSchmId 	= $bon['id'];

				} else {

					$sch = $objCredit->addCreditBonus();
			
					$objCredit->crdtSchmId 	= $sch['id'];

				}

			}	
			//$crdtSchem = explode("###",$_REQUEST['crdtSchmId']);
			$objCredit->creditReqId = str_rand($project_vars["random_creditid_length"],'caps_alphanum');
			if ($_REQUEST['status']!=''){			
				$objCredit->creditRequestStatus = $_REQUEST['status'];
			}
			else {$objCredit->creditRequestStatus = 'pending';}
			if ($_REQUEST['payment_status']!=''){
				$objCredit->creditRequestPaymentStatus 	= $_REQUEST['payment_status'];
			}
			else {$objCredit->creditRequestPaymentStatus = 0;}
			
			$objUser->userId = $_REQUEST['userId'];

			$userDetail = $objUser -> getUserDetailVen( );
			
			$objCredit -> creditRequest 			= $_REQUEST['crditRequested'];
			//$objCredit->crdtSchmId 				= $crdtSchem[0];
			//$objCredit->creditTotalCost 			= (intval($crdtSchem[1])*intval($_REQUEST['crditRequested']));
			$objCredit -> creditTotalCost 			= (intval($_REQUEST['crdtSchmId'])*intval($_REQUEST['crditRequested']));
			$objCredit -> parentVenId = $_SESSION['user']['id'];
			$objCredit -> userId 	= $_REQUEST['userId'];
			
			switch($_REQUEST['payment_status']){
						case '0': $pay = 'Not paid';break;
						case '1': $pay = 'Paid';break;
						case '2': $pay = 'Partially Paid';break;
						case '3': $pay = 'Receipt Sent';break;
						case '4': $pay = 'Bonus';break;
						case '': $pay = 'Not paid';break;
					}
			
			$parentVenCreditBal = $objCredit -> getParentVenCXreditBalance();
			$crdtRequested = $_REQUEST['crditRequested'];
			
			
			
			if( $_REQUEST['status'] == 'allocated' ){
				if($parentVenCreditBal < $crdtRequested){
					$status = false;
					$msg = "Credit was not added to a client because you do not have sufficient credit balance on your account";
					$errorFlag = 1;
					
					
					$objCredit->creditRequestId = $_GET['id'];
					$arrCreditDetails = $objCredit -> getRequestedCreditDetails();
					$objUser->userId = $_GET['id'];
					$arrAddCredit = $objUser -> getUserDetailVen();
					$action = 'edit';
			
					$smarty -> assign('arrCreditScheme',getArray('credit_schemes','id','rate',' ORDER BY rate ASC'));
				}else{
					$status = $objCredit -> requestCreditAdm ();
					$objCredit -> removeCreditToParentVendor ();
					$msg = "Credit added successfully.";
					$action = 'edit';
				}
			}else{
				$objCredit->updateRequestedCredit2();
				$status='false';
				$msg="Bonus Status updated successfully.";
			}
			

			if($status) {
			
				//updating Credit balance reminder
				$objVendor = new vendor();
				$objVendor->userId = $_REQUEST['userId'];
				$rowBalance = $objVendor->getVendorCreditBalance();
				if(($rowBalance['credit_balance']-$rowBalance['credit_blocked']) > $rowBalance['credit_level']){
					$objVendor->reminderSent = 0;
					$objVendor->updateVendorReminderStatus();
				}

				$smarty->assign('req_id',$objCredit->creditReqId);				
				if ($_REQUEST['status']==""){
					$req_status = 'pending';
				}
				else {$req_status = $_REQUEST['status'];}
				

				if ($project_vars['mail_send']===true){	

					//$tmp_rate = explode('###', $_REQUEST['crdtSchmId']);
					//$objCredit->crdtSchmId = $tmp_rate[0];
					//$rate = $objCredit->getCrdtSchmDetails();
									
						$smarty->assign('payment_true', true);
						$smarty->assign('newPayStatus', $pay);
						$smarty->assign('quantity', $_REQUEST['crditRequested']);
						//$smarty->assign('rate', $rate['rate']);
						$smarty->assign('rate', $_REQUEST['crdtSchmId']);
						//$smarty->assign('total', $_REQUEST['crditRequested']*$rate['rate']);
						$smarty->assign('total', $_REQUEST['crditRequested']*$_REQUEST['crdtSchmId']);
						$smarty->assign('PayStatus', $pay);
						$smarty->assign('status', $req_status);

						$smarty->assign('status_true', true);
						$smarty->assign('newStatus', $req_status);

					try{
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
						
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);

							$body = $smarty->fetch('mail_template/mail_change_status.tpl');

							$message = Swift_Message::newInstance('Status of Purchase Request')
							->setFrom(array($resellerDetail['email']=>$resellerDetail['name']))
							->setTo(array($userDetail['email']=>$userDetail['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);

					} catch (Exception $e){
					
						$msg = SUB_REGISTER_MSG_08.$e->getMessage();
						$errorFlag = 1;
					}
					try{
						if($project_vars['mob_confirmation']==true){		
							$massege = str_rand($project_vars["random_mob_conf_length"],$project_vars["random_string_type"]);
							smsSendWithCurl($userDetail['mob_no'],str_replace('MESSAGE',$massege,$project_vars['register_message']),$project_vars['sms_api_senderid']);
							$objUser->mob_conf_msg = $massege;
							$objUser->insertSubMobConfMsg();
							}
					}catch(Exception $objE){
					
							$msg = SUB_REGISTER_MSG_09.$objE->getMessage();;
							$errorFlag = 1;
					
					}

				}
			}			

			if($status && $_REQUEST['status'] == 'allocated'){
				$msg = "Credits allocated to vendors";
			}elseif($status && $_REQUEST['status'] == 'rejected'){
				$msg = "Credits request rejected for vendors";
			}
			
		break;
		default:
			$action = "";
		break;
	
	
	}

	
	$selfUrl = BASE_URL.'ven_add_credit.php';
	$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
	$pagingUrl.= '&vendor_id='.$_REQUEST['vendor_id'];
	$pagingUrl.= '&username='.$_REQUEST['username'];
	SmartyPaginate::setUrl($pagingUrl);

	
	$objCredit->vendorName		= $_REQUEST['vendorName'];
	$objCredit->vendor_id		= $_REQUEST['vendor_id'];
	$objCredit->username		= $_REQUEST['username'];

	$objCreditRequest =  $objCredit->getAddCreditSubVenList();


	$smarty->assign("payment_status", $_GET['payment_status']);
	
	$smarty->assign("msg",$msg);
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("vendorName",$_REQUEST['vendorName']);
	$smarty->assign("username",$_REQUEST['username']);
	$smarty->assign("vendor_id",$_REQUEST['vendor_id']);
	$smarty->assign('ajaxUrl',BASE_URL.'credit_ajax_adm.php');

	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("arrCrdtSchm",getArray('credit_schemes','id','rate',' where active=1 '));
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("action",$action); 
	$smarty->assign("arrCreditDetails",$arrCreditDetails);
	$smarty->assign("arrAddCredit",$arrAddCredit);
	$smarty->assign("objCreditRequest",$objCreditRequest);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_add_credit.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
