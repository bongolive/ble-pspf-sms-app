<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('privacy.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/privacy.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/privacy.tpl');
		}
		else
		{
			$smarty->display('privacy.tpl');
		}
	}
	
	include('footer.php');
?>