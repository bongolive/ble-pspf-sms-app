<?php
	
	/********************************************
	*	File	: adm_credits_request.php				*
	*	Purpose	: Admin crdit  management for vendor *
	*	Author	: Akhilesh						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'credit/credit.class.php');
	include(CLASSES.'SmartyPaginate.class.php');

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);	

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objUser = new user();
	$objCredit = new credit();
	
	$selfUrl = BASE_URL.'ven_credits_request.php';
	$pagingUrl.= '?rate='.$_GET['rate']; 
	$pagingUrl.= '&quantity='.$_GET['quantity']; 
	$pagingUrl.= '&status='.$_GET['status']; 
	$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
	$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
	SmartyPaginate::setUrl($pagingUrl);

	$objCredit->crdtSchmId	= $_REQUEST['rate']; 
	$objCredit->quantity	= $_REQUEST['quantity'];
	$objCredit->status		= $_GET['status'];
	$objCredit->startDate	= getDateFormat($_REQUEST['startDate'],'Y-m-d');
	$objCredit->endDate		= getDateFormat($_REQUEST['endDate'],'Y-m-d');

	$objCredit->userId = $_SESSION['user']['id'];

	$objCreditRequest =  $objCredit->getRequestedCreditList();

	$smarty->assign("msg",$msg);
	$smarty->assign("rate", $_REQUEST['rate']); 
	$smarty->assign("quantity",$_REQUEST['quantity']);
	$smarty->assign("status",$_GET['status']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);

	$smarty->assign("arrVendors",getArray('mst_vendors','id','name',' where active=1 and deleted=0 '));
	$smarty->assign("arrCrdtSchm",getArray('credit_schemes','id','rate',' where active=1 '));
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("action",$action); 
	$smarty->assign("arrCreditDetails",$arrCreditDetails);
	$smarty->assign("objCreditRequest",$objCreditRequest);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_credit_requested.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
