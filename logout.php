<?php
	include_once "bootstrap.php";
	include_once(LIB_DIR . "inc.php");
	
	session_unset();
	session_destroy();
	$_SESSION = array();
	
	redirect('index.php');
?>