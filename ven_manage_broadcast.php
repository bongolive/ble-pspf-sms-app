<?php
	
	/********************************************
	*	File	: ven_manage_broadcast.php		*
	*	Purpose	: Vendor manage Subaccount Boadcast 		*
	*	Author	: leonard Nyirenda						*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'sms/sms.class.php');
	include(MODEL.'vendors/vendor.class.php');
	include(CLASSES.'SmartyPaginate.class.php');


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}

	$selfUrl = BASE_URL.'ven_manage_broadcast.php';

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	$resellerDetail = $objUser->getResellerDetail();
	$smarty->assign('resellerDetails', $resellerDetail);
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	$objSms  = new sms();
	$objSms->parentVenId = $_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){

		/*case'':
			$pagingUrl = $selfUrl;
			SmartyPaginate::setUrl($pagingUrl);
			$arrSms =  $objSms->getSmsList();
		break;*/
	
		case'search':
			
			// setting pagin url			
			$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
			$pagingUrl.= '&username='.$_REQUEST['username'];
			//$pagingUrl.= '&broadcaster_type='.$_REQUEST['broadcaster_type'];
			$pagingUrl.= '&mob_no='.$_REQUEST['mob_no']; 
			$pagingUrl.= '&sms_type='.$_REQUEST['sms_type'];
			$pagingUrl.= '&textMessage='.$_REQUEST['textMessage']; 
			$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
			$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
			$pagingUrl.= '&job_name='.$_REQUEST['job_name'];
			$pagingUrl.= '&senderid='.$_REQUEST['senderid'];
			$pagingUrl.= '&action='.$_REQUEST['action'];
			SmartyPaginate::setUrl($pagingUrl);
			
			// stting search param
			$objSms-> vendorName		= $_REQUEST['vendorName']; 
			$objSms-> username			= $_REQUEST['username']; 
			$objSms-> mob_no			= $_REQUEST['mob_no']; 
			$objSms-> sms_type			= $_REQUEST['sms_type']; 
			$objSms-> textMessage		= $_REQUEST['textMessage'];
			$objSms-> jobName			= $_REQUEST['job_name'];
			$objSms-> senderName		= $_REQUEST['senderid'];
			$objSms-> startDate			= getDateFormat($_REQUEST['startDate'],'Y-m-d');
			$objSms-> endDate			= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
			
			// getting search sms
			$arrSms =  $objSms->getSubVenSmsList();
			
		break;
		case'edit':
				
				$objSms->smsId	= $_REQUEST['sid']; 
				$arrSmsToEdit =  $objSms->getSmsToReschedule();
				$smarty->assign("arrSmsToEdit",$arrSmsToEdit);
			
			break;
		case'update':
				
				$objSms->smsId			= $_REQUEST['sid']; 
				$objSms->scheduleTime	= getDateFormat($_REQUEST['scheduleDate']); 
				$objSms->rescheduleSms();
				$action = "";
				$_REQUEST['textMessage']="";
			break;
		default:
			$action = "";
	
	
	}

	$arrSenderid=$objSms->getVenderSenderidList();
	$smarty->assign("msg",$msg);
	$smarty->assign("vendorName",$_REQUEST['vendorName']);
	$smarty->assign("username",$_REQUEST['username']);
	$smarty->assign("mob_no", $_REQUEST['mob_no']);
	$smarty->assign("sms_type",$_REQUEST['sms_type']);
	$smarty->assign("textMessage",$_REQUEST['textMessage']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	$smarty->assign("senderid",$_REQUEST['senderid']);
	$smarty->assign("job_name",$_REQUEST['job_name']);
	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("arrSenderid",$arrSenderid);
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("ajaxUrl",BASE_URL.'adm_broadcast_ajax.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrSms",$arrSms);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_broadcast.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
