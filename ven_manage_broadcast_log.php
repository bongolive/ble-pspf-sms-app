<?php
	
	/********************************************
	*	File	: vem_manage_credits_request.php		*
	*	Purpose	: Vendor Boadcast management		*
	*	Author	: Leonard Nyirenda						*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'sms/sms.class.php');
	include(MODEL.'vendors/vendor.class.php');
	//include(CLASSES.'SmartyPaginate.class.php');


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}

	$selfUrl = BASE_URL.'ven_manage_broadcast_log.php';

	//SmartyPaginate::connect();
	//SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	$resellerDetail = $objUser->getResellerDetail();
	$smarty->assign('resellerDetails', $resellerDetail);
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	$objSms  = new sms();
	$objSms->parentVenId = $_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'dnd':
			//print_r($_REQUEST);
			// stting search param
			$objSms-> userId		= $_REQUEST['broadcaster']; 
			$objSms-> startDate		= getDateFormat($_REQUEST['startDate'],'Y-m-d');
			$objSms-> endDate		= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
			
			
			$dirPath = 'download_file/';
			$fileName = 'delive_report_'.time().'.xls';
			
			$countData = $objSms->getCountDataReport(); 
			
			//echo $countData,'<br>';// exit;


			$handle= fopen($dirPath.$fileName,'a+');
			
			if($handle){
				
				$header = "User\t Sent Time \t Delivered Time \t Status \t API Status \t Sender Name \t Mobile Number \t Msg Count \t Message \t \n";	
				fwrite($handle,$header);
				
				$rec = 1000;
				$offset= 0;
				$order   = array("\r\n", "\n", "\r");

				while($offset < $countData){
				
					$arrData = $objSms->dowloadSmsLog($offset,$rec);
					
					if($arrData){

						foreach($arrData as $value){
							
							$contents=''; 
							$contents.=(isset($value['username'])?$value['username']."\t":"\t");
							$contents.=(isset($value['created'])?$value['created']."\t":"\t");
							$contents.=(isset($value['remote_deliv_time'])?$value['remote_deliv_time']."\t":"\t");
							$contents.=(isset($value['status'])?$value['status']."\t":"\t");
							$contents.=(isset($value['remote_status'])?$value['remote_status']."\t":"\t");
							$contents.=(isset($value['source_addr'])?$value['source_addr']."\t":"\t");
							$contents.=(isset($value['destination_addr'])?$value['destination_addr']."\t":"\t");
							$contents.=(isset($value['msg_count'])?$value['msg_count']."\t":"\t");
							$contents.=(isset($value['short_message'])?str_replace($order,' ', $value['short_message'])."\t":"\t");
							$contents.= "\n"; 

							
							fwrite($handle,$contents);
							
							//echo "<br>$contents";
						}
						
					
						$offset = $offset+$rec;
					}	
				}

				
				
				fclose($handle);
				
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Length: " . filesize($dirPath.$fileName));
				header('Content-type: application/ms-excel');
				header("Content-Disposition: attachment; filename=$fileName");
				 
				ob_clean();
				flush();
				readfile($dirPath.$fileName);
				unlink($dirPath.$fileName);
				exit;	
			}else{
			
				echo 'file not created';
			}

			
		break;
		
	}
	//$arrAdmins		= getArray('mst_admins','id','username , name',' where active=1 and deleted=0 ');	
	//$arrVendors		= getArray('mst_vendors','id','username, name',' where active=1 and deleted=0 order by username');
	//$arrSmsSender	= array_merge($arrAdmins,$arrVendors);
	
	$smarty->assign("msg",$msg);
	$smarty->assign("arrSmsSender",$arrVendors);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	$smarty->assign("broadcaster", $_REQUEST['broadcaster']);
	$smarty->assign("selfUrl",$selfUrl);
	
	//SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_broadcast_log.tpl");
	//SmartyPaginate::disconnect();
		
	include 'footer.php';

?>