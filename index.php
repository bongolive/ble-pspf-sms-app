<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include("header.php");

	if(isset($_SESSION['user']) && $_SESSION['user']['user_type']=='VEN'){
	
		redirect("ven_dashboard.php");
	}

	if(isset($_REQUEST['action']) && $_REQUEST['action']=='login' ){

		include ('login.php');
	}

	$smarty->assign('msg',$msg);
	$smarty->display('templates/index.tpl');

	include('footer2.php');	
?>