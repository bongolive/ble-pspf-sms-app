-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pspf
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addressbooks`
--

DROP TABLE IF EXISTS `addressbooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressbooks` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `addressbook` varchar(200) CHARACTER SET latin1 NOT NULL,
  `vendor_id` char(36) CHARACTER SET latin1 NOT NULL,
  `contacts_count` int(12) NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressbooks`
--

LOCK TABLES `addressbooks` WRITE;
/*!40000 ALTER TABLE `addressbooks` DISABLE KEYS */;
INSERT INTO `addressbooks` VALUES ('b78b9726-1a85-11e2-961b-001f3c930a21','Default','1abc4c3a-84a3-11e2-bf8a-00185176683e',3,'','2012-10-20 10:13:58','0000-00-00 00:00:00'),('53590a13-a8be-11e2-a0b9-e83935455ca0','pensioners','1abc4c3a-84a3-11e2-bf8a-00185176683e',1,'','2013-04-19 09:56:57','0000-00-00 00:00:00'),('a1b4f2d9-b944-11e2-8106-e83935455ca0','another test','1abc4c3a-84a3-11e2-bf8a-00185176683e',1,'','2013-05-10 10:38:39','0000-00-00 00:00:00'),('a8a11809-b94a-11e2-8974-e83935455ca0','ROIs','1abc4c3a-84a3-11e2-bf8a-00185176683e',1,'','2013-05-10 11:21:48','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `addressbooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `age_ranges`
--

DROP TABLE IF EXISTS `age_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `age_ranges` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `age_range` varchar(10) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `age_ranges`
--

LOCK TABLES `age_ranges` WRITE;
/*!40000 ALTER TABLE `age_ranges` DISABLE KEYS */;
/*!40000 ALTER TABLE `age_ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `category` varchar(200) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES ('b8bc5c96-c0c8-11df-8f82-000c29b8e356','Food & Drinks',1,'2010-09-15 18:24:48','0000-00-00 00:00:00'),('b8bc5fca-c0c8-11df-8f82-000c29b8e356','Beauty, Fashion & Clothing',1,'2010-09-15 18:25:04','2010-10-07 09:55:10'),('b8bc6038-c0c8-11df-8f82-000c29b8e356','Music, Entertainment & Sports',1,'2010-09-15 18:25:20','0000-00-00 00:00:00'),('b8bc6074-c0c8-11df-8f82-000c29b8e356','Career & Education',1,'2010-09-15 18:25:39','0000-00-00 00:00:00'),('b8bc60a6-c0c8-11df-8f82-000c29b8e356','Mobiles, Electronics & Computers',1,'2010-09-15 18:25:57','0000-00-00 00:00:00'),('b8bc60d8-c0c8-11df-8f82-000c29b8e356','Home, Decor & Furnishings',1,'2010-09-15 18:26:15','0000-00-00 00:00:00'),('b8bc610a-c0c8-11df-8f82-000c29b8e356','Financial Products',1,'2010-09-15 18:26:43','0000-00-00 00:00:00'),('b8bc613c-c0c8-11df-8f82-000c29b8e356','Business Services',1,'2010-09-15 18:26:58','0000-00-00 00:00:00'),('b8bc6164-c0c8-11df-8f82-000c29b8e356','Hardware & Equipment',1,'2010-09-15 18:27:18','0000-00-00 00:00:00'),('b8bc61a0-c0c8-11df-8f82-000c29b8e356','Travel & Holiday',1,'2010-09-15 18:27:39','0000-00-00 00:00:00'),('b8bc61d2-c0c8-11df-8f82-000c29b8e356','Real Estate & Services',1,'2010-09-15 18:27:55','0000-00-00 00:00:00'),('b8bc6204-c0c8-11df-8f82-000c29b8e356','Internet & Services',1,'2010-09-15 18:28:08','0000-00-00 00:00:00'),('b8bc6236-c0c8-11df-8f82-000c29b8e356','Cars, Bikes and Automotive',1,'2010-09-15 18:28:22','0000-00-00 00:00:00'),('b8bc6268-c0c8-11df-8f82-000c29b8e356','Jobs & Employment',1,'2010-09-15 18:28:45','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `city` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES ('a2d35bf6-bfc3-11df-8f82-000c29b8e356','Arusha','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3701e-bfc3-11df-8f82-000c29b8e356','Bagamoyo','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d37d70-bfc3-11df-8f82-000c29b8e356','Bukoba','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d38ab8-bfc3-11df-8f82-000c29b8e356','Chalinze','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d398aa-bfc3-11df-8f82-000c29b8e356','Dar-es-Salaam','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3a53e-bfc3-11df-8f82-000c29b8e356','Dodoma','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3b128-bfc3-11df-8f82-000c29b8e356','Iringa','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3bd8a-bfc3-11df-8f82-000c29b8e356','Kibaha','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3cafa-bfc3-11df-8f82-000c29b8e356','Kigoma','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3d6d0-bfc3-11df-8f82-000c29b8e356','Lindi','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3e328-bfc3-11df-8f82-000c29b8e356','Lushoto','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3ef4e-bfc3-11df-8f82-000c29b8e356','Mbeya','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d3fab6-bfc3-11df-8f82-000c29b8e356','Morogoro','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d40862-bfc3-11df-8f82-000c29b8e356','Moshi','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d413de-bfc3-11df-8f82-000c29b8e356','Mtwara','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d41ff0-bfc3-11df-8f82-000c29b8e356','Musoma','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d42d60-bfc3-11df-8f82-000c29b8e356','Mwanza','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d438fa-bfc3-11df-8f82-000c29b8e356','Shinyanga','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d4450c-bfc3-11df-8f82-000c29b8e356','Singida','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d4510a-bfc3-11df-8f82-000c29b8e356','Songea','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d45c9a-bfc3-11df-8f82-000c29b8e356','Sumbawanga','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d46a64-bfc3-11df-8f82-000c29b8e356','Tabora','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d4754a-bfc3-11df-8f82-000c29b8e356','Tanga','2010-09-14 11:18:01','0000-00-00 00:00:00'),('a2d47fd6-bfc3-11df-8f82-000c29b8e356','Zanzibar','2010-09-14 11:18:01','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `addressbook_id` char(36) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(25) CHARACTER SET latin1 NOT NULL,
  `mob_no2` varchar(14) NOT NULL,
  `title` varchar(10) CHARACTER SET latin1 NOT NULL,
  `fname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birth_date` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL DEFAULT 'Tanzania',
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `optional_one` varchar(255) CHARACTER SET latin1 NOT NULL,
  `optional_two` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addressbook_id` (`addressbook_id`,`mob_no`),
  KEY `addressbook_id_2` (`addressbook_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES ('70c6728e-872e-11e2-b563-001f3c930a21','b78b9726-1a85-11e2-961b-001f3c930a21','757447737','','','mimi','','','1970-01-01','','','','','','','','2013-03-07 16:53:40','0000-00-00 00:00:00'),('8e5da38a-a8be-11e2-a0b9-e83935455ca0','b78b9726-1a85-11e2-961b-001f3c930a21','783370311','','Mr','andrew','','','1970-01-01','','','','','','','','2013-04-19 09:58:36','0000-00-00 00:00:00'),('8e5dafda-a8be-11e2-a0b9-e83935455ca0','53590a13-a8be-11e2-a0b9-e83935455ca0','783370311','','Mr','andrew','','','1970-01-01','','','','','','','','2013-04-19 09:58:36','0000-00-00 00:00:00'),('b7868b22-b944-11e2-8106-e83935455ca0','a1b4f2d9-b944-11e2-8106-e83935455ca0','757447737','','Mr','leonard','nyirenda','Male','1970-01-01','','','','','','','','2013-05-10 10:39:16','0000-00-00 00:00:00'),('5d1776bb-b94b-11e2-8974-e83935455ca0','b78b9726-1a85-11e2-961b-001f3c930a21','784467911','','Mr','Erick','Kato','Male','1970-01-01','','','','','','','','2013-05-10 11:26:51','2013-05-10 11:27:29'),('5d178456-b94b-11e2-8974-e83935455ca0','a8a11809-b94a-11e2-8974-e83935455ca0','784467911','','Mr','Erick','Kato','Male','1970-01-01','','','','','','','','2013-05-10 11:26:51','2013-05-10 11:27:29');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `country_code` varchar(4) NOT NULL,
  `country` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES ('255','Tanzania','2012-10-21 23:45:35','0000-00-00 00:00:00'),('254','Kenya','2012-10-22 00:16:38','2012-10-22 00:16:38'),('256','Uganda','2012-10-22 02:12:45','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_balances`
--

DROP TABLE IF EXISTS `credit_balances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_balances` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_balance` int(11) NOT NULL,
  `credit_blocked` int(12) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `sms_reminder` char(1) NOT NULL DEFAULT '0',
  `email_reminder` char(1) NOT NULL DEFAULT '0',
  `credit_level` int(11) NOT NULL DEFAULT '25',
  `reminder_sent` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_balances`
--

LOCK TABLES `credit_balances` WRITE;
/*!40000 ALTER TABLE `credit_balances` DISABLE KEYS */;
INSERT INTO `credit_balances` VALUES ('66b4e74c-dddd-11df-9aa0-0013725528cc','1abc4c3a-84a3-11e2-bf8a-00185176683e',16023,10,'2010-10-22 06:07:51','2012-07-06 15:58:43','1','1',250,'0');
/*!40000 ALTER TABLE `credit_balances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_bonus`
--

DROP TABLE IF EXISTS `credit_bonus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_bonus` (
  `id` varchar(36) CHARACTER SET latin1 NOT NULL,
  `rate` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_bonus`
--

LOCK TABLES `credit_bonus` WRITE;
/*!40000 ALTER TABLE `credit_bonus` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_bonus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_requests`
--

DROP TABLE IF EXISTS `credit_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_requests` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_request_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_scheme_id` varchar(36) CHARACTER SET latin1 NOT NULL,
  `credit_requested` int(12) NOT NULL,
  `total_cost` double NOT NULL,
  `status` enum('allocated','rejected','pending') CHARACTER SET latin1 NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_requests`
--

LOCK TABLES `credit_requests` WRITE;
/*!40000 ALTER TABLE `credit_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `credit_schemes`
--

DROP TABLE IF EXISTS `credit_schemes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `credit_schemes` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `start_range` int(12) NOT NULL,
  `end_range` int(12) NOT NULL,
  `rate` int(12) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `order` int(3) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `credit_schemes`
--

LOCK TABLES `credit_schemes` WRITE;
/*!40000 ALTER TABLE `credit_schemes` DISABLE KEYS */;
/*!40000 ALTER TABLE `credit_schemes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_messages`
--

DROP TABLE IF EXISTS `inbound_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_messages` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `sms_store_id` char(36) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `source_addr_ton` int(11) DEFAULT NULL,
  `schedule_delivery_time` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `protocol_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `short_message` text CHARACTER SET latin1,
  `msg_length` int(11) NOT NULL DEFAULT '0',
  `msg_count` int(11) NOT NULL DEFAULT '0',
  `dest_addr_npi` int(11) DEFAULT NULL,
  `source_addr` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `validity_period` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `registered_delivery` int(11) DEFAULT NULL,
  `dest_addr_ton` int(11) DEFAULT NULL,
  `data_coding` int(11) DEFAULT NULL,
  `service_type` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `replace_if_present_flag` int(11) DEFAULT NULL,
  `priority_flag` int(11) DEFAULT NULL,
  `destination_addr` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `destination_addr_formatted` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `esm_class` int(11) NOT NULL,
  `sm_default_msg_id` bigint(20) NOT NULL,
  `source_addr_npi` int(11) NOT NULL,
  `circle_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `preferred_channel_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `channel_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `remote_id` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `remote_status` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `remote_push_time` datetime DEFAULT NULL,
  `remote_rcv_time` datetime DEFAULT NULL,
  `remote_deliv_time` datetime DEFAULT NULL,
  `remote_deliver_string` text CHARACTER SET latin1,
  `remote` tinyint(1) NOT NULL,
  `user_message_id` varchar(10) DEFAULT NULL,
  `callbackurl_is_called` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_store_id` (`sms_store_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `source_addr` (`source_addr`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_messages`
--

LOCK TABLES `inbound_messages` WRITE;
/*!40000 ALTER TABLE `inbound_messages` DISABLE KEYS */;
INSERT INTO `inbound_messages` VALUES ('ef3e4f6f-c2e8-11e2-b730-e83935455ca0','ef3e203c-c2e8-11e2-b730-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,2,'tet quick sms',13,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'1','2013-05-22 17:07:30','2013-05-22 17:07:30','2013-05-22 17:07:30',NULL,0,'',0,'2013-05-22 17:07:27','2013-05-22 17:07:30'),('67bed6ee-c2e8-11e2-b730-e83935455ca0','67547395-c2e8-11e2-b730-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,2,'leonard nyirenda test group sms',31,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'1','2013-05-22 17:03:41','2013-05-22 17:03:41','2013-05-22 17:03:41',NULL,0,NULL,0,'2013-05-22 17:03:40','2013-05-22 17:03:41'),('86362268-c2e7-11e2-9983-e83935455ca0','8635fab1-c2e7-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,1,'hey',3,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'','2013-05-22 16:57:43','2013-05-22 16:57:43','2013-05-22 16:57:43',NULL,0,'',0,'2013-05-22 16:57:22','2013-05-22 16:57:43'),('c504110f-c2e5-11e2-9983-e83935455ca0','c503f192-c2e5-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,1,'mambo mbona haikubali kutuma sms',32,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'','2013-05-22 16:45:10','2013-05-22 16:45:10','2013-05-22 16:45:10',NULL,0,'',0,'2013-05-22 16:44:48','2013-05-22 16:45:10'),('2053a3ab-c2e3-11e2-9983-e83935455ca0','204e0866-c2e3-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,1,'mambo testing final pspf quick',30,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'','2013-05-22 16:26:15','2013-05-22 16:26:15','2013-05-22 16:26:15',NULL,0,'',0,'2013-05-22 16:25:53','2013-05-22 16:26:15'),('46941b1a-c2e4-11e2-9983-e83935455ca0','464a48f5-c2e4-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,1,'nyirenda mambo wewe now tunajaribu group sms',44,1,NULL,'PSPF',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'757447737',NULL,0,0,0,NULL,NULL,NULL,NULL,'','2013-05-22 16:34:29','2013-05-22 16:34:29','2013-05-22 16:34:29',NULL,0,NULL,0,'2013-05-22 16:34:06','2013-05-22 16:34:29'),('bd1578e8-433f-11e5-8a98-58946b172904','bd155b88-433f-11e5-8a98-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,0,'sdnfdskl',8,1,NULL,'testing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'682148470',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,'2015-08-15 14:21:13','0000-00-00 00:00:00'),('2ebf12c9-e116-11e5-8229-58946b172904','2ebeecd6-e116-11e5-8229-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,0,'ABC File',8,1,NULL,'testing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'682148470',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,'2016-03-03 11:01:49','0000-00-00 00:00:00'),('b2990ed0-e116-11e5-8229-58946b172904','b298e5ab-e116-11e5-8229-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e',NULL,NULL,NULL,0,'abc quick',9,1,NULL,'testing',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'654962605',NULL,0,0,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'',0,'2016-03-03 11:05:30','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `inbound_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `languages` (
  `lang` varchar(5) NOT NULL,
  `title` varchar(128) NOT NULL,
  PRIMARY KEY (`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES ('en_us','English'),('sw_tz','Swahili');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `country_code` varchar(4) NOT NULL DEFAULT '255',
  `location` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locations`
--

LOCK TABLES `locations` WRITE;
/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` VALUES ('600d6176-bf42-11df-8f82-000c29b8e356','255','Arusha','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600d7670-bf42-11df-8f82-000c29b8e356','255','Bagamoyo','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600d832c-bf42-11df-8f82-000c29b8e356','255','Bukoba','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600d90ba-bf42-11df-8f82-000c29b8e356','255','Chalinze','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600d9e16-bf42-11df-8f82-000c29b8e356','255','Dar - Buguruni','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600daec4-bf42-11df-8f82-000c29b8e356','255','Dar - Chang\'ombe','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600dbbda-bf42-11df-8f82-000c29b8e356','255','Dar - Ilala','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600dcb8e-bf42-11df-8f82-000c29b8e356','255','Dar - Kariakoo','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600dd912-bf42-11df-8f82-000c29b8e356','255','Dar - Kawe','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600de736-bf42-11df-8f82-000c29b8e356','255','Dar - Kigamboni','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600df398-bf42-11df-8f82-000c29b8e356','255','Dar - Kijitonyama','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e0112-bf42-11df-8f82-000c29b8e356','255','Dar - Kunduchi','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e0f54-bf42-11df-8f82-000c29b8e356','255','Dar - Magomeni','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e1bc0-bf42-11df-8f82-000c29b8e356','255','Dar - Manzese','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e2a7a-bf42-11df-8f82-000c29b8e356','255','Dar - Masaki','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e37ea-bf42-11df-8f82-000c29b8e356','255','Dar - Mbagala','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e4456-bf42-11df-8f82-000c29b8e356','255','Dar - Mbezi','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e5270-bf42-11df-8f82-000c29b8e356','255','Dar - Mikocheni','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e6062-bf42-11df-8f82-000c29b8e356','255','Dar - Mwenge','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e6cd8-bf42-11df-8f82-000c29b8e356','255','Dar - Other','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e7bb0-bf42-11df-8f82-000c29b8e356','255','Dar - Tabata','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e88f8-bf42-11df-8f82-000c29b8e356','255','Dar - Tandika','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600e96c2-bf42-11df-8f82-000c29b8e356','255','Dar - Town','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ea338-bf42-11df-8f82-000c29b8e356','255','Dar - Ubungo','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600eb09e-bf42-11df-8f82-000c29b8e356','255','Dar - Upanga','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ebecc-bf42-11df-8f82-000c29b8e356','255','Dodoma','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ecc00-bf42-11df-8f82-000c29b8e356','255','Iringa','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ed970-bf42-11df-8f82-000c29b8e356','255','Kibaha','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ee8ca-bf42-11df-8f82-000c29b8e356','255','Kigoma','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600ef5e0-bf42-11df-8f82-000c29b8e356','255','Lindi','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f068e-bf42-11df-8f82-000c29b8e356','255','Lushoto','2010-09-13 19:52:44','2010-10-07 09:47:34'),('600f148a-bf42-11df-8f82-000c29b8e356','255','Mbeya','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f222c-bf42-11df-8f82-000c29b8e356','255','Morogoro','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f2e98-bf42-11df-8f82-000c29b8e356','255','Moshi','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f3d8e-bf42-11df-8f82-000c29b8e356','255','Mtwara','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f4ad6-bf42-11df-8f82-000c29b8e356','255','Musoma','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f571a-bf42-11df-8f82-000c29b8e356','255','Mwanza','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f6548-bf42-11df-8f82-000c29b8e356','255','Shinyanga','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f754c-bf42-11df-8f82-000c29b8e356','255','Singida','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f837a-bf42-11df-8f82-000c29b8e356','255','Songea','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f8f96-bf42-11df-8f82-000c29b8e356','255','Sumbawanga','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600f9cde-bf42-11df-8f82-000c29b8e356','255','Tabora','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600faa08-bf42-11df-8f82-000c29b8e356','255','Tanga','2010-09-13 19:52:44','0000-00-00 00:00:00'),('600fb43a-bf42-11df-8f82-000c29b8e356','255','Zanzibar','2010-09-13 19:52:44','0000-00-00 00:00:00'),('a9bdfa1e-1c43-11e2-b9aa-0013725528cc','254','kisumu','2012-10-22 15:26:11','0000-00-00 00:00:00'),('0a0c0f00-1c44-11e2-b9aa-0013725528cc','256','Kampala','2012-10-22 15:28:52','0000-00-00 00:00:00'),('109d43fc-1c44-11e2-b9aa-0013725528cc','256','Jinja','2012-10-22 15:29:03','2012-10-22 15:40:06'),('67931c5c-1c46-11e2-b9aa-0013725528cc','254','Nairobi','2012-10-22 15:45:48','0000-00-00 00:00:00'),('766504e8-1c46-11e2-b9aa-0013725528cc','254','Mombasa','2012-10-22 15:46:13','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_admins`
--

DROP TABLE IF EXISTS `mst_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_admins` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `username` varchar(200) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(20) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(200) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_admins`
--

LOCK TABLES `mst_admins` WRITE;
/*!40000 ALTER TABLE `mst_admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `mst_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mst_vendors`
--

DROP TABLE IF EXISTS `mst_vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mst_vendors` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `parent_ven_id` varchar(36) NOT NULL,
  `username` varchar(200) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `vendor_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(20) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(25) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(200) CHARACTER SET latin1 NOT NULL,
  `url` varchar(50) NOT NULL,
  `city_id` char(36) CHARACTER SET latin1 NOT NULL,
  `location_id` char(36) CHARACTER SET latin1 NOT NULL,
  `people_range_id` char(36) CHARACTER SET latin1 NOT NULL,
  `postal_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `physical_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `organisation` varchar(255) CHARACTER SET latin1 NOT NULL,
  `organisation_id` char(36) CHARACTER SET latin1 NOT NULL,
  `vendor_type` int(1) NOT NULL COMMENT '1=Individual,2=Organisation',
  `reg_step` tinyint(1) NOT NULL DEFAULT '0',
  `mob_conf_msg` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mob_confirmed` tinyint(1) NOT NULL,
  `term_conditions` tinyint(1) NOT NULL,
  `callback_url` varchar(255) NOT NULL,
  `reseller` tinyint(4) NOT NULL,
  `payment_method` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `support_email` varchar(50) NOT NULL,
  `support_phone` varchar(15) NOT NULL,
  `reseller_url` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `broadcast_reminder` tinyint(1) NOT NULL DEFAULT '1',
  `broadcast_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `access_incoming` tinyint(4) NOT NULL DEFAULT '0',
  `access_registrations` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`,`people_range_id`,`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mst_vendors`
--

LOCK TABLES `mst_vendors` WRITE;
/*!40000 ALTER TABLE `mst_vendors` DISABLE KEYS */;
INSERT INTO `mst_vendors` VALUES ('1abc4c3a-84a3-11e2-bf8a-00185176683e','','admin','25d55ad283aa400af464c76d713c07ad','KICELIEXE','688121252','222120912','pspf live','pspf@pspf-tz.org','www.pspf-tz.org   ','a2d398aa-bfc3-11df-8f82-000c29b8e356','600eb09e-bf42-11df-8f82-000c29b8e356','78c963a2-bf44-11df-8f82-000c29b8e356','Po Box 4843.','PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street','PSPF','7cdf95d4-bf43-11df-8f82-000c29b8e356',2,2,'y8wbjyef',0,1,'http://www.bongolive.co.tz/api/dlr.php',1,'','templates/assets/logo/1abc4c3a-84a3-11e2-bf8a-0018','','','',1,0,1,1,0,0,0,'2010-10-21 08:14:10','2013-05-13 17:25:05');
/*!40000 ALTER TABLE `mst_vendors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organisations`
--

DROP TABLE IF EXISTS `organisations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `organisations` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `organisation` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organisations`
--

LOCK TABLES `organisations` WRITE;
/*!40000 ALTER TABLE `organisations` DISABLE KEYS */;
INSERT INTO `organisations` VALUES ('7cdf95d4-bf43-11df-8f82-000c29b8e356','Accounting, Auditing & Taxation','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7cdfb0a0-bf43-11df-8f82-000c29b8e356','Advertising, PR & Marketing Agencies & Services ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7cdfcb3a-bf43-11df-8f82-000c29b8e356','Agriculture','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7cdfe188-bf43-11df-8f82-000c29b8e356','Air, Road and Marine Transport','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7cdff542-bf43-11df-8f82-000c29b8e356','Airlines','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce00a32-bf43-11df-8f82-000c29b8e356','Architecture, Construction, Building & Contracting','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce01e1e-bf43-11df-8f82-000c29b8e356','Barbers, Hair & Beauty Salons','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce030ac-bf43-11df-8f82-000c29b8e356','Bars & Nightclubs','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce043b2-bf43-11df-8f82-000c29b8e356','Beauty & Fragrance','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce058a2-bf43-11df-8f82-000c29b8e356','Bookshops','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce06a9a-bf43-11df-8f82-000c29b8e356','Brewers, Distillers & Alcoholic Beverages','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce07e36-bf43-11df-8f82-000c29b8e356','Business & Trade Support Organisations ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce090c4-bf43-11df-8f82-000c29b8e356','Cinema & Theatres','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce0a406-bf43-11df-8f82-000c29b8e356','Clothing Boutiques','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce0b7f2-bf43-11df-8f82-000c29b8e356','Computer Hardware & Maintenance Services','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce0c9c2-bf43-11df-8f82-000c29b8e356','Consulting','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce0dd36-bf43-11df-8f82-000c29b8e356','Courier Services','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce0ef4c-bf43-11df-8f82-000c29b8e356','Curios, Arts & Crafts','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce100c2-bf43-11df-8f82-000c29b8e356','Electrical Services & Repairs','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce11242-bf43-11df-8f82-000c29b8e356','Electronics  & Home Appliances','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1237c-bf43-11df-8f82-000c29b8e356','Employment & Recruitment Agencies ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce136fa-bf43-11df-8f82-000c29b8e356','Engineering','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1497e-bf43-11df-8f82-000c29b8e356','Event Planning','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce15d2e-bf43-11df-8f82-000c29b8e356','Fabricated Metal Products & Equipment','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce16e36-bf43-11df-8f82-000c29b8e356','Film, Sound & Video Productions','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1810a-bf43-11df-8f82-000c29b8e356','Finance, Banking & Investing','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce19528-bf43-11df-8f82-000c29b8e356','Fish & Seafood Products & Processors','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1a6da-bf43-11df-8f82-000c29b8e356','Fitness Centres','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1b814-bf43-11df-8f82-000c29b8e356','Florists & Flower Decorators','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1c9b2-bf43-11df-8f82-000c29b8e356','Food Manufacturing, Processing & Wholesaling','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1dc90-bf43-11df-8f82-000c29b8e356','Furniture Manufacturers & Dealers ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce1f090-bf43-11df-8f82-000c29b8e356','Gambling, Casino\'s & Slot Machine Centres','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce202d8-bf43-11df-8f82-000c29b8e356','Garments & Accessories','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce21610-bf43-11df-8f82-000c29b8e356','Gifts, Cards & Toys','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2286c-bf43-11df-8f82-000c29b8e356','Glass & Window','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce23c58-bf43-11df-8f82-000c29b8e356','Government Entity','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce24f5e-bf43-11df-8f82-000c29b8e356','Hardware Retail','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce262be-bf43-11df-8f82-000c29b8e356','Hardware Wholesale & Manufacturing','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce274ca-bf43-11df-8f82-000c29b8e356','Health food products & nutritional supplements ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce28690-bf43-11df-8f82-000c29b8e356','Hospital,Medical & Health','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce29a54-bf43-11df-8f82-000c29b8e356','Hotels, Motels, Guest Houses, Lodges & Campsites','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2adfa-bf43-11df-8f82-000c29b8e356','Importers & Exporters','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2c3c6-bf43-11df-8f82-000c29b8e356','Insurance Companies, Brokers & Agents','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2d5aa-bf43-11df-8f82-000c29b8e356','Interior Decoration & Design','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2e98c-bf43-11df-8f82-000c29b8e356','Internet Products & Services Providers','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce2fb70-bf43-11df-8f82-000c29b8e356','Interpreters & Translators ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce30d36-bf43-11df-8f82-000c29b8e356','Jewellers & Gems Dealers','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce31f6a-bf43-11df-8f82-000c29b8e356','Laundry & Dry Cleaning','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce33130-bf43-11df-8f82-000c29b8e356','Legal Services','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce345d0-bf43-11df-8f82-000c29b8e356','Machinery & Industrial Equipment ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce3580e-bf43-11df-8f82-000c29b8e356','Magazines, Newspapers & Periodicals Publishers & Distributors','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce36cfe-bf43-11df-8f82-000c29b8e356','Marine','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce37e88-bf43-11df-8f82-000c29b8e356','Mining & Quarrying','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce390ee-bf43-11df-8f82-000c29b8e356','Motor Vehicle Sales, Parts & Repairs','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce3a34a-bf43-11df-8f82-000c29b8e356','Music & Musical Instruments Sales & Services ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('7ce3c514-bf43-11df-8f82-000c29b8e356','Musicians ','2010-09-13 20:00:42','0000-00-00 00:00:00'),('9eeda6de-bf43-11df-8f82-000c29b8e356','Non Governmental Organisations (NGO\'S)','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eedbce6-bf43-11df-8f82-000c29b8e356','Opticians & Optical Goods','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eedccfe-bf43-11df-8f82-000c29b8e356','Photography & Photo Processing','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeddf46-bf43-11df-8f82-000c29b8e356','Plumbing Contractors & Suppliers','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eedf256-bf43-11df-8f82-000c29b8e356','Printing & Design','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee062e-bf43-11df-8f82-000c29b8e356','Real Estate, Housing & Apartments','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee16aa-bf43-11df-8f82-000c29b8e356','Religious Organization','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee2910-bf43-11df-8f82-000c29b8e356','Restaurants, Cafes & Catering','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee3a68-bf43-11df-8f82-000c29b8e356','Schools, Colleges  & Universities','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee4e18-bf43-11df-8f82-000c29b8e356','Security Services & Systems','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee6056-bf43-11df-8f82-000c29b8e356','Sports Good & Games Supplies','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee71f4-bf43-11df-8f82-000c29b8e356','Stationery, Office Equipment & Supplies ','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee823e-bf43-11df-8f82-000c29b8e356','Supermarkets & Groceries','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eee962a-bf43-11df-8f82-000c29b8e356','Technology Development & Consultants','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeea7fa-bf43-11df-8f82-000c29b8e356','Telecommunications Equipment & Services','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeeb89e-bf43-11df-8f82-000c29b8e356','Transport & Logistics Management Services','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeecb2c-bf43-11df-8f82-000c29b8e356','Travel & Tours ','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeeddce-bf43-11df-8f82-000c29b8e356','TV, Radio Broadcasting & Stations','2010-09-13 20:01:39','0000-00-00 00:00:00'),('9eeeed96-bf43-11df-8f82-000c29b8e356','Wedding Organisers & Planners','2010-09-13 20:01:39','2010-10-07 09:45:23'),('9eeefcc8-bf43-11df-8f82-000c29b8e356','Other','2010-09-13 20:01:39','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `organisations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_messages`
--

DROP TABLE IF EXISTS `outbound_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sms_in_id` int(11) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `message` varchar(480) NOT NULL,
  `sms_status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_messages`
--

LOCK TABLES `outbound_messages` WRITE;
/*!40000 ALTER TABLE `outbound_messages` DISABLE KEYS */;
INSERT INTO `outbound_messages` VALUES (1,185072,'255757447737','Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185072','response sent'),(2,185073,'255757447737','Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','response sent'),(3,185074,'255757447737','Historia ya mchango wa\nAthumani Kilama\n\n2006-06-30\nKiasi 26460\n\n2006-05-25\nKiasi 26460\n\n2006-04-30\nKiasi 26460\n\nJumla ya mchango wako wote ni 1474725 Kupata michango ya miezi yote tembelea www.pspf-tz.org','response sent'),(4,185075,'255757447737','Malipo ya Pensheni ya Ndugu\nAthumani Kilama\n\n2013-04-01\nA/c 2201602128\nKiasi 674730\n\n2013-01-01\nA/c 2201602128\nKiasi 674730\n\n2012-10-01\nA/c 2201602128\nKiasi 674730\n\n Jumla ya malipo yote yaliyofanywa ni 19342260 Kupata malipo ya miezi yote tembelea www.pspf-tz.org','response sent'),(5,185076,'255757447737','Benefit Paid. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neon pension ikifuatiwa na checkno yako kwenda namba XXXXXX','response sent'),(6,185077,'255765121252','1. Hakiki Usajili \n2. Historia ya Mchango \n3. Pensioner monthly Statement \n4. Benefit Status \n5. Sajili.','response sent'),(7,185078,'255756487306','Tumeshindwa kuutambua ujumbe uliotuma. Tafadhali Tuma ujumbe ulio sahihi.','response sent'),(8,185079,'255784467911','1. Hakiki Usajili \n2. Historia ya Mchango \n3. Pensioner monthly Statement \n4. Benefit Status \n5. Sajili.','response sent'),(9,185080,'255784467911','Historia ya mchango wa\nWilliam Mbela\n\n2005-01-31\nKiasi 9017.5\n\n2004-12-31\nKiasi 9017.5\n\n2004-11-30\nKiasi 9018\n\nJumla ya mchango wako wote ni 437710.5 Kupata michango ya miezi yote tembelea www.pspf-tz.org','response sent'),(10,185081,'255784467911','Hakuna malipo ya pension yaliyofanyika kwa  checkno 4378804','response sent'),(11,185082,'255784467911','Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185082','response sent'),(12,185083,'255784467911','Benefit Paid. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neno pension ikifuatiwa na checkno yako kwenda namba XXXXXX','response sent'),(13,185085,'255757447737','Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','response sent'),(14,185084,'255757447737','Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185084','response sent'),(15,185086,'255757447737','Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','response sent'),(16,185087,'255757447737','Umesajiliwa na PSPF. Namba yako ya uanachama ya muda  ni PSPF-0185087','response sent');
/*!40000 ALTER TABLE `outbound_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `people_ranges`
--

DROP TABLE IF EXISTS `people_ranges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `people_ranges` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `people_range` varchar(100) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `people_ranges`
--

LOCK TABLES `people_ranges` WRITE;
/*!40000 ALTER TABLE `people_ranges` DISABLE KEYS */;
INSERT INTO `people_ranges` VALUES ('78c963a2-bf44-11df-8f82-000c29b8e356','1-9','2010-09-13 20:08:43','2010-10-18 07:40:39'),('78c96910-bf44-11df-8f82-000c29b8e356','10-19','2010-09-13 20:08:53','0000-00-00 00:00:00'),('78c969a6-bf44-11df-8f82-000c29b8e356','20-49','2010-09-13 20:09:02','0000-00-00 00:00:00'),('78c969f6-bf44-11df-8f82-000c29b8e356','50-99','2010-09-13 20:09:16','0000-00-00 00:00:00'),('78c96a46-bf44-11df-8f82-000c29b8e356','100-499','2010-09-13 20:09:27','0000-00-00 00:00:00'),('78c96aa0-bf44-11df-8f82-000c29b8e356','500+','2010-09-13 20:09:38','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `people_ranges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prim_keywords`
--

DROP TABLE IF EXISTS `prim_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prim_keywords` (
  `id_prim_keywords` int(11) NOT NULL AUTO_INCREMENT,
  `text_prim_keywords` varchar(50) NOT NULL,
  `mst_vendors_id` char(36) NOT NULL,
  `sender_id` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_prim_keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prim_keywords`
--

LOCK TABLES `prim_keywords` WRITE;
/*!40000 ALTER TABLE `prim_keywords` DISABLE KEYS */;
INSERT INTO `prim_keywords` VALUES (36,'sajili','1abc4c3a-84a3-11e2-bf8a-00185176683e','0'),(37,'menu','1abc4c3a-84a3-11e2-bf8a-00185176683e','0'),(35,'benefit','1abc4c3a-84a3-11e2-bf8a-00185176683e','0'),(34,'pension','1abc4c3a-84a3-11e2-bf8a-00185176683e','0'),(33,'mchango','1abc4c3a-84a3-11e2-bf8a-00185176683e','0'),(32,'hakiki','1abc4c3a-84a3-11e2-bf8a-00185176683e','0');
/*!40000 ALTER TABLE `prim_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processes`
--

DROP TABLE IF EXISTS `processes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processes` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `check` text CHARACTER SET latin1 NOT NULL,
  `start` text CHARACTER SET latin1 NOT NULL,
  `stop` text CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-stop,1-start,2-restart,3-shutdown,works only for sender.php',
  `run_as` enum('tjiwaji') CHARACTER SET latin1 NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processes`
--

LOCK TABLES `processes` WRITE;
/*!40000 ALTER TABLE `processes` DISABLE KEYS */;
INSERT INTO `processes` VALUES ('9de55dbc-dce0-11df-a355-000c29b8e356','sender','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php 1>/dev/null 2>/dev/null','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php',1,'tjiwaji','2013-02-05 15:55:47'),('a9295ade-dce0-11df-a355-000c29b8e356','push2Q','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php 1>/dev/null 2>/dev/null','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php',1,'tjiwaji','2013-02-05 15:55:47'),('a9295ade-dee0-12df-e255-000c29b8b897','credit_reminder','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php 1>/dev/null 2>/dev/null','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php',1,'tjiwaji','2013-02-05 15:55:47'),('a9295ade-dee0-13be-e255-000b28b8b145','schedule_sms_store_push','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php 1>/dev/null 2>/dev/null','c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php',1,'tjiwaji','2013-02-05 15:55:47'),('a9295ade-dee0-14be-e255-000c28b8b145','sms_reminders','c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php','c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php 1>/dev/null 2>/dev/null','c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php',1,'tjiwaji','2013-02-05 11:48:48');
/*!40000 ALTER TABLE `processes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reminders`
--

DROP TABLE IF EXISTS `reminders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reminders` (
  `id` varchar(36) NOT NULL,
  `sms_store_id` varchar(36) NOT NULL,
  `contact_id` varchar(36) NOT NULL,
  `reminder_type` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `repeat` varchar(10) DEFAULT NULL,
  `repeat_every` smallint(6) DEFAULT NULL,
  `repeat_days` varchar(100) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `send_date` datetime NOT NULL,
  `is_processed` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reminders`
--

LOCK TABLES `reminders` WRITE;
/*!40000 ALTER TABLE `reminders` DISABLE KEYS */;
/*!40000 ALTER TABLE `reminders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `second_keywords`
--

DROP TABLE IF EXISTS `second_keywords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `second_keywords` (
  `id_second_keywords` int(11) NOT NULL AUTO_INCREMENT,
  `text_second_keywords` varchar(50) NOT NULL,
  `prim_keywords_id` int(11) NOT NULL,
  PRIMARY KEY (`id_second_keywords`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `second_keywords`
--

LOCK TABLES `second_keywords` WRITE;
/*!40000 ALTER TABLE `second_keywords` DISABLE KEYS */;
INSERT INTO `second_keywords` VALUES (30,'5',36),(29,'4',35),(28,'3',34),(27,'contribution',33),(26,'2',33),(25,'1',32),(24,'status',32),(23,'regista',32),(31,'jisajili',36),(32,'help',37),(33,'msaada',37),(34,'pspf',37);
/*!40000 ALTER TABLE `second_keywords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `senderids`
--

DROP TABLE IF EXISTS `senderids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `senderids` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `senderid` varchar(25) CHARACTER SET latin1 NOT NULL,
  `vendor_id` char(36) CHARACTER SET latin1 NOT NULL,
  `status` enum('active','inactive','pending') CHARACTER SET latin1 NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `senderids`
--

LOCK TABLES `senderids` WRITE;
/*!40000 ALTER TABLE `senderids` DISABLE KEYS */;
INSERT INTO `senderids` VALUES ('70ba2cda-a8c1-11e2-a0b9-e83935455ca0','testing','1abc4c3a-84a3-11e2-bf8a-00185176683e','active',0,'2013-04-19 10:19:14','0000-00-00 00:00:00'),('616df1d8-a827-11e2-bc65-e83935455ca0','PSPF','1abc4c3a-84a3-11e2-bf8a-00185176683e','active',0,'2013-04-18 15:56:26','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `senderids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_stores`
--

DROP TABLE IF EXISTS `sms_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_stores` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `sent_by` char(36) CHARACTER SET latin1 NOT NULL COMMENT 'ADMIN/VENDOR',
  `sender_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit` int(12) NOT NULL,
  `addressbook_id` text CHARACTER SET latin1 NOT NULL,
  `contact_id` longtext CHARACTER SET latin1 NOT NULL,
  `receiver_id` longtext CHARACTER SET latin1 NOT NULL,
  `receiver_type` varchar(10) CHARACTER SET latin1 NOT NULL,
  `sender_type` varchar(10) CHARACTER SET latin1 NOT NULL,
  `advertiser_id` char(36) CHARACTER SET latin1 NOT NULL COMMENT 'This is  actually mst_vendors.id',
  `text_message` text CHARACTER SET latin1 NOT NULL,
  `message_length` int(11) NOT NULL,
  `message_count` int(11) NOT NULL,
  `is_shceduled` tinyint(1) NOT NULL,
  `send_time` datetime NOT NULL,
  `sent_time` datetime NOT NULL,
  `is_proccessed` int(1) NOT NULL,
  `canceled` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `job_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_stores`
--

LOCK TABLES `sms_stores` WRITE;
/*!40000 ALTER TABLE `sms_stores` DISABLE KEYS */;
INSERT INTO `sms_stores` VALUES ('ef3e203c-c2e8-11e2-b730-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','tet quick sms',13,1,0,'2013-05-22 17:07:27','2013-05-22 17:07:30',2,0,'2013-05-22 17:07:27','2013-05-22 17:07:27',''),('8635fab1-c2e7-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','hey',3,1,0,'2013-05-22 16:57:22','2013-05-22 16:57:43',2,0,'2013-05-22 16:57:22','2013-05-22 16:57:22',''),('67547395-c2e8-11e2-b730-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'a1b4f2d9-b944-11e2-8106-e83935455ca0','','','','VEN','','{first_name} {last_name} test group sms',39,1,0,'2013-05-22 17:03:39','2013-05-22 17:03:41',2,0,'2013-05-22 17:03:39','2013-05-22 17:03:40',''),('c503f192-c2e5-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','mambo mbona haikubali kutuma sms',32,1,0,'2013-05-22 16:44:48','2013-05-22 16:45:10',2,0,'2013-05-22 16:44:48','2013-05-22 16:44:48',''),('464a48f5-c2e4-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'a1b4f2d9-b944-11e2-8106-e83935455ca0','','','','VEN','','{last_name} mambo wewe now tunajaribu group sms',47,1,0,'2013-05-22 16:34:06','2013-05-22 16:40:51',2,0,'2013-05-22 16:34:06','2013-05-22 16:34:06',''),('204e0866-c2e3-11e2-9983-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','mambo testing final pspf quick',30,1,0,'2013-05-22 16:25:53','2013-05-22 16:40:51',2,0,'2013-05-22 16:25:53','2013-05-22 16:25:53',''),('1ccdf102-c218-11e2-a8bd-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'','70c6728e-872e-11e2-b563-001f3c930a21','','','VEN','','{last_name} mambo {first_name} hii group sms',44,1,0,'2013-05-21 16:12:44','2013-05-21 16:13:08',2,0,'2013-05-21 16:12:44','2013-05-21 16:12:46',''),('2f5cb1bf-b945-11e2-8106-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','testing pspf quick sms',22,1,0,'2013-05-10 10:42:37','2013-05-10 10:42:39',2,0,'2013-05-10 10:42:37','2013-05-10 10:42:37',''),('0bed3128-b94d-11e2-8974-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'a8a11809-b94a-11e2-8974-e83935455ca0','','','','VEN','','dear {first_name} tunajaribu',28,1,0,'2013-05-10 11:38:53','2013-05-22 16:37:50',2,0,'2013-05-10 11:38:53','2013-05-10 11:38:54',''),('53730735-b943-11e2-a5ab-e83935455ca0','1abc4c3a-84a3-11e2-bf8a-00185176683e','616df1d8-a827-11e2-bc65-e83935455ca0',1,'QUICK','QUICK','','','VEN','','mambo',5,1,0,'2013-05-10 10:29:22','2013-05-22 16:37:50',2,0,'2013-05-10 10:29:22','2013-05-10 10:29:22',''),('bd155b88-433f-11e5-8a98-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e','70ba2cda-a8c1-11e2-a0b9-e83935455ca0',1,'QUICK','QUICK','','','VEN','','sdnfdskl',8,1,0,'2015-08-15 14:21:13','0000-00-00 00:00:00',0,0,'2015-08-15 14:21:13','0000-00-00 00:00:00',''),('2ebeecd6-e116-11e5-8229-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e','70ba2cda-a8c1-11e2-a0b9-e83935455ca0',1,'FILE','FILE','','','VEN','','ABC File',8,1,0,'2016-03-03 11:01:49','0000-00-00 00:00:00',0,0,'2016-03-03 11:01:49','0000-00-00 00:00:00',''),('b298e5ab-e116-11e5-8229-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e','70ba2cda-a8c1-11e2-a0b9-e83935455ca0',1,'QUICK','QUICK','','','VEN','','abc quick',9,1,0,'2016-03-03 11:05:30','0000-00-00 00:00:00',0,0,'2016-03-03 11:05:30','0000-00-00 00:00:00',''),('c9a7e9d6-e116-11e5-8229-58946b172904','1abc4c3a-84a3-11e2-bf8a-00185176683e','70ba2cda-a8c1-11e2-a0b9-e83935455ca0',3,'b78b9726-1a85-11e2-961b-001f3c930a21','','','','VEN','','abc group',9,1,0,'2016-03-03 11:06:09','0000-00-00 00:00:00',0,0,'2016-03-03 11:06:09','0000-00-00 00:00:00','');
/*!40000 ALTER TABLE `sms_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_template`
--

DROP TABLE IF EXISTS `sms_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_template` (
  `template_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendor_id` varchar(36) NOT NULL,
  `sms_title` varchar(50) NOT NULL,
  `message` varchar(400) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_template`
--

LOCK TABLES `sms_template` WRITE;
/*!40000 ALTER TABLE `sms_template` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `text_in_log`
--

DROP TABLE IF EXISTS `text_in_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `text_in_log` (
  `sms_in_id` int(15) NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) NOT NULL,
  `phone_receive` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `received` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `num_status` smallint(6) NOT NULL DEFAULT '0',
  `outbound_response` text,
  `sent_date` datetime NOT NULL,
  `ven_id` varchar(100) DEFAULT NULL,
  `keyword` varchar(50) NOT NULL,
  `system_error` char(1) NOT NULL DEFAULT '0',
  `system_error_message` varchar(100) NOT NULL,
  `checkno` varchar(20) NOT NULL,
  PRIMARY KEY (`sms_in_id`)
) ENGINE=MyISAM AUTO_INCREMENT=185088 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `text_in_log`
--

LOCK TABLES `text_in_log` WRITE;
/*!40000 ALTER TABLE `text_in_log` DISABLE KEYS */;
INSERT INTO `text_in_log` VALUES (185087,'255757447737','','5 leonard richard nyirenda wa mtwara s.l.p 511 namba ya simu 0757447737','2013-05-22 05:12:24','response sent',1,'Umesajiliwa na PSPF. Namba yako ya uanachama ya muda  ni PSPF-0185087','2013-05-22 17:12:27','1abc4c3a-84a3-11e2-bf8a-00185176683e','sajili','0','',''),(185086,'255757447737','','1 1000030','2013-05-21 04:37:14','response sent',1,'Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','2013-05-21 16:37:17','1abc4c3a-84a3-11e2-bf8a-00185176683e','hakiki','0','','1000030'),(185085,'255757447737','','1 1000030','2013-05-21 04:23:15','response sent',1,'Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','2013-05-21 16:36:19','1abc4c3a-84a3-11e2-bf8a-00185176683e','hakiki','0','','1000030'),(185084,'255757447737','','5 leonard richard nyirenda wa mtwara s.l.p 511 namba ya simu 0757447737','2013-05-21 04:14:30','response sent',1,'Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185084','2013-05-21 16:36:20','1abc4c3a-84a3-11e2-bf8a-00185176683e','sajili','0','',''),(185082,'255784467911','255688121252','5 manfred john buchurago','2013-05-10 12:17:18','response sent',1,'Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185082','2013-05-10 12:17:22','1abc4c3a-84a3-11e2-bf8a-00185176683e','sajili','0','',''),(185083,'255784467911','255688121252','4  4378804','2013-05-10 12:23:24','response sent',1,'Benefit Paid. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neno pension ikifuatiwa na checkno yako kwenda namba XXXXXX','2013-05-10 12:23:25','1abc4c3a-84a3-11e2-bf8a-00185176683e','benefit','0','','4378804'),(185081,'255784467911','255688121252','3  4378804','2013-05-10 12:09:07','response sent',1,'Hakuna malipo ya pension yaliyofanyika kwa  checkno 4378804','2013-05-10 12:09:10','1abc4c3a-84a3-11e2-bf8a-00185176683e','pension','0','',''),(185076,'255757447737','','4 1000030','2013-05-10 10:50:58','response sent',1,'Benefit Paid. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neon pension ikifuatiwa na checkno yako kwenda namba XXXXXX','2013-05-10 10:51:00','1abc4c3a-84a3-11e2-bf8a-00185176683e','benefit','0','','1000030'),(185077,'255765121252','255688121252','pspf','2013-05-10 11:05:44','response sent',1,'1. Hakiki Usajili \n2. Historia ya Mchango \n3. Pensioner monthly Statement \n4. Benefit Status \n5. Sajili.','2013-05-10 11:05:45','1abc4c3a-84a3-11e2-bf8a-00185176683e','menu','0','',''),(185078,'255756487306','255688121252','hi taha, have you signed up for the global forum? if not can you do so quickly and forward me the confirmation','2013-05-10 11:16:34','response sent',1,'Tumeshindwa kuutambua ujumbe uliotuma. Tafadhali Tuma ujumbe ulio sahihi.','2013-05-10 11:16:35','1abc4c3a-84a3-11e2-bf8a-00185176683e','hi','0','',''),(185079,'255784467911','255688121252','pspf','2013-05-10 11:44:30','response sent',1,'1. Hakiki Usajili \n2. Historia ya Mchango \n3. Pensioner monthly Statement \n4. Benefit Status \n5. Sajili.','2013-05-10 11:44:31','1abc4c3a-84a3-11e2-bf8a-00185176683e','menu','0','',''),(185080,'255784467911','255688121252','2  4378804','2013-05-10 11:47:24','response sent',1,'Historia ya mchango wa\nWilliam Mbela\n\n2005-01-31\nKiasi 9017.5\n\n2004-12-31\nKiasi 9017.5\n\n2004-11-30\nKiasi 9018\n\nJumla ya mchango wako wote ni 437710.5 Kupata michango ya miezi yote tembelea www.pspf-tz.org','2013-05-10 11:47:25','1abc4c3a-84a3-11e2-bf8a-00185176683e','mchango','0','','4378804'),(185075,'255757447737','','3 1000030','2013-05-10 10:49:06','response sent',1,'Malipo ya Pensheni ya Ndugu\nAthumani Kilama\n\n2013-04-01\nA/c 2201602128\nKiasi 674730\n\n2013-01-01\nA/c 2201602128\nKiasi 674730\n\n2012-10-01\nA/c 2201602128\nKiasi 674730\n\n Jumla ya malipo yote yaliyofanywa ni 19342260 Kupata malipo ya miezi yote tembelea www.pspf-tz.org','2013-05-10 10:49:07','1abc4c3a-84a3-11e2-bf8a-00185176683e','pension','0','','1000030'),(185073,'255757447737','','1 1000030','2013-05-10 10:47:15','response sent',1,'Ndugu Athumani Kilama Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni 1968-01-15 Checkno 1000030','2013-05-10 10:47:17','1abc4c3a-84a3-11e2-bf8a-00185176683e','hakiki','0','','1000030'),(185074,'255757447737','','2 1000030','2013-05-10 10:48:12','response sent',1,'Historia ya mchango wa\nAthumani Kilama\n\n2006-06-30\nKiasi 26460\n\n2006-05-25\nKiasi 26460\n\n2006-04-30\nKiasi 26460\n\nJumla ya mchango wako wote ni 1474725 Kupata michango ya miezi yote tembelea www.pspf-tz.org','2013-05-10 10:48:17','1abc4c3a-84a3-11e2-bf8a-00185176683e','mchango','0','','1000030'),(185072,'255757447737','','5 leonard richard nyirenda wa mtwara s.l.p 511 namba ya simu 0757447737','2013-05-10 10:44:49','response sent',1,'Usajili wako umefanikiwa. Namba yako ya uanachama ya muda  ni 185072','2013-05-10 10:44:51','1abc4c3a-84a3-11e2-bf8a-00185176683e','sajili','0','','');
/*!40000 ALTER TABLE `text_in_log` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-03 12:13:01
