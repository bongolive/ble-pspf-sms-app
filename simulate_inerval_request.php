<?php
/****************************************************************************************************************
*	File : simulate_interval_request.php
*	Purpose: Simulate sms request for testing purpose
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 
	$text=urlencode("5 leonard richard nyirenda wa mtwara s.l.p 511 namba ya simu 0757447737");
	//$sender="255688121252";
	$sender="255757447737";
	define ("URL_API_DOMAIN", "http://localhost/pspf/");
	$posturl = URL_API_DOMAIN."rcv_incoming.php?sender=$sender&text=$text";
    $ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $posturl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_exec($ch);
	echo "Request Sent successfuly";
?>