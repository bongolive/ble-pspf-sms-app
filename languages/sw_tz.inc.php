<?php

	/* Header variables_BEGIN */
	define('HEADER_WELCOME',	'Karibu');
	define('HEADER_LOGOUT',		'Logout');
	/* Header variables_END */
	
	
	/* Menu variables_BEGIN */
	define('MENU_SERVICES',							'Huduma');
	define('MENU_SERVICES_SMS_OFFERS',				'Ofa kwa SMS');
	define('MENU_SERVICES_GROUP_BULK_SMS',			'SMS za Makundi/Jumla');
	define('MENU_SERVICES_SMS_ADVERTISING',			'Kujitangaza kwa SMS');
	define('MENU_SERVICES_CUSTOM_APPLICATIONS',		'Application za Mahitaji Yako');
	define('MENU_SERVICES_ONLINE_ADVERTISING',		'Kujitangaza kwa Tovuti');
	define('MENU_SERVICES_DEVELOPERS',				'Developer API');
	define('MENU_SERVICES_PLUGINS',					'Plugins');
	/* Menu variables_END */
	
	
	/* Index_page variables_BEGIN */
	define('INDEX_SIGN_UP_INDIVIDUAL_1',	'Pokea ofa buree kupitia SMS');
	define('INDEX_SIGN_UP_INDIVIDUAL_2',	'Jiunge leo ukuwe Memba');
	define('INDEX_SIGN_UP_COMPANY_1',		'Tuma SMS kwa watu wengi');
	define('INDEX_SIGN_UP_COMPANY_2',		'Jiunga buree ukuwe Broadcaster');
	define('INDEX_SUBSCRIBER',				'Memba');
	define('INDEX_BROADCASTER',				'Broadcaster');
	define('INDEX_MOBILE_NO',				'Na. Ya Simu');
	define('INDEX_PASSWORD',				'Neno la Siri');
	define('INDEX_FORGOT_PASSWORD',			'Nimesahau Neno la Siri');
	define('INDEX_USERNAME',				'Username');
	define('INDEX_HAPPY_CUSTOMERS_1',		'Wasemavyo wateja wetu waliofurahia huduma');
	define('INDEX_HAPPY_CUSTOMERS_2',		'"Watu wameitikia SMS walizotumiwa. Nimeshuhudia mwitikio toka <br> kwa wateja wetu wa zamani ambao huenda hata walitusahau.” - Catherine Katala');
	define('INDEX_SOCIAL_MEDIA',			'Social Media');
	define('INDEX_BLOCK_DESC_1',			'Jiunge kupitia SMS  kwa kukliki tu ‘mouse’');
	define('INDEX_BLOCK_DESC_2',			'Lenga Kundi la Wateja Wako');
	define('INDEX_BLOCK_DESC_3',			'Ofa Bure Kupitia SMS');
	define('INDEX_BLOCK_DESC_4',			'‘Application’ za SMS na simu');
	
	define('BANNER_TITLE_1',				'Pata Ofa kwa Simu Yako. BURE!');
	define('BANNER_DESCRIPTION_1',			'Ungependa kupata bidhaa au huduma kwa bei bora kabisa mjini kwako? Pokea ofa kupitia SMS kulingana tu na mahitaji yako. Upeo wa matangazo yetu ni kuanzia fasheni hadi safari; fanicha hadi muziki; na vifaa vya ujenzi na mashine hadi nafasi za kazi. Hakuna ‘spam’. Ni biashara tu.');
	define('BANNER_TITLE_2',				'Meseji za Makundi');
	define('BANNER_DESCRIPTION_2',			'Hakuna tena haja ya kusoma jina moja moja baada ya jingine katika simu yako ili kutuma ujumbe kwa marafiki, familia, wateja au wasambazaji wa huduma na bidhaa. Ingiza majina  MARA MOJA TU, kisha anza kutuma.');
	define('BANNER_TITLE_3',				'Wafikie Wateja Wapya');
	define('BANNER_DESCRIPTION_3',			'Walenge wateja wako moja kwa moja kupitia kwenye simu zao za mkononi. Tuma matangazo ya SMS yaliyoandaliwa kulingana na mahitaji yako, hadi kwa wateja wale tu wanaoendana na huduma zako.');
	define('BANNER_TITLE_4',				'Zipe Utambulisho Meseji Zako');
	define('BANNER_DESCRIPTION_4',			'Je, ungependa kujenga mwonekano ulio ‘professional’? Sasa meseji zako zinaweza kuonyesha jina la biashara au shirika lako kwa kutumia jukwaa letu.');
	define('BANNER_TITLE_5',				'Forbes Africa Top 20 Tech Startup');
	define('BANNER_DESCRIPTION_5',			'Bongo Live ilichaguliwa na Forbes Africa katika kampuni bora 20 za technolojia zilizoanzishwa Africa');
	define('BANNER_TITLE_6',				'Tuvuti ya Simu ya Mkononi Mpya');
	define('BANNER_DESCRIPTION_6',			'Tuvuti yetu inajivunia mwonekano mpya na ina rahisisha kujisajili.');
	define('BANNER_TITLE_7',				'Vutia Wasomaji Wengi Zaidi');
	define('BANNER_DESCRIPTION_7',			'Wezesha Wasomaji zako za Blog kupokea meseji za taarifa moto moto dhani ya blog yako. Jaribu plugins zetu mpya za Wordpress na Blogspot bure.');
	define('BANNER_TITLE_8',				'Bongo Live Ndani ya Al-Jazeera');
	define('BANNER_DESCRIPTION_8',			'Taha Jiwaji, mwanzilishi wa kampuni ya Bongo Live, siku chache zilizopita alifanya mahojiano katika kipindi cha The Stream kwenye Televisheni ya Al-Jazeera.');
	/* Index_page variables_END */
	
	
	/* Project_vars_BEGIN */
	define('PV_TITLE_DEFAULT',				'Bongo Live - Bulk SMS Tanzania | SMS Marketing Tanzania | Mobile Marketing in Tanzania');
	define('PV_TITLE_INDEX',				'Bongo Live - Bulk SMS Tanzania | Mobile Marketing Tanzania | Online Marketing Tanzania');
	define('PV_TITLE_OFFERS',				'Bongo Live - SMS Marketing Tanzania, Bulk SMS Marketing, Bulk SMS Tanzania');	
	define('PV_TITLE_ABOUT_US',				'Bongo Live - Kuhusi Sisi');
	define('PV_TITLE_BULK_SMS',				'Bongo Live - Bulk SMS Tanzania | SMS Marketing Tanzania | Bulk SMS Marketing');
	define('PV_TITLE_ONLINE_ADVERTISING',	'Bongo Live - Google Advertising Tanzania | Facebook Advertising Tanzania | Facebook Advertising');
	define('PV_TITLE_PRIVACY',				'Bongo Live - Sera ya Usiri ');
	define('PV_TITLE_TAILORED_SERVICES',	'Bongo Live - Huduma za SMS Kadiri ya Mahitaji');
	define('PV_TITLE_WHO_CAN_USE',			'Bongo Live - Watumiaji');
	define('PV_TITLE_CONTACT_US',			'Bongo Live - Kuwasiliana');
	define('PV_TITLE_HELP',					'Bongo Live - Usaidizi');
	define('PV_TITLE_CUSTOM_APPLICATIONS',	'Bongo Live - ‘Application’ za SMS na simu kulinganan na mahitaji yako');
	define('PV_TITLE_HOW_IT_WORKS',			'Bongo Live - Utumiaji');
	define('PV_TITLE_PRICING',				'Bongo Live - Gharama');
	define('PV_TITLE_SMS_ADVERTISING',		'Bongo Live - Kujitangaza kwa SMS');
	define('PV_TITLE_ONLINE_ADVERTISING',	'Bongo Live - Kujitangaza kwa Tovuti');
	define('PV_TITLE_TERMS_OF_USE',			'Bongo Live - Masharti ya Matumizi');
	define('PV_TITLE_FAQ',					'Bongo Live - FAQ');
	
	define('PV_REGISTER_MESSAGE',			'Tunashukuru kwa kujisajili Bongo Live! Weka namba ya usajili sehemu ya verification code. Ambayo ni MESSAGE \n Bada ya hapo bonyeza alama ya "Confirm"');
	define('PV_PASSWORD_CHANGE_MESSAGE',	'Neno la siri lako ya Bongo Live! ni');
	define('PV_ACCT_ACTIVATION_MSG',		'Usajili wa Bongo Live!');
	/* Project_vars_END */
	
	
	/* Vendor variables_BEGIN */
	define('VEN_HEADER_TITLE',						'PSPF! - Broadcaster');
	define('VEN_BROADCASTER',						'Broadcaster');
	
	define('VEN_MENUTOP_DASHBOARD',					'Makundi & Contacts');
	define('VEN_MENUTOP_DASHBOARD_1',				'Shughulikia Makundi');
	define('VEN_MENUTOP_DASHBOARD_2',				'Ongeza Contact Mapya');
	define('VEN_MENUTOP_DASHBOARD_3',				'‘Import’ Contacts Mapya');
	define('VEN_MENUTOP_DASHBOARD_4',				'Address Book');
	define('VEN_MENUTOP_DASHBOARD_5',				'Contact Insights');
	define('VEN_MENUTOP_BROADCAST_SMS',				'Tuma SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_1',			'SMS kwa Jumla');
	define('VEN_MENUTOP_BROADCAST_SMS_2',			'Sender Names');
	define('VEN_MENUTOP_BROADCAST_SMS_3',			'Historia');
	define('VEN_MENUTOP_BROADCAST_SMS_4',			'Ripoti ya SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_5',			'Tuma Keyword SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_6',			'Keywords');
	define('VEN_MENUTOP_BROADCAST_SMS_7',			'Repoti Za SMS Uliopokea');
	define('VEN_MENUTOP_BROADCAST_SMS_8',			'Sampuli Za SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_9',			'Sampuli Mpya');
	define('VEN_MENUTOP_BROADCAST_SMS_10',			'SMS Reminders');
	define('VEN_MENUTOP_INCOMING',					'SMS Uliopokea');
	define('VEN_MENUTOP_INCOMING_REG',				'SMS za usajili Ulizopokea');
	define('VEN_MENUTOP_INCOMING_REG_LOG',			'Ripoti ya SMS za usajili ulizopokea');
	define('VEN_MENUTOP_PURCHASE_SMS',				'Nunua SMS');
	define('VEN_MENUTOP_PURCHASE_SMS_1',				'Nunua SMS');
	define('VEN_MENUTOP_PURCHASE_SMS_2',				'Historia');
	define('VEN_MENUTOP_REPORT',					'Repoti');
	define('VEN_MENUTOP_PROFILE',					'Profile');
	define('VEN_MENUTOP_CHANGE_PASSWORD',			'Badili Neno la Siri');
	define('VEN_MENUTOP_HELP',						'Msaada');
	
	define('VEN_DASHBOARD',							'Home');
	
	define('VEN_ADDRESSBOOK_DASHBOARD',				'Shughulikia Makundi');
	define('VEN_DASHBOARD_GROUP_NAME',				'Jina la Kundi');
	define('VEN_DASHBOARD_MANAGE_CONTACTS',			'Shughulikia Contacts');
	define('VEN_DASHBOARD_MSG_1',					"Kundi limefanikiwa kuundwa");
	define('VEN_DASHBOARD_MSG_2',					"Jina la kundi imeshatumiwa");
	define('VEN_DASHBOARD_MSG_3',					"Kundi haijatengenezwa");
	define('VEN_DASHBOARD_MSG_4',					"Sehemu ya kundi haijajazwa");
	define('VEN_DASHBOARD_MSG_5',					"Kundi limefanikiwa kufutwa");
	define('VEN_IMPORT_CONTACTS_SELECT_FILE_CSV',	'Chagua Faili (CSV, XLS au XLSX)');
	define('VEN_IMPORT_CONTACTS_DOWNLOAD_FILE_CSV',	'Download faili la mfano(CSV)');
	
	define('VEN_KEYWORDS',	'Keywords');
	define('VEN_KEYWORD_BROADCAST',	'Tuma SMS za Keywords');
	
	define('VEN_ADD_BOOK_MSG_1',					'Contacts zimefanikiwa kufutwa');
	define('VEN_ADD_BOOK_MSG_2',					'Contacts haijafutwa');
	
	define('VEN_ADD_CONTACT_MSG_1',					"Contact limeongezwa");
	define('VEN_ADD_CONTACT_MSG_2',					"Namba Maradufu");
	
	define('VEN_CHANGE_PSWD_MSG_1',					"Neno la siri yako imebadilishwa. Utapokea barua pepe ku hakikisha neno la siri umpya.");
	define('VEN_CHANGE_PSWD_MSG_2',					"Neno la siri yako imebadilishwa. Utapokea barua pepe ku hakikisha neno la siri umpya.");
	define('VEN_CHANGE_PSWD_MSG_3',					"Neno la siri haiji badilishwa");
	define('VEN_CHANGE_PSWD_MSG_4',					"Neno la siri ya zamani hailingani");
	
	define('VEN_CHANGE_MOBILE_MSG_1',				"Umetumiwa neno la siri kune simi yako. <br/> Andika neno la siri hapa.");
	define('VEN_CHANGE_MOBILE_MSG_2',				"Namba ya simu siyo sahihi");
	define('VEN_CHANGE_MOBILE_MSG_3',				"Neno la siri hailingani");
	define('VEN_CHANGE_MOBILE_MSG_4',				"Neno la siri imekubalika");
	define('VEN_CHANGE_MOBILE_MSG_5',				"Neno la siri haijasasishwa");
	
	define('VEN_CHANGE_CREDIT_MSG_1',				"Ombi la kununua SMS limetumwa");
	define('VEN_CHANGE_CREDIT_MSG_2',				"Ombi la kununua SMS haijatumwa");
	
	define('VEN_CREDIT_PURCHASE_TEXT',				"Asante kwa ombi lako la kununua sms. Tafadhali lipia kwa kutumia yoyote kati ya njia zifuatazo:<br/>Mpesa:<br/>Chagua Option No.4 ku 'Lipa Bili'.<br/>Hatua ya pili - weka namba ya biashara - 222444.<br/>Sehemu namba ya kumbu kumbu andika 'username' au namba ya simu yako ya mkononi uliyotumia kujisajili.<br/>Andika kiasi cha malip, na baada ya hapo hakikisha malipo.<br/> Airtel Money – 0688 121 252 <br/> Tigo Pesa - 0655 121 252<br/> Z-Pesa - 0774 195 784<br/>
Fedha taslimu – Flr No.7 Ibrahim Manzil, Juu ya Kearsley's Travels, Mtaa wa Zanaki, Dar es Salaam. <br/>
Hifadhi namba yako ya maombi ya manunuzi iliyopo hapo chini kwa ajili ya kumbukumbu zako. <br/>");
	
	define('VEN_IMPORT_CONTACTS_TITLE',			   "Fanya uchaguzi wa data");
	define('VEN_IMPORT_CONTACTS_MSG_1_1',			"Formati ya faili - ");
	define('VEN_IMPORT_CONTACTS_MSG_1_2',			" si sahihi");
	define('VEN_IMPORT_CONTACTS_MSG_2_1',			"Data Zimeingizwa kwa Usahihi: ");
	define('VEN_IMPORT_CONTACTS_MSG_2_2',			"<br>Namba Marudufu :");
	define('VEN_IMPORT_CONTACTS_MSG_2_3',			"<br>Simu si Sahihi :");
	define('VEN_IMPORT_CONTACTS_MSG_3',				"Upload faili la CSV, XLS au XLSX");
	define('VEN_IMPORT_CONTACTS_MSG_4',				"Chagua addressbook");
	
	define('VEN_IMPORT_CONTACTS_MSG_5',				'Chagua faili la CSV ku import');
	define('VEN_IMPORT_CONTACTS_MSG_6',				'Chagua kundi la ku import dhaani ya');
	define('VEN_IMPORT_CONTACTS_MSG_7',				'Namba ya simu zinahitajika, vitu vingine ni vya ziada');
	define('VEN_IMPORT_CONTACTS_MSG_8',				'Hizi ni data zilizojirudia kutoka faili uliloupload');
	define('VEN_IMPORT_CONTACTS_MSG_9',				'Hizi ni data zilizojirudia katika addressbook yako.');
	
	define('VEN_REGISTER_MSG_01',					'Username imesha sajiliwa');
	define('VEN_REGISTER_MSG_02',					"Barua pepe imeshatumikiwa");
	define('VEN_REGISTER_MSG_03',					"Congratulations, you're registered with Bongo Live! <br/>You'll receive an email shortly confirming your registration. You can now login on the home page._SW");
	define('VEN_REGISTER_MSG_04',					"To confirm mobile number, enter the verification code you received via sms._SW");
	define('VEN_REGISTER_MSG_05',					"Hongera, umesajiliwa na Bongo Live! <br/>Sasa unaweza ingia kwa kutumia  <a href='index.php'>ukurasa wetu wa kwanza.</a>");
	define('VEN_REGISTER_MSG_06',					"You're almost registered. To confirm your account, enter the verification code you received via sms or click on the link sent to your email._SW");
	define('VEN_REGISTER_MSG_07',					'<br> Sending email failed. <br />_SW');
	define('VEN_REGISTER_MSG_08',					'<br> Sending massege to mobile number failed. <br />_SW');
	define('VEN_REGISTER_MSG_09',					"Mobile verification done._SW");
	define('VEN_REGISTER_MSG_10',					"Verification code is incorrect!_SW");
	define('VEN_REGISTER_MSG_11',					"Mobile confirmation not updated_SW");
	
	define('VEN_SENDERID_MSG_1',					"sehemu ya jina la mtumaji imetumwa bila kujazwa");
	define('VEN_SENDERID_MSG_2',					"Ombi la jina la mtumaji limetumwa kwa usahihi");
	define('VEN_SENDERID_MSG_3',					"Jina la mtumaji limeshatumiwa");
	define('VEN_SENDERID_MSG_4',					"Ombi la jina la mtumaji haitumwa kwa usahihi");
	define('VEN_SENDERID_MSG_5',					"Jina la mtumaji si sahihi");
	define('VEN_SENDERID_MSG_6',					"Namba ya simu isizidi tarakimu 14");
	define('VEN_SENDERID_MSG_7',					"Jina la Mtumaji lisizidi herufi 11");
	define('VEN_SENDERID_TEXT_1',					"Jina la mtumaji ni nini?<br/><br />Jina la mtumaji ni maandishi au tarakimu zinazoonekana kama ‘Kutoka kwa:’ katika meseji yako ya sms. Hii inaweza kuwa ama herufi 11 au namba ya simu ya mkononi katika fomati inayokubalika kimataifa ambayo ina urefu wa hadi tarakimu 13. Mfano, +255784234357.<br /><br />Kwa nini jina la mtumaji linahitaji kuidhinishwa?<br /><br />Kuna uwezekano wa jina la mtumaji kutumiwa na mtu mwingine, shirika au bidhaa nyingine, n.k. (Mfano, duka la kompyuta linaweza kutuma sms yenye jina la mtumaji ‘Dell’). Tunajitahidi kuzuia watu binafsi au mashirika kutumia vibaya uwezekano huo ndiyo maana ni muhimu kupata idhini.<br /><br />...........................................................................................................................................................................<br/>_SW");
	
	define('VEN_SMS_PUSH_MSG_01',					"Huwezi kupanga tarehe ya zamani kwajili ya meseji");
	define('VEN_SMS_PUSH_MSG_02',					"Una salio ya sms lakutosha");
	define('VEN_SMS_PUSH_MSG_03',					"Meseji itatumwa kwa muda uliyopanga");
	define('VEN_SMS_PUSH_MSG_04',					"Meseji limetumwa sahihi");
	define('VEN_SMS_PUSH_MSG_05',					"Kundi uliyochagua aina contacts");
	define('VEN_SMS_PUSH_MSG_06',					"Chagua kundi au contact");
	define('VEN_SMS_PUSH_MSG_07',					"Column iliyo na Jina la Kwanza Inahitajika.");
	define('VEN_SMS_PUSH_MSG_08',					"Column iliyo na namba ya simu inahitajika");
	define('VEN_SMS_PUSH_MSG_09',					"Hakuna taarifa katika faili ulilochagua");
	define('VEN_SMS_PUSH_MSG_10',					"Hakuna namba ya simu iliyopatikana katika faili ulilochagua");
	
	define('SELECT_GROUP_OR_CONTACT',				"Chagua Kundi au Contact");
	define('ENTER_TEXT_MASSAGE',					"Andika Meseji");
	define('SELECT_SENDER_NAME',					"Chagua jina la mtumaji");
	define('MAXIMUM_NUMBER_RECIPIENTS',				"Umefikia Kiwango cha mwisho cha idadi ya namba");
	define('SCHEDULE_DATE',							"Panga tarehe");
	
	define('VEN_COTENTHELP_GROUPSMS_STEP_01',		"</br> Select the groups or contacts that you would like to send an sms to. </br></br> 1. Move the selected groups (if any) from the contact box to the selected contact box. </br></br> 2. Search for an individual by Mobile number, First Name or Last Name by clicking the magnifying glass button. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Group_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Group SMS Manual</a>");
	define('VEN_COTENTHELP_GROUPSMS_STEP_02',		"</br> 1. Sender Name is who the sms will appear 'From'.(Required). </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Use placeholder if you want to use data stored in your addressbook to personalize each sms (optional). You can add more than one. </br></br> 4. Schedule your message to be sent at a later date/time. (optional) </br></br> 5. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Group_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Group SMS Manual</a>");
	
	define('VEN_COTENTHELP_QUICKSMS_STEP_01',		"</br> 1.Type or paste the mobile numbers that you want to send sms to. They can be in any format. Ex. 255784XX, 0784XXX or 784XXX. </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Type your sms message. </br></br> 4. Sender Name is who the sms will appear 'From'.(Required). </br></br> 5. Schedule your message to be sent at a later date/time. (optional) </br></br> 6. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Quick_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Quick SMS Manual</a>");

define('VEN_COTENTHELP_QUICKSMS_STEP_02',		"</br> Confirm that your sms and credit counts are accurate. One credit is the same as one sms.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Quick_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Quick SMS Manual</a>");
	
	define('VEN_COTENTHELP_FILESMS_STEP_01',		"</br> Send the same or personalized sms using data in a file using this tool. </br></br> Upload the file with all your date & contact information. The file can be of type comma-separated values(.csv), excel 2003(.xls), excel 2007 (.xlsx), text file(.txt - with comma separated columns of data).<br /><br /> For any file type, make sure that all the data are sorted neatly in columns. I.e all mobile numbers are in one column, all first names are in another etc. You can upload a file with as many different columns as desired. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");

define('VEN_COTENTHELP_FILESMS_STEP_02',		"</br> Preview the data that you uploaded. </br></br> 1. If you have column titles/headers in your file then select the checkbox for 'Use First Row as Header'. The screen will refresh to refelct the change.</br></br> 2. Now select the column in the dropdown that has the recepient mobile numbers. </br></br> 3. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");

define('VEN_COTENTHELP_FILESMS_STEP_03',		"</br> 1. Sender Name is who the sms will appear 'From'.(Required). </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Use placeholder if you want to use other data/columns in your file to personalize each sms (optional). You can add more then one.</br></br> 4. Schedule your message to be sent at a later date/time. (optional) </br></br> 5. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_01',	"</br> Import contacts into the addressbook using this tool. </br></br> Upload the file with all your date & contact information. The file can be of type comma-separated values(.csv), excel 2003(.xls), excel 2007 (.xlsx), text file(.txt - with comma separated columns of data).<br /><br /> For any file type, make sure that all the data are sorted neatly in columns. I.e all mobile numbers are in one column, all first names are in another etc. <br /><br />You can upload data into all columns listed below: Mobile Number (required), Title, First Name, Last Name, Email, Mobile Number 2, Gender, Date/Date of Birth, Area, City, Country, Optional One, Optional Two.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_02',	"</br> Preview the data that you uploaded. </br></br> 1. If you have column titles/headers in your file then select the checkbox for 'Use First Row as Header'. The screen will refresh to refelct the change.</br></br> 2. Now map your data by selecting the field from the dropdown that matches the data in that column. Repeat for all </br></br> 3. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_03',	"</br> Select the group (s) that you want to add the contacts into. </br></br> The 'Default' group will always be selected. It is your master group. </br></br> 2. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_CONTACT_INSIGHT',	"</br> This page provides you with insights and analytics into the contacts in your addressbook. </br></br> You can select between a bar graph or pie chart representation of your data. To show accurate results ensure that the data in your addressbook is accurate.");
	
	define('VEN_COTENTHELP_CREDIT_PURCHASE',	"</br> Place a new purchase request to get sms credits for your account. </br></br> 1. Enter the quantity of credit you'd like to purchase. Wait for the page to refresh and calculate the rate for the desired quantity. </br></br> 2. Click 'Submit' to place your purchase request. You will get payment instructions on the next screen. Credits will be added after payment is completed.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Purchase_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Purchase SMS Manual</a>");
	
	/* Vendor variables_END */
	
	
	/* Vendor subscribers_BEGIN */
	define('SUB_CHANGE_PSWD_MSG_01',	"Neno lako la siri limerekebishwa. Utapokea baruapepe na meseji kuthibitisha neno jipya la siri");
	define('SUB_CHANGE_PSWD_MSG_02',	"Neno lako la siri limerekebishwa. Utapokea baruapepe kuthibitisha neno jipya la siri");
	define('SUB_CHANGE_PSWD_MSG_03',	"Neno la siri haijabadilishwa");
	define('SUB_CHANGE_PSWD_MSG_04',	"Neno la siri la zamani hai lingani");
	define('SUB_CHANGE_MOBILE_MSG_01',	"Utapokea baruapepe kuthibitisha namba ya simu uliyobadilisha.<br>Utapokea neno la siri katika simu yako <br>Andika neno la siri hapa.");
	define('SUB_CHANGE_MOBILE_MSG_02',	"Utapokea baruapepe kuthibitisha namba ya simu uliyobadilisha.");
	define('SUB_CHANGE_MOBILE_MSG_03',	"Namba ya simu imeshasajiliwa");
	define('SUB_CHANGE_MOBILE_MSG_04',	"Namba ya simu si sahihi");
	define('SUB_CHANGE_MOBILE_MSG_05',	"Neno la siri hai lingani");
	define('SUB_CHANGE_MOBILE_MSG_06',	"Uhakikisho imekamilika sawa sawa");
	define('SUB_CHANGE_MOBILE_MSG_07',	"Neno la siri haijasasishwa");
	
	define('SUBCRIBERS_PROFILE',		'Profile ya Memba');
	
	define('SUB_TITLE',					'PSPF! - Bulk SMS');
	define('SUB_MENU_PROFILE',			'Profile');
	define('SUB_MENU_CHANGE_MOBILE',	'Badilisha Namba ya Simu');
	define('SUB_MENU_CHANGE_PASSWORD',	'Badilisha Neno la Siri');
	define('SUBCRIBERS',				'Memba');
	
	define('SUBREGISTER_SIGN_UP',		'Usajii ya Memba');
	define('SUB_REGISTER_MSG_01',		"Baruapepe imeshasajiliwa");
	define('SUB_REGISTER_MSG_02',		"Namba ya simu imeshasajiliwa");
	define('SUB_REGISTER_MSG_03',		"Namba ya simu si sahihi");
	define('SUB_REGISTER_MSG_04',		"Congratulations, you're registered with Bongo Live! <br/>You'll receive an email shortly confirming your registration. You can now login on the home page._SW");
	define('SUB_REGISTER_MSG_05',		"You're almost registered. To confirm your mobile number, enter the verification code you received via sms._SW");
	define('SUB_REGISTER_MSG_06',		"You're almost registered. To confirm your account, click on the link sent to your email_SW");
	define('SUB_REGISTER_MSG_07',		"You're almost registered. To confirm your account, enter the verification code you received via sms or click on the link sent to your email._SW");
	define('SUB_REGISTER_MSG_08',		'<br> Sending email failed. <br>_SW');
	define('SUB_REGISTER_MSG_09',		'<br> Sending massege to mobile number failed. <br>_SW');
	define('SUB_REGISTER_MSG_10',		"Mobile verification done._SW");
	define('SUB_REGISTER_MSG_11',		"Text does not match_SW");
	define('SUB_REGISTER_MSG_12',		"Mobile confirmation not updated_SW");
	define('SUB_REGISTER_TEXT_01',		'Ofa za <br/>Laptop, Simu, Mitindo,<br/>Burudani, Kazi <br/>Elimu na mengineyo...');
	define('SUB_REGISTER_TEXT_02',		'Hakuna Spam! Ofa na deali halisi tu');
	define('SUB_REGISTER_TEXT_03',		'Jiunge nasi kune Facebook!');
	define('SUB_REGISTER_TEXT_04',		'');
	/* Vendor subscribers_END */
	
	
	/* /activate_acnt.php_BEGIN */
	define('ACTIVATE_ACNT_MSG_01',	"Thank you for confirming your account. Your account has now been activated.<br/><br/>You can now log into the application, manage your profile, contacts and broadcast sms.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a> page.<br/><br/>Click <a href='_SW");
	define('ACTIVATE_ACNT_MSG_02',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>_SW");
	define('ACTIVATE_ACNT_MSG_03',	"<font color='#FF0000'><b>Your account has been already activated.</b></font><br/><br/>You can now log into the application, manage your profile, contacts and broadcast sms.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a>  page.<br/><br/>Click <a href='_SW");
	define('ACTIVATE_ACNT_MSG_04',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>_SW");
	define('ACTIVATE_ACNT_MSG_05',	'Invalid link_SW');
	define('ACTIVATE_ACNT_MSG_06',	"Thank you for confirming your account. Your account has now been activated.<br/><br/>You can now log into the application, manage your profile, preferences and change your mobile number.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a>  page.<br/><br/>Click <a href='_SW");
	define('ACTIVATE_ACNT_MSG_07',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>_SW");
	define('ACTIVATE_ACNT_MSG_08',	"<font color='#FF0000'><b>Your account has been already activated.</b></font><br/><br/>You can now log into the application, manage your profile, preferences and change your mobile number.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a> page.<br/><br/>Click <a href='_SW");
	define('ACTIVATE_ACNT_MSG_09',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>_SW");
	/* /activate_acnt.php_END */
	
	
	/* Common variables_BEGIN */
	define('MEMBER_REGISTRATION',				'Usajili');
	define('ADD_CONTACT',						'Ongeza Contact');
	define('COMPOSE_SMS',						'SMS Kwa Jumla');
	define('COMPOSE_QUICK_SMS',					'SMS Chap Chap');
	define('COMPOSE_FILE_SMS',					'Tumia Faili Kutuma SMS');
	
	define('UPLOAD_FILE_NAME',					'Chagua Faili (csv,txt,xls na xlsx)');
	define('UPLOAD_FILE_MSG_1',					'Ku Upload chagua faili la csv, text, xls au xlsx');
	define('UPLOAD_FILE_MSG_2',					'Mwisho wa ukubwa wa faili ni 3MB');
	define('UPLOAD_FILE_MSG_3',					'Faili za text na excel pekee ');
	define('UPLOAD_FILE_MSG_4',					'Sampuli Za Meseji Kwa Watumiwa Watatu Wakwanza');
	define('UPLOAD_FILE_MSG_5',					'Mwisho wa ukubwa wa faili ni 300KB kwa faili la XLSX.<br />Unaweza ukaligawanya faili lako, <br /> au badili fomati ya faili kuwa CSV au XLS');
	define('ROWS_TO_SHOW',						'Tunaonesha Row 9 Za Kwanza');
	define('HEADER_COLUMN',						'Tumia Row Ya Kwanza Kama Header (Ni hiari)');
	define('RECIPIENT_COLUMN',					'Chagua Column Iliyo Na Namba Za Simu');
	define('PLACE_HOLDER',					    'Place Holder');
	define('TOTAL_RECORDS',					    'Records Kwa Jumla');
	define('INVALID_RECORDS',					'Records Zisizofaa');
	define('VALID_RECORDS',					    'Records Zinazofaa');
	define('JOB_NAME',					        'Job Name');
	
	define('MANAGE_BROADCAST',					'Kusimamia matangazo');
	define('MANAGE_PURCHASE_REQUEST',			'Kusimamia Purchase Request');
	define('DESCRIPTION',						'Maelezo');
	define('CREATE_GROUP',						'Anzisha Kundi');
	define('SMS_BALANCE',						'Salio la SMS');
	define('COPYRIGHT',							'&copy; Bongo Live Enterprise Ltd. Haki Zote Zimehifadhiwa');
	define('SELECTED_GROUPS',					'Makundi Yaliyochaguliwa');
	define('GROUPS',							'Makundi');
	define('SELECTED_CONTACTS',					'Contacts Yaliyochaguliwa');
	define('DEFAULT_GROUP',						'Kundi "Default"');
	define('MESSAGE_BODY',						'Meseji yenyewe');
	define('TEXT_MESSAGE',						'Meseji');
	define('ENTER_NUMBERS',						'Namba Ya Simu');
	define('RECIPIENTS',					        'Namba Ya Simu');
	define('ENTER_RECIPIENT',					'Tafadhali Ingiza Namba Ya Simu');
	define('RECIPIENT_DISCRIPTION',				'Ingiza namba ya simu moja katika kila msitari (Maximum 50)');
	define('COUNTER',							'Kihesabio');
	define('SENDER_NAME',						'Jina la Mtumaji');
	define('ADVANCED_OPTION',					'Option za zaidi');
	define('SMS_COUNT',							'Idadi ya SMS');
	define('CHARACTER_COUNT',					'Idadi ya Herufi');
	define('TOTAL_CONTACTS',					'Jumla ya Contacts Yote');
	define('CREDITS_BALANCE',					'Salio la SMS');
	define('CREDITS_COSTS',						'Bei ya salio la SMS');
	define('REQUEST_NEW',						'Ombi Mpya');
	define('STATUS',							'Status');
	define('CONTACTS',							'Contacts');
	define('CREDIT_SMS_BALANCE',				'Salio la SMS');
	define('PURCHASE_QUANTITY',					'Kiasi kinachonunuliwa');
	define('PURCHASE_PACKAGE',					'Kifurushi cha Kununua');
	define('PRICE_SMS',							'Bei kwa SMS');
	define('TOTAL_COST',						'Gharama Yote');
	define('PURCHASE_REQUEST_NUMBER',			'Namba ya Mombi ya Mnunuzi');
	define('DATE_TIME',							'Tarehe/Muda');
	define('TOTAL_QUANTITY_PURCHASED',			'Kiasi kilichonunuliwa');
	define('TOTAL_PURCHASE_COST',				'Gharama yote ya manunuzi');
	define('ENTER_ONLY_OPOSIT_INT_VALUE',		'Ingiza tu namba kamili chanya');
	define('SELECT_PURCHASE_SCHEME',			'Chagua Kifurushi cha Kununua');
	define('TITLE',								'Cheo');
	define('SELECT',							'Chagua');
	define('MR',								'Mr');
	define('MRS',								'Mrs');
	define('MISS',								'Miss');
	define('FIRST_NAME',						'Jina la Kwanza');
	define('LAST_NAME',							'Jina la Mwisho');
	define('EMAIL',								'Baruapepe');
	define('EMAIL_',							'Baruapepe');
	define('MOBILE_NUMBER',						'Simu ya Mkononi');
	define('MOBILE_NUMBER2',					'Simu ya Mkononi nyingine');
	define('MOBILE_NUMBER_',					'Simu ya Mkononi');
	define('PHONE_NUMBER',						'Simu ya kawaida');
	define('COMMENTS',							'Maoni');
	define('ALL',								'Wote');
	define('MOBILE',							'Simu ya Mkononi');
	define('GROUP_NAME',						'Jina la Kundi');
	define('GENDER',							'Jinsia');
	define('GENDER_',							'Jinsia');
	define('IMPORT',							'Kuagiza');	
	define('SAVE',								'Ila');
	define('NEXT',								'Inayofuata');
	define('BACK',								'Rudia');
	define('DELETE',							'Futa');
	define('CONFIRM',							'Thibitisha');
	define('SEND',								'Tuma');
	define('SEARCH',							'Tafuta');
	define('DOWNLOAD_LOG',							'Shusha Ingia');
	define('SUBMIT',							'Tuma');
	define('EDIT',								'Hariri');
	define('RESET',								'Futa Yote');
	define('NO_RECORDS',						'No records');
	define('SELECT_CONTACTS',					'Chagua Contacts');
	define('ABOUT_US',							'Kuhusu Sisi');
	define('PRIVACY',							'Usiri');
	define('TERMS_OF_USE',						'Masharti');
	define('FAQ',								'FAQ');
	define('FAQ_',								'Maswali Yaulizwayo na Wengi');
	define('CONTACT_US',						'Kuwasiliana');
	//define('WHO_CAN_USE_IT',					'Watumiaji');
	//define('PRICING',							'Gharama');
	define('HOW_IT_WORKS',						'Utumiaji');
	define('YOU_ARE_CONFIRMED',					'Akaunti yako imethibitishwa!');
	define('GROUP_SMS',							'SMS za Makundi/Jumla');
	define('ONLINE_ADVERTISING',				'Kujitangaza kwa Tovuti');
	define('CUSTOM_MOB_APPLICATIONS',			'‘Application’ za Simu & SMS Kulingana na Mahitaji');
	define('PASSWORD_',							'Neno la Siri');
	define('ENTER_SMS_PURCHASE_REQUEST',		'Ingiza Ombi la Kununua SMS');
	define('PROFILE_UPDATED',					'Maelezo binafsi yamerekebishwa');
	define('BROADCASTER_PROFILE',				'Broadcaster Profile');
	define('ORGANIZATION_CATEGORY',				'Kundi la Shirika');
	define('NUMBER_OF_EMPLOYEES',				'Idadi ya Wafanyakazi');
	define('POSTAL_ADDRESS',					'Anwani ya Posta');
	define('PHYSICAL_ADDRESS',					'Anwani ya Mahali Halisi');
	define('LANDLINE_NUMBER',					'Simu ya Mezani');
	define('ORGANIZATION_NAME',					'Jina la Shirika');
	define('NAME_REQUIRED',						'Jina linahitajika');
	define('ENTER_ORGANIZATION_NAME',			'Ingiza jina la shirika');
	define('SELECT_ORGANIZATION_CATEGORY',		'Chagua kundi la shirika');
	define('PHYSICAL_ADDRESS_REQUIRED',			'Anwani ya makazi inatakiwa');
	define('SELECT_NUMBER_OF_EMPLOYEES',		'Chagua idadi ya wafanyakazi');
	define('SELECT_LOCATION',					'Chagua mahali');
	
	define('CONTACT_US_MSG_1',					"Namba la siri hailingani");
	define('CONTACT_US_MSG_2',					"Ombi lako imefika Bongo Live!");
	define('CONTACT_US_MSG_3',					'<br>Baruapepe haijatumwa. <br>');
	
	define('FORGOT_PSWD_MSG_01',				"The email address entered does not exist._SW");
	define('FORGOT_PSWD_MSG_02',				"Your new temporary password has been sent to your email address and mobile phone._SW");
	define('FORGOT_PSWD_MSG_03',				"Your new temporary password has been sent to your email address._SW");
	define('FORGOT_PSWD_MSG_04',				"The email address entered does not exist._SW");
	define('FORGOT_PSWD_MSG_05',				"Your new password is sent to your mail-box_SW");
	define('FORGOT_PSWD_MSG_06',				"Your new temporary password has been sent to your email address and mobile phone._SW");
	define('FORGOT_PSWD_MSG_07',				"Your new temporary password has been sent to your email address._SW");
	define('FORGOT_PSWD_MSG_08',				"Baruapepe siyo halali");
	
	define('SELECT_USER_TYPE',					'Chagua aina ya akaunti');
	define('ENTER_YOUR_EMAIL',					'Ingiza baruapepe');
	define('ENTER_YOUR_VALID_EMAIL',			"Ingiza barapepe halali");
	define('RECORD_INSERTED',					"Record limeingizwa");
	
	define('LOGIN_MSG_01',						'Namba ya simu ya mkononi na neno la siri ni lazima');
	define('LOGIN_MSG_02',						'Namba ya simu au neno la siri si sahihi');
	define('LOGIN_MSG_03',						'Username na neno la siri ni lazima');
	define('LOGIN_MSG_04',						'Namba ya simu au neno la siri si sahihi');
		
	define('USER_TYPE',							'Aina ya Akaunti');
	define('ADMIN',								'Admin');
	define('PRIVACY_POLICY',					'Sera ya Usiri');
	define('SMS_ADVERTISING',					'Matangazo za SMS');
	define('WHO_CAN_USE_THIS',					'Anayeweza kuitumia');
	define('CUSTOMERS',							'Wateja');
	define('ENTER_NAME',						'Andika Jina');
	define('ENTER_SUBJECT',						'Andika Mada');
	define('ENTER_MESSAGE',						'Andika Ujumbe');
	define('ENTER_CAPTCHA_CODE',				'Andika Captcha code');
	define('CONTACT_US_PREINFO',				'<p>Bongo Live! wamejitoa kuleta uzoefu mkubwa katika huduma kwa wateja. Tafadhali wasiliana nasi endapo umevutiwa na huduma zetu au ungependa tushughulikie jambo fulani. Tutapenda kusikia kutoka kwako.</p> <p>Email: <a href="mailto:contact@bongolive.co.tz">contact@bongolive.co.tz</a></p><p>Simu: 0688 121252 (Airtel), 0765 121252 (Voda)</p><p><p>Mauzo: 7th Flr, Ibrahim Manzil, Jengo la Kearsly Travels, Mtaa wa Zanaki, Dar es Salaam</p><p> Ofisi ya Maandalizi: Plot 17/54 Uhuru St, Opp Moodys, Dar es Salaam</p>');
	define('NAME',								'Jina');
	define('NAME_',								'Jina');
	define('EMAIL_ADDRESS',						'Baruapepe');
	define('BUSINESS_NAME',						'Jina la Biashara');
	define('SUBJECT',							'Mada');
	define('MESSAGE',							'Ujumbe');
	define('CAPTCHA_CODE',						'CAPTCHA Code');
	define('ENTER_NEW_MOBILE_NUMBER',			'Andika namba ya simu mpya');
	define('ENTER_MOBILE_VERIFICATION_CODE',	'Andika neno la siri uliyopokea kune simu');
	define('ENTER_MOBILE_CONFIRMATION_CODE',	'Andika neno la siri uliyopokea kune simu');
	define('OLD_PASSWORD',						'Neno la Siri la Zamani');
	define('NEW_PASSWORD',						'Neno la Siri Mpya');
	define('CONFIRM_PASSWORD',					'Thibitisha Neno la Siri');
	define('CONFIRM_PASSWORD_',					'Thibitisha Neno la Siri');
	define('CHANGE_PASSWORD',					'Badilisha Neno la Siri');
	define('CHANGE_MOBILE',						'Badilisha Namba ya Simu');
	define('AGE_RANGE',							'Umri');
	define('AGE_RANGE_',						'Umri');
	define('EDUCATION',							'Elimu');
	define('EDUCATION_',						'Elimu');
	define('EMPLOYMENT',						'Ajira');
	define('EMPLOYMENT_',						'Ajira');
	define('OCCUPATION',						'Ujuzi');
	define('OCCUPATION_',						'Ujuzi');
	define('COUNTRY',							'Nnchi');
	define('LOCATION',							'Mahali');
	define('LOCATION_',							'Mahali');
	define('City',							'Mji');
	define('AREA',							'Kitongoji(Tarafa/Kata)');
	define('INTERESTS',							'Uyapendayo');
	define('INTERESTS_',						'Uyapendayo');
	define('MALE',								'Mume');
	define('FEMALE',							'Kike');
	define('OPTIONAL_ONE',						'Ziada ya kwanza');
	define('OPTIONAL_TWO',						'Ziada ya plili');
	define('BIRTH_DATE',						'Tarehe ya Kuzaliwa');
	define('ACCEPT_BONGOLIVE',					'Na Kubali Masharti za Bongo Live!');
	define('MOBILE_VERIFICATION',				'Thibitisha namba ya simu ya mkononi');
	define('ENTER_VERIFICATION_CODE',			'Andika neno la siri uliyopokea kune simu');
	
	define('SELECT_ANY_GROUP',					'Chagua Kundi');
	define('FIRST_NAME_REQUIRED',				'Jina la kwanza ni lazima');
	
	define('SURE_DELETE_GROUP',					'Una uhakika unataka kufuta kundi hili? ');
	define('SELECT_GROUP',						'Chagua Kundi');
	define('BROADCASTER_ID',					'Utambulisho wa Broadcasterp');
	define('BROADCASTER_APIKEY',				'API Key');
	define('BROADCASTER_WEBSITE',				'Tovuti');
	define('ACCOUNT_TYPE',						'Aina ya Akaunti');
	define('INDIVIDUAL',						'Mtu Binafsi');
	define('ORGANIZATION',						'Shirika');
	define('REPRESENTATIVE_NAME',				'Jina la Mwakilishi');
	define('BROADCASTER_SIGN_UP',				'Usajili ya Mtumiaji wa SMS Jumla');
	/* Common variables_BEGIN */
