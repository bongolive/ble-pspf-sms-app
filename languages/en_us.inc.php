<?php
	/* Header variables_BEGIN */
	define('HEADER_WELCOME',	'Welcome');
	define('HEADER_LOGOUT',		'Logout');
	/* Header variables_END */
	
	
	/* Menu variables_BEGIN */
	define('MENU_SERVICES',							'Services');
	define('MENU_SERVICES_GROUP_BULK_SMS',			'Group / Bulk SMS');
	define('MENU_SERVICES_SMS_ADVERTISING',			'SMS Advertising');
	define('MENU_SERVICES_SMS_OFFERS',				'SMS Offers');
	define('MENU_SERVICES_CUSTOM_APPLICATIONS',		'Custom Applications');
	define('MENU_SERVICES_ONLINE_ADVERTISING',		'Online Advertising');
	define('MENU_SERVICES_DEVELOPERS',				'Developer API');
	define('MENU_SERVICES_PLUGINS',					'Plugins');
	/* Menu variables_END */
		
	/* Index_page variables_BEGIN */
	define('INDEX_SIGN_UP_INDIVIDUAL_1',	'Receive free offers via SMS');
	define('INDEX_SIGN_UP_INDIVIDUAL_2',	'Register as a Member Today!');
	define('INDEX_SIGN_UP_COMPANY_1',		'Broadcast SMS to many people');
	define('INDEX_SIGN_UP_COMPANY_2',		'Register for free as a Broadcaster');
	define('INDEX_SUBSCRIBER',				'Member');	
	define('INDEX_BROADCASTER',				'Broadcaster');
	define('INDEX_MOBILE_NO',				'Mobile No');
	define('INDEX_PASSWORD',				'Password');
	define('INDEX_FORGOT_PASSWORD',			'Forgot Password');
	define('INDEX_USERNAME',				'Username');
	define('INDEX_HAPPY_CUSTOMERS_1',		'What our happy customers say');
	define('INDEX_HAPPY_CUSTOMERS_2',		'People have responded to the SMSs, I\'ve seen a response<br/>from old customers who might have forgotten about us." - Catherine Katala Designs, Slipway');
	define('INDEX_SOCIAL_MEDIA',			'Social Media');
	define('INDEX_BLOCK_DESC_1',			'Connect via SMS<br />with a click of a mouse');
	define('INDEX_BLOCK_DESC_2',			'Target your<br />Customer Segment');			
	define('INDEX_BLOCK_DESC_3',			'Free Offers of your Choice<br />via SMS');
	define('INDEX_BLOCK_DESC_4',			'SMS &amp; Mobile Applications<br />to suit your needs');
	
	define('BANNER_TITLE_1',				'Offers Direct to your Mobile. FREE!');
	define('BANNER_DESCRIPTION_1',			'Want to get the best deals in your town. Receive customized SMS offers based on your interests. Our adverts range from fashion to travel, furniture to music and hardware to jobs. No spam, just pure deals.');
	define('BANNER_TITLE_2',				'Group Messaging');
	define('BANNER_DESCRIPTION_2',			'No more going through your entire mobile address book to send to friends, family, customers and suppliers. Just add your contacts ONCE and start sending.');
	define('BANNER_TITLE_3',				'Reach New Customers');
	define('BANNER_DESCRIPTION_3',			'Target your subscribers directly on their mobiles. Send customized SMS adverts to your target demographic.');
	define('BANNER_TITLE_4',				'Brand Your Messages');
	define('BANNER_DESCRIPTION_4',			'Want to create a professional image. Your messages can now show the name of your business or organization using our platform.');
	define('BANNER_TITLE_5',				'Forbes Africa Top 20 Tech Startup');
	define('BANNER_DESCRIPTION_5',			'Bongo Live was recently names by Forbes Africa Magazine as a Top 20 African Tech Startup.');
	define('BANNER_TITLE_6',				'New Mobi Site');
	define('BANNER_DESCRIPTION_6',			'Our new mobi site boasts a sleek design and a streamlined registration process for new subscribers.');
	define('BANNER_TITLE_7',				'SMS Alerts For Your Blog');
	define('BANNER_DESCRIPTION_7',			'Keep your blog readers informed about the latest posts via SMS. Try out our new Wordpress and Blogspot plugins.');	
	define('BANNER_TITLE_8',				'Bongo Live on Al-Jazeera');
	define('BANNER_DESCRIPTION_8',			'Bongo Live founder Taha Jiwaji was recently interviewed about technology innovation in Africa on The Stream, a talk show on Al-Jazeera.');
	/* Index_page variables_END */
	
	
	/* Project_vars_BEGIN */
	define('PV_TITLE_DEFAULT',				'PSPF - Bulk SMS Tanzania | SMS Marketing Tanzania | Mobile Marketing in Tanzania');
	define('PV_TITLE_INDEX',				'PSPF - Bulk SMS Tanzania | Mobile Marketing Tanzania | Online Marketing Tanzania');
	define('PV_TITLE_OFFERS',				'Bongo Live - SMS Marketing Tanzania, Bulk SMS Marketing, Bulk SMS Tanzania');
	define('PV_TITLE_ABOUT_US',				'Bongo Live - About Us');
	define('PV_TITLE_BULK_SMS',				'Bongo Live - Bulk SMS Tanzania | SMS Marketing Tanzania | Bulk SMS Marketing');
	define('PV_TITLE_ONLINE_ADVERTISING',	'Bongo Live - Google Advertising Tanzania | Facebook Advertising Tanzania | Facebook Advertising');
	define('PV_TITLE_PRIVACY',				'Bongo Live - Privacy Policy ');
	define('PV_TITLE_TAILORED_SERVICES',	'Bongo Live - SMS Voting, Raffles, Surveys, On Demand ');
	define('PV_TITLE_WHO_CAN_USE',			'Bongo Live - Who can use this ');
	define('PV_TITLE_CONTACT_US',			'Bongo Live - Contact Us ');
	define('PV_TITLE_HELP',					'Bongo Live - Help ');
	define('PV_TITLE_CUSTOM_APPLICATIONS',	'Bongo Live - Custom Mobile & SMS Applications ');
	define('PV_TITLE_HOW_IT_WORKS',			'Bongo Live - How it works ');
	define('PV_TITLE_PRICING',				'Bongo Live - Pricing ');
	define('PV_TITLE_SMS_ADVERTISING',		'Bongo Live - Targeted SMS Marketing ');
	define('PV_TITLE_TERMS_OF_USE',			'Bongo Live - Terms of Use ');
	define('PV_TITLE_FAQ',					'Bongo Live - FAQ ');
	
	define('PV_REGISTER_MESSAGE',			'Tunashukuru kwa kujisajili Bongo Live! Weka namba ya usajili sehemu ya verification code. Ambayo ni MESSAGE \n Bada ya hapo bonyeza alama ya "Confirm"');
	define('PV_PASSWORD_CHANGE_MESSAGE',	'Your new Bongo Live! password is ');
	define('PV_ACCT_ACTIVATION_MSG',		'Bongo Live! Registration');
	/* Project_vars_END */
	
	
	/* Vendor variables_BEGIN */
	define('VEN_HEADER_TITLE',						'PSPF! - Broadcaster ');
	define('VEN_BROADCASTER',						'Broadcaster');
	
	define('VEN_MENUTOP_DASHBOARD',					'Groups & Contacts');
	define('VEN_MENUTOP_DASHBOARD_1',				'Manage Group');
	define('VEN_MENUTOP_DASHBOARD_2',				'Add New Contact');
	define('VEN_MENUTOP_DASHBOARD_3',				'Import Contact');
	define('VEN_MENUTOP_DASHBOARD_4',				'Address Book');
	define('VEN_MENUTOP_DASHBOARD_5',				'Contact Insights');
	define('VEN_MENUTOP_BROADCAST_SMS',				'Bulk SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_1',			'Group SMS');
	define('VEN_MENUTOP_BROADCAST_SMS_2',			'Sender Names');
	define('VEN_MENUTOP_BROADCAST_SMS_3',			'Bulk History');
	define('VEN_MENUTOP_BROADCAST_SMS_4',			'Bulk Log Report');
	define('VEN_MENUTOP_BROADCAST_SMS_5',			'Keyword Broadcast');
	define('VEN_MENUTOP_BROADCAST_SMS_6',			'Keywords');
	define('VEN_MENUTOP_BROADCAST_SMS_7',			'Incoming SMS Log');
	define('VEN_MENUTOP_BROADCAST_SMS_8',			'SMS Templates');
	define('VEN_MENUTOP_BROADCAST_SMS_9',			'Add Templates');
	define('VEN_MENUTOP_BROADCAST_SMS_10',			'SMS Reminders');
	define('VEN_MENUTOP_INCOMING',					'Incoming SMS');
	define('VEN_MENUTOP_INCOMING_REG',				'Incoming Registration SMS');
	define('VEN_MENUTOP_INCOMING_REG_LOG',			'Incoming Registration SMS Log');
	define('VEN_MENUTOP_PURCHASE_SMS',				'Purchase SMS');
	define('VEN_MENUTOP_PURCHASE_SMS_1',				'Purchase SMS');
	define('VEN_MENUTOP_PURCHASE_SMS_2',				'History');
	define('VEN_MENUTOP_REPORT',					'Report');
	define('VEN_MENUTOP_PROFILE',					'Profile');
	define('VEN_MENUTOP_CHANGE_PASSWORD',			'Change Password');
	define('VEN_MENUTOP_HELP',						'Help');
	
	define('VEN_DASHBOARD',							'Home');
	
	define('VEN_ADDRESSBOOK_DASHBOARD',				'Manage Groups');
	define('VEN_DASHBOARD_GROUP_NAME',				'Group name');
	define('VEN_DASHBOARD_MANAGE_CONTACTS',			'Manage contacts');
	define('VEN_DASHBOARD_MSG_1',					"Group created successfully");
	define('VEN_DASHBOARD_MSG_2',					"Duplicate Group name");
	define('VEN_DASHBOARD_MSG_3',					"Group not created ");
	define('VEN_DASHBOARD_MSG_4',					"Group field is empty");
	define('VEN_DASHBOARD_MSG_5',					"Group deleted successfully");
	define('VEN_IMPORT_CONTACTS_SELECT_FILE_CSV',	'Select File (CSV, XLS or XLSX)');
	define('VEN_IMPORT_CONTACTS_DOWNLOAD_FILE_CSV',	'Download Sample File(CSV)');
	
	
	
	define('VEN_KEYWORDS',	'Keywords');
	define('VEN_KEYWORD_BROADCAST',	'Keyword Broadcast');

	define('VEN_ADD_BOOK_MSG_1',					'Contacts deleted succefully');
	define('VEN_ADD_BOOK_MSG_2',					'Contacts not deleted');
	
	define('VEN_ADD_CONTACT_MSG_1',					"Contact added successfully");
	define('VEN_ADD_CONTACT_MSG_2',					"Duplicate Mobile Number");
	
	define('VEN_CHANGE_PSWD_MSG_1',					"Your password has been reset. You will receive an email and sms to confirm the new password.");
	define('VEN_CHANGE_PSWD_MSG_2',					"Your password has been reset. You will receive an email to confirm the new password.");
	define('VEN_CHANGE_PSWD_MSG_3',					"Password not changed");
	define('VEN_CHANGE_PSWD_MSG_4',					"Old Password does not match");
	
	define('VEN_CHANGE_MOBILE_MSG_1',				"A mobile verification code is sent to your mobile<br />Please enter that code here.");
	define('VEN_CHANGE_MOBILE_MSG_2',				"Invalide mobile no.");
	define('VEN_CHANGE_MOBILE_MSG_3',				"Verification is not matching ");
	define('VEN_CHANGE_MOBILE_MSG_4',				"Verification is matched ");
	define('VEN_CHANGE_MOBILE_MSG_5',				"Verification code is not updated ");
	
	define('VEN_CHANGE_CREDIT_MSG_1',				"Purchase request sent");
	define('VEN_CHANGE_CREDIT_MSG_2',				"Purchase request not sent");
	
	define('VEN_CREDIT_PURCHASE_TEXT',				"Thank you for your sms purchase request. Please make your payment using any of the following options:<br />Mpesa: <br/>Go to Option No.4 to 'Pay Bills'.<br/>Next step - enter our business number - 222444.<br/>In the reference number field, enter your Bongo Live 'username' or the the mobile number associated with your account.<br/>Enter the amount for your purchase, then confirm your payment.<br/>Airtel Money – 0688 121 252 <br/> Tigo Pesa - 0655 121 252<br/>Z-Pesa - 0774 195 784<br/>Cash - Drop of payment at 7th Floor, Ibrahim Manzil, Above Kearsley's Travels, Zanaki St, Dar es Salaam. Call 0688121252 for assistance.<br /><br />Note your Purchase Request Number below for your records.<br />");
	
	define('VEN_IMPORT_CONTACTS_TITLE',			   "Map Your Data");
	define('VEN_IMPORT_CONTACTS_MSG_1_1',			"Format of file - ");
	define('VEN_IMPORT_CONTACTS_MSG_1_2',			" is not correct");
	define('VEN_IMPORT_CONTACTS_MSG_2_1',			"Data inserted successfully : ");
	define('VEN_IMPORT_CONTACTS_MSG_2_2',			"<br>Duplicate Phone :");
	define('VEN_IMPORT_CONTACTS_MSG_2_3',			"<br>Invalid Phone :");
	define('VEN_IMPORT_CONTACTS_MSG_3',				"Upload CSV,XLS or XLSX file");
	define('VEN_IMPORT_CONTACTS_MSG_4',				"Select addressbook");
	
	define('VEN_IMPORT_CONTACTS_MSG_5',				'Select csv file to import');
	define('VEN_IMPORT_CONTACTS_MSG_6',				'Please select a group to import into');
	define('VEN_IMPORT_CONTACTS_MSG_7',				'Mobile number is required. The other fields are optional');
	define('VEN_IMPORT_CONTACTS_MSG_8',				'Mobile numbers that were repeated in the uploaded file');
	define('VEN_IMPORT_CONTACTS_MSG_9',				'Contacts that already existed in your addressbook.');
	
	define('VEN_REGISTER_MSG_01',					'Username is already registered');
	define('VEN_REGISTER_MSG_02',					"Email-id already exist !!!");
	define('VEN_REGISTER_MSG_03',					"Congratulations, you're registered with Bongo Live! <br/>You'll receive an email shortly confirming your registration. You can now login on the home page.");
	define('VEN_REGISTER_MSG_04',					"To confirm mobile number, enter the verification code you received via sms.");
	define('VEN_REGISTER_MSG_05',					"Congratulations, you're registered with Bongo Live! <br/>You can now login on the <a href='index.php'>home page.</a>");
	define('VEN_REGISTER_MSG_06',					"You're almost registered. To confirm your account, enter the verification code you received via sms or click on the link sent to your email.");
	define('VEN_REGISTER_MSG_07',					'<br> Sending email failed. <br />');
	define('VEN_REGISTER_MSG_08',					'<br> Sending massege to mobile number failed. <br />');
	define('VEN_REGISTER_MSG_09',					"Mobile verification done.");
	define('VEN_REGISTER_MSG_10',					"Verification code is incorrect!");
	define('VEN_REGISTER_MSG_11',					"Mobile confirmation not updated");
	
	define('VEN_SENDERID_MSG_1',					"Sender Name Field submitted is blank");
	define('VEN_SENDERID_MSG_2',					"Request for Sender Name submitted successfully");
	define('VEN_SENDERID_MSG_3',					"Duplicate sender name");
	define('VEN_SENDERID_MSG_4',					"Request for Sender Name is not submitted");
	define('VEN_SENDERID_MSG_5',					"Sender Name is Invalid");
	define('VEN_SENDERID_MSG_6',					"Enter mobile number in proper international format, upto 13 digits long");
	define('VEN_SENDERID_MSG_7',					"Maximum 11 letters  for Sender Name");
	define('VEN_SENDERID_TEXT_1',					"What is a Sender Name?<br /><br />The sender name is the text or number that appears as the 'From:' on your sms message. This can either be 11 letters or a mobile number in the proper international format, upto 13 digits long. Ex. +255784234357.<br /><br />Why does a Sender Name need approval?<br /><br />The Sender Name can be used to impersonate another person, organisation, brand etc. (ex computer shop sends sms with the sender name 'DELL'). We strive to prevent individuals and organisations from misusing this capability and thus require approval.<br /><br />...........................................................................................................................................................................<br/>");
	
	define('VEN_SMS_PUSH_MSG_01',					"The message cannot be scheduled for a date/time in the past");
	define('VEN_SMS_PUSH_MSG_02',					"You have insufficient SMS Balance");
	define('VEN_SMS_PUSH_MSG_03',					"Message scheduled successfully");
	define('VEN_SMS_PUSH_MSG_04',					"Message sent successfully");
	define('VEN_SMS_PUSH_MSG_05',					"In selected group(s) count of mobile number is 0");
	define('VEN_SMS_PUSH_MSG_06',					"Select either atleast a group or atleast a contact");
	define('VEN_SMS_PUSH_MSG_07',					"Column contains First Names is Required.");
	define('VEN_SMS_PUSH_MSG_08',					"Column contains Mobile Numbers is required");
	define('VEN_SMS_PUSH_MSG_09',					"The file you uploaded is empty");
	define('VEN_SMS_PUSH_MSG_10',					"In your selected column no mobile number found");
	
	define('SELECT_GROUP_OR_CONTACT',				"Select  Group or Contact");
	define('ENTER_TEXT_MASSAGE',					"Enter text massage");
	define('SELECT_SENDER_NAME',					"Select Sender name");
	define('MAXIMUM_NUMBER_RECIPIENTS',				"You have reaached maximum number of recipients");
	define('SCHEDULE_DATE',							"Schedule date");
	
	define('VEN_COTENTHELP_GROUPSMS_STEP_01',		"</br> Select the groups or contacts that you would like to send an sms to. </br></br> 1. Move the selected groups (if any) from the contact box to the selected contact box. </br></br> 2. Search for an individual by Mobile number, First Name or Last Name by clicking the magnifying glass button. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Group_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Group SMS Manual</a>");
	define('VEN_COTENTHELP_GROUPSMS_STEP_02',		"</br> 1. Sender Name is who the sms will appear 'From'.(Required). </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Use placeholder if you want to use data stored in your addressbook to personalize each sms (optional). You can add more than one. </br></br> 4. Schedule your message to be sent at a later date/time. (optional) </br></br> 5. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Group_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Group SMS Manual</a>");
	
	define('VEN_COTENTHELP_QUICKSMS_STEP_01',		"</br> 1.Type or paste the mobile numbers that you want to send sms to. They can be in any format. Ex. 255784XX, 0784XXX or 784XXX. </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Type your sms message. </br></br> 4. Sender Name is who the sms will appear 'From'.(Required). </br></br> 5. Schedule your message to be sent at a later date/time. (optional) </br></br> 6. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Quick_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Quick SMS Manual</a>");

define('VEN_COTENTHELP_QUICKSMS_STEP_02',		"</br> Confirm that your sms and credit counts are accurate. One credit is the same as one sms.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Quick_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Quick SMS Manual</a>");
	
	define('VEN_COTENTHELP_FILESMS_STEP_01',		"</br> Send the same or personalized sms using data in a file using this tool. </br></br> Upload the file with all your date & contact information. The file can be of type comma-separated values(.csv), excel 2003(.xls), excel 2007 (.xlsx), text file(.txt - with comma separated columns of data).<br /><br /> For any file type, make sure that all the data are sorted neatly in columns. I.e all mobile numbers are in one column, all first names are in another etc. You can upload a file with as many different columns as desired. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");

define('VEN_COTENTHELP_FILESMS_STEP_02',		"</br> Preview the data that you uploaded. </br></br> 1. If you have column titles/headers in your file then select the checkbox for 'Use First Row as Header'. The screen will refresh to refelct the change.</br></br> 2. Now select the column in the dropdown that has the recepient mobile numbers. </br></br> 3. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");

define('VEN_COTENTHELP_FILESMS_STEP_03',		"</br> 1. Sender Name is who the sms will appear 'From'.(Required). </br></br> 2. Templates are sms messages that you have saved to reuse again (optional). </br></br> 3. Use placeholder if you want to use other data/columns in your file to personalize each sms (optional). You can add more then one.</br></br> 4. Schedule your message to be sent at a later date/time. (optional) </br></br> 5. Use advanced reminders to send repeatedly in the future. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_File_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>File SMS Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_01',	"</br> Import contacts into the addressbook using this tool. </br></br> Upload the file with all your date & contact information. The file can be of type comma-separated values(.csv), excel 2003(.xls), excel 2007 (.xlsx), text file(.txt - with comma separated columns of data).<br /><br /> For any file type, make sure that all the data are sorted neatly in columns. I.e all mobile numbers are in one column, all first names are in another etc. <br /><br />You can upload data into all columns listed below: Mobile Number (required), Title, First Name, Last Name, Email, Mobile Number 2, Gender, Date/Date of Birth, Area, City, Country, Optional One, Optional Two.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_02',	"</br> Preview the data that you uploaded. </br></br> 1. If you have column titles/headers in your file then select the checkbox for 'Use First Row as Header'. The screen will refresh to refelct the change.</br></br> 2. Now map your data by selecting the field from the dropdown that matches the data in that column. Repeat for all </br></br> 3. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_IMPORTCONTACT_STEP_03',	"</br> Select the group (s) that you want to add the contacts into. </br></br> The 'Default' group will always be selected. It is your master group. </br></br> 2. Click 'Next' to proceed. </br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Import_Contacts.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Import Contacts Manual</a>");
	
	define('VEN_COTENTHELP_CONTACT_INSIGHT',	"</br> This page provides you with insights and analytics into the contacts in your addressbook. </br></br> You can select between a bar graph or pie chart representation of your data. To show accurate results ensure that the data in your addressbook is accurate.");
	
	define('VEN_COTENTHELP_CREDIT_PURCHASE',	"</br> Place a new purchase request to get sms credits for your account. </br></br> 1. Enter the quantity of credit you'd like to purchase. Wait for the page to refresh and calculate the rate for the desired quantity. </br></br> 2. Click 'Submit' to place your purchase request. You will get payment instructions on the next screen. Credits will be added after payment is completed.</br></br> <a target=\'_blank\' href='".BASE_URL_HTTP_ASSETS."docs/BongoLive_Tutorial_Purchase_SMS.pdf'><img style='margin-right:10px;' width='25' height='25' src='".BASE_URL_HTTP_ASSETS."images/pdf_icon.png'>Purchase SMS Manual</a>");
	/* Vendor variables_END */
	
	
	/* Vendor subscribers_BEGIN */
	define('SUB_CHANGE_PSWD_MSG_01',	"Your password has been reset. You will receive an email and sms to confirm the new password.");
	define('SUB_CHANGE_PSWD_MSG_02',	"Your password has been reset. You will receive an email to confirm the new password.");
	define('SUB_CHANGE_PSWD_MSG_03',	"Password not changed");
	define('SUB_CHANGE_PSWD_MSG_04',	"Old Password does not match");
	define('SUB_CHANGE_MOBILE_MSG_01',	"A notification mail has been sent to your email address about the change mobile number.<br>A mobile verification code has been sent to your mobile phone<br>Please enter that code here.");
	define('SUB_CHANGE_MOBILE_MSG_02',	"A notification mail has been sent to your email address about the change mobile number.<br>");
	define('SUB_CHANGE_MOBILE_MSG_03',	"Mobile number is already registered");
	define('SUB_CHANGE_MOBILE_MSG_04',	"Invalide mobile no.");
	define('SUB_CHANGE_MOBILE_MSG_05',	"Verification code is not matching ");
	define('SUB_CHANGE_MOBILE_MSG_06',	"Verification done successfully");
	define('SUB_CHANGE_MOBILE_MSG_07',	"Verification code is not updated ");
	
	define('SUBCRIBERS_PROFILE',		'Members Profile');
	
	define('SUB_TITLE',					'PSPF! - Bulk SMS');
	define('SUB_MENU_PROFILE',			'Profile');
	define('SUB_MENU_CHANGE_MOBILE',	'Change Mobile');
	define('SUB_MENU_CHANGE_PASSWORD',	'Change Password');
	define('SUBCRIBERS',				'Members');
	
	define('SUBREGISTER_SIGN_UP',		'Member Sign Up - Receive SMS Deals for Free');
	define('SUB_REGISTER_MSG_01',		"Email address is already registered");
	define('SUB_REGISTER_MSG_02',		"Mobile number is already registered");
	define('SUB_REGISTER_MSG_03',		"Invalid mobile numbers");
	define('SUB_REGISTER_MSG_04',		"Congratulations, you're registered with Bongo Live! <br/>You'll receive an email shortly confirming your registration. You can now login on the home page.");
	define('SUB_REGISTER_MSG_05',		"You're almost registered. To confirm your mobile number, enter the verification code you received via sms.");
	define('SUB_REGISTER_MSG_06',		"You're almost registered. To confirm your account, click on the link sent to your email");
	define('SUB_REGISTER_MSG_07',		"You're almost registered. To confirm your account, enter the verification code you received via sms or click on the link sent to your email.");
	define('SUB_REGISTER_MSG_08',		'<br> Sending email failed. <br>');
	define('SUB_REGISTER_MSG_09',		'<br> Sending massege to mobile number failed. <br>');
	define('SUB_REGISTER_MSG_10',		"Mobile verification done.");
	define('SUB_REGISTER_MSG_11',		"Text dose not matche");
	define('SUB_REGISTER_MSG_12',		"Mobile confirmation not updated");
	define('SUB_REGISTER_TEXT_01',		'Offers on <br/>Laptops, Mobiles, Fashion,<br/>Entertainment, Jobs <br/>Education and more...');
	define('SUB_REGISTER_TEXT_02',		'No spam! Just Pure Deals & Offers');
	define('SUB_REGISTER_TEXT_03',		'Connect with us on Facebook!');
	define('SUB_REGISTER_TEXT_04',		'');
	/* Vendor subscribers_END */
	
	
	/* /activate_acnt.php_BEGIN */
	define('ACTIVATE_ACNT_MSG_01',	"Thank you for confirming your account. Your account has now been activated.<br/><br/>You can now log into the application, manage your profile, contacts and broadcast sms.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a> page.<br/><br/>Click <a href='");
	define('ACTIVATE_ACNT_MSG_02',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>");
	define('ACTIVATE_ACNT_MSG_03',	"<font color='#FF0000'><b>Your account has been already activated.</b></font><br/><br/>You can now log into the application, manage your profile, contacts and broadcast sms.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a>  page.<br/><br/>Click <a href='");
	define('ACTIVATE_ACNT_MSG_04',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>");
	define('ACTIVATE_ACNT_MSG_05',	'Invalid link');
	define('ACTIVATE_ACNT_MSG_06',	"Thank you for confirming your account. Your account has now been activated.<br/><br/>You can now log into the application, manage your profile, preferences and change your mobile number.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a>  page.<br/><br/>Click <a href='");
	define('ACTIVATE_ACNT_MSG_07',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>");
	define('ACTIVATE_ACNT_MSG_08',	"<font color='#FF0000'><b>Your account has been already activated.</b></font><br/><br/>You can now log into the application, manage your profile, preferences and change your mobile number.<br/><br/>Feel free to contact us at any point with questions you may have using the <a href='".BASE_URL."contactus.php'>Contact Us</a> page.<br/><br/>Click <a href='");
	define('ACTIVATE_ACNT_MSG_09',	"index.php'>here</a> to login.<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>");
	/* /activate_acnt.php_END */
	
	
	/* Common variables_BEGIN */
	define('MEMBER_REGISTRATION',				'Registrations');
	define('ADD_CONTACT',						'Add Contact');
	define('COMPOSE_SMS',						'Group SMS');
	define('COMPOSE_QUICK_SMS',						'Quick SMS');
	define('COMPOSE_FILE_SMS',						'File SMS');
	
	define('UPLOAD_FILE_NAME',					'Select File(csv, txt, xls and xlsx)');
	define('UPLOAD_FILE_MSG_1',					'To Upload select csv, text, xls Or xlsx file');
	define('UPLOAD_FILE_MSG_2',					'Maximum file size is 3MB. <br /> For larger files please split the file then try again');
	define('UPLOAD_FILE_MSG_3',					'Only excel and text files are allowed');
	define('UPLOAD_FILE_MSG_4',					'Message Preview - Displaying Only 3 Records');
	define('UPLOAD_FILE_MSG_5',					'Maximum file size for XLSX files is 300KB.<br />Convert the file to XLS or CSV format and try again.');
	define('ROWS_TO_SHOW',						'Showing Only First 9 Rows');
	define('HEADER_COLUMN',						'Use First Row As Header');
	define('RECIPIENT_COLUMN',					'Choose Recipient Column');
	define('PLACE_HOLDER',					    'Place Holder');
	define('TOTAL_RECORDS',					    'Total Records');
	define('INVALID_RECORDS',					'Invalid Records');
	define('VALID_RECORDS',					    'Valid Records');
	define('JOB_NAME',					        'Job Name');	
	
	define('MANAGE_BROADCAST',					'Manage Broadcast');
	define('MANAGE_PURCHASE_REQUEST',			'Manage Purchase Request');
	define('DESCRIPTION',						'Description');
	define('CREATE_GROUP',						'Create Group');
	define('SMS_BALANCE',						'SMS Balance');
	define('COPYRIGHT',							'&copy; Bongo Live Enterprise Ltd. All Rights Reserved');
	define('SELECTED_GROUPS',					'Selected Groups');
	define('GROUPS',							'Groups');
	define('SELECTED_CONTACTS',					'Selected Contacts');
	define('DEFAULT_GROUP',						'Default Group');
	define('MESSAGE_BODY',						'Message Body');
	define('TEXT_MESSAGE',						'Text Message');
	define('ENTER_NUMBERS',						'Enter Numbers');
	define('RECIPIENTS',						'Recipients');
	define('ENTER_RECIPIENT',					'Please Enter Recipient');
	define('RECIPIENT_DISCRIPTION',				'Please enter one recipient per line (Maximum 50)');
	define('COUNTER',							'Counter');
	define('SENDER_NAME',						'Sender Name');
	define('ADVANCED_OPTION',					'Advanced Option');
	define('SMS_COUNT',							'SMS Count');
	define('CHARACTER_COUNT',					'Character Count');
	define('TOTAL_CONTACTS',					'Total contacts');
	define('CREDITS_BALANCE',					'Credits balance');
	define('CREDITS_COSTS',						'Credits costs');
	define('REQUEST_NEW',						'Request New');
	define('STATUS',							'Status');
	define('CONTACTS',							'Contacts');
	define('CREDIT_SMS_BALANCE',				'Credit/SMS balance');
	define('PURCHASE_QUANTITY',					'Purchase Quantity');
	define('PURCHASE_PACKAGE',					'Purchase Package');
	define('PRICE_SMS',							'Price/SMS');
	define('TOTAL_COST',						'Total Cost');
	define('PURCHASE_REQUEST_NUMBER',			'Purchase Request Number');
	define('DATE_TIME',							'Date/Time');
	define('TOTAL_QUANTITY_PURCHASED',			'Total Quantity Purchased');
	define('TOTAL_PURCHASE_COST',				'Total Purchase Cost');
	define('ENTER_ONLY_OPOSIT_INT_VALUE',		'Enter only opsitive integer value');
	define('SELECT_PURCHASE_SCHEME',			'Select Purchase Scheme');
	define('TITLE',								'Title');
	define('SELECT',							'Select');
	define('MR',								'Mr');
	define('MRS',								'Mrs');
	define('MISS',								'Miss');
	define('FIRST_NAME',						'First Name');
	define('LAST_NAME',							'Last Name');
	define('EMAIL',								'Email');
	define('EMAIL_',							'Email (Baruapepe)');
	define('MOBILE_NUMBER',						'Mobile Number');
	define('MOBILE_NUMBER2',					'Other Mobile Number');
	define('MOBILE_NUMBER_',					'Mobile Number (Simu ya Mkono)');
	define('PHONE_NUMBER',						'Phone Number');
	define('COMMENTS',							'Comments');
	define('ALL',								'All');
	define('MOBILE',							'Mobile');
	define('GROUP_NAME',						'Group name');
	define('GENDER',							'Gender');
	define('GENDER_',							'Gender (Jinsia)');
	define('IMPORT',							'Import');	
	define('SAVE',								'Save');
	define('NEXT',								'Next');
	define('BACK',								'Back');
	define('DELETE',							'Delete');
	define('CONFIRM',							'Confirm');
	define('SEND',								'Send');
	define('SEARCH',							'Search');
	define('DOWNLOAD_LOG',						'Download Log');
	define('SUBMIT',							'Submit');
	define('EDIT',								'Edit');
	define('RESET',								'Reset');
	define('NO_RECORDS',						'No records');
	define('SELECT_CONTACTS',					'Select Contacts');
	define('ABOUT_US',							'About Us');
	define('PRIVACY',							'Privacy');
	define('TERMS_OF_USE',						'Terms Of Use');
	define('FAQ',								'FAQ');
	define('FAQ_',								'Frequently Asked Questions (FAQ)');
	define('CONTACT_US',						'Contact Us');
	//define('WHO_CAN_USE_IT',					'Who can use it');
	//define('PRICING',							'Pricing');
	define('CUSTOMERS',							'Customers');
	define('HOW_IT_WORKS',						'How it works');
	define('YOU_ARE_CONFIRMED',					'You are confirmed!');
	define('GROUP_SMS',							'Group SMS');
	define('ONLINE_ADVERTISING',				'Online Advertising');
	define('CUSTOM_MOB_APPLICATIONS',			'Custom Mobile & SMS Applications');
	define('PASSWORD_',							'Password (Neno la Siri)');
	define('ENTER_SMS_PURCHASE_REQUEST',		'Enter SMS purchase request');
	define('PROFILE_UPDATED',					'Profile updated');
	define('BROADCASTER_PROFILE',				'Broadcaster Profile');
	define('ORGANIZATION_CATEGORY',				'Organisation category');
	define('NUMBER_OF_EMPLOYEES',				'Number of employees');
	define('POSTAL_ADDRESS',					'Postal address');
	define('PHYSICAL_ADDRESS',					'Physical address');
	define('LANDLINE_NUMBER',					'Landline Number');
	define('ORGANIZATION_NAME',					'Organisation Name');
	define('NAME_REQUIRED',						'Name is required');
	define('ENTER_ORGANIZATION_NAME',			'Enter Organisation Name');
	define('SELECT_ORGANIZATION_CATEGORY',		'Select Organisation Category');
	define('PHYSICAL_ADDRESS_REQUIRED',			'Physical Address is required');
	define('SELECT_NUMBER_OF_EMPLOYEES',		'Select Number of employees');
	define('SELECT_LOCATION',					'Select Location');
	
	define('CONTACT_US_MSG_1',					" Text code is not matching ");
	define('CONTACT_US_MSG_2',					"Your request is sent to Bonglive");
	define('CONTACT_US_MSG_3',					'<br> Sending email failed. <br>');
	
	define('FORGOT_PSWD_MSG_01',				"The email address entered does not exist.");
	define('FORGOT_PSWD_MSG_02',				"Your new temporary password has been sent to your email address and mobile phone.");
	define('FORGOT_PSWD_MSG_03',				"Your new temporary password has been sent to your email address.");
	define('FORGOT_PSWD_MSG_04',				"The email address entered does not exist.");
	define('FORGOT_PSWD_MSG_05',				"Your new password is sent to your mail-box");
	define('FORGOT_PSWD_MSG_06',				"Your new temporary password has been sent to your email address and mobile phone.");
	define('FORGOT_PSWD_MSG_07',				"Your new temporary password has been sent to your email address.");
	define('FORGOT_PSWD_MSG_08',				"Invalid Email-Id");
	
	define('SELECT_USER_TYPE',					'Select user-type');
	define('ENTER_YOUR_EMAIL',					'Enter your email');
	define('ENTER_YOUR_VALID_EMAIL',			"Enter your valid email-id");
	define('RECORD_INSERTED',					"Record inserted");
	
	define('LOGIN_MSG_01',						'Mobile number and password are mandatory');
	define('LOGIN_MSG_02',						'Invalid mobile number or password');
	define('LOGIN_MSG_03',						'Username and password are mandatory');
	define('LOGIN_MSG_04',						'Invalid username or password');
		
	define('USER_TYPE',							'User Type');
	define('ADMIN',								'Admin');
	define('PRIVACY_POLICY',					'Privacy Policy');
	define('SMS_ADVERTISING',					'Targeted SMS Marketing');
	define('WHO_CAN_USE_THIS',					'Who can use this');
	define('ENTER_NAME',						'Enter name');
	define('ENTER_SUBJECT',						'Enter Subject');
	define('ENTER_MESSAGE',						'Enter message');
	define('ENTER_CAPTCHA_CODE',				'Enter captcha code');
	define('CONTACT_US_PREINFO',				'<p>Bongo Live! is committed to providing a great customer service experience. Please contact us if you are interested in our services or would like us to address an issue. We would love to hear from you.</p><p>Email: <a href="mailto:contact@bongolive.co.tz">contact@bongolive.co.tz</a></p><p>Mobile: 0688 121252 (Airtel), 0765 121252 (Voda)</p><p>Sales Office: 7th Floor, Ibrahim Manzil, Above Kearsly Travels, Zanaki St, Dar es Salaam</p><p> Operations Office: Plot 17/54 Uhuru St, Opp Moodys, Dar es Salaam</p>');
	define('NAME',								'Name');
	define('NAME_',								'Name (Jina)');
	define('EMAIL_ADDRESS',						'E-Mail Address');
	define('BUSINESS_NAME',						'Business Name');
	define('SUBJECT',							'Subject');
	define('MESSAGE',							'Message');
	define('CAPTCHA_CODE',						'CAPTCHA Code');
	define('ENTER_NEW_MOBILE_NUMBER',			'Enter new mobile number');
	define('ENTER_MOBILE_VERIFICATION_CODE',	'Enter mobile verification code');
	define('ENTER_MOBILE_CONFIRMATION_CODE',	'Enter mobile confirmation code');
	define('OLD_PASSWORD',						'Old Password');
	define('NEW_PASSWORD',						'New Password');
	define('CONFIRM_PASSWORD',					'Confirm Password');
	define('CONFIRM_PASSWORD_',					'Confirm Password (Thibitisha Neno la Siri)');
	define('CHANGE_PASSWORD',					'Change Password');
	define('CHANGE_MOBILE',						'Change Mobile');
	define('AGE_RANGE',							'Age Range');
	define('AGE_RANGE_',						'Age Range (Umri)');
	define('EDUCATION',							'Education');
	define('EDUCATION_',						'Education (Elimu)');
	define('EMPLOYMENT',						'Employment');
	define('EMPLOYMENT_',						'Employment (Ajira)');
	define('OCCUPATION',						'Occupation');
	define('OCCUPATION_',						'Occupation (Ujuzi)');
	define('COUNTRY',							'Country');
	define('LOCATION',							'Location');
	define('LOCATION_',							'Location (Mahali)');
	define('CITY',							'City');
	define('AREA',							'Area /Locality');
	define('OPTIONAL_ONE',						'Optional One');
	define('OPTIONAL_TWO',						'Optional Two');
	define('BIRTH_DATE',						'Date of birth');
	define('INTERESTS',							'Interests');
	define('INTERESTS_',						'Interests (Uyapendayo)');
	define('MALE',								'Male');
	define('FEMALE',							'Female');
	define('ACCEPT_BONGOLIVE',					'I Accept the Bongo Live!');
	define('MOBILE_VERIFICATION',				'Moblie phone number verification');
	define('ENTER_VERIFICATION_CODE',			'Enter verification code sent to you via sms');
	
	define('SELECT_ANY_GROUP',					'Select any Group');
	define('FIRST_NAME_REQUIRED',				'First name is required');
	
	define('SURE_DELETE_GROUP',					'Are you sure you want to delete this group?');
	define('SELECT_GROUP',						'Select Group');
	define('BROADCASTER_ID',					'Broadcaster-ID');
	define('BROADCASTER_APIKEY',				'API Key');
	define('BROADCASTER_WEBSITE',				'Website');
	define('ACCOUNT_TYPE',						'Account type');
	define('INDIVIDUAL',						'Individual');
	define('ORGANIZATION',						'Organisation');
	define('REPRESENTATIVE_NAME',				'Representative Name');
	define('BROADCASTER_SIGN_UP',				'Broadcaster Sign Up');
	/* Common variables_BEGIN */
	define('ENTER_TEXT_MASSAGE_ONLY',					"Enter plaint text massage only. Unicode is disabled now.");