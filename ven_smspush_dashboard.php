<?php
/****************************************************************************************************************
*	File : ven_smspush_dashboard.php
*	Purpose: landing page for vendors
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//updating credit
	$remoteCredit=getRemoteCreditBalance($_SESSION['user']['id']);

	$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit);
	
	$smarty->display("vendor/ven_smspush_dashboard.tpl");

	include 'footer.php';
?>