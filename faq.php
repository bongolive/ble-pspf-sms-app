<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('faq.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/faq.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/faq.tpl');
		}
		else
		{
			$smarty->display('faq.tpl');
		}
	}
	
	include('footer.php');
?>