<?php
/****************************************************************************************************************
*	File : ven_dashboard.php
*	Purpose: landing page for vendors
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'vendors/vendor.class.php');
	include(MODEL.'user.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objVendor = new vendor();
	
	$objVendor->userId = $_SESSION['user']['id'];
	$arrCreditReminder= $objVendor->getVendorCreditBalance();
	$smarty->assign('arrCreditReminder',$arrCreditReminder);

	//print_r($_REQUEST);
	switch($_REQUEST['action']){
	
		case'edit':
			$arrVendorDetail = $objVendor-> getVendorDetail();
			$smarty->assign('accType',$arrVendorDetail['vendor_type']);
			if(isset($_REQUEST['error'])){
				$smarty->assign("error",$_REQUEST['error']);
				$smarty->assign("url",trim($_REQUEST['url']));
			}
			
			break;
		case'update':
			$objUser = new user();
			$objUser->userId			= $_SESSION['user']['id'];
			$objUser->name				= trim($_REQUEST['name']);
			$objUser->email				= trim($_REQUEST['email']);
			$objUser->url				= $_REQUEST['url'];
			$objUser->mobNo				= $_REQUEST['mob_no'];
			$objUser->phone				= $_REQUEST['phone'];
			$objUser->organisation		= $_REQUEST['organisation'];
			$objUser->organisationCatId	= $_REQUEST['organisationCat'];
			$objUser->postalAddress		= $_REQUEST['postalAddress'];
			$objUser->physicalAddress	= $_REQUEST['physicalAddress'];
			$objUser->locationId		= $_REQUEST['location'];
			$objUser->peopleRangeId		= $_REQUEST['peopleRange'];
			$objUser->payment_method	= $_REQUEST['payment_method'];
			$objUser->broadcast_reminder= $_REQUEST['broadcast_reminder'];
			$objUser->support_email= $_REQUEST['support_email'];
			$objUser->support_phone= $_REQUEST['support_phone'];
			$objUser->reseller_url= $_REQUEST['reseller_url'];
			
			if(isset($_FILES['file']) && $_FILES['file']!=''){
				$f_detail = pathinfo($_FILES['file']['name']);
				$logoUrl="templates/assets/logo/".$_SESSION['user']['id'].".".$f_detail['extension'];
				//$objUser->logo		= $_SESSION['user']['id'].".".$f_detail['extension'];
				$objUser->logo		= $logoUrl;
			}else{
				$objUser->logo		= $_REQUEST['oldLogo'];
			}
			
			
			if($_REQUEST['SMSReminder'] =='1'){
				$objUser->sms_reminder		    = 1;
			}else{
				$objUser->sms_reminder		    = 0;
			}
			
			if($_REQUEST['EmailReminder'] =='1'){
				$objUser->email_reminder		    = 1;
			}else{
				$objUser->email_reminder		    = 0;
			}
			
			if($_REQUEST['Credit_Level'] !=''){
				$objUser->credit_level		    = $_REQUEST['Credit_Level'];
			}else{
				$objUser->credit_level		    = 25;
			}

			if(trim($_REQUEST['url']) ==''){
				$objUser->updateVenProfile();
				$objUser->VenderSMSEmailReminder();
				if(isset($_FILES['file']) && $_FILES['file']!=''){
					move_uploaded_file($_FILES['file']['tmp_name'],$logoUrl);
				}
				$msg = PROFILE_UPDATED;
				$action = "";
				$arrVendorDetail = $objVendor-> getVendorDetail();
				$_SESSION['user']['name'] = $arrVendorDetail['name'];
				//header("location: ven_profile.php");
			}else{
				if($objUser->checkVenDuplicateUrl()){
					$error="?error=1&action=edit&url=".$_REQUEST['url'];
					$error=$current_url.$error;
					header("location: ".$error);
				}else if($objUser->updateVenProfile()){
					$objUser->VenderSMSEmailReminder();
					if(isset($_FILES['file']) && $_FILES['file']!=''){
						move_uploaded_file($_FILES['file']['tmp_name'],$logoUrl);
					}
					$msg = PROFILE_UPDATED;
					$action = "";
					$arrVendorDetail = $objVendor-> getVendorDetail();
					$_SESSION['user']['name'] = $arrVendorDetail['name'];
					//header("location: ven_profile.php");
				} 
			}
			
			break;
			default:
				$arrVendorDetail = $objVendor-> getVendorDetail();
	}


	$smarty->assign("arrLocation",getArray('locations','id','location',' order by location '));
	$smarty->assign("arrOrganisationCat",getArray('organisations','id','organisation',' order by organisation ', $_SESSION['lang']));
	$smarty->assign("arrPeopleRanges",getArray('people_ranges','id','people_range'));
	$smarty->assign('action',$_REQUEST['action']);
	$smarty->assign('msg',$msg);
	$smarty->assign("arrVendorDetail",$arrVendorDetail);
	$smarty->assign("mobMaxlength",$project_vars['mob_no_length']);
	$smarty->assign("phoneMaxlength",$project_vars['phone_length']);

	$smarty->display("vendor/ven_profile.tpl");
	
	include 'footer.php';
?>