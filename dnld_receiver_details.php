<?php
	/********************************************
	*	File	: adm_credits_request.php				*
	*	Purpose	: Admin crdit  management for vendor *
	*	Author	: Akhilesh						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	//include('admin_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'sms/sms.class.php');
	$loginValidation = check_session($_SESSION['user'],'ADM','admin/index.php');
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='dnld'){
		if($_REQUEST['receiverType']=='SUB'){
			$objSubscribers = new user();
			$objSubscribers->ageRangeId		= $_SESSION['adm_sms']['SUB']['ageRange'];
			$objSubscribers->gender			= $_SESSION['adm_sms']['SUB']['gender'];
			$objSubscribers->educationId	= $_SESSION['adm_sms']['SUB']['education'];
			$objSubscribers->employmentId	= $_SESSION['adm_sms']['SUB']['employment'];
			$objSubscribers->locationId		= $_SESSION['adm_sms']['SUB']['location'];
			$objSubscribers->occupationId	= $_SESSION['adm_sms']['SUB']['occupation'];
			$objSubscribers->categoryId		= $_SESSION['adm_sms']['SUB']['category'];
			$arrUserDetails = $objSubscribers->getSubUserId();
		}elseif($_REQUEST['receiverType']=='VEN'){
			$objVendors = new user();
			$objVendors->vendorType			= $_SESSION['adm_sms']['VEN']['vendorType'];
			$objVendors->organisationCatId	= $_SESSION['adm_sms']['VEN']['organisation'];
			$objVendors->peopleRangeId		= $_SESSION['adm_sms']['VEN']['people'];
			$objVendors->locationId			= $_SESSION['adm_sms']['VEN']['location'];
			$arrUserDetails = $objVendors->getVenUserId();	
		}
		/*
		$contents = "";
		$contents = "Mobile\t Name\t Email \t  \n";
		
		foreach($arrUserDetails as $value){
			
			$contents.=(isset($value['name'])?$value['name']."\t":"\t");
			$contents.=(isset($value['mob_no'])?$value['mob_no']."\t":"\t");
			$contents.=(isset($value['email'])?$value['email']."\t":"\t");
			$contents.= "\n"; 
	
		}

	
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		//header("Content-Length: " . filesize($dirPath.$fileName));
		header('Content-type: application/ms-excel');
		header("Content-Disposition: attachment; filename=dnld_receiver_".time().".xls");
		ob_clean();
		flush();
		echo $contents;
		exit;
		*/
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header('Content-type: application/ms-excel');
		header("Content-Disposition: attachment;filename= dnld_receiver_".time().".xls"); 
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();
		xlsWriteLabel(0,0,"Mobile");
		xlsWriteLabel(0,1,"Name");
		xlsWriteLabel(0,2,"Email");

		$xlsRow = 1;
		if(is_array($arrUserDetails) && count($arrUserDetails)>0){
			foreach($arrUserDetails as $value){
					
				++$i;

				xlsWriteNumber($xlsRow,0,$value['mob_no']);	
				xlsWriteLabel($xlsRow,1,$value['name']);
				xlsWriteLabel($xlsRow,2,$value['email']);
				 
				$xlsRow++;
			}
		}
		xlsEOF();
		exit();	
	}

?>