<?php
	include_once('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	
	$lang_to_set = trim($_GET['lang']);
	
	if(isset($lang_to_set) && (mb_strlen($lang_to_set, "utf-8") > 0))
	{
		if(isset($lang_to_set,$project_vars['site_langs'][$lang_to_set]))
		{
			$_SESSION['lang'] = $lang_to_set;
		}
	}
	else
	{
		$_SESSION['lang'] = $project_vars['site_lang_default'];
	}
	
	header("Location: ".$_SERVER["HTTP_REFERER"]);
?>