<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: sending Qiuck sms for vendors
*	Author : Leonard nyirenda
****************************************************************************************************
*/
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'user.class.php');
	include(MODEL.'sms/sms.class.php');
	include(MODEL.'sms/smstemplate.class.php');
	include(MODEL.'senderid/senderid.class.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(MODEL.'credit/credit.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	//SmartyPaginate::connect();
	
	$selfUrl = BASE_URL.'ven_smspush_quick.php';

	$objUser= new user();
	$objUser->userId = $_SESSION['user']['id'];

	$objSenderid = new senderid();
	$objSenderid-> login_id = $_SESSION['user']['id'];
	$arrSenderid = $objSenderid-> getActiveSenderid();

	$objSms = new sms();
	$objSms->userId = $_SESSION['user']['id'];

	$objAddbook = new addressbook();
	$objAddbook->userId = $_SESSION['user']['id'];
	//$arrAddressbook = $objAddbook->getAddressbookList($limit=false);
	
	$objsmstemplate=new smstemplate();
	$objsmstemplate->vendor_id=$_SESSION['user']['id'];
	$arrsmstemplate=$objsmstemplate->getvendorsmstemplatelist2();
	$mobileCount = 0;
	switch($_REQUEST['action']){
	
		case'smspush':
				if(isset($_REQUEST['recipients']) ){
					$arrRecp = explode("\n", $_REQUEST['recipients']);
					$i=0;
					 
					 $arrRecpF=array_filter($arrRecp);
					 foreach($arrRecpF as $resp){
							$value=trim($resp);
							if ($value == '')
    						{
        						unset($value);
							}else{
								$arrRecpPure[]=trim($objAddbook->CutMobileNumber($value));
							}
						$i++;
					}
					
					$arrRecipients = array_unique($arrRecpPure);
					$mobileCount=count(array_unique($arrRecipients));
				}
				
				//finding  sender type
				$senderType=$_REQUEST['senderid'];
				
				//updating credit only for parent id
				if($_SESSION['user']['parent_ven_id']==''){
					//(int) $remoteCredit = getRemoteCreditBalance();
					(int) $remoteCredit = 1000;//getRemoteCreditBalance();
					//(int) $resCredit = getTotalCreditOfAllVendors();
					(int) $resCredit = 1000;//getTotalCreditOfAllVendors();
					$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
				} 
				
				//checking credit
				$objCredit = new credit();
				$objCredit->userId = $_SESSION['user']['id'];
				$creditBalance = $objCredit->getCreditBalance();
				
				$str=str_replace(chr(13),'', $_REQUEST['textMessage']);	
				$smsCount = ceil(strlen($str)/160);
				$creditNeeded = $smsCount * $mobileCount;
				
				if($creditBalance < $creditNeeded){
					$msg = VEN_SMS_PUSH_MSG_02;
					$errorFlag = 1;
				}
				
				if($errorFlag == 0 ){
				//$smsCount = 1+floor(strlen($_REQUEST['textMessage']-1)/160);
				
					if(is_array($arrRecipients)){
						$_SESSION['smsQuick']['contacts'] = array_unique($arrRecipients);
					}
					$_SESSION['smsQuick']['mobileCount']	= cleanQuery($mobileCount);
					$_SESSION['smsQuick']['textMessage']	= cleanQuery($_REQUEST['textMessage']);
					$_SESSION['smsQuick']['senderid']	= cleanQuery($_REQUEST['senderid']);
					$_SESSION['smsQuick']['scheduleDate']= cleanQuery($_REQUEST['scheduleDate']);
					$_SESSION['smsQuick']['creditNeeded']= $creditNeeded;
					$_SESSION['smsQuick']['smsCount']	= $smsCount;
					if($_REQUEST['repeat2']!=''){
						$_SESSION['smsQuick']['repeat']= cleanQuery($_REQUEST['repeat2']);
						$_SESSION['smsQuick']['repeat_every']= cleanQuery($_REQUEST['repeat_every2']);
						$_SESSION['smsQuick']['repeat_days']= cleanQuery($_REQUEST['repeat_days2']);
						$_SESSION['smsQuick']['end_date']= cleanQuery($_REQUEST['end_date']);
					}
					
					foreach($_SESSION['smsQuick']['contacts'] as $mob){
						if(! phoneValidation($mob)){
							$MobError=1;
						}
					}

					
					if($MobError == 1){
						$smarty->assign('MobError',$MobError);
						$arrRecipients= implode("\n", $_SESSION['smsQuick']['contacts']);
						$smarty->assign('arrRecipients',$arrRecipients);
						$smarty->assign('textMessage',$_SESSION['smsQuick']['textMessage']);
						$smarty->assign('senderid',$_SESSION['smsQuick']['senderid']);
						$smarty->assign('scheduleDate',$_SESSION['smsQuick']['scheduleDate']);
					}else{
						$smarty->assign('arrRecipients',$arrRecipients);
						$smarty->assign('creditNeeded',$_SESSION['smsQuick']['creditNeeded']);
						$smarty->assign('smsCount',$_SESSION['smsQuick']['smsCount']);
						$smarty->assign('mobileCount',$_SESSION['smsQuick']['mobileCount']);
						$smarty->assign('creditBalance',$creditBalance);
						$smarty->assign('smsconf','smsconf');
					}
				}
		break;
		case'backTo1':
				$arrRecipients= implode("\n", $_SESSION['smsQuick']['contacts']);
				$smarty->assign('arrRecipients',$arrRecipients);
				$smarty->assign('textMessage',$_SESSION['smsQuick']['textMessage']);
				$smarty->assign('senderid',$_SESSION['smsQuick']['senderid']);
				$smarty->assign('scheduleDate',$_SESSION['smsQuick']['scheduleDate']);
			break;
			
			//Conferming the sms send
		case'smsconf':
				if($_SESSION['smsQuick']['creditNeeded'] > 0){
					$objSms->userId			= $_SESSION['user']['id'];
					$objSms->status			= 3;
					$textSms = str_replace('\r\n','', $_SESSION['smsQuick']['textMessage']);
					$textSms  = str_replace('\n\r','', $textSms);
					$objSms->textMassage	=$textSms;
					//$objSms->textMassage	= $_SESSION['smsQuick']['textMessage'];
					$objSms->senderID		= $_SESSION['smsQuick']['senderid'];
					$objSms->credit			= $_SESSION['smsQuick']['creditNeeded'];
					$objSms->massegeCount	= $_SESSION['smsQuick']['smsCount'];
					$objSms->massegeLength	= strlen($_SESSION['smsQuick']['textMessage']);
					$objSms->addressbookId	=	'QUICK';
					$objSms->contactId	=	'QUICK';
					$objSms->senderType		= 'VEN';
					
					if(isset($_SESSION['smsQuick']['scheduleDate']) && $_SESSION['smsQuick']['scheduleDate']!=''){
						$objSms->isScheduled	= 1;
						$objSms->scheduleTime	= getDateFormat($_SESSION['smsQuick']['scheduleDate']);
					}

					if($objSms->smsPush()){
						$smsID=$objSms->getSmsStoresID2();
						$objSms->smsStoreID=$smsID;
						$objSms->blockCredit();
						$objSms->status	= 0;

						 	 
						// getting sender name
						$objSenderid-> id = $_SESSION['smsQuick']['senderid'];
						$sendername=$objSenderid->requestSenderName();
						$objSms->senderName = $sendername;
						
						foreach ($_SESSION['smsQuick']['contacts'] as $key=> $value)
        				{  
							$objSms->smsStoreID=$smsID;
							$objSms->mob_no=trim($value);
							$objSms->insertIntoInboundMessage2();
						
						}
						
						
						//inserting to the reminder table reminder table
						if($_SESSION['smsQuick']['repeat'] !=''){
							if($_SESSION['smsQuick']['repeat_days'] =='' && $_SESSION['smsQuick']['repeat']!='WEEK'){
								$_SESSION['smsQuick']['repeat_days']="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
							}else if($_SESSION['smsQuick']['repeat_days'] =='' && $_SESSION['smsQuick']['repeat'] =='WEEK'){
								$sendDate	= date("Y-m-d H:i:s");// current date
								$weekday = date('l', strtotime($sendDate));
								$_SESSION['smsQuick']['repeat_days'] = $weekday;
							}
							
							if(isset($_SESSION['smsQuick']['scheduleDate']) && $_SESSION['smsQuick']['scheduleDate']!=''){
								$sendDate	= getDateFormat($_SESSION['smsQuick']['scheduleDate']);
							}else{
								$sendDate	= date('m/d/Y H:i:s');// current date
							}
							
							if($_SESSION['smsQuick']['end_date']==''){
								// date not specified so we make default after 10 years;
								$currentDate = date("m/d/Y H:i:s");// current date
								$dateTenYearAdded = strtotime(date("m/d/Y H:i:s", strtotime($currentDate)) . " +10 year");
								$_SESSION['smsQuick']['end_date'] = date('m/d/Y H:i:s', $dateTenYearAdded);
							}
							
							
							$objSms->scheduleTime = getDateFormat($sendDate);
							$objSms->repeat = $_SESSION['smsQuick']['repeat'];
							$objSms->repeat_every = $_SESSION['smsQuick']['repeat_every'];
							$objSms->repeat_days = $_SESSION['smsQuick']['repeat_days'];
							$objSms->reminder_type = "sms";
							$objSms->contactId = "";
							$objSms->end_date = getDateFormat($_SESSION['smsQuick']['end_date']);
							$objSms->insertReminder();
							
							//Finding Next send date
							$objSms->updateNextSendDate();
							
						}
						
						
						$objSms->UpdateIsProccessed2();
						$objSms->reminderSent=0;
						$objSms->updateBroadcastReminder();
						
						
						
						
						if(isset($_SESSION['smsQuick']['scheduleDate']) && $_SESSION['smsQuick']['scheduleDate']!=''){

							$msg = VEN_SMS_PUSH_MSG_03;

						}else{
							$msg = VEN_SMS_PUSH_MSG_04;
						}
						unset($_SESSION['smsQuick']);
						
					}
				}else{
					$msg = VEN_SMS_PUSH_MSG_05;
					$errorFlag = 1;
					$arrRecipients= implode("\n", $_SESSION['smsQuick']['contacts']);
					$smarty->assign('arrRecipients',$arrRecipients);
					$smarty->assign('textMessage',$_SESSION['smsQuick']['textMessage']);
					$smarty->assign('senderid',$_SESSION['smsQuick']['senderid']);
				}
		break;
	}
	
	$objCredit = new credit();
	$objCredit->userId = $_SESSION['user']['id'];
	$creditBalance = $objCredit->getCreditBalance();
	
	//End of case ####
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('venCreditBalance',$creditBalance);
	$smarty->assign('msg',$msg);
	$smarty->assign('repeat',$_REQUEST['repeat2']);
	$smarty->assign('repeat_every',$_REQUEST['repeat_every2']);
	$smarty->assign('repreat_days',$_REQUEST['repeat_days2']);
	$smarty->assign('end_date',$_REQUEST['end_date']);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('arrsmstemplate',$arrsmstemplate);
	$smarty->assign('arrSenderid',$arrSenderid);
	$smarty->assign('currentYear',date("Y")+10);
	$smarty->assign('mob_no_length',$project_vars['mob_no_length']);
	$smarty->assign('fname_length',$project_vars['fname_length']);
	$smarty->assign('Iname_length',$project_vars['lname_length']);
	$smarty->display("vendor/ven_smspush_quick.tpl");
	
	include 'footer.php';
?>