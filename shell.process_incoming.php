<?php
/********************************************
	*	File	: shell.process_incoming.php				*
	*	Purpose	: Fetch requested information from pspf database and update the text in log table	*
	*	Author	: leonard nyirenda						*
*********************************************/
	include_once ('bootstrap.php');
	include_once(LIB_DIR . "shell.inc.php");
	include(MODEL.'primkeys.class.php');
	include(MODEL.'secondkeys.class.php');
	include(MODEL.'incoming.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$debug = false;
	// Infinite loop starts
while (true) {
	//	//echo "<pre>Now started...................\n";
	/* Specify the server and connection string attributes. */
	$serverName = "192.168.0.35";

	/* Get UID and PWD from application-specific files.  */
	$uid = "bulksms";
	$pwd = "bulksms33";
	$connectionInfo = array( "UID"=>$uid,"PWD"=>$pwd,"Database"=>"PENSION");

	$objPrimaryKey = new primkeys();
	$objSecondKeyword = new secondkeys();

	$obj = new incoming();
	$arrIncoming = $obj->getAllIncomingPending();
	
	if($arrIncoming!=false){ 
		//echo "<pre>Data found Now processing....\n";
		foreach($arrIncoming as $row){
			
			//=======================Replacing all unwanted text from sms ===============
			$txtMessage = strtolower($row['message']);
			$txtMessage = str_replace("usajili","",$txtMessage);
			$txtMessage = str_replace("jibu","",$txtMessage);
			$txtMessage = str_replace("historia","",$txtMessage);
			$txtMessage = str_replace("ya","",$txtMessage);
			$txtMessage = str_replace("fuatilia","",$txtMessage);
			$txtMessage = str_replace("na","",$txtMessage);
			$txtMessage = str_replace("checkno","",$txtMessage);
			$txtMessage = str_replace("cn","",$txtMessage);
			$txtMessage = str_replace("taarifa","",$txtMessage);
			$txtMessage = str_replace("naomba","",$txtMessage);
			$txtMessage = str_replace("yangu","",$txtMessage);
			$txtMessage = str_replace("idadi","",$txtMessage);
			$txtMessage = str_replace("tuma","",$txtMessage);
			$txtMessage = str_replace("lini","",$txtMessage);
			$txtMessage = str_replace("c/no","",$txtMessage);
			$txtMessage = str_replace("chk/no","",$txtMessage);
			
			//================Checking and formating text message=========================
			$string = preg_replace('/\s+/', ' ', $txtMessage); 
			$mangungo = preg_replace("/[^A-Za-z0-9 ]/", ' ', $string);
			$clean = preg_replace('/\s+/', ' ', $mangungo);
			$actualString = trim($clean);
			
			
			
			$arrsms = parsesms($actualString);
			$arrsms[1] = mysql_real_escape_string($arrsms[1]);
			$error=0;
			$system_error=0;
			$errorMessage="";
			$checkno='';
			
			//keyword from user is not empty
			if(count($arrsms)>0){
				//search in the database to check if primary keyword is valid
				if($objPrimaryKey->checkPrimKey($arrsms[0]) ==false){
					//primary key word exist
					$error=0;
				}else if($objSecondKeyword->checkSekondKey($arrsms[0])==false){
					// not found in primary keyword now search into second keyword
					//Get valid primary keyword from secondary keyword
					$value	=	$objSecondKeyword->getKeywordFrom2ndKeyword($arrsms[0]);
					$arrsms[0]=$value;
					$error=0;
				}else{
					//Key word not exist
					$errorMsg = "Ujumbe uliotuma si sahihi. Tuma neno 'PSPF' kwenda namba 15357 kupata orodha ya huduma zetu.";
					$error=1;
				}
	
			}else{
				//error message if keyword is empty
				$errorMsg = "Umetuma Ujumbe pasipo kuandika kitu chochote. Tuma neno 'PSPF' kwenda namba 15357 kupata orodha ya huduma zetu.";
			}
	

			/* Connect To pspf using SQL Server Authentication. */
			$connPSPF = sqlsrv_connect( $serverName, $connectionInfo);
			if( $connPSPF === false )
			{
				$errorMessage="Connecting to PENSION database fail";
				$errorMsg = "Ujumbe wako unashughulikiwa. Tafadhari endelea kusubiri.";
				$error=1;
				$system_error=1;
			}
	
	
			//code to fetch Data from pspf goes here
			if($error==1){
				$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
				$obj->status ='response to be sent';
				$obj->system_error =cleanQuery($system_error);
				$obj->systen_error_message=cleanQuery($errorMessage);
				$obj->num_status =cleanQuery($row['num_status'])+1;	
				$obj->outbound_response =cleanQuery($errorMsg);	
				$obj->keyword =cleanQuery($arrsms[0]);
				$obj->checkno = $checkno;	
				$obj->updateIncoming();	
			}else{

				//No error occur so feth data from pspf database
				switch($arrsms[0]){
					case 'hakiki':
						$sql2="select * from members where checkno='".$arrsms[1]."'";
						$result2=sqlsrv_query($connPSPF, $sql2);
			
						if ($result2) {
							$rows = sqlsrv_has_rows( $result2 );
							if($rows === true){
								while($row2=sqlsrv_fetch_array($result2)){
									$msg="Ndugu ".$row2['othernames']." ".$row2['lastname']." Umesajiliwa na PSPF. Tarehe ya kuanza kazi ni ". date_format(date_create(substr($row2['datehired']->format('c'),0,10)), 'd/m/Y').". Checkno ".$row2['checkno'];
									$checkno=trim($arrsms[1]);
								}	
							}else {
								$msg="PSPF haina mwanachama mwenye checkno ".$arrsms[1];
							}
						}else{
							$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea ujumbe uliyo na taarifa ulizoomba.";
							$errorMessage="Fail To fetch Data from Member table";
							$system_error=1;
						}
			
			
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='hakiki';
						$obj->checkno = $checkno;	
						$obj->updateIncoming();	
					break;
		
					case 'mchango':
						$sql2="select othernames,lastname,checkno from members where checkno='".trim($arrsms[1])."'";
						$result2=sqlsrv_query($connPSPF, $sql2);
						$msg2='';
						if ($result2) {
							$rows = sqlsrv_has_rows( $result2 );
							if($rows === true){
								while($row2=sqlsrv_fetch_array($result2)){
									$msg1="Michango ".substr($row2['othernames']." ".$row2['lastname'], 0, 18)." ";
									$checkno=trim($arrsms[1]);
								}
									$sql2="select top 3 paydate,member_amount,employer_amount from TRANSACTIONS where checkno='".trim($arrsms[1])."' ORDER BY paydate DESC";
									$result2=sqlsrv_query($connPSPF, $sql2);
			
									if ($result2) {
										$rows = sqlsrv_has_rows( $result2 );
										if($rows === true){
											while($row2=sqlsrv_fetch_array($result2)){
												$msg2 .=date_format(date_create(substr($row2['paydate']->format('c'),0,10)), 'd/m/Y')." ".number_format((float)$row2['member_amount'], 0, '.', '')." Tsh ";
											}
					
											//finding total of contribution
											$sql3="select checkno,sum(employer_amount) as employer_total,sum(member_amount) as member_total from TRANSACTIONS Group By checkno having checkno='".trim($arrsms[1])."'";
											$result3=sqlsrv_query($connPSPF, $sql3);
											if ($result3) {
												$rowz = sqlsrv_has_rows( $result3 );
												if($rowz === true){
													while($row3=sqlsrv_fetch_array($result3)){
														$msg=$msg1.$msg2."Jumla uliyolipa ".number_format((float)$row3['member_total'], 2, '.', '')." Tsh. Jumla mwajiri ".number_format((float)$row3['employer_total'], 2, '.', '')." Tsh";
													}	
												}else {
													$msg="Hakuna mchango kwa mwanachama mwenye checkno ".$arrsms[1];
												}
											}else{
												$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
												$errorMessage="Fail to fetch data from table TRANSACTIONS";
												$system_error=1;
											}
										}else {
											$msg="Hakuna mchango kwa mwanachama mwenye checkno ".$arrsms[1];
										}
									}else{
										$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
										$errorMessage="Fail to fetch data from table TRANSACTIONS";
										$system_error=1;
									}	
							}else {
								$msg="PSPF Haina mwanachama mwenye  checkno ".$arrsms[1];
							}
						}else{
							$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
							$errorMessage="Fail to fetch data from member table";
							$system_error=1;
						}
			
						$msg = iconv('UTF-8', 'ASCII', $msg);
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='mchango';
						$obj->checkno = $checkno;	
						$obj->updateIncoming();	
					break;
		
					case 'pension':
							$sql2="select top 1 checkno,Payee from payroll_run where checkno='".trim($arrsms[1])."'";
							$result2=sqlsrv_query($connPSPF, $sql2);
			
							if ($result2) {
								$rows = sqlsrv_has_rows( $result2 );
								if($rows === true){
									while($row2=sqlsrv_fetch_array($result2)){
										$msg1="Pensheni ya ".$row2['Payee']." ";
										$checkno=trim($arrsms[1]);
									}
										$sql2="SELECT top 3 PP.bdate,PR.accountno,PR.amount FROM payroll_run AS PR LEFT JOIN payroll_proc AS PP ON PP.batch_id=PR.batch_id WHERE PR.checkno='".trim($arrsms[1])."' ORDER BY PP.bdate DESC";
										$result2=sqlsrv_query($connPSPF, $sql2);
			
										if ($result2) {
											$rows = sqlsrv_has_rows( $result2 );
											$msg2='';
											if($rows === true){
												while($row2=sqlsrv_fetch_array($result2)){
													$msg2 .=date_format(date_create(substr($row2['bdate']->format('c'),0,10)), 'd/m/Y')." ".number_format((float)$row2['amount'], 2, '.', '')."Tsh ";
												}
					
												//finding total of contribution
												$sql3="select checkno,sum(amount) as total_amount from payroll_run Group By checkno having checkno='".trim($arrsms[1])."'";
												$result3=sqlsrv_query($connPSPF, $sql3);
												if ($result3) {
													$rowz = sqlsrv_has_rows( $result3 );
													if($rowz === true){
														while($row3=sqlsrv_fetch_array($result3)){
															$msg=$msg1.$msg2." Jumla ya Pensioni lipwa ".number_format((float)$row3['total_amount'], 2, '.', '')." Tsh";
														}	
													}else {
														$msg="Hakuna malipo ya pension yaliyolipwa na PSPF kwa mwanachama mwenye checkno ".$arrsms[1];
													}
												}else{
													$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
													$errorMessage="Fail to fetch data from payroll_run table";
													$system_error=1;
												}
											}else {
												$msg="Hakuna malipo ya pension yaliyolipwa na PSPF kwa mwanachama mwenye  checkno ".$arrsms[1];
											}
										}else{
											$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
											$errorMessage="Fail to fetch data from payroll_proc table";
											$system_error=1;
										}	
								}else {
									$msg="Hakuna malipo ya pension yaliyolipwa na PSPF kwa mwanachama mwenye checkno ".$arrsms[1];
								}
							}else{
								$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
								$errorMessage="Fail to fetch data from payroll_run table";
								$system_error=1;
							}

						
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='pension';
						$obj->checkno = $checkno;	
						$obj->updateIncoming();	
					break;
		
					case 'benefit':
						$sql2="select * from pensioners where checkno='".trim($arrsms[1])."'";
						$result2=sqlsrv_query($connPSPF, $sql2);
						$checkno=trim($arrsms[1]);
						if ($result2) {
							$rows = sqlsrv_has_rows( $result2 );
							if($rows === true){
								while($row2=sqlsrv_fetch_array($result2)){
									if($row2['GratPaid']==0){
										//Benefit is in processing stage
										$msg="Malipo ya Mafao ya mwanachama mwenye checkno ".$arrsms[1]." Yanashughulikiwa.";
									}else if($row2['GratPaid']==1 && $row2['chequeno'] !=''){
										$msg="Malipo ya Mafao  ya mwanachama mwenye checkno ".$arrsms[1]." Yamelipwa. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neno 'pension' ikifuatiwa na checkno yako kwenda namba 15357";
									}else if($row2['GratPaid']==1 && $row2['exit_code'] !='death'){
											$sql3="select * from dependants where checkno='".trim($arrsms[1])."'";
											$result3=sqlsrv_query($connPSPF, $sql3);
											if ($result3) {
												$rowz = sqlsrv_has_rows( $result3 );
												if($rowz === true){
													while($row3=sqlsrv_fetch_array($result3)){
														if($row3['cheque_no'] !=''){
															$msg="Malipo ya Mafao  ya mwanachama mwenye checkno ".$arrsms[1]." Yamelipwa. Kujua a/c, tarehe na kiasi kilicholipwa  tuma neno 'pension' ikifuatiwa na checkno yako kwenda namba 15357";
														}else{
															//The benefit is in advanced processing stage
															$msg="Malipo Mafao ya mwanachama mwenye checkno ".$arrsms[1]." yapo katika hatua za mwisho za ulipwaji.";
														}
													}	
												}else {
													//No dependant details found
													$msg="Hakuna taarifa za ndugu tegemezi kwa mwanachama wa PSPF mwenye  checkno ".$arrsms[1];
												}
											}else{
												$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
												$errorMessage="Fail to fetch data from dependants table";
												$system_error=1;
											}
									}else if($row2['GratPaid']==1){
										//The benefit is in advanced processing stage.
										$msg="Malipo ya Mafao ya mwanachama mwenye checkno ".$arrsms[1]." yapo katika hatua za mwisho za ulipwaji.";
									}else{
										//Benefit is in processing stage
										$msg="Malipo ya Mafao ya mwanachama mwenye checkno ".$arrsms[1]." Yanashughulikiwa.";
									}
								}	
							}else {
								//There is no benefit information for a member with checkno ".$arrsms[1]
								$msg="Hakuna taarifa za Mafao kwa mwanachama wa PSPF mwenye checkno ".$arrsms[1];
							}
						}else{
							$msg="Samahani kuna tatizo la kimtandao Tafadhari endelea kusubiri. Punde utapokea sms iliyo na taarifa zako.";
							$errorMessage="Fail to fetch data from pensioners table";
							$system_error=1;
						}
			
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='benefit';
						$obj->checkno = $checkno;	
						$obj->updateIncoming();	
					break;
		
					case 'menu':
						$msg = "1. Hakiki Usajili 2. Historia ya Mchango 3. Historia Pensheni 4. Fuatilia Mafao 5. Jisajili\n Tuma namba ya huduma, acha nafasi kisha CheckNo kwenda 15357";
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='menu';
						$obj->checkno = $checkno;	
						$obj->updateIncoming();	
					break;
					
					case 'sajili':
						$msg="Umesajiliwa na PSPF. Namba yako ya uanachama ya muda  ni ".generatedefaultmembernumber($row['sms_in_id']);
						//updating text_in_log table
						$obj->sms_in_id =cleanQuery($row['sms_in_id']);	
						$obj->status ='response to be sent';
						$obj->system_error =cleanQuery($system_error);
						$obj->systen_error_message=cleanQuery($errorMessage);
						$obj->num_status =cleanQuery($row['num_status'])+1;	
						$obj->outbound_response =cleanQuery($msg);	
						$obj->keyword ='sajili';	
						$obj->updateIncoming();	
					break;
		
				}
			}	
			//sqlsrv_free_stmt($result2);	
			unset($arrsms);	

		}//closeing loop

	}else{
		//updating FAIL sms status
		$arrFailSms = $obj->getAllFailIncomingSms();
		if($arrFailSms!=false){
			foreach($arrFailSms as $row){
				$obj->sms_in_id =$row['sms_in_id'];	
				$obj->updateIncomingStatus();
			}
		}
		//echo "<pre>No data found now going to sleep....\n\n";
		
	}
}
include_once(LIB_DIR . "close.php");
?>