<?php
	
	/********************************************
	*	File	: vendor_reminders.php		*
	*	Purpose	: Vendor SMS Reminders Management		*
	*	Author	: Leonard Nyirenda						*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'reminder.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');
	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$selfUrl = BASE_URL.'ven_reminders.php';
	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);
	
	$objReminder=new reminder();
	$objReminder->userId = $_SESSION['user']['id'];
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	switch($action){
		case'search':
			// setting pagin url			
			$pagingUrl = $selfUrl.'?userId='.cleanQuery($_SESSION['user']['id']);
			$pagingUrl.= '&reminder_type='.cleanQuery($_REQUEST['reminder_type']); 
			$pagingUrl.= '&startDate='.cleanQuery($_REQUEST['startDate']);
			$pagingUrl.= '&startDate1='.cleanQuery($_REQUEST['startDate1']); 
			$pagingUrl.= '&endDate='.cleanQuery($_REQUEST['endDate']); 
			$pagingUrl.= '&endDate1='.cleanQuery($_REQUEST['endDate1']);
			$pagingUrl.= '&job_name='.cleanQuery($_REQUEST['job_name']);
			$pagingUrl.= '&status='.cleanQuery($_REQUEST['status']);
			SmartyPaginate::setUrl($pagingUrl);
			// stting search param
			$objReminder->userId = $_SESSION['user']['id'];
			$objReminder->reminder_type = cleanQuery($_REQUEST['reminder_type']);
			$objReminder->startDate	= getDateFormat($_REQUEST['startDate']); 
			$objReminder->startDate1= getDateFormat($_REQUEST['startdate1']);  
			$objReminder->endDate	= getDateFormat($_REQUEST['endDate']); 
			$objReminder->endDate1	= getDateFormat($_REQUEST['endDate1']);
			$objReminder->jobName	= cleanQuery($_REQUEST['job_name']); 
			$objReminder->status	= cleanQuery($_REQUEST['status']); 
			// getting search sms
			$arrReminders =  $objReminder->getReminderList();
		break;
		
		case'edit':
			$action = "update";
			$objReminder->reminderId = cleanQuery($_REQUEST['id']);	
			$arrReminders =  $objReminder->getReminderById();
			$arrRepeatDayz=explode(',',$arrReminders['repeat_days']);
			foreach($arrRepeatDayz as $value){
				$arrRepeatDays[$value]=$value;
			}
			
			//Finding the platform used to send sms
			if(strlen($arrReminders['addressbook_id'])>10){
				$platform="GROUP";
			}else{
				$platform=$arrReminders['addressbook_id'];
			}
			
			if($platform==="GROUP" && $arrReminders['addressbook_id']!=''){
				$objReminder->addressbookId=$arrReminders['addressbook_id'];
				$arrContacts=$objReminder->getAddressbookNames();
			}else if($platform==="FILE" && $arrReminders['reminder_type']==='birthday'){
				$objReminder->smsStoreId=$arrReminders['sms_store_id'];
				$objReminder->mobNo=$arrReminders['ss_contact_id'];
				$arrContacts=$objReminder->getInboundContactsByMobNo();
			}else{
				$objReminder->smsStoreId=$arrReminders['sms_store_id'];
				$arrContacts=$objReminder->getInboundContacts();
			}
			
			$smarty->assign("arrContacts",$arrContacts);
			$smarty->assign("arrRepeatDays",$arrRepeatDays);
		break;
		
		case 'update':
			$action = "search";
			if($_REQUEST['repeat']!='WEEK'){
				$repeatDays="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
			}else if(count($_REQUEST['repeat_days'])==0){
				$sendDate	= date("Y-m-d H:i:s");// current date
				$repeatDays = date('l', strtotime($sendDate));
			}else{
				$repeatDays = implode(',',$_REQUEST['repeat_days']);
			}
			
			$objReminder->reminderId =cleanQuery( $_REQUEST['id']);	
			$objReminder->repeat = cleanQuery($_REQUEST['repeat']);	
			$objReminder->repeat_every = cleanQuery($_REQUEST['repeat_every']);
			$objReminder->repeatDays=cleanQuery($repeatDays);
			$objReminder->sendDate = getDateFormat($_REQUEST['sendDate']);	
			$objReminder->endDate = getDateFormat($_REQUEST['editEndDate']);	
			$response=$objReminder->updateReminder();
			
			$objReminder=new reminder();
			$objReminder->userId = $_SESSION['user']['id'];
			$arrReminders =  $objReminder->getReminderList();
			if($response==true){
				$msg="Reminder updated successfully";
				$errorFlag=0;
			}else{
				$msg="Reminder not updated.";
				$errorFlag=1;
			}	
		break;
		case'delete':
			$action = "search";
			$objReminder->reminderId =cleanQuery( $_REQUEST['id']);	
			$response=$objReminder->deleteReminder();
			$objReminder=new reminder();
			$objReminder->userId = $_SESSION['user']['id'];
			$arrReminders =  $objReminder->getReminderList();
			if($response==true){
				$msg="Reminder updated successfully";
				$errorFlag=0;
			}else{
				$msg="Reminder not updated.";
				$errorFlag=1;
			}	
		break;
		
		default:
			$action = "search";	
			$arrReminders =  $objReminder->getReminderList();
	}
	$smarty->assign("msg",$msg);
	$smarty->assign("reminder_type",$_REQUEST['reminder_type']);
	$smarty->assign("startDate",getDateFormat($_REQUEST['startDate']));
	$smarty->assign("startDate1", getDateFormat($_REQUEST['startDate1']));
	$smarty->assign("endDate",getDateFormat($_REQUEST['endDate']));
	$smarty->assign("endDate1", getDateFormat($_REQUEST['endDate1']));
	$smarty->assign("job_name", $_REQUEST['job_name']);
	$smarty->assign("status", $_REQUEST['status']);
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("action",$action);
	$smarty->assign("arrReminders",$arrReminders);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_reminders.tpl");
	SmartyPaginate::disconnect();
	include 'footer.php';
?>
