<?php
/********************************************
	*	File	: sub_request.php				*
	*	Purpose	: add vendors  contacts from  blogspot,website or worldpress	*
	*	Purpose	: Send credit balance and Senderid to a	*
	*	Author	: leonard nyirenda						*
*********************************************/

include_once ('../bootstrap.php');
require_once(LIB_DIR.'inc.php');
require_once(MODEL.'user.class.php');
include_once(LIB_DIR.'smsProcessLib.php');
include(MODEL.'vendors/vendor.class.php');
include(MODEL.'addressbook/addressbook.class.php');

$name	 	= 	cleanQuery($_REQUEST['name']);
$url 		= 	cleanQuery($_REQUEST['url']);
$phone 		= 	cleanQuery($_REQUEST['mobile']);
$group 		= 	cleanQuery($_REQUEST['group']);
$Username	=	cleanQuery($_REQUEST['username']);
$Password	=	md5($_REQUEST['password']);
$Request	=	$_REQUEST['request'];

switch ($Request) {
		case	"sender":
			$objVendor = new vendor();
			$objVendor->username	=	$Username;
			$objVendor->password	=	$Password;
			$response	=	$objVendor->getVenSenderIDByUsernamePass();
			echo $response;
		break;
		
		case	"balance":
			$objVendor = new vendor();
			$objVendor->username	=	$Username;
			$objVendor->password	=	$Password;
			$response	=	$objVendor->getVenCreditBalanceByUsernameAndPass();
			echo $response;
		break;
		
		case	"subscribe_url":
			$objUser = new user();
			$objUser->mobNo = $phone;
			$objUser->url = $url;
			
			//Checking where request come from
			if(isset($_REQUEST['fileId'])){
				$fileID = cleanQuery($_REQUEST['fileId']);
			}

			if($name == '' || $phone == '' || $url ==''){
				if(isset($fileID)){ 
					header("location: plugins/iframe/subscribe_iframe.php?response=-3");
				}else{
					echo "-3";
				}
			}else if( ! $objUser->getVendorDetailIDfromUrl()){
				if(isset($fileID)){ 
					header("location: plugins/iframe/subscribe_iframe.php?response=0");
				}else{
					echo "0";
				}
			}else if($objUser->getVendorDetailIDfromUrl()){
				$userid = $objUser->getVendorDetailIDfromUrl();
				foreach ($userid as $key => $value){
					$VendorId=$value;
				}
				$objUser->userId = $VendorId;
				$objAddressbook= new addressbook();
				$objAddressbook->userId = $VendorId;
				$objAddressbook->fname = $name;
				$objAddressbook->mob_no = $phone;
				$objAddressbook->addressbook = $group;
				$error= "-2";
	
				if($objAddressbook->getAddressbookID()){
					// selecting user defined group
					$addid = $objAddressbook->getAddressbookID();
		
					$objAddressbook->addressbookId = $addid;
					foreach($addid as $addbookId){
						if(! $objAddressbook->CheckDuplicateContact($addbookId,$phone)){
							$error = 1;	
							$objAddressbook->addContact();
							$objAddressbook->updateCountAddressbook($addbookId);
						}else{
							$error = "-1";
						}
					}
			
			
					// get ID of default goup of the vendor
					$objAddressbook->addressbook = "Default";
					if($objAddressbook->getDefaultAddressbook()){
						$addrId=$objAddressbook->getDefaultAddressbook();
						$objAddressbook->addressbookId = $addrId;
						foreach($addrId as $addbookId){
							if(! $objAddressbook->CheckDuplicateContact($addbookId,$phone)){
								$objAddressbook->addContact();
								$objAddressbook->updateCountAddressbook($addbookId);
							}
						}
					} 
				}
		
		
				if($error == 1){
					if(isset($fileID)){ 
						header("location: plugins/iframe/subscribe_iframe.php?response=1");
					}else{
						echo "1";
					}
				}else if($error == "-1"){
					if(isset($fileID)){ 
						header("location: plugins/iframe/subscribe_iframe.php?response=-1");
					}else{
						echo "-1";
					}
				}else {
					if(isset($fileID)){ 
						header("location: plugins/iframe/subscribe_iframe.php?response=-2");
					}else{
						echo "-2";
					}
				}
		
			
			}else{
				echo "Website not found";
			}
		break;
		
		default:
			echo "Request does not exist";
		break;

}

?>