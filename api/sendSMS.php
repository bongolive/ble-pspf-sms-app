<?
/*
Process Flow:
1) Incoming sms received
2) Check if user is authorized
3) Check if Sender Name exists in system for that vendor_id and is active
4) Check if GSM number is in correct format
5) Remove 255 prefix from GSM number
6) Calculate number of messages (based on character count)
7) Check if user has enough credits
8) Insert dummy record into sms_stores table to record history (credit column = message count)
9) Insert record into inbound_messages table
10) Return appropriate error/success message
*/

	include_once ('../bootstrap.php');
	include_once(LIB_DIR . "shell.inc.php");
	include_once(MODEL.'user.class.php');
	include_once(MODEL.'credit/credit.class.php');
	include_once(MODEL.'sms/sms.class.php');
    include_once(MODEL.'senderid/senderid.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

    $sendername = cleanQuery($_GET['sendername']);
    $username = cleanQuery($_GET['username']);
    $password = cleanQuery($_GET['password']);
    $apiKey = cleanQuery($_GET['apikey']);
    $destNumber = $_GET['destnum'];
    $message = $_GET['message'];

/*
    echo "sendername = $sendername"."<br>";
    echo "username = $username<br>";
    echo "password = $password<br>";
    echo "apiKey = $apiKey<br>";
    echo "destNumber = $destNumber <br>";
    echo "message = $message <br>";
*/

    $responseStatus =  $MESSAGE_CODE[0]['code'];
    $responseMessage =  $MESSAGE_CODE[0]['message'];
    $responseMessageDB = $responseMessage;
    $smsStoreID = '';

    $messageSize = strlen($message);
    // assign credit needed for the message   
    $messageCredit = getCredit($messageSize);


    //echo "Message size = $messageSize, $message<br>";
    if ($messageSize == 0)
    {
        $responseStatus =  $MESSAGE_CODE[5]['code'];
        $responseMessage =  $MESSAGE_CODE[5]['message'];
        $responseMessageDB = $responseMessage;
    }
    else
    {         
            // check format of destination number, eg : +255 (must be in international GSM format)
            $validNumber = checkNumberFormat($destNumber);
            if ($validNumber != 1)
            {
                 $responseStatus =  $MESSAGE_CODE[4]['code'];
                 $responseMessage =  $MESSAGE_CODE[4]['message'];
                 $responseMessageDB = $responseMessage;
            }
            else
            {
                // check destination number
                $isValid = checkDestination($destNumber);
                if ($isValid != 1) // currently only Tanzanian numbers can be served.
                {
                    switch ($isValid)
                    {
                        case 7 :
                                $responseStatus =  $MESSAGE_CODE[7]['code'];
                                $responseMessage =  $MESSAGE_CODE[7]['message'];
                                $responseMessageDB = $responseMessage;
                        break;

                        case 4 :
                                $responseStatus =  $MESSAGE_CODE[4]['code'];
                                $responseMessage =  $MESSAGE_CODE[4]['message'];
                                $responseMessageDB = $responseMessage;
                        break;
                    }

                }
                else
                {
                    // get user information
                    $arrUser =	venLogin('mst_vendors', $username, md5($password));

                    // username & password : valid/exist
	                if($arrUser)
                    {
                        $apiKeyDB = $arrUser['id'];
                        //echo "APIKEY = $apiKey<br>";

                        //compare API key
                        if ($apiKeyDB != $apiKey)
                        {
                            $responseStatus =  $MESSAGE_CODE[3]['code'];
                            $responseMessage =  $MESSAGE_CODE[3]['message'];
                            $responseMessageDB = $responseMessage;
                        }
                        else
                        {
                            // get user detail
                        	$objUser = new user();
                            $objUser->userId = $apiKey;
                            $arrDetailUser = $objUser->getUserDetailVen();
                            
                            // get active status    
                            $active = $arrDetailUser['active'];
                            if ($active!=1)
                            {
                                $responseStatus =  $MESSAGE_CODE[6]['code'];
                                $responseMessage =  $MESSAGE_CODE[6]['message'];
                                $responseMessageDB = $responseMessage." : user not active";
                            }
                            else // active user 
                            {
                                // get user balance
                            	$objCredit = new credit();
                                $objCredit->userId = $apiKey;
                                $userBalance = $objCredit->getCreditBalance();
                                $creditLeft = $userBalance - $messageCredit;

                                //echo "balance = $userBalance<br>";
                                if ( ($userBalance <= 0) or ($creditLeft < 0) )
                                {
                                    $responseStatus =  $MESSAGE_CODE[2]['code'];
                                    $responseMessage =  $MESSAGE_CODE[2]['message'];
                                    $responseMessageDB = $responseMessage;
                                }
                                else
                                {
                                    $destNumberWithNoCountryCode = getPhoneNumber($destNumber);

                                    $objSender = new senderid();
                                    $objSender->login_id = $apiKey;
                                    $arrSender = $objSender->getActiveSenderid();
                                    
                                    if ($arrSender)
                                    {
                                        /* check sender name */
                                        $arrSenderName = searchSenderName($arrSender, $sendername);
                                        if (!$arrSenderName)
                                        {
                                            $responseStatus =  $MESSAGE_CODE[6]['code'];
                                            $responseMessage =  $MESSAGE_CODE[6]['message'];
                                            $responseMessageDB = $responseMessage;
                                        }
                                        else
                                        {
                                            /* INSERT INTO sms_stores */
                                            $sender_ID = $arrSenderName['id'];
                                            $senderName = $arrSenderName['senderid'];
                                            $numRecipient = 1;
                                            $status = smsAPISaveToSmsStores($apiKey, $sender_ID, $message, $messageSize, $numRecipient);

                                            //echo "insert into sms_stores! = $status <br>";
                                            /*INSERT INTO inbound_messages (id,sms_store_id,user_id,status,short_message,
                                                                            msg_length,msg_count,source_addr,destination_addr,created)
                                            */
                                            $smsStoreID = getSmsStoresID($apiKey, $sender_ID);
                                            //echo "smsStoreID! = ".$smsStoreID."<br>";
                                            if ($smsStoreID!='')
                                            {
                                                /*call  blockcredit()*/                                            
                                                //echo "block credit = $messageCredit <br> ";
                                                $objMsg = new sms();
                                                $objMsg->credit = $messageCredit; 
                                                $objMsg->userId = $apiKey;
                                                $objMsg->blockCredit();                                   

                                                $status = 0;
                                                $messageCount = $messageCredit;
                                                $sourceAddr = $senderName; //"Bongo Live, vuvuzela, 255688121252, etc";
                                                $destAddr = $destNumberWithNoCountryCode;
                                                $queryString = "(UUID(),'$smsStoreID','$apiKey',$status,'$message',$messageSize, $messageCount, '$sourceAddr', '$destAddr', NOW())"; 
                                                $objSMS = new sms();
                                                $objSMS->queryStr = $queryString;
                                                $insertStatus = $objSMS->proccessInboundMessage();                                    
                                                //echo "insert into inbound_messages! = $insertStatus <br>"; 

                                                /*call  updateCreditBalance()*/
                                                //echo "update credit = $messageCredit <br> ";

                                                $objMsg = new sms();
                                                $objMsg->credit = $messageCredit; 
                                                $objMsg->userId = $apiKey;
                                                $objMsg->updateCreditBalance();

                                                $responseStatus = $messageCredit; // on success, return number of credit used 
                                            }
                                            else
                                            {
                                                //echo "error insert into sms_stores = ($smsStoreID) <br>"; 
                                                $responseStatus =  $MESSAGE_CODE[8]['code'];
                                                $responseMessage =  $MESSAGE_CODE[8]['message'];
                                                $responseMessageDB = $responseMessage." : internal error in fetch SMS ID";
                                            }
                                        } // end else : searchSenderName

                                        //BEGIN : IN LIVE TESTING, COMMENT THIS PART 
                                        /*
                                        $senderid = $sourceAddr;
                                        $outStr = smsSendWithCurl($destNumberWithNoCountryCode,$message,$senderid,'Y');
                                        //echo "output = $outStr<br>";
                                        $responseStatus = $outStr;
                                        $responseMessage = "Successful (directly send to Infobip API for delivery)";
                                        */
                                        //END : IN LIVE TESTING, COMMENT THIS PART 

                                    } 
                                } // end else : userBalance > 0
                            } // end else : get active status
                        } // end else : compare API key
                    }
                    else
                    {        
                            $responseStatus =  $MESSAGE_CODE[13]['code'];
                            $responseMessage =  $MESSAGE_CODE[13]['message'];
                            $responseMessageDB = $responseMessage;
                    } // end else : username or password : not valid

                }// end else : checkDestination == 1 (Tanzanian number only)
            } // end else : checkNumberFormat == 1
    } // end else : messageSize != 0
    

    saveToLogTable($apiKey, $smsStoreID, $username, $sendername, $destNumber, $message, $responseStatus, $responseMessageDB);

    $apiResponse = "$responseStatus"; // "$responseStatus|$responseMessage"
    echo "$apiResponse";  
?>
