<?
	include_once ('../bootstrap.php');
	include_once(LIB_DIR.'inc.php');
	include_once(MODEL.'user.class.php');
	include_once(MODEL.'credit/credit.class.php');
	include_once(MODEL.'sms/sms.class.php');
    include_once(MODEL.'senderid/senderid.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');


    $arrSmsWithReport = array();
    $arrSmsWithReport = getSmsWithDeliveryReport();

    $numberOfSmsWithDLR = count($arrSmsWithReport);

    if ($numberOfSmsWithDLR != 0)
    {
        $i = 0;
        $tempUserId = "";
        while ($i < $numberOfSmsWithDLR)
        {
            $remote_push_time = "X";
            $remote_deliv_time = "X";

            $userId = $arrSmsWithReport[$i]['user_id'];
            if ($tempUserId != $userId)
            {
                $callBackURL = "";
                $callBackURL = getUsersCallBackURL($userId);
                
                if (trim($callBackURL) != "")
                {
                    $tempUserId = $userId;
                    $messageid = $arrSmsWithReport[$i]['user_message_id'];
                    $destnum = $arrSmsWithReport[$i]['destination'];
                    $remote_push_time = $arrSmsWithReport[$i]['remote_push_time'];
                    $remote_deliv_time = $arrSmsWithReport[$i]['remote_deliv_time'];
                    $remotestatus = $arrSmsWithReport[$i]['remote_status'];
                    $dlrmessage = ($remotestatus[0] != "-") ? "SENT_OR_DELIVERED" : "FAILED";

                    sendToCallBackURL($callBackURL, $messageid, $destnum, $dlrmessage, $remote_push_time, $remote_deliv_time);
                    $lastCallBackURL = $callBackURL;

                    $sms_store_id = $arrSmsWithReport[$i]['sms_store_id'];
                    updateIsCalledFlag($sms_store_id, $userId);
                }

                    //echo "1. $messageid = $destnum $dlrmessage <br>";
            }
            else
            {
                    $messageid = $arrSmsWithReport[$i]['user_message_id'];
                    $destnum = $arrSmsWithReport[$i]['destination'];
                    $remote_push_time = $arrSmsWithReport[$i]['remote_push_time'];
                    $remote_deliv_time = $arrSmsWithReport[$i]['remote_deliv_time'];
                    $remotestatus = $arrSmsWithReport[$i]['remote_status'];
                    $dlrmessage = ($remotestatus[0] != "-") ? "SENT_OR_DELIVERED" : "FAILED";

                    sendToCallBackURL($callBackURL, $messageid, $destnum, $dlrmessage, $remote_push_time, $remote_deliv_time);

                    $sms_store_id = $arrSmsWithReport[$i]['sms_store_id'];
                    updateIsCalledFlag($sms_store_id, $userId);
                    //echo "2. $messageid = $destnum $dlrmessage <br>"; 

            }
            
            $i++;
        }
        
    }
    else
    {
        echo "All report have been delivered.";
    }
    
?>
