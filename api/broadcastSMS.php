<?
/*
1) Incoming XML received
2) Check if user is authorized
3) Check if Sender Name exists in system for that vendor_id and is active
4) Check if all GSM numbers are in correct format
5) Remove 255 prefix from all GSM number
6) Calculate number of messages (based on character count)
7) Check if user has enough credits for all messages
8) Insert dummy record into sms_stores table to record history (recepient count should = count(gsm numbers), (credit column = message count*count(gsm numbers))
9) Insert record into inbound_messages table for each GSM number
10) Return appropriate error/success message (success shows count(gsm numbers))
11) Once status of sms is returned from Infobip (i.e remote_status column populated on inbound_messages table) then a call is made to the callback URL with the messageID and status for that sms.
*/

	include_once ('../bootstrap.php');
	include_once(LIB_DIR.'inc.php');
	include_once(MODEL.'user.class.php');
	include_once(MODEL.'credit/credit.class.php');
	include_once(MODEL.'sms/sms.class.php');
    include_once(MODEL.'senderid/senderid.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

    $messageXML = $_POST['messageXML'];

    //echo "XML = ".$messageXML. "<br>";

    $responseStatus =  $MESSAGE_CODE[0]['code'];
    $responseMessage =  $MESSAGE_CODE[0]['message'];
    $responseMessageDB = $responseMessage;
    $broadcastMessage = "";

    $arrRecipient = validateXML($messageXML);
    //print_r($arrRecipient);  
  
    if ($arrRecipient)
    {
        $numData = $arrRecipient['numberofrecipient'];
        if ($numData < 1) // if number of array element <= 1 then there's something wrong with the XML structure
        {           
            // echo "arrReceiver = ".count($arrRecipient)."<br>";
            $responseStatus =  $MESSAGE_CODE[1]['code'];
            $responseMessage =  $MESSAGE_CODE[1]['message'];
            $responseMessageDB = $responseMessage;
        }
        elseif ($numData > MAXRECIPIENTS) 
        {
            $responseStatus =  $MESSAGE_CODE[10]['code'];
            $responseMessage =  $MESSAGE_CODE[10]['message'];
            $responseMessageDB = $responseMessage;
        }
        else
        {

            $broadcastMessage = trim($arrRecipient['message']['content']);
            $messageSize = strlen($broadcastMessage);

            if ($messageSize == 0)
            {
                $responseStatus =  $MESSAGE_CODE[5]['code'];
                $responseMessage =  $MESSAGE_CODE[5]['message'];
                $responseMessageDB = $responseMessage;
            }
            else // $messageSize > 0
            {

                $numRecipient = $numData;
                $indexRecv = 0;
                $indexValid = 0;
                $msgid = "";
                $destnum = "";
                
                $arrValidRecipients = array();
                while ($indexRecv < $numRecipient)
                {
                    $msgid = trim($arrRecipient[$indexRecv]['messageid']);               
                    $destnum = trim($arrRecipient[$indexRecv]['destination']);               

                    //echo "$indexRecv . destnum = $destnum ($msgid) <br>";
                    // check destination number
                    $isValid = checkDestination($destnum);
                    if ($isValid != 1) // currently only Tanzanian numbers can be served.
                    {
                        switch ($isValid)
                        {
                            case 7 :
                                    $responseStatus =  $MESSAGE_CODE[7]['code'];
                                    $responseMessage =  $MESSAGE_CODE[7]['message'];
                                    $responseMessageDB = $responseMessage;
                                    //echo "7. gsm no ($destnum) & messageid ($msgid)<br>";
                            break;

                            case 4 :
                                    $responseStatus =  $MESSAGE_CODE[4]['code'];
                                    $responseMessage =  $MESSAGE_CODE[4]['message'];
                                    $responseMessageDB = $responseMessage;
                                    //echo "4. gsm no ($destnum) & messageid ($msgid)<br>";
                            break;
                        }

                    } 
                    else
                    {
                        $validMsgId = checkMessageId($msgid); 
                        if (!$validMsgId)
                        {
                           $responseStatus =  $MESSAGE_CODE[9]['code'];
                           $responseMessage =  $MESSAGE_CODE[9]['message'];
                           $responseMessageDB = $responseMessage;
                           break; 
                        }
                        else
                        {
                            //echo "valid gsm no ($destnum) & valid messageid ($msgid)<br>";
                            $arrValidRecipients[$indexValid]['messageid'] = $msgid;
                            $arrValidRecipients[$indexValid]['destination'] = $destnum;
                            $indexValid++;

                        } // end else : if (!$validMsgId)

                    } // end else : if ($isValid != 1)            

                    $indexRecv++;
                    $msgid = "";
                    $destnum = "";
                } //end while ($indexRecv < $numRecipient)               

                $numValidRecipient = count ($arrValidRecipients);

                //echo "$numValidRecipient == $numRecipient<br>";

                if ($numValidRecipient == $numRecipient) // proceed if each of all recipients have a valid messageid & destination number format                
                {

                    // get user information
                    $username = $arrRecipient['username'];
                    $password = $arrRecipient['password'];
                    $arrUser =	venLogin('mst_vendors', $username, md5($password));

                    // username & password : valid/exist
	                if($arrUser)
                    {
                        $apiKeyDB = $arrUser['id'];
                        //echo "APIKEY = $apiKey<br>";

                        $apiKey = $arrRecipient['apikey'];
                        //compare API key
                        if ($apiKeyDB != $apiKey)
                        {
                            $responseStatus =  $MESSAGE_CODE[3]['code'];
                            $responseMessage =  $MESSAGE_CODE[3]['message'];
                            $responseMessageDB = $responseMessage;
                        }
                        else
                        {
                            // get user detail
                        	$objUser = new user();
                            $objUser->userId = $apiKey;
                            $arrDetailUser = $objUser->getUserDetailVen();
                            
                            // get active status    
                            $active = $arrDetailUser['active'];
                            if ($active!=1)
                            {
                                $responseStatus =  $MESSAGE_CODE[6]['code'];
                                $responseMessage =  $MESSAGE_CODE[6]['message'];
                                $responseMessageDB = $responseMessage." : user not active";
                            }
                            else // active user 
                            {
                                // add callbackURL as part of user data in mst_vendors
                                $callBackURL = $arrRecipient['callbackurl'];
                                if (trim($callBackURL) != "")
                                {
                                    $res = addCallBackURLToUserData($callBackURL, $apiKey, $username);
                                }

                                // assign credit needed for the message   
                                $messageCredit = getCredit($messageSize);
                                $neededCredit = $numValidRecipient * $messageCredit;

                                //echo "messageCredit = $messageCredit, numValidRecipient = $numValidRecipient => neededCredit = $neededCredit ";

                                /* check credit */
                               	$objCredit = new credit();
                                $objCredit->userId = $apiKey;
                                $userBalance = $objCredit->getCreditBalance();
                                $creditLeft = $userBalance - $neededCredit;

                                //echo "balance = $userBalance<br>";
                                if ( ($userBalance <= 0) or ($creditLeft < 0) )
                                {
                                   $responseStatus =  $MESSAGE_CODE[2]['code'];
                                   $responseMessage =  $MESSAGE_CODE[2]['message'];
                                   $responseMessageDB = $responseMessage;
                                }
                                else
                                {
                                    $objSender = new senderid();
                                    $objSender->login_id = $apiKey;
                                    $arrSender = $objSender->getActiveSenderid();
                                    
                                    if ($arrSender)
                                    {
                                        /* check sender name */
                                        $sendername = $arrRecipient['sendername'];
                                        $arrSenderName = searchSenderName($arrSender, $sendername);
                                        if (!$arrSenderName)
                                        {
                                            $responseStatus =  $MESSAGE_CODE[6]['code'];
                                            $responseMessage =  $MESSAGE_CODE[6]['message'];
                                            $responseMessageDB = $responseMessage;
                                        }
                                        else
                                        {
                                            $indexValid = 0;
                                            $messageid = "";
                                            $destNumber = "";
                                            $smsStoreID = "";
                                            $sender_ID = $arrSenderName['id'];
                                            $senderName = $arrSenderName['senderid'];
                                            $messageCredit = getCredit($messageSize);
                                            $messageCount = $messageCredit;

                                            while ($indexValid < $numValidRecipient)
                                            {
                                                $messageid = $arrValidRecipients[$indexValid]['messageid'];
                                                $destNumber = $arrValidRecipients[$indexValid]['destination'];
                                                $destNumberWithNoCountryCode = getPhoneNumber($destNumber);    

                                                if ($smsStoreID == "")
                                                {
                                                    $status = smsAPISaveToSmsStores($apiKey, $sender_ID, $broadcastMessage, $messageSize, $numRecipient);
                                                    $smsStoreID = getSmsStoresID($apiKey, $sender_ID);

                                                    /*call  blockcredit()*/                                            
                                                    $objMsg = new sms();
                                                    $objMsg->credit = $messageCredit; 
                                                    $objMsg->userId = $apiKey;
                                                    $objMsg->blockCredit();                                   

                                                    $status = 0;
                                                    $sourceAddr = $senderName; //"Bongo Live, vuvuzela, 255688121252, etc";
                                                    $destAddr = $destNumberWithNoCountryCode;
                                                    $messageID = $messageid;

                                                    $insertStatus = insertIntoInboundMessage($smsStoreID, $apiKey, $status, $broadcastMessage, $messageSize, $messageCount, $sourceAddr, $destAddr, $messageID);

                                                    /*call  updateCreditBalance()*/
                                                    $objMsg = new sms();
                                                    $objMsg->credit = $messageCredit; 
                                                    $objMsg->userId = $apiKey;
                                                    $objMsg->updateCreditBalance();

                                                    $responseStatusCredit = $messageCredit;
                                                    $responseStatus = $neededCredit; // on success, return number of credit used 
                                                    $responseMessageDB = $responseMessage;
        
                                                    saveToLogTable($apiKey, $smsStoreID, $username, $senderName, $destNumber, $broadcastMessage, $responseStatusCredit, $responseMessageDB);                            

                                                }
                                                else
                                                {
                                                    /*call  blockcredit()*/                                            
                                                    $objMsg = new sms();
                                                    $objMsg->credit = $messageCredit; 
                                                    $objMsg->userId = $apiKey;
                                                    $objMsg->blockCredit();                                   

                                                    $status = 0;
                                                    $sourceAddr = $senderName; //"Bongo Live, vuvuzela, 255688121252, etc";
                                                    $destAddr = $destNumberWithNoCountryCode;
                                                    $messageID = $messageid;

                                                    $insertStatus = insertIntoInboundMessage($smsStoreID, $apiKey, $status, $broadcastMessage, $messageSize, $messageCount, $sourceAddr, $destAddr, $messageID);
                                                    //echo "insert into inbound_messages = $insertStatus <br>"; 

                                                    /*call  updateCreditBalance()*/
                                                    $objMsg = new sms();
                                                    $objMsg->credit = $messageCredit; 
                                                    $objMsg->userId = $apiKey;
                                                    $objMsg->updateCreditBalance();

                                                    $responseStatusCredit = $messageCredit;
                                                    $responseStatus = $neededCredit; // on success, return number of credit used 
                                                    $responseMessageDB = $responseMessage;
        
                                                    saveToLogTable($apiKey, $smsStoreID, $username, $senderName, $destNumber, $broadcastMessage, $responseStatusCredit, $responseMessageDB);                            
                                                } // end else : if ($smsStoreID!='')

                                                $messageid = "";
                                                $destNumber = "";
                                                $indexValid++;
                                            } // end while ($indexValid < $numValidRecipient), get all recipient data and save them into sms_stores and inbound_messages table                                                       
                                        } // end else : searchSenderName

                                    } 
                                } // end else : userBalance > 0
                            } // end else : get active status
                        } // end else : compare API key
                    }
                    else
                    {        
                            $responseStatus =  $MESSAGE_CODE[13]['code'];
                            $responseMessage =  $MESSAGE_CODE[13]['message'];
                            $responseMessageDB = $responseMessage;
                    } // end else : username or password : not valid

                } // end if ($numValidRecipient == $numRecipient)

            } // end else : if ($messageSize == 0)
        }// end else numData
    }
    else
    {
        $responseStatus =  $MESSAGE_CODE[1]['code'];
        $responseMessage =  $MESSAGE_CODE[1]['message'];
        $responseMessageDB = $responseMessage;
    }
    
    $xmlResponse = "<Responses>
                        <Response>
                           <code>".$responseStatus."</code>
                           <message>".$responseMessage."</message>
                        </Response>
                    </Responses>"; 

    echo $xmlResponse;
?>
