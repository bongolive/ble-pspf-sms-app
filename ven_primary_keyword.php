<?php
	
	/*************************************************
	*	File	: Vven_primary_keyword.php				 *
	*	Purpose	: Manage Primary Keyword for vendor			 *
	*	Author	: Leonard Nyirenda					 	 *
	*************************************************/
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'primkeys.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);


	$objPk= new primkeys(); 
	
	
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	$errorFlag = 0;
	switch($action){
	
		case'add':
			$objPk->vendor_id= $_SESSION['user']['id'];
			$objPk->text_prim_keyword = cleanQuery($_REQUEST['text_prim_keyword']);
			if($objPk->addPrimKey()){
			
				$msg = "New Primary Keyword added successfully";
			}else{
			
				$msg = "Primary keyword not added";
				$errorFlag = 1;
			}
			
			
		break;
		case'edit':
			$objPk->id_prim_keyword = $_REQUEST['id'];
			$arrPk = $objPk->getPrimFromId();
			while($row=mysql_fetch_array($arrPk)){
				$smarty->assign('text_prim_keyword',$row['text_prim_keywords']);
				$smarty->assign('id',$row['id_prim_keywords']);
			}
			$action = 'update';
		break;
		case'update':
			$objPk->id_prim_keyword  = $_REQUEST['id'];
			$objPk->text_prim_keyword = $_REQUEST['text_prim_keyword'];
			
			if($objPk->editPrimKey()){
				$msg = "Primary Keyword updated successfully";
			}else{
				$msg = "Primary keyword not updated";
				$errorFlag = 1;
			}

			$action = "add";
			
		break;
		
		default:
			$action = "add";
	
	
	}
	$arrPk =  $objPk->getAllPrim();
	while($row=mysql_fetch_array($arrPk)){
		$value[]=$row;
	}

	$smarty->assign("msg",$msg);
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("selfUrl",BASE_URL.'ven_primary_keyword.php');
	$smarty->assign("mngUrl",BASE_URL.'ven_second_keyword.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrPk",$value);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_primary_keyword.tpl");
	SmartyPaginate::disconnect();

	include 'footer.php';

?>