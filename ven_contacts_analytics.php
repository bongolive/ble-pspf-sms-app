<?php
/****************************************************************************************************************
*	File : ven_addressbook_dashboard.php
*	Purpose: Showing graphs for contacts
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include('ven_header.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	
	$startPie	=	"{'legendLabel':'";
	$middlePie	=	"', 'magnitude':";
	$endPie	=	"},";

	$age= " yrs";
	$commer=",";
	$newLine="\r\n";
	$string_end	=	" }, ";
	$expressionLocation='';
	$expressionAge='';
	$expressionGender='';


if(isset($_REQUEST['graph']) && $_REQUEST['graph']=="line"){
	//Prepare data for Pie chart
	$graph='line';
	
}else if($_REQUEST['graph']=='pie'){
	$graph='pie';
	//GENDER
	$arrReportGender = $objAddressbook->reportGender();
	while($rowGender=mysql_fetch_array($arrReportGender)){
		$stringGender = $startPie.$rowGender['value_x'].$middlePie.$rowGender['value_y'].$endPie;
		$expressionGender	=	$expressionGender.$stringGender ;
	}
	
	$expressionGender = substr($expressionGender, 0, -1); //removing last commer character from a string
	$expressionGender = str_replace("'",'"',$expressionGender);
	
	
	//LOCATIONS
	$arrReportLocation = $objAddressbook->reportLocation();
	while($rowLocation=mysql_fetch_array($arrReportLocation)){
		$stringLocation = $startPie.$rowLocation['value_x'].$middlePie.$rowLocation['value_y'].$endPie;
		$expressionLocation	=	$expressionLocation.$stringLocation ;
	}
	$expressionLocation = substr($expressionLocation, 0, -1); //removing last commer character from a string
	$expressionLocation = str_replace("'",'"',$expressionLocation);
	
	
	//AGE
	$arrReportAge = $objAddressbook->reportAge();
	while($rowAge=mysql_fetch_array($arrReportAge)){
		$stringAge = $startPie.$rowAge['value_x']." yrs".$middlePie.$rowAge['value_y'].$endPie;
		$expressionAge	=	$expressionAge.$stringAge ;
	}
	$expressionAge = substr($expressionAge, 0, -1); //removing last commer character from a string
	$expressionAge = str_replace("'",'"',$expressionAge);

}else{
	$graph='bar';
	//Prepare data for Pie chart
	$arrReportGender = $objAddressbook->reportGender();
	while($rowGender=mysql_fetch_array($arrReportGender)){
		$stringGender = $rowGender['value_x'].$commer.$rowGender['value_y'].$newLine;
		$expressionGender	=	$expressionGender.$stringGender;
	}
	
	//LOCATIONS
	$arrReportLocation = $objAddressbook->reportLocation();
	while($rowLocation=mysql_fetch_array($arrReportLocation)){
		$stringLocation = $rowLocation['value_x'].$commer.$rowLocation['value_y'].$newLine;
		$expressionLocation	=	$expressionLocation.$stringLocation  ;
	}
	
	//AGE
	$arrReportAge = $objAddressbook->reportAge();
	while($rowAge=mysql_fetch_array($arrReportAge)){
		$stringAge = $rowAge['value_x']." yrs".$commer.$rowAge['value_y'].$newLine;
		$expressionAge	=	$expressionAge.$stringAge  ;
	}
		

}
	
	$smarty->assign('expressionAge',$expressionAge);
	$smarty->assign('expressionGender',$expressionGender);
	$smarty->assign('expressionLocation',$expressionLocation);
	$smarty->assign('graph',$graph);
	$smarty->display("vendor/ven_contacts_analytics.tpl");

	include 'footer.php';
?>