<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: sending Credit balance reminder to a  vendors
*	Author : Leonard nyirenda
****************************************************************************************************
*/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include(MODEL.'vendors/vendor.class.php');
	include(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';
	include(MODEL.'sms/sms.class.php');
	
	$objSms = new sms();
	$objUser= new user();
	$objVendor = new vendor();
	$arrVendor = $objVendor->getAllVendorsWithLowCreditBalances();
	//print_r($arrVendor);
	//if(mysql_num_rows($arrVendor)>0){
	//echo "Data  for Credit Reminder found now in action....";
		while($row = mysql_fetch_assoc($arrVendor)){
			//Testing Type of reminder
			$balance= $row['credit_balance'] - $row['credit_blocked'];
			$smarty->assign('balance',$balance);
			$smarty->assign('reminderLevel',$row['credit_level']);
			$smarty->assign('name',$row['name']);
			
			$objUser->parentVenId = $row['parent_ven_id'];
			
			if($row['parent_ven_id'] != ''){
				$resellerDetail = $objUser->getResellerDetail();
			}else{
				$resellerDetail['name'] ='Admin';
				$resellerDetail['email'] ='contact@bongolive.co.tz';
			}
			$smarty->assign('resellerDetails', $resellerDetail);
			$smarty->assign("parentVenId", $row['parent_ven_id']);
		
			if($row['email_reminder'] == 1){
				//Email Reminder
				try{
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
						
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);

							$body = $smarty->fetch('mail_template/mail_ven_credit_reminder.tpl');

							$message = Swift_Message::newInstance('Bongo Live! Credit Reminder')
							->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
							->setTo(array($row['email']=>$row['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);
							
							//updating reminder status
							$objVendor->userId = $row['id'];
							$objVendor->reminderSent = 1;
							$objVendor->updateVendorReminderStatus();

					} catch (Exception $e){
					
						$msg = SUB_REGISTER_MSG_08.$e->getMessage();
						$errorFlag = 1;
					}
		
		}
			if($row['sms_reminder'] == 1){
				//SMS Reminder
				//disableling international numbers receive sms reminder
				if(strlen($row['mob_no'])<=10 || substr($row['mob_no'],0, 3)=='255'){
					try{	
							$message =$row['name']." your current credit balance  is ".$balance." SMS. Your reminded to recharge your account";
							smsSendWithCurl($row['mob_no'],$message,$project_vars['sms_api_senderid']);
							//updating reminder status
							$objVendor->userId = $row['id'];
							$objVendor->reminderSent = 1;
							$objVendor->updateVendorReminderStatus();
						}catch(Exception $objE){
							$msg = VEN_REGISTER_MSG_08.$objE->getMessage();;
							$errorFlag = 1;
						}
					}
		
				}
		}
		
	//}else{ //This is part of Last broadcast Reminder
	//		echo "Now we are in Last Broadcast reminder...\r\n";
			$arrVendors=$objUser->getLastBroadcastDetails();
			if($arrVendors !=false){
			echo count($arrVendors)."  Vendor found for Last bradcast reminder..\r\n";
				foreach($arrVendors as $rowVendor){
					$objSms->userId = $rowVendor['id'];
					$arrSMS=$objSms->getVendorLastSentSMS();
					$DateToCompare=strtotime ( '-15 day' , time());
					$venDate=strtotime($arrSMS['sent_time']);
					if($venDate < $DateToCompare && $arrSMS != false){ //checking if Last sent sms 
						echo "Data for Paricular vendor found now proccessing...\r\n";
						//===================================================
							//Testing Type of reminder
							$smarty->assign('sent_date',date( 'Y-m-j' , strtotime($arrSMS['sent_time'])));
							$smarty->assign('name',$rowVendor['name']);
							
							$objUser->parentVenId = $rowVendor['parent_ven_id'];
							if($rowVendor['parent_ven_id'] != ''){
								$resellerDetail = $objUser->getResellerDetail();
							}else{
								$resellerDetail['name'] ='Admin';
								$resellerDetail['email'] ='contact@bongolive.co.tz';
							}
							$smarty->assign('resellerDetails', $resellerDetail);
							$smarty->assign("parentVenId",$rowVendor['parent_ven_id']);
		
							//Email Reminder
								try{
									if($project_vars["smtp_auth"]===true){
										$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
										->setUsername($project_vars["smtp_username"])
										->setPassword($project_vars["smtp_password"]);
									}else{
										$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
									}

									$mailer = Swift_Mailer::newInstance($transport);

									$body = $smarty->fetch('mail_template/mail_Broadcast_reminder.tpl');

									$message = Swift_Message::newInstance('Bongo Live! Credit Reminder')
										->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
										->setTo(array($rowVendor['email']=>$rowVendor['name']))
										->setBody($body );
									$message->setContentType("text/html");

									//Send the message
									$result = $mailer->send($message);

								} catch (Exception $e){
					
											$msg = SUB_REGISTER_MSG_08.$e->getMessage();
											$errorFlag = 1;
									}

								//SMS Reminder
								//disableling international numbers receive sms reminder
								if(strlen($rowVendor['mob_no'])<=10 || substr($rowVendor['mob_no'],0, 3)=='255'){
									try{	
										$message ="Hello ".$rowVendor['name'].". We've missed you. Its been 2 weeks since you last sent an sms. Send your offers, reminders & alerts easily at bongolive.co.tz";
										smsSendWithCurl($rowVendor['mob_no'],$message,$project_vars['sms_api_senderid']);
									}catch(Exception $objE){
										$msg = VEN_REGISTER_MSG_08.$objE->getMessage();
										$errorFlag = 1;
									}
								}
						$objSms->userId;
						$objSms->reminderSent=1;
						$objSms->updateBroadcastReminder();
						
						//===================================================
					}
				}
			}
	//}

?>