<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: sending sms for vendors
*	Author : Leonard nyirenda
****************************************************************************************************

on 6th nov 2012 Leonard nyirenda add error message on second step
The error message is when vendor select mobile number column which does not contains mobile numbers

Also we show the error message when user try to upload empty file(work for xls,text and csv only)
and we remove white space within a mobile number
*/ 
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'user.class.php');
	include(MODEL.'sms/sms.class.php');
	include(MODEL.'senderid/senderid.class.php');
	include(MODEL.'credit/credit.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');
	include(MODEL.'sms/smstemplate.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//These setting will be used by file SMS only when script run and will not make any changes to php.ini file
	ini_set(memory_limit,'250M');
	ini_set(max_execution_time,'900');
	ini_set(max_input_time,'900');
	
	$objUser= new user();
	$objUser->userId = $_SESSION['user']['id'];

	$objSenderid = new senderid();
	$objSenderid-> login_id = $_SESSION['user']['id'];
	$arrSenderid = $objSenderid-> getActiveSenderid();
	$smarty->assign('arrSenderid',$arrSenderid);

	$objSms = new sms();
	$objSms->userId = $_SESSION['user']['id'];
	
	$objCredit = new credit();
	$objCredit->userId = $_SESSION['user']['id'];
	$crdBalance = $objCredit->getCreditBalance();
	
	$objsmstemplate=new smstemplate();
	$objsmstemplate->vendor_id=$_SESSION['user']['id'];
	$arrsmstemplate=$objsmstemplate->getvendorsmstemplatelist2();
	
	switch($_REQUEST['action']){
		case 'uploadFile':
				if(isset($_FILES['file'])){
					$f_detail = pathinfo($_FILES['file']['name']);
					$arrRecipient=array();
					if(strtolower($f_detail['extension'])== 'csv'){
						$arrRecipient = uploadMobilephoneFrom_csv($_FILES['file']['tmp_name']);
						$_SESSION['smsFile']['extendionFile']='csv';
					}else if(strtolower($f_detail['extension'])== 'xls'){
						$_SESSION['smsFile']['extendionFile']='xls';
						require_once 'classes/exel/PHPExcel/Reader/excel_reader.php';
   						$data = new Spreadsheet_Excel_Reader($_FILES['file']['tmp_name']);

    					if($data->sheets[0]['numRows']>0){
							for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
								for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
									$arrRecipient[$i-1][$j-1] = $data->sheets[0]['cells'][$i][$j];
        						}
   							}
						}
					}else if(strtolower($f_detail['extension'])== 'txt'){
						$arrRecipient = uploadMobilephoneFrom_txt($_FILES['file']['tmp_name']);
						$_SESSION['smsFile']['extendionFile']='txt';	
					}else if(strtolower($f_detail['extension'])== 'xlsx'){
						//$arrRecipient = getXLSorXLSX($_FILES['file']['tmp_name']);
						$_SESSION['smsFile']['extendionFile']='xlsx';
						require_once "classes/exel/PHPExcel/Reader/simplexlsx.class.php";
						$xlsx = new SimpleXLSX($_FILES['file']['tmp_name'] );
	
						list($cols,) = $xlsx->dimension();
	
						$j	=	0;
						foreach( $xlsx->rows() as $k => $r) {
							for( $i = 0; $i < $cols; $i++){
								$arrRecipient[$j][$i] = $r[$i];
							}
						$j++;
						}
					}
				}else if($_SESSION['smsFile']['recipients'] != ""){
					$arrRecipient = $_SESSION['smsFile']['recipients'];
				}
				 
				if($arrRecipient != ''){
					//Finding number of column of returned array
					$arrFirstRow	=array_slice($arrRecipient, 0, 1);
					$colCounter=0;
					foreach ($arrFirstRow as $value) {
    					$counter=1;
						foreach ($value as $key) {
    						$colCounter++;
							$arrCols[]	=	"VALUE".$counter;
							$counter++;
						}
					}
					if($_SESSION['smsFile']['mobNoColumn'] !=''){
						$mobNoColumn=$_SESSION['smsFile']['mobNoColumn'];
					}else{
						$mobNoColumn="";
					}
				
					$_SESSION['smsFile']['arrCols']=$arrCols;
					$_SESSION['smsFile']['cols']=$colCounter;
					//$_SESSION['smsFile']['fileuploaded']=$_REQUEST['fileuploaded'];
					$_SESSION['smsFile']['recipients'] = $arrRecipient;
					
					// Taking only ten Rows of an array
					$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 0, 9);
					if(count($arrRecipient)>0){
						$smarty->assign('action','uploadedData');
						$smarty->assign('mobNoColumn',$mobNoColumn);
						$smarty->assign('arrColumn',$arrCols);
						$smarty->assign('arrRecipient',$arrRecipient);
					}else{
						$smarty->assign('action','uploadFile');
						$msg = VEN_SMS_PUSH_MSG_05;
						$errorFlag = 1;
					}
					
				}else{
					$action	="uploadFile";
					$smarty->assign('action',$action);
					$msg = VEN_SMS_PUSH_MSG_05;
					$errorFlag = 1;
				}
		break;
		case 'uploadedData':
			if(isset($_REQUEST['headerRow']) && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else if($_SESSION['smsFile']['headerRow'] ==1 && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else{
				$headerRow = 0;
			}
			
			if(isset($_REQUEST['mobNoColumn'])){
				$mobNoColumn	=	$_REQUEST['mobNoColumn'];
			}else if($_SESSION['smsFile']['mobNoColumn'] != ""){
				$mobNoColumn	=$_SESSION['smsFile']['mobNoColumn'];
			}
			
			
			//Building array for column of uploaded data
			$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 0, 1);
			$arrRecipientlist = $_SESSION['smsFile']['recipients'];
			$total =count($arrRecipient);
			if($headerRow == 1){
				foreach ($arrRecipient as $row) {
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= $value;
						$i++;
					}
				}
			}else{
				foreach ($arrRecipient as $row) {
    				$counter = 1;
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= "VALUE".$counter;
						$counter++;
						$i++;
					}
				}
			}
			
			$_SESSION['smsFile']['mobNoColumn']=$mobNoColumn;
			$_SESSION['smsFile']['headerRow']	=	$headerRow;
			$_SESSION['smsFile']['arrCols']	=	$arrCols;
			
			if($_SESSION['smsFile']['textMessage'] != ''){
				$smarty->assign('senderid',$_SESSION['smsFile']['senderid']);
				$smarty->assign('textMessage',$_SESSION['smsFile']['textMessage']);
				$smarty->assign('scheduleDate',$_SESSION['smsFile']['scheduleDate']);
			}
			
			//checking if user select valid mobile number columb
			$arrMobile=array();
			$TotalContact=count($arrRecipientlist);
			for ($i=$headerRow; $i < $TotalContact; $i++){
				$Phone = preg_replace('/[^0-9]/i', '', $arrRecipientlist[$i][$mobNoColumn]);
				$phone=CutMobileNumber(trim($Phone));
				if(phoneValidation($phone) ){
					$arrMobile=$phone;
				}
			}
			
			
			if(count($arrMobile)>0){
				$smarty->assign('action','createMessage');
				$smarty->assign('arrColumn',$arrCols);
				$smarty->assign('mobNoColumn',$mobNoColumn);
			}else{
				$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], $headerRow, 9);
				$smarty->assign('mobNoColumn',$_REQUEST['mobNoColumn']);
				$smarty->assign('headerRow',$_REQUEST['headerRow']);
				$smarty->assign('arrColumn',$arrCols);
				$smarty->assign('arrRecipient',$arrRecipient);
				$smarty->assign('action','uploadedData');
				$msg = VEN_SMS_PUSH_MSG_05;
				$errorFlag = 1;
				
			}
		break;
		case 'refresh':
			$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 0, 1);
			if($_REQUEST['headerRow']==1){
				foreach ($arrRecipient as $row) {
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= $value;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 1, 10);
				
			}else{
				foreach ($arrRecipient as $row) {
    				$counter = 1;
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= "VALUE".$counter;
						$counter++;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 0, 9);
			}
			$smarty->assign('mobNoColumn',$_REQUEST['mobNoColumn']);
			$smarty->assign('headerRow',$_REQUEST['headerRow']);
			$smarty->assign('arrColumn',$arrCols);
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('action','uploadedData');
		
		break;	
	
		Case 'createMessage':
				
				
				//updating credit only for parent id
				if($_SESSION['user']['parent_ven_id']==''){
					(int) $remoteCredit = getRemoteCreditBalance();
					(int) $resCredit = getTotalCreditOfAllVendors();
					$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
				} 
				
				$arrRecepient	=	$_SESSION['smsFile']['recipients'];
				if($_REQUEST['repeat2']!=''){
					$_SESSION['smsFile']['repeat']= cleanQuery($_REQUEST['repeat2']);
					$_SESSION['smsFile']['repeat_every']= cleanQuery($_REQUEST['repeat_every2']);
					$_SESSION['smsFile']['repeat_days']= cleanQuery($_REQUEST['repeat_days2']);
					$_SESSION['smsFile']['end_date']= cleanQuery($_REQUEST['end_date']);
					$_SESSION['smsFile']['reminder_type']= cleanQuery($_REQUEST['reminder_type2']);
					$_SESSION['smsFile']['birthdayfield']= $_REQUEST['birthdayfield'];
				}
				$headerRow		=	$_SESSION['smsFile']['headerRow'];
				$mobNoColumn	=	$_SESSION['smsFile']['mobNoColumn'];
				$Senderid		=	$_REQUEST['senderid'];
				$TextMessage	=	$_REQUEST['textMessage'];
				$TotalContact	= 	count($arrRecepient);
				//$arrRecepient 	= 	$_SESSION['smsFile']['recipients'];
				$arrCols		=	$_SESSION['smsFile']['arrCols'];
				$ColsCount		=	count($arrCols);
				
				if(isset($_REQUEST['scheduleDate'])){
					$scheduleDate = $_REQUEST['scheduleDate'];
				}
				
				//Cupturing only mobile column and placeholder column
				$arrValidMob=array();
				for ($x=$headerRow; $x < $TotalContact; $x++){
					$Phone = preg_replace('/[^0-9]/i', '', $arrRecepient[$x][$mobNoColumn]);
					$Phone=CutMobileNumber(trim($Phone));
					if(phoneValidation($Phone)){
						
						//Checking For Duplicate Mobile Numbers
						if (in_array($Phone, $arrValidMob, true)) {
    						$arrDuplicate[]	=	$Phone;
						}else{
							$arrValidMob[]	= $Phone;
						
							$userSMS	= $TextMessage;
							for($i=0; $i < $ColsCount; $i++){
								$valToReplace = "{".$arrCols[$i]."}";
								$userSMS	= str_replace($valToReplace,$arrRecepient[$x][$i], $userSMS);
							}
					
							$arrTextMessage[]	=	$userSMS;
					
							//Counting sms length
							$str=str_replace(chr(13), '', $userSMS);	
							$smsCount = ceil(strlen($str)/160);
							$arrSMSCount[]	=	$smsCount;
							
							//Counnting SMS characters
							$smsCharacterCount	=	strlen($str);
							$arrSMSCharacter[]	=	$smsCharacterCount;
							
							//array for birthday
							if($_SESSION['smsFile']['repeat'] !='' && $_SESSION['smsFile']['reminder_type']=='birthday'){
								$birthday = $_SESSION['smsFile']['birthdayfield'];
								if($_SESSION['smsFile']['extendionFile']=='xlsx'){
									$arrBirthday[]	=	date('Y-m-d',ExcelToPHP($arrRecepient[$x][$birthday]));
								}else{
									$arrBirthday[] = date('Y-m-d',strtotime($arrRecepient[$x][$birthday]));
								}
							}
						}
						
					}else{
						$arrInvalidMob[]	=	$arrRecepient[$x][$mobNoColumn];
					}
				}
				
				//sms to store on sms store table
				$_SESSION['smsFile']['textMessage'] = $TextMessage;
				
				//Building sms sample array
				$objSenderid->id = $Senderid;
				$SenderName	=	$objSenderid->requestSenderName();
				
				$TotalRowsSample	=	count($arrTextMessage);
				if($TotalRowsSample	== 1){
					$TotalSampleRows	=	1;
				}else if($TotalRowsSample	== 2){
					$TotalSampleRows	=	2;
				}else{
					$TotalSampleRows	=	3;
				}
				
				for ($i=0; $i < $TotalSampleRows; $i++){
					$arrSampleSMS[$i][0]	=	$arrTextMessage[$i];
					$arrSampleSMS[$i][1]	=	$arrValidMob[$i];	
					$arrSampleSMS[$i][2]	=	$SenderName;
					$arrSampleSMS[$i][3]	=	$arrSMSCharacter[$i];
					$arrSampleSMS[$i][4]	=	$arrSMSCount[$i];
					$arrSampleSMS[$i][5]	=	$arrBirthday[$i];
				}
				
				if($headerRow==1){
					$TotalContact=$TotalContact-1;
				}
				
				//Storing to session
				$_SESSION['smsFile']['TotalContact']		=	$TotalContact;
				$_SESSION['smsFile']['ValidContact']		=	$arrValidMob;
				$_SESSION['smsFile']['CountValidContact']	=	count($arrValidMob);
				$_SESSION['smsFile']['CountInvalidContact']	=	count($arrInvalidMob);
				$_SESSION['smsFile']['senderid']			=	$Senderid;
				$_SESSION['smsFile']['arrTextMessage']		=	$arrTextMessage;
				$_SESSION['smsFile']['arrSMSCharacter']		=	$arrSMSCharacter;
				$_SESSION['smsFile']['arrSMSCount']			=	$arrSMSCount;
				$_SESSION['smsFile']['scheduleDate']		= 	$_REQUEST['scheduleDate'];
				$_SESSION['smsFile']['arrBirthday']		= 	$arrBirthday;
				
				if(count($arrSMSCount)>0){
					$_SESSION['smsFile']['creditNeeded']    	=	array_sum($arrSMSCount);
					if($_SESSION['smsFile']['creditNeeded'] > $crdBalance){
						$msg = VEN_SMS_PUSH_MSG_02;
						$errorFlag = 1;
					}
				}else{
					$_SESSION['smsFile']['creditNeeded']		=	0;
				}
				
				
				if($_REQUEST['scheduleDate'] && $_REQUEST['scheduleDate'] !==""){

					if(strtotime($_REQUEST['scheduleDate'])< strtotime(date('Y-m-d H:i:s'))){

						$msg = VEN_SMS_PUSH_MSG_01;
						$errorFlag = 1;
						$smarty->assign('senderid',$_SESSION['smsFile']['senderid']);
						$smarty->assign('textMessage',$_SESSION['smsFile']['textMessage']);
						$smarty->assign('scheduleDate',$_SESSION['smsFile']['scheduleDate']);
						$smarty->assign('arrColumn',$_SESSION['smsFile']['arrCols']);
						$smarty->assign('action','createMessage');
					}else{
						$smarty->assign('TotalContact',$_SESSION['smsFile']['TotalContact']);
						$smarty->assign('CountValidContact',$_SESSION['smsFile']['CountValidContact']);
						$smarty->assign('CountDuplicateContact',count($arrDuplicate));
						$smarty->assign('CountInvalidContact',$_SESSION['smsFile']['CountInvalidContact']);
						$smarty->assign('senderid',$_SESSION['smsFile']['senderid']);
						$smarty->assign('crdBalance',$crdBalance);
						$smarty->assign('arrTextMessage',$_SESSION['smsFile']['arrTextMessage']);
						$smarty->assign('smsCount',$_SESSION['smsFile']['creditNeeded']);
						$smarty->assign('arrSampleSMS',$arrSampleSMS);
						$smarty->assign('action','SMSpreview');
						
						if($_SESSION['smsFile']['reminder_type']=='birthday'){
							$smarty->assign('repeat','birthday');
						}else{
							$smarty->assign('repeat','sms');
						}
					}	
				}else{
					$smarty->assign('TotalContact',$_SESSION['smsFile']['TotalContact']);
					$smarty->assign('CountValidContact',$_SESSION['smsFile']['CountValidContact']);
					$smarty->assign('CountInvalidContact',$_SESSION['smsFile']['CountInvalidContact']);
					$smarty->assign('CountDuplicateContact',count($arrDuplicate));
					$smarty->assign('senderid',$_SESSION['smsFile']['senderid']);
					$smarty->assign('arrTextMessage',$_SESSION['smsFile']['arrTextMessage']);
					$smarty->assign('smsCount',$_SESSION['smsFile']['creditNeeded']);
					$smarty->assign('crdBalance',$crdBalance);
					$smarty->assign('arrSampleSMS',$arrSampleSMS);
					$smarty->assign('action','SMSpreview');
					
					if($_SESSION['smsFile']['reminder_type']=='birthday'){
						$smarty->assign('repeat','birthday');
					}else{
						$smarty->assign('repeat','sms');
					}
				}
		break;
		
		case 'SMSpreview':
			$CreditNeeded	= array_sum($_SESSION['smsFile']['arrSMSCount']);
			if($CreditNeeded > 0){
				$objSms->userId			= $_SESSION['user']['id'];
				$objSms->status			= 3;
				$objSms->jobName	= 	cleanQuery($_REQUEST['jobname']);
				$objSms->textMassage	= str_replace('\r\n', ' ', cleanQuery($_SESSION['smsFile']['textMessage']));
				$objSms->senderID		= $_SESSION['smsFile']['senderid'];
				$objSms->credit			= $_SESSION['smsFile']['creditNeeded'];
				$objSms->massegeCount	= count($_SESSION['smsFile']['arrTextMessage']);
				$objSms->massegeLength	= strlen($_SESSION['smsFile']['textMessage']);
				$objSms->addressbookId	=	'FILE';
				$objSms->contactId	=	'FILE';
				$objSms->senderType		= 'VEN';
				
				if(isset($_SESSION['smsFile']['scheduleDate']) && $_SESSION['smsFile']['scheduleDate']!=''){
					$objSms->isScheduled	= 1;
					$objSms->scheduleTime	= getDateFormat($_SESSION['smsFile']['scheduleDate']);
				}
				
				if($objSms->smsPush()){
						
						$smsID=$objSms->getSmsStoresID2();
						$objSms->smsStoreID=$smsID;
						$objSms->blockCredit();
						$objSms->status			= 0;
						//$res=BlockRemoteCredit($_SESSION['user']['id'],$objSms->credit);
						 
						 
						// getting sender name
						$objSenderid-> id = $_SESSION['smsFile']['senderid'];;
						$sendername=$objSenderid->requestSenderName();
						$objSms->senderName = $sendername;
						 
						//Puting all session variables into simple variables for Easy access
						$arrTextMessage		=	$_SESSION['smsFile']['arrTextMessage'];
						$arrSMSCharacter	=	$_SESSION['smsFile']['arrSMSCharacter'];
						$arrSMSCount		=	$_SESSION['smsFile']['arrSMSCount']	;
						$arrMobileNo		=	$_SESSION['smsFile']['ValidContact'];
						$TotalContact 		= 	count($arrMobileNo);
						
						//Looping to all contact and messages
						for ($i=0; $i < $TotalContact; $i++){
							$objSms->textMassage	= cleanQuery($arrTextMessage[$i]);
							$objSms->massegeCount	= $arrSMSCount[$i];
							$objSms->massegeLength	= $arrSMSCharacter[$i];
							$objSms->mob_no			= trim($arrMobileNo[$i]);
							
							$objSms->insertIntoInboundMessage2();
							
							//updating Birthday Reminder
							if($_SESSION['smsFile']['repeat'] !='' && $_SESSION['smsFile']['reminder_type']=='birthday'){
								if($_SESSION['smsFile']['repeat_days'] =='' && $_SESSION['smsFile']['repeat']!='WEEK'){
									$_SESSION['smsFile']['repeat_days']="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
								}else if($_SESSION['smsFile']['repeat_days'] =='' && $_SESSION['smsFile']['repeat'] =='WEEK'){
									$sendDate	= date("m/d/Y H:i:s");// current date
									$weekday = date('l', strtotime($sendDate));
									$_SESSION['smsFile']['repeat_days'] = $weekday;
								}
								
								$currentDate = date("m/d/Y H:i:s");// current date
								if($_SESSION['smsFile']['end_date']==''){
									// date not specified so we make default after 10 years;
									$dateTenYearAdded = strtotime(date("m/d/Y H:i:s", strtotime($currentDate)) . " +10 year");
									$_SESSION['smsFile']['end_date'] = date('m/d/Y H:i:s', $dateTenYearAdded);
								}
								
								//finding the start date of the reminder
								$arrBirthday=$_SESSION['smsFile']['arrBirthday'];
								
								$birthday=date('m/d/Y H:i:s',strtotime($arrBirthday[$i]));
								//$birthday =date('m/d/Y H:i:s', $birthday);
								//$birthday=$arrBirthday[$i];
								$dbDate = date('Y', strtotime($birthday));
								$dateCurrent = date('Y', strtotime($currentDate));
								$dateDiff = $dateCurrent - $dbDate;
								//$dateDiff = $dateDiff - $_SESSION['smsFile']['repeat_every'];
								$yearToAdd = " +".$dateDiff." year";
								$sendDate = strtotime(date("m/d/Y H:i:s", strtotime($birthday)).$yearToAdd);
								//Put start date to sens sms at 10am
								$sendDate = date('m/d/Y', $sendDate);
								$sendDate=$sendDate." 10:0:0";
								
								//checking if send date is less than now
								$birthdayDate=strtotime($sendDate);
								$curentDate=strtotime(date("m/d/Y H:i:s"));
								if($birthdayDate<$curentDate){
									$yearToAdd = " + 1 year";
									$sendDate = strtotime(date("m/d/Y H:i:s", strtotime($sendDate)).$yearToAdd);
									$sendDate = date('m/d/Y H:i:s', $sendDate);
								}
								
								$objSms->scheduleTime = getDateFormat($sendDate);
								$objSms->repeat = $_SESSION['smsFile']['repeat'];
								$objSms->repeat_every = $_SESSION['smsFile']['repeat_every'];
								$objSms->repeat_days = $_SESSION['smsFile']['repeat_days'];
								$objSms->end_date = getDateFormat($_SESSION['smsFile']['end_date']);
								$objSms->reminder_type = "birthday";
								$objSms->contactId = trim($arrMobileNo[$i]);
								$objSms->insertReminder();
								
								//Finding Next send date
								//$objSms->updateNextSendDate();
								$objSms->updateReminderProcessed();
							}
						}
						
						//inserting to the reminder table reminder table
						if($_SESSION['smsFile']['repeat'] !='' && $_SESSION['smsFile']['reminder_type']=='sms'){
							if($_SESSION['smsFile']['repeat_days'] =='' && $_SESSION['smsFile']['repeat']!='WEEK'){
								$_SESSION['smsFile']['repeat_days']="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
							}else if($_SESSION['smsFile']['repeat_days'] =='' && $_SESSION['smsFile']['repeat'] =='WEEK'){
								$sendDate	= date("Y-m-d H:i:s");// current date
								$weekday = date('l', strtotime($sendDate));
								$_SESSION['smsFile']['repeat_days'] = $weekday;
							}
							
							if(isset($_SESSION['smsFile']['scheduleDate']) && $_SESSION['smsFile']['scheduleDate']!=''){
								$sendDate	= getDateFormat($_SESSION['smsFile']['scheduleDate']);
							}else{
								$sendDate	= date('m/d/Y H:i:s');// current date
							}
							
							if($_SESSION['smsFile']['end_date']==''){
								// date not specified so we make default after 10 years;
								$currentDate = date("m/d/Y H:i:s");// current date
								$dateTenYearAdded = strtotime(date("m/d/Y H:i:s", strtotime($currentDate)) . " +10 year");
								$_SESSION['smsFile']['end_date'] = date('m/d/Y H:i:s', $dateTenYearAdded);
							}
							
							
							$objSms->scheduleTime = getDateFormat($sendDate);
							$objSms->repeat = $_SESSION['smsFile']['repeat'];
							$objSms->repeat_every = $_SESSION['smsFile']['repeat_every'];
							$objSms->repeat_days = $_SESSION['smsFile']['repeat_days'];
							$objSms->end_date = getDateFormat($_SESSION['smsFile']['end_date']);
							$objSms->reminder_type = "sms";
							$objSms->contactId = "";
							$objSms->insertReminder();
							
							//Finding Next send date
							$objSms->updateNextSendDate();
							
						}
						
						
						$objSms->smsStoreID=$smsID;
						$objSms->reminderSent=0;
						$objSms->updateBroadcastReminder();
						//Disable sender clone from sending birthday reminders
						 if($_SESSION['smsFile']['repeat'] !='' && $_SESSION['smsFile']['reminder_type']=='birthday'){
						 	$objSms->UpdateIsProccessedForBirthday();
							$objSms->ReturnReminderCredit();
						 }else{
							$objSms->UpdateIsProccessed2();
						 }
						
						
						if(isset($_SESSION['smsFile']['scheduleDate']) && $_SESSION['smsFile']['scheduleDate']!=''){

							$msg = VEN_SMS_PUSH_MSG_03;

						}else{
							$msg = VEN_SMS_PUSH_MSG_04;
						}
						unset($_SESSION['smsFile']);
						$action	=	"uploadFile";
						$smarty->assign('action',$action);
						
					}
			}else{
				$msg = VEN_SMS_PUSH_MSG_05;
				$errorFlag = 1;
			}
		break;
		
		case 'backTo1':
			$smarty->assign('fileuploaded', $_SESSION['smsFile']['fileuploaded']);
			$action	= "uploadFile";
			$smarty->assign('action',$action);
		break;	
		case 'cancel':
			unset($_SESSION['smsFile']);
			$smarty->assign('action','uploadFile');
		break;	
		case 'backTo2':
			if($_SESSION['smsFile']['headerRow']==1){
				$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 1, 10);
			}else{
				$arrRecipient = array_slice($_SESSION['smsFile']['recipients'], 0, 9);
			}
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('headerRow',$_SESSION['smsFile']['headerRow']);
			$smarty->assign('arrColumn',$_SESSION['smsFile']['arrCols']);
			$smarty->assign('mobNoColumn',$_SESSION['smsFile']['mobNoColumn']);
			$smarty->assign('action','uploadedData');
		break;	
		case 'backTo3':
			$smarty->assign('senderid',$_SESSION['smsFile']['senderid']);
			$smarty->assign('textMessage',$_SESSION['smsFile']['textMessage']);
			$smarty->assign('scheduleDate',$_SESSION['smsFile']['scheduleDate']);
			$smarty->assign('arrColumn',$_SESSION['smsFile']['arrCols']);
			$smarty->assign('action','createMessage');
		break;
		
		default:
			unset($_SESSION['smsFile']);
			$action	="uploadFile";
			$smarty->assign('action',$action);
		break;
	}
	
	$objCredit = new credit();
	$objCredit->userId = $_SESSION['user']['id'];
	$creditBalance = $objCredit->getCreditBalance();
	
	//End of case ####
	$smarty->assign('arrsmstemplate',$arrsmstemplate);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('venCreditBalance',$creditBalance);
	$smarty->assign('currentYear',date("Y")+10);
	$smarty->assign('msg',$msg);
	$smarty->display("vendor/ven_smspush_file.tpl");
	
	include 'footer.php';
?>