<?php
/****************************************************************************************************************
*	File : ven_credit_purchage.php
*	Purpose: SMS puchage request for vendors
*	Author : Akhilesh
	Updated	08/09/2012 By Leonard Nyirenda
	-Script to send Credit Requestt Email to Admin added
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(MODEL.'credit/credit.class.php');
	include(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';
	//include(CLASSES.'SmartyPaginate.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
    //SmartyPaginate::connect();
   // SmartyPaginate::setLimit($project_vars['row_per_page']);
	
	$action = $_REQUEST['action'];

	$objAddressbook = new addressbook();
	$objCredit = new credit();	
	$objCredit->userId = $_SESSION['user']['id'];
	$errorFlag = 0;
	
	$objUser = new user();
	$objUser->userId = $_SESSION['user']['id'];
	$vendorDetail = $objUser->getVendorDetail();
	$objUser->parentVenId = $vendorDetail['parent_ven_id'];
	if($_SESSION['user']['parent_ven_id'] != ''){
		$resellerDetail = $objUser->getResellerDetail();
	}else{
		$resellerDetail['name'] ='Admin';
		$resellerDetail['email'] ='pspf@pspf-tz.org';
	}
	$parentVenId=$vendorDetail['parent_ven_id'];
	$smarty->assign('resellerDetails', $resellerDetail);
	
	
	switch($_REQUEST['action']){
		case 'crdtReg':
			$objCredit->creditRequest = $_REQUEST['creditRequest'];
			$objCredit->parentVenId = $vendorDetail['parent_ven_id'];
			$rowCredit = $objCredit->getCreditScheme();
			if($rowCredit){
				$totalCost= $_REQUEST['creditRequest'] * $rowCredit['rate'];
				$smarty->assign('totalCost',$totalCost);
				$smarty->assign('rowCredit',$rowCredit);
				
				if(isset($_REQUEST['creditRequest'])){
					$objCredit->crdtSchmId = $rowCredit['id'];
					$objCredit->creditRequest = $_REQUEST['creditRequest']; 
					$objCredit->creditTotalCost = $_REQUEST['creditRequest'] * $rowCredit['rate'];
					$objCredit->creditReqId = str_rand($project_vars["random_creditid_length"],'caps_alphanum');
					$objCredit->parentVenId = $_SESSION['user']['parent_ven_id'];
					if($objCredit->requestCredit()){
						$msg = VEN_CHANGE_CREDIT_MSG_1;
						$action='conf';
						$smarty->assign('creditReqId',$objCredit->creditReqId);
						$smarty->assign('requestTime',date($project_vars['date_time_format_short_php']));
						$smarty->assign('creditRequest',$_REQUEST['creditRequest']);
						$smarty->assign('creditTotalCost',$objCredit->creditTotalCost);
			
						//sendind mail
						$smarty->assign('req_id',$objCredit->creditReqId);
						$smarty->assign("quantity", $_REQUEST['creditRequest']);
						$smarty->assign("rate", $rowCredit['rate']);
						$smarty->assign("total", $objCredit->creditTotalCost);
						$smarty->assign("status", 'Pending');
						$smarty->assign("PayStatus", 'Not Paid');
						$smarty->assign("parentVenId", $_SESSION['user']['parent_ven_id']);
						
						if($project_vars["smtp_auth"]===true){
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
							->setUsername($project_vars["smtp_username"])
							->setPassword($project_vars["smtp_password"]);
						}else{	
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
						}

						$mailer = Swift_Mailer::newInstance($transport);
						$body = $smarty->fetch('mail_template/mail_change_status.tpl');
						$message = Swift_Message::newInstance('Credit Request')
						->setFrom(array($vendorDetail['email']=>$vendorDetail['name']))
						->setTo(array($resellerDetail['email'] =>$resellerDetail['name'],$vendorDetail['email']=>$vendorDetail['name']))
						->setBody($body );
						$message->setContentType("text/html");

						//Send the message
						$result = $mailer->send($message);
					}else{
						$msg = VEN_CHANGE_CREDIT_MSG_2;
						$errorFlag =1;
					}
				
				}
				
				$action='crdtReg';
			}else{
				$msg='No credit scheme fou nd for number of sms you want to purchase';
				$errorFlag=1;
				$action='crdtReg';
			}
			
		break;
		case 'confirm':
			if($_REQUEST['creditRequest']==$_REQUEST['oldCreditRequest']){
				$objCredit->crdtSchmId = $_REQUEST['creditScheme'];
				$objCredit->creditRequest = $_REQUEST['creditRequest']; 
				$objCredit->creditTotalCost = $_REQUEST['creditTotalCost'];
				$objCredit->creditReqId = str_rand($project_vars["random_creditid_length"],'caps_alphanum');
				$objCredit->parentVenId = $_SESSION['user']['parent_ven_id'];
				if($objCredit->requestCredit()){
					$msg = VEN_CHANGE_CREDIT_MSG_1;
					$action='conf';
					$smarty->assign('creditReqId',$objCredit->creditReqId);
					$smarty->assign('requestTime',date($project_vars['date_time_format_short_php']));
					$smarty->assign('creditRequest',$_REQUEST['creditRequest']);
					$smarty->assign('creditTotalCost',$_REQUEST['creditTotalCost']);
			
					//sendind mail
					$smarty->assign('req_id',$objCredit->creditReqId);
					$smarty->assign("quantity", $_REQUEST['creditRequest']);
					$smarty->assign("rate", $_REQUEST['creditRate']);
					$smarty->assign("total", $_REQUEST['creditTotalCost']);
					$smarty->assign("status", 'Pending');
					$smarty->assign("PayStatus", 'Not Paid');
					$smarty->assign("parentVenId", $_SESSION['user']['parent_ven_id']);
						
					if($project_vars["smtp_auth"]===true){
						$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
						->setUsername($project_vars["smtp_username"])
						->setPassword($project_vars["smtp_password"]);
					}else{	
						$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
					}

					$mailer = Swift_Mailer::newInstance($transport);
					$body = $smarty->fetch('mail_template/mail_change_status.tpl');
					$message = Swift_Message::newInstance('Credit Request')
					->setFrom(array($vendorDetail['email']=>$vendorDetail['name']))
					->setTo(array($resellerDetail['email'] =>$resellerDetail['name'],$vendorDetail['email']=>$vendorDetail['name']))
					->setBody($body );
					$message->setContentType("text/html");

					//Send the message
					$result = $mailer->send($message);
				}else{
					$msg = VEN_CHANGE_CREDIT_MSG_2;
					$errorFlag =1;
				}
				
			}else{
				$objCredit->creditRequest = $_REQUEST['creditRequest'];
				$objCredit->parentVenId = $vendorDetail['parent_ven_id'];
				$rowCredit = $objCredit->getCreditScheme();
				if($rowCredit){
					$totalCost= $_REQUEST['creditRequest'] * $rowCredit['rate'];
					$smarty->assign('totalCost',$totalCost);
					$smarty->assign('rowCredit',$rowCredit);
					$action='confirm';
				}else{
					$msg='No credit scheme fou nd for number of sms you want to purchase';
					$errorFlag=1;
					$action='crdtReg';
				}
			}
			
		break;
		default:
			$action	="crdtReg";
		break;
		
	}

	$objAddressbook->userId = $_SESSION['user']['id'];
	$objCredit->parentVenId = $_SESSION['user']['parent_ven_id'];
	// addressbook listing
	$arrAddressbook = $objAddressbook->getAddressbookList(false);
	$arrCreditScheme = $objCredit->getCrditSchemes();
   	$creditBalance = $objCredit->getCreditBalance();


	$smarty->assign('msg',$msg);
	$smarty->assign('action',$action);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('creditBalance',$creditBalance);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('arrCreditScheme',$arrCreditScheme);
	$smarty->assign('creditRequest',$_REQUEST['creditRequest']);
	$smarty->assign('selfUrl',BASE_URL.'ven_credit_purchage.php');
	$smarty->assign('ajaxUrl',BASE_URL.'credit_ajax.php');
	//SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_credit_purchage.tpl");
	//SmartyPaginate::disconnect();
	include 'footer.php';
?>
