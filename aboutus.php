<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('aboutus.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/aboutus.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/aboutus.tpl');
		}
		else
		{
			$smarty->display('aboutus.tpl');
		}
	}
	
	include('footer.php');
?>