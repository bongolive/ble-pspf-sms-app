<?php
	ob_start();
	$logout_url   = '';
	$username     = '';
	$welcome      = '';
	$logout       = '';
	$base_url	  = BASE_URL;
	$current_url  = $_SERVER['SCRIPT_NAME'];
	$cur_arr      = explode('/', $current_url);
	$curPage      = $cur_arr[count($cur_arr) - 1]; 
	
	// simple true or false test
	if($mobile==true && $_SESSION['mobile'] != 'bongolive'){
  		header("location: mobile/index.php");
	}
	if($curPage!='ven_smspush.php'){
		unset($_SESSION['sms']);	
	}
	
	if($curPage!='ven_smspush_file.php'){
		unset($_SESSION['smsFile']);	
	}
	
	if($curPage!='ven_smspush_quick.php'){
		unset($_SESSION['smsQuick']);	
	}
	
	
	if(isset($_SESSION['user']) && $_SESSION['user']!=''){
			 $welcome      = HEADER_WELCOME;
			 $logout       = HEADER_LOGOUT;
			 $username     = $_SESSION['user']['name'];
			 $logout_url   = BASE_URL.'logout.php';
			// $links        = get_menus($url,'menus',$cur_page,'','class=current');
			 $login       = true;
	}else{
		
		 $login       = false;
	}
	 //echo $_SERVER['SCRIPT_NAME'];
	//$smarty->assign("links", $links);
	
	if(array_key_exists($curPage,$project_vars['title'])){
	
		$title = $project_vars['title'][$curPage];
	}else{
		$title = $project_vars['title']['default'];
	}

	$smarty->assign("curPage", $curPage);
	$smarty->assign("base_url", $base_url);
	$smarty->assign("userType", $_SESSION['user']['user_type']); // assign user's type to tpl file
	$smarty->assign("login", $login);
	$smarty->assign("welcome", $welcome);
	$smarty->assign("logout", $logout);
	$smarty->assign("username", $username);
	$smarty->assign("logout_url", $logout_url);
	$smarty->assign("title", $title);
	$smarty->display("header.tpl");

?>