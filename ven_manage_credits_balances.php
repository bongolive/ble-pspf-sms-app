<?php
	
	/********************************************
	*	File	: ven_manage_credits_balances.php				*
	*	Purpose	: Vendor crdit  balance for sub vendor *
	*	Author	: Leonard Nyirenda						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'credit/credit.class.php');
	include(MODEL.'vendors/vendor.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	require_once 'swift/lib/swift_required.php';


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}
	
	if($_SESSION['user']['parent_ven_id']==''){
		(int) $remoteCredit = getRemoteCreditBalance();
		(int) $resCredit = getTotalCreditOfAllVendors();
		$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
	} 


	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	$resellerDetail = $objUser->getResellerDetail();
	$smarty->assign('resellerDetails', $resellerDetail);
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	$objCredit = new credit();
	$objCredit->parentVenId = $_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'edit':
			$objCredit->creditRequestId = $_REQUEST['id'];
			$arrCreditDetails = $objCredit->getRequestedCreditDetails();
			$smarty->assign('arrCreditScheme',getArray('credit_schemes','id','rate',' ORDER BY rate ASC'));
			$smarty->assign('arrCreditBonus',getArray('credit_bonus','id','rate',' ORDER BY rate ASC'));
		break;
		
		case'update':
			//$crdtSchem = explode("###",$_REQUEST['crdtSchmId']);
			$objCredit->creditRate = $_REQUEST['crdtSchmId'];
			$tmp_rate = $objCredit->getCrdtSchm();
			if ($tmp_rate){
				$objCredit->crdtSchmId 	= $tmp_rate['id'];	
			}else {
				$bon = $objCredit->getCrdtBon();
				if($bon){
					$objCredit->crdtSchmId 	= $bon['id'];
				} else {
					$sch = $objCredit->addCreditBonus();
					$objCredit->crdtSchmId 	= $sch['id'];
				}
			}	
			$objCredit->creditRequestId 			= $_REQUEST['id'];
			$objCredit->creditRequestStatus 		= $_REQUEST['status'];
			$objCredit->creditRequestPaymentStatus 		= $_REQUEST['payment_status'];
			$objCredit->creditRequest 			= $_REQUEST['crditRequested'];
			//$objCredit->crdtSchmId 				= $crdtSchem[0];
			//$objCredit->creditTotalCost 			= (intval($crdtSchem[1])*intval($_REQUEST['crditRequested']));
			$objCredit->creditTotalCost 			= (intval($_REQUEST['crdtSchmId'])*intval($_REQUEST['crditRequested']));
			$arrCreditDetails = $objCredit->getRequestedCreditDetails();
			$status = $objCredit-> updateRequestedCredit();
			//var_dump($arrCreditDetails['credit_request_id']);
			$objUser->userId = $_REQUEST['userId'];
			$userDetail = $objUser-> getUserDetailVen();

			if($status) {
				if (($_REQUEST['payment_status']!=$_REQUEST['old_payment_status'] || $_REQUEST['status']!=$_REQUEST['old_status']) && $project_vars['mail_send']===true){	
					$smarty->assign('req_id',$arrCreditDetails['credit_request_id']);

					if ((($_REQUEST['payment_status']==1 && $_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) || $_REQUEST['status']!=$_REQUEST['old_status']) && $project_vars['mail_send']===true){	
						//$tmp_rate = explode('###', $_REQUEST['crdtSchmId']);
						//$objCredit->crdtSchmId = $tmp_rate[0];
						//$rate = $objCredit->getCrdtSchmDetails();

						switch($_REQUEST['payment_status']){
							case '0': $pay = 'Not paid';break;
							case '1': $pay = 'Paid';break;
							case '2': $pay = 'Partially Paid';break;
							case '3': $pay = 'Receipt Sent';break;
							case '4': $pay = 'Bonus';break;
						}

						$smarty->assign('quantity', $_REQUEST['crditRequested']);
						//$smarty->assign('rate', $rate['rate']);
						$smarty->assign('rate', $_REQUEST['crdtSchmId']);
						//$smarty->assign('total', $_REQUEST['crditRequested']*$rate['rate']);
						$smarty->assign('total', $_REQUEST['crditRequested']*$_REQUEST['crdtSchmId']);
						$smarty->assign('status', $_REQUEST['status']);
						$smarty->assign('PayStatus', $pay);
									
						//if ($_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) {
						if ($_REQUEST['payment_status']==1 && $_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) {
							$smarty->assign('payment_true', true);
							$smarty->assign('newPayStatus', $pay);
						}
						if ($_REQUEST['status']!=$_REQUEST['old_status']) {
							$smarty->assign('status_true', true);
							$smarty->assign('newStatus', $_REQUEST['status']);
						}

						try{
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);
							$body = $smarty->fetch('mail_template/mail_change_status.tpl');
							//var_dump($body);
							$message = Swift_Message::newInstance('Status of Purchase Request')
							->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
							->setTo(array($userDetail['email']=>$userDetail['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);

						} catch (Exception $e){
							$msg = SUB_REGISTER_MSG_08.$e->getMessage();
							$errorFlag = 1;
						
						try{
							if($project_vars['mob_confirmation']==true){		
								$massege = str_rand($project_vars["random_mob_conf_length"],$project_vars["random_string_type"]);
								smsSendWithCurl($userDetail['mob_no'],str_replace('MESSAGE',$massege,$project_vars['register_message']),$project_vars['sms_api_senderid']);
								$objUser->mob_conf_msg = $massege;
								$objUser->insertSubMobConfMsg();
							}
						}catch(Exception $objE){
							$msg = SUB_REGISTER_MSG_09.$objE->getMessage();;
							$errorFlag = 1;
						}
					}
				}			
			}
			
			if($status && $_REQUEST['status'] == 'allocated'){
				$msg = "Credits allocated to vendors";
			}elseif($status && $_REQUEST['status'] == 'rejected'){
				$msg = "Credits request rejected for vendors";
			}
			}
			$action = "";	
		break;
		
		default:
			$action = "";
		break;
	
	}

	
	$selfUrl = BASE_URL.'ven_manage_credits_balances.php';
	$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
	$pagingUrl.= '&username='.$_REQUEST['username'];
	$pagingUrl.= '&vendor_type='.$_REQUEST['vendor_type'];
	SmartyPaginate::setUrl($pagingUrl);


	$objCredit->vendorName		= $_REQUEST['vendorName'];
	$objCredit->username		= $_REQUEST['username'];
	$objCredit->vendor_type		= $_REQUEST['vendor_type'];

	$objCreditBalance =  $objCredit->getCreditsSubVenBalanceList();
	
	$smarty->assign("msg",$msg);
	$smarty->assign("vendorName",$_REQUEST['vendorName']);
	$smarty->assign("username",$_REQUEST['username']);
	$smarty->assign("vendor_type", $_REQUEST['vendor_type']);
	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("action",$action); 
	$smarty->assign("arrCreditBalances",$arrCreditBalances);
	$smarty->assign("objCreditBalance",$objCreditBalance);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_credit_balances.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
