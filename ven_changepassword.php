<?php
/****************************************************************************************************************
*	File : ven_changepassword.php
*	Purpose: changing password for vendors
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';
	
	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objUser = new user();
	$objUser->userId = $_SESSION['user']['id'];
	$vendorDetail = $objUser->getVendorDetail();
	
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='changpass'){
		
			$objUser->oldPassword=trim($_REQUEST['old_password']);
			$objUser->password=trim($_REQUEST['password']);
			$objUser->table="mst_vendors";
			$status = $objUser->changePassword();
			if($status==1){
				
				//Create the Transport
				if($project_vars["smtp_auth"]===true){
					$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
					->setUsername($project_vars["smtp_username"])
					->setPassword($project_vars["smtp_password"]);
				}else{
				
					$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
				}

				//Create the Mailer using your created Transport
				$mailer = Swift_Mailer::newInstance($transport);

				//Create a message
				$smarty->assign('newPassword',$objUser->password);
				$body = $smarty->fetch('mail_template/mail_change_pass.tpl');

				$message = Swift_Message::newInstance('Password changed')
				->setFrom(array($project_vars["smtp_username"] => 'admin'))
				->setTo(array($vendorDetail['email']=>$vendorDetail['name']))
				->setBody($body );
				$message->setContentType("text/html");

				//Send the message
				$result = $mailer->send($message);
				
				
				if($project_vars['send_message_to_mob']){
					smsSendWithCurl($vendorDetail['mob_no'],$project_vars['password_change_message'].$objUser->password,$project_vars['sms_api_senderid']);
					$msg = VEN_CHANGE_PSWD_MSG_1;
				}else{
					$msg = VEN_CHANGE_PSWD_MSG_2;
				}
			}elseif($status==0){
				$msg = VEN_CHANGE_PSWD_MSG_3;
			}elseif($status==2){
				$msg = VEN_CHANGE_PSWD_MSG_4;
			
			}
	}

	$userDetail = $objUser-> getUserDetailVen();
   
	$smarty->assign('userDetail',$userDetail);
	$smarty->assign('msg',$msg);
	$smarty->assign("action",$_REQUEST['action']);
	$smarty->display("vendor/ven_changepass.tpl");

	include 'footer.php';

?>