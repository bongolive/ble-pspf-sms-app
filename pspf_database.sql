-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 11, 2013 at 05:02 PM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pspf`
--

-- --------------------------------------------------------

--
-- Table structure for table `addressbooks`
--

CREATE TABLE IF NOT EXISTS `addressbooks` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `addressbook` varchar(200) CHARACTER SET latin1 NOT NULL,
  `vendor_id` char(36) CHARACTER SET latin1 NOT NULL,
  `contacts_count` int(12) NOT NULL,
  `description` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vendor_id` (`vendor_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addressbooks`
--

INSERT INTO `addressbooks` (`id`, `addressbook`, `vendor_id`, `contacts_count`, `description`, `created`, `modified`) VALUES
('b78b9726-1a85-11e2-961b-001f3c930a21', 'Default', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 1, '', '2012-10-20 10:13:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `age_ranges`
--

CREATE TABLE IF NOT EXISTS `age_ranges` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `age_range` varchar(10) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `age_ranges`
--


-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `category` varchar(200) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `active`, `created`, `modified`) VALUES
('b8bc5c96-c0c8-11df-8f82-000c29b8e356', 'Food & Drinks', 1, '2010-09-15 18:24:48', '0000-00-00 00:00:00'),
('b8bc5fca-c0c8-11df-8f82-000c29b8e356', 'Beauty, Fashion & Clothing', 1, '2010-09-15 18:25:04', '2010-10-07 09:55:10'),
('b8bc6038-c0c8-11df-8f82-000c29b8e356', 'Music, Entertainment & Sports', 1, '2010-09-15 18:25:20', '0000-00-00 00:00:00'),
('b8bc6074-c0c8-11df-8f82-000c29b8e356', 'Career & Education', 1, '2010-09-15 18:25:39', '0000-00-00 00:00:00'),
('b8bc60a6-c0c8-11df-8f82-000c29b8e356', 'Mobiles, Electronics & Computers', 1, '2010-09-15 18:25:57', '0000-00-00 00:00:00'),
('b8bc60d8-c0c8-11df-8f82-000c29b8e356', 'Home, Decor & Furnishings', 1, '2010-09-15 18:26:15', '0000-00-00 00:00:00'),
('b8bc610a-c0c8-11df-8f82-000c29b8e356', 'Financial Products', 1, '2010-09-15 18:26:43', '0000-00-00 00:00:00'),
('b8bc613c-c0c8-11df-8f82-000c29b8e356', 'Business Services', 1, '2010-09-15 18:26:58', '0000-00-00 00:00:00'),
('b8bc6164-c0c8-11df-8f82-000c29b8e356', 'Hardware & Equipment', 1, '2010-09-15 18:27:18', '0000-00-00 00:00:00'),
('b8bc61a0-c0c8-11df-8f82-000c29b8e356', 'Travel & Holiday', 1, '2010-09-15 18:27:39', '0000-00-00 00:00:00'),
('b8bc61d2-c0c8-11df-8f82-000c29b8e356', 'Real Estate & Services', 1, '2010-09-15 18:27:55', '0000-00-00 00:00:00'),
('b8bc6204-c0c8-11df-8f82-000c29b8e356', 'Internet & Services', 1, '2010-09-15 18:28:08', '0000-00-00 00:00:00'),
('b8bc6236-c0c8-11df-8f82-000c29b8e356', 'Cars, Bikes and Automotive', 1, '2010-09-15 18:28:22', '0000-00-00 00:00:00'),
('b8bc6268-c0c8-11df-8f82-000c29b8e356', 'Jobs & Employment', 1, '2010-09-15 18:28:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `city` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city`, `created`, `modified`) VALUES
('a2d35bf6-bfc3-11df-8f82-000c29b8e356', 'Arusha', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3701e-bfc3-11df-8f82-000c29b8e356', 'Bagamoyo', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d37d70-bfc3-11df-8f82-000c29b8e356', 'Bukoba', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d38ab8-bfc3-11df-8f82-000c29b8e356', 'Chalinze', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d398aa-bfc3-11df-8f82-000c29b8e356', 'Dar-es-Salaam', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3a53e-bfc3-11df-8f82-000c29b8e356', 'Dodoma', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3b128-bfc3-11df-8f82-000c29b8e356', 'Iringa', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3bd8a-bfc3-11df-8f82-000c29b8e356', 'Kibaha', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3cafa-bfc3-11df-8f82-000c29b8e356', 'Kigoma', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3d6d0-bfc3-11df-8f82-000c29b8e356', 'Lindi', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3e328-bfc3-11df-8f82-000c29b8e356', 'Lushoto', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3ef4e-bfc3-11df-8f82-000c29b8e356', 'Mbeya', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d3fab6-bfc3-11df-8f82-000c29b8e356', 'Morogoro', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d40862-bfc3-11df-8f82-000c29b8e356', 'Moshi', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d413de-bfc3-11df-8f82-000c29b8e356', 'Mtwara', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d41ff0-bfc3-11df-8f82-000c29b8e356', 'Musoma', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d42d60-bfc3-11df-8f82-000c29b8e356', 'Mwanza', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d438fa-bfc3-11df-8f82-000c29b8e356', 'Shinyanga', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d4450c-bfc3-11df-8f82-000c29b8e356', 'Singida', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d4510a-bfc3-11df-8f82-000c29b8e356', 'Songea', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d45c9a-bfc3-11df-8f82-000c29b8e356', 'Sumbawanga', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d46a64-bfc3-11df-8f82-000c29b8e356', 'Tabora', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d4754a-bfc3-11df-8f82-000c29b8e356', 'Tanga', '2010-09-14 11:18:01', '0000-00-00 00:00:00'),
('a2d47fd6-bfc3-11df-8f82-000c29b8e356', 'Zanzibar', '2010-09-14 11:18:01', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `addressbook_id` char(36) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(25) CHARACTER SET latin1 NOT NULL,
  `mob_no2` varchar(14) NOT NULL,
  `title` varchar(10) CHARACTER SET latin1 NOT NULL,
  `fname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `lname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birth_date` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `area` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL DEFAULT 'Tanzania',
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `optional_one` varchar(255) CHARACTER SET latin1 NOT NULL,
  `optional_two` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `addressbook_id` (`addressbook_id`,`mob_no`),
  KEY `addressbook_id_2` (`addressbook_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `addressbook_id`, `mob_no`, `mob_no2`, `title`, `fname`, `lname`, `gender`, `birth_date`, `address`, `area`, `city`, `country`, `email`, `optional_one`, `optional_two`, `created`, `modified`) VALUES
('70c6728e-872e-11e2-b563-001f3c930a21', 'b78b9726-1a85-11e2-961b-001f3c930a21', '757447737', '', '', 'mimi', '', '', '1970-01-01', '', '', '', '', '', '', '', '2013-03-07 16:53:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE IF NOT EXISTS `countries` (
  `country_code` varchar(4) NOT NULL,
  `country` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_code`, `country`, `created`, `modified`) VALUES
('255', 'Tanzania', '2012-10-22 02:45:35', '0000-00-00 00:00:00'),
('254', 'Kenya', '2012-10-22 03:16:38', '2012-10-22 03:16:38'),
('256', 'Uganda', '2012-10-22 05:12:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `credit_balances`
--

CREATE TABLE IF NOT EXISTS `credit_balances` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_balance` int(11) NOT NULL,
  `credit_blocked` int(12) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `sms_reminder` char(1) NOT NULL DEFAULT '0',
  `email_reminder` char(1) NOT NULL DEFAULT '0',
  `credit_level` int(11) NOT NULL DEFAULT '25',
  `reminder_sent` char(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_balances`
--

INSERT INTO `credit_balances` (`id`, `user_id`, `credit_balance`, `credit_blocked`, `created`, `modified`, `sms_reminder`, `email_reminder`, `credit_level`, `reminder_sent`) VALUES
('66b4e74c-dddd-11df-9aa0-0013725528cc', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 7, 26, '2010-10-22 06:07:51', '2012-07-06 15:58:43', '1', '1', 250, '0');

-- --------------------------------------------------------

--
-- Table structure for table `credit_bonus`
--

CREATE TABLE IF NOT EXISTS `credit_bonus` (
  `id` varchar(36) CHARACTER SET latin1 NOT NULL,
  `rate` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_bonus`
--


-- --------------------------------------------------------

--
-- Table structure for table `credit_requests`
--

CREATE TABLE IF NOT EXISTS `credit_requests` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_request_id` varchar(50) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit_scheme_id` varchar(36) CHARACTER SET latin1 NOT NULL,
  `credit_requested` int(12) NOT NULL,
  `total_cost` double NOT NULL,
  `status` enum('allocated','rejected','pending') CHARACTER SET latin1 NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `user_id_2` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_requests`
--


-- --------------------------------------------------------

--
-- Table structure for table `credit_schemes`
--

CREATE TABLE IF NOT EXISTS `credit_schemes` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `start_range` int(12) NOT NULL,
  `end_range` int(12) NOT NULL,
  `rate` int(12) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `order` int(3) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `credit_schemes`
--


-- --------------------------------------------------------

--
-- Table structure for table `inbound_messages`
--

CREATE TABLE IF NOT EXISTS `inbound_messages` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `sms_store_id` char(36) CHARACTER SET latin1 NOT NULL,
  `user_id` char(36) CHARACTER SET latin1 NOT NULL,
  `source_addr_ton` int(11) DEFAULT NULL,
  `schedule_delivery_time` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `protocol_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `short_message` text CHARACTER SET latin1,
  `msg_length` int(11) NOT NULL DEFAULT '0',
  `msg_count` int(11) NOT NULL DEFAULT '0',
  `dest_addr_npi` int(11) DEFAULT NULL,
  `source_addr` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `validity_period` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `registered_delivery` int(11) DEFAULT NULL,
  `dest_addr_ton` int(11) DEFAULT NULL,
  `data_coding` int(11) DEFAULT NULL,
  `service_type` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `replace_if_present_flag` int(11) DEFAULT NULL,
  `priority_flag` int(11) DEFAULT NULL,
  `destination_addr` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `destination_addr_formatted` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `esm_class` int(11) NOT NULL,
  `sm_default_msg_id` bigint(20) NOT NULL,
  `source_addr_npi` int(11) NOT NULL,
  `circle_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `preferred_channel_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `channel_id` char(36) CHARACTER SET latin1 DEFAULT NULL,
  `remote_id` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `remote_status` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `remote_push_time` datetime DEFAULT NULL,
  `remote_rcv_time` datetime DEFAULT NULL,
  `remote_deliv_time` datetime DEFAULT NULL,
  `remote_deliver_string` text CHARACTER SET latin1,
  `remote` tinyint(1) NOT NULL,
  `user_message_id` varchar(10) DEFAULT NULL,
  `callbackurl_is_called` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_store_id` (`sms_store_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`),
  KEY `source_addr` (`source_addr`),
  KEY `created` (`created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `inbound_messages`
--

INSERT INTO `inbound_messages` (`id`, `sms_store_id`, `user_id`, `source_addr_ton`, `schedule_delivery_time`, `protocol_id`, `status`, `short_message`, `msg_length`, `msg_count`, `dest_addr_npi`, `source_addr`, `validity_period`, `registered_delivery`, `dest_addr_ton`, `data_coding`, `service_type`, `replace_if_present_flag`, `priority_flag`, `destination_addr`, `destination_addr_formatted`, `esm_class`, `sm_default_msg_id`, `source_addr_npi`, `circle_id`, `preferred_channel_id`, `channel_id`, `remote_id`, `remote_status`, `remote_push_time`, `remote_rcv_time`, `remote_deliv_time`, `remote_deliver_string`, `remote`, `user_message_id`, `callbackurl_is_called`, `created`, `modified`) VALUES
('64e6e7a4-872d-11e2-b563-001f3c930a21', '64e67491-872d-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', NULL, NULL, NULL, 2, 'testing file sms in pspf', 24, 1, NULL, 'sajili', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '757447737', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '1', '2013-03-07 16:47:06', '2013-03-07 16:47:06', '2013-03-07 16:47:06', NULL, 0, '', 0, '2013-03-07 16:46:11', '2013-03-07 16:47:06'),
('f1240852-a1db-11e2-ba7d-001f3c930a21', 'f117a240-a1db-11e2-ba7d-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', NULL, NULL, NULL, 0, 'mambo vp ndugu leonard nyirenda', 31, 1, NULL, 'menu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '757447737', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', 0, '2013-04-10 15:41:01', '0000-00-00 00:00:00'),
('a255021a-872e-11e2-b563-001f3c930a21', '89dd0325-872e-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', NULL, NULL, NULL, 2, 'leonard anatest group sms', 25, 1, NULL, 'menu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '757447737', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '1', '2013-03-07 16:55:06', '2013-03-07 16:55:06', '2013-03-07 16:55:06', NULL, 0, NULL, 0, '2013-03-07 16:55:02', '2013-03-07 16:55:06'),
('5248ae74-872b-11e2-b563-001f3c930a21', '52305f2b-872b-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', NULL, NULL, NULL, 2, 'hey im testing quick sms update for pspf', 40, 1, NULL, 'benefit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '757447737', NULL, 0, 0, 0, NULL, NULL, NULL, NULL, '1', '2013-03-07 16:32:07', '2013-03-07 16:32:07', '2013-03-07 16:32:07', NULL, 0, '', 0, '2013-03-07 16:31:21', '2013-03-07 16:32:07');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `lang` varchar(5) NOT NULL,
  `title` varchar(128) NOT NULL,
  PRIMARY KEY (`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`lang`, `title`) VALUES
('en_us', 'English'),
('sw_tz', 'Swahili');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `country_code` varchar(4) NOT NULL DEFAULT '255',
  `location` varchar(255) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `country_code`, `location`, `created`, `modified`) VALUES
('600d6176-bf42-11df-8f82-000c29b8e356', '255', 'Arusha', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600d7670-bf42-11df-8f82-000c29b8e356', '255', 'Bagamoyo', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600d832c-bf42-11df-8f82-000c29b8e356', '255', 'Bukoba', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600d90ba-bf42-11df-8f82-000c29b8e356', '255', 'Chalinze', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600d9e16-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Buguruni', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600daec4-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Chang''ombe', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600dbbda-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Ilala', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600dcb8e-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Kariakoo', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600dd912-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Kawe', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600de736-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Kigamboni', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600df398-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Kijitonyama', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e0112-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Kunduchi', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e0f54-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Magomeni', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e1bc0-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Manzese', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e2a7a-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Masaki', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e37ea-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Mbagala', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e4456-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Mbezi', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e5270-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Mikocheni', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e6062-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Mwenge', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e6cd8-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Other', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e7bb0-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Tabata', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e88f8-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Tandika', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600e96c2-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Town', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ea338-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Ubungo', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600eb09e-bf42-11df-8f82-000c29b8e356', '255', 'Dar - Upanga', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ebecc-bf42-11df-8f82-000c29b8e356', '255', 'Dodoma', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ecc00-bf42-11df-8f82-000c29b8e356', '255', 'Iringa', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ed970-bf42-11df-8f82-000c29b8e356', '255', 'Kibaha', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ee8ca-bf42-11df-8f82-000c29b8e356', '255', 'Kigoma', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600ef5e0-bf42-11df-8f82-000c29b8e356', '255', 'Lindi', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f068e-bf42-11df-8f82-000c29b8e356', '255', 'Lushoto', '2010-09-13 19:52:44', '2010-10-07 09:47:34'),
('600f148a-bf42-11df-8f82-000c29b8e356', '255', 'Mbeya', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f222c-bf42-11df-8f82-000c29b8e356', '255', 'Morogoro', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f2e98-bf42-11df-8f82-000c29b8e356', '255', 'Moshi', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f3d8e-bf42-11df-8f82-000c29b8e356', '255', 'Mtwara', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f4ad6-bf42-11df-8f82-000c29b8e356', '255', 'Musoma', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f571a-bf42-11df-8f82-000c29b8e356', '255', 'Mwanza', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f6548-bf42-11df-8f82-000c29b8e356', '255', 'Shinyanga', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f754c-bf42-11df-8f82-000c29b8e356', '255', 'Singida', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f837a-bf42-11df-8f82-000c29b8e356', '255', 'Songea', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f8f96-bf42-11df-8f82-000c29b8e356', '255', 'Sumbawanga', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600f9cde-bf42-11df-8f82-000c29b8e356', '255', 'Tabora', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600faa08-bf42-11df-8f82-000c29b8e356', '255', 'Tanga', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('600fb43a-bf42-11df-8f82-000c29b8e356', '255', 'Zanzibar', '2010-09-13 19:52:44', '0000-00-00 00:00:00'),
('a9bdfa1e-1c43-11e2-b9aa-0013725528cc', '254', 'kisumu', '2012-10-22 15:26:11', '0000-00-00 00:00:00'),
('0a0c0f00-1c44-11e2-b9aa-0013725528cc', '256', 'Kampala', '2012-10-22 15:28:52', '0000-00-00 00:00:00'),
('109d43fc-1c44-11e2-b9aa-0013725528cc', '256', 'Jinja', '2012-10-22 15:29:03', '2012-10-22 15:40:06'),
('67931c5c-1c46-11e2-b9aa-0013725528cc', '254', 'Nairobi', '2012-10-22 15:45:48', '0000-00-00 00:00:00'),
('766504e8-1c46-11e2-b9aa-0013725528cc', '254', 'Mombasa', '2012-10-22 15:46:13', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mst_admins`
--

CREATE TABLE IF NOT EXISTS `mst_admins` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `username` varchar(200) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(20) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(200) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_admins`
--


-- --------------------------------------------------------

--
-- Table structure for table `mst_vendors`
--

CREATE TABLE IF NOT EXISTS `mst_vendors` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `username` varchar(200) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `vendor_id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mob_no` varchar(20) CHARACTER SET latin1 NOT NULL,
  `phone` varchar(25) CHARACTER SET latin1 NOT NULL,
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `email` varchar(200) CHARACTER SET latin1 NOT NULL,
  `url` varchar(50) NOT NULL,
  `city_id` char(36) CHARACTER SET latin1 NOT NULL,
  `location_id` char(36) CHARACTER SET latin1 NOT NULL,
  `people_range_id` char(36) CHARACTER SET latin1 NOT NULL,
  `postal_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `physical_address` varchar(255) CHARACTER SET latin1 NOT NULL,
  `organisation` varchar(255) CHARACTER SET latin1 NOT NULL,
  `organisation_id` char(36) CHARACTER SET latin1 NOT NULL,
  `vendor_type` int(1) NOT NULL COMMENT '1=Individual,2=Organisation',
  `reg_step` tinyint(1) NOT NULL DEFAULT '0',
  `mob_conf_msg` varchar(20) CHARACTER SET latin1 NOT NULL,
  `mob_confirmed` tinyint(1) NOT NULL,
  `term_conditions` tinyint(1) NOT NULL,
  `callback_url` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  `broadcast_reminder` tinyint(1) NOT NULL DEFAULT '1',
  `broadcast_reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `location_id` (`location_id`,`people_range_id`,`active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mst_vendors`
--

INSERT INTO `mst_vendors` (`id`, `username`, `password`, `vendor_id`, `mob_no`, `phone`, `name`, `email`, `url`, `city_id`, `location_id`, `people_range_id`, `postal_address`, `physical_address`, `organisation`, `organisation_id`, `vendor_type`, `reg_step`, `mob_conf_msg`, `mob_confirmed`, `term_conditions`, `callback_url`, `active`, `deleted`, `confirmed`, `broadcast_reminder`, `broadcast_reminder_sent`, `created`, `modified`) VALUES
('1abc4c3a-84a3-11e2-bf8a-00185176683e', 'pspflive1', 'e10adc3949ba59abbe56e057f20f883e', 'KICELIEXE', '688121252', '222120912', 'pspf live', 'pspf@pspf-tz.org', 'www.pspf-tz.org', 'a2d398aa-bfc3-11df-8f82-000c29b8e356', '600eb09e-bf42-11df-8f82-000c29b8e356', '78c963a2-bf44-11df-8f82-000c29b8e356', 'Po Box 4843.', 'PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street', 'PSPF', '9eee962a-bf43-11df-8f82-000c29b8e356', 2, 2, 'y8wbjyef', 0, 1, 'http://www.bongolive.co.tz/api/dlr.php', 1, 0, 1, 1, 0, '2010-10-21 08:14:10', '2012-11-20 22:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `organisations`
--

CREATE TABLE IF NOT EXISTS `organisations` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `organisation` varchar(200) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organisations`
--

INSERT INTO `organisations` (`id`, `organisation`, `created`, `modified`) VALUES
('7cdf95d4-bf43-11df-8f82-000c29b8e356', 'Accounting, Auditing & Taxation', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7cdfb0a0-bf43-11df-8f82-000c29b8e356', 'Advertising, PR & Marketing Agencies & Services ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7cdfcb3a-bf43-11df-8f82-000c29b8e356', 'Agriculture', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7cdfe188-bf43-11df-8f82-000c29b8e356', 'Air, Road and Marine Transport', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7cdff542-bf43-11df-8f82-000c29b8e356', 'Airlines', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce00a32-bf43-11df-8f82-000c29b8e356', 'Architecture, Construction, Building & Contracting', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce01e1e-bf43-11df-8f82-000c29b8e356', 'Barbers, Hair & Beauty Salons', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce030ac-bf43-11df-8f82-000c29b8e356', 'Bars & Nightclubs', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce043b2-bf43-11df-8f82-000c29b8e356', 'Beauty & Fragrance', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce058a2-bf43-11df-8f82-000c29b8e356', 'Bookshops', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce06a9a-bf43-11df-8f82-000c29b8e356', 'Brewers, Distillers & Alcoholic Beverages', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce07e36-bf43-11df-8f82-000c29b8e356', 'Business & Trade Support Organisations ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce090c4-bf43-11df-8f82-000c29b8e356', 'Cinema & Theatres', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce0a406-bf43-11df-8f82-000c29b8e356', 'Clothing Boutiques', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce0b7f2-bf43-11df-8f82-000c29b8e356', 'Computer Hardware & Maintenance Services', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce0c9c2-bf43-11df-8f82-000c29b8e356', 'Consulting', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce0dd36-bf43-11df-8f82-000c29b8e356', 'Courier Services', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce0ef4c-bf43-11df-8f82-000c29b8e356', 'Curios, Arts & Crafts', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce100c2-bf43-11df-8f82-000c29b8e356', 'Electrical Services & Repairs', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce11242-bf43-11df-8f82-000c29b8e356', 'Electronics  & Home Appliances', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1237c-bf43-11df-8f82-000c29b8e356', 'Employment & Recruitment Agencies ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce136fa-bf43-11df-8f82-000c29b8e356', 'Engineering', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1497e-bf43-11df-8f82-000c29b8e356', 'Event Planning', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce15d2e-bf43-11df-8f82-000c29b8e356', 'Fabricated Metal Products & Equipment', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce16e36-bf43-11df-8f82-000c29b8e356', 'Film, Sound & Video Productions', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1810a-bf43-11df-8f82-000c29b8e356', 'Finance, Banking & Investing', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce19528-bf43-11df-8f82-000c29b8e356', 'Fish & Seafood Products & Processors', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1a6da-bf43-11df-8f82-000c29b8e356', 'Fitness Centres', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1b814-bf43-11df-8f82-000c29b8e356', 'Florists & Flower Decorators', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1c9b2-bf43-11df-8f82-000c29b8e356', 'Food Manufacturing, Processing & Wholesaling', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1dc90-bf43-11df-8f82-000c29b8e356', 'Furniture Manufacturers & Dealers ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce1f090-bf43-11df-8f82-000c29b8e356', 'Gambling, Casino''s & Slot Machine Centres', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce202d8-bf43-11df-8f82-000c29b8e356', 'Garments & Accessories', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce21610-bf43-11df-8f82-000c29b8e356', 'Gifts, Cards & Toys', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2286c-bf43-11df-8f82-000c29b8e356', 'Glass & Window', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce23c58-bf43-11df-8f82-000c29b8e356', 'Government Entity', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce24f5e-bf43-11df-8f82-000c29b8e356', 'Hardware Retail', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce262be-bf43-11df-8f82-000c29b8e356', 'Hardware Wholesale & Manufacturing', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce274ca-bf43-11df-8f82-000c29b8e356', 'Health food products & nutritional supplements ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce28690-bf43-11df-8f82-000c29b8e356', 'Hospital,Medical & Health', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce29a54-bf43-11df-8f82-000c29b8e356', 'Hotels, Motels, Guest Houses, Lodges & Campsites', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2adfa-bf43-11df-8f82-000c29b8e356', 'Importers & Exporters', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2c3c6-bf43-11df-8f82-000c29b8e356', 'Insurance Companies, Brokers & Agents', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2d5aa-bf43-11df-8f82-000c29b8e356', 'Interior Decoration & Design', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2e98c-bf43-11df-8f82-000c29b8e356', 'Internet Products & Services Providers', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce2fb70-bf43-11df-8f82-000c29b8e356', 'Interpreters & Translators ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce30d36-bf43-11df-8f82-000c29b8e356', 'Jewellers & Gems Dealers', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce31f6a-bf43-11df-8f82-000c29b8e356', 'Laundry & Dry Cleaning', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce33130-bf43-11df-8f82-000c29b8e356', 'Legal Services', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce345d0-bf43-11df-8f82-000c29b8e356', 'Machinery & Industrial Equipment ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce3580e-bf43-11df-8f82-000c29b8e356', 'Magazines, Newspapers & Periodicals Publishers & Distributors', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce36cfe-bf43-11df-8f82-000c29b8e356', 'Marine', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce37e88-bf43-11df-8f82-000c29b8e356', 'Mining & Quarrying', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce390ee-bf43-11df-8f82-000c29b8e356', 'Motor Vehicle Sales, Parts & Repairs', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce3a34a-bf43-11df-8f82-000c29b8e356', 'Music & Musical Instruments Sales & Services ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('7ce3c514-bf43-11df-8f82-000c29b8e356', 'Musicians ', '2010-09-13 20:00:42', '0000-00-00 00:00:00'),
('9eeda6de-bf43-11df-8f82-000c29b8e356', 'Non Governmental Organisations (NGO''S)', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eedbce6-bf43-11df-8f82-000c29b8e356', 'Opticians & Optical Goods', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eedccfe-bf43-11df-8f82-000c29b8e356', 'Photography & Photo Processing', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeddf46-bf43-11df-8f82-000c29b8e356', 'Plumbing Contractors & Suppliers', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eedf256-bf43-11df-8f82-000c29b8e356', 'Printing & Design', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee062e-bf43-11df-8f82-000c29b8e356', 'Real Estate, Housing & Apartments', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee16aa-bf43-11df-8f82-000c29b8e356', 'Religious Organization', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee2910-bf43-11df-8f82-000c29b8e356', 'Restaurants, Cafes & Catering', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee3a68-bf43-11df-8f82-000c29b8e356', 'Schools, Colleges  & Universities', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee4e18-bf43-11df-8f82-000c29b8e356', 'Security Services & Systems', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee6056-bf43-11df-8f82-000c29b8e356', 'Sports Good & Games Supplies', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee71f4-bf43-11df-8f82-000c29b8e356', 'Stationery, Office Equipment & Supplies ', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee823e-bf43-11df-8f82-000c29b8e356', 'Supermarkets & Groceries', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eee962a-bf43-11df-8f82-000c29b8e356', 'Technology Development & Consultants', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeea7fa-bf43-11df-8f82-000c29b8e356', 'Telecommunications Equipment & Services', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeeb89e-bf43-11df-8f82-000c29b8e356', 'Transport & Logistics Management Services', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeecb2c-bf43-11df-8f82-000c29b8e356', 'Travel & Tours ', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeeddce-bf43-11df-8f82-000c29b8e356', 'TV, Radio Broadcasting & Stations', '2010-09-13 20:01:39', '0000-00-00 00:00:00'),
('9eeeed96-bf43-11df-8f82-000c29b8e356', 'Wedding Organisers & Planners', '2010-09-13 20:01:39', '2010-10-07 09:45:23'),
('9eeefcc8-bf43-11df-8f82-000c29b8e356', 'Other', '2010-09-13 20:01:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `people_ranges`
--

CREATE TABLE IF NOT EXISTS `people_ranges` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `people_range` varchar(100) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `people_ranges`
--

INSERT INTO `people_ranges` (`id`, `people_range`, `created`, `modified`) VALUES
('78c963a2-bf44-11df-8f82-000c29b8e356', '1-9', '2010-09-13 20:08:43', '2010-10-18 07:40:39'),
('78c96910-bf44-11df-8f82-000c29b8e356', '10-19', '2010-09-13 20:08:53', '0000-00-00 00:00:00'),
('78c969a6-bf44-11df-8f82-000c29b8e356', '20-49', '2010-09-13 20:09:02', '0000-00-00 00:00:00'),
('78c969f6-bf44-11df-8f82-000c29b8e356', '50-99', '2010-09-13 20:09:16', '0000-00-00 00:00:00'),
('78c96a46-bf44-11df-8f82-000c29b8e356', '100-499', '2010-09-13 20:09:27', '0000-00-00 00:00:00'),
('78c96aa0-bf44-11df-8f82-000c29b8e356', '500+', '2010-09-13 20:09:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `prim_keywords`
--

CREATE TABLE IF NOT EXISTS `prim_keywords` (
  `id_prim_keywords` int(11) NOT NULL AUTO_INCREMENT,
  `text_prim_keywords` varchar(50) NOT NULL,
  `mst_vendors_id` char(36) NOT NULL,
  `sender_id` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_prim_keywords`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `prim_keywords`
--

INSERT INTO `prim_keywords` (`id_prim_keywords`, `text_prim_keywords`, `mst_vendors_id`, `sender_id`) VALUES
(36, 'sajili', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0'),
(37, 'menu', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0'),
(35, 'benefit', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0'),
(34, 'pension', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0'),
(33, 'mchango', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0'),
(32, 'hakiki', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '0');

-- --------------------------------------------------------

--
-- Table structure for table `processes`
--

CREATE TABLE IF NOT EXISTS `processes` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `name` varchar(250) CHARACTER SET latin1 NOT NULL,
  `check` text CHARACTER SET latin1 NOT NULL,
  `start` text CHARACTER SET latin1 NOT NULL,
  `stop` text CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL COMMENT '0-stop,1-start,2-restart,3-shutdown,works only for sender.php',
  `run_as` enum('tjiwaji') CHARACTER SET latin1 NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `processes`
--

INSERT INTO `processes` (`id`, `name`, `check`, `start`, `stop`, `status`, `run_as`, `modified`) VALUES
('9de55dbc-dce0-11df-a355-000c29b8e356', 'sender', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php 1>/dev/null 2>/dev/null', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.sender.php', 1, 'tjiwaji', '2013-02-05 15:55:47'),
('a9295ade-dce0-11df-a355-000c29b8e356', 'push2Q', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php 1>/dev/null 2>/dev/null', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.inbound_messages.php', 1, 'tjiwaji', '2013-02-05 15:55:47'),
('a9295ade-dee0-12df-e255-000c29b8b897', 'credit_reminder', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php 1>/dev/null 2>/dev/null', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_send_reminder.php', 1, 'tjiwaji', '2013-02-05 15:55:47'),
('a9295ade-dee0-13be-e255-000b28b8b145', 'schedule_sms_store_push', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php 1>/dev/null 2>/dev/null', 'c:/xampp/php/php c:/xampp/htdocs/pspf/shell.ven_update_smsstore_status.php', 1, 'tjiwaji', '2013-02-05 15:55:47'),
('a9295ade-dee0-14be-e255-000c28b8b145', 'sms_reminders', 'c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php', 'c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php 1>/dev/null 2>/dev/null', 'c:/xampp/php/php c:/xampp/php/phpshell.fetchReminders.php', 1, 'tjiwaji', '2013-02-05 11:48:48');

-- --------------------------------------------------------

--
-- Table structure for table `reminders`
--

CREATE TABLE IF NOT EXISTS `reminders` (
  `id` varchar(36) NOT NULL,
  `sms_store_id` varchar(36) NOT NULL,
  `contact_id` varchar(36) NOT NULL,
  `reminder_type` varchar(50) NOT NULL,
  `start_time` datetime NOT NULL,
  `repeat` varchar(10) DEFAULT NULL,
  `repeat_every` smallint(6) DEFAULT NULL,
  `repeat_days` varchar(100) DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `send_date` datetime NOT NULL,
  `is_processed` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reminders`
--


-- --------------------------------------------------------

--
-- Table structure for table `second_keywords`
--

CREATE TABLE IF NOT EXISTS `second_keywords` (
  `id_second_keywords` int(11) NOT NULL AUTO_INCREMENT,
  `text_second_keywords` varchar(50) NOT NULL,
  `prim_keywords_id` int(11) NOT NULL,
  PRIMARY KEY (`id_second_keywords`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `second_keywords`
--

INSERT INTO `second_keywords` (`id_second_keywords`, `text_second_keywords`, `prim_keywords_id`) VALUES
(30, '5', 36),
(29, '4', 35),
(28, '3', 34),
(27, 'contribution', 33),
(26, '2', 33),
(25, '1', 32),
(24, 'status', 32),
(23, 'regista', 32),
(31, 'jisajili', 36),
(32, 'help', 37),
(33, 'msaada', 37),
(34, 'pspf', 37);

-- --------------------------------------------------------

--
-- Table structure for table `senderids`
--

CREATE TABLE IF NOT EXISTS `senderids` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `senderid` varchar(25) CHARACTER SET latin1 NOT NULL,
  `vendor_id` char(36) CHARACTER SET latin1 NOT NULL,
  `status` enum('active','inactive','pending') CHARACTER SET latin1 NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `senderids`
--

INSERT INTO `senderids` (`id`, `senderid`, `vendor_id`, `status`, `is_deleted`, `created`, `modified`) VALUES
('71932aed-7b45-11e2-84bb-001f3c930a21', 'menu', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-20 13:08:14', '0000-00-00 00:00:00'),
('55b90b65-7b45-11e2-84bb-001f3c930a21', 'sajili', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-20 13:07:28', '0000-00-00 00:00:00'),
('3ae617c6-7b45-11e2-84bb-001f3c930a21', 'benefit', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-20 13:06:43', '0000-00-00 00:00:00'),
('cc55ff1b-7b44-11e2-84bb-001f3c930a21', 'mchango', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-20 13:03:37', '0000-00-00 00:00:00'),
('8c0411cd-7b44-11e2-84bb-001f3c930a21', 'hakiki', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-20 13:01:49', '0000-00-00 00:00:00'),
('dd4b372d-7a67-11e2-9b8a-001f3c930a21', 'help', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-02-19 10:42:06', '0000-00-00 00:00:00'),
('2a2aca50-6aba-11e2-a9a4-001f3c930a21', 'pension', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'active', 0, '2013-01-30 11:50:50', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sms_stores`
--

CREATE TABLE IF NOT EXISTS `sms_stores` (
  `id` char(36) CHARACTER SET latin1 NOT NULL,
  `sent_by` char(36) CHARACTER SET latin1 NOT NULL COMMENT 'ADMIN/VENDOR',
  `sender_id` char(36) CHARACTER SET latin1 NOT NULL,
  `credit` int(12) NOT NULL,
  `addressbook_id` text CHARACTER SET latin1 NOT NULL,
  `contact_id` longtext CHARACTER SET latin1 NOT NULL,
  `receiver_id` longtext CHARACTER SET latin1 NOT NULL,
  `receiver_type` varchar(10) CHARACTER SET latin1 NOT NULL,
  `sender_type` varchar(10) CHARACTER SET latin1 NOT NULL,
  `advertiser_id` char(36) CHARACTER SET latin1 NOT NULL COMMENT 'This is  actually mst_vendors.id',
  `text_message` text CHARACTER SET latin1 NOT NULL,
  `message_length` int(11) NOT NULL,
  `message_count` int(11) NOT NULL,
  `is_shceduled` tinyint(1) NOT NULL,
  `send_time` datetime NOT NULL,
  `sent_time` datetime NOT NULL,
  `is_proccessed` int(1) NOT NULL,
  `canceled` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `job_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sms_stores`
--

INSERT INTO `sms_stores` (`id`, `sent_by`, `sender_id`, `credit`, `addressbook_id`, `contact_id`, `receiver_id`, `receiver_type`, `sender_type`, `advertiser_id`, `text_message`, `message_length`, `message_count`, `is_shceduled`, `send_time`, `sent_time`, `is_proccessed`, `canceled`, `created`, `modified`, `job_name`) VALUES
('52305f2b-872b-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '3ae617c6-7b45-11e2-84bb-001f3c930a21', 1, 'QUICK', 'QUICK', '', '', 'VEN', '', 'hey im testing quick sms update for pspf', 40, 1, 0, '2013-03-07 16:31:21', '2013-03-07 16:32:07', 2, 0, '2013-03-07 16:31:21', '2013-03-07 16:32:03', ''),
('64e67491-872d-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '55b90b65-7b45-11e2-84bb-001f3c930a21', 1, 'FILE', 'FILE', '', '', 'VEN', '', 'testing file sms in pspf', 24, 1, 0, '2013-03-07 16:46:11', '2013-03-07 16:47:06', 2, 0, '2013-03-07 16:46:11', '2013-03-07 16:47:02', ''),
('89dd0325-872e-11e2-b563-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '71932aed-7b45-11e2-84bb-001f3c930a21', 1, 'b78b9726-1a85-11e2-961b-001f3c930a21', '', '', '', 'VEN', '', 'leonard anatest group sms', 25, 1, 0, '2013-03-07 16:54:21', '2013-03-07 16:55:06', 2, 0, '2013-03-07 16:54:21', '2013-03-07 16:55:02', ''),
('f117a240-a1db-11e2-ba7d-001f3c930a21', '1abc4c3a-84a3-11e2-bf8a-00185176683e', '71932aed-7b45-11e2-84bb-001f3c930a21', 1, 'QUICK', 'QUICK', '', '', 'VEN', '', 'mambo vp ndugu leonard nyirenda', 31, 1, 0, '2013-04-10 15:41:01', '0000-00-00 00:00:00', 0, 0, '2013-04-10 15:41:01', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `sms_template`
--

CREATE TABLE IF NOT EXISTS `sms_template` (
  `template_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `vendor_id` varchar(36) NOT NULL,
  `sms_title` varchar(50) NOT NULL,
  `message` varchar(400) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted` int(11) NOT NULL DEFAULT '0',
  `modified` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=66 ;

--
-- Dumping data for table `sms_template`
--


-- --------------------------------------------------------

--
-- Table structure for table `text_in_log`
--

CREATE TABLE IF NOT EXISTS `text_in_log` (
  `sms_in_id` int(15) NOT NULL AUTO_INCREMENT,
  `phone` varchar(15) NOT NULL,
  `phone_receive` varchar(20) NOT NULL,
  `message` text NOT NULL,
  `received` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  `num_status` smallint(6) NOT NULL DEFAULT '0',
  `outbound_response` text,
  `sent_date` datetime NOT NULL,
  `ven_id` varchar(100) DEFAULT NULL,
  `keyword` varchar(50) NOT NULL,
  `system_error` char(1) NOT NULL DEFAULT '0',
  `system_error_message` varchar(100) NOT NULL,
  `checkno` varchar(20) NOT NULL,
  PRIMARY KEY (`sms_in_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=185020 ;

--
-- Dumping data for table `text_in_log`
--

INSERT INTO `text_in_log` (`sms_in_id`, `phone`, `phone_receive`, `message`, `received`, `status`, `num_status`, `outbound_response`, `sent_date`, `ven_id`, `keyword`, `system_error`, `system_error_message`, `checkno`) VALUES
(185018, '255757447737', '', '3 1000030', '2013-04-09 04:45:13', 'response to be sent', 1, 'Ujumbe wako unashughulikiwa. Tafadhari endelea kusubiri.', '0000-00-00 00:00:00', '1abc4c3a-84a3-11e2-bf8a-00185176683e', 'pension', '1', 'Connecting to PSPFLIVE database fail', '');
