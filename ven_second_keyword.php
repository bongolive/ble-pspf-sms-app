<?php
	
	/*************************************************
	*	File	: Vven_primary_keyword.php				 *
	*	Purpose	: Manage Primary Keyword for vendor			 *
	*	Author	: Leonard Nyirenda					 	 *
	*************************************************/
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'secondkeys.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);


	$objSk= new secondkeys(); 
	
	
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	$errorFlag = 0;
	switch($action){
	
		case'add':
			$objSk->prim_keywords_id=$_REQUEST['prim_keywords_id'];
			$objSk->text_second_keywords = cleanQuery($_REQUEST['text_second_keywords']);
			if($objSk->addSecondKey()){
				$msg = "New Second Keyword added successfully";
			}else{
			
				$msg = "Second keyword not added";
				$errorFlag = 1;
			}
			
			
		break;
		case'edit':
			$objSk->prim_keywords_id=$_REQUEST['prim_keywords_id'];
			$objSk->id_second_keywords=$_REQUEST['id'];
			$arrSk = $objSk->getSecondKeyById2();
			while($row=mysql_fetch_array($arrSk)){
				$smarty->assign('text_second_keywords',$row['text_second_keywords']);
				$smarty->assign('id',$row['id_second_keywords']);
			}
			$action = 'update';
		break;
		case'update':
			$objSk->prim_keywords_id=$_REQUEST['prim_keywords_id'];
			$objSk->id_second_keywords  = $_REQUEST['id'];
			$objSk->text_second_keywords = $_REQUEST['text_second_keywords'];
			
			if($objSk->editSecondKey()){
				$msg = "Second Keyword updated successfully";
			}else{
				$msg = "Second keyword not updated";
				$errorFlag = 1;
			}

			$action = "add";
			
		break;
		
		default:
			$action = "add";
	
	
	}
	$objSk->prim_keywords_id=$_REQUEST['prim_keywords_id'];
	$arrSk =  $objSk->getSecondKeyByPrimarykey();
	while($row=mysql_fetch_array($arrSk)){
		$value[]=$row;
	}

	$smarty->assign("msg",$msg);
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("selfUrl",BASE_URL.'ven_second_keyword.php');
	$smarty->assign("prim_keywords_id",$_REQUEST['prim_keywords_id']);
	$smarty->assign("primarykeyword",$objSk->getPrimaryKeyname($_REQUEST['prim_keywords_id']));
	$smarty->assign("action",$action);
	$smarty->assign("arrSk",$value);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_second_keyword.tpl");
	SmartyPaginate::disconnect();

	include 'footer.php';

?>