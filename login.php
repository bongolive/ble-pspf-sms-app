<?php
/*
	File : login.php
	functionalitie : login vlidation and redirecting to landing page accordin to user type
	Author: leonard Nyirenda
*/
	
	
	if(isset($_SESSION['user']) || !isset($_REQUEST['userType']) || $_REQUEST['userType']=='' ) {
	
		redirect('index.php');
	
	}elseif($_REQUEST['action']=='login'){


			if(!empty($_REQUEST['userType']) ){

				switch($_REQUEST['userType']){
					case 'VEN':

						if(trim($_REQUEST['username'])=='' || trim($_REQUEST['password'])==''){
							$msg = LOGIN_MSG_03;
							break;
						}else{	

							$arrUser =	venLogin('mst_vendors',trim($_POST['username']),md5(trim($_POST['password'])));

							if($arrUser){
			
								$_SESSION['user']['id'] = $arrUser['id'];
								$_SESSION['user']['user_type'] = $_REQUEST['userType'];
								$_SESSION['user']['name'] = $arrUser['name'];
								$_SESSION['user']['reseller'] = $arrUser['reseller'];
								$_SESSION['user']['parent_ven_id'] = $arrUser['parent_ven_id'];
								$_SESSION['user']['access_incoming'] = $arrUser['access_incoming'];
								$_SESSION['user']['access_registrations'] = $arrUser['access_registrations'];
								//updating credit only for parent id
								if($_SESSION['user']['parent_ven_id']==''){
									(int) $remoteCredit = getRemoteCreditBalance();
									(int) $resCredit = getTotalCreditOfAllVendors();
									$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
								} 
								redirect('ven_dashboard.php');
							}else{
			
								$msg = LOGIN_MSG_04;
							}
						}
						
					break;
					
				}

			
			
			}

	
	}

?>
