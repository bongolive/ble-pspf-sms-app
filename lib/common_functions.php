<?php
/*
	File	: common_functions.php
	Purpose	: common funtions are defined here
	Author	: Akhilesh 
*/

	//Redirecting pages
	function redirect($url) {
		header('');	
		header("Location: ".BASE_URL.$url);
		exit;
	}
	
	// Get new UUID()
	function getNewUUID() {
		$sql = "SELECT UUID()";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_array($res);
		return $row[0];
	}
	
	//Login vlidation for subscribers and admin
	function login($table,$mobNo,$password){
	
		//$sql = "SELECT u.id,u.name,ut.abbr AS user_type FROM users AS u, user_types AS ut WHERE ut.id=u.user_type_id AND u.deleted !='1' AND u.mob_no='$mobNo' AND u.password='$password' ";
		
		$sql = "SELECT u.id,u.name FROM ".$table." AS u WHERE  active=1 AND u.deleted !='1' AND u.mob_no='$mobNo' AND u.password='$password' ";

		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($result) >0){
		
			return $row = mysql_fetch_assoc($result);
		
		}else{
		
			return false;
		
		}

	
	}
	
	//Function Login validation for Broadcasters
	function venLogin($table,$username,$password){
		///$sql = "SELECT u.id,u.name,u.parent_ven_id,u.reseller,u.access_incoming,u.access_registrations FROM ".$table." AS u WHERE active=1 AND  u.deleted !='1' AND u.username='$username' AND u.password='$password' ";
		$sql = "SELECT u.id,u.name,u.parent_ven_id,u.reseller FROM ".$table." AS u WHERE active=1 AND  u.deleted !='1' AND u.username='$username' AND u.password='$password' ";
		//echo $sql;exit;
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
if(mysql_num_rows($result) >0){
			return $row = mysql_fetch_assoc($result);
		}else{
			return false;
		}
	}

	
	// common funtion to create dropdaown
	function getDropdown($table,$field1,$field2,$name,$id='',$select='',$val=0,$where='',$classname='',$function='',$extra=''){
		$html='';
		$sql = "SELECT $field1,$field2 FROM " .$table;
		if($where!=''){		
		   $sql = $sql.' '.$where;
		}
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	 
		 if(mysql_num_rows($result)>0){
			 $html = '<select name="'.$name.'"  id="'.$name.'" '.$extra.' '.$classname.' '.$function.' >';
			 if($select){
				 $html .= '<option value="'.$val.'">'.$select.'</option>';
			 }

			while($row=mysql_fetch_array($result)){
				 if($row[$field2]==$id && $id!=''){
					$html .= '<option value="'.$row[$field2].'" selected="selected">'.$row[$field1].'</option>';
				 }else{
					$html .= '<option value="'.$row[$field2].'">'.$row[$field1].'</option>';
				 }
			}
			$html == '</select>';
		}
	}

	// common function to create array
	function getArray($table,$field1,$feild2,$where='',$lang='')
	{
		$arrResult= array();
		
		if((mb_strlen($lang,"utf8") == 0) || (strcmp($lang,'en_us') == 0))
		{
			$sql	= "SELECT $field1,$feild2 FROM $table $where ;";
			$result	=  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result))
			{
				array_push($arrResult,$row);
			}
			if(count($arrResult)>0)
			{
				return $arrResult;
			}
			else
			{
				return false;
			}
		}
		else
		{
			$sql	= 'SELECT LEFT(content_id,POSITION("." IN content_id)-1) AS '.$field1.', field_content AS '.$feild2.' FROM multilanguage WHERE lang="'.$_SESSION['lang'].'" AND (content_id LIKE CONCAT("%",".'.$feild2.'.'.$table.'")) ORDER BY field_content;';
			$result	=  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result))
			{
				array_push($arrResult,$row);
			}
			if(count($arrResult)>0)
				return $arrResult;
			else
				return false;
		}
	}
	
	// count contacts of 'defoult' group
	function countContactOfDefault($user_id){
		$sql = "SELECT count(1) as count FROM addressbooks as a,contacts as c where c.addressbook_id=a.id and a.vendor_id='".$user_id."' and a.addressbook='default'";
		$result=  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_assoc($result);
		return $row['count'];
	}

	// function is used to checke whether user is loggedin
	function check_session($seesionid,$userType,$url){
		
		 if(!isset($seesionid) && $sessionid==''){
			  redirect($url);
		 }else{
			 if($seesionid['user_type'] != $userType){
				 redirect($url);
			 }else{
				 return true;
			 }
		 }
	}

	// Phone number validation
	function phoneValidation($phno){
	
		//global $project_vars;

			$phno = substr($phno,-($project_vars['mob_no_length']));
			//if(ereg('^[8|9]{1}[0-9]{9}$', $phno)){
			//if(preg_match('/^[0-9]{9}$/', $phno)){
			if(preg_match('/^\+?[0-9]{9,12}$/', $phno)){
				return true;
			}else{
				return false;
			}
		
	}


	// Get date acoding to formate 
	function getDateFormat($date,$format=''){
		global $project_vars;
		 if($format ==''){
			$format = $project_vars['date_format_mysql'];
		 }
		 if($date =="")
			 return false;
		  else
			  return date($format,strtotime($date));
	}

	// Sender name validation
	function senderidValidation($senderid){
		global $project_vars;
		$pattern ="/^[A-Za-z0-9 _]+$/";
		$senderid = trim($senderid);
		
		if (strlen($senderid) > $project_vars['senderid_length'] || strlen($senderid) < 1){
			
			return false;
		
		}else{
			if (preg_match($pattern,$senderid)){
				return true;  
			}else	
				return false;
	
		}
	}
	
	// email address validation	
	function emailVlidation($email){
		
		$pattern1 ="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
		$pattern ="/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/";
		
		if (preg_match($pattern,$email)){
			return true;  
		}else{
			return false;
		}
	}	

	// Creating random string
	function str_rand($length = 8, $seeds = 'alphanum'){
    
		// Possible seeds
		$seedings['alpha'] = 'abcdefghijklmnopqrstuvwqyz';
		$seedings['numeric'] = '0123456789';
		$seedings['alphanum'] = 'abcdefghijklmnopqrstuvwqyz0123456789';
		$seedings['caps_alphanum'] = 'ABCDEFGHIJKLMNOPQRSTUVWQYZ0123456789';
		$seedings['hexidec'] = '0123456789abcdef';
		
		// Choose seed
		if (isset($seedings[$seeds])){
			$seeds = $seedings[$seeds];
		}
		
		// Seed generator
		list($usec, $sec) = explode(' ', microtime());
		$seed = (float) $sec + ((float) $usec * 100000);
		mt_srand($seed);
		
		// Generate
		$str = '';
		$seeds_count = strlen($seeds);
		
		for ($i = 0; $length > $i; $i++){
			$str .= $seeds{mt_rand(0, $seeds_count - 1)};
		}
		
		return $str;
	}

	// Function used send massage to API
	function smsSendWithCurl($mobile,$massege,$senderid='',$response='N'){
		
		global $project_vars;
		
	//	echo '<br> ',$url = $project_vars['sms_api_url'].'?'.'username='.$project_vars['sms_api_username'].'&pass='.$project_vars['sms_api_pass'].'&senderid='.urlencode($senderid).'&message='.urlencode($massege).'&dest_mobileno='.$mobile.'&responce='.$response;
		
		$url = $project_vars['sms_api_url'];
		$url = str_replace('TMESSAGE',urlencode($massege),$url);
		$url = str_replace('SENDERID',urlencode($senderid),$url);
		$url = str_replace('MOBNO',formatMobno($mobile),$url);
		
		if(strlen($massege)>160){
			$url = $url.'&Type=LongSMS';
		}
		//echo '<br>',$url;
		$curl_ch = curl_init();
		curl_setopt($curl_ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($curl_ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl_ch, CURLOPT_TIMEOUT, 60);	
		curl_setopt($curl_ch, CURLOPT_URL, $url);
		$output = curl_exec($curl_ch);
		
		return $output;
	}
	
	
	function smsSendWithCurl2($mob,$sms,$senderid){
			global $project_vars;
		
			define ("URL_API_DOMAIN", $project_vars['bulk_gw_sms_url']);
			//$sendername = urlencode($senderid);
			$sendername = urlencode("PSPF");
			$username = $project_vars['bulk_gw_user'];
			$password = $project_vars['bulk_gw_pwd'];
			$apikey = $project_vars['bulk_gw_apikey'];
			$apiKey = urlencode($apikey);
			$mob="+".$mob;
			$destnum = urlencode($mob);
			$message = urlencode($sms);    

			$posturl = URL_API_DOMAIN."?sendername=$sendername&username=$username&password=$password&apikey=$apiKey&destnum=$destnum&message=$message";
    		$proxy_ip = '192.168.0.2'; //proxy IP here
			$proxy_port = 8080;
    		$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $posturl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
			//curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
			//curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
			$response = curl_exec($ch);
			return $response;
	}
	
	
function smsSendWithXML($messageMobile, $username, $password, $senderid = "PSPF"){
			//echo $username;exit;
			$senderid = "255688121252"; // AWS
			//$senderid = 'Achyut'; //Live
			//$sendername = urlencode("PSPF");
			//$username = "pspflive1";
			//$password = "123456";
			//$apikey = "1abc4c3a-84a3-11e2-bf8a-00185176683e";
			//$apiKey = urlencode($apikey);

			//define ("URL_API_DOMAIN", "http://bongolive.co.tz/api/v2/sendsms"); // Cmt 2015.06.03 - Achyut
			define ("URL_API_DOMAIN", "54.75.242.162/bongolive.co.tz/webservice/sendsms");
			/*define ("URL_API_USER", $project_vars['bulk_gw_user'] );
			define ("URL_API_PASSWORD", $project_vars['bulk_gw_pwd'] );
			*/
			//define ("URL_API_USER", "pspflive" ); //Cmt 2015.06.03 - Achyut
			//var_dump( $project_vars['bulk_gw_user'] );exit;
			define ("URL_API_USER", $username );
			//define ("URL_API_PASSWORD", "PSPFBulkSMS987" ); //Cmt 2015.06.03 - Achyut
			define ("URL_API_PASSWORD", $password );


			$messagePackets = null;
			//var_dump($messageMobile);
			foreach($messageMobile as $msgMob){
				//echo 'Inside caught...';
				$messagePackets .= "<message><msgId>".$msgMob['msgId']."</msgId><mobile>".$msgMob['mobile']."</mobile><text>".urlencode($msgMob['text'])."</text></message>";             
			}
             //exit;
			//$url = 'http://localhost/webservice/sendsms';
			//header('Content-type: text/xml');
  			$ch = curl_init();
  			curl_setopt( $ch, CURLOPT_URL, URL_API_DOMAIN );
  			curl_setopt( $ch, CURLOPT_POST, true );
  			curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml'));
  			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  			curl_setopt( $ch, CURLOPT_POSTFIELDS, "<?xml version='1.0' encoding='utf-8'?><xml>
		                                            <sendername>".$senderid."</sendername>
		                                            <username>".URL_API_USER."</username>
		                                            <password>".URL_API_PASSWORD."</password>"
		                                            .$messagePackets."</xml>" );
  			
        $xml = curl_exec($ch); 

        if (curl_errno($ch)) { 
            print "Error: " . curl_error($ch); 
        } else { 
            curl_close($ch); 
        } 
       return $xml;
			/*
    		curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
			curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
			curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
			*/
	}

	//Add couontry code
	function formatMobno($no,$format='') {
		global $project_vars;
		$mobLenght = strlen($no);
		if($mobLenght==9){
			$mobNo = $project_vars['country_code_mob_no'].$no;
		}else if($mobLenght==10){
			$mobNo = $project_vars['country_code_mob_no'].substr($no, -9);
		}else if($mobLenght==13){
			$mobNo = substr($no, 1);
		}else{
			$mobNo = $no;
		} 
		return $mobNo;
	}
	function xlsBOF() {
		echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);  
		return;
	}
	
	
    function validateAndFormatMobile( $destNumber, $userArr=0)  {
		//echo 'Inside'. $destNumber;
      // Remove +,0 and Only numbers
      $destNumber = ltrim($destNumber,'+');
      $destNumber = ltrim($destNumber,'0');
		//echo "After trim : ".$destNumber;      
		$filtered = preg_replace('/[^0-9\']/', '',  $destNumber);
		//echo "After regex : ".$destNumber;

      /** Satisfy 
      Case 1:
      (255)688121252~12
      (255)68812125~11
      (91)8691019829~12
      (44)7837805080~12
      688121252~9 / 0688121252~10
      68812125~8  / 068812125~9
      */
     // var_dump($filtered.'-'.strlen($filtered));//exit;
	//error_reporting(E_ALL);
	//ini_set('display_errors', 1);
	//echo "Strlen : ";
      //var_dump(strlen($filtered));
	//echo  " - ";
      switch( strlen($filtered) ){
                case 12:
				$subthree = substr($filtered,0,3);
				$twofivefive = in_array($subthree , array('255') );
				$subtwo = substr($filtered,0,2);
				$nineone = in_array($subtwo , array('91','44'));
	            $hold =  ( $twofivefive || $nineone) ? $filtered : false;
				return $hold;
		        break;

                case 11: 
                return ( in_array(substr($filtered,0,3), array('255') ) ) ? $filtered : false;
                break;

                case 9:
                case 8:
                return "255".$filtered; 
                break;

                default:
                return "255".$filtered; 
                break;
              }

    }
	function xlsEOF() {
		echo pack("ss", 0x0A, 0x00);
		return;
	}
	
	function xlsWriteNumber($Row, $Col, $Value) {
		echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
		echo pack("d", $Value);
		return;
	}
	
	function xlsWriteLabel($Row, $Col, $Value ) {
		$L = strlen($Value);
		echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
		echo $Value;
	return;
	}

	// Get proccess id running
	function getPID($command="") {
		global $project_vars;
		if($command == "") {
			return false;
		} else {
			//echo $command = "ps aux | grep -v grep | grep '" . $command . "'";
			exec($command, $pslist);
			
			for($i=0; $i < count($pslist); $i++) { 
				$pslist[$i] = ereg_replace(" +"," ",$pslist[$i]);
				$item[0] = strtok($pslist[$i]," ");
				for($s=1 ; $s < 11; $s++) {
					$item[$s]  = strtok(" ");
				}
				return $item[1];

	//			echo "<TR>\n";
	//			for($p=0; $p < 11; $p++) { 
	//				echo "   <TD "; 
	//				echo ">$item[$p]</TD>\n";
	//			} 
	//			echo "</TR>\n";
			}

			return false;
		}
	}



	// Starting new process
	function startProcess($command="") {
		global $project_vars;
		if($command == "") {
			return false;
		} else {
			exec($command . " > /dev/null &");
	//		system($command);
	//		shell_exec($command);
	//		passthru($command);
			return true;
		}
	}

	// Stopping running process
	function stopProcess($pid="") {
		global $project_vars;
		if($pid == "") {
			return false;
		} else {
			exec("kill " . $pid . " > /dev/null &");
	//		system($command);
	//		shell_exec($command);
	//		passthru($command);
			return true;
		}
	}

	//function for load xls and csv files (admin)
	function getXLS($xls){
		include_once (MODEL.'load_file/PHPExcel/IOFactory.php');
		$objPHPExcel = PHPExcel_IOFactory::load($xls);
		$objPHPExcel->setActiveSheetIndex(0);
		$aSheet = $objPHPExcel->getActiveSheet();
		
		$array = array();//array value
		//
		foreach($aSheet->getRowIterator() as $row){
			//
			$cellIterator = $row->getCellIterator();
			//
			$item = array();//array string
			foreach($cellIterator as $cell){
				array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
			}
			//array rezult
			array_push($array, $item);
		}
		return $array;
	}

	//function to convert the date format in excel timestamp

	function excel_to_timestamp($excel_days) {
		static $cache_key;
		static $cache_val;
	    
		if ($excel_days == $cache_key)
		    return $cache_val;
	    
		$excel_days = floatval(str_replace(',', '.', $excel_days));
		$dtz = new DateTimeZone(date_default_timezone_get());
			$dt = new DateTime("now", $dtz);
			$offset = timezone_offset_get($dtz, $dt);
		$sec_in_day = 86400;
		$sec_diff_1900_1970 = 2209161600;
		$seconds  = $excel_days * $sec_in_day;
		$seconds -= $sec_diff_1900_1970;
		$seconds -= $offset;
	    
		$cache_key = $excel_days;
		$cache_val = $seconds;
		return $seconds;
	}
 
	function CutMobileNumber($mobile){
		$mobile=trim($mobile);
		$mobLength=strlen($mobile);
		
		if($mobLength == 10){
			$validMob =  substr($mobile, -9);
		}else if($mobLength == 12 && substr($mobile,0, 3)=='255'){
			$validMob =  substr($mobile, -9);
		}else if($mobLength == 13 && substr($mobile,0, 4) =='+255'){
			$validMob =  substr($mobile, -9);
		}else if($mobLength == 13 && substr($mobile,0, 1) =='+' && substr($mobile,0, 4) !='+255'){
			$validMob =  substr($mobile, -12);
		}else{
			$validMob = $mobile;
		}
		
		return $validMob;
	}
		
		
		function uploadMobilephoneFrom_txt($file){
			$fp = fopen($file, "r"); // Open the file for reading

			while($line = fgets($fp)) { // Loop through each line
     			$contact[] = split("\t", $line, 5); // Split the line by the tab delimiter and store it in our array
			}
		
			return $contact;
		}
		
		
function uploadMobilephoneFrom_csv($file){
			 
	$arrResult = array();
	$handle = fopen($file, "r");
	if( $handle ) {
		while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$arrResult[] = $data;
		}
		fclose($handle);
	}
		
	return $arrResult;
}
		
		
		
function getXLSorXLSX($xls){
		$arr_data=array();
		include_once (MODEL.'load_file/PHPExcel/IOFactory.php');
		
		$inputFileType = PHPExcel_IOFactory::identify($xls);  
		$objReader = PHPExcel_IOFactory::createReader($inputFileType);  
		$objReader->setReadDataOnly(true);
		
		/**  Load $inputFileName to a PHPExcel Object  **/  
		//$objPHPExcel = PHPExcel_IOFactory::load($xls);
		$objPHPExcel = $objReader->load($xls);   
		$total_sheets=$objPHPExcel->getSheetCount(); // here 4  
		//$allSheetName=$objPHPExcel->getSheetNames(); // array ([0]=>'student',[1]=>'teacher',[2]=>'school',[3]=>'college')  
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0); // first sheet  
		$highestRow = $objWorksheet->getHighestRow(); // here 5  
		$highestColumn = $objWorksheet->getHighestColumn(); // here 'E'  
		$highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);  // here 5  )
		
			for ($row = 1; $row <= $highestRow; ++$row) {  
    			for ($col = 0; $col < $highestColumnIndex; ++$col) {  
    				$value=$objWorksheet->getCellByColumnAndRow($col, $row)->getValue();  
        			if(is_array($arr_data) ) { $arr_data[$row-1][$col]=$value; }  
    			}  
			}


		return $arr_data;
	}
	
	function ExcelToPHP($dateValue = 0, $ExcelBaseDate=0) {
    if ($ExcelBaseDate == 0) {
        $myExcelBaseDate = 25569;
        //  Adjust for the spurious 29-Feb-1900 (Day 60)
        if ($dateValue < 60) {
            --$myExcelBaseDate;
        }
    } else {
        $myExcelBaseDate = 24107;
    }

    // Perform conversion
    if ($dateValue >= 1) {
        $utcDays = $dateValue - $myExcelBaseDate;
        $returnValue = round($utcDays * 86400);
        if (($returnValue <= PHP_INT_MAX) && ($returnValue >= -PHP_INT_MAX)) {
            $returnValue = (integer) $returnValue;
        }
    } else {
        $hours = round($dateValue * 24);
        $mins = round($dateValue * 1440) - round($hours * 60);
        $secs = round($dateValue * 86400) - round($hours * 3600) - round($mins * 60);
        $returnValue = (integer) gmmktime($hours, $mins, $secs);
    }

    // Return
    return $returnValue;
}

function getRemoteCreditBalance() {

			global $project_vars;
			define ("URL_API_DOMAIN", $project_vars['bulk_gw_request_url']);
			$username = $project_vars['bulk_gw_user'];
			$password = $project_vars['bulk_gw_pwd'];
			$posturl = URL_API_DOMAIN."?username=$username&password=$password&request=balance";
		    $proxy_ip = '192.168.0.2'; //proxy IP here
			$proxy_port = 8080;
   			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $posturl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			//curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
			curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
			//curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
			$response = curl_exec( $ch );
			return $response;
	}
	
	
function updateLocalCopyCreditBalance( $id, $credit ) {
		$sql = "UPDATE credit_balances SET credit_balance=".$credit." WHERE user_id='".$id."'";
		$result = mysql_query( $sql ) or mysql_error_show( $sql, __FILE__, __LINE__);
		if( $result ){
			return true;
		}else{
			return true;
		}
	}

	
function getTotalCreditOfAllVendors(){
		$sql = "SELECT SUM(credit_balance - credit_blocked) AS credit FROM credit_balances WHERE user_id !='1abc4c3a-84a3-11e2-bf8a-00185176683e'";
		$result = mysql_query($sql) OR mysql_error_show($sql,__FILE__,__LINE__);
		if( $result ){
			return mysql_fetch_assoc( $result );
		}else{
			return false;
		}
	}
	
function createRemoteSernderid($senderid){
		define ("URL_API_DOMAIN", $project_vars['bulk_gw_request_url']);
		$api=urlencode($project_vars['bulk_gw_apikey']);
		$posturl = URL_API_DOMAIN."?id=$api&senderid=$senderid&request=create_senderid";
    	$proxy_ip = '192.168.0.2'; // proxy IP here
		$proxy_port = 8080;
    		$ch = curl_init( );
			curl_setopt($ch, CURLOPT_URL, $posturl);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, $proxy_port);
			curl_setopt($ch, CURLOPT_PROXYTYPE, 'HTTP');
			curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
			$response = curl_exec($ch);
			return $response;
	}
	
function generatedefaultmembernumber($memberno){
		if(strlen($memberno)==1){
			return 'PSPF-000000'.$memberno;
		}else if(strlen($memberno)==2){
			return 'PSPF-00000'.$memberno;
		}else if(strlen($memberno)==3){
			return 'PSPF-0000'.$memberno;
		}else if(strlen($memberno)==4){
			return 'PSPF-000'.$memberno;
		}else if(strlen($memberno)==5){
			return 'PSPF-00'.$memberno;
		}else if(strlen($memberno)==6){
			return 'PSPF-0'.$memberno;
		}else{
			return 'PSPF-'.$memberno;
		}
	}
	
?>