<?php
	function redirect($url) {
		header("location:$url");
		exit;
	}

	function htaccessURL($url='',$cat='') {
		$strurl = $url."/".$cat;
		return $strurl;
	}

	function get_row($id,$table,$fld='id',$op="=",$comma="'") {
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table) . " WHERE " . $fld . $op . $comma . $id . $comma; 
		//echo $sql."<br/>";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_array($res);
		return $row;
	}

	function get_row_assoc($id,$table,$fld='id',$op="=",$comma="'") {
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table) . " WHERE " . $fld . $op . $comma . $id . $comma; 
		//echo $sql."<br/>";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_assoc($res);
		return $row;
	}



	function get_rows($id,$table,$fld='id',$op="=",$comma="'") {
		global $project_vars;
		$ret = array();
		$sql = "SELECT * FROM " . get_table_name($table) . " WHERE " . $fld . $op . $comma . $id . $comma; 
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		while($row = mysql_fetch_array($res)) {
			$ret[] = $row;
		}
		return $ret;
	}






	function row_exists($id,$table,$fld='id',$op="=",$comma="'") {
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table) . " WHERE " . $fld . $op . $comma . $id . $comma;
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($res) >=1){
			return 1;
		}else{
			return 0;
		}
	}


	function getNewUUID() {
		$sql = "SELECT UUID()";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_array($res);
		return $row[0];
	}



	function get_where_record($table, $where=''){
		
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table)." ".$where; 
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($res) >0){
			return $res;
		}else{
			return 0;
		}
	 }

	function get_where_record1($table, $where=''){
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table)." ".$where;
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_assoc($res);
		return $row;
	 }

		function get_where_record2($table, $where=''){
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table)." ".$where; 
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($res) >0){
			return mysql_num_rows($res);
		}else{
			return 0;
		}
		}

	function get_where_record3($table, $where=''){
		global $project_vars;
		$arr = array();
		$sql = "SELECT * FROM " . get_table_name($table)." ".$where;
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($res)>0){
			while($ka = mysql_fetch_assoc($res)){
				$arr[] = $ka;
			}
		}
		 return $arr;
	 }

	function check_login_details($table, $where=''){
		$sql = "SELECT users.id,user_types.right FROM users,user_types ".$where." and user_types.id=users.user_type_id";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_assoc($res);
		return $row;
		
	}
	
	function insert_login($user_id,$ipAdd){
		
		$id = getNewUUID();
		
		$sql = "INSERT INTO	login_logout_details (id,user_id,ip_address,login) values('$id','$user_id','$ipAdd',NOW()) ";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if($res)
			return $id;
		else 
			return false;
	
	}

	function insert_logout($lodID){
		
		$sql = "UPDATE login_logout_details SET logout=NOW() WHERE id='$lodID' ";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		return;
	
	}

	function getRealIpAddr(){

		if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet		
		 
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
		
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		
		} else{

		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	

	function get_record($id,$table,$fld='id',$op="=",$comma="'") {
		
		global $project_vars;
		$sql = "SELECT * FROM " . get_table_name($table) . " WHERE " . $fld . $op . $comma . $id . $comma;
		//echo $sql."<br/>";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row = mysql_fetch_assoc($res);
		return $row;
	}

	function generate_number($len=3){
	  $str=''; 
	  while($i <$len){
		  $str.=rand(0,9);$i++;
	  }

	  $str = trim($str);
	  return $str;
	}

	function check_phone($var,$message1,$message2,$error_label){
	  $error ='';
	  $pattern ="^[0-9-]$";
	  if($var==''){
		  $error = "#".$error_label."*".$message1."#";
	  }elseif(!is_numeric($var)){
			$error = "#".$error_label."*".$message2."#";
		}

	  return $error;
	}

	function check_email($var,$message1,$message2,$error_label){
	 $error ='';
	  $pattern ="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
		   if($var==""){$error = "#".$error_label."*".$message1."#";}
			
			//else if (!eregi($pattern,$var)){
			else if (!preg_match($pattern,$var)){
			  $error = "#".$error_label."*".$message2."#";
			}
	  return $error;
	}

	function pagination($rcount,$row_page,$counter,$url,$a,$op='?'){
	  $html='';
	  $next='';
	  $prev='';
	  $first='';
	  $last ='';
	  $v ='';

		  if(($rcount%$row_page)==0){
			  $v = $rcount-$row_page;
		  }else{
			  $v = $rcount-($rcount%$row_page);
		  }

		  if($counter < $v && $rcount > $row_page){
			  $new_counter1  = $counter+ $row_page;
			  $url1 =$url.$op.$a."=".$new_counter1;
			  $url2 =$url.$op.$a."=".$v;
			  $next ='<a href="'.$url1.'"><img src="img/btn-next-active.gif"></a>&nbsp;';
			  $last ='<a href="'.$url2.'"><img src="img/btn-last-active.gif"></a>&nbsp;';
		  }else{
			  $next ='<a href="javascript:void(0);"><img src="img/btn-next-inactive.gif"></a>&nbsp; ';
			  $last ='<a href="javascript:void(0);"><img src="img/btn-last-inactive.gif"></a>&nbsp;';
		  }

		  if($counter > 0 && $rcount > $row_page){
			  $new_counter2  = $counter-$row_page;
			  $url3 =$url.$op.$a."=".$new_counter2;
			  $url4 =$url.$op.$a."=0";
			  $prev ='<a href="'.$url3.'"><img src="img/btn-prev-active.gif"></a>&nbsp; ';
			  $first ='<a href="'.$url4.'"><img src="img/btn-first-active.gif"></a>&nbsp; ';
		  }else{
			  $prev ='<a href="javascript:void(0);"><img src="img/btn-prev-inactive.gif"></a>&nbsp; ';
			  $first ='<a href="javascript:void(0);"><img src="img/btn-first-inactive.gif"></a>&nbsp; ';
		  }

		  $html =$first.$prev.$next.$last;
		  return $html;

	}


	function get_dropdown($table,$field1,$field2,$where,$name,$id='',$select='',$val=0,$classname='',$function='', $extra='' ){
	  $html='';
	  $sql = "SELECT $field1,$field2 FROM " . get_table_name($table);
	  // if($where){ }
		   $sql = $sql.' '.$where;
	  
	  $res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	 
	 if(mysql_num_rows($res)>0){
		 $html = '<select name="'.$name.'"  id="'.$name.'" '.$extra.' '.$classname.' '.$function.' >';
			 if($select){
				 $html .= '<option value="'.$val.'">'.$select.'</option>';
			 }

		 while($ka=mysql_fetch_array($res)){
				 if($ka[$field2]==$id && $id!=''){
					$html .= '<option value="'.$ka[$field2].'" selected="selected">'.$ka[$field1].'</option>';
				 }else{
					$html .= '<option value="'.$ka[$field2].'">'.$ka[$field1].'</option>';
				 }
		 }
	 }

	  return $html;
	}

	function pagination1($rcount,$row_page,$counter,$url,$a,$div1){
	  $html='';
	  $next='';
	  $prev='';
	  $first='';
	  $last ='';
	  $v ='';
	  $function1 ='';
	  $functio2 ='';
	  $function3 ='';
	  $function4 ='';
		  if(($rcount%$row_page)==0){
			  $v = $rcount-$row_page;
		  }else{
			  $v = $rcount;
		  }

		  if($counter < $v && $rcount > $row_page){
			  $new_counter1  = $counter+ $row_page;
			  $url1 =$url."?".$a."=".$new_counter1;
			  $url2 =$url."?".$a."=".$v;
			  $function1 ='onclick="show_pagination(\''.$a.'\',\''.$new_counter1.'\',\''.$url.'\'
							,\'next\',\''.$div1.'\');"';
			  $function2 ='onclick="show_pagination(\''.$a.'\',\''.$v.'\',\''.$url.'\'
							,\'last\',\''.$div1.'\');"';
			  /*$next ='<a href="'.$url1.'">Next>></a> ';
			  $last ='<a href="'.$url2.'">Last></a> ';*/
			  $next ='<a href="javascript:void(0);" '. $function1.'>Next>></a> ';
			  $last ='<a  href="javascript:void(0);" '. $function2.'>Last></a> ';
		  }else{
			  $next ='<a href="javascript:void(0);">Next</a> ';
			  $last ='<a href="javascript:void(0);">Last</a> ';
		  }

		  if($counter > 0 && $rcount > $row_page){
			  $new_counter2  = $counter-$row_page;
			  $url3 =$url."?".$a."=".$new_counter2;
			  $url4 =$url."?".$a."=0";
			  $function3 ='onclick="show_pagination(\''.$a.'\',\''.$new_counter2.'\',\''.$url.'\'
						  ,\'prev\',\''.$div1.'\');"';
			  $function4 ='onclick="show_pagination(\''.$a.'\',\'0\',\''.$url.'\'
						   ,\'first\',\''.$div1.'\');"';
			 /* $prev ='<a href="'.$url3.'">Prev<<</a> ';
			  $first ='<a href="'.$url4.'">First<</a> ';*/
			   $prev ='<a  href="javascript:void(0);" '. $function3.'>Prev<<</a> ';
			  $first ='<a  href="javascript:void(0);" '. $function4.'>First<</a> ';
		  }else{
			  $prev ='<a href="javascript:void(0);">Prev</a> ';
			  $first ='<a href="javascript:void(0);">First</a> ';
		  }

		  $html =$first.$prev.$next.$last;
		  return $html;

	}
	function check_string($var,$error_label,$message1,$mesage2){
	 $error    ='';
	 $pattern  = '/^([a-zA-Z0-9 ]+([-_. ])?([a-zA-Z0-9 ])*)+$/';

		if($var =''){
			  $error .= "#".$error_label."*".$message1."#";
		}else{
			if (!preg_match($pattern,$var)){
			  $error .= "#".$error_label."*".$message2."#";
			}
		}
	return $error;
	}

	function check_blanks($var,$message,$err_label){
		 $error  = '';

		  if($var ==''){
			   $error .= "#".$err_label."*".$message."#";
		  }

		 return $error;
	}


	function check_drpdwn($var,$message,$err_label){
		 $error  = '';
		  if($var == '0'){ 
			   $error .= "#".$err_label."*".$message."#";
		  }
		 return $error;
	}

	function check_level($arr,$level_key,$level_val){
	 $cut ='';
		if(is_array($arr)){
			foreach($arr as $key=>$val){
				$i++;
					 if($level_key == $key){
						 $cut = $i;
						 break;						
					  }
				}

					if($cut && $cut <= sizeof($arr)){
						$arr = array_slice($arr,0,$cut);
					}else{
						$arr[$level_key] = $level_val;
					}
		}else{
			$arr[$level_key] = $level_val;
		}

		return $arr;
	}


	function check_password($pass1,$pass2,$message1,$message2,$message3,$message4,$label1,$label2,$label3){
	$error ='';

	   if($pass1==''){
		  $error .= "#".$label1."*".$message1."#";
		}
		if($pass2==''){
		   $error .= "#".$label2."*".$message2."#";
		}

			if($pass1 && $pass2){
				 if(strlen($pass1)<6 || strlen($pass1)>14){
					 $error = "#".$label3."*".$message3."#";
				 }else{
						 if(strcmp($pass1,$pass2)!=0){
							 $error = "#".$label3."*".$message4."#";
						 }
				 }
			}

		

		return $error;
	}
	
	function checkOldPassword($oldPass,$user_id){
	
		$sql = "SELECT * FROM users WHERE password='$oldPass' AND id='$user_id' ";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);	
		
		if(mysql_num_rows($result)>0)
			return true;
		else
			return false;

	
	}

	function resetPassword($password,$user_id){
		
		global $myDB;
		
		$sql = "UPDATE users SET password='$password' WHERE id='$user_id' ";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		if(mysql_affected_rows($myDB)==1){
		
			return "Password reset successfully...";
		
		}else{
		
			return "Password reset failed...";
		}
	
	
	
	}

	function check_email2($var,$message1,$message2,$error_label,$table,$where,$message3){
	 $error ='';
	  $pattern ="/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/";
		   if($var==""){$error = "#".$error_label."*".$message1."#";}

		   if($var){
				// if (!eregi($pattern,$var)){
				 if (!preg_match($pattern,$var)){
				  $error = "#".$error_label."*".$message2."#";
				}
				
				/*
				if(empty($error)){
					$e = get_where_record($table, $where);
					if($e){
						$error = "#".$error_label."*".$message3."#";
					}
				}
				*/
		   }

	  return $error;
	}

	function check_duplicates($var,$table,$fld,$msg1,$msg2,$err_label) {
	  $error  = '';
		  if($var ==''){
			   $error = "#".$err_label."*".$msg1."#";
		  }

		  if($var){
			$e=   row_exists($var,$table,$fld," = ","'");
			 if($e){
				 $error = "#".$err_label."*".$msg2."#";
			 }
		  }

		 return $error;


	}

	 function check_duplicates2($var,$table,$where,$msg1,$msg2,$err_label) {
		$error  = '';
		if($var ==''){
		   $error = "#".$err_label."*".$msg1."#";
		}

		if($var){
			$e	=  get_where_record($table,$where);
			if($e){
				$error = "#".$err_label."*".$msg2."#";
			}
		}

		return $error;


	}

	function check_session($seesionid,$table,$id,$url){

		 if(!isset($seesionid) && $sessionid==''){
			  redirect($url);
		 }else{
			 $u = row_exists($seesionid,$table,$id,'=',"'");
			 if($u==0){
				 redirect($url);
			 }else{
				 return true;
			 }
		 }
	}

	function get_multiple_dropdown($table,$field1,$field2,$where,$name,$eid,$id='',$select='',$val=0,$classname='',
		  $function='', $extra='' ){

	$html='';
	$sql = "SELECT $field1,$field2 FROM " . get_table_name($table);
	   if($where){ 
		   $sql = $sql.' '.$where;
	  }
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	 
	 if(mysql_num_rows($res)>0){
		 $html = '<select multiple name="'.$name.'"  id="'.$eid.'" '.$extra.' '.$classname.' '.$function.' >';
			 if($select){
				 $html .= '<option value="'.$val.'">'.$select.'</option>';
			 }

			 while($ka=mysql_fetch_array($res)){
					 if($ka[$field2]==$id && $id!=''){
						$html .= '<option value="'.$ka[$field2].'" selected="selected">'.$ka[$field1].'</option>';
					 }else{
						$html .= '<option value="'.$ka[$field2].'">'.$ka[$field1].'</option>';
					 }
			 }
	 }

	return $html;
	}

	function chars_left($str1,$arr1=array(),$character){
	  $arr2 = array();
	  $html = '';
	  if(!empty($str1)){
		   $lines = explode("\n", $str1);
			   if(count($lines)>1){
				   $arr2['newline'] = (count($lines)-1);
				   
			   }
		   $arr  = str_split($str1);
		   $carr = array_count_values($arr);
					foreach($arr1 as $val){
						 if(in_array($val,$arr)){
							 if($carr[$val]){
							   $arr2[$val] = 2*$carr[$val];
							 }
						 }
					}
			if(is_array($arr2) && sizeof($arr2)>0){				
				$new_carr = array_diff_key($carr,$arr2);				 
				$html= $character - (array_sum($arr2) + array_sum($new_carr));			
				
			}else{
				$html = $character-(array_sum($carr));
			}
	 }
	return $html;
	}

	function get_menus($url,$table,$cur_page,$cond,$class=''){	 
	 $arr  = array();
	 $check =1;

	 $sql  = 'SELECT * from '.$table.' where  show_menu=1 '.$cond.'order by sort_order'; 
	 $res  = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if(mysql_num_rows($res)>0){
			while($ka = mysql_fetch_assoc($res)){				
				 if($ka['parent_id']==0 && $cur_page == $ka['page_url']){
					  $arr[$ka['id']]   = '<li '.$class.'><a href="'.$url.$ka['page_url'].'" >
								   <span>'.ucfirst(strtolower($ka['page_name'])).'</span></a></li>';
				 }elseif($ka['parent_id']!=0 && $cur_page == $ka['page_url']){
					$check = $ka['parent_id'];
				 }
				 else{
					  if($ka['parent_id'] ==0){
					   $arr[$ka['id']]   = '<li><a href="'.$url.$ka['page_url'].'">
										   <span>'.ucfirst(strtolower($ka['page_name'])).'</span></a></li>';
					  }
				}
			 }
			  if($check){
				  if(array_key_exists($check,$arr)){
					 if($arr[$check]){
						$arr[$check] = str_replace('<li>','<li '.$class.'>',$arr[$check]);
					 }
				  }
			  }

		 }
	return $arr;
	}

	function check_access($accessid,$where,$userid,$url,$groupid=array()){	
	 $row = false;
		 if(!isset($accessid) && $accessid==''){
			  redirect($url);
		 }else{
			 $u = get_where_record1('features', $where);
			  if(is_array($groupid) && sizeof($groupid)>0){
				 $str = '("'.implode('","',$groupid).'")';
			  }
				 if($u['id'] && !empty($str)){
					 $sql1  = 'SELECT max(v1) as v1,max(v2) as v2,max(v3) as v3,max(v4) as v4,max(v5) as v5,max(v6)				as v6,max(v7) as v7,max(v8) as v8, max(v9) as v9,max(v10) as v10
							   from groups_features where group_id in '.$str .' and feature_id 	="'.$u['id'].'"';
					 $res1  = mysql_query($sql1) or mysql_error_show($sql1,__FILE__,__LINE__);

					   if(mysql_num_rows($res1)>0){		 					   
							   $row = mysql_fetch_assoc($res1);						   
						  }
					 $sql2  = 'SELECT * from users_features where user_id = "'.$userid.'" and feature_id					    ="'.$u['id'].'"';
					 $res2  = mysql_query($sql2) or mysql_error_show($sql2,__FILE__,__LINE__);
						  if(mysql_num_rows($res2)>0){							  
							   $row = mysql_fetch_assoc($res2);							   
						  }						  
					
					}
			  }
	  return $row;
	}

	 function make_array($a1,$a2=array()){
		 unset($a2);
		 if(is_array($a1) && sizeof($a1)>0){
			foreach($a1 as $key=>$val){		
				 if(isset($val)){
					 $a2[$key] = $val;
				}
			  }
		}
		 return $a2;
	 }

	function get_arr_dropdown($rearr,$name,$index,$id,$gname){
	$html = '';
	$html = '<select name="'.$name.'" id="'.$name.'">';
	foreach($rearr as $key=>$arr){	  			  
		 if($arr[$id]==$index){
			  $html .='<option selected="selected" value="'.$arr[$id].'">'.$arr[$gname].'</option>';
		   }else{
			 $html .='<option  value="'.$arr[$id].'">'.$arr[$gname].'</option>';
		   }  	 
	}
	$html .='</select>';
	return $html;
	}
	function get_single_dropdown($rearr,$name,$index){
		$html = '';
		$html = '<select name="'.$name.'" id="'.$name.'">';
		foreach($rearr as $key=>$val){
			 $html .= '<option selected="selected" value="0">--Select operator--</option>';
			 if($key==$index){
				  $html .='<option selected="selected" value="'.$key.'">'.$rearr[$key].'</option>';
			   }else{
				 $html .='<option  value="'.$key.'">'.$rearr[$key].'</option>';
			   }  	 
		}
		$html .='</select>';
		return $html;
	}


	function phoneValidation($phno){

		if(strlen($phno) == 10  ){

			$phno = substr($phno,-10);
			//if(ereg('^[8|9]{1}[0-9]{9}$', $phno))
			if(preg_match('/^[8|9]{1}[0-9]{9}$/', $phno))
				return true;
			else
				return false;
		}else
			return false;

	}

	function checkDuplicateAddressbookDetails($addressbook_id,$phone)
	{

		$sql = "SELECT COUNT(1) AS count FROM address_bookdetails WHERE  address_id like '%".$addressbook_id."%' and contactNumber ='".$phone."'";
		$res = @mysql_query($sql);
		$row=mysql_fetch_assoc($res);
			
		return $row['count'];
	}

	function get_correct_length($var,$length1,$length2,$msg1,$label){
		  $error ='';
		  if($var){ 
			  if($length2 < strlen($var)  ||  strlen($var) < $length1){
			   $error = "#".$label."*".$msg1."#";
			  }
		  }

		   return $error;
	}

	function check_empty($var,$message){
		 $error  = '';
		  if($var ==''){
			   $error = $message;
		  }
		 return $error;
	}

	function check_duplicates3($var,$table,$where,$msg1,$msg2) {
	  $error  = '';
		  if($var ==''){
			   $error = $msg1;
		  }
		  if($var){
			$e	=  get_where_record($table,$where);
				 if($e){
					 $error = $msg2;
				 }
		  }
	return $error;	
	}

	function senderidValidation($senderid){

		$senderid = trim($senderid);
		if (strlen($senderid)>8){
			return false;
		}else{
			return true;
		}
	}

	function getLoginUseType($id){

		$query ="SELECT * FROM user_types WHERE id = '$id'";
		$result = @mysql_query($query);
		$row =  mysql_fetch_assoc($result);
		return $row['right'];

	}
	function get_left_menu($menu_label='',$class=''){
		global $project_vars;
		$html ='';
		if(is_array($project_vars["menu"]) && sizeof($project_vars["menu"]) > 0){
			 foreach($project_vars["menu"] as $left_menu){ 
				  if($left_menu['label']== $menu_label){
					  $html .='<li ><a href="'.BASE_URL.$left_menu['url'].'" '.$class.'>'.
							   $left_menu['label'].'</a>';
				  }else{
					  $html .='<li ><a href="'.BASE_URL.$left_menu['url'].'">'.$left_menu['label'].'</a>';
				  }
			 }
		}
		 return $html;
	 }

	function get_date_format($date,$format=''){
		global $project_vars;
		 if($format ==''){
			$format = $project_vars['date_format_mysql'];
		 }
		 if($date =="")
			 return false;
		  else
			  return date($format,strtotime($date));
	}
	 function getUserType(){
	
		$sql = "SELECT id,name FROM user_types WHERE user_types.right !=1 ";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		while($row = mysql_fetch_assoc($result)){
		
			$arrUserTypeList[] = array(
									'id'=>$row['id'],
									'user_type'=>$row['name']
								  );
		
		
		}
		if(count($arrUserTypeList)>0)
		
			return $arrUserTypeList;
		
		else
			return false;
	}

	function getOperators(){
	
		

		$sql = "Select id,operator_name from operators ";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		while($row = mysql_fetch_assoc($result)){
		
			$arrOperator[] =array('id'=>$row['id'], 'operator_name'=>$row['operator_name']);
		
		}
		return $arrOperator;

	}

	/*
		function added by akhilesh for getting nth level enduser of a parent company 	
	*/
	
	function getNthLevelEndusers($userid,$arrUserId=array(),$deleted=true){
		

		
		static $html='<ul class="all"><li><input type="checkbox" name="all" id="all" onclick="checkAllUsers();"	/>All</li></ul> 
		<ul id="example">';
		
		if($deleted){
			
			$sqlDeleted = " AND active=1 AND is_deleted !=1 ";

		}else{
		
			$sqlDeleted = "  ";
		}

		$sql = "SELECT id,username,is_deleted,active FROM users WHERE  parent_id='".$userid."' AND id != '".$userid."' ".$sqlDeleted;
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		while($row = mysql_fetch_assoc($result)){
			
			$html .= '<li>&nbsp;<input type="checkbox" name="users[]" value="'.$row['id'].'" ';
		
			if(is_array($arrUserId)){
				if(in_array($row['id'],$arrUserId)){
					$html .= 'checked';
				}
			}
			
			$html .=' />&nbsp;';
			if($row['is_deleted']==1 || $row['active'] == 0){
			
			  $html .='<font style="color:#FF0000">'.$row['username'].'</font>';
			
			}else{
			
				$html .= $row['username'];
			}
			
			//$sqlInner = "Select COUNT(1) AS count FROM users WHERE active=1 AND is_deleted!=1 AND parent_id='".$row['id']."'";
			$sqlInner = "Select COUNT(1) AS count FROM users WHERE  parent_id='".$row['id']."'";
			
			$resultInner = mysql_query($sqlInner) or mysql_error_show($sqlInner,__FILE__,__LINE__);
			$rowInner = mysql_fetch_assoc($resultInner);
			
			if($rowInner['count'] > 0){
			
				$html .= '<ul>';
				getNthLevelEndusers($row['id'],$arrUserId,$deleted);
			
			}
			
			$html .= '</li>';
			
		}
		
		$html.='</ul>';

		return  $html;
		
	
	}

	/*
		function added by akhilesh for transction of mysql rollback and commit 	
	*/
	function begin() {
		@mysql_query("BEGIN");
	}

	function commit() {
		@mysql_query("COMMIT");
	}

	function rollback() {
		@mysql_query("ROLLBACK");
	}

	function getNthLevelChild($user_id){

		static $arrChildUserId = array();
		$sql= " SELECT id FROM users WHERE parent_id='".$user_id."' AND active='1' and is_deleted !=1";
		$result   = mysql_query($sql);
		
		while($rec = mysql_fetch_assoc($result)){

			array_push($arrChildUserId,$rec['id']);
			$sql2 = " SELECT COUNT(1) as count FROM users WHERE parent_id='".$rec['id']."' AND 	active=1 AND is_deleted != 1";
			$result2  = mysql_query($sql2);
			$row =  mysql_fetch_assoc($result2);
			if($row['count']>0){
				getNthLevelChild($rec['id']);
			}

		}
		return $arrChildUserId;
		
	}









































/*----------------- sender related functions - start -----------------*/
function getPID($command="") {
	global $project_vars;
	if($command == "") {
		return false;
	} else {
		$command = "ps aux | grep -v grep | grep '" . $command . "'";
		exec($command, $pslist);
		for($i=0; $i < count($pslist); $i++) { 
			$pslist[$i] = ereg_replace(" +"," ",$pslist[$i]);
			$item[0] = strtok($pslist[$i]," ");
			for($s=1 ; $s < 11; $s++) {
				$item[$s]  = strtok(" ");
			}
			return $item[1];

//			echo "<TR>\n";
//			for($p=0; $p < 11; $p++) { 
//				echo "   <TD "; 
//				echo ">$item[$p]</TD>\n";
//			} 
//			echo "</TR>\n";
		}

		return false;
	}
}


function getNowMySQL() {
	$sql = "SELECT NOW()";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$row = mysql_fetch_array($res);
	return $row[0];

}

function startProcess($command="") {
	global $project_vars;
	if($command == "") {
		return false;
	} else {
		exec($command . " > /dev/null &");
//		system($command);
//		shell_exec($command);
//		passthru($command);
		return true;
	}
}


function stopProcess($pid="") {
	global $project_vars;
	if($pid == "") {
		return false;
	} else {
		exec("kill " . $pid . " > /dev/null &");
//		system($command);
//		shell_exec($command);
//		passthru($command);
		return true;
	}
}

function rLog($msg){
	$msg = "[".date('Y-m-d H:i:s')."] ".$msg."\n";
	print($msg);


	$myFile = LOGS_DIR . date("Y-m-d") . ".log";
	$fh = fopen($myFile, 'a') or die("can't open file");
	fwrite($fh, $msg);
	fclose($fh);
}


function rLogDLR($msg){
	$msg = "[".date('Y-m-d H:i:s')."] ".$msg."\n";
	print($msg);
	$myFile = LOGS_DIR . "dlr-" . date("Y-m-d") . ".log";
	$fh = fopen($myFile, 'a') or die("can't open file");
	fwrite($fh, $msg);
	fclose($fh);
}


function TestLink($id) {
	global $project_vars, $smpp;
	$ret = $smpp[$id]["object"]->TestLink();
	$smpp[$id]["name"]["status"] = $ret;
	if($ret == 0) {
		rLog($smpp[$id]["name"] . " Link Status: UP...");
		return true;
	} else {
		rLog($smpp[$id]["name"] . " Link Status: DOWN...");
		return false;
	}
}


function TestLinkDLR($id) {
	global $project_vars, $smpp;
	$ret = $smpp[$id]["object"]->TestLink();
	$smpp[$id]["name"]["status"] = $ret;
	if($ret == 0) {
		rLogDLR($smpp[$id]["name"] . " Link Status: UP...");
		return true;
	} else {
		rLogDLR($smpp[$id]["name"] . " Link Status: DOWN...");
		return false;
	}
}


function openSMPP($id) {
	global $project_vars, $smpp;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE id LIKE '" . $id . "'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$row = mysql_fetch_assoc($res);
	$smpp[$row["id"]] = $row;
	$smpp[$row["id"]]["object"] = new SMPPClass();
	$smpp[$row["id"]]["object"]->_debug = false;
	$smpp[$row["id"]]["status"] = $smpp[$row["id"]]["object"]->Start($row["send_ip"], $row["send_port"], $row["send_user"], $row["send_pass"], $row["send_sys_type"], 'transmitter');

	if($smpp[$row["id"]]["status"] == true) {
		rLog($row["name"] . " connected.");
		TestLink($row["id"]);
	} else {
		rLog($row["name"] . " could not connect.");
	}
	return $smpp[$row["id"]]["status"];
}


function openSMPPs() {
	global $project_vars, $smpp;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE active=1 AND send_method LIKE 'SMPP'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	while($row = mysql_fetch_assoc($res)) {
		openSMPP($row["id"]);
	}
}


function openHTTP($id) {
	global $project_vars, $http;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE id LIKE '" . $id . "'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$row = mysql_fetch_assoc($res);
	$http[$row["id"]] = $row;
	rLog($row["name"] . " connected.");
	return true;
}


function openHTTPs() {
	global $project_vars, $http;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE active=1 AND send_method LIKE 'HTTP'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	while($row = mysql_fetch_assoc($res)) {
		openHTTP($row["id"]);
	}
	return true;
}


function openSMPPDLR($id) {
	global $project_vars, $smpp;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE id LIKE '" . $id . "'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$row = mysql_fetch_assoc($res);
	$smpp[$row["id"]] = $row;
	$smpp[$row["id"]]["object"] = new SMPPClass();
	$smpp[$row["id"]]["object"]->_debug = false;
	$smpp[$row["id"]]["status"] = $smpp[$row["id"]]["object"]->Start($row["receive_ip"], $row["receive_port"], $row["receive_user"], $row["receive_pass"], $row["receive_sys_type"], 'receiver');

	if($smpp[$row["id"]]["status"] == true) {
		rLogDLR($row["name"] . " connected.");
		TestLink($row["id"]);
	} else {
		rLogDLR($row["name"] . " could not connect.");
	}
	return $smpp[$row["id"]]["status"];
}


function openSMPPsDLR() {
	global $project_vars, $smpp;
	$sql = "SELECT * FROM " . get_table_name("channels") . " WHERE active=1 AND receive_method LIKE 'SMPP'";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	while($row = mysql_fetch_assoc($res)) {
		openSMPPDLR($row["id"]);
	}
}


function TestLinks() {
	global $project_vars, $smpp, $global_conn_status, $argv;
	if($global_conn_status == 0) {
		$sql = "UPDATE " . get_table_name("processes") . " SET status='2' WHERE id like '" . $argv[1] . "'";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		return false;
	}
	rLog("Testing all connections...");
	$global_conn_status = 0;
	foreach ($smpp as $smpp_element) {
		$ret = TestLink($smpp_element["id"]);
		if($ret == false) {
			rLog($smpp_element["name"] . " seems to be down, trying to reconnect...");
			openSMPP($smpp_element["id"]);
		}
	}
	$global_conn_status = 1;

	$sleep = rand(1,5);
	sleep($sleep);
}

function TestLinksDLR() {
	global $project_vars, $smpp;
	rLogDLR("Testing all connections...");
	foreach ($smpp as $smpp_element) {
		$ret = TestLinkDLR($smpp_element["id"]);
		if($ret == false) {
			rLogDLR($smpp_element["name"] . " seems to be down, trying to reconnect...");
			openSMPPDLR($smpp_element["id"]);
		}
	}

	$sleep = rand(1,5);
	sleep($sleep);
}


function format_no($no,$format) {
	global $project_vars;
	if($format == "10 Digit") {
		return substr($no, -10);
	} else if($format == "91 + 10 Digit") {
		return ("91" . substr($no, -10));
	} else if($format == "+91 + 10 Digit") {
		return ("+91" . substr($no, -10));
	} else if($format == "0 + 10 Digit") {
		return ("0" . substr($no, -10));
	}
	return $no;
}


function hexToStr($hex) {
	$string='';
	for ($i=0; $i < strlen($hex)-1; $i+=2) {
		$string .= chr(hexdec($hex[$i].$hex[$i+1]));
	}
	return $string;
}

function msgCtr($text="") {
	$ret = array();
	$text1 = $text;

	$splChrs = array("~", "^", "_", "{", "}", "[", "]", "\\", "\n");

	foreach($splChrs as $splChr) {
		$text1 = str_replace($splChr, ($splChr . " "), $text1);
	}

	$ret[0] = strlen($text1);
	$ret[1] = ceil(strlen($text1) / 160);

	return $ret;
}

function authUser($user="", $pass="") {
	global $project_vars;
	if(($user == "") || ($pass == "")) {
		return false;
	}
	$sql = "SELECT * FROM " . get_table_name("users") . " WHERE username LIKE '" . $user . "' AND active=1 AND is_deleted !=1";
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	if(mysql_num_rows($res) != 1) {
		return false;
	} else {
		$row = mysql_fetch_assoc($res);
		if($row["password"] == $pass) {
			return $row;
		} else {
			return false;
		}
	}
}

function showHttpError ($msg = "Error") {
	echo $msg;
	exit;
}


function showHttpResp ($msg = "Resp") {
	echo $msg;
}


function insertNewMsg($id="", $client_id="", $user_id="", $status=0, $short_message="", $source_addr="", $destination_addr="") {
	global $project_vars;

	$ret = array();

	if($id == "") {
		$id = getNewUUID();
	}
	$tm = getNowMySQL();
	$tm1 = $tm;

	$sql = "INSERT INTO " . get_table_name("inbound_messages") . " (id, client_id, user_id, status, short_message, source_addr, destination_addr, created, modified) VALUES ('" . $id . "', '" . $client_id . "', '" . $user_id . "', '" . $status . "', '" . addslashes($short_message) . "', '" . $source_addr . "', '" . $destination_addr . "', '" . $tm . "', '" . $tm1 . "')";
	mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$ret[] = $id;
	$ret[] = $tm;

	return $ret;
}


function loadUser($user_id="") {
	global $project_vars;
	if($user_id == "") {
		return false;
	}
	$u = get_row_assoc($user_id, "users", 'id', " LIKE ", "'");
	if(isset($u["id"]) && ($u["id"] != "")) {
		return $u;
	} else {
		return false;
	}
}

function loadDefaultDeliverPercentage() {
	global $project_vars;
	$sql = "SELECT * FROM " . get_table_name("global_delivery");
	$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
	$row = mysql_fetch_assoc($res);
	return $row;
}


function getUserDeliveryPercentage($user_id) {
	global $project_vars;
	$ret = array();

	$u = loadUser($user_id);
	if(isset($u["id"]) && ($u["id"] != "")) {
		if($u["default_delive"] == 1) {
			if($u["parent_id"] != $u["id"]) {
				return getUserDeliveryPercentage($u["parent_id"]);
			} else {
				$ret = loadDefaultDeliverPercentage();
				unset($ret["id"]);
				unset($ret["created"]);
				unset($ret["modified"]);
			}
		} else {
			$ret["to_deliv"] = $u["to_deliv"];
			$ret["sent"] = $u["sent"];
			$ret["deliv"] = $u["deliv"];
			$ret["undeliv"] = $u["undeliv"];
			$ret["pending"] = $u["pending"];
			$ret["expired"] = $u["expired"];
		}
		return $ret;
	}
	return false;
}
/*----------------- sender related functions - end -----------------*/



?>