<?php
    define("ISPROCESSED", 1); 
    define("MAXRECIPIENTS", 100);

    $MESSAGE_CODE = array(
             0 => array('code' => 100, 'message' => 'Successful (placed message in queue for delivery)'),
             1 => array('code' => -1, 'message' => 'Invalid XML format'),
             2 => array('code' => -2, 'message' => 'Not enough credits in account'),
             3 => array('code' => -3, 'message' => 'Invalid API key'),
             4 => array('code' => -4, 'message' => 'Destination Mobile number missing / Invalid format'),
             5 => array('code' => -5, 'message' => 'SMS text missing'),
             6 => array('code' => -6, 'message' => 'Sender name missing / invalid format / Not active in account'),
             7 => array('code' => -7, 'message' => 'Network not covered'),
             8 => array('code' => -8, 'message' => 'Error-Undefined'),
             9 => array('code' => -9, 'message' => 'Invalid message id, too long (max 10 chars) or contains non numeric character'),
             10 => array('code' => -10, 'message' => 'Maximum number of recipient in one single API call is 100'),
             11 => array('code' => -11, 'message' => 'Error -11'),
             12 => array('code' => -12, 'message' => 'Message too long (max 160 char)'),
             13 => array('code' => -13, 'message' => 'Invalid username/password')
                    ); 

    // http://www.tech-evangelist.com/2007/11/05/preventing-sql-injection-attack/
    function cleanQuery($string)
    {
      if(get_magic_quotes_gpc())  // prevents duplicate backslashes
      {
        $string = stripslashes($string);
      }
      if (phpversion() >= '4.3.0')
      {
        $string = mysql_real_escape_string($string);
      }
      else
      {
        $string = mysql_escape_string($string);
      }
      return $string;
    }



    function saveToLogTable($apikey, $smsid, $username, $sender_name, $destination_number, $message, $response_code, $response_message)
    {
			$sqlLog = "INSERT INTO api_log_messages 
					set 
					apikey = '".$apikey."',
					sms_store_id = '".$smsid."',
					username = '".$username."',
					sender_name ='".$sender_name."',
					destination_number ='".$destination_number."',						
					message ='".mysql_real_escape_string($message)."',
					response_code ='".$response_code."',
					response_message ='".mysql_real_escape_string($response_message)."'";

			$resutLog = mysql_query($sqlLog) or mysql_error_show($sqlLog,__FILE__,__LINE__); 
    }        


    function getPhoneNumber($phoneNumber)
    {
        $phoneNumberWithNoCountryCode =  substr($phoneNumber, 4); 

        return $phoneNumberWithNoCountryCode;
    }


    function checkNumberFormat($phoneNumber)
    {
        $valid = 0;        
        $valid =  ($phoneNumber[0] == "+") ? 1 : 0;

        return $valid;
    }

    function checkDestination($destNumber)
    {        
        $plusSign = $destNumber[0];
        $countryCode = $destNumber[0].$destNumber[1].$destNumber[2].$destNumber[3];

        $statuscheckDestination = (strcmp($countryCode,"+255") == 0) ? 1 : 7;

        if ($statuscheckDestination==1)
        {
            $strNumber = substr($destNumber, 1);
            $numberLength = strlen($strNumber);

            if ( ($numberLength < 11) or ($numberLength > 14) )
            {
                $statuscheckDestination = 4;
            }
            else
            {
                $i=0;
                while ($i<$numberLength)
                {
                    if (!is_numeric($strNumber[$i]))
                    {
                        $statuscheckDestination = 4;
                        break;
                    }
                    $i++;
                }
            }
        }

        if ($plusSign != '+')
        {
            $statuscheckDestination = 4; 
        }

        return $statuscheckDestination;
    }


    function getCredit($messageLength)
    {
        $credit = 0;
        $credit = ceil($messageLength / 160);
        return $credit;
    }

    function smsAPISaveToSmsStores($apikey, $sender_id, $message, $messageSize, $numberOfRecipient)
        {
          $userId = $apikey;
          $senderID = $sender_id;
          $credit = getCredit($messageSize);
          $credit = $credit * $numberOfRecipient;
          $addressbookId = "API";
          $contactId = "API";
          $receiverId = '';  
          $receiverType = '';  
          $senderType = "VEN";
          $advertiserId = '';
          $textMassage = $message;
          $massegeLength = $messageSize;
          $massegeCount = getCredit($messageSize);
          $is_proccessed = ISPROCESSED; // shell.sender.php will fetch the row with this field in it's WHERE condition
                                    


			$sql = "INSERT INTO sms_stores 
					set 
					id = UUID(),
					sent_by = '".$userId."',
					sender_id = '".$senderID."',
					credit ='".$credit."',
					addressbook_id ='".$addressbookId."',						
					contact_id ='".$contactId."',
					receiver_id ='".$receiverId."',
					receiver_type ='".$receiverType."',
					sender_type ='".$senderType."',
					advertiser_id ='".$advertiserId."',
					text_message ='".mysql_real_escape_string($textMassage)."',
					message_length ='".$massegeLength."',
					message_count ='".$massegeCount."',
					is_proccessed = ".$is_proccessed.",
					send_time = NOW(),
                    sent_time = NOW(),
					created = NOW(),  
                    modified = NOW()";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
            
            $rowStatus = (mysql_affected_rows()>0) ? true : false;
            return $rowStatus;
			
		}



    function getSmsStoresID($apikey, $sender_id)
    {
			
			$sql= "SELECT  id FROM	sms_stores WHERE is_proccessed = ".ISPROCESSED."  
                AND sent_by = '$apikey' and sender_id = '$sender_id'
				AND send_time <= NOW() 
				ORDER BY created DESC LIMIT 1";
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row	= mysql_fetch_assoc($result);

			$smsstoreID = ($row) ? $row['id'] : '';
		    return $smsstoreID;
	}


    function searchSenderName($arraySender, $sendername) 
    {   
        foreach ($arraySender as $subarray)
        {  
            if (isset($subarray['senderid']) && strtolower($subarray['senderid']) == strtolower($sendername))
            {
                  return $subarray;       
            }
        } 
    }

    /* Broadcast library */
    function validateXML($msgXML)
    {
        $i=0;
        try 
        { 
            $xml = new SimpleXMLElement($msgXML);

            try
            {     
                $arrReceiver = array();   
                foreach ($xml->Message as $msg) 
                {
                    $recvCount = count($msg->Receivers->Receiver);
                    //print_r($msg->Receivers)."<p>";
                    //echo "Message = ".$msg->Content."<br>";
                    $arrReceiver['message']['content'] = $msg->Content;
                    $arrReceiver['callbackurl'] = $msg->Callbackurl->Url;
                    $tempStrRecipient = "";
                    $idxClean = 0;    
                    $i=0;
                    while ($i < $recvCount)
                       { 
                            $messageId = $msg->Receivers->Receiver[$i]['id'];
                            $destinationNumber = $msg->Receivers->Receiver[$i];
                            // if there's double destination number, skip the next one
                            $double = strpos($tempStrRecipient, $destinationNumber);
                            if ($double === false) // was not found
                            {
                                $arrReceiver[$idxClean]['messageid'] = $messageId;
                                $arrReceiver[$idxClean]['destination'] = $destinationNumber;
                                //echo "$idxClean. $messageId = $destinationNumber ( $tempStrRecipient )<br>";
                                $idxClean++;
                                $tempStrRecipient .= $destinationNumber.",";
                            } 
                            $i++;
                       }
                }

                //echo " recipients = $idxClean<br>";
                $arrReceiver['numberofrecipient'] = $idxClean;

                foreach ($xml->Authentication as $auth) 
                {                    
                   $arrReceiver['sendername'] = cleanQuery($auth->Sendername);
                   $arrReceiver['username'] = cleanQuery($auth->Username);
                   $arrReceiver['password'] = cleanQuery($auth->Password);
                   $arrReceiver['apikey'] = cleanQuery($auth->Apikey); 
                }

                //print_r($arrReceiver);
            
                return $arrReceiver; 
            }
            catch (Exception $e) 
            { 
                return false; 
                //echo  "bad xml element"; 
            }

        }
        catch (Exception $e) 
        { 
            return false; 
            //echo "bad xml..."; 
        }
    }


    function insertIntoInboundMessage($smsStoreID, $apiKey, $status, $message, $messageSize, $messageCount, $sourceAddr, $destAddr, $messageid)
    {
	
			$sqlinsertIntoInboundMessage = "INSERT INTO inbound_messages (id,sms_store_id,user_id,status,short_message,msg_length,msg_count,source_addr,destination_addr,created, user_message_id) VALUES "."(UUID(),'$smsStoreID','$apiKey',$status,'$message',$messageSize, $messageCount, '$sourceAddr', '$destAddr', NOW(),'$messageid')";

			$result = mysql_query($sqlinsertIntoInboundMessage) or mysql_error_show($sqlinsertIntoInboundMessage,__FILE__,__LINE__);
			if(mysql_affected_rows()>0)
				return true;
			else
				return false;
			
    }

    function addCallBackURLToUserData($callbackurl, $apikey, $username)
    {
		
		$sql = "UPDATE mst_vendors SET callback_url = '$callbackurl' WHERE id='$apikey' AND username = '$username' ";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		return $res;
	
	}

    function checkMessageId($msgID)
    {
        $status = true;
        $i=0;
        $numberLength = strlen($msgID);
        if ($numberLength<=10)
        {
            while ($i < $numberLength)
            {
               if (!is_numeric($msgID[$i]))
               {
                  $status = false;
                  break;
               }
              $i++;
            }
        } 
        else
        {
            $status = false;
        }
        return $status;
    } 

    function getSmsWithDeliveryReport()
    {
                $sqlDLR = "select b.sms_store_id, b.user_id, b.destination_addr, b.remote_status, b.user_message_id, b.remote_push_time, b.remote_deliv_time from sms_stores a, inbound_messages b where a.addressbook_id='API' 
and a.contact_id='API' 
and a.id = b.sms_store_id 
and b.remote_status <> ''
and b.user_message_id <> ''
and b.remote_push_time <> ''
and b.remote_deliv_time <> '' 
and b.callbackurl_is_called = 0 ORDER BY b.created ASC LIMIT 0, 100";			

			    $result = mysql_query($sqlDLR) or mysql_error_show($sqlDLR,__FILE__,__LINE__);
                $arrSms = array();
                $i = 0;
			    while($row	= mysql_fetch_assoc($result))
                {
				    $arrSms[$i]['sms_store_id']	= $row['sms_store_id'];
				    $arrSms[$i]['user_id'] = $row['user_id'];
				    $arrSms[$i]['destination'] = $row['destination_addr'];
				    $arrSms[$i]['remote_status'] = $row['remote_status'];
				    $arrSms[$i]['user_message_id'] = $row['user_message_id'];
				    $arrSms[$i]['remote_push_time'] = $row['remote_push_time'];
				    $arrSms[$i]['remote_deliv_time'] = $row['remote_deliv_time'];

                    $i++;
			    }			
			    return $arrSms;	
    }


    function getUsersCallBackURL($apikey)
    {
        $sql= "select callback_url from mst_vendors where id = '$apikey'";

	    $result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		$row	= mysql_fetch_assoc($result);
		$theUrl = ($row) ? $row['callback_url'] : '';
        return $theUrl;    
    }

    function sendToCallBackURL($callbackurl, $messageid, $destnum, $deliverystring, $remotepushtime, $remotedelivtime)
    {
        $posturl = $callbackurl."?id=$messageid&mobile=$destnum&status=".urlencode($deliverystring)."&sentdate=".urlencode($remotepushtime)."&donedate=".urlencode($remotedelivtime);
        $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $posturl);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    $response = curl_exec($ch);
    }

    function updateIsCalledFlag($sms_store_id, $apikey)
    {
		$sql = "UPDATE inbound_messages SET callbackurl_is_called = 1 WHERE sms_store_id = '$sms_store_id' AND user_id = '$apikey' ";
		$res = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);		
		return $res;
	}
	
function parsesms($text){
	$text=str_replace("\n"," ",$text);  // Used as a new line character in Unix
	$text=str_replace("\r"," ",$text);	// Used as a new line character in Mac OS
	$text=str_replace("\t"," ",$text);	//when someone press tab

	$array = explode(" ",$text);
	$array = array_filter($array);
	$array = array_unique($array);

	//building index of array obtained from text msg
	foreach ($array as $value){
		if($value != ''){
			$newarray[]	= trim($value);
		}
	}
	
	return $newarray;
}

?>
