<?php
	//These setting will be used by file SMS only when script run and will not make any changes to php.ini file
	ini_set(memory_limit,'250M');
	ini_set(max_execution_time,'900');
	ini_set(max_input_time,'900');
	$cookie_path = "/";
	$cookie_timeout = 60 * 60 * 24; // in seconds
	$garbage_timeout = $cookie_timeout + 600; // in seconds
	session_set_cookie_params($cookie_timeout, $cookie_path);
	ini_set('session.cookie_lifetime', 0); // added by akhilesh
	ini_set('session.gc_maxlifetime', $garbage_timeout);
	strstr(strtoupper(substr($_SERVER["OS"], 0, 3)), "WIN") ? $sep = "\\" : $sep = "/";
	$sessdir = ini_get('session.save_path').$sep.$project_vars['appname'];
	if (!is_dir($sessdir)) { mkdir($sessdir, 0777); }
	ini_set('session.save_path', $sessdir);
	session_name($project_vars['user_session_name']);
	session_start();
	
	// Set default site language
	if(!isset($_SESSION['lang']))
	{
		$_SESSION['lang'] = $project_vars['site_lang_default'];
	}
?>