<?php
	/*----------site general info - start -------------*/
	// Array of title list
	$project_vars['title'] = array(
		'default'					=> PV_TITLE_DEFAULT,
		'index.php'					=> PV_TITLE_INDEX,
		'aboutus.php'				=> PV_TITLE_ABOUT_US,
		'bulk_sms.php'				=> PV_TITLE_BULK_SMS,
		'privacy.php'				=> PV_TITLE_PRIVACY,
		'tailored_services.php'		=> PV_TITLE_TAILORED_SERVICES,
		'who_can_use.php'			=> PV_TITLE_WHO_CAN_USE,
		'contactus.php'				=> PV_TITLE_CONTACT_US,
		'help.php'					=> PV_TITLE_HELP,
		'custom_applications.php'	=> PV_TITLE_CUSTOM_APPLICATIONS,
		'how_it_works.php'			=> PV_TITLE_HOW_IT_WORKS,
		'pricing.php'				=> PV_TITLE_PRICING,
		'sms_advertising.php'		=> PV_TITLE_SMS_ADVERTISING,
		'terms_of_use.php'			=> PV_TITLE_TERMS_OF_USE,
		'faq.php'					=> PV_TITLE_FAQ
	);
	/*----------site general info - end -------------*/
	
	
	/* Mobile msg */
	// Registraion msg sent to mobile phone after regitration
	$project_vars['register_message']			= PV_REGISTER_MESSAGE;
	//Password change msg sent moblie phone after password is changed
	$project_vars['password_change_message']	= PV_PASSWORD_CHANGE_MESSAGE;
	
	
	/* Mobile confirmation config & registration */
	$project_vars['acct_activation_msg']		= PV_ACCT_ACTIVATION_MSG;
?>