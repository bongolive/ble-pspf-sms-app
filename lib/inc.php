<?php
	include_once(LIB_DIR."grab_globals.lib.php");
	include_once(LIB_DIR."config.php");
	include_once(LIB_DIR."dbconn.inc.php");
	include_once(LIB_DIR."langs.inc.php");
	include_once(LIB_DIR."common_functions.php");
	include_once(LIB_DIR."session.inc.php");
	include_once('languages/'.$_SESSION['lang'].'.inc.php');
	include_once(LIB_DIR."config_multilang.php");
	include_once(LIB_DIR."smarty.inc.php");
?>