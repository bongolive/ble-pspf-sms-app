<?php
	require_once ROOT. 'smarty/Smarty.class.php';
	$smarty = new Smarty;

	$smarty->compile_check = true;
	$smarty->debugging = false;
	$smarty->caching = 0;
	$smarty->plugins_dir[] = ROOT . 'plugins';

	$smarty->template_dir = ROOT.'templates/'; 
	$smarty->compile_dir = ROOT.'templates_c/'; 
	$smarty->cache_dir = ROOT.'cache/';
	$smarty->config_dir = ROOT.'configs/'; 


	// assing assetes path
	$smarty->assign('BASE_URL_HTTP_ASSETS',BASE_URL_HTTP_ASSETS);
	$smarty->assign('BASE_URL_HTTPS_ASSETS',BASE_URL_HTTPS_ASSETS);

	/*
	$smarty->setPluginsDir[] = ROOT . 'plugins';

	$smarty->setTemplateDir = ROOT.'temp/templates/'; 
	$smarty->setCompileDir = ROOT.'temp/templates_c/'; 
	$smarty->setCacheDir = ROOT.'cache/';
	$smarty->setConfigDir = ROOT.'configs/'; 
	*/
	
	$site_langs_array			= $site_langs->getLangs();
	$project_vars['site_langs']	= $site_langs_array;
	$smarty->assign('site_langs',$site_langs_array);
?>
