<?php
	
	/********************************************
	*	File	: ven_manage_sub_vendor.php				*
	*	Purpose	: Vendor management *
	*	Author	: Leonard Nyirenda						*
	*********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	include(MODEL.'vendors/vendor.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'search':
			$action = "search";
			$objVendor->userId		=	cleanQuery($_REQUEST['vendorName']);
			$objVendor->vendorType	=	cleanQuery($_REQUEST['vendor_type']);
			$objVendor->username	=	cleanQuery($_REQUEST['username']);
			$objVendor->mobNo		=	cleanQuery($_REQUEST['mob_no']);
			$objVendor->email		=	cleanQuery($_REQUEST['email']);
			$objVendor->start_date	=	$_REQUEST['startDate'];
			$objVendor->end_date	=	$_REQUEST['endDate'];
			$objVendor->status		=	cleanQuery($_REQUEST['status']);
			
			//seting pagenation paramiters
			$selfUrl = BASE_URL.'adm_manage_ven.php';
			$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
			$pagingUrl.= '&vendor_type='.$_REQUEST['vendor_type'];
			$pagingUrl.= '&username='.$_REQUEST['username'];
			$pagingUrl.= '&mob_no='.$_REQUEST['mob_no']; 
			$pagingUrl.= '&email='.$_REQUEST['email']; 
			$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
			$pagingUrl.= '&endDate='.$_REQUEST['endDate']; 
			$pagingUrl.= '&status='.$_REQUEST['status']; 
			$pagingUrl.= '&action='.$action;
			SmartyPaginate::setUrl($pagingUrl);
			
			//assign variables to smarty
			$arrVendorList = $objVendor->getAllSubVendorDetails();
			$smarty->assign("arrVendorList",$arrVendorList);
			$smarty->assign("vendorName",$_REQUEST['vendorName']);
			$smarty->assign("vendor_type",$_REQUEST['vendor_type']);
			$smarty->assign("username2",$_REQUEST['username']);
			$smarty->assign("mob_no",$_REQUEST['mob_no']);
			$smarty->assign("email",$_REQUEST['email']);
			$smarty->assign("startDate",$_REQUEST['startDate']);
			$smarty->assign("endDate",$_REQUEST['endDate']);
			$smarty->assign("status",$_REQUEST['status']);
		break;
		case'edit':
			$selfUrl = BASE_URL.'ven_manage_sub_vendor.php';
			$pagingUrl = $selfUrl;
			SmartyPaginate::setUrl($pagingUrl);
			
			$objVendor->userId		=	$_REQUEST['vendor_id'];
			$arrVendorDetail	=	$objVendor->getVendorDetail();
			$smarty->assign("arrVendorDetail",$arrVendorDetail);
			$smarty->assign('accType',$arrVendorDetail['vendor_type']);
			$smarty->assign("id",$_REQUEST['vendor_id']);
			
			$arrCreditReminder= $objVendor->getVendorCreditBalance();
			$smarty->assign('arrCreditReminder',$arrCreditReminder);
			
			$smarty->assign("arrLocation",getArray('locations','id','location',' order by location '));
	$smarty->assign("arrOrganisationCat",getArray('organisations','id','organisation',' order by organisation ', $_SESSION['lang']));
	$smarty->assign("arrPeopleRanges",getArray('people_ranges','id','people_range'));
			$action = "update";
		break;
		
		
		case'update':
			$objUser->userId			= cleanQuery($_REQUEST['userId']);
			$objUser->parentVenId		= $_SESSION['user']['id'];
			$objUser->name				= cleanQuery(trim($_REQUEST['name']));
			$objUser->username			= cleanQuery(trim($_REQUEST['username']));
			$objUser->email				= cleanQuery(trim($_REQUEST['email']));
			$objUser->url				= cleanQuery($_REQUEST['url']);
			$objUser->mobNo				= cleanQuery($_REQUEST['mob_no']);
			$objUser->phone				= cleanQuery($_REQUEST['phone']);
			$objUser->organisation		= cleanQuery($_REQUEST['organisation']);
			$objUser->organisationCatId	= cleanQuery($_REQUEST['organisationCat']);
			$objUser->postalAddress		= cleanQuery($_REQUEST['postalAddress']);
			$objUser->physicalAddress	= cleanQuery($_REQUEST['physicalAddress']);
			$objUser->locationId		= cleanQuery($_REQUEST['location']);
			$objUser->peopleRangeId		= cleanQuery($_REQUEST['peopleRange']);
			$objUser->active		= 	cleanQuery($_REQUEST['active']);
			$objUser->delete		= 	cleanQuery($_REQUEST['deleted']);
			$objUser->confirmed		= 	cleanQuery($_REQUEST['comfirmed']);
			$objUser->accessIncoming = 	cleanQuery($_REQUEST['access_incoming']);
			$objUser->accessRegistrations	= 	cleanQuery($_REQUEST['access_registrations']);
			
			$pass					= 	cleanQuery($_REQUEST['password']);
			
			if($pass != ''){
				$objUser->password	=	md5($pass);
			}else{
				$objUser->password	=	$_REQUEST['oldPass'];
			}
			
			if($_REQUEST['SMSReminder'] =='1'){
				$objUser->sms_reminder		    = 1;
			}else{
				$objUser->sms_reminder		    = 0;
			}
			
			if($_REQUEST['EmailReminder'] =='1'){
				$objUser->email_reminder		    = 1;
			}else{
				$objUser->email_reminder		    = 0;
			}
			
			if($_REQUEST['Credit_Level'] !=''){
				$objUser->credit_level		    = $_REQUEST['Credit_Level'];
			}else{
				$objUser->credit_level		    = 25;
			}

			if($objUser->admUpdateVenProfile()){
				$objUser->email_reminder;
				$objUser->VenderSMSEmailReminder();
				$msg = PROFILE_UPDATED;
				$action = "search";
				$arrVendorList = $objVendor->getAllSubVendorDetails();
				$smarty->assign("arrVendorList",$arrVendorList);
			}else{
				
			}
			
		break;
		
		
		default:
			$action = "search";
			$arrVendorList = $objVendor->getAllSubVendorDetails();
			$smarty->assign("arrVendorList",$arrVendorList);
		break;
	}
	
	$arrVendors=$objVendor->getSubVendorList();
	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("action",$action); 
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_sub_vendor.tpl");
	SmartyPaginate::disconnect();
	include 'footer.php';

?>
