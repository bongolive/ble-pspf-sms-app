<?php
	
	/********************************************
	*	File	: ven_incoming_log_report.php		*
	*	Purpose	: Vendor Incoming SMS Log Report	*
	*	Author	: Taha Jiwaji							*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'incoming.class.php');
	require_once(MODEL.'sms/sms.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$selfUrl = BASE_URL.'ven_incoming_reg_log_report.php';

	$objUser = new user();
	$objIncoming  = new incoming();
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'dnd':
			
			$objIncoming-> userId		= $_SESSION['user']['id']; 
			$objIncoming-> mob_no		= $_REQUEST['mob_no'];
			$objIncoming-> startDate	= getDateFormat($_REQUEST['startDate'],'Y-m-d');
			$objIncoming-> endDate		= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
			
			$dirPath = 'download_file/';
			$fileName = 'delive_report_'.time().'.xls';
			
			
			$countData = $objIncoming->getCountIncomingRegDataReport(); 
			//$countData = $objSms->getCountDataReport(); 
			
			$handle= fopen($dirPath.$fileName,'a+');
			
			if($handle){
				
				$header = "User\t Mobile \t Received Time \t Message \t Outbound Response \t \n";	
				
				fwrite($handle,$header);
				
				$rec = 1000;
				$offset= 0;
				$order   = array("\r\n", "\n", "\r");

				while($offset < $countData){
				
					$arrData = $objIncoming->dowloadIncomingRegSmsLog($offset,$rec);
					if($arrData){

						foreach($arrData as $value){
							
							$contents='';
							$contents.=(isset($value['username'])?$value['username']."\t":"\t");
							$contents.=(isset($value['phone'])?$value['phone']."\t":"\t");
							$contents.=(isset($value['received'])?$value['received']."\t":"\t");
							$contents.=(isset($value['message'])?str_replace("\n", "",$value['message'])."\t":"\t");
							$contents.=(isset($value['outbound_response'])?str_replace("\n", "",$value['outbound_response'])."\t":"\t");
							$contents.= "\n"; 
							fwrite($handle,$contents);
							
						}
						
					
						$offset = $offset+$rec;
					}	
				}

				
				
				fclose($handle);
				
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Length: " . filesize($dirPath.$fileName));
				header('Content-type: application/ms-excel');
				header("Content-Disposition: attachment; filename=$fileName");
				 
				ob_clean();
				flush();
				readfile($dirPath.$fileName);
				unlink($dirPath.$fileName);
				exit;	
				
				
				
			}else{
			
				echo 'file not created';
			}

			
		break;
		
	}
	$arrSmsSender 		= getArray('mst_vendors','id','username, name',' where active=1 and deleted=0 order by username');
	

	$smarty->assign("msg",$msg);
	$smarty->assign("arrSmsSender",$arrVendors);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	$smarty->assign("mob_no", $_REQUEST['mob_no']);
	$smarty->assign("broadcaster", $_REQUEST['broadcaster']);
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->display("vendor/ven_incoming_reg_log_report.tpl");
	include 'footer.php';

?>
