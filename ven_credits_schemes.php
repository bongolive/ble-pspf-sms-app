<?php
	
	/*************************************************
	*	File	: ven_credits_schemes.php			 *
	*	Purpose	: Vendor crdit  management for vendor *
	*	Author	: Leonard Nyirenda							 *
	*************************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	include(MODEL.'vendors/vendor.class.php');
	require_once(MODEL.'credit/credit.class.php');
	include(CLASSES.'SmartyPaginate.class.php');


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$objCredit = new credit();
	$objCredit->parentVenId = $_SESSION['user']['id'];
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	
	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	$resellerDetail = $objUser->getResellerDetail();
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();

	switch($action){
	
		case'add':

			$objCredit->startCredit = $_REQUEST['startCredit'];
			$objCredit->endCredit	= $_REQUEST['endCredit'];
			$objCredit->creditRate	= $_REQUEST['creditRate'];
			$objCredit->parentVenId = $_SESSION['user']['id'];
			
			if($objCredit->addCreditScheme()){
				$msg = "SMS scheme added";
			}else{
				$msg = "SMS scheme not added !!!";
				$smarty->assign('startCredit',$_REQUEST['startCredit']);
				$smarty->assign('endCredit',$_REQUEST['endCredit']);
				$smarty->assign('creditRate',$_REQUEST['creditRate']);
			}
			
		break;
		case'edit':
			$objCredit->crdtSchmId = $_REQUEST['id'];
			$arrCrdtSchmDetails = $objCredit->getCrdtSchmDetails();
			$smarty->assign('startCredit',$arrCrdtSchmDetails['start_range']);
			$smarty->assign('endCredit',$arrCrdtSchmDetails['end_range']);
			$smarty->assign('creditRate',$arrCrdtSchmDetails['rate']);
			$smarty->assign('id',$arrCrdtSchmDetails['id']);
			$action = 'update';
			
		break;
		case'update':
			$objCredit->crdtSchmId  = $_REQUEST['id'];
			$objCredit->startCredit = $_REQUEST['startCredit'];
			$objCredit->endCredit	= $_REQUEST['endCredit'];
			$objCredit->creditRate	= $_REQUEST['creditRate'];

			if($objCredit->updateCreditScheme()){
				$msg = "Credits scheme updated ";
			}else{
				$msg = "Credits scheme not updated !!!";
			}

			$action = "add";
			
		break;
		/*
		case'delete':
			
			$objCredit->crdtSchmId = $_REQUEST['id'];
			if($objCredit->deleteCreditScheme()){
				$msg = "Credits scheme deleted";
			
			}else{
				$msg = "Credits scheme not deleted !!!";
			}
			
			$action = 'add';
			
		break;
		*/
		default:
			$action = "add";
	
	
	}

	$arrCreditSchemes =  $objCredit->getResellerCrditSchemes();

	$smarty->assign("msg",$msg);
	$smarty->assign("selfUrl",BASE_URL.'ven_credits_schemes.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrCreditSchemes",$arrCreditSchemes);
	$smarty->assign("arrCrdtSchmDetails",$arrCrdtSchmDetails);
	$smarty->display("vendor/ven_credits_schemes.tpl");
	include 'footer.php';

?>