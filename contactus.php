<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	require_once(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';
	
	
	$errorFlag = 0;
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='send'){
		if($_SESSION["security_code"] != $_REQUEST['security_code']){
		
			$msgSecCode = CONTACT_US_MSG_1;
			$errorFlag = 1;
		}
		if($errorFlag!=1){

			$name			= $_REQUEST['name'];
			$email			= $_REQUEST['email'];
			$mob_no			= $_REQUEST['mob_no'];
			$business_name	= $_REQUEST['business_name'];
			$subject		= $_REQUEST['subject'];
			$message		= $_REQUEST['message'];

			try{
				if($project_vars["smtp_auth"]===true){
					$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
					->setUsername($project_vars["smtp_username"])
					->setPassword($project_vars["smtp_password"]);
				}else{
				
					$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
				}

				$mailer = Swift_Mailer::newInstance($transport);

				//Create a message
				$smarty->assign('cname',$name);
				$smarty->assign('cemail',$email);
				$smarty->assign('cmob_no',$mob_no);
				$smarty->assign('cbusiness_name',$business_name);
				$smarty->assign('cmessageBody',$message);
				$body = $smarty->fetch('mail_template/mail_contact_us.tpl');

				$message = Swift_Message::newInstance($subject)
				->setFrom(array($project_vars["smtp_username"] => 'admin'))
				->setTo(array($project_vars['system_admin_email']=>$project_vars['system_admin_name']))
				->setReplyTo($email)
				->setBody($body);
				$message->setContentType("text/html");

				//Send the message
				$result = $mailer->send($message);

				if($result){
				
					$msg = CONTACT_US_MSG_2;
				}
			} catch (Exception $e){
					
				$msg = CONTACT_US_MSG_2.$e->getMessage();
				$errorFlag = 3;
			}


		}else{
			
			$smarty->assign("name",$name);
			$smarty->assign("email",$email);
			$smarty->assign("mob_no",$mob_no);
			$smarty->assign("business_name",$business_name);
			$smarty->assign("subject",$subject);
			$smarty->assign("message",$message);
			
		
		}
	}

	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("msg",$msg);
	$smarty->assign("msgSecCode",$msgSecCode);
	$smarty->assign("action",$action);
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('contactus.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/contactus.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/contactus.tpl');
		}
		else
		{
			$smarty->display('contactus.tpl');
		}
	}

	include 'footer.php';
?>