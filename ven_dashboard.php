<?php
/****************************************************************************************************************
*	File : ven_smspush_dashboard.php
*	Purpose: landing page for vendors
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(MODEL.'sms/sms.class.php');
	require_once(MODEL.'credit/credit.class.php');
	require_once(MODEL.'senderid/senderid.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	include('ven_header.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//updating credit only for parent id
	if($_SESSION['user']['parent_ven_id']==''){
		(int) $remoteCredit = getRemoteCreditBalance();
//		var_dump( $remoteCredit );exit;
		(int) $resCredit = getTotalCreditOfAllVendors();
		$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
	} 
	
	// required connect
 	SmartyPaginate::connect();
	// set items per page
	SmartyPaginate::setLimit($project_vars['row_per_page']);
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	$arrAddressbook = $objAddressbook->getAddressbookList();
	
	$objSms = new sms();
	$objSms->userId = $_SESSION['user']['id'];
	
	$objCredit		= new credit();
	$objCredit->parentVenId= $_SESSION['user']['id'];
	
	$objSenderid	= new senderid();
	$objSenderid->parentVenId= $_SESSION['user']['id'];
	
	$smarty->assign('creditStat',$objCredit->requestedResellerCreditStatus());
	$smarty->assign('senderidStat',$objSenderid->requestedResellerSenderidStatus());
	
	//$string_x	=	"{ x: ";
	//$string_y	=	", y: ";
	//$string_end	=	" }, ";
	$commer=",";
	$newLine="\r\n";
	$expression='';
	
	if(isset($_REQUEST['period']) && $_REQUEST['period']=="year"){
		$arrReport=$objSms->getCurrentYearSMSReport();
		$smarty->assign('period','year');
		
		while($row=mysql_fetch_assoc($arrReport)){
			$string = $row['value_x'].$commer.$row['value_y'].$newLine;
			$expression	=	$expression.$string;
		}
	}else if(isset($_REQUEST['period']) && $_REQUEST['period']=="month"){
		$arrReport=$objSms->getCurrentMonthSMSReport();
		$smarty->assign('period','month');
		while($row=mysql_fetch_assoc($arrReport)){
			$string = substr($row['value_x'],0,3)." ".$row['value_z'].$commer.$row['value_y'].$newLine;
			$expression	=	$expression.$string;
		}
	}else if(isset($_REQUEST['period']) && $_REQUEST['period']=="week"){
		$arrReport=$objSms->getCurrentWeekSMSReport();
		$smarty->assign('period','week');
		while($row=mysql_fetch_assoc($arrReport)){
			$string = substr($row['value_x'],0,3)." ".$row['value_z'].$commer.$row['value_y'].$newLine;
			$expression	=	$expression.$string;
		}
	}
	
	$arrAddressBookList=array();
	$arrSMS = $objSms->getLastSentAddressbookSMS();
	$arrAddr = explode(',', $arrSMS['addressbook_id']);
	$objSms->sent_time=$arrSMS['sent_time'];
	
	foreach($arrAddr as $value){
		$objSms->addressbookId = $value;
		$result=$objSms->getLastBroadcast();
		array_push($arrAddressBookList,$result);
	}
	
	$smarty->assign('arrSms',$arrAddressBookList);
	$smarty->assign('expression',$expression);
	$smarty->display("vendor/ven_dashboard.tpl");

	include 'footer.php';
?>