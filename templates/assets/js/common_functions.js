/*
	File : common_function.js
	Purpose: common functions are defined here
	Author : Akhilesh
	
	Updated 5thOct2012 by Leonard
	1. getCreditShcemeDetails() function modified so that it enable submit button only after ajax return credit scheme
*/

/* global variable setting */

// mobile no length
var mob_no_length = 9;

// function for email validation check
function emailValidation(){
	var pattern =/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/;
	var email = $("#email").val();
	if(email ==""){
		$("#emailDiv").html(msg_01);
		return 0;
	}else if(!email.match(pattern)){

		//$("#emailDiv").html('<img src="'+imagePath+'error_icon.gif" >');
		$("#emailDiv").html(msg_01);
		return 0;

	}else{
		$("#emailDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}
}

// function for password validtion check
function passValidation(){

	var re = /^[a-z0-9]\w{4,}[a-z0-9]$/;
	//var pattern =/^[_a-zA-Z0-9-]{6,}$/;
	var pattern =/^[_a-zA-Z0-9-~`!@#$%^&*_:;,<.>?\/\'\"\(\)\{\}\[\]]{6,}$/;
	var pass = $("#password").val();
	if(pass == ""){
		$("#passwordDiv").html(msg_02);
		return 0;
//	}else if (!re.test(pass)) {
	}else if (!pass.match(pattern)) {

		$("#passwordDiv").html(msg_03);
		return 0;
	}else{
		$("#passwordDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;

	}
}

// username or login name validation check(for broadcasters)
function usernameValidation(){
	var pattern =/^[_a-zA-Z0-9-]{6,}$/;
	var username = $("#username").val();
	if(username == ""){
		$("#usernameDiv").html(msg_04);
		return 0;
//	}else if (!re.test(pass)) {
	}else if (!username.match(pattern)) {

		$("#usernameDiv").html(msg_05);
		return 0;
	}else{
		$("#usernameDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;

	}
}


// mobile number validation
function mobileValidatio2n(){

	var pattern =/^[0-9]*$/;

	var mob =$("#mob_no").val();

	if (mob.charAt(0)=='0'){
		$("#mobDiv").html(msg_06);
		return 0;
	}else if(mob==""){
		$("#mobDiv").html(msg_07);
		return 0;
	}else if (!mob.match(pattern)) {

		$("#mobDiv").html(msg_08);
		return 0;
	}else if(mob.length != mob_no_length){

		$("#mobDiv").html(msg_09_1+mob_no_length+msg_09_2);
		return 0;
	}else{

		$("#mobDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}



}


function mobileValidation(){

	var pattern =/^\+?[0-9]{8,12}$/;

	var mob =$("#mob_no").val();
    if(mob.substr(0,1) == '7' || mob.substr(0,1) == '6'){
		mob_no_length = 9;
	}else if(mob.substr(0,1) == '0'){
       	mob_no_length = 10;
    }else if(mob.substr(0,1) == '+'){
        mob_no_length = 13;
    }else {
      	mob_no_length = 12;
    }
	
	if(mob==""){
		$("#mobDiv").html(msg_07);
		return 0;
	}else if (!mob.match(pattern)) {

		$("#mobDiv").html(msg_07);
		return 0;
	}else if(mob.length != mob_no_length){

		$("#mobDiv").html(msg_09_1+mob_no_length+msg_09_2);
		return 0;
	}else{

		$("#mobDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}



}


// function for ckecking password and cofir password match
function confPassValidation(){

	var  cpass = $("#conf_password").val();
	if($("#password").val()=="" && cpass==""){

		$("#cpasswordDiv").html(msg_10);
		return false;
	}else if($("#password").val()!=cpass){

		$("#cpasswordDiv").html(msg_11);
		return false;
	}else{

		$("#cpasswordDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}

}

// function for phone number validation
function phneValidation(phone){

	if(!numberValidation(phone)){

		$("#phoneDiv").html(msg_12);
		return false;
	}else{
		$("#phoneDiv").html('');
		return true;
	}
}

// this is common function for used data is number or not
function numberValidation(num){

	pattern = /^[0-9]*$/
	if(num=='' || num == null){

		return false;
	}
	if(num.match(pattern)){

		return true;
	}else{

		return false;
	}
}


//subcriber login for validation
function subFormValidation(){
	var error='';
	var step = $("#step").val();

	if(step==1){

		var errorMob = mobileValidation();
		var errorEmail = emailValidation();
		var errorPass = passValidation();
		var errorConfPass = confPassValidation();

		/*
		if(!mobileValidation() || !emailValidation() ||  !passValidation() || !confPassValidation()){

			error=1;

		}
		*/
		if(!errorMob || !errorEmail ||  !errorPass || !errorConfPass){

			error=1;

		}
	}else if(step==2){

		if($("#name").val()==''){

			$("#nameDiv").html(msg_13);
			error=1;
		}else{
			$("#nameDiv").html("");
		}

		if($("#gender").val()==''){

			$("#genderDiv").html(msg_14);
			error=1;
		}else{
			$("#genderDiv").html("");
		}

		if($("#ageRange").val()==''){

			$("#ageDiv").html(msg_15);
			error=1;
		}else{
			$("#ageDiv").html("");
		}

	}else if(step==3){

		if($("#location").val()==''){

			$("#locationDiv").html(msg_16);
			error=1;
		}else{
			$("#locationDiv").html("");
		}
		if($("#termCondition ").attr('checked')==false){

			$("#tncDiv").html(msg_17);
			error=1;
		}else{
			$("#tncDiv").html("");
		}
	}

	if(error!=1){

		document.subRegister.submit();
	}
}

// subcriber edit profile validation
function subEditFormValidation(){

		//var errorMob;
		var name;
		var email;
		var gender;
		var ageRange;
		var locations;

		var error=0;

		if(jQuery.trim($("#name").val())==""){

			$("#nameDiv").html(msg_13);
			name=false;
			error = 1;
		}else{
			$("#nameDiv").html("");
			name=true;

		}

		email = emailValidation($("#email").val());

		if($("#gender").val()==""){

			$("#genderDiv").html(msg_18);
			gender = false;
			error  = 1;
		}else{
			$("#genderDiv").html("");
			gender = true;

		}

		if($("#ageRange").val()==""){

			$("#ageRangeDiv").html(msg_19);
			ageRange = false;
			error  = 1;
		}else{
			$("#ageRangeDiv").html("");
			ageRange = true;

		}

		if($("#location").val()==""){

			$("#locationDiv").html(msg_20);
			locations = false;
			error  = 1;
		}else{
			$("#locationDiv").html("");
			locations = true;

		}




		if(!name || !email || !gender || !ageRange || !locations ){

			error=1;

		}

	if(error!=1){

		document.subProfile.submit();
	}
}

// function to neighborhood of selected city
function getNeighborhood(cityId,neighborhoodId,url){
	$.ajax({
              url: url,
              type: "POST",
              data: "cityId="+cityId+"&nid="+neighborhoodId,
              success: function (response) {

				$("#neighborhood").html(response);
			  }
         });
}

// function to vendor list
function getVendorByUserType(vendor_type,url){
	$.ajax({
              url: url,
              type: "POST",
              data: "vendor_type="+vendor_type+"&action=vendor",
              success: function (response) {

				$("#vendorName").html(response);
			  }
         });
}

// function to cancle sheduled sms
function cancelSms(sid,url){

	$.ajax({

		url: url,
		type:"POST",
		data:"sid="+sid+"&action=cancel",
		success: function(response){

			if(response=='1'){

				window.location.reload();
			}

		}

	});
}

// function for vendor registration form validation
function venFormValidation(){
	var error='';
	var step = $("#step").val();
	if(step == 1){

		var errorUsername = usernameValidation();
		var errorPass = passValidation();
		var errorConfPass = confPassValidation();

		if($("#accType").val()==""){
			$("#accTypeDiv").html(msg_27);
			error=1;
		}else if(!errorUsername || !errorPass || !errorConfPass){
			error=1;
		}
	}else if(step==2){
		var accType = $("#accType").val();
		var termCondition;
		var name;
		var organisation;
		var organisation_cat;
		var errorMob;
		var errorEmail;
		var phone = true;
		var locations;
		var physicalAdd;
		var epm_range;
		var countries;

		if(accType==2){
			if($("#organisation").val()==""){

				$("#organisationDiv").html(msg_21);
				organisation = false;
			}else{
				$("#organisationDiv").html("");
				organisation = true;
			}

			if($("#organisationCat").val()==""){

				$("#organisationCatDiv").html(msg_22);
				organisation_cat = false;
			}else{
				$("#organisationCatDiv").html("");
				organisation_cat = true;
			}

			if($("#phone").val()!=""){
				if(!phneValidation($("#phone").val())){
					phone = false;
				}

			}else{
				$("#phoneDiv").html("");

			}


			if($("#peopleRange").val()==""){
				$("#peopleRangeDiv").html(msg_23)
				epm_range = false;

			}else{
				$("#peopleRangeDiv").html("");
				epm_range = true;
			}
			if($("#physicalAddress").val()==""){
				$("#physicalAddressDiv").html(msg_24)
				physicalAdd = false;

			}else{
				$("#physicalAddressDiv").html("");
				physicalAdd = true;
			}

		}

		if($("#name").val()==""){

			$("#nameDiv").html(msg_25);
			name=false;
		}else{
			$("#nameDiv").html("");
			name=true;
		}
		
		if($("#location").val()==""){

			$("#locationDiv").html(msg_20);
			locations=false;
		}else{
			$("#locationDiv").html("");
			locations=true;
		}
		
		
		
		if(validateCountryCode($("#mob_no").val())){
			$("#countryDiv").html("");
			countries=true;
		}else{
			$("#countryDiv").html("Mobile number Entered above does not belong to the country selected");
			countries=false;
		}
		

		errorMob = mobileValidation();
		errorEmail = emailValidation();

		if($("#termCondition ").attr('checked')==false){

			$("#tncDiv").html(msg_17);
			termCondition=false;
		}else{
			$("#tncDiv").html("");
			termCondition=true;
		}
			
			
			
		if(accType==1){
			if(!errorEmail  || !errorMob || !termCondition || !name || !locations || !countries){
				error=1;
			}
		}else if(accType==2){

			if(!organisation || !organisation_cat || !errorEmail  || !errorMob || !termCondition || !name || !phone || !locations || !epm_range || !physicalAdd ){
				error=1;
			}
		}

	}

	if(error!=1){

		document.venRegister.submit();
	}
}

// function to check length of input message
function checkMessageLength(){

	var value = $("#textMessage").val();
	var val = $("#textMessage").val().length;

	$("#charCount").html(val);
	var counter = 1+parseInt((val-1)/160,10);
	$("#messageCount").html(msg_28+counter);

}
//function to display and hide advance option at message sending
function showHideAdvOption(){

	if(document.getElementById("advOptionDiv").style.display=='none'){
		$("#advOptionDiv").show();

		//$("#senderid").attr("disabled",false);
		$("#scheduleDate").attr("disabled",false);
		$("#advOptionTd").html('- '+msg_26);
	}else{
		$("#advOptionTd").html('+ '+msg_26);
		$("#advOptionDiv").hide();
		//$("#senderid").attr("disabled",true);
		$("#scheduleDate").attr("disabled",true);
	}
}

// function to validate mobile numer at time of login
function loginMobValidation(mob){

	var pattern =/^[0-9]*$/;
	if(!mob.match(pattern)) {

		$("#mobDiv").html(msg_27);
		return false;
	}else{
		$("#mobDiv").html('');
	}
}

//function to used to checked all checkboxes
function checkAll(all,container){

	if($("#"+all).attr('checked')== true){

		 $( "#"+container+" :checkbox").attr('checked',  true);

	}else{

		 $( "#"+container+" :checkbox").attr('checked',false);
	}

}

// function for opening terms & condition in another window
function openTermToRead(url){

	window.open(url,
		'Bongolive',
		'width=600,	height=600,directories=no,location=no,top=50,menubar=no, resizable=no,scrollbars=1,status=no, 	toolbar=no'
	);
}

function keepCheckAddbook(bookId){
	$("#"+bookId).attr("checked","checked");
}

// funtion used to check all checkboxes of addressbook or groups
function checkAllGroup(all,container,leavBook){

	if($("#"+all).attr('checked')== true){

		$("#"+container+" :checkbox").attr('checked',true);

	}else{

		$("#"+container+" :checkbox").attr('checked',false);
		$("#"+leavBook).attr("checked","checked");
	}

}

// funtion for the purchase price and total
function getCreditShcemeDetails(crdamt,url){

		if(!numberValidation(crdamt)){
			$("#creditRequest").val('');
			$("#creditRate").val('');
			$("#creditTotalCost").val('');
			/*$("#sub_display").html('');*/

		}else{
			document.getElementById("submit").disabled = true;
			
			$.ajax({
				  url: url,
				  type: "POST",
				  data: "crdamt="+crdamt+"&action=getSchm",
				  dataType: "json",
				  success: function (response) {

						$("#creditRate").val(response.rate);

						$("#creditScheme option").each(function (){

							if($(this).val()==response.id){
								$(this).attr("selected","selected");
							}
						});

						$("#creditTotalCost").val(parseInt(crdamt) * parseInt(response.rate));
						//$("#submit").val("ready");
						document.getElementById("submit").disabled = false;
						/*$("#sub_display").html('<input type="submit" name="submit" id="submit" value="Submit" class="editbtn">');	*/
				  }
			});
		}


}

// funtion for the purchase price and total in admin
function getCreditShcemeDetailsAdm(crdamt,pack,url){
		if(!numberValidation(crdamt) || crdamt=='0' || !numberValidation(pack) || pack=='0'){
			if(!numberValidation(crdamt) || crdamt=='0') $("#crditRequested").val('');
			//if( !numberValidation(pack) || pack=='0') $("#creditRate").val('');
			if( !numberValidation(pack) || pack=='0') $("#crdtSchmId").val('');

			$("#creditTotalCostAdm").html('');
			$("#sub_display").html('');

		}else{

			$("#creditTotalCostAdm").html(parseInt(crdamt) * parseInt(pack));
			/*$.ajax({
				  url: url,
				  type: "POST",
				  data: "pack="+pack+"&action=getSchm",
				  dataType: "json",
				  success: function (response) {
						$("#creditTotalCostAdm").html(parseInt(crdamt) * parseInt(response.rate));
				  }
			});*/
		}


}

function validateRequestForm(){

	var ansver = confirm('Submit?');
	if(!ansver)return;
	var creditRequest = $("#creditRequest").val();
	var creditScheme = $("#creditScheme").val();console.log("tyy");
		if(creditRequest=='' || $("#creditScheme").val()==''){
			$("#creditRequest").focus();
		alert("Input credit request!");
			return false;
		}else {
			document.smsCredit.submit();
			return true;
		}

}


function search_menu(){
		if($("#search_select").is(":hidden")){
			$("#search_select").show();
		}
		else {
			$("#search_select").hide();
		}
}

function search_hide(){
		$("#search_select").hide();
}


/**
 * jQuery SMS Counter
 * @author Louy Alakkad <me@l0uy.com>
 * @website http://l0uy.com/
 * @version 1.1
 */
function is_ascii(Str){for(i=0;i<Str.length;i++){charCode=Str.charCodeAt(i);if(charCode>127){return false
	;}}return true;}

/*(function($){$.fn.smsCounter=function(selector){doCount=function(){var text=$(this).val();if(is_ascii
 (text))limit=160;else limit=70;if(text.length>limit){if(is_ascii(text))limit=limit-7;else limit=limit-3
 ;}diff=text.length%limit;left=limit-diff;count=((text.length-diff)/limit)+1;if(diff==0){left=0;count
 =count-1;}$(selector).html('('+count+') '+left);};this.keyup(doCount);this.keyup();}})(jQuery)*/

smsCounter=function(){
	var selector = $("#charCount");
	var txt = $("#textMessage");
	var text=$(txt).val();
	if(is_ascii(text)){
		limit=160;
//$('#m_text').
		$('input:radio[name=m_type][value=m_text]').attr('checked', true);
		console.log(limit);
	}else {
		limit=70;
		$('input:radio[name=m_type][value=m_unicode]').attr('checked', true);
		console.log(limit);
	}if(text.length>limit){if(is_ascii(text
		))limit=limit-7;else limit=limit-3;}diff=text.length%limit;left=limit-diff;count=((text.length-diff)/limit)+1;if(diff==0){left=0;count=count-1;}$(selector).html('('+count+') '+left);};


$('document').ready(function(){
// $("#textMessage").keyup(function(){
//
//	var value = $("#textMessage").val();
//	var val = $("#textMessage").val().length;
//
//	$("#charCount").html(val);
//	var counter = 1+parseInt((val-1)/160,10);
//	$("#messageCount").html(msg_28+counter);
//
//});

	$("#textMessage").bind("keyup change blur paste", function(e) {
		//console.log(e);
		var navKeys = [33,34,35,36,37,38,39,40];
		switch(e.type) {
			case 'keyup':
				// Skip navigational key presses
				if ($.inArray(e.which, navKeys) < 0) {smsCounter(); }
				break;
			case 'paste':
				//alert($(this).val());
				// Wait a few miliseconds if a paste event
				setTimeout(function(){ smsCounter(); }, (e.type === 'paste' ? 5 : 0));
				break;
			default:
				smsCounter;
				break;
		}


		//$(this).smsCounter('#count');
		//$(this).smsCounter();
	});

/*******coupone page*******/
$('#prim_key').change(function(){
    $("#count_user_subscriber").empty();
    $("#count_user_subscriber").css('color','black');
	var val = $(this).val();
	$('#prim_key_id').val(val);
    if(val==-1)
    {$("#count_user_subscriber").empty();$("#input_count_user").val(-1);return;}
	$.post('adm_coupone_ajax.php',{count:val},function(data){
	$("#count_user_subscriber").html(data);
	$("#input_count_user").val(data);

	});

});/*
$('#coupone_broadcater').change(function(){
	var id = $(this).val();
	if(id == -1 || id == '')
	{
		$('#broadcaster_sms_count').html("");
	 	return;
	}

	$.post('adm_coupone_ajax.php',{id:id},function(data){
	if(data){
		$('#broadcaster_sms_count').html((data.credit_balance==null)?0:data.credit_balance);
		$('#coupone_broadcater_credit').val((data.credit_balance==null)?0:data.credit_balance);
		$('#prim_key').empty();
		$('#prim_key').append(data.prim);
		$('#senderid').empty();
		if(data.options!='none')
		{
			$('#senderid').append(data.options);
		}
	}
	else{$('#broadcaster_sms_count').html(0);$('#senderid').empty();}
	},"json");
*
});*/


});


