$(document).ready(function() {
		$( "#incomig_export_date_start" ).datepicker();
		$( "#incomig_export_date_finish" ).datepicker();
		$( "#incomig_export_date_start" ).click(function(){$('#errmsg').html('');});
		$( "#incomig_export_date_finish" ).click(function(){$('#errmsg').html('');});
		$('#gen_button').click(function(){
			var d1 = $( "#incomig_export_date_start" ).val();
			var d2 = $( "#incomig_export_date_finish" ).val()
			
			if( d1=='' || d2=='')
			{
				$('#errmsg').html('Error! No date chosen.');
				return false;
			}
			if(d1==d2)
			{
				$('#errmsg').html('Error! date .');
				return false;
			}
			
			var arr1 = d1.split("/");
			var arr2 = d2.split("/");
			if( (arr1[2]>arr2[2]) )
			{console.log('y');
				$('#errmsg').html('The finish date can not be earlier then a start date.');
				return false;				
			}
			if( (arr1[0]>arr2[0])  )
			{	console.log('m');
				$('#errmsg').html('The finish date can not be earlier then a start date.');
				return false;				
			}
			if( arr1[1]>arr2[1] && arr1[0] > arr2[0] )
			{
				console.log('day')
				$('#errmsg').html('The finish date can not be earlier then a start date.');
				return false;				
			}
		});
		
		$( "#incomig_export_date_start" ).val('');
		$( "#incomig_export_date_finish" ).val('');
		});//end document ready
