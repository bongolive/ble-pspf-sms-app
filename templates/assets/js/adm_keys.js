$(document).ready(function(){
$('#select_mst_vendor :first').attr("selected", "selected");
$('#select_mst_vendor').change(function(){
	var id = $(this).val();
	if( id == -1 || id== null )
	{
		$('#list_prim_keywords').empty();
		$('#list_second_keywords').empty();
		return;
	}
	$.post('mst_vendor_ajax.php',{action:'change',id:id},function(data){
			if(data!='')
			{
				$('#list_prim_keywords').empty();
				$('#list_prim_keywords').html(data);
			}
			else
			{
				$('#list_prim_keywords').empty();
			}
		});
	});/*
$('#list_prim_keywords').change(function(){
	var id = $(this).val();
	var obj = $('#list_prim_keywords :selected');
	id = $(obj).val();
	if( id == -1 || id=='')
	{
		$('#list_second_keywords').empty();
		return;
	}
		$.post('mst_vendor_ajax.php',{action:'changeprim',id:id},function(data){
			if(data!='')
			{
				$('#list_second_keywords').empty();
				$('#list_second_keywords').html(data);
			}
			else
			{
				$('#list_second_keywords').empty();
			}
		});

	});	*/

$('#new_prim_keywords').click(function(){
var id = $('#select_mst_vendor :selected').val();
if(id == -1  || id =="" )
	return;
var text = prompt("Enter a new name for primary keywords");
if( text == null)
	return;
if(text.length>50)
{
	alert('Your keyword should be less than 50 characters');return;
}
$.post('mst_vendor_ajax.php',{action:'addprim',prim_text:text,vendor:id},function(data){
		if(data!='invalid')
		{
			var opt = $('#list_prim_keywords :first');
			if($(opt).val() == -1)
			{
				$(opt).remove();
			}
		$('#list_prim_keywords').append('<option value="'+data+'">'+text+'</option>');
		}
		else
		{
			alert(' Such wordkey already exists.');
			return;
		}
	});//ajax add prim

});//new prim


$('#new_second_keywords').click(function(){
	var obj = $('#list_prim_keywords :selected');
	var id = $(obj).val();
	if(!id || id == -1 )return;
	var text = prompt("Enter a new name for a secondary keyword");
		if( text == '' || !text )
			return;
	if(text.length>50)
	{
		alert('Your keyword should be less than 50 characters');return;
	}
	$.post('mst_vendor_ajax.php',{action:'addsecond',second_text:text,prim:id},function(data){

					if(data!="invalid")
					{
						var opt = $('#list_second_keywords :first');
							if($(opt).val() == -1)
							{
							$(opt).remove();
							}


						$('#list_second_keywords').append('<option value="'+data+'">'+text+'</option>');
					}
					else
					{
						alert(' Such wordkey already exists.');
						return;
					}
		});//ajax add second
});//end new second


$('#list_prim_keywords').change(function(){
	var id = $(this).val();
	var obj = $('#list_prim_keywords :selected');
	id = $(obj).val();
	if( id == -1 || id=='')
	{
		$('#list_second_keywords').empty();
		return;
	}
	$.post('mst_vendor_ajax.php',{action:'changeprim',id:id},function(data){
			if(data!='')
			{
				$('#list_second_keywords').empty();
				$('#list_second_keywords').html(data);
			}
			else
			{
				$('#list_second_keywords').empty();
			}
		});
	});


$('#edit_prim_keywords').click(function(){
	var val = $('#list_prim_keywords :selected').val();
	if( val == -1 || !val )return;
	var text = $('#list_prim_keywords :selected').text();
	var n = prompt("Enter new name", text);
	if( n == null || n == text)
		return;
	if(text.length>50)
	{
		alert('Your keyword should be less than 50 characters');return;
	}
	$.post('mst_vendor_ajax.php',{action:'editprim',prim_text:n,id:val},function(data){
		if(data == 'invalid')
		{
			alert(' Such wordkey already exists.');
			return;
		}
		if(data==true)
		{
			$('#list_prim_keywords [value="'+val+'"]').text(n);
		}
		});

});//end edit


$('#remove_second_keywords').click(function(){
	var obj = $('#list_second_keywords :selected');
	var id = $(obj).val();
	if(!id || id == -1 )return;
	var text = $(obj).text();
	var answer = confirm ("Do you want to remove "+text+" ?");
	if (answer)
	{
		$.post('mst_vendor_ajax.php',{action:'removsecond',id:id},function(data){
		if(data==true)
		{
			$(obj).remove();
		}
		});
	}
});

$('#remove_prim_keywords').click(function(){
	var obj = $('#list_prim_keywords :selected');
	var id = $(obj).val();
	if(!id || id == -1 )return;
	var text = $(obj).text();
	var answer = confirm ("Do you want to remove "+text+"?");
	if (answer)
	{
		$.post('mst_vendor_ajax.php',{action:'removprim',id:id},function(data){
		if(data==true)
		{
			$(obj).remove();
		}
		if(data=='invalid')
		{
			alert('A primary keyword is connected with a secondary keyword and can not be deleted');
		}
		});
	}
});//end remove

$('#edit_second_keywords').click(function(){
	var obj = $('#list_second_keywords :selected');
	var id = $(obj).val();
	if(id==-1 || !id ) return;

	var text = $('#list_second_keywords :selected').text();
	var n = prompt("Enter new name", text);

	if( n == null || n == text)
		return;
	if(text.length>50)
	{
		alert('Your keyword should be less than 50 characters');return;
	}
	$.post('mst_vendor_ajax.php',{action:'editsecond',second_text:n,id:id},function(data){
		if(data == 'invalid')
		{
			alert(' Such wordkey already exists.');
			return;
		}
		if(data==true)
		{
			$(obj).text(n);
		}
		});

});


});//end document ready

