/*
	File : common_function.js
	Purpose: common functions are defined here
	Author : Akhilesh
*/ 

/* global variable setting */

// mobile no length
var mob_no_length = 9; 





// function for email validation check
function emailValidation(){
	
	var pattern =/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/;
	var email = $("#email").val();
	if(email ==""){
		$("#emailDiv").html("Enter valid email address");
		return 0;
	}else if(!email.match(pattern)){
	
		//$("#emailDiv").html('<img src="'+imagePath+'error_icon.gif" >');
		$("#emailDiv").html('Enter valid email address');
		return 0;
		
	}else{
		$("#emailDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;	
	}
}

// function for password validtion check
function passValidation(){
	
	var re = /^[a-z0-9]\w{4,}[a-z0-9]$/;
	//var pattern =/^[_a-zA-Z0-9-]{6,}$/;
	var pattern =/^[_a-zA-Z0-9-~`!@#$%^&*_:;,<.>?\/\'\"\(\)\{\}\[\]]{6,}$/;
	var pass = $("#password").val();
	if(pass == ""){
		$("#passwordDiv").html('Enter password');
		return 0;
//	}else if (!re.test(pass)) { 
	}else if (!pass.match(pattern)) { 
		
		$("#passwordDiv").html('Password must be at least 6 characters');
		return 0;
	}else{
		$("#passwordDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
		
	}
}

// username or login name validation check(for broadcasters) 
function usernameValidation(){
	var pattern =/^[_a-zA-Z0-9-]{6,}$/;
	var username = $("#username").val();
	if(username == ""){
		$("#usernameDiv").html('Enter username');
		return 0;
//	}else if (!re.test(pass)) { 
	}else if (!username.match(pattern)) { 
		
		$("#usernameDiv").html('Username must be at least 6 characters without any special symbols');
		return 0;
	}else{
		$("#usernameDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
		
	}
}


// mobile number validation
function mobileValidation(){
	
	var pattern =/^[0-9]*$/;
	
	var mob =$("#mob_no").val(); 
	/*
	if (mob.charAt(0)=='0'){
		$("#mobDiv").html('Mobile Number should not start with 0');
		return 0;
	}else*/
	if(mob==""){
		$("#mobDiv").html('Enter Mobile Number');
		return 0;
	}else if (!mob.match(pattern)) {

		$("#mobDiv").html("Mobile number contains illegal characters");
		return 0;
	}else if(mob.length != mob_no_length){
	
		$("#mobDiv").html("Mobile number should be "+mob_no_length+" digits long");
		return 0;
	}else{
	
		$("#mobDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}



}

// function for ckecking password and cofir password match
function confPassValidation(){
	
	var  cpass = $("#conf_password").val();
	if($("#password").val()=="" && cpass==""){
		
		$("#cpasswordDiv").html('Enter both password and confirm-password');
		return false;
	}else if($("#password").val()!=cpass){
	
		$("#cpasswordDiv").html('Password does not match');
		return false;
	}else{
	
		$("#cpasswordDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}

}

// function for phone number validation
function phneValidation(phone){

	if(!numberValidation(phone)){
	
		$("#phoneDiv").html('Please enter numbers only');
		return false;
	}else{
		$("#phoneDiv").html('');
		return true;
	}
}

// this is common function for used data is number or not 
function numberValidation(num){
	
	pattern = /^[0-9]*$/
	if(num=='' || num == null){
	
		return false;
	}
	if(num.match(pattern)){
	
		return true;
	}else{
	
		return false;
	}
}


//subcriber login for validation
function subFormValidation(){
	var error='';
	var step = $("#step").val();
	
	if(step==1){

		var errorMob = mobileValidation();
		var errorEmail = emailValidation();
		var errorPass = passValidation();
		var errorConfPass = confPassValidation();
		
		/*
		if(!mobileValidation() || !emailValidation() ||  !passValidation() || !confPassValidation()){
		
			error=1;
		
		} 
		*/
		if(!errorMob || !errorEmail ||  !errorPass || !errorConfPass){
		
			error=1;
		
		}
	}else if(step==2){
	
		if($("#name").val()==''){
		
			$("#nameDiv").html("Name is required");
			error=1;
		}else{
			$("#nameDiv").html("");
		}
		
		if($("#gender").val()==''){
		
			$("#genderDiv").html("Gender is required");
			error=1;
		}else{
			$("#genderDiv").html("");
		}

		if($("#ageRange").val()==''){
		
			$("#ageDiv").html("Age is required");
			error=1;
		}else{
			$("#ageDiv").html("");
		}

	}else if(step==3){
		
		if($("#location").val()==''){
		
			$("#locationDiv").html("Location  is required");
			error=1;
		}else{
			$("#locationDiv").html("");
		}
		if($("#termCondition ").attr('checked')==false){
		
			$("#tncDiv").html("Check Term & Condition");
			error=1;
		}else{
			$("#tncDiv").html("");
		}
	}
	
	if(error!=1){
	
		document.subRegister.submit();
	}
}

// subcriber edit profile validation
function subEditFormValidation(){

		//var errorMob; 
		var name;
		var email;
		var gender;
		var ageRange;
		var locations;
		
		var error=0;

		if(jQuery.trim($("#name").val())==""){
		
			$("#nameDiv").html("Name is required");
			name=false;
			error = 1;
		}else{
			$("#nameDiv").html("");
			name=true;

		}

		email = emailValidation($("#email").val());

		if($("#gender").val()==""){
		
			$("#genderDiv").html("Select Gender");
			gender = false;
			error  = 1;
		}else{
			$("#genderDiv").html("");
			gender = true;

		}

		if($("#ageRange").val()==""){
		
			$("#ageRangeDiv").html("Select Age Range");
			ageRange = false;
			error  = 1;
		}else{
			$("#ageRangeDiv").html("");
			ageRange = true;

		}

		if($("#location").val()==""){
		
			$("#locationDiv").html("Select Location");
			locations = false;
			error  = 1;
		}else{
			$("#locationDiv").html("");
			locations = true;

		}



		
		if(!name || !email || !gender || !ageRange || !locations ){
		
			error=1;
		
		}
	
	if(error!=1){
	
		document.subProfile.submit();
	}
}

// function to neighborhood of selected city
function getNeighborhood(cityId,neighborhoodId,url){
	$.ajax({              
              url: url, 
              type: "POST",                         
              data: "cityId="+cityId+"&nid="+neighborhoodId,            
              success: function (response) {                
                
				$("#neighborhood").html(response);
			  }
         });
}

// function to vendor list
function getVendorByUserType(vendor_type,url){
	$.ajax({              
              url: url, 
              type: "POST",                         
              data: "vendor_type="+vendor_type+"&action=vendor",            
              success: function (response) {                
				
				$("#vendorName").html(response);
			  }
         });
}

// function to cancle sheduled sms
function cancelSms(sid,url){
	
	$.ajax({
	
		url: url,
		type:"POST",
		data:"sid="+sid+"&action=cancel",
		success: function(response){
			
			if(response=='1'){
			
				window.location.reload();
			}
		
		}
	
	});
}

// function for vendor registration form validation
function venFormValidation(){
	var error='';
	var step = $("#step").val(); 
	if(step == 1){

		var errorUsername = usernameValidation();
		var errorPass = passValidation();
		var errorConfPass = confPassValidation();
		
		if($("#accType").val()==""){
			$("#accTypeDiv").html("Select account type");			
			error=1;
		}else if(!errorUsername || !errorPass || !errorConfPass){
			error=1;
		}
	}else if(step==2){
		var accType = $("#accType").val();
		var termCondition;
		var name;
		var organisation;
		var organisation_cat;
		var errorMob; 
		var errorEmail;
		var phone = true;
		var locations;
		var physicalAdd;
		var epm_range;
		
		if(accType==2){
			if($("#organisation").val()==""){
		
				$("#organisationDiv").html("Enter Organisation name");
				organisation = false;
			}else{
				$("#organisationDiv").html("");
				organisation = true;
			}

			if($("#organisationCat").val()==""){
		
				$("#organisationCatDiv").html("Select Organisation Category");
				organisation_cat = false;
			}else{
				$("#organisationCatDiv").html("");
				organisation_cat = true;
			}

			if($("#phone").val()!=""){
				if(!phneValidation($("#phone").val())){
					phone = false;
				}	
				
			}else{
				$("#phoneDiv").html("");
				
			}


			if($("#peopleRange").val()==""){
				$("#peopleRangeDiv").html("Select Number of employees")
				epm_range = false;
				
			}else{
				$("#peopleRangeDiv").html("");
				epm_range = true;
			}
			if($("#physicalAddress").val()==""){
				$("#physicalAddressDiv").html("Physical Address is required")
				physicalAdd = false;
				
			}else{
				$("#physicalAddressDiv").html("");
				physicalAdd = true;
			}
		
		}

		if($("#name").val()==""){
		
			$("#nameDiv").html("First name is required");
			name=false;
		}else{
			$("#nameDiv").html("");
			name=true;
		}
		if($("#location").val()==""){
		
			$("#locationDiv").html("Select Location");
			locations=false;
		}else{
			$("#locationDiv").html("");
			locations=true;
		}
		
		errorMob = mobileValidation();
		errorEmail = emailValidation();
		
		if($("#termCondition ").attr('checked')==false){
		
			$("#tncDiv").html("Check Term & Condition");
			termCondition=false;
		}else{
			$("#tncDiv").html("");
			termCondition=true;
		}
		
		if(accType==1){
			if(!errorEmail  || !errorMob || !termCondition || !name || !locations){
				error=1;
			}
		}else if(accType==2){
		
			if(!organisation || !organisation_cat || !errorEmail  || !errorMob || !termCondition || !name || !phone || !locations || !epm_range || !physicalAdd ){
				error=1;
			}
		}
	
	}
	
	if(error!=1){
	
		document.venRegister.submit();
	}
}

// function to check length of input message
function checkMessageLength(){
	
	var value = $("#textMessage").val();
	var val = $("#textMessage").val().length;
	
	//$("#charCount").val(val);
	$("#charCount").html(val);
	var counter = 1+parseInt((val-1)/160);
	$("#messageCount").html("Message Count :"+counter);
	
}
//function to display and hide advance option at message sending
function showHideAdvOption(){
	
	if(document.getElementById("advOptionDiv").style.display=='none'){
		$("#advOptionDiv").show();
		
		//$("#senderid").attr("disabled",false);
		$("#scheduleDate").attr("disabled",false);
		$("#advOptionTd").html('- Schedule Message');
	}else{
		$("#advOptionTd").html('+ Schedule Message');
		$("#advOptionDiv").hide();
		//$("#senderid").attr("disabled",true);
		$("#scheduleDate").attr("disabled",true);
	}
}

// function to validate mobile numer at time of login
function loginMobValidation(mob){

	var pattern =/^[0-9]*$/;
	if(!mob.match(pattern)) {
		
		$("#mobDiv").html("Enter valid mobile number");
		return false;
	}else{
		$("#mobDiv").html('');
	}
}

//function to used to checked all checkboxes 
function checkAll(all,container){

	if($("#"+all).attr('checked')== true){
		
		 $( "#"+container+" :checkbox").attr('checked',  true);
		
	}else{
		
		 $( "#"+container+" :checkbox").attr('checked',false);
	}

}

// function for opening terms & condition in another window
function openTermToRead(url){

	window.open(url, 
		'Bongolive', 
		'width=600,	height=600,directories=no,location=no,top=50,menubar=no, resizable=no,scrollbars=1,status=no, 	toolbar=no'
	);
}

function keepCheckAddbook(bookId){
	$("#"+bookId).attr("checked","checked");
}

// funtion used to check all checkboxes of addressbook or groups
function checkAllGroup(all,container,leavBook){

	if($("#"+all).attr('checked')== true){
		
		$("#"+container+" :checkbox").attr('checked',true);
		 
	}else{

		$("#"+container+" :checkbox").attr('checked',false);
		$("#"+leavBook).attr("checked","checked");
	}

}