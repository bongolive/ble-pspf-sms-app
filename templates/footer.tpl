	<div id="footer"> <img class="bg-footer-purple" alt="bg" src="{$BASE_URL_HTTP_ASSETS}images/bg_bottom01.png" />
		<p class="copy">{$smarty.const.COPYRIGHT}</p>
		<div class="nav-bottom">
			<ul>
				<li><a href="http://blog.bongolive.co.tz">{$smarty.const.BLOG}</a></li>
				<li>|</li>
				<li><a href="aboutus.php">{$smarty.const.ABOUT_US}</a></li>
				<li>|</li>
				<li><a href="privacy.php">{$smarty.const.PRIVACY}</a></li>
				<li>|</li>
				<li><a href="terms_of_use.php">{$smarty.const.TERMS_OF_USE}</a></li>
				<li>|</li>
				<li><a href="faq.php">{$smarty.const.FAQ}</a></li>
				<li>|</li>
				<li><a href="contactus.php">{$smarty.const.SUPPORT}</a></li>
				<li>|</li>
				<li><a href="count.php">{$smarty.const.COUNTER}</a></li>
			</ul>
</div>
		<div class="clear"></div>
	</div>
</body>
<script type="text/javascript">
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-10249291-9']);
	_gaq.push(['_trackPageview']);

	(function(){
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
</html>