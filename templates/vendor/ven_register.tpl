<div id="container">
<div class="line01"></div>
	<div class="column2-ex-left-2 column2_contents">
    <h1>{$smarty.const.BROADCASTER_SIGN_UP}</h1>

{*
    File : ven_register.tpl
    Purpose: used to register for cliens or vendors, for all steps in registration this file is used 
    
*}
<div style="clear: both;">
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg"{else} class="sucess_msg"{/if}
{if $msg} style="display:block;" {else} style="display:none;"{/if}>{$msg}</div>

<form name="venRegister" method="POST" action="" onsubmit="javascript: venFormValidation(); return false;">
    {if $step == '1' || $step == "" || $step=='0'}
        <table width="100%" cellspacing="10">
            <tr>
                <td width="24%">{$smarty.const.USERNAME}<span class="star_col">*</span></td>
                <td width="76%"><span class="form_dinatrea standard-input">
				<input type="text" name="username" id="username" value="{$username}" onkeyup="usernameValidation();"></span>
			  <span id="usernameDiv" class="form_error"></span></td>
            </tr>
            <tr>
                <td>{$smarty.const.PASSWORD}<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input"><input type="password" name="password" id="password" onkeyup="passValidation();"  value="{$password}"></span><span id="passwordDiv" class="form_error"></span></td>
            </tr>
            <tr>
                <td>{$smarty.const.CONFIRM_PASSWORD}<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input"><input type="password" name="conf_password" id="conf_password" onkeyup="confPassValidation();"  value="{$password}"></span><span id="cpasswordDiv" class="form_error"></span></td>
            </tr>
	    <tr>
                <td>{$smarty.const.ACCOUNT_TYPE}<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input">	
			<select name="accType" id="accType">
			<option value="">{$smarty.const.SELECT}</option>
			<option value="1" {if $accType=='1'}selected{/if}>{$smarty.const.INDIVIDUAL}</option>	
			<option value="2" {if $accType=='2'}selected{/if}>{$smarty.const.ORGANIZATION}</option>	
			</select></span>
		<span id="accTypeDiv" class="form_error"></span></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="next1" id="next1" value="{$smarty.const.NEXT}" class="editbtn">
                </td>
			</tr>
			<tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 			
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="{$BASE_URL_HTTP_ASSETS}images/van_stp-0.png" />
                </td>
			</tr>           
        </table>
    {elseif $step == '2'}
        {if $accType=='1'}
		<table width="100%" cellspacing="10">
		    <tr>
			<td width="45%">{$smarty.const.NAME} <span class="star_col">*</span></td>
			<td width="55%"><span class="form_dinatrea standard-input">
			  <input type="text" name="name" id="name" value="{$name}" ></span>
			  <span id="nameDiv" class="form_error"></span>
			  </td>
	      </tr>
		    <tr>
			<td>
				{$smarty.const.MOBILE_NUMBER} <span class="star_col">*</span><br/><span class="mobexmp">(Ex. 784845785)</span>
			</td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" value="{$mob_no}" onkeyup="mobileValidation();" maxlength="{$mobMaxlength+1}"></span><span id="mobDiv" class="form_error"></span></td>
		    </tr>
		    <tr>
			<td>{$smarty.const.EMAIL} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="{$email}" onkeyup="emailValidation();" ></span>
			<span id="emailDiv" class="form_error"></span></td>

		    </tr>
		    <!-- this option is not required for individual vendor
			<tr>
			<td>Number of employees</td>
			<td><span class="form_dinatrea standard-input">
				<select name="peopleRange" id="peopleRange" >
				<option value="">Select</option>
				{section name=pplRange loop=$arrPeopleRanges}
				<option value="{$arrPeopleRanges[pplRange].id}" {if $arrPeopleRanges[pplRange].id==$peopleRange}selected{/if}>{$arrPeopleRanges[pplRange].people_range}</option>
				{/section}
				</select></span>

			<span id="emailDiv" class="form_error"></span></td>

		    </tr>
			-->
		    <tr>
			<td>{$smarty.const.POSTAL_ADDRESS}</td>
			<td><span class="form_dinatrea standard-input"><textarea name="postalAddress" id="postalAddress" >{$postalAddress}</textarea></span>
			</td>
		    </tr>
		    
		     <tr>
			<td>{$smarty.const.LOCATION} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
			    <select name="location" id="location" >
				<option value="">{$smarty.const.SELECT}</option>
				{section name=location loop=$arrLocation}
				<option value="{$arrLocation[location].id}" {if $arrLocation[location].id==$location}selected{/if}>{$arrLocation[location].location}</option>
				{/section}
			    </select>
				</span>
				<span id="locationDiv" class="form_error"></span>
			</td>
			<td></td>
		    </tr>
		    <tr>
			<td>{$smarty.const.ACCEPT_BONGOLIVE} <a  class="content-link" href="javascript: void(0)" 
				onclick="openTermToRead('{$BASE_URL}{if $terms_of_use==1}lang_{$smarty.session.lang}/{/if}terms_of_use.html'); return false;">{$smarty.const.TERMS_OF_USE}<a> <span class="star_col">*</span></td>
			<td>
			    <span style="width:20px; display:block;float:left;"><input type="checkbox" name="termCondition" id="termCondition" value="1" ></span>
			<span id="tncDiv" class="form_error"></span></td>
		    </tr>
			
		    <tr>
			<td>
			    <input type="submit" name="back" id="back" value="{$smarty.const.BACK}" class="editbtn">
			</td>
			<td>
			    <input type="submit" name="next2" id="next2" value="{$smarty.const.SUBMIT}" class="editbtn">
			</td>
		    </tr>
				<tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="{$BASE_URL_HTTP_ASSETS}images/van_stp-1.png" />
                </td>
			</tr>  
		</table>
	{elseif $accType=='2'}

		<table width="100%" cellspacing="10">
		    <tr>
			<td width="45%">{$smarty.const.ORGANIZATION_NAME} <span class="star_col">*</span></td>
			<td width="55%"><span class="form_dinatrea standard-input">
			  <input type="text" name="organisation" id="organisation" value="{$organisation}" ></span>
			  <span id="organisationDiv" class="form_error"></span>
			  </td>
			</tr>
		    <tr>
			<td>{$smarty.const.ORGANIZATION_CATEGORY} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"> <select name="organisationCat" id="organisationCat" >
				<option value="">{$smarty.const.SELECT}</option>
				{section name=onganisation loop=$arrOrganisationCat}
				<option value="{$arrOrganisationCat[onganisation].id}" {if $arrOrganisationCat[onganisation].id==$organisationCat}selected{/if}>{$arrOrganisationCat[onganisation].organisation}</option>
				{/section}
			    </select></span>
			    <span id="organisationCatDiv" class="form_error"></span>
			</td>
		    </tr>
		    <tr>
			<td>{$smarty.const.REPRESENTATIVE_NAME}<span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="name" id="name" value="{$name}"/></span>
			<span id="nameDiv" class="form_error"></span>
			
			</td>
		    </tr>
		    <tr>
			<td>{$smarty.const.MOBILE_NUMBER} <span class="star_col">*</span> <br/> <span class="mobexmp">(Ex. 784845785)</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" value="{$mob_no}" onkeyup="mobileValidation();" maxlength="{$mobMaxlength}"></span><span id="mobDiv" class="form_error"></span></td>
		    </tr>
		    <tr>
				<td>{$smarty.const.LANDLINE_NUMBER} <span class="star_col"></span> <br/><span class="mobexmp">(Ex. 0222180995)</span></td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="phone" id="phone" value="{$phone}" maxlength="{$phoneMaxlength}" onkeyup="javascript: phneValidation(this.value);"/></span>
				<span id="phoneDiv" class="form_error"></span>
				</td>
		    </tr>
		     <tr>
			<td>{$smarty.const.EMAIL} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="{$email}" onkeyup="emailValidation();" ></span><span id="emailDiv" class="form_error"></span></td>

		    </tr>
		    <tr>
			<td>{$smarty.const.NUMBER_OF_EMPLOYEES} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
				<select name="peopleRange" id="peopleRange" >
				<option value="">{$smarty.const.SELECT}</option>
				{section name=pplRange loop=$arrPeopleRanges}
				<option value="{$arrPeopleRanges[pplRange].id}" {if $arrPeopleRanges[pplRange].id==$peopleRange}selected{/if}>{$arrPeopleRanges[pplRange].people_range}</option>
				{/section}
				</select></span>

			<span id="peopleRangeDiv" class="form_error"></span></td>

		    </tr>
		     <tr>
			<td>{$smarty.const.PHYSICAL_ADDRESS} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><textarea name="physicalAddress" id="physicalAddress" >{$physicalAddress}</textarea></span>
			<span id="physicalAddressDiv" class="form_error"></span>
			</td>
		    </tr>
		     <tr>
			<td>{$smarty.const.POSTAL_ADDRESS}</td>
			<td><span class="form_dinatrea standard-input"><textarea name="postalAddress" id="postalAddress" >{$postalAddress}</textarea></span>
			</td>
		    </tr>
		     <tr>
			<td>{$smarty.const.LOCATION} <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
			    <select name="location" id="location" >
				<option value="">{$smarty.const.SELECT}</option>
				{section name=location loop=$arrLocation}
				<option value="{$arrLocation[location].id}" {if $arrLocation[location].id==$location}selected{/if}>{$arrLocation[location].location}</option>
				{/section}
			    </select>
				</span>
				<span id="locationDiv" class="form_error"></span>
			</td>
			<td></td>
		    </tr>
		    <tr>
			<td>{$smarty.const.ACCEPT_BONGOLIVE} <a  class="content-link" href="javascript: void(0)" 
				onclick="openTermToRead('{$BASE_URL}{if $terms_of_use==1}lang_{$smarty.session.lang}/{/if}terms_of_use.html'); return false;">{$smarty.const.TERMS_OF_USE}<a> <span class="star_col">*</span>
			</td>
			<td>
			    <span style="width:20px; display:block;float:left;"><input type="checkbox" name="termCondition" id="termCondition" value="1" ></span>
			<span id="tncDiv" class="form_error"></span></td>
		    </tr>
			
		    <tr>
			<td>
			    <input type="submit" name="back" id="back" value="{$smarty.const.BACK}" class="editbtn">
			</td>
			<td>
			    <input type="submit" name="next2" id="next2" value="{$smarty.const.SUBMIT}" class="editbtn">
			    
			</td>
		    </tr>
				<tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="{$BASE_URL_HTTP_ASSETS}images/van_stp-1.png" />
                </td>
			</tr>  
		</table>

	{/if}
	 <input type="hidden" name="accType" id="accType" value="{$accType}">
     {elseif $step=='3'}

	<table width="100%" cellspacing="10">
            <tr>
                <td width="33%">{$smarty.const.MOBILE_VERIFICATION}</td>
                <td width="67%"></td>
            </tr>
            
            <tr>
                <td>{$smarty.const.ENTER_VERIFICATION_CODE}</td>
                <td><span class="form_dinatrea standard-input">
                    <input type="text" name="mob_conf_msg" id="mob_conf_msg" value="{$mobConfMsg}"></span>
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    <input type="submit" name="next4" id="next4" value="{$smarty.const.CONFIRM}" class="editbtn">
                </td>
            </tr>				
        </table>
 {elseif $step=='4'}
	 <table width="100%" cellspacing="10">
		<tr>	
			<td>&nbsp;</td>
			<td align="left">
				<img alt="0%" src="{$BASE_URL_HTTP_ASSETS}images/van_stp-2.png" />
			</td>
		</tr>  
	</table>

    {/if}
	<input type="hidden" name="step" id="step" value="{$step}">
	<span class="form_dinatrea standard-input">
	<input type="hidden" name="userId" id="userId" value="{$userId}">
	</span>
	<input type="hidden" name="action" id="action" value="{$action}">
</form>

</div>
</div>
      <div class="clear"></div>
</div>

{literal}
    
    <script type="text/javascript">
		//<!--
            
            $("#back").click(function(){
                
                var step = parseInt($("#step").val())-2;
                $("#step").val(step);
                $("#action").val('edit');
                document.subRegister.submit();
            });
			
			//$(document).ready(
			//function(){
				//var str = $("#errorDiv").html();
				//alert(str.length);
			//	if(str.length<=0){
			//		$("#errorDiv").hide();
			//	}else{
			//		$("#errorDiv").show();
			//	}
			//}
			
		//	);

		
        //-->
     </script>


{/literal}