<div id="container">
	<div class="line01"></div>
  <div class="column2-ex-left-2-old">
		<h1>{$smarty.const.VEN_SIMULATE_SMS_REQUEST}</h1>
	    <div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
		<form name="smsRequest" method="post" action="" onsubmit="return validateSmsRequest();">
		<table border="0">
          <tr>
            <td valign="top" width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td width="150"><strong>{$smarty.const.MOBILE_NUMBER}</strong></td>
            <td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" maxlength="13" value="{$mobNo}" onkeyup="javascript: mobileValidation();"></span><span id="mobDiv" class="form_error"></span></td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td valign="top" width="150"><div align="left"><strong>{$smarty.const.MESSAGE_BODY}:</strong></div></td>
            <td><textarea name="textMessage" cols="40"  rows="4" class="textarea" id="textMessage">{$textMessage}</textarea>
			<span id="textMessageDiv" class="form_error" style="width:500px;"></span>
			</td>
          </tr>
          <tr>
            <td width="150"><strong>{$smarty.const.COUNTER}:</strong></td>
            <td><div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
				<div style="width:70%; float:left;" id="messageCount">&nbsp;</div></td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td><input type="submit" name="send" id="send" value="{$smarty.const.SEND}" class="editbtn"></td>
          </tr>
        </table>
		<br/>
        </form>
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>
{literal}
    <script type="text/javascript">
		function validateSmsRequest(){
			if(!mobileValidation()){
                    return false; 
            }else if($("#textMessage").val()==''){
				$("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE}{literal}");
				return false;
			}else{
				$("#textMessageDiv").html("");
				$("#mobDiv").html("");
				return true;
			}
		
		}
	</script>
{/literal}