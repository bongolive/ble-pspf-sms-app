<div id="container">
    <h1>SMS Log Report </h1>
	<div style="width:850px;">
	<div id="errorDiv"></div>
	
	<form name="broccastLogForm" action="{$selfUrl}" method="POST">
		<table cellspacing="10" width="600">	
			<tr>
				<td><strong>Client  Name:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<select name="broadcaster" id="broadcaster">
					<option value="">Select</option>
					{section name=broadcaster loop=$arrSmsSender}
					<option value="{$arrSmsSender[broadcaster].id}" 
					{if $arrSmsSender[broadcaster].id==$broadcaster}selected{/if}
					>{$arrSmsSender[broadcaster].username} ({$arrSmsSender[broadcaster].name})</option>
					{/section}
					</select></span>
				</td>				
			</tr>
			
			<tr>
				<td><strong>Start Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
			</tr>
			<tr>
				<td><strong>End Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="dndLog" id="dndLog" value="Download Log" class="green_btnbg">
					<input type="hidden" name="action" id="action" value="dnd">
				</td>
			</tr>
		</table>
	</form>	
	
</div>

</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		function selectBroadcatedrType(brdType){
			
			if(brdType==3){
				$("#vendorName").val("");
				$("#vendorName").attr("disabled",true);
				
			}else{
				
				$("#vendorName").removeAttr("disabled");
				getVendorByUserType(brdType,$("#ajaxUrl").val());
			}

		}
		
		
        //-->
     </script>