<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.VEN_KEYWORD_BROADCAST}</h1>
	<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
	{if $msg} style="display:block;" {else} style="display:none;" {/if}> {$msg} </div>

<table align="center" cellpadding="0" cellspacing="0" width="100%">
		<tr>
		<td>
		<form name="smsPushForm" method="POST" action="" onsubmit="validatSmspushVen(); return false;">
		{if !$smsconf}
		 <table align="center" border="0" cellpadding="0" cellspacing="10" width="100%">
		<!--prim key--><tr  style=" background:#f6f6f6;" width="83%">
			<!-- <td>Broadcaster name:</td>
			<td>
				<span class="form_dinatrea standard-input">
					<input type="hidden" value="" id="coupone_broadcater_credit" name="cb_credit" />
					<select name="vendorName" id="coupone_broadcater">
					<option value="-1">Select broadcater</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}"
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
			</td>-->
			<td><div style="float:right; margin-right:50px; font-size:25px;" id="broadcaster_sms_count">
			<input type="hidden" value="" name="ven_sms_credit" /></td>
			</tr>
			<tr>
				<td valign="top" colspan="3">&nbsp;</td>
			</tr>

			<tr>
				<td valign="top">{$smarty.const.MESSAGE_BODY}:</td>
				<td align="left">
				<textarea name="textMessage" id="textMessage" class="textarea"  rows="3">{$textMessage}</textarea>
				</td>
				<td><span id="textMessageDiv" class="form_error"></span></td>
			</tr>
			<tr>
				<td>{$smarty.const.COUNTER}:</td>
				<td colspan="2" align="left">
				<div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
				<div style="width:70%; float:left;" id="messageCount"></div></td>
			</tr>
			<tr>
				<td>{$smarty.const.SENDER_NAME}:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="senderid" id="senderid">
					<option value="">{$smarty.const.SELECT}</option>
					{section name=senderid loop=$myArray}
					<option value="{$myArray[senderid].id}" {if $senderid==$myArray[senderid].id}selected{/if}>{$myArray[senderid].senderid}</option>
					{/section}
					<!--  {foreach from=$myArray key=k item=v}
					   <option value="{$k}">{$v}</option>
					{/foreach}-->
					</select></span>
				</td>
				<td><span id="senderidDiv" class="form_error"></span></td>

			</tr>
			<!--prim key--><tr>
				<td>{$smarty.const.VEN_KEYWORDS}:</td>
				<td>
					<span class="form_dinatrea standard-input">
						<select name="prim_key" id="prim_key">
						<option value="-1">Select key</option>
							{foreach from=$PrimKeyArray key=k item=v}
					   			<option value="{$k}">{$v}</option>
							{/foreach}
						</select>
					<input type="hidden" name="prim_key_id" id="prim_key_id" value="-1" />
					</span>
				</td>
				<td><span id="count_user_subscriber" ></span><input type="hidden" name="count_user_subscriber" id="input_count_user" value="0" /><span id="keyDiv" class="form_error"></span></td>
			</tr>

			<tr>
				<td id="advOptionTd" style="cursor:pointer; font-weight:bold;" onclick="javascript: showHideAdvOption();">

				{if $scheduleDate && $scheduleDate!=''}
				- {$smarty.const.ADVANCED_OPTION}
				{else}
				+ {$smarty.const.ADVANCED_OPTION}
				{/if}
				</td>

				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="5">
					<div id="advOptionDiv" {if $scheduleDate && $scheduleDate!=''}style="display:block" {else}style="display:none"{/if}>
						<table align="center" cellpadding="0" cellspacing="10" width="100%">

							<tr>
								<td width="39%">{$smarty.const.SCHEDULE_DATE}:</td>
								<td width="61%"><span class="form_dinatrea standard-input"><input type="text" name="scheduleDate" id="scheduleDate" value="{$scheduleDate}"></span></td>

							</tr>

						</table>

					</div>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>

					<input type="submit" name="send" id="send" value="{$smarty.const.SEND}" class="editbtn" >
					<input type="hidden" name="action" id="action" value="smspush" >
				</td>
				<td></td>
			</tr>


		</table>
		{else}
			<table align="center" width="600" cellpadding="0" cellspacing="6" style="background-color:#f8f8f8;">
				<tr>
					<td>{$smarty.const.SMS_COUNT}</td>
					<td>{$smsCount}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.TOTAL_CONTACTS}</td>
					<td>{$mobileCount}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.CREDITS_BALANCE}</td>
					<td>{$creditBalance}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.CREDITS_COSTS}</td>
					<td>{$creditNeeded}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>
					<input type="button" name="back" id="back" value="Back"  onclick="javascript: backTo(1)" class="editbtn">
					</td>
					<td>
					<input type="submit" name="conf" id="conf" value="{$smarty.const.SEND}" {if $mobileCount<1 || $creditBalance<$mobileCount}disabled  class="redbtn"{else}class="editbtn" {/if} />
					<input type="hidden" name="action" id="action" value="smsconf" >
					</td>
				</tr>
			</table>

		{/if}
		</form>



		<!-- middle body end-->



        </td>
    </tr>
</table>
</div>
  <div class="clear"></div>
</div>
{literal}

    <script type="text/javascript">
	//<!--

		$('document').ready(function() {
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		});



		function backTo(val){

			$("#action").val('backTo'+val);
			document.smsPushForm.submit();
		}

		function validatSmspushVen(){
			var errorFlag = 0;
			console.log('iii');
			if($("#action").val()!='smsconf'){
				if($('#coupone_broadcater').val()==-1)
				{
					$("#senderidDiv").html("{/literal}{$smarty.const.SELECT_BROADCASTER}{literal}");
					errorFlag = 1;
					return false;
				}
				else{ $("#senderidDiv").html(""); }
				if($("#textMessage").val()==''){

					$("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE}{literal}");
					errorFlag = 1;
					return false;
				}else{
					$("#textMessageDiv").html("");
				}

				if($("#senderid").val()==''){

					$("#senderidDiv").html("{/literal}{$smarty.const.SELECT_SENDER_NAME}{literal}");
					errorFlag = 1;
					return false;
				}else{
					$("#senderidDiv").html("");
				}
                if($('#prim_key_id').val()==-1)
                {
                    $("#count_user_subscriber").css("color","red");$("#count_user_subscriber").html("select keyword");
                    errorFlag = 1;
					return false;
                }
                 if($('#input_count_user').val()==0)
                {
                    $("#count_user_subscriber").css("color","red");$("#count_user_subscriber").html("zero subscriber");
                    errorFlag = 1;
					return false;
                }
			}

			if(errorFlag == 0){

			   document.smsPushForm.submit();
			}

		}




        //-->
     </script>


{/literal}

