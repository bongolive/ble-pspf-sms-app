<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 910px;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 910px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 130px;
	text-align: left;
		}


.row-2{
 width:190px;
 text-align: left;
}

.row-3{
 width:60px;
 text-align:center;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 950px; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}

</style>
<div id="container">
{if $action=='edit'}
<div>
    <h1>Edit Scheduled Message</h1>
</div>
{else}
<div>
    <h1>{$smarty.const.MANAGE_BROADCAST}</h1>
</div>
{/if}
	<div style="width:850px;">
	<div id="errorDiv"></div>
	{if $action=='' || $action=='search'}
	<form name="creditRequest" action="{$selfUrl}" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Broadcast Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="sms_type" id="sms_type" >
						<option value="" >Select</option>
						<option value="1" {if $sms_type=='1'}selected{/if} >All</option>
						<option value="2"  {if $sms_type=='2'}selected{/if}>Pushed</option>
						<option value="3"  {if $sms_type=='3'}selected{/if}>Scheduled</option>
					</select>
				</span>
				</td>
			</tr>
			<tr>
				<td>SMS Message:</td>
				<td><span class="form_dinatrea standard-input" style="width:250px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="3">{$textMessage}</textarea></span>
				</td>
			</tr>
			<tr>
				<td>Job Name:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="jobName" id="jobName" value="{$jobName}"></span>
				</td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="{$smarty.const.SEARCH}" class="editbtn">
					<input type="hidden" name="action" id="action" value="search">
					<input type="hidden" name="ajaxUrl" id="ajaxUrl" value="{$ajaxUrl}">
				</td>
			</tr>
		</table>
	</form>	
	
	<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
      <!--  <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	<table style="margin-top:20px; margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">		
		<td class="row-1">Sender ID</td>
		<!--
		<td class="row-1">Broadcast Type</td>
		-->
		<td class="row-2">SMS Message</td>
		<td class="row-1">Date Created </td>
		<td class="row-1">Schedule Date </td>
		<!--
		<td class="row-1">Status</td>
		
		<td class="row-1">Price</td>
		-->
		<td class="row-3">Sms Count</td>
		
		<td class="row-3">Credit Cost </td>
		<td class="row-1">Job Name </td>
		<td class="row-3">Edit</td>
		<td class="row-3">&nbsp;</td>
	</tr>
	{if $arrSms}
		{section name=sms loop=$arrSms}	
			<tr class="row-body">	
				<td class="row-1">{$arrSms[sms].vensenderid}</td>
				<!--
				<td class="row-1">{$arrSms[sms].credit_requested}</td>
				-->
				<td class="row-2">{$arrSms[sms].text_message|wordwrap:20:"<br />\n"}</td>
				<td class="row-1">{$arrSms[sms].created}</td>
				<td class="row-1">{$arrSms[sms].send_time}</td>
				<!--
				<td class="row-1">{$arrSms[sms].status}</td>
				
				<td class="row-1">{$arrSms[sms].credit}</td>
				-->
				<td class="row-3">{$arrSms[sms].message_count}</td>
				
				<td class="row-3">{$arrSms[sms].credit}</td>
				<td class="row-1">{$arrSms[sms].job_name}</td>
				<td class="content-link row-3">
					{if $arrSms[sms].is_shceduled=='1' && $arrSms[sms].is_proccessed=='0'}
					<a href="?sid={$arrSms[sms].id}&action=edit"><img src="{$BASE_URL_HTTP_ASSETS}images/watch.png"></a>
					{/if}				</td>
				<td class="content-link row-3" style="padding:8px;">
					{if $arrSms[sms].is_shceduled=='1' && $arrSms[sms].is_proccessed=='0'}
					<a href="javascript: void(0);" onclick="javascript: if(confirm('Are you sure to cancel this scheduled broadcast'))cancelSms('{$arrSms[sms].id}','{$ajaxUrl}');"><img src="{$BASE_URL_HTTP_ASSETS}images/cancel.png"></a>
					{/if}				</td>
			</tr>
		{/section}
	
		<tr style="background-color:#f8f8f8;">
			<td align="right" colspan="13" class="content-link" style="padding:10px 0;">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}</td>
		</tr>
	{else}
		<tr class="TR_spc"><td colspan="9" class="TD_spc"></td>
		</tr>
		<tr style="background-color:#f8f8f8;">
			<td colspan="9" align="center"  class="content-link" valign="middle" style="padding:10px;">&nbsp;</td>
		</tr>		

	{/if}
	</table>
	</div>
          <div class="clear"></div>
     </div>

{elseif $action=='edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST">
	<table cellspacing="10" width="600">	

		<tr>
			<td valign="top">SMS Message:</td>
			<td><span class="form_dinatrea standard-input" style="width:250px;">
				<textarea name="textMessage" id="textMessage" cols="40" readonly rows="3">{$arrSmsToEdit.text_message}</textarea></span>
			</td>
		</tr>
		<tr>
				<td>Schedule date</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="scheduleDate" id="scheduleDate" value="{$arrSmsToEdit.send_time}"></span>
				</td>
		</tr>
		<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Save" class="editbtn">
					<input type="hidden" name="action" id="action" value="update">
					<input type="hidden" name="sid" id="sid" value="{$arrSmsToEdit.id}">
				</td>
		</tr>
		
	</table>
	</form>
{/if}


</div>

</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});

		function selectBroadcatedrType(brdType){
			
			if(brdType==3){
				
				$("#vendorName").val("");
				$("#mob_no").val("");
				$("#vendorName").attr("disabled",true);
				$("#mob_no").attr("disabled",true);
			
			}else{
				
				$("#vendorName").removeAttr("disabled");
				$("#mob_no").removeAttr("disabled");
				getVendorByUserType(brdType,$("#ajaxUrl").val());
			}



		}
		
		
        //-->
     </script>
