<div id="container">
    <h1>Manage Primary Keywords </h1>
    <div style="width:700px; margin:0 auto;">	
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg"{else} class="sucess_msg"{/if}
{if $msg} style="display:block;" {else} style="display:none;"{/if}>{$msg}</div>

<form name="orgCatForm" method="POST" action="{$selfUrl}" onsubmit="orgCatValidation(); return false;">
<table cellspacing="10" width="100%">
	<tr>
		<td width="250">Primary Keyword </td>
		<td width="737"><span class="form_dinatrea standard-input">
		<input type="text" name="text_prim_keyword" id="text_prim_keyword" value="{$text_prim_keyword}" size="70"></span>
		<span id="locationDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="save" id="save" value="Save" class="editbtn" >
			<input type="hidden" name="action" id="action" value="{$action}" >
			<input type="hidden" name="id" id="id" value="{$id}" >
	</tr>
</table>
</form>
<br /><br />
 <table align="center" width="100%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
	<tr>
		<td width="50%" class="content-link_green">Primary Keywords </td>
		<td width="30%" class="content-link_green">&nbsp;</td>
		<td width="10%" class="content-link_green">Edit</td>
		
	</tr>
	{section name=result loop=$arrPk}
	<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td align="left">{$arrPk[result].text_prim_keywords}</td>
			<td align="left"><a href="{$mngUrl}?prim_keywords_id={$arrPk[result].id_prim_keywords}" title="Manage">
			Manage Second Keywords</a></td>	
			<td><a href="{$selfUrl}?action=edit&id={$arrPk[result].id_prim_keywords}" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
			
		</tr>
	
	{/section}
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="10" class="content-link" style="padding:10px;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
</table>
</div></div>
{literal}
    
    <script type="text/javascript">
	//<!--
		function orgCatValidation(){
			
			var error=0;
			if($("#text_prim_keyword").val()==''){
			
				$("#locationDiv").html('Enter Primary Keyword Name');
				error=1;	
			}else {
				$("#locationDiv").html('');
			
			}
			if(error !=1){
				document.orgCatForm.submit();
			}
		
		
		}
		
		
        //-->
     </script>


{/literal}

