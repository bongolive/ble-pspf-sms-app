<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 70px;
	text-align: left;
		}


.row-2{
 width:300px;
 text-align: left;
}

.footer-top{
    border: 0px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}
</style>
<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.ADD_CONTACT}</h1>
<div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>

<table align="center" cellpadding="0" cellspacing="0">
<tr><td>&nbsp;</td></tr>
	</tr>
	<tr>
        <!--td>{include file="vendor/ven_left.tpl"}</td-->
        <td>  
             
		<form name="addContactForm" method="POST" action="" onsubmit="javascript: validationAddContact(); return false;">
			
			<table align="center" width="600" cellpadding="0" cellspacing="10" >
				<tr>
					<td>{$smarty.const.MOBILE_NUMBER} <span class="star_col">*</span> <br/></td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" maxlength="{$mob_no_length + 1}" value="{$arrContactDetails.mob_no}" onkeyup="javascript: mobileValidation();"></span><span id="mobDiv" class="form_error"></span></td>
				</tr>
				<tr>
					<td>{$smarty.const.FIRST_NAME}</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="fname" id="fname" value="{$arrContactDetails.fname}"></span><span id="fnameDiv" class="form_error"></span></td>
				</tr>
				<tr>
					<td>{$smarty.const.LAST_NAME}</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="lname" id="lname" value="{$arrContactDetails.lname}"></span></td>
				</tr>
				<tr>
					<td width="140">{$smarty.const.TITLE}:</td>
					<td width="428"><span class="form_dinatrea standard-input">
					<select name="title" id="title" >
					<option value="" >{$smarty.const.SELECT}</option>	
					<option value="Mr" {if $arrContactDetails.title=='Mr'}selected{/if}>{$smarty.const.MR}</option>
					<option value="Mrs" {if $arrContactDetails.title=='Mrs'}selected{/if}>{$smarty.const.MRS}</option>
					<option value="Miss" {if $arrContactDetails.title=='Miss'}selected{/if} >{$smarty.const.MISS}</option>
				  </select></span><span id="titleDiv" class="form_error"></span></td>
				</tr>
				<tr>
				  <td>{$smarty.const.BIRTH_DATE}</td>
				  <td>
				  
				  <span class="form_dinatrea standard-input" style="width:65px; margin-right:10px;">
				    	 <select name="day" id="day">
							<option value="">Day:</option>
							{section name=arrDay loop=31} 
							<option value="{$smarty.section.arrDay.iteration}" {if $smarty.section.arrDay.iteration==$day} selected{/if}>{$smarty.section.arrDay.iteration}</option>
							{/section}
						</select>
				  </span>
				  
				  <span class="form_dinatrea standard-input" style="width:80px; margin-right:10px; margin-left:0px">
				    	 <select name="month" id="month">
						 	<option value="">Month:</option>
							<option value="1" {if $month==1} selected{/if}>Jan</option>
							<option value="2" {if $month==2} selected{/if}>Feb</option>
							<option value="3" {if $month==3} selected{/if}>Mar</option>
							<option value="4" {if $month==4} selected{/if}>Apr</option>
							<option value="5" {if $month==5} selected{/if}>May</option>
							<option value="6" {if $month==6} selected{/if}>Jun</option>
							<option value="7" {if $month==7} selected{/if}>Jul</option>
							<option value="8" {if $month==8} selected{/if}>Aug</option>
							<option value="9" {if $month==9} selected{/if}>Sep</option>
							<option value="10" {if $month==10} selected{/if}>Oct</option>
							<option value="11" {if $month==11} selected{/if}>Nov</option>
							<option value="12" {if $month==12} selected{/if}>Dec</option>
						</select>
				  	 </span>
				  
				  <span class="form_dinatrea standard-input" style="width:70px; margin-right:5px;">
				     <select name="year" id="year">
							<option value="">Year:</option>
							{section name=arrYear loop=80} 
							<option value="{($currentYear - $smarty.section.arrYear.iteration)}" {if $year==($currentYear - $smarty.section.arrYear.iteration)} selected{/if}>{($currentYear - $smarty.section.arrYear.iteration)}</option>
							{/section}
						</select>
				 		 </span><span id="birthdayDiv" class="form_error"></span>
				  
				  </td>
			  </tr>
				<tr>
				  <td>{$smarty.const.GENDER_}</td>
				  <td><span class="form_dinatrea standard-input">
				    <select name="gender" id="gender" >
                      <option value="" >{$smarty.const.SELECT}</option>
                      <option value="Male" {if $arrContactDetails.gender=='Male'} selected{/if} >{$smarty.const.MALE}</option>
                      <option value="Female" {if $arrContactDetails.gender=='Female'} selected{/if} >{$smarty.const.FEMALE}</option>
                    </select>
				  </span></td>
			  </tr>
				<tr>
				  <td>{$smarty.const.MOBILE_NUMBER2}</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="mob_no2" id="mob_no2" maxlength="{$mob_no_length + 1}" value="{$arrContactDetails.mob_no2}" onkeyup="javascript: mobile2Validation();" />
				  </span><span id="mob2Div" class="form_error"></span></td>
			  </tr>
				<!--
				<tr>
				  <td>{$smarty.const.POSTAL_ADDRESS}</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="address" id="address" value="{$arrContactDetails.address}" />
				  </span></td>
			  </tr>
			  -->
				<tr>
					<td>{$smarty.const.EMAIL}</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="{$arrContactDetails.email}" onkeyup="javascript: emailValidation();"></span><span id="emailDiv" class="form_error"></span></td>
				</tr>
				<!--
				<tr>
					<td>{$smarty.const.COMMENTS} </td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="customs" id="customs" value="{$arrContactDetails.customs}"></span></td>
				</tr> -->
				<tr>
				  <td>{$smarty.const.COUNTRY}</td>
				  <td><span class="form_dinatrea standard-input">
				    <select name="country" id="country">
						<option value="" >{$smarty.const.SELECT}</option>	
						{section name=countries loop=$arrCountry}
						<option value="{$arrCountry[countries].country}" {if $arrCountry[countries].country==$arrContactDetails.country}selected{/if}>{$arrCountry[countries].country}</option>
						{/section}
					</select>
				  </span></td>
			  </tr>
				<tr>
				  <td>{$smarty.const.CITY}</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="city" id="city" value="{$arrContactDetails.city}" />
				  </span></td>
			  </tr>
				<tr>
				  <td>{$smarty.const.AREA}<input name="address" id="address" type="hidden" value="{$arrContactDetails.address}" /><input name="customs" id="customs" type="hidden" value="{$arrContactDetails.customs}" /></td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="area" id="area" value="{$arrContactDetails.area}" />
				  </span></td>
			  </tr>
				
				<tr>
				  <td>{$smarty.const.OPTIONAL_ONE}</td>
				  <td><span class="form_dinatrea standard-input"><input type="text" name="optional_one" id="optional_one" value="{$arrContactDetails.optional_one}"></span></td>
			  </tr>
				<tr>
				  <td>{$smarty.const.OPTIONAL_TWO}</td>
				  <td><span class="form_dinatrea standard-input"><input type="text" name="optional_two" id="optional_two" value="{$arrContactDetails.optional_two}"></span></td>
			  </tr>
				</table>
				{if $action !='update'}
					
                   
                    <table style="margin-top:20px; margin-right:10px;" id="tblAddContact">
					<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
						<td class="row-1"><input type="checkbox" name="all" id="all" onclick="javascript: checkAllGroup('all','tblAddContact','defaultAddbook');">&nbsp;{$smarty.const.ALL}</td>
						<td class="row-2">{$smarty.const.GROUP_NAME}</td>
					
					</tr>
					<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
					{section name=addressbook loop=$arrAddressbook}
					<tr class="row-body">
						<td class="row-1"><input  type="checkbox" name="addbookId[]" {if $arrAddressbook[addressbook].addressbook=='Default'}id="defaultAddbook"
						checked
						onclick="javascript: keepCheckAddbook(this.id);"
						{else}id="addbookId" {/if} value="{$arrAddressbook[addressbook].id}"></td>
						<td class="row-2">{$arrAddressbook[addressbook].addressbook}</td>
					</tr>
					<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
					{/section}
					</table>
                    
				{/if}
				<table align="center" width="600" cellpadding="0" cellspacing="10" >
				<tr>
					<td></td>
					<td>
					<input type="submit" name="save" id="save" value="{$smarty.const.SAVE}" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
					<input type="hidden" name="bid" id="bid" value="{$arrContactDetails.addressbook_id}">
					<input type="hidden" name="cid" id="cid" value="{$cid}">

					</td>
				</tr>
				    
			</table>
            
             
					
	</form>
            <!-- middle body end-->
        </td>
    </tr>
</table>
  </div>
      <div class="clear"></div>
  </div>
{literal}
	<script type="text/javascript">
	<!--
	

	function validationAddContact(){

            var addbookError = 0;
            var mobError = 0;
            var emailError = 0;
            var errorFlag= 0;
			if($("#action").val()=='add'){
				if($("#defaultAddbook").attr("checked")==true){
					addbookError = 1;
				}else if(document.addContactForm.addbookId.length>1){
					for (var x = 0; x < document.addContactForm.addbookId.length; x++)
					{
						if(document.addContactForm.addbookId[x].checked == true){
						
							addbookError = 1;
						}
						
					}
				}else{
					
					if($("#defaultAddbook").attr("checked")==true){
						
						addbookError = 1;
					
					}else{

						if($("#addbookId").attr("checked")==true){

							addbookError = 1;
						}
					}

				}
				if(addbookError !=1){

					$("#msg").removeClass();
					$("#msg").addClass("error_msg");
					$("#msg").html("{/literal}{$smarty.const.SELECT_ANY_GROUP}{literal}");
					$("#msg").show();
					errorFlag=1; 
				
				}else{
					
					$("#msg").removeClass();
					$("#msg").html("");
					$("#msg").hide();
					
				}
			}
			
			 if(!mobileValidation()){
                    errorFlag=1; 
            
            }
			//if($("#fname").val()==""){
            //	$("#fnameDiv").html("{/literal}{$smarty.const.FIRST_NAME_REQUIRED}{literal}")
            //    errorFlag=1; 
           // }else{
			//	$("#fnameDiv").html('')
           // }
		   
		   if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   		if($("#day").val()==""){
					$("#birthdayDiv").html("Day of your birtday is required");
					errorFlag=1; 
				}else if($("#month").val()==""){
					$("#birthdayDiv").html("Month of your birthday is required");
					errorFlag=1; 
				}else if($("#year").val()==""){
					$("#birthdayDiv").html("Year of your birthday is required");
					errorFlag=1; 
				}
		   }else{
				$("#birthdayDiv").html("");
			}
		   

            if($("#email").val()!=""){
            
                if(!emailValidation()){
                    errorFlag=1; 
                }
            }
			
			if($("#mob_no2").val()!=""){
            
                if(!mobile2Validation()){
                    errorFlag=1; 
                }
            }
			
            if(errorFlag!=1){
            
                document.addContactForm.submit();
            }


	}
	
	
	
	$(function() {
		    $('#birth_date').datepicker({
			duration: '',
			showTime: false,
			time24h: false,
			constrainInput: false
		     });
		});
		
		
	function mobile2Validation(){

	var pattern =/^\+?[0-9]{8,12}$/;

	var mob =$("#mob_no2").val();
    if(mob.substr(0,1) == '7' || mob.substr(0,1) == '6'){
		mob_no_length = 9;
	}else if(mob.substr(0,1) == '0'){
       	mob_no_length = 10;
    }else if(mob.substr(0,1) == '+'){
        mob_no_length = 13;
    }else {
      	mob_no_length = 12;
    }
	
	if(mob==""){
		$("#mob2Div").html("");
		return 0;
	}else if (!mob.match(pattern)) {

		$("#mob2Div").html(msg_07);
		return 0;
	}else if(mob.length != mob_no_length){

		$("#mob2Div").html(msg_09_1+mob_no_length+msg_09_2);
		return 0;
	}else{

		$("#mob2Div").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}



}
        
	-->	
	</script>
	
{/literal}