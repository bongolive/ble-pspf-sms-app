<style>
.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 800px; /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
.spinnerDiv {
		position: absolute;
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 40%; 
	    left:50%; 
	    text-align:center;
	    z-index:1236;
	    overflow: auto;
	   /* width: 100px;  width of the spinner gif */
	   /* height: 102px; hight of the spinner gif +2px to fix IE8 issue */ 
	}
	.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 330px;
	border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}
</style>
<div id="spinner" class="spinner" style="display:none; vertical-align:middle;">
	<div id="spinner2" class="spinnerDiv">
	    <img id="img-spinner" src="{$BASE_URL_HTTP_ASSETS}images/ajax-loader.gif" alt="Loading"/>
	</div>
</div>
<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.VEN_DASHBOARD}</h1>
	
	
	<table border="1"> 
 <tr><td valign="top">
	
	
	
	
	
	<table align="center" border="0" cellpadding="10" cellspacing="10" style=" width:auto;">
  <tr>
    <td><a href="ven_addressbook.php"><img src="{$BASE_URL_HTTP_ASSETS}images/address-book.png" alt="Manage Address Book" width="165" height="113" border="0" /></a></td>
    <td><a href="ven_smspush.php"><img src="{$BASE_URL_HTTP_ASSETS}images/groupsms.png" alt="Group SMS" width="165" height="113" border="0"/></a></td>
    <td><a href="ven_smspush_quick.php"><img src="{$BASE_URL_HTTP_ASSETS}images/quicksms.png" alt="Quick SMS" width="165" height="113" border="0"/></a></td>
    <td><a href="ven_smspush_file.php"><img src="{$BASE_URL_HTTP_ASSETS}images/filesms.png" alt="File SMS" width="165" height="113" border="0" /></a></td>
  </tr>
</table>


<form action="" method="post" name="dashboard"> 
  
  
  
  {if $reseller==1}
 <table width="100%" border="0">
  <tr>
    <td>
		<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
 		<table align="center" width="225" cellpadding="0" cellspacing="10">
			<tr>
			  <td colspan="2" class="content-link_green">Purchase Requests:</td>
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td width="58" class="content-link_Black">Status</td>
			<td width="208" class="content-link_Black" align="right">Count</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Pending</td><td align="right">{$creditStat.pending}</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Allocated</td><td align="right">{$creditStat.allocated}</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Rejected</td><td align="right">{$creditStat.rejected}</td></tr>
		</table>
  </div>
  <div class="clear"></div>
</div> 
	</td>
    <td>
		<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
 		<table align="center" width="225" cellpadding="0" cellspacing="10">
			<tr><td colspan="2" class="content-link_green">Sender IDs:</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td class="content-link_Black">Status</td><td class="content-link_Black" align="right">Count</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Pending</td><td align="right">{$senderidStat.pending}</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Active</td><td align="right">{$senderidStat.active}</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Inactive</td><td align="right">{$senderidStat.inactive}</td></tr>
		</table>
  </div>
  <div class="clear"></div>
</div>
	</td>
  </tr>
</table>

 {/if}

  
  
  
  
  
  
  
  
  <table width="600" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><h2> SMS TRAFFIC </h2></td>
    </tr>
  </table>
  <div id="outerDiv" style="width:700px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
  <table width="603" border="0" style="margin-left:40px;">
  <tr>
    <td width="115">&nbsp;</td>
    <td width="478">&nbsp;</td>
  </tr>
  <tr>
    <td>Select Period : </td>
    <td><span class="form_dinatrea standard-input" style=" line-height:8px;">
	<select name="period" id="period" onchange="SendForm();">
      <option value="" {if $period==''} selected {/if}>Select</option>
      <option value="week" {if $period=='week'} selected {/if}>Past Seven Days</option>
      <option value="month" {if $period=='month'} selected {/if}>Current Month</option>
      <option value="year" {if $period=='year'} selected {/if}>Current Year</option>
    </select>
	</span>    </td>
  </tr>
</table>

{if $expression !=''}
<div id="chart" style="margin-top:20px; margin-bottom:20px;"></div>
</div>
{else}
	<div style="margin-top:20px; margin-bottom:20px; margin-left:40px;">
	{if $period=='year'}
	No Message Sent in current year
	{elseif $period=='month'}
	No Message Sent in current month
	{else}
	No Message Sent in past seven days
	{/if}
	</div>
	</div>
{/if}
</form>





</td>
<td valign="top">
<div id="reminderDiv" style=" margin-left:10px; margin-top:10px; width:220px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
<table width="220" border="0" cellpadding="5" cellspacing="10" style="background-color:#f8f8f8; border-radius: 9px 9px 9px 9px;">
  <tr>
    <td><strong>Group Name </strong></td>
    <td><strong>Last Sent Date </strong></td>
  </tr>
  <tr class="TR_spc"><td colspan="2" class="TD_spc"></td></tr>
  {if $arrSms}
		{section name=sms loop=$arrSms}	
  <tr>
    <td>{$arrSms[sms].addressbook}
	</td>
    <td><div align="center">{$arrSms[sms].sent_time|date_format:"%A %e %B %Y"}</div></td>
  </tr>
  <tr class="TR_spc"><td colspan="2" class="TD_spc"></td></tr>
  {/section}
  {else}
  <tr>
    <td colspan="2">No SMS Found</td>
    </tr>
  {/if}
</table>

</div> 
</td>
</table>

	
</div>
      <div class="clear"></div>
  </div>
{literal}
  <script>
function renderChart() {

var data = d3.csv.parse(d3.select('#csv').text());
var valueLabelWidth = 60; // space reserved for value labels (right)
var barHeight = 20; // height of one bar
var barLabelWidth = 100; // space reserved for bar labels
var barLabelPadding = 5; // padding between bar and bar labels (left)
var gridLabelHeight = 30; // space reserved for gridline labels
var gridChartOffset = 3; // space between start of grid and first bar
var maxBarWidth = 420; // width of the bar with the max value
 
// accessor functions 
var barLabel = function(d) { return d['Name']; };
var barValue = function(d) { return parseFloat(d['Population (mill)']); };
 
// scales
var yScale = d3.scale.ordinal().domain(d3.range(0, data.length)).rangeBands([0, data.length * barHeight]);
var y = function(d, i) { return yScale(i); };
var yText = function(d, i) { return y(d, i) + yScale.rangeBand() / 2; };
var x = d3.scale.linear().domain([0, d3.max(data, barValue)]).range([0, maxBarWidth]);
// svg container element
var chart = d3.select('#chart').append("svg")
  .attr('width', maxBarWidth + barLabelWidth + valueLabelWidth)
  .attr('height', gridLabelHeight + gridChartOffset + data.length * barHeight);
// grid line labels
var gridContainer = chart.append('g')
  .attr('transform', 'translate(' + barLabelWidth + ',' + gridLabelHeight + ')'); 
gridContainer.selectAll("text").data(x.ticks(5)).enter().append("text")
  .attr("x", x)
  .attr("dy", -3)
  .attr("text-anchor", "middle")
  .text(String);
// vertical grid lines
gridContainer.selectAll("line").data(x.ticks(5)).enter().append("line")
  .attr("x1", x)
  .attr("x2", x)
  .attr("y1", 0)
  .attr("y2", yScale.rangeExtent()[1] + gridChartOffset)
  .style("stroke", "#ccc");
// bar labels
var labelsContainer = chart.append('g')
  .attr('transform', 'translate(' + (barLabelWidth - barLabelPadding) + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
labelsContainer.selectAll('text').data(data).enter().append('text')
  .attr('y', yText)
  .attr('stroke', 'none')
  .attr('fill', 'black')
  .attr("dy", ".35em") // vertical-align: middle
  .attr('text-anchor', 'end')
  .text(barLabel);
// bars
var barsContainer = chart.append('g')
  .attr('transform', 'translate(' + barLabelWidth + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
barsContainer.selectAll("rect").data(data).enter().append("rect")
  .attr('y', y)
  .attr('height', yScale.rangeBand())
  .attr('width', function(d) { return x(barValue(d)); })
  .attr('stroke', 'white')
  .attr('fill', '#993399');
// bar value labels
barsContainer.selectAll("text").data(data).enter().append("text")
  .attr("x", function(d) { return x(barValue(d)); })
  .attr("y", yText)
  .attr("dx", 3) // padding-left
  .attr("dy", ".35em") // vertical-align: middle
  .attr("text-anchor", "start") // text-align: right
  .attr("fill", "black")
  .attr("stroke", "none")
  .text(function(d) { return d3.round(barValue(d), 2); });
// start line
barsContainer.append("line")
  .attr("y1", -gridChartOffset)
  .attr("y2", yScale.rangeExtent()[1] + gridChartOffset)
  .style("stroke", "#000");
  
{/literal}{if $expression!=''}{literal}
barsContainer.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", 240)
    .attr("y", -20)
    .text("Volume of sms");
	
{/literal}{/if}{literal}
 
 //svg.append("text")
  //  .attr("class", "y label")
  //  .attr("text-anchor", "end")
  //  .attr("y", 6)
  //  .attr("dy", ".75em")
  //  .attr("transform", "rotate(-90)")
  //  .text("life expectancy (years)");

}
    </script>
    <script id="csv" type="text/csv">Name,Population (mill)
{/literal}{$expression}{literal}</script>
    <script>
	function SendForm(){
	$('#spinner').show();
	document.dashboard.submit();
}
	renderChart();</script>
{/literal}