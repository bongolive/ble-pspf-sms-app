<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_2}</h1>
<table align="center" cellpadding="0" cellspacing="0">
	<tr>
       <td></td> 
        <td>
           <div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
            <form action="" method="POST" name="senderidForm" onsubmit="return MM_validateForm();">
                <table width="93%" cellspacing="10">
					 <tr>
                        <td width="152">Broadcaster name:</td>
                      <td width="408"><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span><span id="vendorNameDiv" class="form_error" style="width:100%"></span></td>
                    </tr>
					
                    <tr>
                        <td width="152" valign="top">{$smarty.const.REQUEST_NEW}: </td>
                      <td width="408"><span class="form_dinatrea standard-input">
                      <input type="text" name="senderid" id="senderid" value="{$senderid}"></span><span id="senderidDiv" class="form_error" style="width:100%"></span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input name="send" type="submit" class="editbtn" id="send" value="{$smarty.const.SUBMIT}">
                            <input type="hidden" name="action" id="action" value="add">
                        </td>
                    </tr>
                    
              </table>
    </form>
             <br><br>
            <table align="center" width="600" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
                <tr>
                    <td>&nbsp;</td>
                    <td class="content-link_Black">{$smarty.const.SENDER_NAME}</td>
                    <td class="content-link_Black">{$smarty.const.STATUS}</td>
                </tr>
                {section name=senderid loop=$arrSenderid}
				<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
                <tr>
                    <td>{$smarty.section.senderid.iteration}</td>
                    <td>{$arrSenderid[senderid].senderid}</td>
                    <td>{$arrSenderid[senderid].status}</td>
                </tr>
                {/section}
            </table>

            <!-- middle body end-->
        </td>
    </tr>
</table>
  </div>
      <div class="clear"></div>
  </div>
  {literal}

<script type="text/javascript">
//<!--
function MM_validateForm() { //v4.0
  	checkMobilePhone();
	var v = $("#senderid").val();
	var senderregex=/^\+?[A-Za-z0-9 _]+$/;
	var phoneregex=/^\+?[0-9]{9,13}$/;
	
	if($("#vendorName").val()==''){
		$("#vendorNameDiv").html("Please select Broadcaster name");
		return false;
	}else if(v==0){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_1}{literal}");
		return false;
	}else if(! v.match(senderregex)){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
		return false;
	}else if( ! isNaN(v)){
		if(v.length >14){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_6}{literal}");
			return false;
		}else if(! v.match(phoneregex)){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
			return false;
		}else{
			$("#senderidDiv").html("");
		}
	}else{
		if(v.length >11){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_7}{literal}");
			return false;
		}else{
			$("#vendorNameDiv").html("");
			$("#senderidDiv").html("");
		}
	}
}

$('#senderid').keyup(function() {
	var v = $("#senderid").val();
	var senderregex=/^\+?[A-Za-z0-9 _]+$/;
	var phoneregex=/^\+?[0-9]{9,14}$/;
	if(! v.match(senderregex)){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
	}else if( ! isNaN(v)){
		if(v.length >14){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_6}{literal}");
		}else if(! v.match(phoneregex)){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
		}else{
			$("#senderidDiv").html("");
		}
	}else{
		if(v.length >11){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_7}{literal}");
		}else{
			$("#senderidDiv").html("");
		}
	}
});
	
function checkMobilePhone(){

 	 var phone=jQuery.trim($("#senderid").val());

    if (phone.substr(0,1) == '+'){

      var newPhone = phone.substr(1);

    }else{

      var newPhone = phone;

    }
	var validphone=jQuery.trim(newPhone);
	document.senderidForm.senderid.value=validphone;
}
 //-->
 </script>
{/literal}
