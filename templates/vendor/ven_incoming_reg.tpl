<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 910px;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 910px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 200px;
	text-align: left;
		}


.row-2{
 width:150px;
 text-align: left;
}
.row-3{
 width:100px;
 text-align: left;
}

.row-4{
 width:50px;
 text-align:left;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 950px; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}

</style>
<div id="container">
    <h1>Incoming Registration Messages </h1>
	<div style="width:100%; margin:0 auto;">
	<div id="errorDiv"></div>
	{if $action=='' || $action=='search'}
	<form name="creditRequest" action="{$selfUrl}" method="GET">
		<table cellspacing="10" width="600">	
			<tr>
				<td></td>
				<td>				</td>				
			</tr>
			
			<tr>
			  <td><strong>Status:</strong></td>
			  <td><span class="form_dinatrea standard-input">
			  <select name="status" id="status">
			    <option value="" {if $status==''}selected{/if}>All</option>
			    <option value="Received" {if $status=='Received'}selected{/if}>Received</option>
				<option value="retry" {if $status=='retry'}selected{/if}>Retry</option>
			    <option value="response to be sent" {if $status=='response to be sent'}selected{/if}>Response to be sent</option>
			    <option value="response sent" {if $status=='response sent'}selected{/if}>Response sent</option>
			    </select></span>
			  </td>
		  </tr>
		  <tr>
			  <td><strong>Temporaly Checkno:</strong></td>
			  <td><span class="form_dinatrea standard-input">
				<input type="text" name="checkno" id="checkno" value="{$checkno}"></span>
			  </td>
		  </tr>
			<tr>
				<td><strong>Phone Number:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="mob_no" id="mob_no" value="{$mob_no}"></span>				</td>
			</tr>
			<tr>
				<td valign="top"><strong>SMS Message:</strong></td>
				<td><span class="form_dinatrea standard-input" style="width:200px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="2">{$textMessage}</textarea></span>				</td>
			</tr>
			<tr>
				<td><strong>Start Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>				</td>
			</tr>
			<tr>
				<td><strong>End Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>				</td>
			</tr>
			<tr>
				<td></td>
			  <td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="search">
					<input type="hidden" name="ajaxUrl" id="ajaxUrl" value="{$ajaxUrl}"></td>
			</tr>
		</table>
	</form>	
	 <div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
      <!--  <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	<table style="margin-top:20px; margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">		
		<td class="row-1">Received SMS</td>
		<td class="row-2">Mobile</td>
        <td class="row-3">Received Date</td>
		<td class="row-1">Response Sent</td>
		<td class="row-3">Sent Date</td>
        <td class="row-4">No of Retry</td>
		<td class="row-3">SMS Status</td>
	</tr>
	{if $arrSms}
		{section name=sms loop=$arrSms}	
		<tr class="row-body">	
			<td class="row-1">{$arrSms[sms].message|wordwrap:20:"<br />\n"}</td>
			<td class="row-2">{$arrSms[sms].phone}</td>
            <td class="row-3">{$arrSms[sms].received}</td>
			<td class="row-">{$arrSms[sms].outbound_response|wordwrap:20:"<br />\n"}</td>
			<td class="row-3">{$arrSms[sms].sent_date}</td>
			<td class="row-4">{$arrSms[sms].num_status}</td>
			<td class="row-3">{$arrSms[sms].status}</td>
		</tr>
		{/section}
		<tr style="background-color:#f8f8f8;">
			<td align="right" colspan="7" class="content-link" style="padding:5px 0;">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}</td>
		</tr>
	{else}
		</tr>
		<tr style="background-color:#f8f8f8;">
			<td colspan="7" align="center"  class="content-link" valign="middle" style="padding:5px;">&nbsp;</td>
		</tr>		
	{/if}
	</table>
    </div>
          <div class="clear"></div>
     </div>
</div>
{/if}
</div>

<script type="text/javascript">

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});

		function selectBroadcatedrType(brdType){
			
			if(brdType==3){
				
				$("#vendorName").val("");
				$("#mob_no").val("");
				$("#vendorName").attr("disabled",true);
				$("#mob_no").attr("disabled",true);
			
			}else{
				
				$("#vendorName").removeAttr("disabled");
				$("#mob_no").removeAttr("disabled");
				getVendorByUserType(brdType,$("#ajaxUrl").val());
			}



		}
		
     </script>
