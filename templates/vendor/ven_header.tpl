<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>

	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" /> -->



	<title>{$smarty.const.VEN_HEADER_TITLE}</title>



	<link href="{$BASE_URL_HTTP_ASSETS}css/style.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/fader.css" type="text/css" media="screen" />

	<link rel="stylesheet" type="text/css" href="{$BASE_URL_HTTP_ASSETS}css/ddsmoothmenu.css" />



	<script type="text/javascript">

		var assetPath = '{$BASE_URL_HTTP_ASSETS}';

	</script>
	
	<script src="{$BASE_URL_HTTP_ASSETS}js/jquery-1.4.2.min.js" type="text/javascript"></script>

	<script src="{$BASE_URL_HTTP_ASSETS}js/png-transparent.js" language="javascript" type="text/javascript"></script>

	<script src="{$BASE_URL_HTTP_ASSETS}js/jquery.anythingfader.js" type="text/javascript"></script>
	
	<link type="text/css" rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/graph.css">
	
	<script src="{$BASE_URL_HTTP_ASSETS}js/d3.v2.js"></script>
	

	<script type="text/javascript">

		/*

			$(document).ready(function(){

				//Default Action

				$(".tab_content").hide(); //Hide all content

				$("ul.tabs li:first").addClass("active").show(); //Activate first tab

				$(".tab_content:first").show(); //Show first tab content



				//On Click Event

				$("ul.tabs li").click(function(){

					$("ul.tabs li").removeClass("active"); //Remove any "active" class

					$(this).addClass("active"); //Add "active" class to selected tab

					$(".tab_content").hide(); //Hide all tab content

					var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

					$(activeTab).fadeIn(); //Fade in the active content

					return false;

				});

			});

		*/

		$(document).ready(function(){

			//On Click Event

			$("ul.tabs li").click(function(){

				$("ul.tabs li").removeClass("active"); //Remove any "active" class

				$(this).addClass("active"); //Add "active" class to selected tab

				$(".tab_content").hide(); //Hide all tab content

				var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

				$(activeTab).fadeIn(); //Fade in the active content

				return false;

			});

		});

	</script>

	<script type="text/javascript">

		function formatText(index, panel){

			return index + "";

		}



		$(function (){

			$('.anythingFader').anythingFader({

				autoPlay: true,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.

				delay: 8000,                    // How long between slide transitions in AutoPlay mode

				startStopped: false,            // If autoPlay is on, this can force it to start stopped

				animationTime: 1000,             // How long the slide transition takes

				hashTags: true,                 // Should links change the hashtag in the URL?

				buildNavigation: true,          // If true, builds and list of anchor links to link to each slide

				pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover

				startText: "",                // Start text

				stopText: "",               // Stop text

				navigationFormatter: formatText   // Details at the top of the file on this use (advanced use)

			});



			$("#slide-jump").click(function(){

				$('.anythingFader').anythingFader(6);

			});

		});

	</script>


	<script type="text/javascript">

		// ddeclaration of image path global for javascript

		var imagePath = '{$BASE_URL_HTTP_ASSETS}images/';

	</script>



	<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions_lang_{$smarty.session.lang}.js" type="text/javascript"></script>

	<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions.js" type="text/javascript"></script>

{if ($css)}

    	<link type="text/css" href="{$BASE_URL_HTTP_ASSETS}css/{$css}" rel="stylesheet" />

{/if}
	<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/jquery-ui-1.7.2.custom.min.js"></script>
	<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />

	<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/timepicker.js"></script>

{if ($js)}

<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/{$js}"></script>

{/if}



	<!--[if IE]>

		<style type="text/css" media="all">@import "{$BASE_URL_HTTP_ASSETS}css/ie6_tab.css";</style>

	<![endif]-->
<link rel="icon" type="image/ico" href="favicon.ico">
</head>



<body>

<div id="container-top">

  <div class="logo"><a href="index.php"><img alt="PSPFlogo" src="{$BASE_URL_HTTP_ASSETS}images/logo.png" /></a></div>

  <div class="nav-sign-in">

  	{include file="langs.tpl"}

	{if $login}

		<ul>

		  <li id="user-id">{$welcome} {$username},</li>

		  <li></li>

		  <li><a href="{$logout_url}">{$logout}</a></li>

		</ul>

	{/if}

  </div>

  <div id="subpage_main-menu">

  </div>

  <div id="main-info">

    <table width="100%" border="0" cellspacing="0">

      <tr>

        <td class="main-info-title01">{$smarty.const.SMS_BALANCE}:</td>

        <td class="main-info-sms-balance"><div id="myBalance">{$crdedit[0].credit_balance}</div></td>

        <td class="main-info-contacts">{$smarty.const.CONTACTS}</td>

	<td class="main-info-sms-balance">{$contacts}</td>

      </tr>

    </table>

  </div>

  <div class="clear"></div>

  <p id="user-role">{$smarty.const.VEN_BROADCASTER}</p>

  <div class="topmenu">

    <ul class="primary">

      <li {if $curPage=='ven_dashboard.php'}
			class="active-topmenu current item-eight-a"

			{else}  class="item-eight"

			{/if} style="width:90px;">

			<a href="ven_dashboard.php">{$smarty.const.VEN_DASHBOARD}</a>

        <ul class="secondary">

		  <li><a href=""></a></li>

          <li><a href=""></a></li>

          <li><a href=""></a></li>

		   <!-- <li><a href="dnld_unsaved_contact.php">Download Unsaved Contacts</a></li> -->

        </ul>

      </li>
	  <li {if $curPage=='ven_addressbook.php'

			|| $curPage=='ven_addbook_details.php'

			|| $curPage=='ven_addcontact.php'

			|| $curPage=='ven_import_contacts.php'

			|| $curPage=='dnld_unsaved_contact.php'
			|| $curPage=='ven_contacts_analytics.php'}

			class="active-topmenu  item-nine-a current"

			{else}  class="item-nine"

			{/if} style="width:150px;">

			<a href="ven_addressbook.php">{$smarty.const.VEN_MENUTOP_DASHBOARD_4}</a>

        <ul class="secondary">

		  <li><a href="ven_addressbook.php">{$smarty.const.VEN_MENUTOP_DASHBOARD_1}</a></li>

          <li><a href="ven_addcontact.php">{$smarty.const.VEN_MENUTOP_DASHBOARD_2}</a></li>

          <li><a href="ven_import_contacts.php">{$smarty.const.VEN_MENUTOP_DASHBOARD_3}</a></li>
		  <li><a href="ven_contacts_analytics.php">{$smarty.const.VEN_MENUTOP_DASHBOARD_5}</a></li>

		   <!-- <li><a href="dnld_unsaved_contact.php">Download Unsaved Contacts</a></li> -->

        </ul>

      </li>

      <li {if $curPage=='ven_senderid.php' || $curPage == 'ven_word_keys.php' || $curPage == 'ven_credit_requested.php' || $curPage == 'ven_credit_purchage.php' || $curPage=='ven_smspush.php' || $curPage=='ven_sms_template.php' || $curPage=='ven_smspush_quick.php' || $curPage=='ven_smspush_file.php' || $curPage=='ven_reminders.php'}

			class="active-topmenu current item-two-a"

		  {else}

			class="item-two"

		  {/if}  style="width:110px;"><a href="ven_smspush.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS}</a>

        <ul class="secondary">

			<li><a href="ven_smspush.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_1}</a></li>
			
			<li><a href="ven_smspush_quick.php">{$smarty.const.COMPOSE_QUICK_SMS}</a></li>
			
			<li><a href="ven_smspush_file.php">{$smarty.const.COMPOSE_FILE_SMS}</a></li>

		 	<li><a href="ven_senderid.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_2}</a></li>

            <li><a href='ven_credit_purchage.php'>{$smarty.const.VEN_MENUTOP_PURCHASE_SMS_1}</a></li>
			{if $parent_ven_id==''}
        	<li><a href="ven_word_keys.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_6}</a></li>
			{/if}
			<li><a href="ven_sms_template.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_8}</a></li>

			<li><a href="ven_reminders.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_10}</a></li>
        </ul>

      </li>

	  
	{if $parent_ven_id=='' || $access_registrations=='1'}
	  

	  <li {if $curPage=='ven_incoming_reg.php' || $curPage=='ven_incoming_reg_log_report.php'}class="active-topmenu current item-three-a" {else} class="item-three"{/if style="width:120px;"}><a href="ven_incoming_reg.php">{$smarty.const.MEMBER_REGISTRATION}</a>

        <ul class="secondary">
		 <li><a href="ven_incoming_reg.php">{$smarty.const.VEN_MENUTOP_INCOMING_REG}</a></li>
         <li><a href="ven_incoming_reg_log_report.php">{$smarty.const.VEN_MENUTOP_INCOMING_REG_LOG}</a></li>
          <li></li>

        </ul>

      </li>
      
      {/if}

		<li {if $curPage=='ven_broadcast.php' || $curPage=='ven_log_report.php' || $curPage=='ven_incoming_log_report.php' || $curPage=='ven_incoming.php'}class="active-topmenu current item-one-a" {else} class="item-one"{/if} style="width:90px;"><a href="ven_incoming.php">{$smarty.const.VEN_MENUTOP_REPORT}</a>

        <ul class="secondary">
          	{if $parent_ven_id=='' || $access_incoming=='1'}
            <li><a href="ven_incoming.php">{$smarty.const.VEN_MENUTOP_INCOMING}</a></li>
			<li><a href="ven_incoming_log_report.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_7}</a></li>
            {/if}
			<li><a href="ven_broadcast.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_3}</a></li>
			<li><a href="ven_log_report.php">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_4}</a></li>
        </ul>

      </li>
      
      {if $reseller==1}
	  <li {if $curPage=='ven_manage_sub_vendor.php' || $curPage == 'ven_new_reseller.php' || $curPage == 'ven_manage_broadcast.php'|| $curPage=='ven_manage_broadcast_log.php' || $curPage=='ven_credits_schemes.php' || $curPage=='ven_manage_credits_request.php' || $curPage=='ven_manage_senderid.php' || $curPage=='ven_add_credit.php' || $curPage=='ven_add_senderid.php'}

			class="active-topmenu current item-two-a"

		  {else}

			class="item-two"

		  {/if} style="width:80px;"><a href="ven_manage_sub_vendor.php">Users</a>

        <ul class="secondary">
			<li><a href="ven_manage_sub_vendor.php">Manage Client </a></li>
			<li><a href="ven_manage_credits_request.php">Credit</a></li>
			<!-- <li><a href="ven_manage_credits_balances.php">Credit Balance</a></li> -->
			<li><a href="ven_manage_senderid.php">Sende rNames</a></li>
		 	<li><a href="ven_manage_broadcast.php">History Report</a></li>
            <li><a href="ven_manage_broadcast_log.php">SMS Log Report</a></li>
			<li><a href="ven_credits_schemes.php">Credit Scheme</a></li>
        </ul>

      </li>
		{/if}
      
      

	   <li {if $curPage=='ven_profile.php' || $curPage=='ven_changepassword.php'}class="active-topmenu current item-five-a" {else} class="item-five"{/if}  style="width:80px;"><a href="ven_profile.php" style="margin-left:0px;">{$smarty.const.VEN_MENUTOP_PROFILE}</a>

       <ul class="secondary">

          <li><a href="ven_profile.php">{$smarty.const.VEN_MENUTOP_PROFILE}</a></li>

          <li><a href="ven_changepassword.php">{$smarty.const.VEN_MENUTOP_CHANGE_PASSWORD}</a></li>

          <li></li>

        </ul>

      </li>

	  <li {if $curPage=='help.php'}class="active-topmenu current item-four-a" {else} class="item-four"{/if}  style="width:80px;">

	  <a href="help.php"><a href="help.php">{$smarty.const.VEN_MENUTOP_HELP}</a>

        <ul class="secondary">

          <li></li>

          <li></li>

          <li></li>

        </ul>

      </li>



    </ul>

  </div>



  <div class="clear"></div>

</div>



