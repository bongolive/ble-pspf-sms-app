<!--header start-->
<!--header end-->
<style type="text/css">
<!--
.hidden { display: none; }
.unhidden { display: block; }

.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 130% /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 250px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}-->
</style>
<script type="text/javascript">
		$(document).ready(function ()
   {
      $("#btnShowSimple").click(function (e)
      {
         ShowDialog(false);
         e.preventDefault();
      });

      $("#btnShowModal").click(function (e)
      {
         ShowDialog(true);
         e.preventDefault();
      });

      $("#btnClose").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
         
		 var errorFlag=0;
		 //if($("#repeat").val()==''){
		 //	$("#repeatErrorDiv").html("Please Select This Field");
		//	errorFlag=1;
		 //}
		 
		  if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
		   	if($("#dayStart").val()==""){
				$("#startDateDiv").html("Please select Day to start");
				errorFlag=1; 
			}else if($("#monthStart").val()==""){
				$("#startDateDiv").html("Please select Month to start");
				errorFlag=1; 
			}else if($("#yearStart").val()==""){
				$("#startDateDiv").html("Please Select Year to start");
				errorFlag=1;
			}else{
				var d1=new Date();
				var d2=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				var d3=new Date(d2);
				if(d3<d1){
					$("#startDateDiv").html("Start Date Should be greater than now");
					errorFlag=1;
				}else{
					$("#scheduleDate").val(d2);
					$("#startDateDiv").html("");
					errorFlag=0;
				}
			}
		 }
		 
		 
		 
		 if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   	if($("#day").val()==""){
				$("#birthdayDiv").html("Please select Day");
				errorFlag=1; 
			}else if($("#month").val()==""){
				$("#birthdayDiv").html("Please select Month");
				errorFlag=1; 
			}else if($("#year").val()==""){
				$("#birthdayDiv").html("Please Select Year");
				errorFlag=1;
			}else{
				var d4=new Date();
				var d5=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				var d6=new Date(d5);
				var startDate=$("#scheduleDate").val();
				var d7=new Date(startDate);
				if(d6<d4){
					$("#birthdayDiv").html("End Date Should be greater than now");
					errorFlag=1;
				}else if(d6<d7){
					$("#birthdayDiv").html("End Date Should be greater than Start Date");
					errorFlag=1;
				}else{
						$("#birthdayDiv").html("");
						errorFlag=0;
				}
			}
		 }
		 
		
		 
		 
		 if(errorFlag==1){
		 	return false;
		 }else{
		 	$("#repeatErrorDiv").html("");
			$("#birthdayDiv").html("");
			$("#startDateDiv").html("");
			
			//Assign new data to hidden controls on a page
			$("#repeat2").val($("#repeat").val());
			$("#repeat_every2").val($("#repeat_every").val());
			var start_date='';
			
			if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
				start_date=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				
				if(document.getElementById("advOptionDiv").style.display=='none'){
					$("#advOptionDiv").show();
					$("#scheduleDate").attr("disabled",false);
					$("#advOptionTd").html('- '+msg_26);
				}
				$("#scheduleDate").val(start_date);
				
		 	}else{
				start_date="Current Time.";	
			}
			
			if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
				var end_date=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				$("#end_date").val(end_date);
		 	}
			
			var expression='';
			if($("#repeat").val()=='WEEK'){
				var rpDays = document.smsPushForm.elements["repeat_days[]"];
				for(i=0;i<rpDays.length;i++)
				{
 					if(document.smsPushForm.repeat_days[i].checked){
						expression = expression+document.smsPushForm.repeat_days[i].value+',';
					}
				}
				
				if(expression.length >0){
				 expression = expression.substring(0, expression.length - 1);
				 $("#repeat_days2").val(expression);
				}
			}else if($("#repeat").val()=='MONTH'){
				expression='Monthly on a day.';
			}else if($("#repeat").val()=='YEAR'){
				expression='Yearly on a day.';
			}else if($("#repeat").val()=='DAY'){
				expression='Daily.';
			}
			
			//Writting Summary for User preview
			if($("#repeat").val()==''){
				var summary= 'Never Repeat.';
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}
		 }

		 
		 $("#repeatSummary").html(summary);
		 
		 //var brand = $("#brands input:radio:checked").val();
         //$("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

   });

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   }
   
    function pickRepeat(){
   	if($("#repeat").val()=="DAY"){
		$("#repeatDiv").html("Day");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="WEEK"){
		$("#repeatDiv").html("Week");
		$("#repeatDaysRow").show();
		$("#repeat_every").val(1);
		$("#repeat_every").attr('disabled', true);
	}else if($("#repeat").val()=="MONTH"){
		$("#repeatDiv").html("Month");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="YEAR"){
		$("#repeatDiv").html("Year");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else{
		$("#repeatDiv").html("");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}
	
   }
   
   
   function changeNumberOfRepeat(){
   		if($("#repeat_every").val()>1){
			if($("#repeat").val()=="DAY"){
				$("#repeatDiv").html("Days");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="WEEK"){
				$("#repeatDiv").html("Weeks");
				document.getElementById(repeatDaysRow).style.display="block";
			}else if($("#repeat").val()=="MONTH"){
				$("#repeatDiv").html("Months");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="YEAR"){
				$("#repeatDiv").html("Years");
				document.getElementById(repeatDaysRow).style.display="none";
			}else{
				$("#repeatDiv").html("");
				document.getElementById(repeatDaysRow).style.display="none";
			}
		}
   }	
	</script>


<div id="spinner" class="spinner" style="display:none;">
	    <img id="img-spinner" src="{$BASE_URL_HTTP_ASSETS}images/ajax-loader.gif" alt="Loading" style="margin-top:25%"/>
</div>
<div id="container">
<form name="smsPushForm" id="smsPushForm" method="post" action="" enctype="multipart/form-data" onsubmit="return validatSmspushVen();">




 <div id="overlay" class="web_dialog_overlay"></div>
   
<div id="dialog" class="web_dialog">
	<div id="output"></div>
   <div style="text-align:right; margin-right:10px; margin-top:10px; display:inline; float:right;"><a href="#" id="btnClose">Close</a></div>
   <div style="float:left; display:inline;"><h2 style="margin-left:20px;">Advanced Settings</h2></div>
   <table width="450" border="0" align="center" cellpadding="10" cellspacing="10" style="margin-left:20px;">
  <tr>
    <td valign="top" width="90">Repeat</td>
    <td width="360"><div>
	
	<select name="repeat" id="repeat" onchange="pickRepeat();"style="border:2px solid #ccc;" >
		<option value="">Never</option>
		<option value="DAY">Daily</option>
		<option value="WEEK">Weekly</option>
		<option value="MONTH">Monthly</option>
		<option value="YEAR">Yearly</option>
	</select>

	</div> <span id="repeatErrorDiv" class="form_error"></span></td>
  </tr>
  <tr>
    <td valign="top">Repeat Every</td>
    <td valign="bottom">
	<select id="repeat_every" name="repeat_every" onchange="changeNumberOfRepeat();" style="border:2px solid #ccc;" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
	</select>
	<label id="repeatDiv"></label>
	
	</td>
  </tr>
  <tr id="repeatDaysRow" style="display:none;">
    <td valign="top">Repeat Days</td>
    <td>
		<div>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Sunday" />S</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Monday" />M</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Tuesday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Wednesday" />W</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Thursday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Friday" />F</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Saturday" />S</label>
		</div>
	</td>
  </tr>
  <tr>
  	<td>Start Date</td>
	<td>
		<select name="dayStart" id="dayStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							{section name=arrDay2 loop=31} 
							<option value="{$smarty.section.arrDay2.iteration}" {if $smarty.section.arrDay2.iteration==$day} selected{/if}>{$smarty.section.arrDay2.iteration}</option>
							{/section}
						</select>

				  
				  
				    	 <select name="monthStart" id="monthStart" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" {if $month==1} selected{/if}>Jan</option>
							<option value="2" {if $month==2} selected{/if}>Feb</option>
							<option value="3" {if $month==3} selected{/if}>Mar</option>
							<option value="4" {if $month==4} selected{/if}>Apr</option>
							<option value="5" {if $month==5} selected{/if}>May</option>
							<option value="6" {if $month==6} selected{/if}>Jun</option>
							<option value="7" {if $month==7} selected{/if}>Jul</option>
							<option value="8" {if $month==8} selected{/if}>Aug</option>
							<option value="9" {if $month==9} selected{/if}>Sep</option>
							<option value="10" {if $month==10} selected{/if}>Oct</option>
							<option value="11" {if $month==11} selected{/if}>Nov</option>
							<option value="12" {if $month==12} selected{/if}>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="yearStart" id="yearStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							{section name=arrYear2 loop=10} 
							<option value="{($currentYear - $smarty.section.arrYear2.iteration)}" {if $year==($currentYear - $smarty.section.arrYear2.iteration)} selected{/if}>{($currentYear - $smarty.section.arrYear2.iteration)}</option>
							{/section}
						</select>
						 
					
						 <select name="hourStart" id="hourStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							{section name=fooStart start=0 loop=24 step=1} 
								<option value="{$smarty.section.fooStart.index}">{$smarty.section.fooStart.index}</option>
							{/section} 
						 </select>
					
					 
					 
						<!-- <select name="minutesStart" id="minutesStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							{section name=foo2Start start=0 loop=60 step=1}
								<option value="{$smarty.section.foo2Start.index}">{$smarty.section.foo2Start.index}</option>
							{/section}
						 </select> -->
					 		 
						 
						 <span id="startDateDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td valign="top">End Date</td>
    <td>
	
	
				    	 <select name="day" id="day" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							{section name=arrDay loop=31} 
							<option value="{$smarty.section.arrDay.iteration}" {if $smarty.section.arrDay.iteration==$day} selected{/if}>{$smarty.section.arrDay.iteration}</option>
							{/section}
						</select>

				  
				  
				    	 <select name="month" id="month" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" {if $month==1} selected{/if}>Jan</option>
							<option value="2" {if $month==2} selected{/if}>Feb</option>
							<option value="3" {if $month==3} selected{/if}>Mar</option>
							<option value="4" {if $month==4} selected{/if}>Apr</option>
							<option value="5" {if $month==5} selected{/if}>May</option>
							<option value="6" {if $month==6} selected{/if}>Jun</option>
							<option value="7" {if $month==7} selected{/if}>Jul</option>
							<option value="8" {if $month==8} selected{/if}>Aug</option>
							<option value="9" {if $month==9} selected{/if}>Sep</option>
							<option value="10" {if $month==10} selected{/if}>Oct</option>
							<option value="11" {if $month==11} selected{/if}>Nov</option>
							<option value="12" {if $month==12} selected{/if}>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="year" id="year" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							{section name=arrYear loop=10} 
							<option value="{($currentYear - $smarty.section.arrYear.iteration)}" {if $year==($currentYear - $smarty.section.arrYear.iteration)} selected{/if}>{($currentYear - $smarty.section.arrYear.iteration)}</option>
							{/section}
						</select>
						 
					
						 <select name="hour" id="hour" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							{section name=foo start=0 loop=24 step=1} 
								<option value="{$smarty.section.foo.index}">{$smarty.section.foo.index}</option>
							{/section} 
						 </select>
					
					 
					 
						<!-- <select name="minutes" id="minutes" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							{section name=foo2 start=0 loop=60 step=1}
								<option value="{$smarty.section.foo2.index}">{$smarty.section.foo2.index}</option>
							{/section}
						 </select> -->
					 		 
						 
						 <span id="birthdayDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input id="btnSubmit" type="button" value="Ok" style="border:0px solid #93278F; width:100px; height:31px; background:url(templates/assets/images/btn_bg.png) no-repeat left; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif; font-size:1.2em; vertical-align:text-top; text-align:center; color:#FFFFFF;" /></td>
  </tr>
</table>

</div>







  <div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.COMPOSE_QUICK_SMS}</h1>
	<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
	{if $msg} style="display:block;" {else} style="display:none;" {/if}> {$msg}	</div>
  
  {if !$smsconf}
  
  <table border="0">
  <tr>
    <td valign="top"><div style=" margin-top:10px; width:600px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
	<table width="600" border="0" cellpadding="0" cellspacing="10" style="background:#f6f6f6; border-radius: 9px 9px 9px 9px;">
    <tr>
      <td width="200" valign="top"><strong>{$smarty.const.ENTER_NUMBERS}:</strong>	  </td>
      <td width="350"><div style="font-size:10px">{$smarty.const.RECIPIENT_DISCRIPTION}</div>
          <textarea name="recipients" id="recipients" rows="5"  class="textarea">{$arrRecipients}</textarea>
          <span id="recipientsDiv" class="form_error" style="width:350px"></span> {if $MobError==1}
        <script type="text/javascript">
		$("#recipientsDiv").html("Recepient should be a mobile number");
	</script>
        {/if}
        <div style="width:350px">
          <div id="recepientCount" style="margin-left:10px; margin-top:0">{$smarty.const.COUNTER}: 0/50</div>
        </div></td>
    </tr>
    <tr>
      <td width="200" valign="top"><strong>{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_8}</strong></td>
      <td width="350"><div id="smstemplate"  style="width:40px"><span class="form_dinatrea standard-input">
          <select name="templatesms" id="smstemplate" onchange="addsmstemplate();" style="margin-right:20px;">
            <option value="">{$smarty.const.SELECT}</option>
            
					      
				{section name=sms loop=$arrsmstemplate}
				
					      
            <option value="{$arrsmstemplate[sms].message}">{$arrsmstemplate[sms].sms_title}</option>
            
					      
				{/section}
			  
				        
          </select>
        </span></div>
      <!--  <a href="JavaScript:newPopup('ven_sms_template.php');" style=" margin-left:20px;">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_9}</a> --> </td>
    </tr>
    <tr>
      <td width="200" valign="top"><strong>{$smarty.const.MESSAGE_BODY}:</strong></td>
      <td width="350"><textarea name="textMessage"  rows="4" class="textarea" id="textMessage">{$textMessage}</textarea>
          <span id="textMessageDiv" class="form_error" style="width:350px"></span>
		  <div style="float:left;">Msgs. / Chars. Remaining<span id="charCount" style="margin-left:10px">( 0 ) 0</span></div>
        <div style=" margin-left:60px; float:left;" id="messageCount">&nbsp;</div></td>
    </tr>
		<tr><td><strong>Message Type :</strong> </td>
			<td><input type="radio" name="m_type" checked value="m_text">Text
				<input type="radio" name="m_type" value="m_unicode">Unicode</td></tr>
		<tr>
    <tr>
      <td width="200" valign="top"><strong>{$smarty.const.SENDER_NAME}:
        <input name="repeat2" type="hidden" id="repeat2" value="" />
        <input name="repeat_every2" type="hidden" id="repeat_every2" value="" />
        <input name="repeat_days2" type="hidden" id="repeat_days2" value="" />
        <input name="end_date" type="hidden" id="end_date" value="" />
        <input name="startDate" type="hidden" id="startDate" value="" />
      </strong></td>
      <td align="left" valign="bottom">
          <div align="left" style="width:40px;"><span class="form_dinatrea standard-input">
            <select name="senderid" id="senderid" class="select">
              <option value="">{$smarty.const.SELECT}</option>                             
					{section name=senderid loop=$arrSenderid}                
              <option value="{$arrSenderid[senderid].id}" {if $senderid==$arrSenderid[senderid].id}selected{/if}>{$arrSenderid[senderid].senderid}</option>
                       				{/section}
            </select>
            <span id="senderidDiv" class="form_error"></span> </div> <!-- <a href="JavaScript:newPopup('ven_senderid.php');" style=" margin-left:20px;">{$smarty.const.REQUEST_NEW}</a> --> </td>
    </tr>
    <tr>
      <td width="200" valign="top"><div align="left"><a id="advOptionTd" style="cursor:pointer; font-weight:bold;" onclick="javascript: showHideAdvOption();"> {if $scheduleDate && $scheduleDate!=''}
        - {$smarty.const.SCHEDULE_DATE}
        {else}
        + {$smarty.const.SCHEDULE_DATE}
        {/if}</a></div></td>
      <td width="350"><div id="advOptionDiv" {if $scheduleDate && $scheduleDate!=''}style="display:block" {else}style="display:none"{/if}> <span class="form_dinatrea standard-input">
              <input type="text" name="scheduleDate" id="scheduleDate" value="{$scheduleDate}" />
      </div></td>
    </tr>
    <tr>
      <td valign="top"><div id="btnShowModal" title="Click Me to view advanced Settings" style="cursor:pointer; font-weight:bold;">+ Custom Reminder</div></td>
      <td><div id="repeatSummary"></div></td>
    </tr>
    <tr>
      <td width="200" valign="top">&nbsp;</td>
      <td><input type="submit" name="send" id="send" value="{$smarty.const.SEND}" class="editbtn" />
          <input type="hidden" name="action" id="action" value="smspush" /></td>
    </tr>
  </table>
	</div></td>
    <td valign="top"><div style=" margin-left:10px; margin-top:10px; width:320px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px; background:#f6f6f6;">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;">{$smarty.const.VEN_COTENTHELP_QUICKSMS_STEP_01}</p>
	</div></td>
  </tr>
</table>
   {else}
  	<table border="0">
  <tr>
    <td valign="top"><div style=" margin-top:10px; width:600px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
		<table align="center" width="600" cellpadding="0" cellspacing="6" style="background-color:#f8f8f8; border-radius: 9px 9px 9px 9px;">
				<tr>
					<td>{$smarty.const.SMS_COUNT}</td>
					<td>{$smsCount}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.TOTAL_CONTACTS}</td>
					<td>{$mobileCount}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.CREDITS_BALANCE}</td>
					<td>{$creditBalance}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>{$smarty.const.CREDITS_COSTS}</td>
					<td>{$creditNeeded}</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>
						
					</td>
					<td>
					
					</td>
				</tr>

  </table>
  </div>
  <table width="600" border="0" style="margin-top:10px;">
  <tr>
    <td width="300" align="left"><input type="button" name="back" id="back" value="Back"  onclick="javascript: backTo(1)" class="editbtn"></td>
    <td width="300" align="right"><input type="submit" name="conf" id="conf" value="{$smarty.const.SEND}" {if $mobileCount<1 || $creditBalance<$mobileCount}disabled  class="redbtn"{else}class="editbtn" {/if} >
					<input type="hidden" name="action" id="action" value="smsconf" ></td>
  </tr>
</table>

  </td>
    <td valign="top"><div style=" margin-left:10px; margin-top:10px; width:320px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px; background:#f6f6f6;">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;">{$smarty.const.VEN_COTENTHELP_QUICKSMS_STEP_02}</p>
	</div></td>
  </tr>
</table>

	
	
  	{/if}
  </div>
<div class="clear"></div>
</form>
</div>
{literal}
    
    <script type="text/javascript">
	//<!--
		 function unhide(divID) {
 			var item = document.getElementById(divID);
 			if (item) {
				 item.className=(item.className=='hidden')?'unhidden':'hidden';
			 }
		 }
		 
		 function addsmstemplate() {
		 	document.smsPushForm.textMessage.value=document.smsPushForm.smstemplate.value;
		 }
		 
		 function backTo(val){
			
			$("#action").val('backTo'+val);
			$('#spinner').show();
			document.smsPushForm.submit();	
		}
		 
		 function SendForm(){
			$('#spinner').show();
			document.smsPushForm.submit();
		}
		
		
		function validatSmspushVen(){
			var phoneregex=/^\+?[0-9]{9,12}$/;
			var errorFlag = 0;	
			if($("#action").val()!='smsconf'){

				if(  $("input[type='radio'][name='m_type']:checked").val() == "m_unicode"){
					$("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE_ONLY}{literal}");
					alert("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE_ONLY}{literal}");
					return false;
				}


				if($("#recipients").val()==''){
					$("#recipientsDiv").html("{/literal}{$smarty.const.ENTER_RECIPIENT}{literal}");
					errorFlag = 1;
					return false;
				}else if($("#recipients").val()!=''){
					$("#recipientsDiv").html("");
					var lines = $("#recipients").val().split('\n');
					lines = lines.filter(Number)
					for(var i = 0;i < lines.length;i++){
    					//code here using lines[i] which will give you each line
						
						if(lines.length  <= 50){
							if(! lines[i].match(phoneregex)){
								$("#recipientsDiv").html("{/literal}{$smarty.const.SUB_REGISTER_MSG_03}{literal}");
								errorFlag = 1;
								return false;
							}else if(lines[i].length<9){
								$("#recipientsDiv").html("{/literal}{$smarty.const.SUB_REGISTER_MSG_03}{literal}");
								errorFlag = 1;
								return false;
							}else if(lines[i].length>13){
								$("#recipientsDiv").html("{/literal}{$smarty.const.SUB_REGISTER_MSG_03}{literal}");
								errorFlag = 1;
								return false;
							}
						}else{
							$("#recipientsDiv").html("{/literal}{$smarty.const.MAXIMUM_NUMBER_RECIPIENTS}{literal}");
							errorFlag = 1;
							return false;
						}
					}
				}
				
				if($("#textMessage").val()==''){
				
					$("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE}{literal}");
					errorFlag = 1;
					return false;
				}else{
					$("#textMessageDiv").html("");
				}
				
				if($("#senderid").val()==''){
					$("#senderidDiv").html("{/literal}{$smarty.const.SELECT_SENDER_NAME}{literal}");
					errorFlag = 1;
					return false;
				}else{
					$("#senderidDiv").html("");
				}
			}

			if(errorFlag == 0){
				$('#spinner').show();
			   document.smsPushForm.submit();
			}
		
		}
		
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$('#recipients').keyup(function() {
        var count= 1;
		var chars= 12;
		var v = $(this).val();
        var vl = v.replace(/(\r\n|\n|\r)/gm,"").length;
		var phoneregex=/^\+?[0-9]{9,12}$/;
	
	//counting Valid lines of recipient
	var Nolines = $("#recipients").val().split('\n');
	Nolines = Nolines.filter(Number)
	document.getElementById("recepientCount").innerHTML = "{/literal}{$smarty.const.COUNTER}{literal}:  "+Nolines.length;
	
	
	var lines = $("#recipients").val().split('\n');
	lines = lines.filter(Number)

	for(var i = 0;i < lines.length;i++){
		
		if(lines.length  <= 50){
			if(! checkMobile(lines[i])){
				$("#recipientsDiv").html("{/literal}{$smarty.const.SUB_REGISTER_MSG_03}{literal}");
			}else if(! lines[i].match(phoneregex)){
				$("#recipientsDiv").html("{/literal}{$smarty.const.SUB_REGISTER_MSG_03}{literal}");
			}else{
				$("#recipientsDiv").html("");
			}
		}else{
			$("#recipientsDiv").html("{/literal}{$smarty.const.MAXIMUM_NUMBER_RECIPIENTS}{literal}");
			return false;
		}
		
	}

});
 
 function checkMobile(mobile){
  var len = mobile.length;
    if((mobile.substr(0,1) == '7' || mobile.substr(0,1) == '6') && len == 9){
		return true;
	}else if((mobile.substr(0,2) == '07' || mobile.substr(0,2) == '06') && len == 10){
       return true;
    }else if(len == 12 && (mobile.substr(0,1) != "+") && (mobile.substr(0,1) != "0")){
      return true;
    }else if (len == 13 && (mobile.substr(0,1) == "+")){
        return true;
    }else{
      return false;
    }
  }

        //-->
     </script>
	 
	 
	 <script type="text/javascript">
	$(document).ready(function(){
	    $("#spinner").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
	    });
	 
	     });
		 
		 window.onload=function() {
 			$("#myBalance").html("{/literal}{$venCreditBalance}{literal}");
		}
	</script>
	
	
{/literal}
