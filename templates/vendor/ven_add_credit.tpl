<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 220px;
	text-align: left;
		}


.row-2{
 width:130px;
 text-align: center;
}

.row-3{
 width:150px;
 text-align: left;
}

.row-4{
 width:110px;
 text-align: center;
}

.row-5{
 padding-left: 5px;
 padding-right: 5px;
 text-align: left;
 width:100px;
 text-align: right;
}

.row-6{
 width:120px;
 text-align: left;
 padding:5px;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}
</style>
<div id="container">
    <h1>Manage Add Credit</h1>
	
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}

	{if $msg} style="display:block; margin-bottom:20px; margin-left:21px" {else} style="display:none; margin-left:21px;" {/if}> {$msg}	</div>
{if $action == 'edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST" onsubmit='if(confirm("Add credit?"))if(!Error()) return false;'> <!-- onsubmit="if(!confirm('Are you sure change status?'))return false;" -->
		<table cellspacing="10">
			<tr>
				<td>Client Name:</td>
				<td>{$arrAddCredit.name}</td>
				
			</tr>
			<tr>
				<td>Credit Requested: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crditRequested" id="crditRequested" value="{$arrCreditDetails.credit_requested}" onkeyup="getCreditShcemeDetailsAdm(this.value,document.getElementById('crdtSchmId').value,'{$ajaxUrl}');"></span></td>
				<td><div id='error_req' style='color:red;'></div></td>
			</tr>
			<!--<tr>
				<td>Rate:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="crdtSchmId" id="crdtSchmId" onchange="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');">
						<option value="0">Select</option>
						{section name="crdtSchmId" loop=$arrCreditScheme}
						<option value="{$arrCreditScheme[crdtSchmId].id}###{$arrCreditScheme[crdtSchmId].rate}" 
						{if $arrCreditScheme[crdtSchmId].id== $arrCreditDetails.credit_scheme_id}Selected{/if}>{$arrCreditScheme[crdtSchmId].rate}</option>
						{/section}
					</select></span>
				</td>
				<td><div id='error_rate' style='color:red;'></div></td>
			</tr>-->
			<tr>
				<td>Rate: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crdtSchmId" id="crdtSchmId" value="{$tmp}" onkeyup="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');"></span></td>
				
			</tr>
			<tr>
				<td>Total Cost:</td>
				<td><div id="creditTotalCostAdm">{$arrCreditDetails.total_cost}</div></td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" >
						<option value="">Select</option>
						<option value="allocated" {if $arrCreditDetails.status == 'allocated'} selected{/if}>Allocate</option>
						<option value="rejected"  {if $arrCreditDetails.status == 'rejected'} selected{/if}>Reject</option>
						<option value="pending" {if $arrCreditDetails.status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>
				
			</tr>
		<!--	<tr>
				<td>Payment Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="payment_status" id="payment_status" >
						<option value="">Select</option>
						<option value="0" {if $arrCreditDetails.payment_status == '0'} selected{/if}>Not Paid</option>
						<option value="1" {if $arrCreditDetails.payment_status == '1'} selected{/if}>Paid</option>
						<option value="2" {if $arrCreditDetails.payment_status == '2'} selected{/if}>Partially Paid</option>
						<option value="3" {if $arrCreditDetails.payment_status == '3'} selected{/if}>Receipt Sent</option>
						<option value="4" {if $arrCreditDetails.payment_status == '4'} selected{/if}>Bonus</option>
					</select></span>
				</td>
				
			</tr> -->
			<tr>
				
				<td>
					<input type="submit" name="save" id="save" value="Save" class="editbtn">
					<input type="hidden" name="id" id="id" value="{$arrCreditDetails.id}">
					<input type="hidden" name="userId" id="userId" value="{$arrAddCredit.id}">
					<input type="hidden" name="old_status" id="old_stutus" value="{$arrCreditDetails.status}">
					<input type="hidden" name="old_payment_status" id="old_pay" value="{$arrCreditDetails.payment_status}">
					<input type="hidden" name="action" id="action" value="add">
				</td>
				<td>
					<input type="button" name="cancel" id="cancel" value="Cancel" class="editbtn" onclick="history.back()">
				<input name="payment_status" type="hidden" id="payment_status" value="1" />				</td>

			</tr>
		</table>
	</form>


{elseif $action==""}
	<form name="creditRequest" action="" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Client name: </td>
	    <td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Client ID:</td>
	    <td><span class="form_dinatrea standard-input">
					<input type="text" name="vendor_id" id="vendor_id" value="{$vendor_id}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Username:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="username" id="username" value="{$username}"></span>
				</td>
				
			</tr>
			
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="action" id="action" value="{$action}">
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
				</td>
				
			</tr>
		</table>
	</form>	
	<br />
	
	
	
<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
	<table style="margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">	
		<td class="row-1">Client Name</td>
		<td class="row-1">Username</td>
		<td class="row-1">Client ID</td>
		<td class="row-1">Client Type</td>
		
	</tr>
	{if $objCreditRequest}
	
	{section name=reqCredit loop=$objCreditRequest}
	
	<tr class="row-body">
		<td class="row-1"><a href="?id={$objCreditRequest[reqCredit].id}&action=edit" title="Edit" style='text-decoration:none;color:green'>{$objCreditRequest[reqCredit].name}</a> </td>
		<td class="row-1"><a href="?id={$objCreditRequest[reqCredit].id}&action=edit" title="Edit" style='text-decoration:none;color:green'>{$objCreditRequest[reqCredit].username}</a> </td>
		<td class="row-1"> {$objCreditRequest[reqCredit].vendor_id} </td>
		<td class="row-1">
		{if $objCreditRequest[reqCredit].vendor_type=='2'}
			Organisation
		{elseif $objCreditRequest[reqCredit].vendor_type=='1'}
			Individual
		{/if}
		</td>
	
	{/section}
	
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="10" class="content-link" style="padding:10px;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr>
		<tr style="background-color:#f8f8f8;">
		<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Rocords found
		</td>
	</tr>	
	</tr>		

	{/if}
	</table>
	
</div>
          <div class="clear"></div>
     </div>  
{/if}

</div>

<script type="text/javascript">

	function Error(){
		var crditReq = $("#crditRequested").val();
		var crdtSchmId = $("#crdtSchmId").val();

		if (crditReq == '' || crdtSchmId == '0'){
			if (crditReq == ''){
				$('#error_req').html('Input credit requested');	
			} else { $('#error_req').html('');	}

			if (crdtSchmId == '0'){
				$('#error_rate').html('Select rate');		
			} else { $('#error_rate').html('');}
			$("#action").val('');
			return false;
		}else{
			$("#action").val('add');
			return true;
		}
	}
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
        //-->
     </script>
