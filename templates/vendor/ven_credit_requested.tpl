<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 910px;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 910px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 100px;
	text-align: left;
		}


.row-2{
 width:150px;
 text-align: left;
}

.row-3{
 width:50px;
 text-align:center;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 950px; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}

</style>
<div id="container">
<div>
    <h1>{$smarty.const.MANAGE_PURCHASE_REQUEST}</h1>
</div>
	
<div id="errorDiv"></div>
	<form name="creditRequest" action="" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Price/SMS:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="rate" id="rate">
					<option value="">Select</option>
					{section name=rate loop=$arrCrdtSchm}
					<option value="{$arrCrdtSchm[rate].id}" 
					{if $arrCrdtSchm[rate].id==$rate}selected{/if}
					>{$arrCrdtSchm[rate].rate}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Quantity:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="quantity" id="quantity" value="{$quantity}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">	
					<select name="status" id="status" >
						<option value="" >Select</option>
						<option value="allocated" {if $status == 'allocated'} selected{/if}>Allocated</option>
						<option value="rejected"  {if $status == 'rejected'} selected{/if}>Rejected</option>
						<option value="pending" {if $status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>				
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
				
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="{$smarty.const.SEARCH}" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
				
			</tr>
		</table>
	</form>	

<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
      <!--  <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	<table style="margin-top:20px; margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">	
		<td class="row-1">P R No.</td>
		<td class="row-2">Broadcaster</td>
		<!-- <td class="row-1">Broadcaster ID</td> -->
		<td class="row-2">Broadcaster Type</td>
		<td class="row-1">Pric/SMS</td>
		<td class="row-1">Quantity</td>
		<td class="row-1">Total Cost</td>
		<td class="row-1">Status</td>
		<td class="row-1">Request Date</td>
		
	</tr>
	{if $objCreditRequest}
	{section name=reqCredit loop=$objCreditRequest}

	<tr class="row-body">		
		<td class="row-1"> {$objCreditRequest[reqCredit].credit_request_id} </td>
		<td class="row-2"> {$objCreditRequest[reqCredit].name} </td>
		<!-- <td class="row-1"> {$objCreditRequest[reqCredit].vendor_id} </td> -->
		<td class="row-2">
		{if $objCreditRequest[reqCredit].vendor_type=='2'}
			Organisation
		{elseif $objCreditRequest[reqCredit].vendor_type=='1'}
			Individual
		{/if}
		</td>
		<td class="row-1"> {if ($objCreditRequest[reqCredit].credit_scheme_id==$objCreditRequest[reqCredit].idbon)}{$objCreditRequest[reqCredit].ratebon}{else}{$objCreditRequest[reqCredit].rate}{/if} </td>
		<td class="row-1"> {$objCreditRequest[reqCredit].credit_requested} </td>
		<td class="row-1"> {$objCreditRequest[reqCredit].total_cost} </td>
		<td class="row-1"> {$objCreditRequest[reqCredit].status|ucfirst} </td>
		<td class="row-1"> {$objCreditRequest[reqCredit].created} </td>
		
	</tr>
	{/section}
	
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="9" class="content-link" style="padding:10px;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr>
		<td colspan="9" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Rocords found
		</td>
	</tr>	
	</tr>		

	{/if}
	</table>
	</div>
          <div class="clear"></div>
     </div>
</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
        //-->
     </script>
