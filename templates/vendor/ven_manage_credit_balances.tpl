<div id="container">
    <h1>Vendor Credit Balances</h1>
	
<div id="errorDiv"></div>
{if $action==""}
	<form name="creditBalance" action="" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Client Name: </td>
	    <td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Client Username:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="username" id="username" value="{$username}">
				</span>
				</td>
				
			</tr>
			<tr>
				<td>Client Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendor_type" id="vendor_type" >
						<option value="" >Select</option>
						<option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
					</select>
				</span>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
				
			</tr>
		</table>
	</form>	
	<br />
	<table width="100%" border="0" cellspacing="2" style="background-color:#FFFFFF;">
	<tr style="font-size:12px; font-weight:bold;background-color:#f8f8f8;">	
		<td style="padding:10px;" align="center"><div align="left">Client</div></td>
		<td align="center"><div align="left">Client ID</div></td>
		<td align="center"><div align="left">Client Type</div></td>
		<td align="center"><div align="left">Credit Balance</div></td>
		<td align="center"><div align="left">Credit Blocked</div></td>
		<td align="center">Edit</td>
	</tr>
	{if $objCreditBalance}
	{section name=balCredit loop=$objCreditBalance}
	<tr style="font-size:12px;background-color:#f8f8f8;">		
		<td style="padding:10px;"> {$objCreditBalance[balCredit].name} </td>
		<td> {$objCreditBalance[balCredit].vendor_id} </td>
		<td>
		{if $objCreditBalance[balCredit].vendor_type=='2'}
			Organisation
		{elseif $objCreditBalance[balCredit].vendor_type=='1'}
			Individual
		{/if}
		</td>
		<td>{$objCreditBalance[balCredit].credit_balance}</td>
		<td>{$objCreditBalance[balCredit].credit_blocked}  </td>
		<td align="center"><!-- <a href="?id={$arrCreditBalances[balCredit].id}&action=edit" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a> --> </td>
	</tr>
	{/section}
	
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="10" class="content-link" style="padding:10px 0;">
			{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr>
		<tr style="background-color:#f8f8f8;">
		<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Records found
		</td>
	</tr>	
	</tr>		

	{/if}
	</table>
{/if}

</div>

<script type="text/javascript">

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});		

     </script>
