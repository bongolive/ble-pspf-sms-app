<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 910px;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 910px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 100px;
	text-align: left;
		}


.row-2{
 width:200px;
 text-align: left;
}

.row-3{
 width:50px;
 text-align:center;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 950px; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}

</style>
<script type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var answer=confirm("Do you realy want to diactive this reminder?");
  if(answer==true){
  	var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  	for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }
}
//-->
</script>
<div id="container">
    <h1>SMS Reminders </h1>
	<div style="width:100%; margin:0 auto;">
	<div id="errorDiv"></div>
	{if $action=='' || $action=='search'}
	<form name="creditRequest" action="{$selfUrl}" method="GET">
		<table cellspacing="10" width="633">	
			<tr>
				<td width="261"></td>
				<td width="145">				</td>				
			    <td width="17"></td>
			    <td width="150"></td>
			</tr>
			
			<tr>
			  <td><strong>Reminder Type:</strong></td>
			  <td><span class="form_dinatrea standard-input">
			  <select name="reminder_type" id="reminder_type">
			    <option value="" {if $reminder_type==''}selected{/if}>Select</option>
			    <option value="sms" {if $reminder_type=='sms'}selected{/if}>SMS Reminder</option>
			    <option value="birthday" {if $reminder_type=='birthday'}selected{/if}>Birthday Reminder</option>
			  </select> </span>			  </td>
		      <td>&nbsp;</td>
		      <td>&nbsp;</td>
		  </tr>
			<tr>
				<td><strong>Start Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}">
				</span></td>
			    <td>To</td>
			    <td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate1" id="startDate1" value="{$startDate1}">
				</span>	</td>
			</tr>
			<tr>
				<td><strong>End Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}"> </span>				</td>
			    <td>To</td>
			    <td>
				<span class="form_dinatrea standard-input">
					<input type="text" name="endDate1" id="endDate1" value="{$endDate1}">
				</span>				</td>
			</tr>
			<tr>
				<td><strong>Job Name;</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="job_name" id="job_name" value="{$job_name}"></span>				</td>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			</tr>
			<tr>
			  <td><strong>Reminder Status : </strong> </td>
			  <td><span class="form_dinatrea standard-input">
			  <select name="status" id="status">
			    <option value="" {if $status==''}selected{/if}>All</option>
				<option value="0" {if $status=='0'}selected{/if}>Active</option>
			    <option value="1" {if $status=='1'}selected{/if}>InActive</option>
			    <option value="2" {if $status=='2'}selected{/if}>Expired</option>
			  </select> </span>
			  </td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
		  </tr>
			<tr>
				<td></td>
			  <td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="search"></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			</tr>
			<tr>
			  <td></td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
			  <td>&nbsp;</td>
		  </tr>
		</table>
	</form>	
	
	<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
      <!--  <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	<table style="margin-top:20px; margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">		
		<td class="row-1">Reminder Type</td>
		<td class="row-1">Platform Used</td>
		<td class="row-2">Text Message</td>
		<td class="row-1">Repeat</td>
		<td class="row-1">Next Send Date </td>
		<td class="row-1">End Date</td>
        <td class="row-1">Job Name</td>
		<td class="row-3">Status</td>
		<td class="row-3">Edit</td>
		<td class="row-3">Disable</td>
	</tr>
	{if $arrReminders}
		{section name=reminder loop=$arrReminders}
		<!-- <tr class="TR_spc"><td colspan="10" class="TD_spc"></td></tr> -->	
		<tr class="row-body">	
			<td class="row-1">{$arrReminders[reminder].reminder_type}</td>
			<td class="row-1">
			{if $arrReminders[reminder].contact_id|count_characters > 10}
				GROUP SMS
			{elseif $arrReminders[reminder].contact_id|count_characters eq 0}
				GROUP SMS
			{else}
			 {$arrReminders[reminder].contact_id} SMS
			{/if}
			</td>
			<td class="row-2">{$arrReminders[reminder].text_message|wordwrap:20:"<br />\n"}</td>
			<td class="row-1">{$arrReminders[reminder].repeat_every} {$arrReminders[reminder].repeat}</td>
			<td class="row-1">{$arrReminders[reminder].send_date}</td>
			<td class="row-1">{$arrReminders[reminder].end_date}</td>
			<td class="row-1">{$arrReminders[reminder].job_name}</td>
			<td class="row-3">{if $arrReminders[reminder].status==0}Active{elseif $arrReminders[reminder].status==2}Expired{else}InActive{/if}</td>
			<td class="row-3"><a href="ven_reminders.php?id={$arrReminders[reminder].id}&action=edit"><img src="{$BASE_URL_HTTP_ASSETS}images/edit.png" alt="edit " border="0"  /></a></td>
            <td class="row-3">
			<div align="center"><img src="{$BASE_URL_HTTP_ASSETS}images/delete.png" alt="Disable" onclick="MM_goToURL('parent','ven_reminders.php?id={$arrReminders[reminder].id}&action=delete');return document.MM_returnValue" /></div></td>
		</tr>
		{/section}
	
		<tr style="background-color:#f8f8f8;">
			<td align="right" colspan="10" class="content-link" style="padding:10px 0;">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}</td>
		</tr>
	{else}
		</tr>
		<tr style="background-color:#f8f8f8;">
			<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">&nbsp;</td>
		</tr>		
	{/if}
	</table>
	
	</div>
          <div class="clear"></div>
     </div>
</div>
{/if}



{if $action=='update'}
<form name="reminder" id="reminder" action="" method="post">
<table width="444" border="0" cellpadding="10" cellspacing="10">
  <tr>
    <td valign="top" width="200"><strong>Repeat:</strong></td>
    <td width="255"><div>
	
	<select name="repeat" id="repeat" onchange="pickRepeat();"style="border:2px solid #ccc;" >
		<option value="" {if $arrReminders.repeat==''} selected {/if}>Never</option>
		<option value="DAY" {if $arrReminders.repeat=='DAY'} selected {/if}>Daily</option>
		<option value="WEEK" {if $arrReminders.repeat=='WEEK'} selected {/if}>Weekly</option>
		<option value="MONTH" {if $arrReminders.repeat=='MONTH'} selected {/if}>Monthly</option>
		<option value="YEAR" {if $arrReminders.repeat=='YEAR'} selected {/if}>Yearly</option>
	</select>
	</div> <span id="repeatErrorDiv" class="form_error"></span></td>
  </tr>
  <tr>
    <td valign="top"><strong>Repeat Every:</strong></td>
    <td valign="bottom">
	<select id="repeat_every" name="repeat_every" onchange="changeNumberOfRepeat();" style="border:2px solid #ccc;" >
		{section name=foo start=1 loop=31 step=1} 
			<option value="{$smarty.section.foo.index}" {if $arrReminders.repeat_every==$smarty.section.foo.index} selected {/if} >{$smarty.section.foo.index}</option>
		{/section} 
	</select>
	<label id="repeatDiv">{$arrReminders.repeat}</label>	</td>
  </tr>
  <tr id="repeatDaysRow" style="display:none;">
    <td valign="top"><strong>Repeat Days:</strong></td>
    <td>
		<div>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Sunday" {if $arrRepeatDays.Sunday=='Sunday'} checked {/if} />S</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Monday" {if $arrRepeatDays.Monday=='Monday'} checked {/if} />M</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Tuesday" {if $arrRepeatDays.Tuesday=='Tuesday'} checked {/if} />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Wednesday" {if $arrRepeatDays.Wednesday=='Wednesday'} checked {/if} />W</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Thursday" {if $arrRepeatDays.Thursday=='Thursday'} checked {/if} />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Friday" {if $arrRepeatDays.Friday=='Friday'} checked {/if} />F</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Saturday" {if $arrRepeatDays.Saturday=='Saturday'} checked {/if} />S</label>
		</div>	</td>
  </tr>
  <tr>
  	<td><strong>Next Send Date:</strong></td>
	<td>				 		 
	<span class="form_dinatrea standard-input">
		<input type="text" name="sendDate" id="sendDate" value="{$arrReminders.send_date|date_format:'%m/%d/%Y %H:%M:%S'}">
	</span>				 
	<span id="startDateDiv" class="form_error"></span>	</td>
  </tr>
  <tr>
    <td valign="top"><strong>End Date:</strong></td>
    <td>
	<span class="form_dinatrea standard-input">
	<input type="text" name="editEndDate" id="editEndDate" value="{$arrReminders.end_date|date_format:'%m/%d/%Y %H:%M:%S'}"> 
	</span>			 
	<span id="birthdayDiv" class="form_error"></span>	</td>
  </tr>
  <tr>
    <td valign="top"><strong>Contacts / Addresskooks:</strong></td>
    <td><span class="standard-input">
		<select multiple id="addBook" name="addBook[]" style="height:100px; width:300px;">
			{section name=addressbook loop=$arrContacts}
			<option value="{$arrContacts[addressbook].addressbook}">{$arrContacts[addressbook].addressbook}</option>
			{/section}
			</select>
			</span></td>
  </tr>
  <tr>
    <td valign="top"><strong>Text Message</strong></td>
    <td><span class="standard-input"><textarea name="textMessage" id="textMessage" cols="40" rows="6" class="textarea">{$arrReminders.text_message}</textarea></span><div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
      <div style="width:70%; float:left;" id="messageCount"></div></td>
  </tr>
  <tr>
    <td><input type="hidden" name="action" id="action" value="{$action}" />
      <input name="id" type="hidden" id="id" value="{$arrReminders.id}" /></td>
    <td><input type="submit" name="save" id="save" value="{$smarty.const.SAVE}" class="editbtn" onclick="return validateReminder();"></td>
  </tr>
</table>
</form>

<script type="text/javascript">

{if $arrReminders.repeat=='WEEK'}
	$("#repeatDaysRow").show();
{else}
	$("#repeatDaysRow").hide();
{/if}


      		function validateReminder(){
		 		var errorFlag=0;
		 		if($("#repeat").val()==''){
		 			$("#repeatErrorDiv").html("Please Select This Field");
					errorFlag=1;
		 		}else{
					$("#repeatErrorDiv").html("");
				}
		 
		  	/*	if($("#sendDate").val()!=""){
					var d1=new Date();
					var d2=$("#sendDate").val();
					var d3=new Date(d2);
					if(d3<d1){
						$("#startDateDiv").html("Next Send Date Should be greater than now");
						errorFlag=1;
					}else{
						$("#startDateDiv").html("");
					}
		 		} */
		 
		 		if($("#editEndDate").val()!=""){
					var d4=new Date();
					var d5=$("#editEndDate").val();
					var d6=new Date(d5);
					var startDate=$("#sendDate").val();
					var d7=new Date(startDate);
					if(d6<d4){
						$("#birthdayDiv").html("End Date Should be greater than now");
						errorFlag=1;
					}else if(d6<d7){
						$("#birthdayDiv").html("End Date Should be greater than Send Date");
						errorFlag=1;
					}else{
						$("#birthdayDiv").html("");
					}
		 		}
		 
		 		if(errorFlag==1){
					return false;
				}else{
		 			$("#repeatErrorDiv").html("");
					$("#birthdayDiv").html("");
					$("#startDateDiv").html("");
				}
			}


    function pickRepeat(){
   	if($("#repeat").val()=="DAY"){
		$("#repeatDiv").html("Day");
		$("#repeat_every").attr('disabled', false);
		$("#repeatDaysRow").hide();
	}else if($("#repeat").val()=="WEEK"){
		$("#repeatDiv").html("Week");
		$("#repeat_every").val(1);
		$("#repeat_every").attr('disabled', true);
		$("#repeatDaysRow").show();
	}else if($("#repeat").val()=="MONTH"){
		$("#repeatDiv").html("Month");
		$("#repeat_every").attr('disabled', false);
		$("#repeatDaysRow").hide();
	}else if($("#repeat").val()=="YEAR"){
		$("#repeatDiv").html("Year");
		$("#repeat_every").attr('disabled', false);
		$("#repeatDaysRow").hide();
	}else{
		$("#repeatDiv").html("");
		$("#repeat_every").attr('disabled', false);
		$("#repeatDaysRow").hide();
	}
	
   }
   
   
   function changeNumberOfRepeat(){
   		if($("#repeat_every").val()>1){
			if($("#repeat").val()=="DAY"){
				$("#repeatDiv").html("Days");
			}else if($("#repeat").val()=="WEEK"){
				$("#repeatDiv").html("Weeks");
			}else if($("#repeat").val()=="MONTH"){
				$("#repeatDiv").html("Months");
			}else if($("#repeat").val()=="YEAR"){
				$("#repeatDiv").html("Years");
			}else{
				$("#repeatDiv").html("");
			}
		}
   }	
	</script>

{/if}



</div>

<script type="text/javascript">

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#startDate1').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate1').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		$(function() {
		    $('#sendDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#editEndDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
     </script>
