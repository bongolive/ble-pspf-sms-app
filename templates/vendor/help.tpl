<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.VEN_MENUTOP_HELP}</h1>
	
	<ol type="1">
		<li><a href="#1">What file types do you accept to upload contacts?</a></li>
		<li><a href="#2">Is there a specific format that the file with numbers I upload should be in?</a></li>
		<li><a href="#3">How do I purchase sms credits from Bongo Live?</a></li>
		<li><a href="#4">How long does it take for sms credits purchased to appear on my account?</a></li>
		<li><a href="#5">How do I change my password?</a></li>
		<li><a href="#6">How do I change my mobile number?</a></li>
		<li><a href="#7">What is a Sender Name?</a></li>
		<li><a href="#8">How do I request a Sender Name?</a></li>
		<li><a href="#9">What Sender Names can I request?</a></li>
		<li><a href="#10">How long does it take for a Sender Name request to be approved?</a></li>
		<li><a href="#11">How do I add a contact?</a></li>
		<li><a href="#12">How do I edit a contact?</a></li>
		<li><a href="#13">How do I create a new group?</a></li>
		<li><a href="#14">How can I delete a group?</a></li>
		<li><a href="#15">How do I broadcast an SMS?</a></li>
		<li><a href="#16">I want to purchase more than 25,000 sms, is the price negotiable?</a></li>
		<li><a href="#17">Is there a difference between an Individual or Organization broadcaster account?</a></li>
		<li><a href="#18">How many contacts can I add to the system?</a></li>
	</ol>
	<br />
		<ol type="1">
		<li><b><a name="1">What file types do you accept to upload contacts?</a></b></li>
			<p>At the moment we only accept .CSV file types. You can convert an excel file into this format by doing a FILE > SAVE AS > (select .CSV from the file type drop down)</p>
		<li><b><a name="2">Is there a specific format that the file with numbers I upload should be in?</a></b></li>
		<p>Yes. The format can be copied from the sample file available on the Dashboard > Import Contacts page.</p>
		<li><b><a name="3">How do I purchase sms credits from Bongo Live?</a></b></li>
		<p>To purchase credits click on the �Purchase SMS � link. Complete the form by entering the number of sms credits that you wish to purchase. The price will automatically be calculated. Click �Submit� to confirm your purchase request.  You will be shown a purchase request number and details about how to pay for your purchase (via ZAP, MPESA, cash).  Once payment is received and confirmed. A Bongo Live representative will approve the addition of credits to your account. The customer is responsible for any transaction charges.</p>
		<li><b><a name="4">How long does it take for sms credits purchased to appear on my account?</a></b></li>
		<p>Once payment is received, credits should usually appear within 2-3 hours. Please allow up to 6 hours. </p>
		<li><b><a name="5">How do I change my password</a></b>?</li>
		<p>Click on the �Change Password� link once you are logged into your account. You will be asked for your old and new passwords. Click �Submit� to confirm the change. You will receive a confirmation of your new password via sms and email. </p>
		<li><b><a name="6">How do I change my mobile number?</a></b></li>
		<p>Click on the �Change Mobile Number� link. You will be asked for a new mobile number. </p>
		<li><b><a name="7">What is a Sender Name?</a></b></li>
		<p>The sender name is the text or number that appears as the �From:� on your sms message. This can either be 11 letters or a mobile number is the proper international format and upto 14 digits long. Ex. 255784234357.</p>
		<li><b><a name="8">How do I request a Sender Name?</a></b></li>
		<p>To request a sender name. Click on the �Sender Name� link. You will be asked to enter your requested sender name and the reason you are requesting it.  </p>
		<li><b><a name="9">What Sender Names can I request?</a></b></li>
		<p>You can request a sender name that is not the name of any organization, person, product or brand name that is not your own.  Phone numbers requested will only be approved if they are associated with your broadcaster account. We strive to prevent fraudulent use of sender names.</p>
		<li><b><a name="10">How long does it take for a Sender Name request to be approved?</a></b></li>
		<p>Once a Sender Name request is received, it will be processed usually within 2-3 hours. Please allow up to 6 hours. </p>
		<li><b><a name="11">How do I add a contact?</a></b></li>
		<p>To add a contact visit the Dashboard > Add New Contact screen.  Fill in the information available about the contact and click �Save�. The only required field is the mobile number.</p>
		<li><b><a name="12">How do I edit a contact?</a></b></li>
		<p>To edit a contact visit the �Dashboard� page and click on �Manage Contacts� for the group in which the contact exists. Once the contact is found click on the �Edit� button to change any details. Remember to save once you are finished making changes.</p>
		<li><b><a name="13">How do I create a new group?</a></b></li>
		<p>Creating a group is simple. Visit the �Dashboard� page, enter the name of your new group in the form and click �Submit�.</p>
		<li><b><a name="14">How can I delete a group?</a></b></li>
		<p>Deleting a group is also simple. Visit the �Dashboard� page. Select the group that you want to delete from the list of groups. Click the �Delete� button and confirm the delete.</p>
		<li><b><a name="15">How do I broadcast an SMS?</a></b></li>
		<p>To broadcast an sms you first need to ensure a few things.</p>
		<ol type="A">
		<li>Do you have sufficient sms credits to send sms to the groups you want to send the sms to.  If not visit the �Purchase SMS� page to start the process. </li>
		<li>Do you have a valid sender name approved by the administrators? If not visit the Broadcast > Manager Sender Names page to request a sender name.</li>
		<li>Once the above steps are complete. Visit the Broadcast > Compose SMS view. </li>
		    <ol type="1">
			<li>Select the groups you want to broadcast to, enter the sms message, select the sender name</li>
			<li>If you want to schedule the sms for the future you can also select the date and time. </li>
			<li>Click �Send� and then confirm the send. </li>
			</ol>
		</ol>
		</p>
		<li><b><a name="16">I want to purchase more than 25,000 sms, is the price negotiable?</a></b></li>
		<p>For large bulk SMS purchases we will be more than happy to negotiate a better sms rate with you.  Please call us at 0784 845784 for more details or contact us through the contact form.</p>
		<li><b><a name="17">Is there a difference between an Individual or Organization broadcaster account?</a></b></li>
		<p>There is no differece between these two account types. We only distinguish between them so that we can better serve our customers.</p>
		<li><b><a name="18">How many contacts can I add to the system?</a></b></li>
		<p>Theoretically there is no limit to the number of contacts you can add to the system. </p>
	</ol>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>