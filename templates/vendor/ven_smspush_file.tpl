<!--header start-->
<!--header end-->
<style type="text/css">
<!--
.hidden { display: none; }
.unhidden { display: block; }

.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 165% /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
.spinnerDiv {
		position: absolute;
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 40%; 
	    left:50%; 
	    text-align:center;
	    z-index:1236;
	    overflow: auto;
	   /* width: 100px;  width of the spinner gif */
	   /* height: 102px; hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
	/*.search-box form input[type="text"]*/
@charset "utf-8";
/* CSS Document */

*{
	margin:0;
	padding:0;
}

.clr{
   clear:both;
}
/*-------------------wrapper------------------*/
#wrapper{ 
   
    margin: 0;
    padding: 0; margin-bottom:25px;

}

.center-wrapper{
   margin:0 auto;
    width:620px;
}

 
/*-------------------header------------------*/ 
#header-page{
  float:left;
  width:700px;
  margin-left:21px;
}

.top-navi2{
  float:left;
  width:313px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color:#E9CFEC;
}

.top-navi3{
  float:left;
  width:484px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
}

.top-navi4{
  float:left;
  width:631px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
}

.top-navi{
  float:left;
  width:480px;
  height:30px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
  margin-top:10px;
}

.top-navi ul{
 list-style:none;
}



.content{
   width:850px;
   height:120px;
   border:1px solid #bbbbbb;
   margin-top:100px;
   margin-left:21px; 
   border-radius: 9px 9px 9px 9px;
}


.content ul{
  list-style:none;
}

.content ul li{
border: 1px solid #DDDDDD;
    float: left;
    margin-left: 17px;
    margin-top: 20px;
    padding: 4px 10px;
    text-align: center;}


.one{ 
   color: #000;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.two{ 
   color: #cc0000;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.three{ 
   color: #339900;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.four{ 
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

span.total{ 
   	color: #000;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}

span.invalid{ 
   color: #cc0000;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
    font-weight:bold;
}

span.valid{ 
   color: #339900;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}

span.smscount{ 
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}


.clear{
	 clear:both;
}

.smsPreview{
   width:850px;
   height:auto;
   border:1px solid #bbbbbb;
   margin-top:50px;
   margin-left:21px;
   padding-bottom: 10px; border-radius: 9px 9px 9px 9px; padding-bottom: 30px;
}


.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 680px}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 830px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
	padding-left: 5px;
    padding-right: 5px;
    text-align: left;
    width: 370px;
	text-align: left;
		}


.row-2{
 padding-right: 5px;
 text-align: left;
 width:120px;
 text-align: left;
}

.row-3{
 width:100px;
 padding-right: 5px;
    text-align: center;
}

.row-4{
 width:120px;
 padding-right: 5px;
    text-align: center;
}

.row-5{
 padding-right: 5px;
    text-align: center;
    width: 90px;
}

.row-6{
 width:120px;
 text-align: left;
 padding:5px;
}

.para{
  
border: 1px solid #ABADB3;
    color: #000000 !important;
    margin-top: 2px;
    padding: 0 2px;
    width: 350px;}

.footer-top{
    border: 1px solid #BBBBBB;
    height: 90px;
    margin-top: 40px;
	margin-left:21px;
    position: relative;
    width: 850px; border-radius: 9px 9px 9px 9px; 
}

.footer-bottom{
   float:left;
   width:700px;
   margin-top:40px;
   margin-left:20px;
}

.footer-bottom3{
   float:left;
   width:850px;
   margin-top:40px;
   margin-left:20px;
}


.left-bottom{
   float:left;
   width:200px;
}


.right-bottom{
     float: right;
    width: 196px;
}


.back{
   float:right;
   margin-left:20px;
}

.summy{
 margin-left: 15px;
    margin-top: -15px;
}


.center-job{
 margin-left: 15px;
    margin-top: -12px;
}


.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}

.title
{
color: #555555;
font-size: 20px;
margin-left: 13px;
margin-top: 10px;
}


.content-sec {
    border: 3px;
    height: 208px;
    margin-top: 8px;
    padding: 9px;
    width: 733px;
}

.second-nav li
{
display: inline;
    float: left;
    font-family: arial;
    font-size: 14px;
    font-weight: bold;
    line-height: 30px;
    list-style-image: none;
    list-style-position: outside;
    margin-right: 0px;
	margin-left:21px; 
	color: #AAAAAA;
	}

.second-nav ul
{
margin-top:10px;
}

.second-nav li.first-option
{
background-image: url("templates/assets/images/filesms_arrow.png");
    background-repeat: no-repeat;
    color: #FFFFFF;
    height: 31px;
    padding-left: 21px;
    width: 151px;}
	
.second-nav li.first-option2
{
background-image: url("templates/assets/images/filesms_arrow.png");
    background-repeat: no-repeat;
    color: #FFFFFF;
    height: 31px;
	padding-left: 21px;
    width: 151px;}

.btm-content
{
border: 1px solid #ccccd2;
height:auto;
margin-left: 21px;
margin-top: 40px;
width: 700px;border-radius: 9px 9px 9px 9px;
}
.btm-help
{
border: 1px solid #ccccd2;
height:auto;
margin-left: 21px;
margin-top: 40px;
width: 200px;border-radius: 9px 9px 9px 9px;
background-color:#f8f8f8;
}


.file-upload{

background-image: url("templates/assets/images/file-upload.png");
background-repeat: no-repeat;
height: 28px;
margin-left: 14px;
margin-top: -12px;
position: absolute;
width: 100px;
}

.file-create-message{

background-image: url("templates/assets/images/file_create_message.png");
background-repeat: no-repeat;
height: 28px;
margin-left: 14px;
margin-top: -12px;
position: absolute;
width: 150px;
}

.instrution
{
color: #CCCCD2;
    font-family: arial;
    font-size: 13px;
    font-weight: bold;
    margin-left: 18px;
    margin-top: 28px;}



.upload
{

margin-left: 18px;
    margin-top: 12px;
}
.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 430px;
   height: 270px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
-->
</style>
<script type="text/javascript">
		$(document).ready(function ()
   {
      $("#btnShowSimple").click(function (e)
      {
         ShowDialog(false);
         e.preventDefault();
      });

      $("#btnShowModal").click(function (e)
      {
         ShowDialog(true);
         e.preventDefault();
      });

      $("#btnClose").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
         
		 var errorFlag=0;
		 //if($("#repeat").val()==''){
		 //	$("#repeatErrorDiv").html("Please Select This Field");
		//	errorFlag=1;
		 //}
		 
		 if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
		   	if($("#dayStart").val()==""){
				$("#startDateDiv").html("Please select Day to start");
				errorFlag=1; 
			}else if($("#monthStart").val()==""){
				$("#startDateDiv").html("Please select Month to start");
				errorFlag=1; 
			}else if($("#yearStart").val()==""){
				$("#startDateDiv").html("Please Select Year to start");
				errorFlag=1;
			}else{
				var d1=new Date();
				var d2=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				var d3=new Date(d2);
				if(d3<d1){
					$("#startDateDiv").html("Start Date Should be greater than now");
					errorFlag=1;
				}else{
					$("#scheduleDate").val(d2);
					$("#startDateDiv").html("");
					errorFlag=0;
				}
			}
		 }
		 
		 
		 
		 if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   	if($("#day").val()==""){
				$("#birthdayDiv").html("Please select Day");
				errorFlag=1; 
			}else if($("#month").val()==""){
				$("#birthdayDiv").html("Please select Month");
				errorFlag=1; 
			}else if($("#year").val()==""){
				$("#birthdayDiv").html("Please Select Year");
				errorFlag=1;
			}else{
				var d4=new Date();
				var d5=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				var d6=new Date(d5);
				var startDate=$("#scheduleDate").val();
				var d7=new Date(startDate);
				if(d6<d4){
					$("#birthdayDiv").html("End Date Should be greater than now");
					errorFlag=1;
				}else if(d6<d7){
					$("#birthdayDiv").html("End Date Should be greater than Start Date");
					errorFlag=1;
				}else{
						$("#birthdayDiv").html("");
						errorFlag=0;
				}
			}
		 }
		 
		 
		 if(errorFlag==1){
		 	return false;
		 }else{
		 	$("#repeatErrorDiv").html("");
			$("#birthdayDiv").html("");
			$("#startDateDiv").html("");
			
			//Assign new data to hidden controls on a page
			$("#repeat2").val($("#repeat").val());
			$("#repeat_every2").val($("#repeat_every").val());
			var start_date='';
			
			var rType = document.smsPushForm.elements["reminder_type[]"];
			for(i=0;i<rType.length;i++)
			{
 				if(document.smsPushForm.reminder_type[i].checked){
					var mkk=document.smsPushForm.reminder_type[i].value;
					$("#reminder_type2").val(mkk);
				}
			}
			
			
			
			if($("#reminder_type2").val()=='sms'){
				if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
					start_date=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				
					if(document.getElementById("advOptionDiv").style.display=='none'){
						$("#advOptionDiv").show();
						$("#scheduleDate").attr("disabled",false);
						$("#advOptionTd").html('- '+msg_26);
					}
					$("#scheduleDate").val(start_date);
				
		 		}else{
					start_date="Current Time.";
					$("#scheduleDate").val('');	
				}	
			}else{
				$("#scheduleDate").val('');
				$("#birthdayfield").val($("#startDateColumn").val());
				start_date ="Pick from Selected Field";	
			}
				
			
			
			if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
				var end_date=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				$("#end_date").val(end_date);
		 	}
			
			var expression='';
			if($("#repeat").val()=='WEEK'){
				var rpDays = document.smsPushForm.elements["repeat_days[]"];
				for(i=0;i<rpDays.length;i++)
				{
 					if(document.smsPushForm.repeat_days[i].checked){
						expression = expression+document.smsPushForm.repeat_days[i].value+',';
					}
				}
				
				if(expression.length >0){
				 expression = expression.substring(0, expression.length - 1);
				 $("#repeat_days2").val(expression);
				}
			}else if($("#repeat").val()=='MONTH'){
				expression='Monthly on a day.';
			}else if($("#repeat").val()=='YEAR'){
				expression='Yearly on a day.';
			}else if($("#repeat").val()=='DAY'){
				expression='Daily.';
			}
			
			//Writting Summary for User preview
			if($("#repeat").val()==''){
				$("#scheduleDate").val('');
				var summary= 'Never Repeat.';
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}
		 }

		 
		 $("#repeatSummary").html(summary);
		 
		 //var brand = $("#brands input:radio:checked").val();
         //$("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

   });

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   }
   
   function showStartDate(){
   		$("#smsStartDateDiv").show();
		$("#birthdayStartDateDiv").hide();
		$("#birthdayStartDateLabel").hide();
		$("#smsStartDateLabel").show();	
   }
   
   function hideStartDate(){
   		$("#birthdayStartDateDiv").show();
		$("#smsStartDateDiv").hide();
		$("#birthdayStartDateLabel").show();
		$("#smsStartDateLabel").hide();	
   }
   
    function pickRepeat(){
   	if($("#repeat").val()=="DAY"){
		$("#repeatDiv").html("Day");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="WEEK"){
		$("#repeatDiv").html("Week");
		$("#repeatDaysRow").show();
		$("#repeat_every").val(1);
		$("#repeat_every").attr('disabled', true);
	}else if($("#repeat").val()=="MONTH"){
		$("#repeatDiv").html("Month");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="YEAR"){
		$("#repeatDiv").html("Year");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else{
		$("#repeatDiv").html("");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}
	
   }
   
   
   function changeNumberOfRepeat(){
   		if($("#repeat_every").val()>1){
			if($("#repeat").val()=="DAY"){
				$("#repeatDiv").html("Days");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="WEEK"){
				$("#repeatDiv").html("Weeks");
				document.getElementById(repeatDaysRow).style.display="block";
			}else if($("#repeat").val()=="MONTH"){
				$("#repeatDiv").html("Months");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="YEAR"){
				$("#repeatDiv").html("Years");
				document.getElementById(repeatDaysRow).style.display="none";
			}else{
				$("#repeatDiv").html("");
				document.getElementById(repeatDaysRow).style.display="none";
			}
		}
   }	
	</script>

<div id="spinner" class="spinner" style="display:none; vertical-align:middle;">
	<div id="spinner2" class="spinnerDiv">
	    <img id="img-spinner" src="{$BASE_URL_HTTP_ASSETS}images/ajax-loader.gif" alt="Loading"/>
	</div>
</div>
<div id="container">
<form name="smsPushForm" id="smsPushForm" method="post" action="" enctype="multipart/form-data" onsubmit="return validatSmspushVen();">
<input name="action" type="hidden" id="action" value="{$action}" />
<input name="filesize" type="hidden" id="filesize" value="" />
<input name="fileuploaded" type="hidden" id="fileuploaded" value=""/>
  <div class="column2-ex-left-2 column2_contents">
	<h1 style="margin-bottom:30px;">{$smarty.const.COMPOSE_FILE_SMS}</h1>
	<div id="errorDiv" {if $errorFlag=='1'} class='error_msg' {else} class='sucess_msg_2' {/if}
	{if $msg} style='display:block; margin-bottom:20px; margin-left:21px' {else} style='display:none; margin-left:21px;' {/if}> {$msg}	</div>
	
<!--  Content For Uploading Data start -->
 {if $action=='uploadFile'}
 
 <!--  Menu start here  -->
 <div class="second-nav"> 
     <ul>
    	<li  class="first-option">1.Upload a file</li>
    	<li >2.View Uploaded data</li>
    	<li >3.Create message</li>
    	<li> 4.Process/Preview </li>
   </ul>
</div>
	
<!--  Menu end here  -->
 
 
 
 <table  border="0">
  <tr>
    <td valign="top"><div class="btm-content">
 	<div class="file-upload"> </div>
 	<table width="650" border="0" style="margin-top:40px; margin-left:10px; margin-bottom:40px;">
      <tr>
        <td width="280" valign="top">{$smarty.const.UPLOAD_FILE_NAME} : </td>
        <td width="280"><span class="form_dinatrea standard-input" style="width:250px">
            <input type="file" name="file" id="file" value="{$fileuploaded}" class="form_dinatrea standard-input"/></span>
            <span id="fileDiv" class="form_error"></span></td>
			<td width="90" valign="top">
			<input type="image" src="templates/assets/images/file_browse.png" />
			</td>
      </tr>
    </table>
 	</div></td>
    <td valign="top"><div id="btm-help" class="btm-help">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;">{$smarty.const.VEN_COTENTHELP_FILESMS_STEP_01}</p>

	</div></td>
  </tr>
</table>

 
 	
 {/if}
<!--  Content For Uploading Data end  -->
	
	
	
	
<!--  Content For  viewing Uploaded Data start -->
{if $action=='uploadedData'}


<!--  Menu start here  -->        
<div class="second-nav"> 
     <ul>
    	<div class="top-navi2">
		<li>1.Upload a file</li>
    	<li  class="first-option2"> 2.View Uploaded data</li>
		</div>
    	<li >3.Create message</li>
    	<li> 4.Process/Preview </li>
   </ul>
</div>
<!--  Menu end here  -->



<table border="0">
  <tr>
    <td valign="top"><table border="0" style="margin-top:40px; margin-left:21px;">
  <tr>
    <td><strong>{$smarty.const.ROWS_TO_SHOW}</strong></td>
</tr>
 <tr>
    <td></td>
</tr>
<tr>
	<td>
			<table cellpadding="5"  cellspacing="2" style="background-color:#f8f8f8; font-size:11px;">
					<tr style="background-color:#E5E5E5;font-weight:bold; text-transform:uppercase;">
						{foreach name=outercols from=$arrColumn item=cols}
							{foreach name=innercols from=$cols item=colvalue}
								<td style="padding:5px;"><strong>{$colvalue}</strong></td>
							{/foreach}  				
  						{/foreach}	</tr>
				<tr class="TR_spc"><td colspan="{count($arrColumn)}" class="TD_spc"></td></tr>
  				{foreach name=outer from=$arrRecipient item=row}
				<tr>
					{foreach name=inner from=$row item=value}
					<td  style="padding:5px;">{$value}</td>
					{/foreach}</tr>
					<tr class="TR_spc"><td colspan="{count($arrColumn)}" class="TD_spc"></td></tr>
  				{/foreach}
			</table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="600" border="0">
      <tr>
        <td width="209"><strong>{$smarty.const.HEADER_COLUMN} : </strong></td>
        <td width="381"><input name="headerRow" type="checkbox" id="headerRow" value="1" {if $headerRow == '1'} checked="checked" {/if} style="float:left" onchange="refreshPage();"/></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="600" border="0">
      <tr>
        <td width="208" valign="top"><strong>{$smarty.const.RECIPIENT_COLUMN} :</strong></td>
        <td width="382">
		<span class="form_dinatrea standard-input">
		<select name="mobNoColumn" id="mobNoColumn" class="select">
                          <option value="" {if $mobNoColumn==''}selected{/if}>{$smarty.const.SELECT}</option>
                         {foreach name=outerMobCol from=$arrColumn key=keyvalue item=cols}
							{foreach name=innerMobCol from=$cols item=colvalue}
												<option value="{$keyvalue}" {if $mobNoColumn==$keyvalue}selected{/if}>{$colvalue}</option>
			{/foreach}  				
  						{/foreach}
                        </select><span id="MobileColumnDiv" class="form_error"></span></td>
      </tr>
    </table></td>
  </tr>
</table>

 
    <div class="footer-bottom">
             <p align="left" class="left-bottom"><img src="templates/assets/images/filesms_cancel-button.png" onclick="canceljob();" /></p>
             <p class="right-bottom"><input type="image" src="templates/assets/images/filesms_next.png" align="left"  class="back" /><img src="templates/assets/images/filesms_back.png" align="left" class="back" onclick="backtoone();" />
			 </p>
             
               <div class="clear"></div>
             </div>
 </td>
    <td valign="top"><div style=" margin-left:10px; margin-top:65px; width:220px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px; background:#f6f6f6;">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;">{$smarty.const.VEN_COTENTHELP_FILESMS_STEP_02}</p>
	</div></td>
  </tr>
</table>
 
{/if}	
<!--  Content For  viewing Uploaded Data end  -->	
  
  
  
  
  
  <!--  Content For  Creat Message start -->
  {if $action=='createMessage'}
  
  <!--  Menu start here  -->        
<div class="second-nav"> 
     <ul>
    	<div class="top-navi3">
			<li>1.Upload a file</li>
    		<li>2.View Uploaded data</li>
    		<li  class="first-option2">3.Create message</li>
		</div>
    	<li> 4.Process/Preview </li>
   </ul>
</div>
	
<!--  Menu end here  -->
  
  
  <table border="0">
  <tr>
    <td valign="top"><div class="btm-content">
 <div class="file-create-message"> </div>
 
 
 
 
 
 
 
 <div id="overlay" class="web_dialog_overlay"></div>
   
<div id="dialog" class="web_dialog">
	<div id="output"></div>
   <div style="text-align:right; margin-right:10px; margin-top:10px; display:inline; float:right;"><a href="#" id="btnClose">Close</a></div>
   <div style="float:left; display:inline;"><h2 style="margin-left:20px;">Advanced Settings</h2></div>
   <table width="440" border="0" align="center" cellpadding="10" cellspacing="10" style="margin-left:20px;">
 <tr>
 	<td width="110">Reminder Type:</td>
	<td width="330">
		<label><input name="reminder_type[]" id="reminder_type" type="radio" value="sms" checked  onclick="showStartDate();" />SMS Reminder</label>
		<label style="margin-left:15px;"><input name="reminder_type[]" id="reminder_type" type="radio" value="birthday" onclick="hideStartDate();" />Use File Dates</label>
	</td>
 </tr>
  <tr>
    <td valign="top" >Repeat</td>
    <td ><div>
	
	<select name="repeat" id="repeat" onchange="pickRepeat();"style="border:2px solid #ccc;" >
		<option value="">Never</option>
		<option value="DAY">Daily</option>
		<option value="WEEK">Weekly</option>
		<option value="MONTH">Monthly</option>
		<option value="YEAR">Yearly</option>
	</select>

	</div> <span id="repeatErrorDiv" class="form_error"></span></td>
  </tr>
  <tr>
    <td valign="top">Repeat Every</td>
    <td valign="bottom">
	<select id="repeat_every" name="repeat_every" onchange="changeNumberOfRepeat();" style="border:2px solid #ccc;" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
	</select>
	<label id="repeatDiv"></label>
	
	</td>
  </tr>
  <tr id="repeatDaysRow" style="display:none;">
    <td valign="top">Repeat Days</td>
    <td>
		<div>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Sunday" />S</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Monday" />M</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Tuesday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Wednesday" />W</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Thursday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Friday" />F</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Saturday" />S</label>
		</div>
	</td>
  </tr>
  <tr id="startDateRow">
  	<td><label id="smsStartDateLabel" style="display:block">Start Date</label> <label id="birthdayStartDateLabel" style="display:none">Date/Date of Birth</label></td>
	<td>
		<div id="smsStartDateDiv" style="display:block;">
		<select name="dayStart" id="dayStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							{section name=arrDay2 loop=31} 
							<option value="{$smarty.section.arrDay2.iteration}" {if $smarty.section.arrDay2.iteration==$day} selected{/if}>{$smarty.section.arrDay2.iteration}</option>
							{/section}
						</select>

				  
				  
				    	 <select name="monthStart" id="monthStart" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" {if $month==1} selected{/if}>Jan</option>
							<option value="2" {if $month==2} selected{/if}>Feb</option>
							<option value="3" {if $month==3} selected{/if}>Mar</option>
							<option value="4" {if $month==4} selected{/if}>Apr</option>
							<option value="5" {if $month==5} selected{/if}>May</option>
							<option value="6" {if $month==6} selected{/if}>Jun</option>
							<option value="7" {if $month==7} selected{/if}>Jul</option>
							<option value="8" {if $month==8} selected{/if}>Aug</option>
							<option value="9" {if $month==9} selected{/if}>Sep</option>
							<option value="10" {if $month==10} selected{/if}>Oct</option>
							<option value="11" {if $month==11} selected{/if}>Nov</option>
							<option value="12" {if $month==12} selected{/if}>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="yearStart" id="yearStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							{section name=arrYear2 loop=10} 
							<option value="{($currentYear - $smarty.section.arrYear2.iteration)}" {if $year==($currentYear - $smarty.section.arrYear2.iteration)} selected{/if}>{($currentYear - $smarty.section.arrYear2.iteration)}</option>
							{/section}
						</select>
						 
					
						 <select name="hourStart" id="hourStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							{section name=fooStart start=0 loop=24 step=1} 
								<option value="{$smarty.section.fooStart.index}">{$smarty.section.fooStart.index}</option>
							{/section} 
						 </select>
					
					 
					 
					<!--	 <select name="minutesStart" id="minutesStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							{section name=foo2Start start=0 loop=60 step=1}
								<option value="{$smarty.section.foo2Start.index}">{$smarty.section.foo2Start.index}</option>
							{/section}
						 </select> -->
					 </div>
					 <div id="birthdayStartDateDiv" style="display:none;">
						
						<select name="startDateColumn" id="startDateColumn" style="border:2px solid #ccc; width:150px;">
                  			<option value="" {if $placeHolder==''}selected{/if}>{$smarty.const.SELECT}</option>
                         	{foreach name=outerPlaceholder2 from=$arrColumn key=keyvalue item=cols}
							{foreach name=inneplaceholder2 from=$cols item=colvalue}
                  			<option value="{$keyvalue}">{$colvalue}</option>
							{/foreach}  				
  							{/foreach}
                		</select>
					 </div>	 
						 
						 <span id="startDateDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td valign="top">End Date</td>
    <td>
	
	
				    	 <select name="day" id="day" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							{section name=arrDay loop=31} 
							<option value="{$smarty.section.arrDay.iteration}" {if $smarty.section.arrDay.iteration==$day} selected{/if}>{$smarty.section.arrDay.iteration}</option>
							{/section}
						</select>

				  
				  
				    	 <select name="month" id="month" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" {if $month==1} selected{/if}>Jan</option>
							<option value="2" {if $month==2} selected{/if}>Feb</option>
							<option value="3" {if $month==3} selected{/if}>Mar</option>
							<option value="4" {if $month==4} selected{/if}>Apr</option>
							<option value="5" {if $month==5} selected{/if}>May</option>
							<option value="6" {if $month==6} selected{/if}>Jun</option>
							<option value="7" {if $month==7} selected{/if}>Jul</option>
							<option value="8" {if $month==8} selected{/if}>Aug</option>
							<option value="9" {if $month==9} selected{/if}>Sep</option>
							<option value="10" {if $month==10} selected{/if}>Oct</option>
							<option value="11" {if $month==11} selected{/if}>Nov</option>
							<option value="12" {if $month==12} selected{/if}>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="year" id="year" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							{section name=arrYear loop=10} 
							<option value="{($currentYear - $smarty.section.arrYear.iteration)}" {if $year==($currentYear - $smarty.section.arrYear.iteration)} selected{/if}>{($currentYear - $smarty.section.arrYear.iteration)}</option>
							{/section}
						</select>
						 
					
						 <select name="hour" id="hour" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							{section name=foo start=0 loop=24 step=1} 
								<option value="{$smarty.section.foo.index}">{$smarty.section.foo.index}</option>
							{/section} 
						 </select>
					
					 
					 
						<!-- <select name="minutes" id="minutes" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							{section name=foo2 start=0 loop=60 step=1}
								<option value="{$smarty.section.foo2.index}">{$smarty.section.foo2.index}</option>
							{/section}
						 </select>  -->
					 		 
						 
						 <span id="birthdayDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input id="btnSubmit" type="button" value="Ok" style="border:0px solid #93278F; width:100px; height:31px; background:url(templates/assets/images/btn_bg.png) no-repeat left; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif; font-size:1.2em; vertical-align:text-top; text-align:center; color:#FFFFFF;" /></td>
  </tr>
</table>

</div>
 
 
 
 
 
 
 
 	<table width="600" border="0" cellpadding="10" cellspacing=4" style="margin-top:40px; margin-left:10px; margin-bottom:20px;">
      <tr>
        <td width="486"><table width="576" border="0">
            <tr>
              <td valign="top" width="200"><strong>{$smarty.const.SENDER_NAME}:</strong></td>
              <td align="left" valign="bottom">
          <div align="left" style="width:40px; margin-bottom:4px;"><span class="form_dinatrea standard-input">
                  <select name="senderid" id="senderid" class="select">
                    <option value="">{$smarty.const.SELECT}</option>
                    
                                                  
					{section name=senderid loop=$arrSenderid}                
                        
                          
                    <option value="{$arrSenderid[senderid].id}" {if $senderid==$arrSenderid[senderid].id}selected{/if}>{$arrSenderid[senderid].senderid}</option>
                    
                                           
					{/section}
                        
                  </select>
                  <span id="senderidDiv" class="form_error"></span></div><img alt="Help" src="{$BASE_URL_HTTP_ASSETS}images/filesms_What-this.png" width="20" height="20" id="senderidtip" title="{$smarty.const.VEN_SENDERID_TEXT_1}" ><a href="ven_senderid.php" style=" margin-left:20px;">{$smarty.const.REQUEST_NEW}</a></td>
            </tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td><strong>{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_8}</strong></td>
				<td><div id="smstemplate"  style="width:40px"><span class="form_dinatrea standard-input">
			<select name="templatesms" id="smstemplate" onchange="addsmstemplate();" style="margin-right:20px;">
               <option value="">{$smarty.const.SELECT}</option> 
				{section name=sms loop=$arrsmstemplate}
				<option value="{$arrsmstemplate[sms].message}">{$arrsmstemplate[sms].sms_title}</option>    
				{/section}
			</select>
		</span></div><img alt="Help" src="{$BASE_URL_HTTP_ASSETS}images/filesms_What-this.png" width="20" height="20" id="smsTemplatetip" title="Content of Help for sms templates goes here"><a href="JavaScript:newPopup('ven_sms_template.php');" style=" margin-left:20px;">{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_9}</a></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
            <tr>
              <td width="200" valign="top"><strong>{$smarty.const.PLACE_HOLDER}: </strong></td>
              <td><span class="form_dinatrea standard-input" style="margin-bottom:5px;">
                <select name="placeHolder" id="PlaceHolder" class="select" onchange="addPlaceholder();">
                  <option value="" {if $placeHolder==''}selected{/if}>{$smarty.const.SELECT}</option>
                  
                         {foreach name=outerPlaceholder from=$arrColumn item=cols}
							{foreach name=inneplaceholder from=$cols item=colvalue}
								
                  <option value="{$colvalue}">{$colvalue}</option>
                  
							{/foreach}  				
  						{/foreach}
                        
                </select>
              </span><img alt="Help" src="{$BASE_URL_HTTP_ASSETS}images/filesms_What-this.png" width="20" height="20" id="placeholderTooltip" title="Content for Placeholder tip" ></td>
            </tr>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
                <tr>
                    <td valign="top" width="200"><strong>{$smarty.const.MESSAGE_BODY}:</strong></td>
                    <td><textarea name="textMessage" id="textMessage" cols="40" rows="6" class="textarea">{$textMessage}</textarea>
                        <span id="textMessageDiv" class="form_error" style="width:300px"></span></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><div style="width:100%; float:left;">Msgs. / Chars. Remaining <span id="charCount">( 0 ) 0</span></div>
                        <div style="width:70%; float:left;" id="messageCount"></div></td>
                </tr>

                <tr><td><strong>Message Type :</strong> </td>
                    <td><input type="radio" name="m_type" checked value="m_text">Text
                        <input type="radio" name="m_type" value="m_unicode">Unicode</td></tr>
                <tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>

            <tr>
              <td valign="top" width="200"><div align="left"><a id="advOptionTd" style="cursor:pointer; font-weight:bold;" onclick="javascript: showHideAdvOption();"> {if $scheduleDate && $scheduleDate!=''}
                - {$smarty.const.SCHEDULE_DATE}
                {else}
                + {$smarty.const.SCHEDULE_DATE}
                {/if} </a></div></td>
              <td><div id="advOptionDiv" {if $scheduleDate && $scheduleDate!=''}style="display:block" {else}style="display:none"{/if}> <span class="form_dinatrea standard-input">
                  <input type="text" name="scheduleDate" id="scheduleDate" value="{$scheduleDate}" />
              </span> </div></td>
            </tr>
			<tr>
              <td><input name="repeat2" type="hidden" id="repeat2" value="" />
        		<input name="repeat_every2" type="hidden" id="repeat_every2" value="" />
        		<input name="repeat_days2" type="hidden" id="repeat_days2" value="" />
       	 		<input name="end_date" type="hidden" id="end_date" value="" />
				<input name="reminder_type2" type="hidden" id="reminder_type2" value="" />
        		<input name="birthdayfield" type="hidden" id="birthdayfield" value="" />
			</td>
              <td>&nbsp;</td>
            </tr>
			<tr>
      <td valign="top"><div id="btnShowModal" title="Click Me to view advanced Settings" style="cursor:pointer; font-weight:bold;">+ Custom Reminders</div></td>
      <td><div id="repeatSummary"></div></td>
    </tr>

        </table></td>
      </tr>
      <tr>
        <td></td>
      </tr>
    </table>
  </div>
  
  
  <div class="footer-bottom">
       <p align="left" class="left-bottom"><img src="templates/assets/images/filesms_cancel-button.png" onclick="canceljob();" /></p>
       <p class="right-bottom"><input type="image" src="templates/assets/images/filesms_next.png" align="left"  class="back" />  <img src="templates/assets/images/filesms_back.png" align="left" class="back" onclick="backtotwo();"/>
        </p>
         <div class="clear"></div>
  </div></td>
    <td valign="top"><div id="btm-help" class="btm-help">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;">{$smarty.const.VEN_COTENTHELP_FILESMS_STEP_03}</p>

	</div></td>
  </tr>
</table>

  
  
  
  
	{/if}  
  <!--  Content For  Creat Message end -->
  
  
  
  
  <!--  Content For  proccess/preview start -->
  {if $action=='SMSpreview'}
  
  <!--  Menu start here  -->
<div class="second-nav"> 
     <ul>
    	<div class="top-navi4">
			<li>1.Upload a file</li>
    		<li>2.View Uploaded data</li>
    		<li>3.Create message</li>
    		<li  class="first-option2"> 4.Process/Preview </li>
		</div>
   </ul>
</div>
<!--  Menu end here  -->
  
  
  
  <div class="content">
        <a href="#"><img src="templates/assets/images/filesms_summy.png" class="summy"/></a>
        <ul> 
            <li class="one">{$smarty.const.TOTAL_RECORDS}<br /><span class="total">{$TotalContact}</span></li>
            <li class="two">{$smarty.const.INVALID_RECORDS}<br /><span  class="invalid">{$CountInvalidContact}</span></li>
			<li class="two">Duplicate Records<br />
		  <span  class="invalid">{$CountDuplicateContact}</span></li>
            <li class="three">{$smarty.const.VALID_RECORDS}<br /><span  class="valid">{$CountValidContact}</span></li>
			<li class="four">{$smarty.const.CREDITS_BALANCE}<br /><span  class="smscount">{$crdBalance}</span></li>
			<li class="four">{$smarty.const.CREDITS_COSTS}<br /><span  class="smscount">{$smsCount}</span></li>
        </ul>   
  </div>
  
  <div class="smsPreview">
      <img src="templates/assets/images/filesms_center.png" class="center-job" />
       {if $repeat=='sms'}
	   <table border="0" style="margin-top:20px;" width="680">                        
            <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;"> 
				 <td class="row-1"><strong>{$smarty.const.TEXT_MESSAGE}</strong></td>
				 <td class="row-2"><strong>{$smarty.const.RECIPIENTS}</strong></td>  
                 <td class="row-2"><strong>{$smarty.const.SENDER_NAME}</strong></td> 
				 <td class="row-3"><strong>{$smarty.const.CHARACTER_COUNT}</strong></td> 
				 <td class="row-4"><strong>{$smarty.const.SMS_COUNT}</strong></td> 
			</tr>                                                    
                  {section name=sample loop=$arrSampleSMS}
            {if $arrSampleSMS[sample].0 != ""}
			<tr class="row-body"> 
					<td class="row-1">{$arrSampleSMS[sample].0|wordwrap:40:"<br />\n"}</td>
					<td class="row-2">{$arrSampleSMS[sample].1}</td>  
					<td class="row-2">{$arrSampleSMS[sample].2}</td> 
					<td class="row-3">{$arrSampleSMS[sample].3}</td>  
					<td class="row-4">{$arrSampleSMS[sample].4}</td>   
			</tr>
			{/if}
				{/section}     
       </table>
	   {else}
	   		<table border="0" style="margin-top:20px;" width="680">                        
            <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;"> 
				 <td class="row-1"><strong>{$smarty.const.TEXT_MESSAGE}</strong></td>
				 <td class="row-2"><strong>{$smarty.const.RECIPIENTS}</strong></td>  
                 <td class="row-2"><strong>{$smarty.const.SENDER_NAME}</strong></td> 
				 <td class="row-3"><strong>{$smarty.const.CHARACTER_COUNT}</strong></td> 
				 <td class="row-4"><strong>{$smarty.const.SMS_COUNT}</strong></td>
				 <td class="row-5"><strong>{$smarty.const.BIRTH_DATE}</strong></td> 
			</tr>                                                    
                  {section name=sample loop=$arrSampleSMS}
            {if $arrSampleSMS[sample].0 != ""}
			<tr class="row-body"> 
					<td class="row-1">{$arrSampleSMS[sample].0|wordwrap:40:"<br />\n"}</td>
					<td class="row-2">{$arrSampleSMS[sample].1}</td>  
					<td class="row-2">{$arrSampleSMS[sample].2}</td> 
					<td class="row-3">{$arrSampleSMS[sample].3}</td>  
					<td class="row-4">{$arrSampleSMS[sample].4}</td>
					<td class="row-5">{$arrSampleSMS[sample].5}</td>   
			</tr>
			{/if}
				{/section}     
       </table>
	   {/if}
  </div>
             
             
      <div class="footer-top">
           <div style="margin:20px 0 20px 20px;">
             <img src="templates/assets/images/filesms_save-job.png" class="save-job"/>
             <table width="500" border="0">
                  <tr>
                    <td width="150">{$smarty.const.JOB_NAME}:</td>
                    <td width="300"><span class="form_dinatrea standard-input">
                      <input type="text" name="jobname" id="jobname" value="{$jobname}" /></td>
                  </tr>
                </table>
        	</div>
          <div class="clear"></div>
     </div>       
        
        
      <div class="footer-bottom3">
       <p align="left" class="left-bottom"><img src="templates/assets/images/filesms_cancel-button.png" onclick="canceljob();" /></p>
       <p class="right-bottom">
	   {if $CountValidContact !=0}
       {if $smsCount <= $crdBalance}
	   <input type="image" src="templates/assets/images/file_send.png" align="left" class="back"/> 
	   {/if} 
	   {/if}
	   <img src="templates/assets/images/filesms_back.png" align="left" class="back" onclick="backtothree();"/>
        </p>
         <div class="clear"></div>
  </div>  
			 
  
  
  {/if}
  <!--  Content For  proccess/preview start -->
  </div>
<div class="clear"></div>
</form>
</div>
{literal}
    
    <script type="text/javascript">
	//<!--
	
	function addsmstemplate() {
		document.smsPushForm.textMessage.value=document.smsPushForm.smstemplate.value;
	}

		function backtothree(){
		 	document.smsPushForm.action.value='backTo3';
			if(document.smsPushForm.action.value=='backTo3'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		
		 function backtotwo(){
		 	document.smsPushForm.action.value='backTo2';
			if(document.smsPushForm.action.value=='backTo2'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function backtoone(){
		 	document.smsPushForm.action.value='backTo1';
			if(document.smsPushForm.action.value=='backTo1'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function refreshPage(){
		 	document.smsPushForm.action.value='refresh';
			if(document.smsPushForm.action.value=='refresh'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function canceljob(){
		 	document.smsPushForm.action.value='cancel';
			var con = confirm("Are You Sure ?");
			if (con ==true)
  			{
  				if(document.smsPushForm.action.value=='cancel'){
					$('#spinner').show();
					document.smsPushForm.submit();
				}
			}
		 }
		 
		 function SendForm(){
			$('#spinner').show();
			document.smsPushForm.submit();
		}
		
		function addPlaceholder(){
			var plceholder = document.smsPushForm.placeHolder.value;
			plceholder	='{'+plceholder+'}';
			insertAtCursor(document.smsPushForm.textMessage, plceholder);
			
		}
		
		$('#file').bind('change', function() {
    		var filesize = this.files[0].size;
			document.smsPushForm.fileuploaded.value = document.smsPushForm.file.value;
			document.smsPushForm.filesize.value = filesize;
		});
		
		function validatSmspushVen(){
			var action = document.smsPushForm.action.value;
			if(action=='uploadFile'){
				if($("#file").val()==''){
					$("#fileDiv").html("{/literal}{$smarty.const.UPLOAD_FILE_MSG_1}{literal}");
					return false;
				}else if($("#file").val()!=''){
					var filename = document.getElementById("file").value;
   					var ext = getExt(filename);
   					if(ext == "txt" || ext == "csv" || ext == "xls" || ext == "xlsx"){
    					var uplFileSize=document.smsPushForm.filesize.value;
						if(uplFileSize>3145728 && ext != "xlsx"){
						 $("#fileDiv").html("{/literal}{$smarty.const.UPLOAD_FILE_MSG_2}{literal}");
							return false;
						}else if(uplFileSize>307200 && ext == "xlsx"){
						 $("#fileDiv").html("{/literal}{$smarty.const.UPLOAD_FILE_MSG_5}{literal}");
							return false;
						}else{
							$("#fileDiv").html("");
							$('#spinner').show();
							return true;
						}
					}else{
						$("#fileDiv").html("{/literal}{$smarty.const.UPLOAD_FILE_MSG_3}{literal}");
						return false;
					}
				}
			}else if(action=='uploadedData'){
				if($("#mobNoColumn").val()==''){
					$("#MobileColumnDiv").html("{/literal}{$smarty.const.RECIPIENT_COLUMN}{literal}");
					return false;
				}else{
					$("#fileDiv").html("");
					$('#spinner').show();
					return true;
				}
			}else if(action=='createMessage'){
				if($("#senderid").val()==''){
        			$("#senderidDiv").html("{/literal}{$smarty.const.SELECT_SENDER_NAME}{literal}");
					return false;
				}

                if(  $("input[type='radio'][name='m_type']:checked").val() == "m_unicode"){
                    $("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE_ONLY}{literal}");
                    alert("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE_ONLY}{literal}");
                    return false;
                }

				if($("#textMessage").val()==''){
					$("#textMessageDiv").html("{/literal}{$smarty.const.ENTER_TEXT_MASSAGE}{literal}");
					return false;
				}else{
					$("#senderidDiv").html("");
					$("#textMessageDiv").html("");
					$('#spinner').show();
					return true;
				}
			}else{
				$("#senderidDiv").html("");
				$("#textMessageDiv").html("");
				$('#spinner').show();
				return true;
			}
		}
		
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
	function insertAtCursor(myField, myValue) {
		//IE support
		if (document.selection) {
			myField.focus();
			sel = document.selection.createRange();
			sel.text = myValue;
		}
		//MOZILLA/NETSCAPE support
		else if (myField.selectionStart || myField.selectionStart == '0') {
			var startPos = myField.selectionStart;
			var endPos = myField.selectionEnd;
			myField.value = myField.value.substring(0, startPos)
+ myValue
+ myField.value.substring(endPos, myField.value.length);
		} else {
			myField.value += myValue;
		}
	}


function getExt(filename) {
   var dot_pos = filename.lastIndexOf(".");
   if(dot_pos == -1)
      return "";
   return filename.substr(dot_pos+1).toLowerCase();
}

$('#mydiv').tooltipsy(
  {
    show: function (e, $el)
     {
        $el.css({ 'z-index':'1000000'})
     }
   }
);

        //-->
     </script>
	 
	 
	 <script type="text/javascript">
	$(document).ready(function(){
	    $("#spinner").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
	    });
	 
	     });
		 
		 window.onload=function() {
 			$("#myBalance").html("{/literal}{$venCreditBalance}{literal}");
		}
	</script>


{/literal}
