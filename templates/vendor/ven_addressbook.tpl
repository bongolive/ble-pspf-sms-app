<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 20px;
	text-align: left;
		}


.row-2{
 width:200px;
 text-align: left;
}

.row-3{
 width:100px;
 text-align: left;
}

.row-4{
 width:110px;
 text-align: center;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}
</style><div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.VEN_ADDRESSBOOK_DASHBOARD}</h1>
	<div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}> {$msg} </div>
    
	<table align="center" cellpadding="0" cellspacing="0">
	<tr>       
        <td>
            <form name="addressbookForm" method="POST" action="">
                <table cellspacing="10">
                    <tr>
                        <td>{$smarty.const.CREATE_GROUP} : </td>
                        <td><span class="form_dinatrea standard-input"><input type="text" name="addressbook" id="addressbook" value="{$addressbook}"></span></td>
                    </tr>
		    <tr>
                        <td>{$smarty.const.DESCRIPTION} : </td>
                        <td><span class="form_dinatrea standard-input"><input type="text" name="description" id="description" value="{$discription}"></span></td>
                  </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input type="submit" name="save" id="save" value="{$smarty.const.SAVE}" class="editbtn">
                            <input type="hidden" name="action" id="action" value="add">
                        </td>
                    </tr>
                    
                </table>
          </form>
 <br>
		<form name="addressbookForm2"  id="addressbookForm2" method="POST" action="" >

			
	<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
			<table style="margin-right:10px;">
			<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
			    <td class="row-3">
			    	<input  type="checkbox" name="addbookIdMaster" id="addbookIdMaster" />All</td>
			    <td class="row-2">{$smarty.const.VEN_DASHBOARD_GROUP_NAME}</td>
			    <td class="row-3">{$smarty.const.CONTACTS}</td>
				<td class="row-2">{$smarty.const.DESCRIPTION}</td>
			    <td class="row-2">&nbsp;</td>
			</tr>
			{if $arrAddressbook}
			{section name=addressbook loop=$arrAddressbook}
			<tr class="row-body">
			    <td class="row-1"><input class="ChildCheck" type="checkbox" name="addbookId[]" id="addbookId" value="{$arrAddressbook[addressbook].id}" ></td>
			    <td class="row-2">{$arrAddressbook[addressbook].addressbook}</td>
			    <td class="row-3">{$arrAddressbook[addressbook].contacts_count}</td>
				<td class="row-2">{$arrAddressbook[addressbook].description}</td>
			    <td class="row-2 content-link"><a href="{$manageAddbookUrl}?bid={$arrAddressbook[addressbook].id}">{$smarty.const.VEN_DASHBOARD_MANAGE_CONTACTS}</a>
				</td>
			</tr>
			{/section}
			<tr  style="background-color:#f8f8f8;">
				<td colspan="5" class="content-link">&nbsp;</td>
			</tr>
			<tr  style="background-color:#f8f8f8;">
			    <td align="right" colspan="5" class="content-link">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}</td>
			</tr>

			</table>
			
			</div>
          <div class="clear"></div>
     </div>  
			<table border= "0" cellspacing="5">
				<tr>
					<td>
						
					</td>
					<td>
						<input type="button" name="delete" id="delete" value="{$smarty.const.DELETE}" onclick="javascript: deleteAddbook(); " class="redbtn">
					</td>
					<td>
						
					</td>
					<td>
						<input type="hidden" name="action" id="action" value="">
						<input type="hidden" name="selectedBox" id="selectedBox" value="addressbooks">
						
					</td>
				</tr>
		    </table>	

			{else}
			<table>
			<tr>
			    <td colspan="5" align="center"> No records </td>
			</tr>
			</table>
			{/if}
					
	</form>
            <!-- middle body end-->
        </td>
    </tr>
</table>
  </div>
      <div class="clear"></div>
  </div>
{literal}
	<script type="text/javascript">
	$(document).ready(function(){
		//alert('Hello');
		$('#addbookIdMaster').bind('click',function(){
			//alert('Clicked');
			var p_check =  $(this).is(":checked");

			$('input[class="ChildCheck"]').each(function(){
					//console.log(p_check);
					$(this).attr('checked',p_check);
				//if(this.checked == true)
				//flag = 1;

			});


		});
	});
	<!--
		function deleteAddbook(){
			var flag=0; 
			/*
			for (var x = 0; x < document.addressbookForm2.addbookId.length; x++)
			{
			    if(document.addressbookForm2.addbookId[x].checked == true){
				
					flag = 1;
			    }
			    
			}*/

			$(':checkbox').each(function(){
					
				if(this.checked == true)
				flag = 1;

			});

			
			if(flag == 1){

				if(confirm("{/literal}{$smarty.const.SURE_DELETE_GROUP}{literal}")){
					
					document.addressbookForm2.action.value='delete';
					document.addressbookForm2.submit();
					
				}
			}else if (flag == 0){
				alert("{/literal}{$smarty.const.SELECT_GROUP}{literal}");
				return false;
			}
			
		}

		function sendSms(send,form_name,url){
			
			var flag = 0;

			if(send=='selected'){
				$(':checkbox').each(function(){
					
					if(this.checked == true)
					flag =1;
				 });
				 if(flag==1){
					document.addressbookForm2.action = url;
					document.addressbookForm2.submit();
				}else{
					alert("{/literal}{$smarty.const.SELECT_CONTACTS}{literal}");
					return false;

				}

			}else if(send=='all'){
				  $(':checkbox').each(function(){
					
					this.checked = true;
				  }); 

				document.addressbookForm2.action = url;
				document.addressbookForm2.submit();
			}		
		}
		
		
		function UncheckDefault()
			{
			var i;
			for (i=0;i< document.addressbookForm2.addbookId.length;i++)
				{
					if(document.addressbookForm2.addbookId[i].checked == true)
						{
							var group;
							group=document.addressbookForm2.addbookId[i].value;
							if(group=='Default'){
								document.addressbookForm2.addbookId[i].checked == faalse;
							}
							break;
						}
					
				}
				
			}
	-->	
	</script>
{/literal}