<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 200px;
	text-align: left;
		}


.row-2{
 width:300px;
 text-align: left;
}

.row-3{
 width:100px;
 text-align: left;
}

.row-4{
 width:200px;
 text-align: left;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}

</style>
<script type="text/JavaScript">
<!--
function MM_goToURL() { //v3.0
  var answer=confirm("Do you realy want to delete this template?");
  if(answer==true){
  	var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  	for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
  }
}
//-->
</script>

<div id="container">
    <h1>Manage SMS template </h1>
	<div>
	<div id="errorDiv"></div>
	{if $action=='' || $action=='save'}
	<form name="smstemplate" action="{$selfUrl}" method="POST">
		<table cellspacing="10" width="600" style="margin-left:0px;"> 
			<tr>
			  <td width="138" valign="top"><strong>Message Title</strong> </td>
			  <td width="426"><span class="form_dinatrea standard-input">
		      <input name="sms_title" type="text" id="sms_title" size="40" /></span></span><span id="sms_titleDiv" class="form_error" style="width:300px"></span></td>
		  </tr>
			<tr>
				<td valign="top"><strong>Text Message:</strong></td>
				<td><span class="form_dinatrea standard-input" style="width:250px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="4"></textarea></span><span id="textMessageDiv" class="form_error" style="width:300px"></span>
				</td>
			</tr>
			<tr>
			  <td valign="top"><strong>{$smarty.const.COUNTER}</strong></td>
			  <td><div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
				<div style="width:70%; float:left;" id="messageCount"></td>
		  </tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="Save" id="Save" value="Save" class="editbtn" onclick=" return validatemsmTemplates();">
					<input type="hidden" name="action" id="action" value="save">
					<input type="hidden" name="ajaxUrl" id="ajaxUrl" value="{$ajaxUrl}">				</td>
			</tr>
			<tr>
			  <td></td>
			  <td></td>
		  </tr>
		</table>
		
	<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
       <!-- <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	    <table style="margin-right:10px;">
          <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
            <td class="row-1">SMS Title </td>
            <td class="row-2">Message</td>
            <td class="row-1">Date Created</td>
			<td class="row-3">Edit</td>
			<td class="row-3">Delete</td>
          </tr>
		  {if $arrsmstemplate}
		{section name=sms loop=$arrsmstemplate}	
          <tr class="row-body">
            <td class="row-1">{$arrsmstemplate[sms].sms_title}</td>
            <td class="row-2">{$arrsmstemplate[sms].message}</td>
            <td class="row-1">{$arrsmstemplate[sms].created}</td>
			<td class="row-3"><a href="ven_sms_template.php?templateid={$arrsmstemplate[sms].template_id}&amp;action=edit"><img src="{$BASE_URL_HTTP_ASSETS}images/edit.png" alt="edit " border="0"  /></a>
		    <input name="smsid[]" type="hidden" id="smsid" value="{$arrsmstemplate[sms].template_id}" /></td>
            <td class="row-3"><div align="center"><img src="{$BASE_URL_HTTP_ASSETS}images/delete.png" alt="delete" onclick="MM_goToURL('parent','ven_sms_template.php?templateid={$arrsmstemplate[sms].template_id}&action=delete');return document.MM_returnValue" /></div></td>
          </tr>
		  {/section}
		  {else}
		  <tr>
		  	<td colspan="5">No Record found</td>
		  </tr>
		  {/if}
		  <tr style="background-color:#f8f8f8;">
			<td align="right" colspan="5" class="content-link" style="padding:10px 0;">
			{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}
			</td>
		</tr>
      </table>
	  </div>
          <div class="clear"></div>
     </div>
      <p>&nbsp;</p>
	</form>	
	
	{else if $action=='edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST">	
		<table cellspacing="10" width="600" id="myTable"> 
			<tr>
			  <td width="134" valign="top"><strong>Message Title:</strong> </td>
			  <td width="430"><span class="form_dinatrea standard-input">
		      <input name="sms_title" type="text" id="sms_title" size="40" value="{$arrsmstemp[0].sms_title}" /></span><span id="sms_titleDiv" class="form_error" style="width:300px"></span></td>
		  </tr>
			<tr>
				<td valign="top"><strong>Text Message:</strong></td>
				<td><span class="form_dinatrea standard-input" style="width:250px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="4">{$arrsmstemp[0].message}</textarea></span><span id="textMessageDiv" class="form_error" style="width:300px"></span>
				</td>
			</tr>
			<tr>
			  <td valign="top"><strong>{$smarty.const.COUNTER}:</strong></td>
			  <td><div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
				<div style="width:70%; float:left;" id="messageCount"></td>
		  </tr>
			<tr>
				<td></td>
			  <td>
					<input type="submit" name="update" id="update" value="update" class="editbtn" onclick="onclick=" return validatemsmTemplates();";">
					<input type="hidden" name="action" id="action" value="update">
					<input type="hidden" name="ajaxUrl" id="ajaxUrl" value="{$ajaxUrl}">				<input name="templateid" type="hidden" id="templateid"  value="{$templateid}"/></td>
			</tr>
		</table>
	</form>	
	
{/if}
    </div>
</div>

<script type="text/javascript">
	function validatemsmTemplates(){
		var errorFlag=0;
		if($("#sms_title").val()==''){
			$("#sms_titleDiv").html("Message Title should not be empty");
			errorFlag=1;
		}else{
			$("#sms_titleDiv").html("");
		}
		
		if($("#textMessage").val()==''){
			$("#textMessageDiv").html("Text Message should not be empty");
			errorFlag=1;
		}else{
			$("#textMessageDiv").html("");
		}
		
		if(errorFlag==0){
			return true;
		}else{
			return false;
		}
	}
</script>