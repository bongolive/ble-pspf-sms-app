
<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 150px;
	text-align: left;
		}


.row-2{
 width:250px;
 text-align: left;
}

.row-3{
 width:100px;
 text-align: left;
}

.row-4{
 width:110px;
 text-align: center;
}

.row-5{
 width:100px;
 text-align: right;
}

.row-6{
 width:120px;
 text-align: left;
 padding:5px;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}

</style>
<div id="container">
    <h1>Client Management</h1>
<form id="manageUser" name="manageUser" method="post" action="">
<input type="hidden" name="action" id="action" value="{$action}" />
<input type="hidden" name="vendor_id" id="vendor_id" value="{$id}" />
{if $action=='search'}
  <table width="100%" border="0">
    <tr>
      <td><table cellspacing="10">
          <tr>
            <td>Client Name: </td>
            <td><span class="form_dinatrea standard-input">
              <select name="vendorName" id="vendorName">
                <option value="">Select</option>
					{section name=vendors loop=$arrVendors}
                <option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}>
					{$arrVendors[vendors].name}</option>
					{/section}
              </select>
            </span> </td>
          </tr>
          <tr>
            <td>Client Type:</td>
            <td><span class="form_dinatrea standard-input">
              <select name="vendor_type" id="vendor_type" >
                <option value="" >Select</option>
                <option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
                <option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
              </select>
            </span> </td>
          </tr>
          <tr>
            <td>Username</td>
            <td><span class="form_dinatrea standard-input">
			<input type="text" name="username" id="username" value="{$username2}"></span></td>
          </tr>
          <tr>
            <td>Mobile</td>
            <td><span class="form_dinatrea standard-input">
			<input type="text" name="mob_no" id="mob_no" value="{$mob_no}"></span></td>
          </tr>
          <tr>
            <td>E-Mail</td>
            <td><span class="form_dinatrea standard-input">
			<input type="text" name="email" id="email" value="{$email}"></span></td>
          </tr>
          <tr>
            <td>Registration Start Date: </td>
            <td><span class="form_dinatrea standard-input">
			<input type="text" name="startDate" id="startDate" value="{$startDate}"></span></td>
          </tr>
          <tr>
            <td>Registration End Date: </td>
            <td><span class="form_dinatrea standard-input">
				<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span></td>
          </tr>
          <tr>
            <td>Status:</td>
            <td><span class="form_dinatrea standard-input">
			<select name="status" id="status" class="select">
                <option value="">Select</option>
                <option value="active" {if $status=='active'}selected{/if}>Active</option>
                <option value="not active" {if $status=='not active'}selected{/if}>Not active</option>
                <option value="confirmed" {if $status=='confirmed'}selected{/if}>Confirmed</option>
                <option value="not confirmed" {if $status=='not confirmed'}selected{/if}>Not Confirmed</option>
                <option value="deleted" {if $status=='deleted'}selected{/if}>Deleted</option>
              </select></span></td>
          </tr>
          <tr>
            <td></td>
            <td><input type="submit" name="search" id="search" value="Search" class="editbtn" />            </td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>
	  
	  
	<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
       <!-- <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
	  <table style="margin-right:10px;">
          <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
            <td class="row-1">Client Name</td>
			<td class="row-1">Username</td>
            <td class="row-1">Mobile</td>
            <td class="row-2">E-Mail</td>
			<td class="row-3">Credits</td>
            <td class="row-3">Status</td>
            <td class="row-3">Edit</td>
          <!--  <td class="row-3">&nbsp;</td> -->
          </tr>
          {if $arrVendorList}
		{section name=ven loop=$arrVendorList}
		  <tr class="row-body">
            <td class="row-1">{$arrVendorList[ven].name}</td>
			<td class="row-1">{$arrVendorList[ven].username}</td>
            <td class="row-1">{$arrVendorList[ven].mob_no}</td>
            <td class="row-2">{$arrVendorList[ven].email}</td>
			<td class="row-3"><a href="ven_add_credit.php?id={$arrVendorList[ven].id}&action=edit" title="Edit" style='text-decoration:none;color:green'>{$arrVendorList[ven].credit}</a></td>
            <td class="row-3">{if $arrVendorList[ven].active=='1'}Active{elseif $arrVendorList[ven].active=='0'}Not Active{/if}</td>
            <td  class="row-3"><input type="button" id="editVendor" value="" onclick=" editVendorDetails('{$arrVendorList[ven].id}');" class="editbtn_2"></td>
          <!--  <td  class="row-3">&nbsp;<a href="ven_add_credit.php?id={$arrVendorList[ven].id}&action=edit" title="Edit" style='text-decoration:none;color:green'>Add</a></td> -->
		  </tr>
		  <tr class="TR_spc"><td colspan="8" class="TD_spc"></td></tr>
		  {/section}
		  {else}
		  	<tr>
				<td colspan="8">No Data Found</td>
			</tr>
		  {/if}
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8" class="content-link"> <div style="display:inline; float:left;"><a href="ven_new_reseller.php"><img src="{$BASE_URL_HTTP_ASSETS}images/new_client.png" alt="Add New Client" width="103" height="25" /></a></div><div style="display:inline; float:right;">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
              {paginate_next} {paginate_last}</div></td>
            </tr>
      </table>
	  </div>
          <div class="clear"></div>
     </div> 
	 
	<!-- <table width="100%" border="0">
	 	<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
  		<tr>
    		<td><a href="ven_new_reseller.php"><img src="{$BASE_URL_HTTP_ASSETS}images/new_client.png" alt="Add New Senderid" width="103" height="25" /></a></td>
    		<td>&nbsp;</td>
  		</tr>
	</table>  -->
	  
	  </td>
    </tr>
  </table>
{/if}



{if $action=='update'}
<table cellpadding="0" cellspacing="10">
  {if $accType=='2'}
  <tr>
      <td>{$smarty.const.REPRESENTATIVE_NAME} <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="name" id="name" value="{$arrVendorDetail.name}">
    </span> <span id="nameDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  {else}
  <tr>
    <td>Name <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="name" id="name" value="{$arrVendorDetail.name}">
    </span> <span id="nameDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  {/if}
  <!-- <tr>

						<td>Account Type</td>

						<td>	

							<select name="accType" id="accType">

							<option value="">Select</option>

							<option value="1" {if $accType=='1'}selected{/if}>Indivdual</option>	

							<option value="2" {if $accType=='2'}selected{/if}>Organisation</option>	

							</select>

						</td>

						<td><div id="accTypeDiv" style="color:#FF0000"></div></td>

					</tr>

					-->
  <tr>
    <td>{$smarty.const.USERNAME}<span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
				<input type="text" name="username2" id="username2" value="{$arrVendorDetail.username}" 	readonly="readonly" ></span>
			  <span id="usernameDiv" class="form_error"></span><input type="hidden" name="username" id="username" value="{$arrVendorDetail.username}" />
			  <input name="oldPass" type="hidden" id="oldPass" value="{$arrVendorDetail.password}" /></td>
  </tr>
  <tr>
    <td>{$smarty.const.MOBILE_NUMBER} <span class="star_col">*</span> <br/>
        <span class="mobexmp">(Ex. 784845785)</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="mob_no" id="mob_no" value="{$arrVendorDetail.mob_no}" onkeyup="javascript: mobileValidation();" maxlength="{$mobMaxlength}"/>
    </span> <span id="mobDiv" class="form_error" style="width:100%"></span></td>
  </tr>
  <tr>
    <td>API Key: </td>
    <td><span class="form_dinatrea standard-input" style="width:260px;">
				<input type="text" name="apikey" id="apikey" value="{$arrVendorDetail.id}" 	readonly="readonly" ></span></td>
  </tr>
  {if $accType=='2'}
  <tr>
    <td>{$smarty.const.LANDLINE_NUMBER} <br/>
        <span class="mobexmp">(Ex. 0222180995)</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="phone" id="phone" value="{$arrVendorDetail.phone}" maxlength="{$phoneMaxlength}" />
    </span><span id="phoneDiv" class="form_error" style="width:100%"></span></td>
  </tr>
  {/if}
  <tr>
    <td>{$smarty.const.EMAIL} <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="email" id="email" value="{$arrVendorDetail.email}" onkeyup="emailValidation();">
    </span> <span id="emailDiv" class="form_error" style="width:100%"></span></td>
  </tr>
  <tr>
    <td>Website</td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="url" id="url" value="{$arrVendorDetail.url}">
      <span id="urlDiv" class="form_error" style="width:100%">{if $error == '1'}
        <p id="urlError" style="color:#FF0000; font-size:10px;">Url arleady Existing</p>
        {/if}</span></span></td>
  </tr>
  {if $accType=='2'}
  <tr>
    <td>{$smarty.const.ORGANIZATION_NAME} <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <input type="text" name="organisation" id="organisation" value="{$arrVendorDetail.organisation}">
    </span> <span id="organisationDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  <tr>
    <td>{$smarty.const.ORGANIZATION_CATEGORY} <span class="star_col">*</span> </td>
    <td><span class="form_dinatrea standard-input">
      <select name="organisationCat" id="organisationCat" >
        <option value="">{$smarty.const.SELECT}</option>
        

							{section name=onganisation loop=$arrOrganisationCat}

							
        <option value="{$arrOrganisationCat[onganisation].id}" {if $arrOrganisationCat[onganisation].id==$arrVendorDetail.organisation_id}selected{/if}>{$arrOrganisationCat[onganisation].organisation}</option>
        

							{/section}

							
      </select>
    </span> <span id="organisationCatDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  <tr>
    <td>{$smarty.const.NUMBER_OF_EMPLOYEES} <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <select name="peopleRange" id="peopleRange" >
        <option value="">{$smarty.const.SELECT}</option>
        

							{section name=pplRange loop=$arrPeopleRanges}

							
        <option value="{$arrPeopleRanges[pplRange].id}" {if $arrPeopleRanges[pplRange].id==$arrVendorDetail.people_range_id}selected{/if}>{$arrPeopleRanges[pplRange].people_range}</option>
        

							{/section}

							
      </select>
    </span> <span id="peopleRangeDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  {/if}
  <tr>
    <td valign="top">{$smarty.const.POSTAL_ADDRESS} </td>
    <td><div class="standard-input" style="width:300px;">
      <textarea name="postalAddress" id="postalAddress" >{$arrVendorDetail.postal_address}</textarea>
    </div></td>
  </tr>
  {if $accType=='2'}
  <tr>
    <td valign="top">{$smarty.const.PHYSICAL_ADDRESS} <span class="star_col">*</span></td>
    <td><div class="standard-input" style="width:300px;">
      <textarea name="physicalAddress" id="physicalAddress" >{$arrVendorDetail.physical_address}</textarea>
    </div>
        <span id="physicalAddressDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  {/if}
  <tr>
    <td>{$smarty.const.LOCATION} <span class="star_col">*</span></td>
    <td><span class="form_dinatrea standard-input">
      <select name="location" id="location" >
        <option value="">{$smarty.const.SELECT}</option>
        

							{section name=location loop=$arrLocation}

							
        <option value="{$arrLocation[location].id}" {if $arrLocation[location].id==$arrVendorDetail.location_id}selected{/if}>{$arrLocation[location].location}</option>
        

							{/section}

							
      </select>
    </span> <span id="locationDiv" class="form_error" style="width:100%"></span> </td>
  </tr>
  <tr style="display:none">
    <td>Comfirmed</td>
    <td><span class="form_dinatrea standard-input">
	<select name="comfirmed" id="comfirmed">
      <option value="" {if $arrVendorDetail.confirmed==''}selected{/if}>Select</option>
      <option value="0" {if $arrVendorDetail.confirmed=='0'}selected{/if}>No</option>
      <option value="1" {if $arrVendorDetail.confirmed=='1'}selected{/if}>Yes</option>
    </select></span>    </td>
  </tr>
  <tr>
    <td>Active</td>
    <td><span class="form_dinatrea standard-input">
	<select name="active" id="active">
      <option value="" {if $arrVendorDetail.active==''}selected{/if}>Select</option>
      <option value="0" {if $arrVendorDetail.active=='0'}selected{/if}>No</option>
      <option value="1" {if $arrVendorDetail.active=='1'}selected{/if}>Yes</option>
    </select></span>    </td>
  </tr>
  <tr  style="display:none">
    <td>Deleted</td>
    <td><span class="form_dinatrea standard-input">
	<select name="deleted" id="deleted">
      <option value="" {if $arrVendorDetail.deleted==''}selected{/if}>Select</option>
      <option value="0" {if $arrVendorDetail.deleted=='0'}selected{/if}>No</option>
      <option value="1" {if $arrVendorDetail.deleted=='1'}selected{/if}>Yes</option>
    </select></span>    </td>
  </tr>
  <tr>
    <td>Allow Access Incoming SMS</td>
    <td><span class="form_dinatrea standard-input">
	<select name="access_incoming" id="access_incoming">
      <option value="0" {if $arrVendorDetail.access_incoming=='0'}selected{/if}>No</option>
      <option value="1" {if $arrVendorDetail.access_incoming=='1'}selected{/if}>Yes</option>
    </select></span></td>
  </tr>
  <tr>
    <td>Allow Access Registrations SMS</td>
    <td><span class="form_dinatrea standard-input">
	<select name="access_registrations" id="access_registrations">
      <option value="0" {if $arrVendorDetail.access_registrations=='0'}selected{/if}>No</option>
      <option value="1" {if $arrVendorDetail.access_registrations=='1'}selected{/if}>Yes</option>
    </select></span></td>
  </tr>
  <tr>
    <td valign="top"> New Password:</td>
    <td><span class="form_dinatrea standard-input">
      <input type="password" name="password" id="password" value="{$password}">
    </span> <span id="passwordDIV" class="form_error" style="width:100%"></span></td>
  </tr>
  <tr>
    <td valign="top"> Confirm Password:</td>
    <td><span class="form_dinatrea standard-input">
      <input type="password" name="confirmpassword" id="confirmpassword" value="{$confirmpassword}">
    </span> <span id="comfirmpasswordDIV" class="form_error" style="width:100%"></span></td>
  </tr>
  <tr>
    <td valign="top">Credit Balance Reminder </td>
    <td><table width="332" border="0">
      <tr>
        <td width="190"><label>
          <input name="SMSReminder" type="checkbox" id="SMSReminder" value="1"  {if $arrCreditReminder.sms_reminder=='1'}checked=&quot;checked&quot;{/if} style="margin-right:10px;" />
          SMS(Tanzania only)</label>
              <span id="SMSReminderDIV" class="form_error" style="width:100%"></span> </td>
        <td width="147"><label>
          <input name="EmailReminder" type="checkbox" id="EmailReminder" value="1" style="margin-right:10px;" {if $arrCreditReminder.email_reminder == '1'}checked="checked" {/if} />
          E-mail</label>
              <span id="EmailReminderDIV" class="form_error" style="width:100%"></span> </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td valign="top">&nbsp;</td>
    <td><span class="form_dinatrea standard-input" style="width:50px;">
      <input name="Credit_Level" type="text" id="Credit_Level" size="4" value="{$arrCreditReminder.credit_level}" />
    </span> <label style="margin-left:10px;">Credit reminder level</label> </td>
  </tr>
  <tr>
    <td></td>
    <td><input type="hidden" name="accType" id="accType" value="{$accType}" />
        <input type="hidden" name="userId" id="userId" value="{$arrVendorDetail.id}" />
        <input type="submit" name="update" id="update" value="{$smarty.const.UPDATE}" class="editbtn"  onclick="return venProfileValidation();"/></td>
  </tr>
</table>
{/if}
</form>
</div>
{literal}
<script type="text/javascript">
	function editVendorDetails(id){
		$("#action").val("edit");
		$("#vendor_id").val(id);
		document.manageUser.submit();
	}



		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		function venProfileValidation(){
			
			var accType = $("#accType").val();

			if(jQuery.trim($("#name").val())==""){
				$("#nameDiv").html("{/literal}{$smarty.const.NAME_REQUIRED}{literal}");
				return false;
			}else{
				$("#nameDiv").html("");
			}

			if(usernameValidation()==0){
				$("#usernameDiv").html(msg_04);
				return false;
			}else{
				$("#usernameDiv").html("");
			}
			
			if(mobileValidation()){
				$("#mobDiv").html("");
			}else{
				$("#mobDiv").html(msg_07);
				return false;
			}

			if(emailValidation()){
				$("#emailDiv").html("");
			}else{
				$("#emailDiv").html(msg_01);
				return false;
			}

			if(jQuery.trim($("#url").val())!=""){

				// Shortcut to save writing

  				var url = jQuery.trim($("#url").val());

  				// Check basic domain name

				 var rgx = /^\s*((ht|f)tps?:\/\/)?[a-z0-9-\.]+\.[a-z]{2,4}\/?([^\s<>\#%"\,\{\}\\|\\\^\[\]`]+)?$/;

				if(rgx.test(url)==false){
					$("#urlError").html("");
					$("#urlDiv").html("Enter valid website address");
					return false
				}else{
					document.venProfile.url.value=getUrl();
					$("#urlDiv").html("");
					name=true;
				}
			}
			
			if($("#password").val() != ''){
				if($("#confirmpassword").val()==''){
					$("#comfirmpasswordDIV").html("Verification of passsword is required");
					return false;
				}else if($("#password").val() != $("#confirmpassword").val()){
					$("#comfirmpasswordDIV").html("Password does not match");
					return false;
				}
			}
			
			
			if($("#SMSReminder").is(':checked')){
				if($("#mob_no").val()==""){
					$("#SMSReminderDIV").html("No Mobile number specified");
					return false;
				}
			
			}
			
			if($("#EmailReminder").is(':checked')){
				if($("#email").val()==""){
					$("#EmailReminderDIV").html("No email specified");
					return false;
				}
			
			}



			if(accType==2){
				if(jQuery.trim($("#organisation").val())==""){
					$("#organisationDiv").html("{/literal}{$smarty.const.ENTER_ORGANIZATION_NAME}{literal}");
					return false;
				}else{
					$("#organisationDiv").html("");
				}



				if($("#organisationCat").val()==""){
					$("#organisationCatDiv").html("{/literal}{$smarty.const.SELECT_ORGANIZATION_CATEGORY}{literal}");
					return false;
				}else{
					$("#organisationCatDiv").html("");
				}



				if(jQuery.trim($("#physicalAddress").val())==""){
					$("#physicalAddressDiv").html("{/literal}{$smarty.const.PHYSICAL_ADDRESS_REQUIRED}{literal}");
					return false;
				}else{
					$("#physicalAddressDiv").html("");
				}



				if($("#peopleRange").val()==""){
					$("#peopleRangeDiv").html("{/literal}{$smarty.const.SELECT_NUMBER_OF_EMPLOYEES}{literal}");
					return false;

				}else{
					$("#peopleRangeDiv").html("");
				}	
			}

			

			if($("#location").val()==""){
				$("#locationDiv").html("{/literal}{$smarty.const.SELECT_LOCATION}{literal}");
				return false;
			}else{
				$("#locationDiv").html("");
			}

		}
</script>
{/literal}
