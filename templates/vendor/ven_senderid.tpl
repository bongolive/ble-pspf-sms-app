<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 50px;
	text-align: left;
		}


.row-2{
 width:250px;
 text-align: left;
}

.row-3{
 width:200px;
 text-align: left;
}

.row-4{
 width:200px;
 text-align: left;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}

</style>
<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.VEN_MENUTOP_BROADCAST_SMS_2}</h1>
	<div style="font:12px/16px Verdana;">{$smarty.const.VEN_SENDERID_TEXT_1}</div>
<table align="center" cellpadding="0" cellspacing="0">
	<tr>
        <!--td>{include file="vendor/ven_left.tpl"}</td-->
        <td>
            <!-- middle body start-->
           <div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
            <form action="" method="POST" name="senderidForm" onsubmit="return MM_validateForm();">
                <table width="93%" cellspacing="10">
					 <tr>
                        <td width="152">&nbsp;</td>
                      <td width="408"></td>
                    </tr>
                    <tr>
                        <td width="152" valign="top">{$smarty.const.REQUEST_NEW}: </td>
                      <td width="408"><span class="form_dinatrea standard-input">
                      <input type="text" name="senderid" id="senderid" value="{$senderid}"></span><span id="senderidDiv" class="form_error" style="width:100%"></span></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <input name="send" type="submit" class="editbtn" id="send" value="{$smarty.const.SUBMIT}">
                            <input type="hidden" name="action" id="action" value="add">
                        </td>
                    </tr>
                    
              </table>
    </form>
             <br>
		<div class="footer-top">
       <div style="margin:20px 0 20px 20px; margin-top:20px;">
       <!-- <img src="templates/assets/images/filesms_save-job.png" class="save-job"/> -->
            <table style="margin-right:10px;">
                <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
                    <td class="row-1">&nbsp;</td>
                    <td class="row-2">{$smarty.const.SENDER_NAME}</td>
                    <td class="row-2">{$smarty.const.STATUS}</td>
                </tr>
                {section name=senderid loop=$arrSenderid}
				<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
                <tr class="row-body">
                    <td class="row-1">{$smarty.section.senderid.iteration}</td>
                    <td class="row-2">{$arrSenderid[senderid].senderid}</td>
                    <td class="row-2">{$arrSenderid[senderid].status}</td>
                </tr>
                {/section}
            </table>
		</div>
          <div class="clear"></div>
     </div>
            <!-- middle body end-->
        </td>
    </tr>
</table>
  </div>
      <div class="clear"></div>
  </div>
  {literal}

<script type="text/javascript">
//<!--
function MM_validateForm() { //v4.0
	checkMobilePhone();
	var v = $("#senderid").val();
	var senderregex=/^\+?[A-Za-z0-9 _]+$/;
	var phoneregex=/^\+?[0-9]{9,13}$/;
	if(v==0){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_1}{literal}");
		return false;
	}else if(! v.match(senderregex)){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
		return false;
	}else if( ! isNaN(v)){
		if(v.length >14){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_6}{literal}");
			return false;
		}else if(! v.match(phoneregex)){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
			return false;
		}else{
			$("#senderidDiv").html("");
		}
	}else{
		if(v.length >11){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_7}{literal}");
			return false;
		}else{
			$("#senderidDiv").html("");
		}
	}
}

$('#senderid').keyup(function() {
	var v = $("#senderid").val();
	var senderregex=/^\+?[A-Za-z0-9 _]+$/;
	var phoneregex=/^\+?[0-9]{9,14}$/;
	if(! v.match(senderregex)){
		$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
	}else if( ! isNaN(v)){
		if(v.length >14){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_6}{literal}");
		}else if(! v.match(phoneregex)){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_5}{literal}");
		}else{
			$("#senderidDiv").html("");
		}
	}else{
		if(v.length >11){
			$("#senderidDiv").html("{/literal}{$smarty.const.VEN_SENDERID_MSG_7}{literal}");
		}else{
			$("#senderidDiv").html("");
		}
	}
});
	
function checkMobilePhone(){

 	 var phone=jQuery.trim($("#senderid").val());

    if (phone.substr(0,1) == '+'){

      var newPhone = phone.substr(1);

    }else{

      var newPhone = phone;

    }
	var validphone=jQuery.trim(newPhone);
	document.senderidForm.senderid.value=validphone;
}
 //-->
 </script>
{/literal}
