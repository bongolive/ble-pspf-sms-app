<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 120px;
	text-align: left;
		}


.row-2{
 width:130px;
 text-align: center;
}

.row-3{
 width:150px;
 text-align: left;
}

.row-4{
 width:110px;
 text-align: center;
}

.row-5{
 padding-left: 5px;
 padding-right: 5px;
 text-align: left;
 width:100px;
 text-align: right;
}

.row-6{
 width:120px;
 text-align: left;
 padding:5px;
}
.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}
</style>
<div id="container">
    <h1>Manage Purchase Request</h1>
	
<div id="errorDiv"></div>
<div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
{if $action == 'edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST" onsubmit="if(!confirm('Are you sure change status?'))return false;">
		<table cellspacing="10">
			<tr>
				<td>Client Name:</td>
				<td>{$arrCreditDetails.name}</td>
				
			</tr>
			<tr>
				<td>Credit Requested: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crditRequested" id="crditRequested" value="{$arrCreditDetails.credit_requested}" onkeyup="getCreditShcemeDetailsAdm(this.value,document.getElementById('crdtSchmId').value,'{$ajaxUrl}');"></span></td>
				
			</tr>
			<!--<tr>
				<td>Rate:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="crdtSchmId" id="crdtSchmId" onchange="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');">
						<option value="">Select</option>
						{section name="crdtSchmId" loop=$arrCreditScheme}
						<option value="{$arrCreditScheme[crdtSchmId].id}###{$arrCreditScheme[crdtSchmId].rate}" 
						{if $arrCreditScheme[crdtSchmId].id== $arrCreditDetails.credit_scheme_id}Selected{/if}>{$arrCreditScheme[crdtSchmId].rate}</option>
						{/section}
					</select></span>
				</td>
				
			</tr>-->

{section name="crdtSchmId" loop=$arrCreditScheme}
	{if $arrCreditScheme[crdtSchmId].id == $arrCreditDetails.credit_scheme_id}
		{$tmp = $arrCreditScheme[crdtSchmId].rate}
	{/if}
{/section}

{section name="crdtSchmId" loop=$arrCreditBonus}
	{if $arrCreditBonus[crdtSchmId].id == $arrCreditDetails.credit_scheme_id}
		{$tmp = $arrCreditBonus[crdtSchmId].rate}
	{/if}
{/section}

	
			<tr>
				<td>Rate: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crdtSchmId" id="crdtSchmId" value="{$tmp}" onkeyup="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');"></span></td>
				
			</tr>
			<tr>
				<td>Total Cost:</td>
				<td><div id="creditTotalCostAdm">{$arrCreditDetails.total_cost}</div></td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" {if $arrCreditDetails.status == 'allocated'} disabled {/if}>
						<option value="">Select</option>
						<option value="allocated" {if $arrCreditDetails.status == 'allocated'} selected{/if}>Allocate</option>
						<option value="rejected"  {if $arrCreditDetails.status == 'rejected'} selected{/if}>Reject</option>
						<option value="pending" {if $arrCreditDetails.status == 'pending'} selected{/if}>Pending</option>
					</select></span>
					{if $arrCreditDetails.status == 'allocated'} <input type='hidden' name='status' value='allocated'>{/if}
				</td>
				
			</tr>
			<!-- <tr>
				<td>Payment Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="payment_status" id="payment_status" >
						<option value="">Select</option>
						<option value="0" {if $arrCreditDetails.payment_status == '0'} selected{/if}>Not Paid</option>
						<option value="1" {if $arrCreditDetails.payment_status == '1'} selected{/if}>Paid</option>
						<option value="2" {if $arrCreditDetails.payment_status == '2'} selected{/if}>Partially Paid</option>
						<option value="3" {if $arrCreditDetails.payment_status == '3'} selected{/if}>Receipt Sent</option>
						<option value="4" {if $arrCreditDetails.payment_status == '4'} selected{/if}>Bonus</option>
					</select></span>
				</td>
				
			</tr> -->
			<tr>
				
				<td>
					<input type="submit" name="save" id="save" value="Save" class="editbtn">
					<input type="hidden" name="id" id="id" value="{$arrCreditDetails.id}">
					<input type="hidden" name="userId" id="userId" value="{$arrCreditDetails.user_id}">
					<input type="hidden" name="old_status" id="old_stutus" value="{$arrCreditDetails.status}">
					<input type="hidden" name="old_payment_status" id="old_pay" value="{$arrCreditDetails.payment_status}">
					<input type="hidden" name="action" id="action" value="update">
				</td>
				<td>
					<input type="button" name="cancel" id="cancel" value="Cancel" class="editbtn" onclick="history.back()">
				<input name="payment_status" type="hidden" id="payment_status" value="1" />				</td>

			</tr>
		</table>
	</form>


{elseif $action==""}
	<form name="creditRequest" action="" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Client name: </td>
	    <td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Client Username:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="username" id="username" value="{$username}">
				</span>
				</td>
				
			</tr>
			<tr>
				<td>Client Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendor_type" id="vendor_type" >
						<option value="" >Select</option>
						<option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
					</select>
				</span>
				</td>
				
			</tr>
			<!-- <tr>
				<td>Price/SMS:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="rate" id="rate">
					<option value="">Select</option>
					{section name=rate loop=$arrCrdtSchm}
					<option value="{$arrCrdtSchm[rate].id}" 
					{if $arrCrdtSchm[rate].id==$rate}selected{/if}
					>{$arrCrdtSchm[rate].rate}</option>
					{/section}
					</select></span>
				</td>
				
			</tr> -->
			<tr>
				<td>Quantity:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="quantity" id="quantity" value="{$quantity}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">	
					<select name="status" id="status" >
						<option value="" >Select</option>
						<option value="allocated" {if $status == 'allocated'} selected{/if}>Allocated</option>
						<option value="rejected"  {if $status == 'rejected'} selected{/if}>Rejected</option>
						<option value="pending" {if $status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>				
			</tr>
			<tr>
				<td>Payment Status:</td>
				<td><span class="form_dinatrea standard-input">	
					<select name="payment_status" id="payment_status" >
						<option value="" >Select</option>
						<option value="0" {if $payment_status == '0'} selected{/if}>Not Paid</option>
						<option value="1" {if $payment_status == '1'} selected{/if}>Paid</option>
						<option value="2" {if $payment_status == '2'} selected{/if}>Partially Paid</option>
						<option value="3" {if $payment_status == '3'} selected{/if}>Receipt Sent</option>
						<option value="4" {if $payment_status == '4'} selected{/if}>Bonus</option>
					</select></span>
				</td>				
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
				
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
				
			</tr>
		</table>
	</form>	
	<br />


<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
	<table style="margin-right:10px;">
	<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">	
		<td class="row-1">P R Number.</td>
		<td class="row-3">Client Name</td>
		<!-- <td class="row-2">Broadcaster ID</td>
		<td class="row-2">Broadcaster Type</td> -->
		<td class="row-4">Pric/SMS</td>
		<td class="row-4">Quantity</td>
		<td class="row-4">Total Cost</td>
		<td class="row-4">Status</td>
		<td class="row-4">Payment Status</td>
		<td class="row-2">Request Date</td>
		<td class="row-5">Edit</td>
	</tr>
	{if $objCreditRequest}
	{section name=reqCredit loop=$objCreditRequest}
	<tr class="row-body">		
		<td class="row-1"> {$objCreditRequest[reqCredit].credit_request_id} </td>
		<td class="row-3"> {$objCreditRequest[reqCredit].name} </td>
		<!-- <td class="row-2"> {$objCreditRequest[reqCredit].vendor_id} </td>
		<td class="row-2">
		{if $objCreditRequest[reqCredit].vendor_type=='2'}
			Organisation
		{elseif $objCreditRequest[reqCredit].vendor_type=='1'}
			Individual
		{/if}
		</td> -->
		<td class="row-4"> {if ($objCreditRequest[reqCredit].credit_scheme_id==$objCreditRequest[reqCredit].idbon)}{$objCreditRequest[reqCredit].ratebon}{else}{$objCreditRequest[reqCredit].rate}{/if} </td>
		<td class="row-4"> {$objCreditRequest[reqCredit].credit_requested} </td>
		<td class="row-4"> {$objCreditRequest[reqCredit].total_cost} </td>
		<td class="row-4"> {$objCreditRequest[reqCredit].status|ucfirst} </td>
		<td class="row-4"> 
		{if $objCreditRequest[reqCredit].payment_status=='0'}
			Not Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='1'}
			Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='2'} 
			Partially Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='3'} 
			Receipt Sent
		{elseif $objCreditRequest[reqCredit].payment_status=='4'} 
			Bonus
		{/if}
		</td>
		<td class="row-2"> {$objCreditRequest[reqCredit].created} </td>
		<td class="row-5"> <a href="?id={$objCreditRequest[reqCredit].id}&action=edit" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a> </td>
	</tr>
	{/section}
	{else}
	<tr>
		<tr style="background-color:#f8f8f8;">
		<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Records found
		</td>
	</tr>			
	{/if}
    <tr style="background-color:#f8f8f8;" class="content-link">
		<td align="right" colspan="10" class="content-link" style="padding:10px 0;">
			<div style="display:inline; float:left;"><a href="ven_add_credit.php"><img src="{$BASE_URL_HTTP_ASSETS}images/add_credit.png" alt="Add Credit" width="103" height="25" /></a></div>
		<div style="display:inline; float:right;">{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}</div>
		</td>
	</tr>
    </tr>
	</table>
	
</div>
          <div class="clear"></div>
     </div>  
{/if}

</div>

<script type="text/javascript">

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});		

     </script>
