<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
    <h1>{$smarty.const.GROUP_SMS}</h1>
    <p>Bongo Live!s unique online system allows you to send sms messages to subscribers on all the mobile networks in Tanzania. Use our service for a variety of purposes from sending reminders to greetings. Both individuals and organizations can take advantage of this to reach a large group of people within seconds directly on their phone.</p>
    <p>Once you sign up, you have free access to our online application that has a variety of features to make it easy for you to send these messages.</p>
	
    <h2>Key Features</h2>
	<ul>
		<li type="circle">Add individual contacts or upload in bulk from an excel file
		<li type="circle">Manage your contacts online and even organize them into sub groups
		<li type="circle">Secure and private. Only you have access to your contacts
		<li type="circle">Schedule messages for a later date/time
		<li type="circle">Low Flat Rates per SMS
		<li type="circle">Purchase SMS with ZAP or Mpesa
		<li type="circle">Get discounts for buying sms in bulk
	</ul>
	<h2>How can I use Group SMS?</h2>
	<ul>
		<li type="circle">Send reminders for events, payments & appointments
		<li type="circle">Send offers and discounts to bring customers to you
		<li type="circle">Introduce new products and services to inform customers
		<li type="circle">Send emergency updates quickly at the last minute
		<li type="circle">Gather information from many people at once
		<li type="circle">Send greetings and messages to friends and family
	</ul>
	 <h2>Who can use Group Messaging?</h2>
	<ul>
		<li type="circle">Small and Large Businesses
		<li type="circle">Government Entities
		<li type="circle">NGOs and Social Groups
		<li type="circle">Schools/Higher Education
		<li type="circle">Event Planners (weddings, functions, seminars)
		<li type="circle">Religious Groups
		<li type="circle">Bars & Nightclubs
		<li type="circle">Restaurants
		<li type="circle">Individuals
	</ul>

  </div>
		
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>