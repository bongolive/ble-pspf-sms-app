<div id="container">
<div class="line01"></div>
	<div class="column2-ex-left-2 column2_contents">
	<h1>{$smarty.const.INDEX_FORGOT_PASSWORD}</h1>
<div id="msg" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
{if $action!=sent}
<form name="fgtpass" id="fgtpass" method="post" action="" onsubmit="javascript: validateForgotpass(); return false;">             
	
		
		<table cellspacing="10" align="left" cellpadding="0">
			<tr>
				<td width="136">{$smarty.const.SELECT_USER_TYPE}:</td>
				<td width="168"><div class="form_dinatrea standard-input">
					<select name="userType" id="userType">
						<option value="">{$smarty.const.SELECT}</option>
						<option value="VEN" {if $userType=='VEN'}Selected{/if}>{$smarty.const.INDEX_BROADCASTER}</option>
					</select></div>
			  </td>
			</tr>
			<tr>
				<td>{$smarty.const.ENTER_YOUR_EMAIL}:</td>
				<td><div class="form_dinatrea standard-input">
				<input type="text" name="email" id="email" value="{$email}" size="24"/></div></td>
			</tr>
			
			<tr>
			    <td>&nbsp;</td>
				<td class="submit">
					<input type="submit" name="submit" id="submit" value="{$smarty.const.SUBMIT}" class="editbtn" />
					<input type="hidden" name="action" id="action" value="fgtpass">
				</td>
			</tr>

		</table>
</form>
{/if}
</div>
<div class="clear"></div>
</div>

{literal}
	<script type="text/javascript">
	<!--
		function validateForgotpass(){

			var errorFlag = 0;
			if($("#userType").val()==""){
				alert("{/literal}{$smarty.const.SELECT_USER_TYPE}{literal}");
				errorFlag = 1;
				$("#userType").focus();
				return false;

			}
			if($("#email").val()==""){
				alert("{/literal}{$smarty.const.ENTER_YOUR_VALID_EMAIL}{literal}");
				errorFlag = 1;
				$("#email").focus();
				return false;

			}
			
			if(errorFlag !=1){
			
				document.fgtpass.submit();
			}
			
		
		}
	-->	
	</script>
	
{/literal}