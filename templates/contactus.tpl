<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2">
    <h1>{$smarty.const.CONTACT_US}</h1>
    {$smarty.const.CONTACT_US_PREINFO}
    <!--Form Begins-->
    <form name="contactusForm" id="contactusForm" method="POST" action="" onsubmit="contactusValidation();return false;">
          <table cellspacing="10px">
		  <tr>
              <td colspan="4"><div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div> </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_name"> {$smarty.const.NAME}<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="name" name="name" value="{$name}"  /> </span>

			  <span id="nameDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_email">{$smarty.const.EMAIL_ADDRESS}<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="email" name="email" value="{$email}"  /> </span>
			  <span id="emailDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_ex_field2">{$smarty.const.MOBILE_NUMBER}<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="mob_no" name="mob_no" value="{$mob_no}"  /> </span>
			  <span id="mobDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_ex_field3">{$smarty.const.BUSINESS_NAME}</label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="business_name" name="business_name" value="{$business_name}"  /> </span></td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_subject">{$smarty.const.SUBJECT}<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">

			  <input style="text-align:left;" type="text" id="subject" name="subject" value="{$subject}"  /> </span>
			  <span id="subjectDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right top">
				<label for="si_contact_message"> {$smarty.const.MESSAGE}<span class="star_col">*</span></label>
			  </td>
              <td><span class="form_dinatrea standard-input" style="width:350px;" >
				<textarea style="text-align:left;" id="message" name="message"  cols="40" rows="10">{$message}</textarea> </span>
				<span id="messageDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td><label for="si_contact_captcha_code">{$smarty.const.CAPTCHA_CODE}<span class="star_col">*</span></label></td>
              <td>
				 <!-- <img id="si_image_ctf" style="padding-bottom:10px; float:left; border-style:none; margin:0;" src="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/securimage_show_medium.php?sid=dd11e0638ece111518369b9539ca3559" alt="CAPTCHA Image" title="CAPTCHA Image" /> -->
				 <img id="si_image_ctf" style="padding-bottom:10px; float:left; border-style:none; margin:0;" src="captcha.php" alt="CAPTCHA Image" title="CAPTCHA Image" />
				  <!--
				  <a href="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/securimage_play.php" title="CAPTCHA Audio"> 
				  <img src="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/images/audio_icon.gif" alt="CAPTCHA Audio" style="padding-top:2px; vertical-align:top; float:left; border-style:none; margin:0;" onclick="this.blur()" />
				  </a>
				  -->
				  &nbsp; 
				  <a href="#" title="Refresh Image" onclick="document.getElementById('si_image_ctf').src = 'captcha.php?' + Math.random(); return false">
				  <img src="{$BASE_URL_HTTP_ASSETS}images/refresh.gif" alt="Refresh Image" style="vertical-align:top; float:left; border-style:none; margin:0;" onclick="this.blur()" />
				  </a>
			  </td>
				
            </tr>
        <tr>
              <td></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" value="" name="security_code" id="security_code"  size="9" />
			   </span>
			  <span id="captchaDiv" class="form_error">{$msgSecCode} </span>
			  </td>
		</tr>
        <tr>
              <td>
			  
				<input type="button" name="resset" id="rest" value="{$smarty.const.RESET}" class="editbtn"/>
			  </td>
              <td>
				<input type="submit" name="submit" id="submit" value="{$smarty.const.SUBMIT}" class="editbtn"/>
				<input type="hidden" name="action" value="send" />
			  </td>
            </tr>
      </table>
          <div class="clear"></div>
        </form>
    
    <!-- Form Ends --> 
  </div>
      <div class="column2-ex-right-2"> &nbsp;</div>
      <div class="clear"></div>
    </div>

{literal}    
    <script type="text/javascript">
		//<!--
            
		function contactusValidation(){
			
			var error=0;
			var name;
			var errorMob;
			var errorEmail;
			var subject;
			var message;
			var captcha;
			
			errorMob = mobileValidation();
			errorEmail = emailValidation();
			
			
			if($("#name").val()==""){
		
				$("#nameDiv").html("{/literal}{$smarty.const.ENTER_NAME}{literal}");
				name=false;
			}else{
				$("#nameDiv").html("");
				name=true;
			}

			if($("#subject").val()==""){
				$("#subjectDiv").html("{/literal}{$smarty.const.ENTER_SUBJECT}{literal}");
				subject=false;
			}else{
				$("#subjectDiv").html("");
				subject=true;
			}

			if($("#message").val()==""){
				$("#messageDiv").html("{/literal}{$smarty.const.ENTER_MESSAGE}{literal}");
				message=false;
			}else{
				$("#messageDiv").html("");
				message=true;
			}

			if($("#security_code").val()==""){
				$("#captchaDiv").html("{/literal}{$smarty.const.ENTER_CAPTCHA_CODE}{literal}");
				captcha=false;
			}else{
				$("#captchaDiv").html("");
				captcha=true;
			}


			
			if(!errorEmail  || !errorMob || !name || !message || !subject || !captcha){
					error=1;
			}
			
			
			if(error !=1){
				document.contactusForm.submit();
			}
		
		
		}



        //-->
     </script>


{/literal}