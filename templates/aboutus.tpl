<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
    <h1>{$smarty.const.ABOUT_US}</h1>
    <p>Bongo Live! is a mobile services company based in Dar Es Salaam, Tanzania. We�re a team of individuals committed to bringing the next level of mobile and SMS based services to Tanzania. We believe that there is much more to be unleashed from the power of the mobile and we want to create services that empower businesses, NGOs, government and consumers. </p>
	
	<p>Our range of services includes group messaging, targeted sms offers, tailored sms services (raffles, voting, Surveys) and custom applications that cater to organizations and individuals of all types and sizes. So no matter what your needs, size or budget, Bongo Live! will work with you.  We believe that nothing makes for better business than a happy customer and we will go to lengths to ensure that.  We are constantly working to improve our services and appreciate any feedback we receive both positive and negative.</p>

<br />
	<h2>How did we Start?</h2>
	<p>The founding team members at Bongo Live! had been involved either directly or through family with many businesses in Tanzania.  A common challenge that was faced by these small businesses was a lack marketing and branding.  The main reasons for this were lack of awareness and secondly minimal budgets. Bongo Live! was thus formed in March 2010 in order to leverage the explosion of mobile phones  and make communication and advertising easy, efficient and economical. </p>

     <br/>
	<h2>Our Customers</h2>
	<p>Consumers that want to receive highly targeted and customized offers directly on their phone.</p>
	<p>Local businesses in Tanzania that seek to deliver high impact mobile marketing campaigns that engage their customers to keep coming back for more.</p>
	<p>Businesses, NGOs, government entities that seek to develop mobile and SMS applications to disseminate and/or collect information from individuals.</p>

<br />
    <h2>What we believe in</h2>
	<ul>
		<li type="circle">A great customer service experience
		<li type="circle">Delivering what we promise
		<li type="circle">Technology empowers
		<li type="circle">Simplicity
	</ul>
	<br/>

  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>