<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
		<h1>{$smarty.const.HOW_IT_WORKS}</h1>
	<br />
	
		<h2>Memba(Wanaojiunga)</h2>

	<p>Wateja(Memba) wanajiunga kwenye tovuti ya Bongo Live! na kuandika taarifa zao. Kisha, Bongo Live! tunakutumia jumbe kupitia simu yako ya mkononi zinazohusu mambo yale tu uyapendayo. Jumbe hizi ni BURE kabisa bila gharama yoyote!</p>
		
	<p>Je, ungependa kuanza kupokea ofa bora kabisa mjini? Jiunge <a href="sub_register.php">hapa.</a></p>
	
	<br />
	
	<h2>SMS Broadcaster</h2>

	<p><img src="{$BASE_URL_HTTP_ASSETS}images/how_group_sms_works.png" longdesc="" alt="how group sms works"/></p>
	
		<p>‘Broadcaster’ ni watu au mashirika yanayojiunga kwenye tovuti ya Bongo Live! kwa ajili ya kutuma meseji za makundi. Anapojiunga, ‘broadcaster’ huingiza wateja wake kwenye akaunti yake na kuwapanga katika makundi. Baada ya kununua SMS za kutosha, ‘broadcaster’ huweza kuzituma kwa hao wateja wake.</p>
	
	<p>Ili kufahamu zaidi juu ya huduma hii, tembelea <a href="bulk_sms.php">Ukurasa wa Meseji za Makundi/Jumla.</a></p>
	
	<p>Ungependa kuanza kutuma SMS? Jiunge <a href="index.php">hapa.</a></p>

	<br />
	
	<h2>Mtangaza Biashara</h2>
	
	<p><img src="{$BASE_URL_HTTP_ASSETS}images/how_sms_advertising_works.png" longdesc="" alt="how targeted sms advertising works"/></p>
	
	<p>Washauri wa masoko wa Bongo Live! hufanya kazi bega kwa bega na wateja wetu katika kuplani na kuandaa kampeni zinazoendana na malengo ya kimasoko ya wateja wetu. Hii ni pamoja na usanifu wa maneno kwa ajili ya kila utumaji wa SMS. Mara kampeni inapokuwa imewekwa sawa, inatekelezwa pamoja na utumaji wowote wa SMS uliopangwa. Kisha matokeo hukusanywa, hufanyiwa uchambuzi na uboreshaji wowote utakaofanywa unatumiwa katika kampeni zinazofuata.</p>

	<p>Ili kufahamu zaidi kuhusu huduma hii tembelea kurasa wa <a href="sms_advertising.php">Matangazo za SMS</a>au <a href="tailored_services.php">{$smarty.const.MENU_SERVICES_TAILORED_SMS_SERVICES}</a>.</p>
	<br/>
	
	<h2>‘Application’ za Simu & SMS Kulingana na Mahitaji</h2>
	<p>Learn more about our unique approach to working with clients and developing custom solutions <a href="custom_applications.php">here.</a> </p>
	<br/>

    	
	  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>