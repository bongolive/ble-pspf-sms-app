<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.MENU_SERVICES_TAILORED_SMS_SERVICES}</h1>
    <p>Ungependa kufahamu huduma za SMS zilizopo ambazo zinalenga mahitaji mahsusi? Tunazo huduma nyingi zilizoongezwa thamani na ubora, kwa mfano Bahati Nasibu kwa SMS, Kupiga Kura kwa SMS, Kufanya �Survey� kwa SMS na Kutuma Taarifa kwa SMS Pale Zinapohitajika. Hii yote ni kwa ajili ya kutimiza mahitaji yako ya kujitangaza kibiashara.</p>
    <p>Timu yetu ya wataalamu wanaohusika na matangazo ya biashara kwa SMS inaweza kukupatia ushauri kuhusu mbinu unazoweza kutumia  ili kufikia malengo yako. Tutafanya kazi pamoja nawe hatua kwa hatua katika kupanga kampeni zako, kujumuisha SMS kwenye matangazo yoyote ambayo tayari unayatoa, kutekeleza kampeni hizo na hatimaye kuzifuatilia na kutathmini matokeo yake. </p>
	<p>Kwa kawaida, utambulisho mfupi kama vile �15555� hutumika kuitambulisha biashara; lakini pia unaweza kutumiwa na biashara mbalimbali kupitia manenofunguo (keywords). Kwa mfano, duka la kompyuta linaweza kutumia nenofunguo �laptop� au mgahawa ukatumia �tasty�. Mwitiko wa walengwa wako utakuwa ni kwenye maneno haya na mfumo wetu utafuatilia miitikio hii na unaweza kurudisha meseji kama inabidi kufanya hivyo.</p>
	<p>Je, ungependa kupata huduma hii? Wasiliana nasi <a href="contactus.php">hapa</a> kwa ajili ya kupata ushauri unaotolewa bure ili kutathmini mahitaji yako. Hatukulazimishi kufanya chochote baada ya ushauri huo.</p>

    <h2>Kupiga Kura kwa SMS</h2>
	<p>Kupiga kura huwapa watu nafasi ya kushiriki katika jambo na kuamua matokeo. Hii huwafanya wasisahau, hivyo kuendelea kujihusisha nalo. Wale waliojiunga (subscribers) hujibu kwenye utambulisho mfupi (short code)/nenofunguo kwa kutumia namba au herufi katika upigaji kura.</p>
	<ul> 
	<li type="none">Mfano: Nani huunda magari bora zaidi? Tuma kura yako kwa SMS. �A � Toyota, B � Nissan, C � Mercedes, D � Suzuki�</li>
	</ul>
		
	<h2>Bahati Nasibu kwa SMS</h2>
	<p>Bahati nasibu ni nzuri sana kwa kuwashirikisha watu katika kujenga msisimko kuhusu bidhaa, huduma au jambo. Wale waliojiunga wataitikia kwenye utambulisho mfupi/nenofunguo kwa kutuma jibu la swali au kutuma majina yao ili kuweza kuingia kwenye droo ya bahati nasibu.</p>
	<ul> 
	<li type="none">Mfano: Tuma jina lako ili upate nafasi ya kushinda tiketi ya bure kwenda Dubai.</li>
	</ul>
	
	<h2>�Survey� kwa SMS</h2>
	<p>Kama unahitaji kukusanya taarifa au data kutoka kwenye idadi kubwa ya watu, hii hasa ndiyo kwa ajili yako. Kusanya data kama vile jina, umri, n.k., na hata kutuma SMS ya makosa pale mteja anapotuma ingizo lisilo sahihi. Wale waliojiunga hujibu kutokana na utambulisho mfupi/nenofunguo kwa kutuma data uliyowaomba.</p>
	<ul> 
	<li type="none">Mfano: Jibu kwa kutuma jina la Kwanza na la Ukoo kwa ajili ya databesi ya wafanyakazi wa kampuni.</li>
	</ul>
	
	<h2>SMS Zinazohitajiwa</h2>
	<p>Fanya iwe rahisi na �interactive� kwa wateja kupata taarifa kuhusu kampuni yako. Wateja huitikia kutokana na utambulisho mfupi/nenofunguo, kisha wanapokea taarifa au menyu yenye �options� kwa ajili ya kupata taarifa zaidi. Hii inaweza kuwa ni pamoja na mahali duka lilipo, muda, ofa, huduma zitolewazo, n.k.</p>
	<ul> 
	<li type="none">Mtumaji anatuma �Bongo�</li>
	<li type="none">Mwitikio: Bongo Live! � Iko 212 Magore St, Upanga. Jibu: �A� � SMS za Makundi, �B� � Kujitangaza kwa SMS, �C� � Omba Kupigiwa.</li>
	</ul>
 </p>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>