<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
    <h1>{$smarty.const.ONLINE_ADVERTISING}</h1>
    <p>Bongo Live now lets you bring your adverts to some of the most popular online destinations the world. Millions of people browse the internet around the world and in Tanzania. Now you can reach them directly where they spend their time. We combine an indepth experience of multiple online mediums, including Google, Social Media and Email. Social media is a term used for services such as Facebook, Twitter and LinkedIn that are popular online sharing communities with hundreds of thousands of subscribers in Tanzania. These mediums combined have an immense potential to showcase your brand, product or service and cannot be ignored. </p>

    <h2>Google</h2>
    <p>Google search and Google products such as Gmail and Youtube are among the world's most popular web sites. Even in East Africa these are among the top 5 visited sites. Online visitors to these sites are of all ranges and backgrounds but especially those that are well off and can afford access to a computer and internet. Bongo Live will manage Google Ads for clients across all their networks and target those interested in your products and services as well as related content.</p>
    
     <h2>Facebook</h2>
<p>Facebook has over 750million users worldwide and hundreds of thousands users in Tanzania. Young people in the age ranges of 18-35 spend massive amounts of time on Facebook daily and this is the place to engage them and establish your brand. Facebookis visited by people of all ages and backgrounds but we can target individuals based on specific demographic criteria such as age, interests and location thus maximizing the impact of your adverts. Bongo Live manages all aspects of your Facebook campaign including desigining and managing your Facebook page, adverts, posts and integrating Facebook with other mediums. </p>
    	
    <h2>Email </h2>
<p>Almost anyone that is using the web has an email address, what better way to reach someone then through email. Bongo Live's email campaign reach our thousands of subscribers directly in their inbox with a eye catching e-flyer. Whats more Bongo Live uses its demographic profiles to send emails to targeted individuals that would bring a higher response rate for you. </p>

   	<h2>How Do You Benefit?</h2>
	<ul>
	  <li type="circle">Online users are usually employed staff, businessmen or wealthy individuals
	  <li type="circle">Online users have a  greater purchasing power
	  <li type="circle">Be one of the first to advertise on these mediums in Tanzania
	  <li type="circle">Drive traffic for a promotion to your website or store
   	  <li type="circle">Increase attendance at events, seminars and workshops
	  <li type="circle">Integrate online and social media advertising with our sms adverts
	  <li type="circle">We manage your complete branding and online presence
	  <li type="circle">Guaranteed brand awareness and advert clicks
	</ul>
    
    <br />
    <p><a href="www.bongolive.co.tz/contactus.php">Contact us</a> today to get a free evaluation of how you could use online advertising.</p>

  </div>
		
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>