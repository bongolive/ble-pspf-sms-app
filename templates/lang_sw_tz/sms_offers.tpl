<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.MENU_SERVICES_SMS_OFFERS}</h1>
    <p>Je ungependa kupokea offa moto moto kwa meseji? Bongo Live! tunakutumia offa maalum zinazolingana na &quot;profile&quot; (jinsia, umri, uyapendayo etc) yako.</p>
    <p>Sisi hatupendi kutuma meseji hovyo hovyo,  na haswa wakati wa usiku sana na asubuhi mapema. Bongo Live! tunatuma offa maalum zinazohusu mambo yale tu uyapendayo na kati ya saa 3 asubuhi na saa 3 usiku. Aidha, unaweza ku &quot;login&quot; tena kubadilisha mapendekezo yako au kusimamisha huduma yetu, wakati wowote.</p>
<p>Aina za offa tunazotuma:</p>
    <ul>
      <li>Simu, Elektroniki &amp;  Kompyuta</li>
      <li>Urembo, Mitindo &amp;  Mavazi</li>
      <li>Vyakula &amp; Vinywaji</li>
      <li>Mashine &amp; Vifaa</li>
      <li>Business Services</li>
      <li>Huduma za Biashara</li>
      <li>Makazi, Mapambo &amp;  Fanicha</li>
      <li>Safari &amp; Likizo</li>
      <li>Muziki, Burudani &amp;  Michezo</li>
      <li>Kazi &amp; Elimu</li>
      <li>Kazi &amp; Ajira</li>
      <li>Mali Isiyohamishika &amp;  Huduma</li>
      <li>Huduma za Intaneti</li>
      <li>Vyombo vya Usafiri    </li>
    </ul>
    <p></p>
    <p>Je, ungependa kuanza kupokea offa kwa meseji? <a href="sub_register.php"><strong>Jiunge!</strong></a></p>
<p>Je, unazo maswali zaidi? Wasiliana nasi <a href="contactus.php">hapa</a>.</p></div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>