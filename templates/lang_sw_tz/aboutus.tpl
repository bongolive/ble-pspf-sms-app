<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
    <h1>{$smarty.const.ABOUT_US}</h1>
    <p>Bongo live ni kampuni inayojishughulisha na kutoa huduma kwa njia ya SMS. Makao makuu ya kampuni hii yako Dar es Salaam, Tanzania. Sisi ni timu ya watu tulioamua kuleta huduma za kiwango cha juu hapa Tanzania ambazo zinafungulia nguvu ya simu za mkononi na SMS. Sisi tunaamini kuwa zipo fursa nyingi zinazoweza kuvumbuliwa kutokana na nguvu ya simu za mkononi, hivyo tunataka kutoa huduma zinazoziwezesha na kuzipa nguvu biashara, NGO, idara za serikali na hata watumiaji wa kawaida.</p>
	
	<p>Huduma zetu zinajumuisha utumaji was SMS za makundi, ofa za SMS zilizolengwa kwa watu maalum, huduma za SMS kulingana na mahitaji  mahsusi (bahati nasibu, kupiga kura, ‘survey’) pamoja na ‘application’ zinazoendana na mashirika na watu binafsi wa aina na saizi tofauti. Hivyo, bila kujali mahitaji yako ni yapi, kiwango chake na bajeti yako, Bongo Live! itafanya kazi pamoja nawe. Imani yetu ni kuwa hakuna jambo linalofanikisha biashara kama mteja aliyeridhishwa na huduma; nasi tutafanya kila juhudi ili kulitimiza hilo. Kila wakati tunajibidisha ili kuboresha huduma zetu na tutashukuru kupokea maoni yoyote, yawe chanya au hasi.</p>

<br />
	<h2>Tulianzaje?</h2>
	<p>Timu ya waasisi wa Bongo Live! ni ya watu ambao walikuwa wanajihusisha na biashara nchini Tanzania, ama moja kwa moja au kupitia familia zao. Changamoto kubwa ambayo imekuwa inazikabili biashara zote ndogo ni kukosekana kwa fursa ya kujitangaza na kuzijengea majina. Sababu kubwa ya hali hii imekuwa ni kutojua, na pili ni uhaba wa fedha. Matokeo yake ilikuwa ni kuundwa kwa Bongo Live! mwezi Machi 2010 ili kutumia nguvu ya ushawishi ya mlipuko wa simu za mkononi na kufanya mawasiliano na kujitangaza kibiashara kuwe rahisi, kuwe na nguvu, na kuwe ni kwa gharama nafuu.</p>

     <br/>
	<h2>Wateja Wetu</h2>
	<p>Wanunuzi ambao, kupitia moja kwa moja kwenye simu zao, wako tayari kupokea ofa zilizolengwa kwa watu maalum, na zilizoandaliwa kulingana kabisa na mahitaji yao.</p>
	<p>Biashara za humu ndani Tanzania zenye shauku ya kufanya kampeni zenye nguvu za kibiashara kwa  njia ya simu za mkononi; kampeni ambazo zinaleta matokeo makubwa na zitakazowafanya wateja wao waendelee kuja kwao tena na tena.</p>
	<p>Biashara, NGO na taasisi za serikali zenye nia ya kuanzisha na kuendeleza matumizi ya simu za mkononi na SMS ili kusambaza na/au kukusanya taarifa toka kwa mtu mmoja mmoja.</p>

<br />
    <h2>Kile Tunachoamini</h2>
	<ul>
		<li type="circle">Uzoefu mkubwa kuhusu utoaji wa huduma kwa wateja
		<li type="circle">Kukupatia kile tunachoahidi
		<li type="circle">Teknolojia inakupatia uwezo
		<li type="circle">Urahisi (simplicity)
	</ul>
	<br/>

  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>