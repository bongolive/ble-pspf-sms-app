<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
		<h1>{$smarty.const.CUSTOMERS}</h1>
	
	<p> Bongo Live ina hudumia wateja aina mbali mbali, wateja wakubwa wenye viwanda pamoja na biashara za wastani na ndogo ndogo, waliopo katika sekta mbali mbali na kutumia huduma mbali mbali zinazoendeshwa na Bongo Live.</p>
<p>Wateja wetu wapo katika sekta zifatazwo</p>

	<p align="center"><a href="#insurance">Bima</a> | <a href="#consumerbrands">Bidhaa kwa Walaji</a> | <a href="#education">Elimu</a> | <a href="#foodentertainment"> Vyakula & Burudani</a> </p> 
    <p align="center"> <a href="#technologysoftware"> Teknolojia & Software</a> | <a href="#promoters">Wadhamini & Wapangaji Matukio</a> | <a href="#tourismhospitality">Utalii & Ukarimu</a>  </p>
	<p align="center"> <a href="#government">Idara za Serekali</a> | <a href="#religiousgroups"> Vikundi vya Kidini</a> |  <a href="#other"> Wengineo</a> </p>
    <br/>
	<p><a name="insurance">Bima</a></p>
		<ul>
		<li type="circle">Golden Crescent Assurance Ltd
		<li type="circle">Milembe Insurance Brokers
		<li type="circle">Pride Insurance Brokers
   		<li type="circle">Lumumba Insurance Brokers
		</ul>
     <br/>
	<p><a name="consumerbrands">Bidhaa kwa Walaji</a></p>
		<ul>
            <li type="circle">Tanzania Breweries Ltd (Grand Malt)
            <li type="circle">Toto Junction
            <li type="circle">World of Shoes
            <li type="circle">Mazeline Boutique
            <li type="circle">Beauty Point
            <li type="circle">BluCheriMoe Boutique
            <li type="circle">Shoe Lounge
            <li type="circle">Lifesytle
            <li type="circle">Tausi Enterprises Ltd
		</ul>
     <br/> 
 	    <p><a name="technologysoftware">Teknolojia & Software</a></p>
		<ul>
            <li type="circle">Computech ICS (T) Ltd
            <li type="circle">Cats Net Ltd
            <li type="circle">Burhani Infosys Ltd
            <li type="circle">Ark Technologies
            <li type="circle">JSK.COM
            <li type="circle">Computronix Ltd
            <li type="circle">City Axis Computers Ltd
            <li type="circle">TJD Multimedia Computers Ltd
            <li type="circle">Catridges & Toners Ltd
          <li type="circle">Maisha Computers
          <li type="circle">Technotion Ltd
		</ul>
     <br/>
	 	 <p><a name="tourismhospitality">Utalii & Ukarimu</a></p>
		<ul>
          <li type="circle">Kilimanjaro Kempinski Hotel
          <li type="circle">Travel Start (www.travelstart.co.tz)
          <li type="circle">Fortune Travels Ltd
		</ul>
     <br/>
	 	 <p><a name="education">Elimu</a></p>
		<ul>
          <li type="circle">Global Education Placement
          <li type="circle">UniServe Tanzania
          <li type="circle">Rusoma Services Ltd
          <li type="circle">London School of Commerce
          <li type="circle">MUE.CO.TZ
		</ul>
     <br/>
	 	<p><a name="foodentertainment">Vyakula & Burudani</a></p>
		<ul>
		<li type="circle">Shahi Refresh Once More Cafe
		</ul>
     <br/>
	 	 <p><a name="promoters">Wadhamini & Wapangaji Matukio</a></p>
		<ul>
		<li type="circle">Vayle Springs Ltd
		<li type="circle">Dar Fashion Festival
		</ul>
     <br/>
 	<p><a name="government">Idara za Serekali</a></p>
		<ul>
		<li type="circle">Tanzania Small Holders Tea Agency
		<li type="circle">Engineers Registration Board		
   		<li type="circle">Engineers Institute of Tanzania
		</ul>
     <br/>
		<p><a name="religiousgroups">Vikundi vya Kidini</a></p>
		<ul>
		<li type="circle">Dawoodi Bohra Jamaat
		<li type="circle">Kokni Muslim Jamaat
		</ul>
     <br/>
   	 	<p><a name="other">Wengineo</a></p>
		<ul>
		<li type="circle">Megawealth Club
   		<li type="circle">Oriflame (T) Ltd
		<li type="circle">Imaging Smart Printers
   		<li type="circle">Egg Energy
   		<li type="circle">Media Tactic Ltd
   		<li type="circle">Kilimanjaro Cables Ltd
   		<li type="circle">Hodi Tanzania Ltd
   		<li type="circle">M2 Advertising Ltd
		</ul>
     <br/>
	  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>