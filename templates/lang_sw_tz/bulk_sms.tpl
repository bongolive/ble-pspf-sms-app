<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
    <h1>{$smarty.const.GROUP_SMS}</h1>
    <p>Mfumo wa kipekee wa Bongo Live! unaotumia kompyuta unakuwezesha kutuma SMS  katika mitandao yote ya simu Tanzania kwa wateja waliojiunga. Tumia huduma zetu kwa malengo ya aina mbalimbali kuanzia kutuma ujumbe wa kukumbushia mambo mbalimbali hadi kusalimia. Mtu mmoja na hata mashirika wanaweza kutumia fursa hii kuwafikia maelfu ya watu ndani ya sekunde chache tu kupitia simu zao za mkononi.</p>
    <p>Mara tu unapojiunga, unakuwa na uhuru wa kuingia kwenye �application� yetu ya kwenye mtandao ambayo ina namna (features) nyingi mbalimbali zinazokupatia urahisi wa kutuma SMS hizi.</p>
	
    <h2>�Features� za Msingi</h2>
	<ul>
		<li type="circle">Ongeza mawasiliano ya mtu mmoja mmoja au wengi kwa mara moja kutoka kwenye faili la ekseli (Excel)
		<li type="circle">Shughulikia contacts zako 'online' na unaweza hata kuwapanga makundi makundi
		<li type="circle">Ni salama na asikoweza kuona mtu mwingine yeyote. Wewe tu ndiwe mwenye kuwaona contacts zako
		<li type="circle">Panga meseji zako ili ziweze kwenda kwa wale uliowalenga katika tarehe/muda wa baadaye
		<li type="circle">Kiwango cha malipo kidogo na kile kile kwa kila SMS, yaani �flat rate�
		<li type="circle">Nunua SMS kwa kutumia Airtel Money, Z-Pesa, Tigo Pesa, M-PESA na fedha taslimu
		<li type="circle">Pata diskaunti kwa kununua SMS jumla
	</ul>
	<h2>Naweza Kutumiaje Huduma ya SMS Makundi/Jumla?</h2>
	<ul>
		<li type="circle">Tuma SMS za kukumbusha matukio, malipo na miadi
		<li type="circle">Tuma ofa na diskaunti ili kuwaleta wateja kwako
		<li type="circle">Tambulisha bidhaa na huduma mpya ili kuwajulisha wateja
		<li type="circle">Tuma taarifa mpya za dharura kwa haraka katika dakika za mwisho
		<li type="circle">Kusanya taarifa toka kwa watu wengi kwa mara moja
		<li type="circle">Tuma salamu na jumbe kwa marafiki na familia
	</ul>
	 <h2>Nani Anaweza Kutumia Huduma ya SMS Makundi/Jumla?</h2>
	<ul>
		<li type="circle">Biashara ndogo na kubwa
		<li type="circle">Taasisi za serikali
		<li type="circle">NGO na vikundi vya kijamii
		<li type="circle">Shule na taaisisi za juu za elimu
		<li type="circle">Wapangaji wa matukio (harusi, semina, washa, makongamano)
		<li type="circle">Vikundi vya kidini
		<li type="circle">Migahawa
		<li type="circle">Watu binafsi
	</ul>

  </div>
		
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>