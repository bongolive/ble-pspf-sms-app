<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
		 <h1>{$smarty.const.FAQ_}</h1>
		<br />
		<h2>Meseji za Makundi/Jumla</h2>
		<ol type="1">
			<li><a href="#gm1">Je, mtu mwingine anaweza kupatiwa orodha ya wateja wangu?</a></li>
			<li><a href="#gm2">Je, kutuma SMS inagharimu kiasi gani?</a></li>
			<li><a href="#gm3">Naweza kutuma SMS ngapi kwa mara moja?</a></li>
			<li><a href="#gm4">Mpokeaji wa SMS ataipokea toka kwa nani?</a></li>
			<li><a href="#gm5">Mimi ni �broadcaster�. Nitajuaje kama SMS imepokelewa?</a></li>
		</ol>
		
			<br />
		<h2>Matangazo za SMS</h2>
		<ol type="1">
			<li><a href="#t1">1.	Je, mnatuma SMS kwa namba nyingi zozote?</a></li>
			<li><a href="#t2">2.	Sina uhakika wa jinsi ya kuandika matangazo ya SMS. Mtanisaidia?</a></li>
		</ol>
	
		<br />
		<h2>Maswali Mengine</h2>
		<ol type="1">
			<li><a href="#g1">Mfumo wenu unafanyaje kazi?</a></li>
			<li><a href="#g2">Nitatakiwa kuweka �software� yoyote kwenye kompyuta yangu?</a></li>
			<li><a href="#g3">Nitawalipaje Bongo Live!?</a></li>
		</ol>	
	
	<br />
		<h2>Meseji za Makundi/Jumla</h2>
	
			<ol type="1">
			<li><b><a name="gm1">Je, mtu mwingine anaweza kupatiwa orodha ya wateja wangu?</a></b></li>
				<p>Hapana. Bongo Live! tuko makini kwa kiwango cha juu kuhakikisha kuwa orodha ya watu wako  iko salama kabisa na kwamba ni matangazo ya SMS tu ndiyo yanayotumwa kwa wateja wako. Unaweza kusoma sera yetu kuhusu usiri ili kufahamu zaidi.</p>
			<li><b><a name="gm2">Je, kutuma SMS inagharimu kiasi gani?</a></b></li>
			<p>Unaweza kufahamu zaidi kuhusu viwango vyetu vya malipo kupitia <a href="pricing.php">Ukurasa wa Gharama.</a></p>
			
			<li><b><a name="gm3">Naweza kutuma SMS ngapi kwa mara moja?</a></b></li>
			<p>Hakuna kikomo cha idadi ya SMS unazoweza kutuma kutoka kwenye mfumo wetu kwa mara moja. Tumeshatuma maelfu ya SMS kwa mara moja.</p>
			
			<li><b><a name="gm4">Mpokeaji wa SMS ataipokea toka kwa nani?</a></b></li>
			<p>Waliojiunga watapokea SMS zenye ofa toka kwenye majina ya mtumaji �Bongo Live!�.</p>
			<p> �Broadcaster� wana uwezo wa kuomba majina mbalimbali kwa ajili ya utumaji.</p>
			
			<li><b><a name="gm5">Mimi ni �broadcaster�. Nitajuaje kama SMS imepokelewa?</a></b>?</li>
			<p>Bongo Live! inaendelea wakati wote kufuatilia shughuli za SMS na inapokea ripoti juu ya meseji zote zilizotumwa.</p>		
		</ol>
	
	<br />
		<h2>Matangazo za SMS</h2>
		<ol type="1">
			<li><b><a name="t1">Je, mnatuma SMS kwa namba nyingi zozote?</a></b></li>
				<p>Kusambaza meseji kwa namba zozote nyingi haikuhakikishii kuwa utapata mwitikio. Kwa mfano, tangazo la SMS kuhusu saluni ya urembo ya wanawake linaweza kupokelewa na mwanamume wa miaka 50. Sasa hiyo haina maana wala faida kibiashara, au siyo? </p>
			<li><b><a name="t2">2.	Sina uhakika wa jinsi ya kuandika matangazo ya SMS. Mtanisaidia?</a></b></li>
			<p>Timu yetu ya washauri wa mambo ya masoko watafurahi kukusidia kutengeneza matangazo ya SMS yaliyosanifiwa vizuri  na yanayoendana na malengo ya kujitangaza kwako  na pia kuonyesha bidhaa yako. Kwa mfano, duka la nguo lililoko Msasani lingependa kuwafikia wanaume wanaoishi maeneo ya karibu na duka hilo, lakini wanaopenda fasheni.</a></p>
		</ol>
		
	<br />
		<h2>Maswali Mengine </h2>
		<ol type="1">
			<li><b><a name="g1">1.	Mfumo wenu unafanyaje kazi?</a></b></li>
				<p>Bongo Live! inatumia mfumo wa makundi kutuma matangazo ya SMS kwa kila mmoja wa wateja wake. Ukiwa ni mtu aliyejiunga ili kupokea ofa kwa SMS, mfumo wetu unakutafutia ofa zinazoendana na taarifa zafko na mambo uyapendayo na kuzituma kwako. �Broadcaster� wana uwezo wa kutuma SMS kwa makundi yao yoyote ya watu wao kwa kuingia kwenye tovuti na kutumia jukwaa au ulingo wetu ambao umeandaliwa kulingana na mahitaji yao.</p>
			<li><b><a name="g2">2.	Nitatakiwa kuweka �software� yoyote kwenye kompyuta yangu?</a></b></li>
			<p>Huhitaji kuweka �software� yoyote maalum. Karibu kompyuta zote zina taftishi za mtandao, kwa mfano Internet Explorer, Firefox na Google Chrome zinazoweza kutumiwa kuzifikia huduma zetu kupitia kwenye tovuti yetu ya www.bongolive.co.tz</a></p>
			<li><b><a name="g3">3.	Nitawalipaje Bongo Live!?</a></b></li>
			<p>Tunapokea malipo kupitia Airtel Money, M-PESA, Tigo Pesa, Z-Pesa au credit transfer. Airtel Money - 0688 121 252, MPESA � 0765 121 252, Tigo Pesa - 0655 121 252. Simu ya mkononi unayoitumia kutulipa ni lazima iwe imesajiliwa kwetu kwa ajili ya biashara yako. Tunapokea pia malipo ya fedha taslimu.</a></p>
		</ol>
			
	  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>