<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
		<h1>{$smarty.const.WHO_CAN_USE_THIS}</h1>
	
	<p>�Application� za SMS na simu zinaweza kutumika kwenye maeneo mbalimbali na kwa matumizi tofauti na ya kipekee. Huduma hizi zinaweza kuboresha biashara yako na kuongeza mauzo, kujenga utii wa wateja  kwa bidhaa, kuifanya bidhaa ijulikane kwa wateja, kuboresha huduma kwa wateja, kushughulikia haraka oda za wateja, n.k. Huduma zinaweza kujumuisha kuwawezesha wateja kupata taarifa pale wanapozihitaji, utumaji wa jumbe za kuhadharisha na kukumbusha; na pia kutangaza bidhaa na huduma.</p>
	
	<p>Hapa chini ni orodha ya baadhi ya njia chache kuonyesha ambavyo SMS zinaweza kutumika katika shughuli zako:</p>
	<p align="center"><a href="#insurance">Bima</a> | <a href="#financebanking">Fedha & Benki</a> | <a href="#consumerbrands">Bidhaa kwa Walaji</a> | <a href="#mediapublishing">Vyombo vya habari na Uchapishaji</a>  | <a href="#education">Elimu</a> </p> 
	<p align="center"><a href="#supermarkets">Supamaketi</a> | <a href="#foodentertainment">Vyakula & Burudani</a> | <a href="#technologysoftware"> Teknolojia & Software</a>  | <a href="#promoters">Wadhamini & Wapangaji Matukio</a> </p>
	<p align="center"> <a href="#tourismhospitality">Utalii & Ukarimu</a> |  <a href="#ngosocialgroups">NGO & Vikundi vya Kijamii</a> |  <a href="#religiousgroups"> Vikundi vya Kidini</a> </p>
	<br/>
	<p><a name="insurance">Bima</a></p>
		<ul>
		<li type="circle">Kukumbushia ufanyaji malipo
		<li type="circle">Kutaarifu wateja juu ya huduma mpya
		<li type="circle">Kutaarifu wateja juu ya huduma mpya
		<li type="circle">Kupata Ofisi/Wakala aliye karibu
		</ul>
     <br/>
	<p><a name="financebanking">Fedha & Benki</a></p>
		<ul>
		<li type="circle">Kutuma hadhari kuhusu udanganyifu
		<li type="circle">Kukumbushia malipo
		<li type="circle">Marekebisho/maulizo kuhusu salio
		<li type="circle">Bei ya hisa
		</ul>
     <br/>
		<p><a name="consumerbrands">Bidhaa kwa Walaji</a></p>
		<ul>
		<li type="circle">Uzinduzi wa bidhaa mpya
		<li type="circle">Kuinua kiwango cha kufahamika kwa bidhaa
		<li type="circle">Kutuma ofa za punguzo
		<li type="circle">Kupata mrejesho kuhusu bidhaa
		<li type="circle">Kuendesha mashindano yenye tuzo
		</ul>
     <br/> 
	 	<p><a name="mediapublishing">Vyombo vya Habari & Uchapishaji</a></p>
		<ul>
		<li type="circle">Wasiliana na kushirikisha hadhira yako
		<li type="circle">Tuma habari/mambo  mapya
		<li type="circle">Pokea maoni ya wateja muda uleule wanapokuwa nayo
		<li type="circle">Pokea maombi muda uleule watu wanapokuwa nayo
		</ul>
     <br/>
	 	 <p><a name="technologysoftware">Teknolojia & �Software�</a></p>
		<ul>
		<li type="circle">Tuma SMS moja kwa moja toka kwenye tovuti yako au �application� kwa kutumia 
		<li type="circle">Andaa huduma zako mwenyewe za SMS na kutumia mfumo wetu
		</ul>
     <br/>
	 	 <p><a name="tourismhospitality">Utalii & Ukarimu</a></p>
		<ul>
		<li type="circle">Tuma uthibitisho wa �reservation�
		<li type="circle">Taarifa zinazoombwa papo hapo kuhusu ratiba, bei na mambo mengineyo
		<li type="circle">Tuma ofa za kipekee kwa wateja watiifu
		<li type="circle">Boresha huduma kwa wateja kwa kutuma meseji za kukumbusha na za �asante�
		</ul>
     <br/>
	 	 <p><a name="education">Elimu</a></p>
		<ul>
		<li type="circle">Kukumbushia malipo ya ada
		<li type="circle">Kutuma taarifa juu ya sera mpya
		<li type="circle">Wataarifu wanafunzi, wazazi na wengine kuhusu matukio
		<li type="circle">Kutoa tahadhari juu ya dharura au kuondoka mahali Fulani wakati wa hatari
		<li type="circle">Taarifa za kufunga shule/chuo
		</ul>
     <br/>
	 	<p><a name="foodentertainment">Vyakula & Burudani</a></p>
		<ul>
		<li type="circle">Tuma ofa za diskaunti kwa wateja
		<li type="circle">Ongeza wateja katika sikuza wiki  ambazo wateja ni wachache
		<li type="circle">Toa taarifa kuhusu matukio maalum
		<li type="circle">Kukumbushia kuhusu �reservation�
		<li type="circle">Kutoa oda kwa SMS
		<li type="circle">Kufanya �reservation� kwa SMS
		</ul>
     <br/>
	 	 <p><a name="promoters">Wadhamini & Wapangaji Matukio</a></p>
		<ul>
		<li type="circle">Wafanya wahusika wajumuike katika kupiga kura na kucheza bahati nasibu
		<li type="circle">Tuma jumbe za kukumbushia pamoja na mabadilko katika dakika za mwisho
		<li type="circle">Kualika/ kukumbusha marafiki na ndugu walio mbali kuhusu harusi
		</ul>
     <br/>
		 	<p><a name="supermarkets">Supamaketi</a></p>
		<ul>
		<li type="circle">Ofa za diskaunti maalum za kila siku au kila wiki
		<li type="circle">Zawadia wateja waaminifu kwa kutumia vocha
		<li type="circle">Toa oda, uliza bei na uwepo wa bidhaa kwa SMS
		<li type="circle">Toa taarifa juu ya ofa zinazohusu bidhaa zinazokaribia kwisha muda wa matumizi
		</ul>
    	     <br/>
		 	<p><a name="ngosocialgroups">NGO & Vikundi vya Kijamii</a></p>
		<ul>
		<li type="circle">Hamasisha haraka wanachama/wahamasishaji
		<li type="circle">Kusanya kwa urahisi data za ubora wa juu toka kwa wawakilishi walio kwenye �field�
		<li type="circle">Punguza urefu wa muda wa kupokea  majibu kutokana na ufuatiliaji ulio bora
		<li type="circle">Punguza wizi na udanganyifu kutokana na kuboreshwa kwa ufuatiliaji
		<li type="circle">Punguza matumizi kwa kupunguza gharama za safari za kwenda kukusanya data
		</ul>
     <br/>
		<p><a name="religiousgroups">Vikundi vya Kidini</a></p>
		<ul>
		<li type="circle">Wataarifu waumini kuhusu mabadiliko yanayotokea dakika za mwisho
		<li type="circle">Kusanya haraka taarifa au RSVP toka kwa waumini au wajumbe
		<li type="circle">Tangaza harusi, hafla, misiba
		</ul>
     <br/>
	  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>