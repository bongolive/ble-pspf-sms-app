<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.SMS_ADVERTISING}</h1>
    <p>Je, unapoteza wateja kwa vile wanakimbilia kwa washindani wako? Je, ungependa kuona matokeo halisi ya kujitangaza kwako kibiashara? Matangazo kwa kutumia SMS ni njia mwafaka ya kutangaza biashara. Ni njia inayoleta ndani ya bishara yako ile nguvu ya simu za mkononi na uwezo was SMS wa kufika kila mahali. Mfumo wa Bongo Live! wa kuwasiliana na makundi ya watu kwa mara moja unaziwezesha biashara kuwafikia wateja wapya kokote Tanzania moja kwa moja kupitia simu zao za mkononi.</p>
    <p>Bongo Live! ina watu wengi ambao wameshajiunga na jukwaa letu ili waweze kupokea matangazo yetu kwa njia ya SMS. Tunakusanya taarifa za aina mbalimbali kuhusiana na hao waliojiunga nasi, zikiwamo takwimu zao za kidemografia. Hiyo inatoa uhakika kwamba, watu hao wataweza kupokea matangazo yale tu ambayo yanaendana na jinsi wao walivyo na ambayo yanawavutia. Matokeo yake kwa mtu au kampuni inayotuma matangazo ni kuwa, wanakuwa na uwezo wa kuwalenga watu sahihi, hivyo kuwawezesha watumaji hao kupokea kwa kiwango kikubwa majibu kutokana na huko kujitangaza kwao kibiashara.</p>
	<p>Iwapo una wasiwasi na bajeti yako, usiwe na shaka! Bongo Live! tunaweza kukutengenezea kampeni kulingana kabisa na bajeti yako, huku tukihakikisha inakuwa na matokeo makubwa yatakayokuletea wateja wapya.  Hii inahusu biashara zote, kubwa na ndogo. Bila kujali saizi ya biashara yako, kampeni za Bongo Live! zitatimiza shauku yako.</p>
	<p>Je, ungependa kupata huduma hii? Wasiliana nasi<a href="contactus.php">hapa.</a></p>
	<h2>�Features� za Muhimu</h2>
	<ul>
		<li type="circle">Taarifa za kidemografia, kwa mfano umri, jinsia, kazi, makazi</li>
		<li type="circle">Tunafika kila kona ya nchi</li>
		<li type="circle">Mitandao ya simu Tigo, Zantel, Airtel na Vodacom inaweza kutumika</li>
		<li type="circle">Usimamiaji wa kampeni kupitia kwenye mtandao</li>
		<li type="circle">Hakuna gharama zingine zilizojificha</li>
	</ul>

    <h2>Biashara Yako Inafaidikaje?</h2>
	<ul>
		<li type="circle">o	Kuwa wa HALI YA JUU � onekana kama kampuni ya kimataifa</li>
		<li type="circle">o	Wazawadie wateja wako walio waaminifu zaidi</li>
		<li type="circle">o	Wafanye wateja wako waendelee kununua bidhaa zako</li>
		<li type="circle">o	Imarisha utambulisho wa bidhaa yako</li>
		<li type="circle">o	Wafikie wateja wapya</li>
		<li type="circle">o	Matokeo bora kutokana na kuwekeza kwako kwenye kujitangaza kibiashara</li>
	</ul>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>