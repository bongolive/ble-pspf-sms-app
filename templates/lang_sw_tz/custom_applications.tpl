<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
    <h1>{$smarty.const.CUSTOM_MOB_APPLICATIONS}</h1>

	<p>Simu ya mkononi imebadili karibu kila eneo la maisha yetu na hasa jinsi tunavyowasiliana. Biashara, taasisi za serikali, NGO na shirika lolote kubwa, sasa wanaweza kutumia nguvu ya ushawishi ya simu za mkononi kuboresha namna ya kubadilshana taarifa, kukusanya data, kuhudumia wateja na mengi mengineyo.</p>
		
	<p>Iwapo ofa za huduma za Bongo Live! haziendani na mahitaji ya shirika lako, tutafurahi kushirikiana nawe kutengeneza namna inayoendana na mahitaji hayo. Mtazamo wetu ni kushirikiana nawe ili kuelewa kwa undani utendaji wa shirika lako ili tuweze kuujumuisha katika utendaji wa kiteknolojia ambao, si tu utatimiza malengo ya shirika lako, bali pia utaboresha sana utendaji mzima. Cha muhimu zadi ni kuwa, tunaangalia mahitaji yako na baadaye kupendekeza namna ya kuyatimiza. Tunatambua kuwa masuluhisho ya kiteknolojia mara nyingi huweza kuwa rahisi na ndiyo maana urahisi ni moja ya maadili yetu ya msingi kabisa.</p>
	
	<br/>
	
	<h2>Mtazamo wa Bongo Live!</h2>
	<ul>
		<li type="circle">Kukusanya welewa wa kina kuhusu mahitaji
		<li type="circle">Kuandaa mfano au sampuli na kupata idhini ya mteja
		<li type="circle">Kutengeneza suluhisho na kulijaribu
		<li type="circle">Kuwafundisha watumiaji, kisha kutumia suluhisho
		<li type="circle">Kuendelea kutoa msaada wa kudumu
	</ul>
	
	<br/>
	
	<h2>Mifano ya masuluhisho kwa wateja ni pamoja na:</h2>
	<ul>
		<li type="circle">Mfumo unaojiendesha wenyewe ambao unawakumbusha wateja kuweka fedha kwenye akaunti zao za benki 
		<li type="circle">‘Application’ ya Nokia au Blackberry kwa ajili ya kufuatilia orodha ya vitu au kiwango cha biashara
		<li type="circle">Kitengo cha msaada kwa wateja kwa ajili ya kushughulikia malalamiko yanayopokelewa kupitia SMS
		<li type="circle">'Website' kushugulikia malalamiko ya wateja wilivyo pokelewa kwa sms
	</ul>
	<br/>
	<p>Wasiliana nasi <a href="contactus.php">hapa</a> kupata ushauri wa bure ili  kutathmini mahitaji yako. Hatukulazimishi kufanya chochote baada ya ushauri huo.</p>
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>