<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.PRIVACY_POLICY}</h1>
	
	<h2>TAFADHALI SOMA MAKUBALIANO YAFUATAYO KWA MAKINI</h2>
	
	<p>Katika Bongo Live tumejitoa kulinda usiri  wako na kujibidisha  kuifanya tovuti yetu kuwa mazingira salama kwa kila mtu anayetembelea na kutumia huduma za Bongo Live! Madhumuni ya tamko hili la sera ni kukushirikisha mambo yafuatayo:</p>
	
	<p>1.   Jinsi tunavyokusanya taarifa kwa ajili ya wateja wetu
2.	Jinsi tunavyotumia taarifa tulizokusanya kutoka kwa watumiaji wetu
3.	Usalama wa Taarifa za Databesi
4.	Kupitia upya/Kubadili Taarifa
5.	Ukomo wa Uwezo Wetu
6.	Utaratibu wa Malalamiko
</p>
	
	<h2>1.	Jinsi Tunavyokusanya taarifa kwa ajili ya watumiaji wetu</h2>
		<p>Hatuwezi kamwe kukusanya taarifa nyeti zinazokuhusu bila ridhaa ya wazi kutoka kwako. Unapojiunga na tovuti ya Bongo Live kama �broadcaster� au mtu aliyejiunga kwa kujaza na kutuma fomu ya kujiunga iliyo kwenye tovuti ya Bongo Live, akaunti inayokuhusu wewe inajitengeneza yenyewe.</p>
		<p>Unapotembelea tovuti ya Bongo Live, seva za tovuti yetu zinaweza kutumia teknolojia kufuatilia tabia au mazoea  ya wale wanaotembelea tovuti na kukusanya taarifa kama vile: ametembelea mara ngapi, wastani wa muda aliotumia, kurasa alizotembelea, jina la �domain� aliyotumia kuingia kwenye intaneti na tovuti alikotokea ili kuja kutembelea tovuti yetu.</p>
		
	<h2>2.	Jinsi tunavyotumia taarifa tulizokusanya kutoka kwa watumiaji wetu</h2>
		<p>Taarifa zako binafsi hazitatumika au kuwekwa wazi kwa malengo tofauti na yale yaliyomo ndani ya sera hii ya Bongo Live kuhusu usiri. Hatutauza au kuazimisha taarifa zako binafsi ulizotupatia. Tunatumia taarifa zako kufuatilia huduma za Bongo Live ambazo umechagua kuzipokea, kisha tunafanya uchambuzi wa kitakwimu juu ya matumizi ya tovuti , kuboresha taarifa na bidhaa zetu tutoazo, na kuzifanya taarifa pamoja na mpangilio wake kwenye tovuti viendane na mahitaji ya wateja wetu. Tunafanya hivi ili kujaribu kuboresha tovuti  yetu na kuifanya itimize mahitaji ya watumiaji. Tunatumia taarifa za kwenye faili lako ambalo tunalitunza, na taarifa zingine ambazo tunazipata toka kwenye shughuli zako za sasa na zilizopita ndani ya tovuti kwa ajili ya kutatua migogoro, kubaini matatizo na kuhakikisha kuwa masharti yetu yanafuatwa. Bongo Live inaweza kutumia anwani yoyote ya baruapepe, namba ya simu au anwani ya makazi unayotupatia kwa lengo la kuwasiliana nawe mara kwa mara kuhusiana na notisi za kiutawala, taarifa za akaunti, taarifa mpya na maboresho ya Bongo Live na bidhaa zingine za Bongo Live zinazokuwa zimepatikana. Zaidi ya hayo, ni lazima tutii amri za mahakama, maombi kuhusu taarifa yanayotoka  kwa wakala au wadhibiti wa serikali na taratibu zingine za kisheria na kiudhibiti  zinazoweza kututaka tutoe taarifa binafsi zinazokutambulisha wewe.</p>
		
		<p>Endapo Bongo Live au mali zote za Bongo Live zitamilikishwa kwa mtu mwingine, tunaweza kuhamisha kwa mmiliki mpya taarifa za mteja, zikiwamo taarifa binafsi ulizotupatia wakati wa  utaratibu wa kujiunga, vinginevyo, hatutatumia kamwe  au kutoa taarifa zako binafsi kwa upande wa tatu isipokuwa kwa �service providers� wetu kama inavyotakiwa katika uendeshaji wa Bongo Live.</p>
		
		<h2>3.	 Usalama wa Taarifa za Databesi</h2>
		
		<p>Taarifa yoyote inayotumwa Bongo Live kwa ajili ya kutuma ujumbe wa maandishi na au kuhifadhiwa itabakia kuwa ni mali ya mwenye akaunti , ambayo Bongo Live tutaitunza  salama kabisa kulingana na sera na sheria zetu za ndani kuhusu usalama. Bongo Live itachuklua hatua za kutosha ili kulinda taarifa zako binafsi  kwa kutumia kinga za kiusalama zinazolingana na kiwango cha unyeti  wa taarifa kwa kutumia njia za kiteknolojia (kwa mfano firewalls, passwords, encryption) na mafunzo kwa wafanyakazi wake. Bongo Live wakati wowote, haitakusanya au kusambaza tena taarifa hii bila ridhaa yako, isipokuwa pale itakapotakiwa kisheria kufanya hivyo.</p>
		
			<h2>4.	Kupitia Upya/Kubadili Taarifa</h2>
		<p>Ni sera ya Bongo Live kwamba, kama ambavyo watumiaji wanaweza kujiunga na huduma yetu, wanayo pia haki ya kujiondoa. Mtumiaji anapojiondoa, hatapokea tena meseji zozote labda ajiunge tena. Bongo Live hatuwajibiki na meseji zinazotumwa na wateja wetu wanaotumia bidhaa au huduma za Bongo Live kutuma meseji. Ni wajibu wa �broadcaster� mwenyewe kupata ruhusa inayotakiwa ili kutuma meseji kwa walengwa.</p>
		<p>Endapo katika wakati wowote utataka kubadili taarifa yoyote kwenye akaunti yako, ingia kupitia kwenye Ukurasa wa Mwanzo. Hapa unaweza kubadili au kuingiza mambo mapya kwenye taarifa zako.</p>
		
		<h2>5.	Ukomo wa Uwezo Wetu</h2>
		<p>Ingawaje usiri wako ni muhimu sana kwetu, kutokana na mazingira ya kisheria na kiufundi yaliyopo, hatuwezi kutoa hakikisho kabisa kwamba taarifa binafsi zinazokutambulisha hazitaufikia upande wa tatu kwa njia ambazo hazijaelezwa ndani ya sera hii. Zaidi ya hapo, tunaweza (na wewe ndio utatupatia  ruhusa) kuweka wazi taarifa zinazokuhusu kwa vyombo binafsi, vyombo vya kisheria au maofisa wengine wa serikali, maana sisi kwa upande wetu tunaamini ni lazima au muhimu kushughulikia au kumaliza uchunguzi au matatizo.</p>
		
	  <h2>6.	Utaratibu wa Malalamiko</h2>
		<p>Endapo una malalamiko yopyote kuhusiana na eneo lolote la huduma zetu, tafadhali tutumie baruapepe kupitia contact(at) bongolive.co.tz. Tafadhali eleza kwa ufasaha malalamiko yako. Ndani ya siku 5 za kazi, tutakujibu kuwa tumeyapokea. Vilevile, utapatiwa jina la mawasiliano litakalohusika na kukuletea taarifa juu ya maendeleo ya malalamiko yoyote.</p>
<p>Bongo Live ina haki ya kubadili sera hii ya kwenye mtandao kuhusu usiri mara kwa mara itakapoona ni muhimu kufanya hivyo. Mabadiliko yatatumwa kwenye ukurasa wa tovuti unaohusu sera ya usiri na yataanza kufanya kazi mara yatakapokuwa yametumwa. Kuendelea kwako kutumia Bongo Live baada ya mabadiliko kwenye sera hii ya kwenye mtandao kuhusu usiri, kutachukuliwa kuwa ni kuyakubali mabadiliko hayo. Ni wajibu wako mwenyewe kufuatilia sera ya usiri ili kujua endapo kuna mabadilko yoyote mapya yaliyoingizwa. Sera hii ya usiri inategemea na inatumika katika sheria zote zinazohusu usiri.</p>
<p>Unakubali kuwa kukubaliana na sera hii kuhusu usiri ni sharti la wewe kutumia huduma zetu na unakubali kufuata masharti yake yote.</p>
<p>Iwapo una maswali zaidi kuhusu sera yetu ya usiri au utekelezaji wake, tafadhali wasiliana nasi kupitia contact (at) bongolive.co.tz
</p>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>