<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1>{$smarty.const.TERMS_OF_USE}</h1>
	
	<h2>PLEASE READ THIS AGREEMENT CAREFULLY</h2>
	
	<p>The following are the terms and conditions for use of bongolive.co.tz by any user.</p>
	
	<p>By clicking on the "ACCEPT" button or accessing and/or using the bongolive.co.tz web site, you agree that you have read, understand and accept all of the terms and conditions below for all use of the Bongolive.co.tz services.</p>
	
	<p>If you do not agree to these terms and conditions you may not use the Bongolive.co.tz website. Any use past the registration page of the Bongolive.co.tz website shall automatically bind the user to this agreement. If you do not agree to the terms and conditions OF THIS SERVICE AGREEMENT, OR CANNOT COMPLY WITH THESE TERMS AND CONDITIONS, you SHOULD CLOSE THE BROWSER WINDOW, TERMINATING THE REGISTRATION PROCESS WILLFULLY, AND AGREE TO STOP ALL USE OF THE SERVICE. By registering, you agree to receive messages from Bongo Live. Clicking "ACCEPT" indicates you agree to receive these messages.</p>
	
	<p>These terms and conditions are a binding and enforceable service agreement against all persons that access any part of the bongolive.co.tz website, and/or place any Bongo Live products on a website.</p>

<h2>Interpretations and Definitions</h2>
<p>The Bongolive.co.tz website encompass both the website located at www.Bongolive.co.tz and any page, element or part thereof, as owned and operated by Bongo Live.</p>
<p>Bongo Live includes the websites Bongolive.co.tz, Bongolive.co.tz, its subsidiaries, owners and shareholders.</p>
<p>User refers to any person or entity who enters or uses the Bongolive.co.tz website, notwithstanding the fact that such person only visits the home page of the Bongolive.co.tz website.</p>
<p>Hyperlinks herein should be deemed to be part of these terms and conditions.</p>

<h2>Services</h2>
<p>The Service consists of a browser interface, message application, SMS gateway access, data encryption, data transmission, data-access and data storage.</p>
<p>Bongo Live makes no guarantees as to the continuous availability of the service or any specific feature(s) of the service. Bongo Live reserves the right to change or terminate the service and/or alter the terms and conditions of this Agreement at any time with 14 days notice.</p>
<p>Messages shall be deemed to have been delivered when Bongo Live delivers the messages to the immediate destination, including SMTP servers, mobile telephone networks, or any other intermediary server/API that is designated the point of delivery for the message. Bongo Live does not guarantee delivery on behalf of mobile telephone providers.</p>
<p>Bongo Live makes no guarantees to its network coverage. Bongo Live provides extensive coverage to the majority of carriers, yet Mobile providers/carriers may opt to not be part of the Bongo Live network, resulting in undelivered messages.</p>

<h2>Eligibility</h2>
<p>You must be at least 18 years old to be eligible to use any of Bongo Live services. If you are at least 15 years old but younger then 18, you may use the Bongo Live services if you have your parents' or guardians' prior permission. No one under age 15 may use the Bongo Live services under any circumstances. By clicking the "ACCEPT" button you are representing that you are at least 18, or that you are at least 15 years old and have your parents' permission to register for the services.</p>

<h2>Use of Site</h2>
<p>Bongo Live licenses the User to view, download, save and print the content of the Bongolive.co.tz website, provided that such content is used for private, personal, educational and/or non-commercial purposes only or to view, download, save and print this agreement for legal and/or evidential purposes.</p>
<p>Content from the Bongolive.co.tz website may not be used or exploited by Users for any commercial and non-private purposes without the prior written consent of Bongo Live.</p>
<p>If any User uses content from the Bongolive.co.tz website in breach of any part of the Terms and Agreements, the following rights are given.</p>
<p>1.	Bongo Live reserves the right to claim damages from the User;
2.	Bongo Live reserves the right to institute criminal proceedings against the User; and
3.	Bongo Live shall not be liable, in any manner whatsoever, for any damage, loss or liability that resulted from the use of such content by the User or any third party who obtained any content from the User.</p>

<p>No person may use or attempt to use any technology or applications (including web crawlers, robots or web spiders) to search, collect or copy content from the Bongolive.co.tz website for any purpose whatsoever, without the prior written consent of Bongo Live.</p>

<p>E-mail addresses, names and telephone numbers published on the Bongolive.co.tz website may not be incorporated into any database, used for electronic marketing or similar purposes. No permission is given or should be implied that information on the Bongolive.co.tz website may be used to communicate unsolicited communications to Bongo Live.</p>

<h2>User Account and Password</h2>
<p>In order to open an account with Bongo Live, the user must complete the registration process by providing Bongo Live with current, complete and accurate information as prompted by the registration page. Inaccurate, false or misleading details will result in suspension. The User will also choose a password along with a username/mobile number. The User is entirely responsible for maintaining confidentiality with regard to its password and any account information. The User is responsible for all activity, which occurs in their account. Bongo Live will not be held liable for actions taken with respect to services offered and/or any third party claims.</p>

<p>The User agrees to notify Bongo Live immediately of any breach of security, or any unauthorized use of its account.</p>

<h2>Security</h2>
<p>Bongo Live shall take all reasonable steps to secure the content of the Bongolive.co.tz website and the information provided by and collected from Users from unauthorized access and/or disclosure. However, Bongo Live does not make any warranties or representations that content shall be 100% safe or secure.</p>

<p>Users may not deliver or attempt to deliver, whether on purpose or negligently, any damaging code, such as computer viruses, robots or spyware, to the Bongolive.co.tz website or the server and computer network that support the Bongolive.co.tz website.</p>

<p>Notwithstanding criminal prosecution, any person who delivers or attempts to deliver any damaging code to the Bongolive.co.tz website, whether on purpose or negligently, shall, without any limitation, indemnify and hold Bongo Live harmless against any and all liabilities, damages, risks and losses that Bongo Live and its partners/affiliates may suffer as a result of such delivery, attempt or damaging code.</p>

<p>Users may not develop, distribute or use any device or program designed to breach or overcome the security measures of the restricted pages, products and services on the Bongolive.co.tz website and Bongo Live reserves the right to claim damages from any and all persons involved, directly and indirectly, in the development, use and distribution of such devices or programs.</p>

<h2>User Privacy</h2>
<p>Bongo Live will not edit, monitor or disclose any personal information about the User or their account which includes its contents, without the User's prior permission unless such action is necessary to: (1) conform to legal requirements or comply with legal orders, (2) defend and protect the property or rights of Bongo Live, (3) enforce this Service Agreement or protect Bongo Live business or reputation, (4) respond to request for identification in connection with claim of trademark or copyright infringement by the User (5) or a claim by a third party that a User is using the service in any prohibited manner in the Member Conduct Code.</p>

<p>The User agrees that Bongo Live may access its account, including its contents for reasons above and to attend to technical issues and/or servicing.</p>

<p>The user grants Bongo Live the right to send communication via e-mail and/or text message regarding company notices, updates, upgrades or information Bongo Live deems important for its users to be notified about regarding the Bongo Live service.</p>

<h2>Advertising Guidelines for Users</h2>
<p>This section regulates the manner in which the User may advertise/promote any of their products, events and/or services via text messages through the Bongo Live network.</p>
<p>If the User advertises/promotes their products, event and/or services in a manner that violated the specified rule set out below, Bongo Live will be entitled to suspend and/or terminate a service and/or this agreement.</p>
<p>In regards to advertising/promoting products and/or services the user agrees to not (1) use the service in connection with junk SMS messages (spamming or any unsolicited messages), (2) create a false identity mobile phone address or header, or otherwise attempt to mislead others as to the identity of the sender or the origin of the message (3) display, provide, promote, sell or offer to sell products, content and/or service that is unlawful in the location at which the content is posted or received, material that exploits children, or otherwise exploits children under 18 years of age, pornography or illicitly pornographic sexual products, escort services, illegal goods, illegal drugs, illegal drug contraband, pirated computer programs, instructions on how to assemble or otherwise make bombs, grenades or other weapons.</p>
<p>The User is not allowed to use the Bongo Live logo, trademark, slogan or any other intellectual property belonging to Bongo Live in your advertising without the prior written consent of Bongo Live.</p>

<h2>Messages and Storage</h2>
<p>Bongo Live assumes no responsibility/liability for the failure to store information regarding users information, contact list and/or recently transmitted messages. Bongo Live reserves the right in the future, at its sole discretion, with prior notification to limit the amount of text messages available for access.</p>
<p>Bongo Live prides itself on the timely delivery of messages, yet messages may be delivered late due to network traffic or queuing, resulting in sometimes late messages.</p>

<h2>Member Conduct</h2>
<p>Any unauthorized commercial use of the Service, is expressly prohibited. The User agrees to abide by all applicable local, national and international laws and regulations and is solely responsible for all acts or omissions that occur under its account or password, including the content of any transmissions through the service. By way of example, and not as a limitation, the User agrees not to:</p>

<p>1.	Use the Service in connection with junk SMS messages, spamming or any unsolicited messages (commercial or otherwise).
2.	Harvest or otherwise collect information about others, including email addresses, without their consent;
3.	Create a false identity mobile phone address or header, or otherwise attempt to mislead others as to the identity of the sender or the origin of the message;
4.	Transmit material through the Service, associate with the Service or publishing with the Service unlawful, grossly offensive, libelous, defamatory, scandalous, threatening, harassing activity, blatant expressions of bigotry, prejudice, racism, hatred, excessive profanity, obscene, lewd, lascivious, filthy, excessively violent or otherwise objectionable content.
5.	Transmit any material that may infringe the intellectual property rights of authors, artists, photographers or other rights of third parties, including trademark, copyright without the express written consent of the content owner.
6.	Transmit any material that introduces viruses, Trojan horses, worms, time bombs; cancel bots, harmful code or deleterious programs on the Internet.
7.	Interfere with or disrupt networks connected to the service or violate the regulations, policies or procedures of such networks.
8.	Attempt to gain unauthorized access to the Service, other accounts, computer systems or networks connected to the service, through password mining or any other means.
9.	Interfere with another user's use and enjoyment of the Service or another entity's use and enjoyment of similar services.
10.	Engage in any other activity that Bongo Live believes could subject it to criminal liability or civil penalty or judgment.
11.	Display, provide, promote, sell or offer to sell the following products or content (or services related to the same): unlawful material/products in the location at which the content is posted or received, material that exploits children, or otherwise exploits children under 18 years of age, pornography or illicitly pornographic sexual products, escort services, illegal goods, illegal drugs, illegal drug contraband, pirated computer programs, instructions on how to assemble or otherwise make bombs, grenades or other weapons.
12.	Post or disclose any personally identifying information or private information about children without their consent (or their parents consent in case of a minor).
13.	Post any content that advocates promotes or otherwise encourages violence against any government, organization, group or individual.
14.	Provide instruction, information or assistance in causing or carrying out violence or harm.
</p>

<p>In addition, Bongo Live reserves the right to prohibit the use of Bongo Live by any company or site in its sole discretion.</p>

<h2>Code of Conduct</h2>
<p>The User warrants that it will comply with the following code of conduct regulated by the Mobile Marketing Association <a href="http://mmaglobal.com/codeofconduct.pdf"></a></p>

<h2>Fees</h2>
<p>Bongo Live reserves the right to change their pricing structure with notification to the User of such adjustments in writing, prior to such adjustments. Within this notification period the User may decide to continue with the service and the said increase, or terminate their use of Bongo Live services.</p>
	
	<h2>Termination, Cancellation or Suspension by Bongo Live</h2>
	
	<p>Bongo Live may terminate all or portions of offered services, disable your account or put your account on inactive status. This may happen anytime with or without cause, and with or without notice. Bongo Live shall have no responsibility or liability to you or any third party because of such action or termination.</p>
	
	<p>If your group account is inactive for over 120 days, Bongo Live has sole discretion and right to permanently remove your account, including contact lists recent messages and any other information stored by Bongo Live. Bongo Live will attempt to contact you via the email address you provided us, prior to taking any permanent actions in regards to removal.</p>
	
	<p>The User is free to terminate or cancel this Agreement at any time, and for any reason. Any questions concerning the appropriate method by which to cancel this Agreement, should be addressed to contact (at) bongolive.co.tz</p>
	
	<p>In the event that Bongo Live receives complaints from recipients or third parties with respect to the use of the Service, Bongo Live additionally reserves the right, in its sole discretion, to disclose any and all information to the recipient, applicable authorities or any other party with regard to its clients and application Users.</p>
	
	<h2>Disclaimers</h2>
	<p>Bongo Live PROVIDES THE SERVICES "AS IS" WITH NO WARRANTIES OF ANY KIND. Bongo Live EXPRESSLY DISCLAIMS ANY WARRANTY, EXPRESSED OR IMPLIED, REGARDING THE SERVICES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT, OR THAT THE SERVICES WILL BE SECURE, UNINTERRUPTED OR FREE OF ERRORS, VIRUSES OR OTHER HARMFUL COMPONENTS. WE DISCLAIM ALL RESPONSIBILITY FOR OUR USERS, GROUPS AND CHANNELS, AND THEIR POSTINGS, THEIR ACTS AND OMISSIONS.</p>
	<h2>Limitations of Liability</h2>
	<p>UNDER NO CIRCUMSTANCES WILL Bongo Live OR ITS EMPLOYEES, OFFICERS OR DIRECTORS BE LIABLE TO YOU FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL OR EXEMPLARY DAMAGES ARISING OUT OF OR IN CONNECTION WITH USE OF THE SERVICES WHETHER OR NOT Bongo Live HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
	
	<h2>Additional Limitations</h2>
	<p>Without limiting the generality of the terms set forth, Bongo Live and its affiliates, agents, content providers, service providers, and licensors:</p>
	<p>1.	HEREBY DISCLAIM ALL EXPRESS AND IMPLIED WARRANTIES AS TO THE ACCURACY, COMPLETENESS, NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR PARTICULAR PURPOSE OF Bongo Live GENERALLY, AND ANY CONTENT OR SERVICES CONTAINED THEREIN, AS WELL AS ALL EXPRESS AND IMPLIED WARRANTIES THAT THE OPER ATION OF Bongo Live GENERALLY AND ANY CONTENT OR SERVICES CONTAINED THEREIN WILL BE UNINTERRUPTED OR ERROR-FREE;</p><p>
2.	SHALL IN NO EVENT BE LIABLE TO The USER OR ANYONE ELSE FOR ANY INACCURACY, ERROR OR OMISSION IN, OR LOSS, INJURY OR DAMAGE CAUSED IN WHOLE OR IN PART BY FAILURES, DELAYS OR INTERRUPTIONS OF THE SERVICE GENERALLY OR IN ANY ASPECT.
</p>

<h2>Indemnification</h2>
<p>The User agrees to indemnify, defend and hold harmless Bongo Live and its employees, affiliates, officers, agents, content providers and service providers, against any and all claims, liabilities, damages, costs and expenses (including, but not limited to, consequential damages, incidental damages, special damages, attorneys' fees and disbursements) arising from or relating to (1) the use of the Service in any manner which violates the terms of this Service Agreement 2) any violation or failure by you to comply with all laws and regulations in connection with the Services (4) any content that you or anyone using your account may submit, post or transmit to the web site (3) any claims made by third parties arising from your use of the Service, including without limitation any and regulation all third party claims arising from or related to any failure, delay or interruption to the Service. The User agrees to cooperate as fully as reasonably required in the defense of any claim. Bongo Live reserves the right, at its own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by the User.</p>

<h2>Modifications to Terms and Conditions</h2>
<p>Bongo Live frequently updates, modifies, and otherwise continually seeks to improve the Bongo Live services. Bongo Live shall have the right to change, modify and/or update the terms of this Agreement and to change or discontinue any aspect or feature of the Bongo Live service, in either case, as it deems reasonably necessary. The amended Terms and Conditions shall automatically be effective five days after they are initially posted on the web site. Your continued use of the Services after the effective date of any posted change constitutes your acceptance of the amended Terms and Conditions as modified by the posted changes. For this reason, we encourage you to review these Terms and Conditions whenever you use the Services.</p>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>