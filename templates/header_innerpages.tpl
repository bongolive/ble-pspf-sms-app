<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>
	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" />
<title>{$title}</title>
<link href="{$BASE_URL_HTTP_ASSETS}css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/fader.css" type="text/css" media="screen" />	

<script src="{$BASE_URL_HTTP_ASSETS}js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="{$BASE_URL_HTTP_ASSETS}js/jquery.anythingfader.js" type="text/javascript"></script>


<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions.js" type="text/javascript"></script>
<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/timepicker.js"></script>
</head>

<body>
{include file="langs.tpl"}
<div id="container-top">
  <div class="logo"><a href="#"><img alt="Bongo Live logo" src="images/logo.png" /></a></div>
  <div class="nav-sign-in">
    <ul>
      <li id="user-id">John Doe</li>
      <li><a href="pricing.html">Help</a></li>
      <li><a href="how_it_works.html">Log out</a></li>
    </ul>
  </div>
  <div id="main-info">
    <table width="100%" border="0" cellspacing="0">
      <tr>
        <td class="main-info-title01">SMS Balance:</td>
        <td class="main-info-sms-balance">259</td>
        <td class="main-info-contacts">Contacts</td>
      </tr>
      <tr>
        <td class="main-info-title02">Statistics:</td>
        <td class="main-info-statistics" colspan="2">place some stats here or other useful info</td>
      </tr>
    </table>
  </div>
  <div class="clear"></div>
  <p id="user-role">Broadcaster</p>
  <div class="topmenu">
    <ul class="primary">
      <li class="active-topmenu  item-one-a current"><a href="#">Dashboard</a>
        <ul class="secondary">
          <li><a href="#" class="current-sec">item One</a></li>
          <li><a href="#">item Two</a></li>
          <li><a href="#">item Three</a></li>
        </ul>
      </li>
      <li class="item-two"><a href="inpage_01-b.html">Broadcast SMS</a>
        <ul class="secondary">
          <li><a href="#">Upload Contacts</a></li>
          <li><a href="#">Add Contact</a></li>
          <li><a href="#">Create Group</a></li>
          <li><a href="#">Manage Group</a></li>
          <li><a href="#">Manage Sender IDs</a></li>
          <li><a href="#">Compose SMS</a></li>
        </ul>
      </li>
      <li class="item-three"><a href="inpage_01-c.html">Purchase SMS</a>
        <ul class="secondary">
          <li><a href="#">item One</a></li>
          <li><a href="#">item Two</a></li>
          <li><a href="#">item Three</a></li>
        </ul>
      </li>
      <li class="item-four"><a href="inpage_01-d.html">Help</a>
        <ul class="secondary">
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </li>
      <li class="item-five"><a href="inpage_01-e.html">Change Password</a>
        <ul class="secondary">
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="clear"></div>
</div>