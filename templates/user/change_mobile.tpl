<div style="height:250px;">

<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>

<form name="chngMobile" method="POST" action="" onsubmit="javascript: validateChngMobile(); return false;">
{if $step=='1'}
	<table cellpadding="0" cellspacing="10">
		<tr>
			<td>{$smarty.const.ENTER_NEW_MOBILE_NUMBER}:
				<br/>
				<span class="mobexmp">(Ex. 784845785)</span>
			</td>
		  <td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" value="{$mob_no}" maxlength="{$mobMaxLen}" onkeyup="javascript: loginMobValidation(this.value);"></span><span id="mobDiv"  class="form_error"></span></td>
			
		<tr>
			<td>&nbsp;</td>
			<td>
				<input type="submit" name="save" id="save" value="{$smarty.const.SAVE}" class="editbtn">
				<input type="hidden" name="action" id="action" value="chMob">
			</td>
			
		</tr>
	</table>
{elseif $step=='2'}
	<table cellpadding="0" cellspacing="10">
		<tr>
			<td valign="top">{$smarty.const.ENTER_MOBILE_VERIFICATION_CODE}</td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="mob_conf" id="mob_conf" value="{$mob_conf}">
			</span> <span id="mobConfDiv"  class="form_error"></span></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><input type="submit" name="save" id="save" value="{$smarty.const.SAVE}" class="editbtn">
				<input type="hidden" name="action" id="action" value="confMob"></td>			
		</tr>
	</table>

{/if}
</form>
</div>
{literal}
    
    <script type="text/javascript">
	//<!--
            
           function  validateChngMobile(){
		var action = $("#action").val();
			
		var error =0; 	
		if(action == 'chMob'){
		
			if(!mobileValidation()){
				error = 1;
			}
		
		}else if(action == 'confMob'){
			if($("#mob_conf").val()==''){
			
				$("#mobConfDiv").html("{/literal}{$smarty.const.ENTER_MOBILE_CONFIRMATION_CODE}{literal}");
				error = 1;

			}
		
		}

		if(error==0){
		
			document.chngMobile.submit();
		}
	   
	   }


        //-->
     </script>


{/literal}