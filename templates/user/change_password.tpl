<form name="changePass" method="POST" action="" onsubmit="javascript: changePassValidation(); return false;">
	<div style="height:250px;">
	<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg" {/if}
{if $msg} style="display:block;" {else} style="display:none;" {/if}>{$msg}</div>
	<table cellpadding="0" cellspacing="10">
		<tr>
			<td colspan="3">
			</td>
		</tr>
		<tr>
			<td>{$smarty.const.OLD_PASSWORD}: </td>
			<td><span class="form_dinatrea standard-input"><input type="password" name="old_password" id="old_password" value=""></span>  <span id="old_passwordDiv" class="form_error"></span></td>
		</tr>
		<tr>
			<td>{$smarty.const.NEW_PASSWORD}: </td>
			<td><span class="form_dinatrea standard-input"><input type="password" name="password" id="password" onkeyup="passValidation();"></span> <span id="passwordDiv" class="form_error"></span></td>
		</tr>

		<tr>
			<td>{$smarty.const.CONFIRM_PASSWORD}: </td>
			<td><span class="form_dinatrea standard-input"><input type="password" name="conf_password" id="conf_password" onkeyup="confPassValidation();"></span> <span id="cpasswordDiv" class="form_error"></span></td>
		</tr>
		<tr>
		
			<td>&nbsp;</td>
			<td><input type="submit" name="submit" id="submit" value="{$smarty.const.SAVE}" class="editbtn">
				<input type="hidden" name="action" id="action" value="changpass"></td>
		</tr>
	</table>
	</div>
</form>
{literal}
    
    <script type="text/javascript">
	//<!--
	     function changePassValidation(){
			
			var oldPass= true;
			if($("#old_password").val()==""){
			
				oldPass = false;
				$("#old_passwordDiv").html("Enter you old password!");
			}
			var errorPass = passValidation();
			var errorConfPass = confPassValidation();
		
			if(errorPass && errorConfPass && oldPass){
		
				document.changePass.submit();
		
			}else{
				return false;
			}

		 
		 }
    //-->
    </script>


{/literal}