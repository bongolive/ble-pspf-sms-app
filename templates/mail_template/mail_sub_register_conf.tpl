<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="{$base_url}index.php"><img alt="Bongo Live" src="{$BASE_URL_HTTP_ASSETS}images/logo.png" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:10px 0px 10px 0px;">Subscriber Registration</h1>
		<br />

		<p>Thank you for registering with Bongo Live! Bringing you the next level of mobile services in Tanzania.</p>
		<p>You now have access to your choice of the best deals around you directly on your phone.</p>
		<p>Your login details:</p>
		<p>{$field1} : {$field1_val}</p>
		<p>{$field2} : {$field2_val}</p>
		<p>If you forget your password it can be retrieved on the <a href="{$base_url}index.php">home page</a></p>
		{if $activationLink===true}
		<p>Before you can sign into your account you must activate your account by clicking on the link below. If the link is not active please copy and paste the link into your browser&#39;s address bar. </p>
		<p>{$activateUrl}</p>
		{/if}
		<p>Once registered you will be able to sign into your account, manage your profile and change your mobile number. You can sign in on the <a href="{$base_url}index.php">home page</a></p>

		<p>If you have any questions you can browse through the frequently asked questions here <a href="faq.html">FAQ</a> or you can get in touch with us using the <a href="{$base_url}contactus.php">Contact Us</a> page.</p>

		<p> PSPF. <br />
		  PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street <br />
		  Dar es Salaam, Tanzania<br />
		  Tel: +255 222120912
		  .<br />
		  E-Mail: pspf@pspf-tz.org </p><br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;"></div>

</body>
</html>
