<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="{$base_url}index.php"><img alt="{$resellerDetails.name}" src="{$resellerDetails.logo}" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:10px 0px 10px 0px;">Password Reset</h1>
		<br />

		<p>The password for your account has been changed successfully.</p>
		
		<p>Your new password : <strong>{$newPassword}</strong></p>

		{if $parentVenId==''}
        <p>If you did not change your password or if have any other questions, you can get in touch with us using the  <a href="{$base_url}contactus.php">Contact Us</a> page.</p>
		<p> PSPF. <br />
		  PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street <br />
		  Dar es Salaam, Tanzania<br />
		  Tel: +255 222120912
		  .<br />
		  E-Mail: pspf@pspf-tz.org </p>
		{else}
		<p> {$resellerDetails.organisation} <br />
		  {$resellerDetails.physical_address}<br />
		  Tel: {$resellerDetails.support_phone}
		  .<br />
		  Mob: {$resellerDetails.mob_no}<br />
		  Email: {$resellerDetails.support_email}
		<p> 
		{/if}<br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;"></div>

</body>
</html>
