<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->

<div id="container">
	<h1>Site languages</h1>
	<div style="width:700px; margin:0 auto;">
		<div id="errorDiv" style="color:#FF0000">{$msg}</div>
		<table align="center" cellpadding="0" cellspacing="10" style="background-color:#fff;">
			<tr>
				<td valign="top" align="left">
					{if count($languages)>0}
						<form name="admLangEdit" id="admLangEdit" method="POST" action="adm_langs.php" onsubmit="langSaveValidation(); return false;">
							<table cellspacing="10" width="100%">
								<tr>
									<td width="121">Id</td>
									<td width="802">
										<span class="form_dinatrea standard-input">
											<input type="text" name="lang_id" id="lang_id" value="{$Lang}" size="10"></span>
										<span id="langId"  class="form_error"></span>
									</td>
								</tr>
								<tr>
									<td>Title</td>
									<td>
										<span class="form_dinatrea standard-input">
											<input type="text" name="lang_title" id="lang_title" value="{$Title}" size="10">
										</span>
										<span id="langTitle" class="form_error"></span>
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<input type="submit" name="add" id="add" value="Add" class="editbtn" onclick="javascript:addLanguage();">
										<input type="submit" name="save" id="save" value="Save" class="editbtn">
										<input type="hidden" name="action" id="action" value="{$action}">
										<input type="hidden" name="lang_id_to_edit" id="lang_id_to_edit" value="{$lang_id_to_edit}">
									</td>
								</tr>
							</table>
						</form>

						<table align="center" width="350" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
							<tr>
								<td width="75" class="content-link_green" align="left">Id</td>
								<td width="175" class="content-link_green" align="left">Title</td>
								<td width="50" class="content-link_green" align="center">Edit</td>
								<td width="50" class="content-link_green" align="center">Delete</td>
							</tr>
							<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
							{foreach from=$languages item=lng name=languages}
								<tr>
									<td class="content-link_Black" align="left">{$lng@key}</td>
									<td align="left">{$lng}</td>
									<td align="center"><a href="{$selfUrl}?action=edit&lang={$lng@key}&title={$lng}" title="Edit"><img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
									<td align="center">{if $lng@key!='en_us'}<a href="{$selfUrl}?action=delete&lang={$lng@key}" title="Delete" onClick="return window.confirm('Are you sure you want to delete a language and all language content from the site?')"><img alt="Delete" src="{$BASE_URL_HTTP_ASSETS}images/error_msg_icon.png" /></a>{/if}</td>
								</tr>
								{if $smarty.foreach.languages.index < $smarty.foreach.languages.total-1}
								<tr class="TR_spc"><td colspan="4" class="TD_spc"></td></tr>
								{/if}
							{/foreach}
						</table>
					{else}
						<div id="errorDiv" style="color:#FF0000">There are no languages in the table "languages".<br />Default (basic) language is 'en_us'. It must exist in the table!</div>
					{/if}
				</td>
			</tr>
		</table>
	</div>
</div>

{literal}
	<script type="text/javascript">
	//<!--
		function langSaveValidation()
		{
			var error = 0;
			
			if($("#lang_id").val()=='')
			{
				$("#langId").html('Input Language Id');
				error = 1;
			}
			else
			{
				$("#langId").html('');
			}
			
			if($("#lang_title").val()=='')
			{
				$("#langTitle").html('Input Language Title');
				error = 1;
			}
			else
			{
				$("#langTitle").html('');
			}

			if(error !=1)
			{
				document.admLangEdit.submit();
			}
		}

		function addLanguage()
		{
			document.admLangEdit.action.value='add';
			
			langSaveValidation();
		}
	//-->
	</script>
{/literal}