<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->

<div id="container">
    <h1>Manager Import Subscribers </h1>
	<div id='load' >
	      <form action="" method="post" enctype="multipart/form-data">
	      <div id='file_inp'><input type="file" name="filename"></div>
	      <div id='file_sub'><input type="submit" value="Upload" class="green_btnbg"></div>
	      </form>
	</div>
	<div>
		{if ($error_size)} {$error_size} {/if}
		{if ($error_upload)} {$error_upload} {/if}
		{if ($file==true)}
			{if ($y)}<h3>Uploaded successfully: {$y} </h3> {/if}
			{if ($x)}<h3>Uploaded failed: {$x}</h3>{/if}
			{if ($z)}<h3>Uploaded dublicated: {$z} </h3>{/if}
			{if ($valid && $name)}<h3>{$name} - {$valid} </h3>{/if}
		{/if}
	</div>
	<div>	
		{if ($error_str)}
			{foreach from=$error_str key=key_str item=val}
				Errors in  {$val}  string!&nbsp;
				{foreach from=$errors.$key_str key=key item=item} 
					{if ($item==true)}
						Invalid {$key}!&nbsp;
					{/if}
				{/foreach}<br>
			{/foreach}
		{/if} 
	</div>
</div>

