<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!-- admin header end -->
<div id="container">
    <h1>Manage SMS Packages</h1>
<div style="width:700px; margin:0 auto;">	
	<div id="errorDiv" style="color:#FF0000">{$msg}</div>
<form name="admCreditShceme" method="POST" action="" onsubmit="creditShcemeValidation(); return false;">
<table cellspacing="10" width="100%">
	<tr>
		<td width="121">Start range</td>
		<td width="802"><span class="form_dinatrea standard-input">
		<input type="text" name="startCredit" id="startCredit" value="{$startCredit}" size="10"></span>
		<span id="startCreditDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td>End range</td>
		<td><span class="form_dinatrea standard-input">
		<input type="text" name="endCredit" id="endCredit" value="{$endCredit}" size="10"></span>
		<span id="endCreditDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td>Rate/SMS</td>
		<td><span class="form_dinatrea standard-input"><input type="text" name="creditRate" id="creditRate" value="{$creditRate}" size="5"></span>
		<span id="creditRateDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="save" id="save" value="Save" class="editbtn" >
			<input type="hidden" name="action" id="action" value="{$action}" >
		</td>
	</tr>
</table>
</form>
<br /><br />
 <table align="center" width="100%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
	<tr>
		<td width="20%" class="content-link_green">SMS Range</td>
		<td width="20%" class="content-link_green"></td>
		<td width="20%" class="content-link_green">Rate</td>
		<td width="20%"class="content-link_green"></td>
		<td width="20%" class="content-link_green">Edit</td>
		
	</tr>
	{section name=smsScheme loop=$arrCreditSchemes}
	<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td align="right">{$arrCreditSchemes[smsScheme].start_range} - {$arrCreditSchemes[smsScheme].end_range}</td>
			<td width="20%" class="content-link_green"></td>
			<td>{$arrCreditSchemes[smsScheme].rate}</td>
			<td width="20%" class="content-link_green"></td>
			<td><a href="{$selfUrl}?action=edit&id={$arrCreditSchemes[smsScheme].id}" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
			
		</tr>
	
	{/section}
</table>
</div></div>
{literal}
    
    <script type="text/javascript">
	//<!--
		function creditShcemeValidation(){
			
			var error=0;
			if($("#startCredit").val()==''){
			
				$("#startCreditDiv").html('Input credit start range');
				error=1;	
			}else if(!numberValidation($("#startCredit").val())){
				$("#startCreditDiv").html('Input only numbers');
				error=1;
			
			}else{
				$("#startCreditDiv").html('');
			}
			if($("#endCredit").val()==''){
			
				$("#endCreditDiv").html('Input credit end range');
				error=1;	
			}else if(!numberValidation($("#endCredit").val())){
				$("#endCreditDiv").html('Input only numbers');
				error=1;
			
			}else{
				$("#endCreditDiv").html('');
			}

			if($("#creditRate").val()==''){
			
				$("#creditRateDiv").html('Input credit end range');
				error=1;	
			}else if(!numberValidation($("#creditRate").val())){
				$("#creditRateDiv").html('Input only numbers');
				error=1;
			
			}else{
				$("#creditRateDiv").html('');
			}
			
			if(error !=1){
				document.admCreditShceme.submit();
			}
		
		
		}
		
		
        //-->
     </script>


{/literal}

