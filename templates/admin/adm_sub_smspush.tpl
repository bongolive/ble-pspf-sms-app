{include file="admin/adm_header.tpl"}
<div id="container">
    <h1>Admin Broadcast</h1>
	<div id="errorDiv" {if $errorFlag=='1'} class="error_msg" {else} class="sucess_msg_2" {/if}
	{if $msg} style="display:block;" {else} style="display:none;" {/if}> {$msg} </div>
	<div style="width:700px; margin:0 auto;">
<form name="admSubSmspus" action="" method="POST" onsubmit="admSubSmspus1(); return false;">
<table cellspacing="10" cellpadding="0" align="center" width="100%">
	
	<tr>
		<td class="content-link_green" align="left">Age Ranges</td>
		<td class="content-link_green" align="left">Gender</td>
	</tr>
	<tr>
		<td valign="top">
			<div id="BodyContent" class='flexcroll' style="height:200px; width:310px;">

			<table id="tableAgeRange" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">				
				<tr>
					<td><input type="checkbox" name="allAgeRange" id="allAgeRange" value="" onclick="checkAll('allAgeRange','tableAgeRange');"></td>
					<td>All</td>
				</tr>
				{section name=ageRange loop=$arrAgeRange}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>
						<input type="checkbox" name="ageRange[]" id="ageRange" value="{$arrAgeRange[ageRange].id}" 
						{if is_array($ageRange)}
							{if in_array($arrAgeRange[ageRange].id,$ageRange)}
							checked
							{/if}
						{/if}>
						</td>
					<td>{$arrAgeRange[ageRange].age_range}</td>
				</tr>
				{/section}
			</table>
			</div>
		</td>

		<td valign="top">
           <div class='flexcroll' style="height:200px; width:310px;">
			<table cellpadding="0" width="97%" cellspacing="5" style="background-color:#f8f8f8;">
				
				<tr>
					<td><input type="checkbox" name="gender[]" id="gender" value="M"
					{if is_array($gender)}
							{if in_array('M',$gender)}
							checked
							{/if}
					{/if}
					>	
					</td>
					<td>Male</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="gender[]" id="gender" value="F" 
					{if is_array($gender)}
							{if in_array('F',$gender)}
							checked
							{/if}
					{/if}
					>	
					</td>
					<td>Female</td>
				</tr>				
			</table>
           </div>
		</td>
	</tr>
	<tr>
		<td class="content-link_green" align="left">Education</td>
		<td class="content-link_green" align="left">Employment Status</td>
	</tr>
	<tr>
		<td valign="top">
		<div id="BodyContent" class='flexcroll' style="height:200px; width:310px;">

			<table id="tableEducation" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
				
				<tr>
					<td><input type="checkbox" name="allEducation" id="allEducation" value="" onclick="checkAll('allEducation','tableEducation');"></td>
					<td>All</td>
				</tr>
				{section name=education loop=$arrEducation}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="education[]" id="education" value="{$arrEducation[education].id}" 
					{if is_array($education)}
							{if in_array($arrEducation[education].id,$education)}
							checked
							{/if}
					{/if}
					></td>
					<td>{$arrEducation[education].education_level}</td>
				</tr>
				{/section}
			</table>
			</div>
		</td>

		<td valign="top">
			<div class='flexcroll' style="height:200px; width:310px;">

			<table id="tableEmployment" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
			
				<tr>
					<td><input type="checkbox" name="allEmployment" id="allEmployment" value="" onclick="checkAll('allEmployment','tableEmployment');"></td>
					<td>All</td>
				</tr>
				{section name=employment loop=$arrEmployment}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="employment[]" id="employment" value="{$arrEmployment[employment].id}" 
					{if is_array($employment)}
						{if in_array($arrEmployment[employment].id,$employment)}
							checked
							{/if}
					{/if}
					></td><td>{$arrEmployment[employment].employment}  </td>
				</tr>
				{/section}
			</table>
			</div>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="content-link_green" align="left">Locations</td>
		<td class="content-link_green" align="left">Occupation</td>
	</tr>
	<tr>
		<td valign="top">
		<div id="BodyContent" class='flexcroll' style="height:200px; width:310px;">

			<table id="tableLocation" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
				
				<tr>
					<td><input type="checkbox" name="allLocation" id="allLocation" value="" onclick="checkAll('allLocation','tableLocation');"></td>
					<td>All</td>
				</tr>
				{section name=location loop=$arrLocation}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="location[]" id="location" value="{$arrLocation[location].id}" 
					{if is_array($location)}
							{if in_array($arrLocation[location].id,$location)}
							checked
							{/if}
					{/if}
					></td>
					<td>{$arrLocation[location].location}</td>
				</tr>
				{/section}
			</table>
			</div>
		</td>

		<td valign="top">
			<div class='flexcroll' style="height:200px; width:310px;">

			<table id="tableOccupation" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
			
				<tr>
					<td><input type="checkbox" name="allOccupation" id="allOccupation" value="" onclick="checkAll('allOccupation','tableOccupation');"></td>
					<td>All</td>
				</tr>
				{section name=occupation loop=$arrOccupation}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="occupation[]" id="occupation" value="{$arrOccupation[occupation].id}" 
					{if is_array($occupation)}
						{if in_array($arrOccupation[occupation].id,$occupation)}
							checked
							{/if}
					{/if}
					></td><td>{$arrOccupation[occupation].occupation}  </td>
				</tr>
				{/section}
			</table>
			</div>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="content-link_green" align="left">Interest</td>
		<td class="content-link_green" align="left"></td>
	</tr>

	<tr>
		<td valign="top">
		<div id="BodyContent" class='flexcroll' style="height:200px; width:310px;">

			<table id="tableCategory" width="97%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
				
				<tr>
					<td><input type="checkbox" name="allCategory" id="allCategory" value="" onclick="checkAll('allCategory','tableCategory');"></td>
					<td>All</td>
				</tr>
				{section name=category loop=$arrCategory}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="category[]" id="category" value="{$arrCategory[category].id}" 
					{if is_array($category)}
						{if in_array($arrCategory[category].id,$category)}
							checked
							{/if}
					{/if}
					
					></td>
					<td>{$arrCategory[category].category}</td>
				</tr>
				{/section}
			</table>
			</div>
		</td>
		<td valign="top">
		<!--
			<table id="tableIndustry" width="300" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
				<tr>
					<td colspan="2" class="content-link_green">Industry</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="allIndustry" id="allIndustry" value="" onclick="checkAll('allIndustry','tableIndustry');"></td>
					<td>All</td>
				</tr>
				{section name=industry loop=$arrIndustry}
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="industry[]" id="industry" value="{$arrIndustry[industry].id}"   
					{if is_array($industry)}
						{if in_array($arrIndustry[industry].id,$industry)}
							checked
							{/if}
					{/if}
					
					> </td>
					<td>{$arrIndustry[industry].industry}</td>
				</tr>
				{/section}
			</table>
		-->
		</td>
		
	</tr>
</table>


<br><br>
<table cellspacing="10">
	<tr>
		<td valign="top">SMS Massage</td>
		<td><span class="form_dinatrea standard-input" style="width:250px;">
			<textarea name="textMessage" id="textMessage" onkeyup="checkMessageLength();" cols="40" rows="3" >{$textMessage}</textarea></span>
		</td>
		<td></td>
	</tr>
	<tr>
		<td>Counter</td>
		<td><span id="charCount">{if !$charCount}0{else}{$charCount}{/if}</span>/160</td>
		<td><div id="messageCount"></div></td>
	</tr>
	<tr>
		<td>Sender ID</td>
		<td><span class="form_dinatrea standard-input">
			<select name="senderid" id="senderid" >
			<option value="">Select</option>
			{section name=senderid loop=$arrSenderid}
			<option value="{$arrSenderid[senderid].id}" 
			{if $arrSenderid[senderid].id==$senderid}selected{/if}
			>{$arrSenderid[senderid].senderid}</option>

			{/section}
			</select></span>
		</td>
		<td></td>
	</tr>
	<tr>
		{if isset($scheduleDate) && $scheduleDate!=''}	
		<td id="advOptionTd" onclick="javascript: showHideAdvOption();">- Advanced Option</td>
		{else}
		<td id="advOptionTd" onclick="javascript: showHideAdvOption();">+ Advanced Option</td>	
		{/if}
		<td></td>
		
	</tr>
	
	<tr>
		<td colspan="5">
			{if isset($scheduleDate) && $scheduleDate!=''}	
				<div id="advOptionDiv" style="display:block">

			{else}
				<div id="advOptionDiv" style="display:none">

			{/if}
				<table>
					
					<tr>
						<td width="39%">Schedule date</td>
						<td><span class="form_dinatrea standard-input"><input type="text" name="scheduleDate" id="scheduleDate"  value="{$scheduleDate}"
						{if !isset($scheduleDate) || $scheduleDate==''}	
							disabled
						{/if}
						
						></span></td>
						<td></td>
					</tr>
					
				</table>
			
			</div>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="button" name="back" id="back" value="Back" onclick="javascript: backTo(1);" class="editbtn">
			<input type="submit" name="send" id="send" value="Send" class="editbtn">
			<input type="hidden" name="action" id="action" value="smspush" >
			<input type="hidden" name="receiverType" id="receiverType" value="{$receiverType}" >
		</td>
		<td></td>
	</tr>
	

</table>
</form>
</div>		
</div>
{literal}
    
    <script type="text/javascript">
	//<!--
            
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		function backTo(val){
			
			$("#action").val('backTo'+val);
			document.admSubSmspus.submit();	
		}

		function admSubSmspus1(){
		
			if($("#textMessage").val()==''){
				alert('Inter text message');
				return false;
			}else if($("#senderid").val()==''){
				alert('Select senderId');
				errorFlag = 1;
				return false;
			}else{
				document.admSubSmspus.submit()
			}
		
		}
        //-->
     </script>


{/literal}