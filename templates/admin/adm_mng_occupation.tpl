<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!-- admin header end -->
<div id="container">
    <h1>Manage Occupation</h1>
<div style="width:700px; margin:0 auto;">	
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg"{else} class="sucess_msg"{/if}
{if $msg} style="display:block;" {else} style="display:none;"{/if}>{$msg}</div>

<form name="orgCatForm" method="POST" action="{$selfUrl}" onsubmit="orgCatValidation(); return false;">
<table cellspacing="10" width="100%">
	<tr>
		<td width="200">Occupation</td>
		<td width="802"><span class="form_dinatrea standard-input">
		<input type="text" name="occupation" id="occupation" value="{$occupation}" size="100"></span>
		<span id="OccupationDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="save" id="save" value="Save" class="editbtn" >
			<input type="hidden" name="action" id="action" value="{$action}" >
			<input type="hidden" name="id" id="id" value="{$id}" >
		</td>
	</tr>
</table>
</form>
<br /><br />
 <table align="center" width="100%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
	<tr>
		<td width="{if $smarty.session.lang!='en_us'}45%{else}90%{/if}" class="content-link_green" align="center">Occupations</td>
		{if $smarty.session.lang!='en_us'}
		<td width="45%" class="content-link_green" align="right">English</td>
		{/if}
		<td width="10%" class="content-link_green" align="center">Edit</td>
	</tr>
	{section name=result loop=$arrOccupation}
	<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td align="left">{$arrOccupation[result].occupation}</td>
			{if $smarty.session.lang!='en_us'}
			<td align="right" style="color:#C0C0C0;">{$arrOccupation[result].occupation_en_us}</td>
			{/if}	
			<td align="center"><a href="{$selfUrl}?action=edit&id={$arrOccupation[result].id}" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
		</tr>
	
	{/section}
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="{if $smarty.session.lang!='en_us'}3{else}2{/if}" class="content-link" style="padding:10px;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
</table>
</div></div>
{literal}
    
    <script type="text/javascript">
	//<!--
		function orgCatValidation(){
			
			var error=0;
			if($("#Occupation").val()==''){
			
				$("#OccupationDiv").html('Enter Occupation name');
				error=1;	
			}else {
				$("#OccupationDiv").html('');
			
			}
			if(error !=1){
				document.orgCatForm.submit();
			}
		
		
		}
		
		
        //-->
     </script>


{/literal}

