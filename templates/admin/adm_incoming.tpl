<!--admin header start-->
	{include file="admin/adm_header.tpl"}
<!--admin header end-->
<div id="container">
    <h1>Manage Incoming SMS</h1>
	<div style="width:850px; margin:0 auto;">
	<div id="errorDiv"></div>
	{if $action=='' || $action=='search'}
	<form name="creditRequest" action="{$selfUrl}" method="GET">
		<table cellspacing="10" width="600">	
<!--		<tr>
				<td>Broadcaster Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="broadcaster_type" id="broadcaster_type" onchange="javascript: selectBroadcatedrType(this.value);">
						<option value="" >Select</option>
						<option value="1" {if $broadcaster_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $broadcaster_type=='2'}selected{/if}>Organisation</option>
						<option value="3"  {if $broadcaster_type=='3'}selected{/if}>Admin</option>
					</select>
				</span>
				</td>
			</tr>-->
			<tr>
				<td>Broadcaster name:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>				
			</tr>
			
			<tr>
				<td>Broadcaster Phone:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="mob_no" id="mob_no" value="{$mob_no}"></span>
				</td>
			</tr>
			
	<!--		<tr>
				<td>Broadcast Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="sms_type" id="sms_type" >
						<option value="" >Select</option>
						<option value="1" {if $sms_type=='1'}selected{/if} >All</option>
						<option value="2"  {if $sms_type=='2'}selected{/if}>Pushed</option>
						<option value="3"  {if $sms_type=='3'}selected{/if}>Scheduled</option>
					</select>
				</span>
				</td>
			</tr> -->
			<tr>
				<td valign="top">SMS Message:</td>
				<td><span class="form_dinatrea standard-input" style="width:250px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="3">{$textMessage}</textarea></span>
				</td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="search">
					<input type="hidden" name="ajaxUrl" id="ajaxUrl" value="{$ajaxUrl}">
				</td>
			</tr>
		</table>
	</form>	
	
	<table cellpadding="5" width="100%" cellspacing="2" style="background-color:#ffffff;">
	<tr  style="background-color:#f8f8f8;font-weight:bold;font-size:12px;">		
		<td style="padding:10px;"><b>Broadcaster</b></td>
		<!--<td><b>Broadcaster Type</b></td>-->
		<td><b>Mobile</b></td>
        <td><b>Date</b></td>
		<td><b>SMS Message</b></td>
        <td><b>Response</b></td>
	<!--<td><b>Sms Cout</b></td>
		
		<td>&nbsp;</td>
		<td>&nbsp;</td>-->
	</tr>
	{if $arrSms}
		{section name=sms loop=$arrSms}	
			<tr style="font-size:12px;background-color:#f8f8f8;">	
				<td style="padding:10px;">{$arrSms[sms].username}</td>
	<!--			<td style="padding:10px;">
				{if $arrSms[sms].sen_type=='0'}
					Admin
				{elseif $arrSms[sms].sen_type=='1'}
					Individual
				{elseif $arrSms[sms].sen_type=='2'}
					Organisation
				{/if}
				</td>-->
				<!--
				<td>{$arrSms[sms].credit_requested}</td>
				-->
				<td style="padding:10px;">{$arrSms[sms].phone|wordwrap:40:"<br />\n"}</td>
                <td style="padding:10px;">{$arrSms[sms].received}</td>
				<td style="padding:10px;">{$arrSms[sms].message|wordwrap:40:"<br />\n"}</td>
				<td style="padding:10px;">{$arrSms[sms].outbound_response|wordwrap:40:"<br />\n"}</td>
				<!--
				<td>{$arrSms[sms].status}</td>
				
				<td>{$arrSms[sms].credit}</td>
				-->

				
<!--				<td class="content-link" style="padding:0 10px">
					{if $arrSms[sms].is_shceduled=='1' && $arrSms[sms].is_proccessed=='0'}
					<a href="?sid={$arrSms[sms].id}&action=edit"><img src="{$BASE_URL_HTTP_ASSETS}images/watch.png"></a>
					{/if}
				</td>
				<td class="content-link" style="padding:0 10px">
					{if $arrSms[sms].is_shceduled=='1' && $arrSms[sms].is_proccessed=='0'}
					<a href="javascript: void(0);" onclick="javascript: if(confirm('Are you sure to cancle this scheduled broadcast'))cancelSms('{$arrSms[sms].id}','{$ajaxUrl}');"><img src="{$BASE_URL_HTTP_ASSETS}images/cancel.png"></a>
					{/if}
				</td>-->
			</tr>
		{/section}
	
		<tr style="background-color:#f8f8f8;">
			<td align="right" colspan="10" class="content-link" style="padding:10px 0;">
			{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}
			</td>
		</tr>
	{else}
		<tr class="TR_spc"><td colspan="10" class="TD_spc"></td></tr>
		<tr style="background-color:#f8f8f8;">
			<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
			No Rocords found
			</td>
		</tr>		

	{/if}
	</table>
<!--{elseif $action=='edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST">
	<table cellspacing="10" width="600">	
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
			<td valign="top">SMS Message:</td>
			<td><span class="form_dinatrea standard-input" style="width:250px;">
				<textarea name="textMessage" id="textMessage" cols="40" readonly rows="3">{$arrSmsToEdit.text_message}</textarea></span>
			</td>
		</tr>
		<tr>
				<td>Schedule date</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="scheduleDate" id="scheduleDate" value="{$arrSmsToEdit.send_time}"></span>
				</td>
		</tr>
		<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Save" class="editbtn">
					<input type="hidden" name="action" id="action" value="update">
					<input type="hidden" name="sid" id="sid" value="{$arrSmsToEdit.id}">
				</td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		<tr>
				<td>&nbsp;</td>
				<td></td>
		</tr>
		
	</table>
	</form>
{/if}-->

</div>

</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});

		function selectBroadcatedrType(brdType){
			
			if(brdType==3){
				
				$("#vendorName").val("");
				$("#mob_no").val("");
				$("#vendorName").attr("disabled",true);
				$("#mob_no").attr("disabled",true);
			
			}else{
				
				$("#vendorName").removeAttr("disabled");
				$("#mob_no").removeAttr("disabled");
				getVendorByUserType(brdType,$("#ajaxUrl").val());
			}



		}
		
		
        //-->
     </script>
