<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->

<div id="container">
    <h1>Admin Dashboard</h1>
	<div style="width:700px; margin:0 auto;">
<table align="center" cellpadding="0" cellspacing="10" style="background-color:#fff;">
	<tr>
		<td valign="top" align="left">
			<table align="center" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
				<tr><td colspan="2" class="content-link_green">Purchage Requests:</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td width="58" class="content-link_Black">Status</td>
				<td width="208" class="content-link_Black" align="right">Count</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Pending</td><td align="right">{$creditStat.pending}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Allocated</td><td align="right">{$creditStat.allocated}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Rejected</td><td align="right">{$creditStat.rejected}</td></tr>
			</table>
		</td>
		<td valign="top" align="left">
			<table align="center" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
				<tr><td colspan="2" class="content-link_green">Sender IDs:</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td class="content-link_Black">Status</td><td class="content-link_Black" align="right">Count</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Pending</td><td align="right">{$senderidStat.pending}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Active</td><td align="right">{$senderidStat.active}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Inactive</td><td align="right">{$senderidStat.inactive}</td></tr>
		  </table>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
			<table align="center" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
				<tr><td colspan="2" class="content-link_green">Broadcast/SMS:</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
				<td width="220" class="content-link_Black">Status</td>
				<td width="73" class="content-link_Black" align="right">Count</td>	
				</tr>			
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Completed</td><td>{$smsStat.completed}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Pending</td><td>{$smsStat.pending}</td></tr>
				
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Active/Scheduled</td><td>{$smsStat.scheduled}</td></tr>
				
			</table>
		</td>
		<td valign="top" align="left">
			<table align="center" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
				<tr><td colspan="2" class="content-link_green">Subscribers :</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td class="content-link_Black">Status</td><td class="content-link_Black" align="right">Count</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Active</td><td align="right">{$subscriberStat.active}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Pending Confirmation</td><td align="right">{$subscriberStat.pending_conf}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Incomplete</td><td align="right">{$subscriberStat.incomplete}</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top">
		   <table width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
				<tr><td colspan="2" class="content-link_green">Broadcasters :</td></tr>
				
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				
				<tr><td class="content-link_Black">Status</td><td class="content-link_Black" align="right">Count</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Active-Individual</td><td>{$vendorStat.active_indivdual}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Active-Org</td><td>{$vendorStat.active_organisation}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Pending confirmation</td><td>{$vendorStat.pending_conf}</td></tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr><td>Incomplete</td><td>{$vendorStat.active_organisation}</td></tr>
			</table>		
		</td>
		<td valign="top" align="left">&nbsp;</td>
	</tr>
</table>
</div>
</div>
