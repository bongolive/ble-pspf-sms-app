<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->
<div id="container">
    <h1>Admin Broadcast</h1>
	<div style="width:700px; margin:0 auto;">
<div id="errorDiv" style="color:#FF0000">{$msg}</div>
<form name="smsSend" action="" method="POST" onsubmit="javascript: smsSend1(); return false;">

<table align="center" cellspacing="10">
	<tr>
		<td class="content-link_green" colspan="2"> Custom Broadcast/SMS</td>
		
	</tr>
	<tr>
		<td>Advertiser Name:</td>
		<td><span class="form_dinatrea standard-input">	
			<select name="advName" id="advName">
			<option value="">Select</option>
			{section name=vendors loop=$arrVendors}
			<option value="{$arrVendors[vendors].id}" 
			{if $arrVendors[vendors].id==$advName}selected{/if}
			style="padding:0 0 0 5px"> {$arrVendors[vendors].username} ({$arrVendors[vendors].name}) </option>
			{/section}
			</select></span>
		</td>
	</tr>
	<tr>
		<td>Receiver Type</td>
		<td><span class="form_dinatrea standard-input">
			<select name="receiverType" id="receiverType">
			<option value="">Select</option>
			<option value="SUB"
			{if 'SUB'==$receiverType}selected{/if}
			>Subscribers</option>
			<option value="VEN"
			{if 'VEN'==$receiverType}selected{/if}
			>Broadcaster</option>
			</select></span>
			
		</td>
	</tr>
	<tr>	
		<td></td>
		<td>
			<input type="submit" name="sms" id="sms" value="Next" class="editbtn">
			<input type="hidden" name="action" id="action" value="next" >
		</td>
		
	</tr>
	

</table>
</form>
</div>
</div>
{literal}
    
    <script type="text/javascript">
	//<!--

		function smsSend1(){
		
			var errorFlag = 0;
			if($("#advName").val()==""){
				alert("Select advertiser name");
				errorFlag=1;
				return false;

			}
			if($("#receiverType").val()==""){
				alert("Select Recever type");
				errorFlag=1;
				return false;

			}
			if(errorFlag ==0){
				document.smsSend.submit();
			
			}
		
		}
            
        //-->
     </script>


{/literal}