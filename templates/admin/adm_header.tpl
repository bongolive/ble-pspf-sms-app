<!--header start-->
<!--header end-->
<div class="topmenu_adm">
	<ul class="primary">
		<li {if $curPage=='ven_dashboard.php' || $curPage=='adm_dashboard.php'} class="active-topmenu  item-one-a current" {else} class="item-one"{/if}><a href="{$BASE_URL}adm_dashboard.php">Dashboard</a>
			<ul class="secondary">
				<li></li>
				<li></li>
				<li></li>
			</ul>
		</li>
		<li {if $curPage=='adm_coupone.php' || $curPage=='adm_keys.php' || $curPage=='adm_incoming_log.php' || $curPage=='adm_smspush.php'}class="active-topmenu current item-two-a" {else}class="item-two"{/if}><a href="{$BASE_URL}adm_smspush.php">Custom Broadcast</a> 
  	<ul class="secondary"> 
   	<li><a href="{$BASE_URL}adm_coupone.php">Coupon Broadcast</a></li> 
   	<li><a href="{$BASE_URL}adm_keys.php"> Keywords</a></li> 
   	<li><a href="{$BASE_URL}adm_incoming_log.php">Incoming log</a></li> 
  	</ul> 
 	</li>       		
		<li {if $curPage=='adm_senderid.php'} class="active-topmenu current item-six-a" {else} class="item-six"{/if}><a href="{$BASE_URL}adm_senderid.php">Sender ID</a>
			<ul class="secondary">
				<li></li>
				<li></li>
				<li></li>
			</ul>
		</li>
		<li {if $curPage=='adm_credits_request.php' || $curPage=='adm_add_credit.php'} class="active-topmenu current item-four-a" {else} class="item-four"{/if}><a href="{$BASE_URL}adm_credits_request.php">Purchase</a>
			<ul class="secondary">
				<li><a href="{$BASE_URL}adm_credits_request.php">Purchase</a></li>
				<li><a href="{$BASE_URL}adm_add_credit.php">Add Credit</a></li>
                <li><a href="{$BASE_URL}adm_credits_balances.php">Credit Balances</a></li>
				<li></li>
			</ul>
		</li>
		<li {if $curPage=='adm_broadcast.php' || $curPage=='adm_broadcast_log.php'}class="active-topmenu current item-five-a" {else} class="item-five"{/if}><a href="{$BASE_URL}adm_broadcast.php">Broadcast</a>
			<ul class="secondary">
				<li><a href="{$BASE_URL}adm_broadcast.php">Manage Broadcast</a></li>
                <li><a href="{$BASE_URL}adm_incoming.php">Manage Incoming SMS</a></li>
				<li><a href="{$BASE_URL}adm_broadcast_log.php">Log Report</a></li>
				<li></li>
			</ul>
		</li>
		<li {if $curPage=='adm_mng_orgcat.php'
			|| $curPage=='adm_emp_ranges.php'
			|| $curPage=='adm_age_ranges.php'
			|| $curPage=='adm_mng_location.php'
			|| $curPage=='adm_credits_schemes.php'
			|| $curPage=='adm_mng_occupation.php'
			|| $curPage=='adm_mng_category.php'
			|| $curPage=='adm_mng_education.php'
			|| $curPage=='adm_langs.php'
			|| $curPage=='adm_mng_employment.php'}class="active-topmenu current item-three-a" {else} class="item-three"{/if}><a href="{$BASE_URL}adm_credits_schemes.php">Master Setting</a>
			<ul class="secondary">
				<li><a href="{$BASE_URL}adm_credits_schemes.php">SMS Price</a></li>
				<li><a href="{$BASE_URL}adm_mng_orgcat.php">Organisations</a></li>
				<li><a href="{$BASE_URL}adm_emp_ranges.php">Employees Rnges</a></li>
				<li><a href="{$BASE_URL}adm_age_ranges.php">Age Ranges</a></li>
				<li><a href="{$BASE_URL}adm_mng_location.php">Locations</a></li>
				<li><a href="{$BASE_URL}adm_mng_occupation.php">Occupation</a></li>
				<li><a href="{$BASE_URL}adm_mng_category.php">Category</a></li>
				<li><a href="{$BASE_URL}adm_mng_education.php">Education</a></li>
				<li><a href="{$BASE_URL}adm_mng_employment.php">Employment</a></li>
				<li><a href="{$BASE_URL}adm_langs.php">Langs</a></li>
			</ul>
		</li>
		
		
		<li {if $curPage=='adm_manage_ven.php'}class="active-topmenu current" {else} class="" {/if}><a href="{$BASE_URL}adm_manage_ven.php">Vendors</a>
			<ul class="secondary">
				<!--<li><a href="{$BASE_URL}adm_broadcast.php">Manage Broadcast</a></li>-->
			</ul>
		</li>

		<li {if $curPage=='ven_subscribers.php' || $curPage=='adm_subscribers.php'}class="active-topmenu current item-seven-a" {else} class="item-seven"{/if}><a href="{$BASE_URL}adm_subscribers.php">Subscribers</a>
			<ul class="secondary">
				<li></li>
				<li></li>
				<li></li>
			</ul>
		</li>
		
		
	</ul>
</div>
<div class="clear"></div>
</div>
