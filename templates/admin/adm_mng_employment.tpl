<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!-- admin header end -->
<div id="container">
    <h1>Manage Employment</h1>
<div style="width:700px; margin:0 auto;">	
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg"{else} class="sucess_msg"{/if}
{if $msg} style="display:block;" {else} style="display:none;"{/if}>{$msg}</div>

<form name="employmentForm" method="POST" action="{$selfUrl}" onsubmit="employmentValidation(); return false;">
<table cellspacing="10" width="100%">
	<tr>
		<td width="150">Employment</td>
		<td width="802"><span class="form_dinatrea standard-input">
		<input type="text" name="employment" id="employment" value="{$employment}" size="100"></span>
		<span id="employmentDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="save" id="save" value="Save" class="editbtn" >
			<input type="hidden" name="action" id="action" value="{$action}" >
			<input type="hidden" name="id" id="id" value="{$id}" >
		</td>
	</tr>
</table>
</form>
<br /><br />
 <table align="center" width="100%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
	<tr>
		<td width="{if $smarty.session.lang!='en_us'}45%{else}90%{/if}" class="content-link_green" align="center">Employments</td>
		{if $smarty.session.lang!='en_us'}
		<td width="45%" class="content-link_green" align="right">English</td>
		{/if}
		<td width="10%" class="content-link_green" align="center">Edit</td>
	</tr>
	{section name=result loop=$arrEmployment}
	<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td align="left">{$arrEmployment[result].employment}</td>
			{if $smarty.session.lang!='en_us'}
			<td align="right" style="color:#C0C0C0;">{$arrEmployment[result].employment_en_us}</td>
			{/if}	
			<td align="center"><a href="{$selfUrl}?action=edit&id={$arrEmployment[result].id}" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
		</tr>
	
	{/section}
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="{if $smarty.session.lang!='en_us'}3{else}2{/if}" class="content-link" style="padding:10px;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
</table>
</div></div>
{literal}
    
    <script type="text/javascript">
	//<!--
		function employmentValidation(){
			
			var error=0;
			if($("#employment").val()==''){
			
				$("#employmentDiv").html('Enter Employment Name');
				error=1;	
			}else {
				$("#employmentDiv").html('');
			
			}
			if(error !=1){
				document.employmentForm.submit();
			}
		
		
		}
		
		
        //-->
     </script>


{/literal}

