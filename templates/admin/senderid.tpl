<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->
<div id="container">
    <h1>Manage Sender Name</h1>
	<div style="width:900px; margin:0 auto;">
<div id="errorDiv"></div>

	{if $action == 'update'}
	<form name="senderidEditForm" action="" method="POST" onsubmit='if(confirm("Save?")) if(!Error()) return false;'>
		<table cellspacing="10" cellpadding="0" align="center">
			<tr>
				<td>Sender id:</td>
				<td>
					<span class="form_dinatrea standard-input"><input type='text' name='senderId' id='senderId' value='{$arrSenderidDetails.senderid}'></span>
				</td><td><div id='error_id' style='color:red;'></div></td>
			</tr>
			<tr>
				<td>Sender name:</td>
				<td>
					<span class="form_dinatrea standard-input"><input type='text' name='name_broad' id='name_broad' value='{$arrSenderidDetails.username}'></span>
				</td><td><div id='error_name' style='color:red;'></div></td>
			</tr>
			<tr>
				<td>Status</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" >
						<option value="active" {if $arrSenderidDetails.status == 'active'} selected{/if}>Active</option>
						<option value="inactive"  {if $arrSenderidDetails.status == 'inactive'} selected{/if}>Inactive</option>
						<option value="pending" {if $arrSenderidDetails.status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" name="save" id="save" value="Save" class="editbtn">
					<input type="hidden" name="old_name" id="old_name" value="{$arrSenderidDetails.username}">
					<input type="hidden" name="old_status" id="old_status" value="{$arrSenderidDetails.status}">
					<input type="hidden" name="old_sender_id" id="old_sender_id" value="{$arrSenderidDetails.senderid}">
					<input type="hidden" name="id" id="id" value="{$arrSenderidDetails.id}">
					<input type="hidden" name="userId" id="userId" value="{$arrSenderidDetails.user_id}">
					<input type="hidden" name="vendorId" id="vendorId" value="{$arrSenderidDetails.vendor_id}">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
				<td>
					<input type="button" name="cancel" id="cancel" value="Cancel" class="editbtn" onclick="history.back()">
				</td>
				
			</tr>
		<table>	
		
	</form> 
	{else}
	<form name="senderidSearchForm" action="{$selfUrl}" method="GET">
		<table cellspacing="10">	
			<tr>
				<td width="136">Broadcaster name: </td>
				<td width="279"><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
			  </td>
				
			</tr>
			<tr>
				<td>Broadcaster ID: </td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="vendor_id" id="vendor_id" value="{$vendor_id}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Broadcaster Type: </td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendor_type" id="vendor_type" >
						<option value="" >Select</option>
						<option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
					</select>
				</span>
				</td>
				
			</tr>
			<tr>
				<td>Sender ID:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="senderid" id="senderid" value="{$senderid}"></span>
				</td>
			
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" >
						<option value="" >Select</option>
						<option value="active" {if $status == 'active'} selected{/if}>Active</option>
						<option value="inactive"  {if $status == 'inactive'} selected{/if}>Inactive</option>
						<option value="pending" {if $status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>
			
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
			
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
			
			</tr>
		</table>
	</form>	
	{/if}
			
		
	<form name="senderidForm" action="" method="POST">
		<table>
			<tr>
			<td colspan="3">
				<input type="hidden" name="id" id="id" value="{$arrSenderidDetails.id}">
				<input type="hidden" name="userId" id="userId" value="{$arrSenderidDetails.user_id}">
				<input type="hidden" name="action" id="action" value="edit">
			</td>
			</tr>
		</table>
	</form>
	<br />	<br />

	<table border="0" cellspacing="1" cellpadding="5" width="100%" style="background-color:#FFFFFF;">
	<tr style="background-color:#f8f8f8;">
		<td style="padding:10px;"><b>Broadcaster</b></td>
		<td><b>Broadcaster ID</b></td>
		<td><b>Broadcaster Type</b></td>
		<td><b>Sender ID</b></td>
		<td><b>Request Date</b></td>
		<td><b>Stats</b></td>
		<td><b>Edit</b></td>
	</tr>
	{if $arrSenderid}
	{section name=senderid loop=$arrSenderid}
	<tr {if $arrSenderidDetails.id==$arrSenderid[senderid].id} style="background-color:#DEFFD5" {else} style="background-color:#f8f8f8;" {/if}>	
		<td style="padding:10px;">{$arrSenderid[senderid].name}</td>
		<td>{$arrSenderid[senderid].vendor_id}</td>
		<td>
		{if $arrSenderid[senderid].vendor_type=='2'}
			Organisation
		{elseif $arrSenderid[senderid].vendor_type=='1'}
			Individual
		{/if}
		</td>
		<td>{$arrSenderid[senderid].senderid}</td>
		<td>{$arrSenderid[senderid].created}</td>
		<td id="statusTd">{$arrSenderid[senderid].status}</td>
		<td><input type="button" id="editSenderid" value="" onclick="javascript: editSenderid('{$arrSenderid[senderid].id}','{$arrSenderid[senderid].name}','{$arrSenderid[senderid].user_id}');" class="editbtn_2"></td>
	</tr>
	{/section}
	
	<tr>
		<td align="right" colspan="7" class="content-link">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} {paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr style="background-color:#f8f8f8;">
		<td colspan="7" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Rocords found
		</td>
	</tr>	
	{/if}
	</table>
</div>
</div>
<script type="text/javascript">
	//<!--
		function editSenderid(id,name_id,user_id){
			$("#id").val(id);
			$("#userId").val(user_id);
			$("#action").val('edit');
			$("#name_broad").val(name_id);

			document.senderidForm.submit();
		
		}

		function Error(){

		var name_broad = $("#name_broad").val();
		var vendorId = $("#vendorId").val();

		if (name_broad == '' || vendorId == ''){
			if (name_broad == ''){
				$('#error_name').html('Input Name!');	
			} else { $('#error_name').html('');	}

			if (vendorId == ''){
				$('#error_id').html('Input Id!');		
			} else { $('#error_id').html('');}

			return false;
		}else{
			return true;
		}
	}

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
        //-->
     </script>
