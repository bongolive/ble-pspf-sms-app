<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->
<div id="container">
    <h1>Manage Purchase Request</h1>
	
<div id="errorDiv"></div>
{if $action == 'edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST" onsubmit="if(!confirm('Are you sure change status?'))return false;">
		<table cellspacing="10">
			<tr>
				<td>Name:</td>
				<td>{$arrCreditDetails.name}</td>
				
			</tr>
			<tr>
				<td>Credit Requested: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crditRequested" id="crditRequested" value="{$arrCreditDetails.credit_requested}" onkeyup="getCreditShcemeDetailsAdm(this.value,document.getElementById('crdtSchmId').value,'{$ajaxUrl}');"></span></td>
				
			</tr>
			<!--<tr>
				<td>Rate:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="crdtSchmId" id="crdtSchmId" onchange="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');">
						<option value="">Select</option>
						{section name="crdtSchmId" loop=$arrCreditScheme}
						<option value="{$arrCreditScheme[crdtSchmId].id}###{$arrCreditScheme[crdtSchmId].rate}" 
						{if $arrCreditScheme[crdtSchmId].id== $arrCreditDetails.credit_scheme_id}Selected{/if}>{$arrCreditScheme[crdtSchmId].rate}</option>
						{/section}
					</select></span>
				</td>
				
			</tr>-->

{section name="crdtSchmId" loop=$arrCreditScheme}
	{if $arrCreditScheme[crdtSchmId].id == $arrCreditDetails.credit_scheme_id}
		{$tmp = $arrCreditScheme[crdtSchmId].rate}
	{/if}
{/section}

{section name="crdtSchmId" loop=$arrCreditBonus}
	{if $arrCreditBonus[crdtSchmId].id == $arrCreditDetails.credit_scheme_id}
		{$tmp = $arrCreditBonus[crdtSchmId].rate}
	{/if}
{/section}

	
			<tr>
				<td>Rate: </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="crdtSchmId" id="crdtSchmId" value="{$tmp}" onkeyup="getCreditShcemeDetailsAdm(document.getElementById('crditRequested').value,this.value,'{$ajaxUrl}');"></span></td>
				
			</tr>
			<tr>
				<td>Total Cost:</td>
				<td><div id="creditTotalCostAdm">{$arrCreditDetails.total_cost}</div></td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" {if $arrCreditDetails.status == 'allocated'} disabled {/if}>
						<option value="">Select</option>
						<option value="allocated" {if $arrCreditDetails.status == 'allocated'} selected{/if}>Allocate</option>
						<option value="rejected"  {if $arrCreditDetails.status == 'rejected'} selected{/if}>Reject</option>
						<option value="pending" {if $arrCreditDetails.status == 'pending'} selected{/if}>Pending</option>
					</select></span>
					{if $arrCreditDetails.status == 'allocated'} <input type='hidden' name='status' value='allocated'>{/if}
				</td>
				
			</tr>
			<tr>
				<td>Payment Status:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="payment_status" id="payment_status" >
						<option value="">Select</option>
						<option value="0" {if $arrCreditDetails.payment_status == '0'} selected{/if}>Not Paid</option>
						<option value="1" {if $arrCreditDetails.payment_status == '1'} selected{/if}>Paid</option>
						<option value="2" {if $arrCreditDetails.payment_status == '2'} selected{/if}>Partially Paid</option>
						<option value="3" {if $arrCreditDetails.payment_status == '3'} selected{/if}>Receipt Sent</option>
						<option value="4" {if $arrCreditDetails.payment_status == '4'} selected{/if}>Bonus</option>
					</select></span>
				</td>
				
			</tr>
			<tr>
				
				<td>
					<input type="submit" name="save" id="save" value="Save" class="editbtn">
					<input type="hidden" name="id" id="id" value="{$arrCreditDetails.id}">
					<input type="hidden" name="userId" id="userId" value="{$arrCreditDetails.user_id}">
					<input type="hidden" name="old_status" id="old_stutus" value="{$arrCreditDetails.status}">
					<input type="hidden" name="old_payment_status" id="old_pay" value="{$arrCreditDetails.payment_status}">
					<input type="hidden" name="action" id="action" value="update">
				</td>
				<td>
					<input type="button" name="cancel" id="cancel" value="Cancel" class="editbtn" onclick="history.back()">
				</td>

			</tr>
		</table>
	</form>


{elseif $action==""}
	<form name="creditRequest" action="" method="GET">
		<table cellspacing="10">	
			<tr>
				<td>Broadcaster name: </td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Broadcaster ID:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="vendor_id" id="vendor_id" value="{$vendor_id}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Broadcaster Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendor_type" id="vendor_type" >
						<option value="" >Select</option>
						<option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
					</select>
				</span>
				</td>
				
			</tr>
			<tr>
				<td>Price/SMS:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="rate" id="rate">
					<option value="">Select</option>
					{section name=rate loop=$arrCrdtSchm}
					<option value="{$arrCrdtSchm[rate].id}" 
					{if $arrCrdtSchm[rate].id==$rate}selected{/if}
					>{$arrCrdtSchm[rate].rate}</option>
					{/section}
					</select></span>
				</td>
				
			</tr>
			<tr>
				<td>Quantity:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="quantity" id="quantity" value="{$quantity}"></span>
				</td>
				
			</tr>
			<tr>
				<td>Status:</td>
				<td><span class="form_dinatrea standard-input">	
					<select name="status" id="status" >
						<option value="" >Select</option>
						<option value="allocated" {if $status == 'allocated'} selected{/if}>Allocated</option>
						<option value="rejected"  {if $status == 'rejected'} selected{/if}>Rejected</option>
						<option value="pending" {if $status == 'pending'} selected{/if}>Pending</option>
					</select></span>
				</td>				
			</tr>
			<tr>
				<td>Payment Status:</td>
				<td><span class="form_dinatrea standard-input">	
					<select name="payment_status" id="payment_status" >
						<option value="" >Select</option>
						<option value="0" {if $payment_status == '0'} selected{/if}>Not Paid</option>
						<option value="1" {if $payment_status == '1'} selected{/if}>Paid</option>
						<option value="2" {if $payment_status == '2'} selected{/if}>Partially Paid</option>
						<option value="3" {if $payment_status == '3'} selected{/if}>Receipt Sent</option>
						<option value="4" {if $payment_status == '4'} selected{/if}>Bonus</option>
					</select></span>
				</td>				
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
				
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
				
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
				
			</tr>
		</table>
	</form>	
	<br />
	<table width="100%" border="0" cellspacing="2" style="background-color:#FFFFFF;">
	<tr style="font-size:12px; font-weight:bold;background-color:#f8f8f8;">	
		<td style="padding:10px;" align="center">P R No.</td>
		<td align="center">Broadcaster</td>
		<td align="center">Broadcaster ID</td>
		<td align="center">Broadcaster Type</td>
		<td align="center">Pric/SMS</td>
		<td align="center">Quantity</td>
		<td align="center">Total Cost</td>
		<td align="center">Status</td>
		<td align="center">Payment Status</td>
		<td align="center">Request Date</td>
		<td align="center">Edit</td>
	</tr>
	{if $objCreditRequest}
	{section name=reqCredit loop=$objCreditRequest}
	<tr style="font-size:12px;background-color:#f8f8f8;">		
		<td style="padding:10px;"> {$objCreditRequest[reqCredit].credit_request_id} </td>
		<td> {$objCreditRequest[reqCredit].name} </td>
		<td> {$objCreditRequest[reqCredit].vendor_id} </td>
		<td>
		{if $objCreditRequest[reqCredit].vendor_type=='2'}
			Organisation
		{elseif $objCreditRequest[reqCredit].vendor_type=='1'}
			Individual
		{/if}
		</td>
		<td> {if ($objCreditRequest[reqCredit].credit_scheme_id==$objCreditRequest[reqCredit].idbon)}{$objCreditRequest[reqCredit].ratebon}{else}{$objCreditRequest[reqCredit].rate}{/if} </td>
		<td> {$objCreditRequest[reqCredit].credit_requested} </td>
		<td> {$objCreditRequest[reqCredit].total_cost} </td>
		<td> {$objCreditRequest[reqCredit].status|ucfirst} </td>
		<td align="center"> 
		{if $objCreditRequest[reqCredit].payment_status=='0'}
			Not Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='1'}
			Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='2'} 
			Partially Paid
		{elseif $objCreditRequest[reqCredit].payment_status=='3'} 
			Receipt Sent
		{elseif $objCreditRequest[reqCredit].payment_status=='4'} 
			Bonus
		{/if}
		</td>
		<td> {$objCreditRequest[reqCredit].created} </td>
		<!--<td align="center">{if $objCreditRequest[reqCredit].status=='allocated'} &nbsp; {else} <a href="?id={$objCreditRequest[reqCredit].id}&action=edit" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a> {/if}</td>-->

		<td align="center"> <a href="?id={$objCreditRequest[reqCredit].id}&action=edit" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a> </td>
	</tr>
	{/section}
	
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="10" class="content-link" style="padding:10px 0;">
			{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
			{paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr>
		<tr style="background-color:#f8f8f8;">
		<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Rocords found
		</td>
	</tr>	
	</tr>		

	{/if}
	</table>
{/if}

</div>

<script type="text/javascript">

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});		

     </script>
