<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!--admin header end-->
<div id="container">
    <h1>Admin Dashboard</h1>
	<div style="width:800px; margin:0 auto;">
<div id="errorDiv"></div>
{if $action == 'edit'}
	<form name="creditRequest" action="{$selfUrl}" method="POST">
		<table cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;" align="center">
			<tr>
				<td>Name</td>
				<td>{$arrCreditDetails.name}</td>				
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr>
				<td>Credit Requested</td>
				<td>{$arrCreditDetails.credit_requested}</td>
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr>
				<td>Total Cost</td>
				<td>{$arrCreditDetails.total_cost}</td>
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr>
				<td>Status</td>
				<td><span class="form_dinatrea standard-input">
					<select name="status" id="status" >
						<option value="">Select</option>
						<option value="allocated" {if $arrCreditDetails.status == 'allocated'} 
						selected{/if}>Allocate</option>
						<option value="rejected"  {if $arrCreditDetails.status == 'rejected'} 
						selected{/if}>Reject</option>
						<option value="pending" {if $arrCreditDetails.status == 'pending'} 
						selected{/if}>Pending</option>
					</select>
					</span>
				</td>
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="save" id="save" value="Save" class="editbtn">
					<input type="hidden" name="id" id="id" value="{$arrCreditDetails.id}">
					<input type="hidden" name="userId" id="userId" value="{$arrCreditDetails.user_id}">
					<input type="hidden" name="action" id="action" value="update">

				</td>				
			</tr>
		</table>
	</form>


{elseif $action==""}
	<form name="creditRequest" action="" method="GET">
		<table cellspacing="10" width="600">	
			<tr>
				<td>Broadcaster name:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendorName" id="vendorName">
					<option value="">Select</option>
					{section name=vendors loop=$arrVendors}
					<option value="{$arrVendors[vendors].id}" 
					{if $arrVendors[vendors].id==$vendorName}selected{/if}
					>{$arrVendors[vendors].name}</option>
					{/section}
					</select></span>
				</td>				
			</tr>
			
			<tr>
				<td>Broadcaster Type:</td>
				<td><span class="form_dinatrea standard-input">
					<select name="vendor_type" id="vendor_type" >
						<option value="" >Select</option>
						<option value="1" {if $vendor_type=='1'}selected{/if} >Individual</option>
						<option value="2"  {if $vendor_type=='2'}selected{/if}>Organisation</option>
					</select>
				</span>
				</td>
			</tr>
			<tr>
				<td>Broadcaster Phone:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="mob_no" id="mob_no" value="{$mob_no}"></span>
				</td>
			</tr>
			<tr>
				<td valign="top">SMS Message:</td>
				<td><span class="form_dinatrea standard-input" style="width:250px;">
					<textarea name="textMessage" id="textMessage" cols="40"  rows="3"></textarea></span>
				</td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="{$startDate}"></span>
				</td>
			</tr>
			<tr>
				<td>End Date:</td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="{$endDate}">	</span>
				</td>
			</tr>

			<tr>
				<td valign="top">Status:</td>
				<td colspan="3" valign="top">
					<table align="left" cellspacing="5">
						<tr>
						  <td width="20"><input type="checkbox" name="status" id="status" value="Delivered"></td>
							<td width="256">Delivered</td>
						</tr>
						<tr>
							<td><input type="checkbox" name="status" id="status" value="Undelivered"></td>
							<td>Undelivered</td>
						</tr>
						<tr>
							<td><input type="checkbox" name="status" id="status" value="Pending"></td>
							<td>Pending</td>
						</tr>
						<tr>
							<td><input type="checkbox" name="status" id="status" value="Failed"></td>
							<td>Failed</td>
						</tr>
						
					</table>
				</td>
			</tr>
			
			<tr>
				<td colspan="3">
					<table align="left" cellspacing="5" width="100%">
						<tr>
							<td width="181">Total Price Range - </td>
							<td width="156"><span class="form_dinatrea standard-input">
						  <input type="text" name="priceRangeTo" id="priceRangeTo" value="{$priceRangeTo}"></span>	</td>
							<td width="21">to </td>
							<td width="183"><span class="form_dinatrea standard-input">
						  <input type="text" name="priceRangeFrom" id="priceRangeFrom" value="{$priceRangeFrom}"></span></td>
						</tr>
					</table>
				</td>
				
			</tr>
			<tr>
				<td colspan="3">
					<table align="left" cellspacing="5" width="100%">
						<tr>
							<td width="181">SMS Count Range - </td>
							<td width="153"><span class="form_dinatrea standard-input">
								<input type="text" name="smsCountTo" id="smsCountTo" value="{$smsCountTo}">	</span>
						  </td>
							<td width="21">to </td>
							<td width="186"><span class="form_dinatrea standard-input">
								<input type="text" name="smsCountFrom" id="smsCountFrom" value="{$smsCountFrom}"></span>
						  </td>
						</tr>
					</table>
				</td>
				
			</tr>
			
			<tr>
				<td></td>
				<td>
					<input type="submit" name="search" id="search" value="Search" class="editbtn">
					<input type="hidden" name="action" id="action" value="{$action}">
				</td>
			</tr>
		</table>
	</form>	
	
	<table cellpadding="5" width="100%" cellspacing="2" style="background-color:#ffffff;">
	<tr  style="background-color:#f8f8f8;font-weight:bold;font-size:12px;">		
		<td style="padding:10px;">Broadcaster</td>
		<td>Broadcaster Type</td>
		<td>Broadcast Type</td>
		<td>SMS Message</td>
		<td>Date</td>
		<td>Status</td>
		<td>Price</td>
		<td>Sms Cout</td>
		<td>&nbsp;</td>
	</tr>
	{if $arrSms}
	{section name=sms loop=$arrSms}	
	<tr style="font-size:12px;background-color:#f8f8f8;">	
		<td style="padding:15px;">{$arrSms[sms].username}</td>
		<td>
		{if $arrSms[sms].vendor_type=='2'}
			Organisation
		{elseif $arrSms[sms].vendor_type=='1'}
			Individual
		{/if}
		</td>		
		<td>{$arrSms[sms].credit_requested}</td>
		<td>{$arrSms[sms].text_message}</td>
		<td>{$arrSms[sms].created}</td>
		<td>{$arrSms[sms].status}</td>
		<td>{$arrSms[sms].credit}</td>
		<td>{$arrSms[sms].credit}</td>
		<td class="content-link"><a href="?id={$arrSms[sms].id}&action=edit">Edit</a></td>
	</tr>
	{/section}
	
	<tr style="background-color:#f8f8f8;">
		<td align="right" colspan="10" class="content-link" style="padding:10px 0;">
		{paginate_first} {paginate_prev} {paginate_middle format="page" page_limit="5" prefix=" " suffix=" "} 
		{paginate_next} {paginate_last}
		</td>
	</tr>
	{else}
	<tr class="TR_spc"><td colspan="10" class="TD_spc"></td></tr>
	<tr style="background-color:#f8f8f8;">
		<td colspan="10" align="center"  class="content-link" valign="middle" style="padding:10px;">
		No Rocords found
		</td>
	</tr>		

	{/if}
	</table>
{/if}

</div>

</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
        //-->
     </script>
