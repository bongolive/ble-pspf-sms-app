<!--admin header start-->
{include file="admin/adm_header.tpl"}
<!-- admin header end -->
<div id="container">
    <h1>Manage employees-range</h1>
<div style="width:700px; margin:0 auto;">	
<div id="errorDiv" {if $errorFlag=='1'} class="error_msg"{else} class="sucess_msg"{/if}
{if $msg} style="display:block;" {else} style="display:none;"{/if}>{$msg}</div>

<form name="admAgeRange" method="POST" action="{$selfUrl}" onsubmit="ageRageValidation(); return false;">
<table cellspacing="10" width="100%">
	<tr>
		<td width="121">Employees range</td>
		<td width="802"><span class="form_dinatrea standard-input">
		<input type="text" name="empRange" id="empRange" value="{$empRange}" size="10"></span>
		<span id="empRangeDiv"  class="form_error"></span></td>
	</tr>
	<tr>
		<td></td>
		<td>
			<input type="submit" name="save" id="save" value="Save" class="editbtn" >
			<input type="hidden" name="action" id="action" value="{$action}" >
			<input type="hidden" name="id" id="id" value="{$id}" >
		</td>
	</tr>
</table>
</form>
<br /><br />
 <table align="center" width="100%" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
	<tr>
		<td width="20%" class="content-link_green" align="center">Employees Ranges</td>
		<td width="20%" class="content-link_green">&nbsp;</td>
		<td width="20%" class="content-link_green">&nbsp;</td>
		<td width="20%" class="content-link_green">&nbsp;</td>
		<td width="10%" class="content-link_green">Edit</td>
		
	</tr>
	{section name=result loop=$arrEmpRanges}
	<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td align="center">{$arrEmpRanges[result].people_range}</td>
			<td align="right"></td>
			<td align="right"></td>
			<td align="right"></td>	
			<td><a href="{$selfUrl}?action=edit&id={$arrEmpRanges[result].id}" title="Edit">
			<img alt="Edit" src="{$BASE_URL_HTTP_ASSETS}images/edit.png" /></a></td>
			
		</tr>
	
	{/section}
</table>
</div></div>
{literal}
    
    <script type="text/javascript">
	//<!--
		function ageRageValidation(){
			
			var error=0;
			if($("#empRange").val()==''){
			
				$("#empRangeDiv").html('Input employees-range');
				error=1;	
			}else {
				$("#empRangeDiv").html('');
			
			}
			if(error !=1){
				document.admAgeRange.submit();
			}
		
		
		}
		
		
        //-->
     </script>


{/literal}

