{include file="admin/adm_header.tpl"}
<div id="container">
    <h1>Admin Broadcast</h1>
	<div style="width:700px; margin:0 auto;">
<form name="admConfSmspus" action="" method="POST">
	<table align="center" width="600" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
		<tr>
			<td width="150">Advertiser Name</td>
			<td width="450">{$advName}</td>
		</tr>
		<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td>SMS</td>
			<td>{$textSms|nl2br|wordwrap:40}</td>
		</tr>
		<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td>Total Receiver</td>
			<td>{$countReceivers} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="content-link" href="{$BASE_URL}dnld_receiver_details.php?receiverType={$receiverType}&action=dnld">Download Contacts</a></td>
		</tr>
		<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
		<tr>
			<td>
				<input type="button" name="back2" id="back2" value="Back" onclick="javascript: backTo(2)" class="editbtn">
			</td>
			<td>
				<input type="submit" name="conf" id="conf" value="Confirm" {if $countReceivers==0}disabled class="disable_btn"  {else} class="editbtn" {/if}>
				<input type="hidden" name="action" id="action" value="smsconf" >
				<input type="hidden" name="receiverType" id="receiverType" value="{$receiverType}" >
			</td>
		</tr>

	</table>
</form>
</div>
</div>
<script type="text/javascript">
	//<!--
            
		function backTo(val){
			
			$("#action").val('backTo'+val);
			document.admConfSmspus.submit();	
		}
        //-->
     </script>