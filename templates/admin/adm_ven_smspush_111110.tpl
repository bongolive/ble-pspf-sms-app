{include file="admin/adm_header.tpl"}
<div id="container">
    <h1>Admin Broadcast</h1>
		<div style="width:700px; margin:0 auto;">
<form name="admVenSmspus" action="" method="POST" onsubmit="admVenSmspus1(); return false;">
<table cellpadding="0" cellspacing="10" align="center">
	<tr>
		<td valign="top">
			<table cellpadding="0" width="325" cellspacing="10" style="background-color:#f8f8f8;">
				<tr>
					<td colspan="2" class="content-link_green">Brosdcaster/Vendor Type</td>
					
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="vendorType[]" id="vendorType" value="1" 
					{if is_array($vendorType)}
					{if in_array(1,$vendorType)}
						checked
					{/if}
					{/if}
					></td>
					<td>Individual</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><input type="checkbox" name="vendorType[]" id="vendorType" value="2"
					{if is_array($vendorType)}
					{if in_array(2,$vendorType)}
						checked
					{/if}
					{/if}
					></td>
					<td>Organisation</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			
		</td>
		</tr>
		<tr>
			<td class="content-link_green" align="left">Organisation</td>
			<td  class="content-link_green" align="left">Number of people</td>
			
		</tr>
		<tr>
			<td>
			    <div class='flexcroll' style="height:200px; width:310px;">
					<table id="tableOrganisation" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
						<tr>
							<td><input type="checkbox" name="allOrganisation" id="allOrganisation" value="" onclick="checkAll('allOrganisation','tableOrganisation');"></td>
							<td>All</td>
						</tr>
						{section name=organisation loop=$arrOrganisations}
						<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
						<tr>
							<td><input type="checkbox" name="organisation[]" id="organisation" value="{$arrOrganisations[organisation].id}" 
							{if is_array($organisation)}
							{if in_array($arrOrganisations[organisation].id,$organisation)}
								checked
							{/if}
							{/if}
							></td>
							<td>{$arrOrganisations[organisation].organisation}</td>
						</tr>
					{/section}
					</table>
				</div>
			</td>
			
			<td>
				<div class='flexcroll' style="height:200px; width:310px;">
					<table id="tablePeople" width="325" cellpadding="0" cellspacing="10" style="background-color:#f8f8f8;">
						<tr>
							<td><input type="checkbox" name="allPeople" id="allPeople" value="" onclick="checkAll('allPeople','tablePeople');"></td>
							<td>All</td>
						</tr>
						{section name=people loop=$arrPeople}
						<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
						<tr>
							<td><input type="checkbox" name="people[]" id="people" value="{$arrPeople[people].id}"
							{if is_array($people)}
							{if in_array($arrPeople[people].id,$people)}
								checked
							{/if}
							{/if}
							></td>
							<td>{$arrPeople[people].people_range}</td>
						</tr>
						{/section}
					</table>
				</div>
			</td>
		</tr>
		<tr>
			<td class="content-link_green" align="left">Locations</td>
			<td  class="content-link_green" align="left"></td>
			
		</tr>
		<tr>
		<td valign="top">
			<div class='flexcroll' style="height:200px; width:310px;">
				<table id="tableLocation" width="300" cellpadding="0" cellspacing="5" style="background-color:#f8f8f8;">
					<tr>
						<td><input type="checkbox" name="allLocation" id="allLocation" value="" onclick="checkAll('allLocation','tableLocation');"></td>
						<td>All</td>
					</tr>
					{section name=location loop=$arrLocation}
					<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
					<tr>
						<td><input type="checkbox" name="location[]" id="location" value="{$arrLocation[location].id}" 
						{if is_array($location)}
								{if in_array($arrLocation[location].id,$location)}
								checked
								{/if}
						{/if}
						></td>
						<td>{$arrLocation[location].location}</td>
					</tr>
					{/section}
				</table>
			</div>
		</td>
		<td valign="top">
			
		</td>
	</tr>
</table>
<br /><br />
<table cellspacing="10">
	<tr>
		<td valign="top">SMS Massage</td>
		<td><span class="form_dinatrea standard-input" style="width:250px;">
			<textarea name="textMessage" id="textMessage" onkeyup="checkMessageLength();" cols="40" rows="3">{$textMessage}</textarea></span>
		</td>
		
	</tr>
	<tr>
		<td>Counter</td>
		<td><span id="charCount">{if !$charCount}0{else}{$charCount}{/if}</span>/160</td>
		<td><div id="messageCount"></div></td>
	</tr>
	<tr>
		<td>Sender ID</td>
		<td><span class="form_dinatrea standard-input">
			<select name="senderid" id="senderid" >
			<option value="">Select</option>
			{section name=senderid loop=$arrSenderid}
			<option value="{$arrSenderid[senderid].id}" 
			{if $arrSenderid[senderid].id==$senderid}selected{/if}
			>{$arrSenderid[senderid].senderid}</option>

			{/section}
			</select></span>
		</td>
		
	</tr>
	<tr>
		{if isset($scheduleDate) && $scheduleDate!=''}	
		<td id="advOptionTd" onclick="javascript: showHideAdvOption();">- Advanced Option</td>
		{else}
		<td id="advOptionTd" onclick="javascript: showHideAdvOption();">+ Advanced Option</td>	
		{/if}
		
	</tr>
	<tr>
		<td colspan="5">
			{if isset($scheduleDate) && $scheduleDate!=''}	
				<div id="advOptionDiv" style="display:block">

			{else}
				<div id="advOptionDiv" style="display:none">

			{/if}
				<table>
					
					<tr>
						<td>Schedule date</td>
						<td><span class="form_dinatrea standard-input"><input type="text" name="scheduleDate" id="scheduleDate"  value="{$scheduleDate}"
						{if !isset($scheduleDate) || $scheduleDate==''}	
							disabled
						{/if}						
						></span></td>
						
					</tr>					
				</table>
			
			</div>
		</td>
	</tr>
	<tr>
		<td></td>

		<td>
			<input type="button" name="back" id="back" value="Back" onclick="javascript: backTo(1);" class="editbtn">
			<input type="submit" name="send" id="send" value="Send" class="editbtn">
			<input type="hidden" name="action" id="action" value="smspush" >
			<input type="hidden" name="receiverType" id="receiverType" value="{$receiverType}" >
		</td>
		
	</tr>
	

</table>
</form>
</div>		
</div>
{literal}
    
    <script type="text/javascript">
	//<!--
            
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		function backTo(val){
			
			$("#action").val('backTo'+val);
			document.admVenSmspus.submit();	
		}

		function admVenSmspus1(){
		
			if($("#textMessage").val()==''){
				alert('Inter text message');
				return false;
			}else if($("#senderid").val()==''){
				alert('Select senderId');
				errorFlag = 1;
				return false;
			}else{
				document.admVenSmspus.submit()
			}
		
		}
		
        //-->
     </script>


{/literal}