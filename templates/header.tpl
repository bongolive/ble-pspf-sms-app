<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>
	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{$smarty.const.SUB_TITLE}</title>
		<link href="{$BASE_URL_HTTP_ASSETS}css/style.css" rel="stylesheet" type="text/css" />
		<!--<link href="{$BASE_URL_HTTP_ASSETS}css/stylesheet_revisions.css" rel="stylesheet" type="text/css" />-->
		<link rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/fader.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="{$BASE_URL_HTTP_ASSETS}css/ddsmoothmenu.css" />
		<script type="text/javascript">
			var assetPath = '{$BASE_URL_HTTP_ASSETS}';

		</script>
		<script src="{$BASE_URL_HTTP_ASSETS}js/jquery-1.4.2.min.js" type="text/javascript"></script>
		<script src="{$BASE_URL_HTTP_ASSETS}js/png-transparent.js" language="javascript" type="text/javascript"></script>
		<script src="{$BASE_URL_HTTP_ASSETS}js/jquery.anythingfader.js" type="text/javascript"></script>
		<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/ddsmoothmenu.js"></script>
				
		<script type="text/javascript">
			// ddeclaration of image path global for javascript
			var imagePath = '{$BASE_URL_HTTP_ASSETS}images/';
		</script>
		<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions_lang_{$smarty.session.lang}.js" type="text/javascript"></script>
		<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions.js" type="text/javascript"></script>
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/timepicker.js"></script>
        <link rel="icon" type="image/ico" href="favicon.ico">
</head>
<body>
<div id="container-top">
	<div class="logo"><a href="index.php"><img alt="Bongo Live logo" src="{$BASE_URL_HTTP_ASSETS}images/logo.png" /></a></div>
	<div id="header_topright">
		{include file="langs.tpl"}

		<div style="text-align:right;width:650px; margin:0; height:20px;"></div>
		
	</div>
	<div class="clear"></div>