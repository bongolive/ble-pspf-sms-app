<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>
	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" />
<title>Bongo Live! - Admin </title>
<link href="{$BASE_URL_HTTP_ASSETS}css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="{$BASE_URL_HTTP_ASSETS}css/fader.css" type="text/css" media="screen" />	

<script src="{$BASE_URL_HTTP_ASSETS}js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="{$BASE_URL_HTTP_ASSETS}js/jquery.anythingfader.js" type="text/javascript"></script>

<link href="{$BASE_URL_HTTP_ASSETS}css/flexcrollstyles.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src="{$BASE_URL_HTTP_ASSETS}js/flexcroll.js"></script>


<script type="text/javascript">
/*
$(document).ready(function() {

	//Default Action
	$(".tab_content").hide(); //Hide all content
	$("ul.tabs li:first").addClass("active").show(); //Activate first tab
	$(".tab_content:first").show(); //Show first tab content
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});

});
*/

$(document).ready(function() {
	
	//On Click Event
	$("ul.tabs li").click(function() {
		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content
		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content
		$(activeTab).fadeIn(); //Fade in the active content
		return false;
	});

});

</script>	
	<script type="text/javascript">
		
			function formatText(index, panel) {
			  return index + "";
			}
		
			$(function () {
			
				$('.anythingFader').anythingFader({
					autoPlay: true,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.
					delay: 8000,                    // How long between slide transitions in AutoPlay mode
					startStopped: false,            // If autoPlay is on, this can force it to start stopped
					animationTime: 1000,             // How long the slide transition takes
					hashTags: true,                 // Should links change the hashtag in the URL?
					buildNavigation: true,          // If true, builds and list of anchor links to link to each slide
					pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover
					startText: "",                // Start text
					stopText: "",               // Stop text
					navigationFormatter: formatText   // Details at the top of the file on this use (advanced use)
				});
				
				$("#slide-jump").click(function(){
					$('.anythingFader').anythingFader(6);
				});
				
			});
		</script>
	<script type="text/javascript">  
		
	$(document).ready(function () {	
		
		$('#nav li').hover(
			function () {
				//show its submenu
				$('ul', this).slideDown(100);
	
			}, 
			function () {
				//hide its submenu
				$('ul', this).slideUp(100);			
			}
		);
		
	});
	   
	</script>	
	
	<script type="text/javascript">
		
		// ddeclaration of image path global for javascript

		var imagePath = '{$BASE_URL_HTTP_ASSETS}images/';
	</script>

	<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions_lang_{$smarty.session.lang}.js" type="text/javascript"></script>
	<script src="{$BASE_URL_HTTP_ASSETS}js/common_functions.js" type="text/javascript"></script>
	<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
	<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/jquery-ui-1.7.2.custom.min.js"></script>
	<script type="text/javascript" src="{$BASE_URL_HTTP_ASSETS}js/timepicker.js"></script>


</head>

<body>
<div id="container-top">
  <div class="logo"><a href="index.php"><img alt="Bongo Live logo" src="{$BASE_URL_HTTP_ASSETS}images/logo.png" /></a></div>
  <div class="nav-sign-in">
  	{include file="langs.tpl"} 
	{if $login}
		<ul>
		  <li id="user-id">{$welcome} {$username},</li>
		  <li></li>
		  <li><a href="{$logout_url}">{$logout}</a></li>
		</ul>
	{/if}
  </div>
  <div id="main-info">
    
  </div>
  <div class="clear"></div>
  <p id="user-role">Admin</p>
 


