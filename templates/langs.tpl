<div id="lang_container">
{foreach from=$site_langs item=lng name=site_langs}
	{if $smarty.session.lang==$lng@key}
	<strong><img src="{$BASE_URL_HTTP_ASSETS}images/flags/{$lng@key}.png">&nbsp;{$lng}</strong>
	{else}
	<a href="lang.php?lang={$lng@key}"><img src="{$BASE_URL_HTTP_ASSETS}images/flags/{$lng@key}.png">&nbsp;{$lng}</a>
	{/if}
	{if $lng@iteration<$lng@total}&nbsp;|&nbsp;{/if}
{/foreach}
</div>