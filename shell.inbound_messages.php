<?php
	include_once "bootstrap.php";
	include_once(LIB_DIR . "shell.inc.php");
	include_once(MODEL . "sms/sms.class.php");
	include_once(MODEL . "addressbook/addressbook.class.php");
	include_once(MODEL . "user.class.php");
	
	$objSms = new sms();
	$objUser = new user();
	$objAddressbook = new addressbook();
	$counter = 0;
	
	// Infinite loop starts
	while (true) {   
	
		echo "<pre>In action again, checking...\n";
		$arraySmsDetails = $objSms ->getSmsDetails();
		
		$smsCount = count($arraySmsDetails);
		//print_r($arraySmsDetails); exit;
		if($smsCount > 0){
			echo "Data found.. so process...\n";
			
			foreach($arraySmsDetails as $sms){
				$arrMobNo = array();
				$sendMessage ='';
				$queryStr ='';
				
				$sendMessage = $sms['text_message'];
				$msgLength	 = $sms['message_length'];
				$msgCount	 = $sms['message_count'];
				$objAddressbook->userId = $sms['sent_by'];
			
				
				// if sender_type is ADM
				if($sms['sender_type']=='ADM'){

					if($sms['receiver_type']=='SUB'){
						$objUser->userId = explode(',',$sms['receiver_id']);	
						$arrMobNo = $objUser->getAllMobiles('mst_subscribers');
					}elseif($sms['receiver_type']=='VEN'){
						$objUser->userId = explode(',',$sms['receiver_id']);	
						$arrMobNo = $objUser->getAllMobiles('mst_vendors');
					}

				}elseif($sms['sender_type']=='VEN'){ // if sms send by Vendor
					
					$objAddressbook -> addressbookId = $sms['addressbook_id'];
					$objAddressbook -> contactId = $sms['contact_id'];
					$arrMobNo = $objAddressbook -> getAllMobiles();
				}

				
				//finding duplicate in contacts provided
				$arrSampleValidMob= array();
				$arrValidMob= array();

				foreach($arrMobNo as $rowCheck){
					if(in_array($rowCheck['mob_no'], $arrSampleValidMob, true)){
						
					}else{
						array_push($arrSampleValidMob,$rowCheck['mob_no']);
						array_push($arrValidMob,$rowCheck);
					}
				}
				
				
				//$arrMobNo = array_unique($arrMobNo);
				
				foreach($arrValidMob as $rowMob){
					$mobNo = $rowMob['mob_no'];
					$objAddressbook->contactId=$rowMob['id'];
					$objAddressbook->mob_no=$mobNo;
					$arrContactDetail = $objAddressbook->getContactDetailByPhone();
						
						
						//$txtSMS = $sendMessage;
						$msg = str_replace('{mobile}', $arrContactDetail['mob_no'] , $sendMessage);
						$msg = str_replace('{first_name}', $arrContactDetail['fname'] , $msg);
						$msg = str_replace('{last_name}', $arrContactDetail['lname'] , $msg);
						$msg = str_replace('{title}', $arrContactDetail['title'] ,$msg);
						$msg = str_replace('{birth_date}', $arrContactDetail['birth_date'] ,$msg);
						$msg = str_replace('{gender}', $arrContactDetail['gender'] ,$msg);
						$msg = str_replace('{email}', $arrContactDetail['email'] ,$msg);
						$msg = str_replace('{optional_one}', $arrContactDetail['optional_one'] ,$msg);
						$msg = str_replace('{optional_two}', $arrContactDetail['optional_two'] , $msg);
						$msg = str_replace('{area}', $arrContactDetail['area'] ,$msg);
						$msg = str_replace('{city}', $arrContactDetail['city'] ,$msg);
						$msg = str_replace('{country}', $arrContactDetail['country'],$msg);
					
					
						$str=str_replace(chr(13),'', $msg);	
						$smsCount = ceil(strlen($str)/160);
						$charCount = strlen($str);

					
						//$queryStr = " INSERT INTO inbound_messaages (id,sms_store_id,user_id,status,short_message,msg_length,msg_count,source_addr,destination_addr,created) VALUES"
						$queryStr = "(UUID(),'".$sms['id']."','".$sms['sent_by']."','0','".mysql_real_escape_string ($msg)."','".$charCount."','".$smsCount."','".mysql_real_escape_string($sms['senderid'])."','".$mobNo."',NOW())";
						$objInboundMsg = new sms();
						$objInboundMsg ->queryStr = $queryStr;
						$status = $objInboundMsg -> proccessInboundMessage();
						
						
						//Checking type of Reminder
						$objInboundMsg ->smsStoreID=$sms['id'];
						$objInboundMsg ->contactId=$rowMob['id'];
						$arrReminder=$objInboundMsg->getReminderByContactId();
						$isBirthdayReminder=false;
						if($arrReminder!=false){
							$objInboundMsg->updateReminderProcessed();
							$isBirthdayReminder=true;
						}
					}
				
				
				//$objInboundMsg = new sms();
				$objInboundMsg = new sms();
				$objInboundMsg ->id =$sms['id'];
				if($isBirthdayReminder==false){
					if($objInboundMsg->smsProccessed() ){
						if($sms['sender_type']=='VEN'){
							$objInboundMsg->userId =$sms['sent_by'];
							$objInboundMsg->credit = $sms['credit'];
							$objInboundMsg-> updateCreditBalance(); 
						}
					}
				}else{
					$objInboundMsg->userId =$sms['sent_by'];
					$objInboundMsg->credit = $sms['credit'];
					$objInboundMsg->UpdateIsProccessedForBirthday();
					$objInboundMsg->ReturnReminderCredit();
				}
			}
				
				$counter++;
		}
		echo "Going to sleep..." . time() . "\n";
		sleep(2);
		echo "Just got up..." . time() . "\n";
	
	// Infinite loop ends
	}
	

	include_once(LIB_DIR . "close.php");
?>