<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: Upload vendor contacts from excel files
*	Author : Leonard nyirenda
****************************************************************************************************
*/
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//SmartyPaginate::connect();
	
	//These setting will be used by file SMS only when script run and will not make any changes to php.ini file
	ini_set(memory_limit,'250M');
	ini_set(max_execution_time,'900');
	ini_set(max_input_time,'900');
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	$errorFlag =0;
	
	switch($_REQUEST['action']){
		case 'uploadFile':
				if(isset($_FILES['file']) ){
					$f_detail = pathinfo($_FILES['file']['name']);
					if(strtolower($f_detail['extension'])== 'csv'){
						$arrRecipient = uploadMobilephoneFrom_csv($_FILES['file']['tmp_name']);
						$_SESSION['contact']['extendionFile']='csv';
					}else if(strtolower($f_detail['extension'])== 'xls'){
						require_once 'classes/exel/PHPExcel/Reader/excel_reader.php';
						$_SESSION['contact']['extendionFile']='xls';
   						$data = new Spreadsheet_Excel_Reader($_FILES['file']['tmp_name']);

    					for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
							for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
								$arrRecipient[$i-1][$j-1] = $data->sheets[0]['cells'][$i][$j];
        					}
   						}
					}else if(strtolower($f_detail['extension'])== 'txt'){
						$arrRecipient = uploadMobilephoneFrom_txt($_FILES['file']['tmp_name']);
						$_SESSION['contact']['extendionFile']='txt';	
					}else if(strtolower($f_detail['extension'])== 'xlsx'){
						//$arrRecipient = getXLSorXLSX($_FILES['file']['tmp_name']);
						$_SESSION['contact']['extendionFile']='xlsx';
						require_once "classes/exel/PHPExcel/Reader/simplexlsx.class.php";
						$xlsx = new SimpleXLSX($_FILES['file']['tmp_name'] );
	
						list($cols,) = $xlsx->dimension();
	
						$j	=	0;
						foreach( $xlsx->rows() as $k => $r) {
							for( $i = 0; $i < $cols; $i++){
			 					$arrRecipient[$j][$i] = $r[$i];
							}
							$j++;
						}	
					}
				}else if($_SESSION['contact']['recipients'] != ""){
					$arrRecipient = $_SESSION['contact']['recipients'];
				}
				 
				if($arrRecipient != ''){
					//Finding number of column of returned array
					$arrFirstRow	=array_slice($arrRecipient, 0, 1);
					$colCounter=0;
					foreach ($arrFirstRow as $value) {
    					$counter=1;
						foreach ($value as $key) {
    						$colCounter++;
							$arrCols[]	=	"VALUE".$counter;
							$counter++;
						}
					}
					
					$_SESSION['contact']['arrCols']=$arrCols;
					$_SESSION['contact']['cols']=$colCounter;
					//$_SESSION['contact']['fileuploaded']=$_REQUEST['fileuploaded'];
					$_SESSION['contact']['recipients'] = $arrRecipient;
					
					// Taking only ten Rows of an array
					$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
					if(count($arrRecipient)>0){
						$smarty->assign('action','uploadedData');
						$smarty->assign('arrColumn',$arrCols);
						$smarty->assign('arrRecipient',$arrRecipient);
					}else{
						$smarty->assign('action','uploadFile');
						$msg = VEN_SMS_PUSH_MSG_09;
						$errorFlag = 1;
					}
					
				}else{
					$smarty->assign('action','uploadFile');
					$msg = VEN_SMS_PUSH_MSG_09;
					$errorFlag = 1;
				}
		break;
		case 'uploadedData':
			
			if(isset($_REQUEST['headerRow']) && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else if($_SESSION['contact']['headerRow'] ==1 && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else{
				$headerRow = 0;
			}
			
			if(isset($_REQUEST['mobNoColumn'])){
				$mobNoColumn	=	$_REQUEST['mobNoColumn'];
			}else if($_SESSION['contact']['mobNoColumn'] != ""){
				$mobNoColumn	=$_SESSION['contact']['mobNoColumn'];
			}
			
			//Finding mobile,fname,lname,email,title nad comment user select
			$counter =0;
			foreach ($mobNoColumn as $value) {
				if($value=='title'){
					$title=$counter;
				}else if($value=='fname'){
					$fname=$counter;
				}else if($value=='lname'){
					$lname=$counter;
				}else if($value=='mobile'){
					$mobile=$counter;
				}else if($value=='email'){
					$email=$counter;
				}else if($value=='comment'){
					$comment=$counter;
				}else if($value=='mobile2'){
					$mobile2=$counter;
				}else if($value=='gender'){
					$gender=$counter;
				}else if($value=='birth_date'){
					$birth_date=$counter;
				}else if($value=='address'){
					$address=$counter;
				}else if($value=='optional_one'){
					$optional_one=$counter;
				}else if($value=='optional_two'){
					$optional_two=$counter;
				}else if($value=='area'){
					$area=$counter;
				}else if($value=='city'){
					$city=$counter;
				}else if($value=='country'){
					$country=$counter;
				}
				$counter++;
			}
			
			
			//checking if user select valid mobile number columb
			$arrMobile=array();
			$arrRecipientlist	=	$_SESSION['contact']['recipients'];
			$TotalContact=count($arrRecipientlist);
			for ($i=$headerRow; $i < $TotalContact; $i++){
				$phone = preg_replace('/[^0-9]/i', '', $arrRecipientlist[$i][$mobile]);
				$phone=CutMobileNumber(trim($phone));
				if(phoneValidation($phone) ){
					$arrMobile=$phone;
				}
			}
			
			
			 if(!isset($mobile)){
				$smarty->assign('action','uploadedData');
				$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
				$smarty->assign('headerRow',$_REQUEST['headerRow']);
				$smarty->assign('arrRecipient',array_slice($_SESSION['contact']['recipients'], 0, 9));
				$msg = VEN_SMS_PUSH_MSG_08;
				$errorFlag = 1;
			}else if(count($arrMobile)<1){
				$smarty->assign('action','uploadedData');
				$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
				$smarty->assign('headerRow',$_REQUEST['headerRow']);
				$smarty->assign('arrRecipient',array_slice($_SESSION['contact']['recipients'], 0, 9));
				$msg = VEN_SMS_PUSH_MSG_10;
				$errorFlag = 1;
			}else{
				//assigning data to array
				$_SESSION['contact']['mobNoColumn'] =	$mobNoColumn;
				$_SESSION['contact']['headerRow']	=	$headerRow;
				
				$smarty->assign('action','addressbook');
			}
		break;
		
		case 'addressbook':
			$data = $_SESSION['contact']['recipients'];
			$totalRows = count($data);
			
			$addrBookId = $_REQUEST['addbookId'];
			$_SESSION['contact']['addrBookId']	=  $addrBookId;
			
			//Finding mobile,fname,lname,email,title nad comment user select
			$counter =0;
			foreach ($_SESSION['contact']['mobNoColumn'] as $value) {
				if($value=='title'){
					$title=$counter;
				}else if($value=='fname'){
					$fname=$counter;
				}else if($value=='lname'){
					$lname=$counter;
				}else if($value=='mobile'){
					$mobile=$counter;
				}else if($value=='email'){
					$email=$counter;
				}else if($value=='comment'){
					$comment=$counter;
				}else if($value=='mobile2'){
					$mobile2=$counter;
				}else if($value=='gender'){
					$gender=$counter;
				}else if($value=='birth_date'){
					$birth_date=$counter;
				}else if($value=='address'){
					$address=$counter;
				}else if($value=='optional_one'){
					$optional_one=$counter;
				}else if($value=='optional_two'){
					$optional_two=$counter;
				}else if($value=='area'){
					$area=$counter;
				}else if($value=='city'){
					$city=$counter;
				}else if($value=='country'){
					$country=$counter;
				}
				$counter++;
			}
			
			
			//Build array for all valid data
			$rowCounter = 0;
			
			//$arrValidContact=array();
			$arrValidPhones=array();
			$arrExisting=array();
			for ($x=$_SESSION['contact']['headerRow']; $x < $totalRows; $x++){
				//validating Mobile phonr
				$Phone = preg_replace('/[^0-9]/i', '', $data[$x][$mobile]);
				$Phone=CutMobileNumber($Phone);
				if(phoneValidation($Phone)){ //and $data[$x][$fname]!=''
					
					//checking for duplicate mobile numbers in a file  uploaded
					if (in_array($Phone, $arrValidPhones, true) || in_array($Phone, $arrExisting, true)) {
    					$arrDuplicate[]	=	$Phone;
					}else{
					
					
					
						//check Check if phone number exist on group selected
						foreach($addrBookId as $addbookId){
							if($objAddressbook->CheckDuplicateContact($addbookId,$Phone)){
								if(! in_array($Phone, $arrExisting, true) && ! in_array($phone,$arrValidPhones, true)){
									$arrExisting[]	=	$Phone;
								}
							}else{
								// valid mobile phone
								$arrValidContact[$rowCounter][0] =$addbookId;//addressbookid
								$arrValidContact[$rowCounter][1] =$Phone; //mobile phone
								
								//assign phone to Single dimension array
								if ( ! in_array($Phone, $arrValidPhones, true)) {
    								$arrValidPhones[]	=	$Phone;
								}
			
								//adding title
								if(isset($title)){
									$arrValidContact[$rowCounter][2] =$data[$x][$title];
								}else{
									$arrValidContact[$rowCounter][2] ="";
								}
							
								//adding fname
								if(isset($fname)){
									$arrValidContact[$rowCounter][3] =$data[$x][$fname]; //first name
								}else{
									$arrValidContact[$rowCounter][3] ="";
								}
								
								//adding last name
								if(isset($lname)){
									$arrValidContact[$rowCounter][4] =$data[$x][$lname];
								}else{
									$arrValidContact[$rowCounter][4] ="";
								}
							
							
								//adding email
								if(isset($email)){
									$arrValidContact[$rowCounter][5] =$data[$x][$email];
								}else{
									$arrValidContact[$rowCounter][5] ="";
								}
							
								//adding comment
								if(isset($comment)){
									$arrValidContact[$rowCounter][6] =$data[$x][$comment];
								}else{
									$arrValidContact[$rowCounter][6] ="";
								}
								
								//adding Second mobile number
								if(isset($mobile2)){
									$Phon=preg_replace('/[\s]*/', '', $data[$x][$mobile2]);
									$Phon=CutMobileNumber($Phon);
									if(phoneValidation($Phon)){ //and $data[$x][$fname]!=''
										$arrValidContact[$rowCounter][7] =$Phon;
									}else{
										$arrValidContact[$rowCounter][7] ="";
									}
								}else{
									$arrValidContact[$rowCounter][7] ="";
								}
								
								//adding gender
								if(isset($gender)){
									$arrValidContact[$rowCounter][8] =$data[$x][$gender];
								}else{
									$arrValidContact[$rowCounter][8] ="";
								}
								
								
								//adding date of birth
								if(isset($birth_date)){
									if($_SESSION['contact']['extendionFile']=='xlsx'){
										$arrValidContact[$rowCounter][9] = date('Y-m-d',ExcelToPHP($data[$x][$birth_date]));
									}else{
										$arrValidContact[$rowCounter][9] = date('Y-m-d',strtotime($data[$x][$birth_date]));
									}
									//$timestamp=ExcelToPHP($data[$x][$birth_date]);
									//$arrValidContact[$rowCounter][9] =date('Y-m-d', $timestamp);
								}else{
									$arrValidContact[$rowCounter][9] ="";
								}
								
								
								//adding address
								if(isset($address)){
									$arrValidContact[$rowCounter][10] =$data[$x][$address];
								}else{
									$arrValidContact[$rowCounter][10] ="";
								}
								
								//adding optional one
								if(isset($optional_one)){
									$arrValidContact[$rowCounter][11] =$data[$x][$optional_one];
								}else{
									$arrValidContact[$rowCounter][11] ="";
								}
								
								//adding optional two
								if(isset($optional_two)){
									$arrValidContact[$rowCounter][12] =$data[$x][$optional_two];
								}else{
									$arrValidContact[$rowCounter][12] ="";
								}
								
								//adding area
								if(isset($area)){
									$arrValidContact[$rowCounter][13] =$data[$x][$area];
								}else{
									$arrValidContact[$rowCounter][13] ="";
								}
								
								//adding city
								if(isset($city)){
									$arrValidContact[$rowCounter][14] =$data[$x][$city];
								}else{
									$arrValidContact[$rowCounter][14] ="";
								}
								
								
								//adding country
								if(isset($country)){
									$arrValidContact[$rowCounter][15] =$data[$x][$country];
								}else{
									$arrValidContact[$rowCounter][15] ="";
								}
								
								
									$rowCounter++;
							} //clossing check existing if statement
						}//closing foreach loop
						
						
					}//closing check duplicaate if statement	
						
						
						
						
						
				}else{ //invalid records
					$arrInvalid[]	=	$Phone;
				}
				
			}
			 
			//buonding sample data to display
			$TotalRowsSample	=	count($arrValidContact);
				$counter=0;
				for ($i=0; $i < $TotalRowsSample; $i++){
						if($arrValidContact[$i][0]==$addbookId){
							$arrSampleContact[$counter][0]	=	$arrValidContact[$i][0];
							$arrSampleContact[$counter][1]	=	$arrValidContact[$i][1];	
							$arrSampleContact[$counter][2]	=	$arrValidContact[$i][2];
							$arrSampleContact[$counter][3]	=	$arrValidContact[$i][3];
							$arrSampleContact[$counter][4]	=	$arrValidContact[$i][4];
							$arrSampleContact[$counter][5]	=	$arrValidContact[$i][5];
							$arrSampleContact[$counter][6]	=	$arrValidContact[$i][9];
							$counter++;
						}
				}
			
			
			$_SESSION['contact']['countValid']=count($arrValidPhones);
			$_SESSION['contact']['valid']	=	$arrValidContact;
			$_SESSION['contact']['addrBookId']=	$addrBookId;
			
			if($_SESSION['contact']['headerRow']==0){
				$smarty->assign('TotalContact',count($data));
			}else{
				$smarty->assign('TotalContact',count($data)-1);
			}
			$smarty->assign('CountValidContact',count($arrValidPhones));
			$smarty->assign('CountInvalidContact',count($arrInvalid));
			$smarty->assign('CountExistContact',count($arrExisting));
			$smarty->assign('CountDuplicateContact',count($arrDuplicate));
			if(count($arrValidContact)>0){
				$smarty->assign('arrSampleSMS',array_slice($arrSampleContact, 0, 5));
			}
			$smarty->assign('action','preview');
		break;
		
		case 'refresh':
			$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 1);
			if($_REQUEST['headerRow']==1){
				foreach ($arrRecipient as $row) {
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= $value;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 1, 10);
				
			}else{
				foreach ($arrRecipient as $row) {
    				$counter = 1;
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= "VALUE".$counter;
						$counter++;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
			}
			$smarty->assign('mobNoColumn',$_REQUEST['mobNoColumn']);
			$smarty->assign('headerRow',$_REQUEST['headerRow']);
			$smarty->assign('arrColumn',$arrCols);
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('action','uploadedData');
		
		break;
		
		case 'backTo1':
			$smarty->assign('fileuploaded', $_SESSION['contact']['fileuploaded']);
			$action	= "uploadFile";
			$smarty->assign('action',$action);
		break;
		case 'backTo2':
			if($_SESSION['contact']['headerRow']==1){
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 1, 10);
			}else{
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
			}
			
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('headerRow',$_SESSION['contact']['headerRow']);
			$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
			$smarty->assign('action','uploadedData');
		break;		
		case 'cancel':
			unset($_SESSION['contact']);
			$smarty->assign('action','uploadFile');
		break;	
		case 'preview':
			$data	=	$_SESSION['contact']['valid'];
			$totalRows	= count($_SESSION['contact']['valid']);
			$addrBookId	=	$_SESSION['contact']['addrBookId'];
			
			
			for ($x=0; $x < $totalRows; $x++){
				$objAddressbook->addressbookId = $addrBookId;
				$objAddressbook->mob_no =cleanQuery($data[$x][1]);
				$objAddressbook->title = cleanQuery($data[$x][2]);
				$objAddressbook->fname =cleanQuery( $data[$x][3]);
				$objAddressbook->lname = cleanQuery($data[$x][4]);
				$objAddressbook->email = cleanQuery($data[$x][5]);
				//$objAddressbook->customs = cleanQuery($data[$x][6]);
				
				$objAddressbook->mob_no2 = cleanQuery($data[$x][7]);
				$objAddressbook->gender = cleanQuery($data[$x][8]);
				$objAddressbook->birth_date = cleanQuery($data[$x][9]);
				$objAddressbook->address = cleanQuery($data[$x][10]);
				$objAddressbook->optional_one = cleanQuery($data[$x][11]);
				$objAddressbook->optional_two = cleanQuery($data[$x][12]);
				$objAddressbook->area = cleanQuery($data[$x][13]);
				$objAddressbook->city = cleanQuery($data[$x][14]);
				$objAddressbook->country = cleanQuery($data[$x][15]);
				$objAddressbook->addContact();			
			}
			
			//Updating Count of Contact to each addressbook
			foreach($addrBookId as $addbkId){
				$objAddressbook->updateCountAddressbook($addbkId);
			}
						
			//msg for success upload
			$msg = VEN_IMPORT_CONTACTS_MSG_2_1. $_SESSION['contact']['countValid'] ;
			unset($_SESSION['contact']);					
			$smarty->assign('action','uploadFile');
		break;
		default:
			unset($_SESSION['contact']);
			$action	="uploadFile";
			$smarty->assign('action',$action);
		break;
	}
	
	//End of case ####
	// addressbook listing
	$arrAddressbook = $objAddressbook->getAddressbookList(false);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('msg',$msg);
	$smarty->display("vendor/ven_import_contacts.tpl");
	
	include 'footer.php';
?>