<?php
/****************************************************************************************************************
*	File : ven_dashboard.php
*	Purpose: landing page for vendors
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(CLASSES.'SmartyPaginate.class.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);

	// required connect
    SmartyPaginate::connect();
    // set items per page
    SmartyPaginate::setLimit($project_vars['row_per_page']);
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	$errorFlag = 0;

	if(isset($_REQUEST['action']) && $_REQUEST['action']=='add'){
		
		if($_REQUEST['addressbook']!=''){
		
			$objAddressbook-> addressbook = $_REQUEST['addressbook'];
			$objAddressbook-> description = $_REQUEST['description'];
			$status = $objAddressbook-> insertAddressbook();
			
			if($status==1){
			
				$msg = VEN_DASHBOARD_MSG_1;
			}elseif($status==2){
				
				$msg = VEN_DASHBOARD_MSG_2;
				$errorFlag = 1;

			}elseif($status==2){
				
				$msg = VEN_DASHBOARD_MSG_3;
				$errorFlag = 1;
				$smarty->assign('addressbook',$_REQUEST['addressbook']);
				$smarty->assign('discription',$_REQUEST['description']);
			}
		}else{

			$msg = VEN_DASHBOARD_MSG_4;
			$errorFlag = 1;
		}
	
	}elseif(isset($_REQUEST['action']) && $_REQUEST['action']=='delete'){
		
		$objAddressbook->addressbookId = $_REQUEST['addbookId'];
		if($objAddressbook->deleteAddressbook()){
		
			$msg = VEN_DASHBOARD_MSG_5;

		}
	}

	// addressbook listing
	$arrAddressbook = $objAddressbook->getAddressbookList();
    
	$smarty->assign('msg',$msg);
	
	$smarty->assign('manageAddbookUrl',BASE_URL.'ven_addbook_details.php');
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('sendSmsUrl',BASE_URL.'ven_smspush.php');
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_addressbook.tpl");
	SmartyPaginate::disconnect();

	include 'footer.php';
?>