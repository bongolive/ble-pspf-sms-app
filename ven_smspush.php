<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: sending sms for vendors
*	Author : Leonard nyirenda
****************************************************************************************************
*/ 
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'user.class.php');
	include(MODEL.'sms/sms.class.php');
	include(MODEL.'senderid/senderid.class.php');
	include(MODEL.'credit/credit.class.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(MODEL.'sms/smstemplate.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);

	//SmartyPaginate::connect();

	$objUser= new user();
	$objUser->userId = $_SESSION['user']['id'];

	$objSenderid = new senderid();
	$objSenderid-> login_id = $_SESSION['user']['id'];
	$arrSenderid = $objSenderid-> getActiveSenderid();
	$smarty->assign('arrSenderid',$arrSenderid);

	$objSms = new sms();
	$objSms->userId = $_SESSION['user']['id'];
	
	$objCredit = new credit();
	$objCredit->userId = $_SESSION['user']['id'];
	$crdBalance = $objCredit->getCreditBalance();
	
	$objAddbook = new addressbook();
	$objAddbook->userId = $_SESSION['user']['id'];
	$arrAddressbook = $objAddbook->getAddressbookList($limit=false);
	
	$objsmstemplate=new smstemplate();
	$objsmstemplate->vendor_id=$_SESSION['user']['id'];
	$arrsmstemplate=$objsmstemplate->getvendorsmstemplatelist2();
	
	switch($_REQUEST['action']){
		case 'selectContact':
			if(isset($_REQUEST['addressbooks']) && count($_REQUEST['addressbooks'])>0){
				$_SESSION['sms']['addressbooks'] = array_unique($_REQUEST['addressbooks']);
			}else{
				$_SESSION['sms']['addressbooks'] = '';
			}

			if(isset($_REQUEST['contacts']) && count($_REQUEST['contacts'])>0){
				$_SESSION['sms']['contacts']	= $_REQUEST['contacts'];
			}else{
				$_SESSION['sms']['contacts']	= '';
			}
			
			if($_SESSION['sms']['contacts'] == '' && $_SESSION['sms']['addressbooks'] == ''){
				$msg = VEN_SMS_PUSH_MSG_06;
				$errorFlag = 1;
			}else{
				//Checking if obile number exists for selected groups or contacts
				if($_SESSION['sms']['addressbooks'] != ''){
					$addressbookIds = implode(',',$_SESSION['sms']['addressbooks']);
					$objAddbook->addressbookId = $addressbookIds;
				}
				
				if($_SESSION['sms']['contacts'] != ''){
					$contactIds = implode(',',$_SESSION['sms']['contacts']);
					$objAddbook->contactId	= $contactIds;
				}
				
				$arrValidContacts = $objAddbook->getAllMobiles();
				if(count($arrValidContacts)>0){
					$smarty->assign('action',"createMessage");
					$smarty->assign('textMessage', $_SESSION['sms']['textMessage']);
					$smarty->assign('senderid', $_SESSION['sms']['senderid']);
					$smarty->assign('scheduleDate',$_SESSION['sms']['scheduleDate']);
				}else{
					if($_SESSION['sms']['addressbooks'] != ''){

					$arrSelectedAddbooks =  $objAddbook->getSelectedAddressbookList(array_unique($_SESSION['sms']['addressbooks']));
					$arrAddressbookTemp = array();
					foreach($arrAddressbook as $book){
						if(!in_array($book,$arrSelectedAddbooks)){
							array_push($arrAddressbookTemp,$book);
						}
					}
					$arrAddressbook = $arrAddressbookTemp;
			}
				
				if($_SESSION['sms']['contacts'] !=''){
					$arrSelectedContacts =  $objAddbook->getSelectedContacList(array_unique($_SESSION['sms']['contacts']));
				}

				$smarty->assign('arrSelectedAddbooks',$arrSelectedAddbooks);
				$smarty->assign('arrSelectedContacts',$arrSelectedContacts);
				$smarty->assign('action',"selectContact");
					$msg = VEN_SMS_PUSH_MSG_05;
					$errorFlag = 1;
				}
			}
		break;
		
		Case 'createMessage':
			if($_REQUEST['senderid']==''){
				$msg=SELECT_SENDER_NAME;
				$errorFlag = 1;
				$smarty->assign('textMessage', $_REQUEST['textMessage']);
				$smarty->assign('senderid', $_REQUEST['senderid']);
				$smarty->assign('scheduleDate', $_REQUEST['scheduleDate']);
				$smarty->assign('action','createMessage');
			}else if($_REQUEST['textMessage']==''){
				$msg=ENTER_TEXT_MASSAGE;
				$errorFlag = 1;
				$smarty->assign('textMessage', $_REQUEST['textMessage']);
				$smarty->assign('senderid', $_REQUEST['senderid']);
				$smarty->assign('scheduleDate', $_REQUEST['scheduleDate']);
				$smarty->assign('action','createMessage');
			}else if(isset($_REQUEST['scheduleDate']) && $_REQUEST['scheduleDate'] !=="" && strtotime($_REQUEST['scheduleDate'])< strtotime(date('Y-m-d H:i:s'))){
					$msg = VEN_SMS_PUSH_MSG_01;
					$errorFlag = 1;
					$smarty->assign('textMessage', $_REQUEST['textMessage']);
					$smarty->assign('senderid', $_REQUEST['senderid']);
					$smarty->assign('scheduleDate', $_REQUEST['scheduleDate']);
					$smarty->assign('action','createMessage');
			}else{
				$smarty->assign('action','SMSpreview');
				$_SESSION['sms']['textMessage']		= $_REQUEST['textMessage'];
				$_SESSION['sms']['senderid']		= $_REQUEST['senderid'];
				$_SESSION['sms']['scheduleDate']	= $_REQUEST['scheduleDate'];
				if($_REQUEST['repeat2']!=''){
					$_SESSION['sms']['repeat']= cleanQuery($_REQUEST['repeat2']);
					$_SESSION['sms']['repeat_every']= cleanQuery($_REQUEST['repeat_every2']);
					$_SESSION['sms']['repeat_days']= cleanQuery($_REQUEST['repeat_days2']);
					$_SESSION['sms']['end_date']= cleanQuery($_REQUEST['end_date']);
					$_SESSION['sms']['reminder_type']= cleanQuery($_REQUEST['reminder_type2']);
					$_SESSION['sms']['birthdayfield']= cleanQuery($_REQUEST['birthdayfield']);
				}
				
				if($_SESSION['sms']['addressbooks'] != ''){
					$addressbookIds = implode(',',$_SESSION['sms']['addressbooks']);
					$objAddbook->addressbookId = $addressbookIds;
				}
				
				if($_SESSION['sms']['contacts'] != ''){
					$contactIds = implode(',',$_SESSION['sms']['contacts']);
					$objAddbook->contactId	= $contactIds;
				}
				
				$arrValidContacts = $objAddbook->getAllMobiles();
				$mobileCount = count($arrValidContacts);
				
				//updating credit only for parent id
				if($_SESSION['user']['parent_ven_id']==''){
					(int) $remoteCredit = getRemoteCreditBalance();
					(int) $resCredit = getTotalCreditOfAllVendors();
					$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
				} 
				
				$objCredit = new credit();
				$objCredit->userId = $_SESSION['user']['id'];
				$creditBalance = $objCredit->getCreditBalance();
				
				$objSenderid->id = $_SESSION['sms']['senderid'];
				$SenderName	=	$objSenderid->requestSenderName();
				
				//finding duplicate in contacts provided
				$arrSampleValidMob= array();
				$arrValidMob= array();
				$arrDuplicatePhone =array();
				foreach($arrValidContacts as $row){
					if(in_array($row['mob_no'], $arrSampleValidMob, true)){
						array_push($arrDuplicatePhone,$row);
					}else{
						array_push($arrSampleValidMob,$row['mob_no']);
						array_push($arrValidMob,$row);
					}
				}
				
				
				$_SESSION['sms']['arrValidMob'] =$arrValidMob;
					$counter=0;
				foreach($arrValidMob as $rowMobilePhone){
					$phone = $rowMobilePhone['mob_no'];
					$objAddbook->contactId = $rowMobilePhone['id'];
					$objAddbook->mob_no=$phone;
					$arrContactDetail=$objAddbook->getContactDetailByPhone();
					$userSMS = $_SESSION['sms']['textMessage'];
					$userSMS = str_replace('\r\n','', $userSMS);
					$userSMS = str_replace('\n\r','', $userSMS);
					
					//replacing Values from sms to actual values from database
					$userSMS	= str_replace('{mobile}',$arrContactDetail['mob_no'], $userSMS);
					$userSMS	= str_replace('{first_name}',$arrContactDetail['fname'], $userSMS);
					$userSMS	= str_replace('{last_name}',$arrContactDetail['lname'], $userSMS);
					$userSMS	= str_replace('{title}',$arrContactDetail['title'], $userSMS);
					$userSMS	= str_replace('{birth_date}',$arrContactDetail['birth_date'], $userSMS);
					$userSMS	= str_replace('{gender}',$arrContactDetail['gender'], $userSMS);
					$userSMS	= str_replace('{email}',$arrContactDetail['email'], $userSMS);
					$userSMS	= str_replace('{optional_one}',$arrContactDetail['optional_one'], $userSMS);
					$userSMS	= str_replace('{optional_two}',$arrContactDetail['optional_two'], $userSMS);
					$userSMS	= str_replace('{area}',$arrContactDetail['area'], $userSMS);
					$userSMS	= str_replace('{city}',$arrContactDetail['city'], $userSMS);
					$userSMS	= str_replace('{country}',$arrContactDetail['country'], $userSMS);
					
					
					$str=str_replace(chr(13), '', $userSMS);	
					$smsCount = ceil(strlen($str)/160);
					$charCount = strlen($str);
					$arrSMSCount[]=$smsCount;
					
					//Building sample Preview SMS
					$arrTextMessage[$counter][0]=$userSMS;
					$arrTextMessage[$counter][1]=$phone;
					$arrTextMessage[$counter][2]=$SenderName;
					$arrTextMessage[$counter][3]=$charCount;
					$arrTextMessage[$counter][4]=$smsCount;
					$arrTextMessage[$counter][5]=$arrContactDetail['birth_date'];
					
					$counter=$counter+1;
				}
				
				
				$TotalRowsSample	=	count($arrTextMessage);
				if($TotalRowsSample	== 1){
					$TotalSampleRows	=	1;
				}else if($TotalRowsSample	== 2){
					$TotalSampleRows	=	2;
				}else{
					$TotalSampleRows	=	3;
				}
				
				$arrSampleSMS = array_slice($arrTextMessage, 0, $TotalSampleRows);
				
				
				if(count($arrSMSCount)>0){
					$_SESSION['sms']['creditNeeded']   =	array_sum($arrSMSCount);
					if($_SESSION['sms']['creditNeeded'] > $crdBalance){
						$msg = VEN_SMS_PUSH_MSG_02;
						$errorFlag = 1;
					}
				}else{
					$_SESSION['sms']['creditNeeded']		=	0;
				}
				
				if($_SESSION['sms']['reminder_type']=='birthday'){
					$smarty->assign('repeat','birthday');
				}else{
					$smarty->assign('repeat','sms');
				}
				
				$smarty->assign('TotalContact',count($arrValidContacts));
				$smarty->assign('CountValidContact',count($arrValidMob));
				$smarty->assign('CountDuplicateContact',count($arrDuplicatePhone));
				$smarty->assign('CountInvalidContact',0);
				$smarty->assign('smsCount',$_SESSION['sms']['creditNeeded']);
				$smarty->assign('arrSampleSMS',$arrSampleSMS);
				$smarty->assign('credit',$crdBalance);
				$smarty->assign('smsCount',$_SESSION['sms']['creditNeeded']);
				$smarty->assign('action','SMSpreview');
				
			}	
		break;
		case 'SMSpreview':
			if($_SESSION['sms']['creditNeeded']>0){

					if($_SESSION['sms']['addressbooks'] != ''){
						$objSms->addressbookId	= implode(',',$_SESSION['sms']['addressbooks']);
					}

					if($_SESSION['sms']['contacts'] != ''){
						$objSms->contactId		= implode(',',$_SESSION['sms']['contacts']);
					}

					$textSms = str_replace('\r\n','', $_SESSION['sms']['textMessage']);
					$textSms  = str_replace('\n\r','', $textSms);
					$objSms->textMassage	=$textSms;
					$objSms->status			= 3;
					$objSms->senderID		= $_SESSION['sms']['senderid'];
					$objSms->credit			= $_SESSION['sms']['creditNeeded'];
					$objSms->massegeCount	= ceil(strlen($_SESSION['sms']['textMessage'])/160);
					$objSms->massegeLength	= strlen($_SESSION['sms']['textMessage']);
					$objSms->senderType		= 'VEN';
					$objSms->jobName	= 	cleanQuery($_REQUEST['jobname']);

					if(isset($_SESSION['sms']['scheduleDate']) && $_SESSION['sms']['scheduleDate']!=''){
						$objSms->isScheduled	= 1;
						$objSms->scheduleTime	= getDateFormat($_SESSION['sms']['scheduleDate']);
					}

					if($objSms->smsPush()){
						$objSms->blockCredit();
						//$resp=BlockRemoteCredit($_SESSION['user']['id'],$objSms->credit);
						$objSms->reminderSent=0;
						$objSms->updateBroadcastReminder();
						$smsID=$objSms->getSmsStoresID2();
						$objSms->smsStoreID=$smsID;
						
						//inserting to the reminder table reminder table
						if($_SESSION['sms']['repeat'] !='' && $_SESSION['sms']['reminder_type']=='sms'){
							if($_SESSION['sms']['repeat_days'] =='' && $_SESSION['sms']['repeat']!='WEEK'){
								$_SESSION['sms']['repeat_days']="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
							}else if($_SESSION['sms']['repeat_days'] =='' && $_SESSION['sms']['repeat'] =='WEEK'){
								$sendDate	= date("Y-m-d H:i:s");// current date
								$weekday = date('l', strtotime($sendDate));
								$_SESSION['sms']['repeat_days'] = $weekday;
							}
							
							if(isset($_SESSION['sms']['scheduleDate']) && $_SESSION['sms']['scheduleDate']!=''){
								$sendDate	= getDateFormat($_SESSION['sms']['scheduleDate']);
							}else{
								$sendDate	= date('m/d/Y H:i:s');// current date
							}
							
							if($_SESSION['sms']['end_date']==''){
								// date not specified so we make default after 10 years;
								$currentDate = date("Y-m-d H:i:s");// current date
								$dateTenYearAdded = strtotime(date("m/d/Y H:i:s", strtotime($currentDate)) . " +10 year");
								$_SESSION['sms']['end_date'] = date('m/d/Y H:i:s', $dateTenYearAdded);
							}
							
							
							$objSms->scheduleTime = getDateFormat($sendDate);
							$objSms->repeat = $_SESSION['sms']['repeat'];
							$objSms->repeat_every = $_SESSION['sms']['repeat_every'];
							$objSms->repeat_days = $_SESSION['sms']['repeat_days'];
							$objSms->end_date = getDateFormat($_SESSION['sms']['end_date']);
							$objSms->reminder_type = "sms";
							$objSms->contactId = "";
							$objSms->insertReminder();
							
							//Finding Next send date
							$objSms->updateNextSendDate();
							
						}else if($_SESSION['sms']['repeat'] !='' && $_SESSION['sms']['reminder_type']=='birthday'){
							foreach($_SESSION['sms']['arrValidMob'] as $rowMob){
								if($_SESSION['sms']['repeat_days'] =='' && $_SESSION['sms']['repeat']!='WEEK'){
									$_SESSION['sms']['repeat_days']="Sunday,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday";
								}else if($_SESSION['sms']['repeat_days'] =='' && $_SESSION['sms']['repeat'] =='WEEK'){
									$sendDate	= date("m/d/Y H:i:s");// current date
									$weekday = date('l', strtotime($sendDate));
									$_SESSION['sms']['repeat_days'] = $weekday;
								}
								
								$currentDate = date("m/d/Y H:i:s");// current date
								if($_SESSION['sms']['end_date']==''){
									// date not specified so we make default after 10 years;
									$dateTenYearAdded = strtotime(date("m/d/Y H:i:s", strtotime($currentDate)) . " +10 year");
									$_SESSION['sms']['end_date'] = date('m/d/Y H:i:s', $dateTenYearAdded);
								}
								
								//finding the start date of the reminder
								$birthdayfield=$_SESSION['sms']['birthdayfield'];
								$objAddbook->contactId = $rowMob['id'];
								$objAddbook->mob_no=$rowMob['mob_no'];
								$arrContactDetail=$objAddbook->getContactDetailByPhone();
								$dbDate = date('Y', strtotime($arrContactDetail[$birthdayfield]));
								$dateCurrent = date('Y', strtotime($currentDate));
								$dateDiff = $dateCurrent - $dbDate;
								//$dateDiff = $dateDiff - $_SESSION['sms']['repeat_every'];
								$yearToAdd = " +".$dateDiff." year";
								$sendDate = strtotime(date("m/d/Y H:i:s", strtotime($arrContactDetail[$birthdayfield])).$yearToAdd);
								
								//Put start date to sens sms at 10am
								$sendDate = date('m/d/Y', $sendDate);
								$sendDate=$sendDate." 10:0:0";
								
								//checking if send date is less than now
								$birthdayDate=strtotime($sendDate);
								$curentDate=strtotime(date("m/d/Y H:i:s"));
								if($birthdayDate<$curentDate){
									$yearToAdd = " + 1 year";
									$sendDate = strtotime(date("m/d/Y H:i:s", strtotime($sendDate)).$yearToAdd);
									$sendDate = date('m/d/Y H:i:s', $sendDate);
								}
								
								$objSms->scheduleTime = getDateFormat($sendDate);
								$objSms->repeat = $_SESSION['sms']['repeat'];
								$objSms->repeat_every = $_SESSION['sms']['repeat_every'];
								$objSms->repeat_days = $_SESSION['sms']['repeat_days'];
								$objSms->end_date = getDateFormat($_SESSION['sms']['end_date']);
								$objSms->reminder_type = "birthday";
								$objSms->contactId = $rowMob['id'];
								$objSms->insertReminder();
								
								//Finding Next send date
								//$objSms->updateNextSendDate();
								//$objSms->updateReminderProcessed();
							}
						}
						
						$objSms->smsStoreID=$smsID;
						//Disable sender clone from sending birthday reminders
						 if($_SESSION['sms']['repeat'] !='' && $_SESSION['sms']['reminder_type']=='birthday'){
						 	$objSms->UpdateIsProccessedForBirthday();
							$objSms->ReturnReminderCredit();
						 }else{
						 	$objSms->UpdateIsProccessed2();
						 }
						
						if(isset($_SESSION['sms']['scheduleDate']) && $_SESSION['sms']['scheduleDate']!=''){
							$msg = VEN_SMS_PUSH_MSG_03;
						}else{
							$msg = VEN_SMS_PUSH_MSG_04;
						}
						unset($_SESSION['sms']);
					}
			}else{
				$msg = VEN_SMS_PUSH_MSG_05;
				$errorFlag = 1;
			}
			
			$smarty->assign('action','selectContact');
		break;
		
		case 'cancel':
			unset($_SESSION['sms']);
			$smarty->assign('action',"selectContact");
		break;	
		case 'backTo2':
			$smarty->assign('textMessage', $_SESSION['sms']['textMessage']);
			$smarty->assign('senderid', $_SESSION['sms']['senderid']);
			$smarty->assign('scheduleDate',$_SESSION['sms']['scheduleDate']);
			$smarty->assign('action','createMessage');
		break;	
		
		case 'backTo1':
			if($_SESSION['sms']['addressbooks'] != ''){

					$arrSelectedAddbooks =  $objAddbook->getSelectedAddressbookList(array_unique($_SESSION['sms']['addressbooks']));
					$arrAddressbookTemp = array();
					foreach($arrAddressbook as $book){
						if(!in_array($book,$arrSelectedAddbooks)){
							array_push($arrAddressbookTemp,$book);
						}
					}
					$arrAddressbook = $arrAddressbookTemp;
			}
				
				if($_SESSION['sms']['contacts'] !=''){
					$arrSelectedContacts =  $objAddbook->getSelectedContacList(array_unique($_SESSION['sms']['contacts']));
				}
				
				$_SESSION['sms']['textMessage']		= $_REQUEST['textMessage'];
				$_SESSION['sms']['senderid']		= $_REQUEST['senderid'];
				$_SESSION['sms']['scheduleDate']	= $_REQUEST['scheduleDate'];
				
				
				$smarty->assign('arrSelectedAddbooks',$arrSelectedAddbooks);
				$smarty->assign('arrSelectedContacts',$arrSelectedContacts);
				$smarty->assign('action',"selectContact");
		break;	
		
		default:
			unset($_SESSION['sms']);
			$smarty->assign('action',"selectContact");
		break;
	}
	
	$objCredit = new credit();
	$objCredit->userId = $_SESSION['user']['id'];
	$creditBalance = $objCredit->getCreditBalance();
	
	//End of case ####
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('venCreditBalance',$creditBalance);
	$smarty->assign('mob_no_length',$project_vars['mob_no_length']);
	$smarty->assign('fname_length',$project_vars['fname_length']);
	$smarty->assign('Iname_length',$project_vars['lname_length']);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('msg',$msg);
	$smarty->assign('currentYear',date("Y")+10);
	$smarty->assign('arrsmstemplate',$arrsmstemplate);
	$smarty->assign('arrSenderid',$arrSenderid);
	$smarty->display("vendor/ven_smspush.tpl");
	
	include 'footer.php';
?>