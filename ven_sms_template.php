<?php
	
	/********************************************
	*	File	: vendor_sms_template.php		*
	*	Purpose	: Vendor template SMS Management		*
	*	Author	: Leonard Nyirenda						*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	//require_once(MODEL.'sms/sms.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');
	require_once(MODEL.'sms/smstemplate.class.php');
	include(CLASSES.'SmartyPaginate.class.php');


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//foreach($_REQUEST as $key=>$value){
	//	$_REQUEST[$key] = trim($value);
	//}
	
	$selfUrl = BASE_URL.'ven_sms_template.php';

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	//$objSms  = new sms();
	$objsmstemplate  = new smstemplate();
	$objsmstemplate-> vendor_id = $_SESSION['user']['id'];
	

		$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	
	if(isset($_REQUEST['action'])){
		$action =$_REQUEST['action'];
		if($action=="edit"){
			$objsmstemplate-> templateid = $_REQUEST['templateid'];
			$arrsmstemp=$objsmstemplate->getsmstemplatebyid();
		} else if($action=="delete"){
			
			$objsmstemplate-> templateid = $_REQUEST['templateid'];
			//deleting sms
			$objsmstemplate->DeleteMessageTemplate();
			$action=="";
		}
		
	}
	
	switch($action){
	
		case 'save':
			
			$objsmstemplate-> vendor_id		= $_SESSION['user']['id']; 
			$objsmstemplate-> message		= $_REQUEST['textMessage'];
			$objsmstemplate->sms_title = $_REQUEST['sms_title'];
			
			// Inserting data to the database
			$status =  $objsmstemplate->AddMessageTemplate();
		break;
		
		case 'update':
			$objsmstemplate->templateid = cleanQuery($_REQUEST['templateid']); 
			$objsmstemplate->vendor_id	= cleanQuery($_SESSION['user']['id']); 
			$objsmstemplate->message	= cleanQuery($_REQUEST['textMessage']);
			$objsmstemplate->sms_title	= cleanQuery($_REQUEST['sms_title']);
			
			// updating data to the database
			$status =  $objsmstemplate->UpdateMessageTemplate();
			$action = "";
		break;
		
		case 'delete':
			$action = "";
		break;
		default:
			$action = "";	
	}

	$arrsmstemplate=$objsmstemplate->getvendorsmstemplatelist();
	if(isset($_GET['action'])){
		$action =$_GET['action'];
		if($action=="edit"){
			$objsmstemplate-> templateid = $_GET['templateid'];
			$arrsmstemp=$objsmstemplate->getsmstemplatebyid();
		}else if($action=="delete"){
			$action="";
		}
	}
	
	$smarty->assign('currentYear',date("Y")+79);
	$smarty->assign("arrsmstemp",$arrsmstemp);
	$smarty->assign("templateid",$_REQUEST['templateid']);
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("ajaxUrl",BASE_URL.'ven_sms_template_ajax.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrsmstemplate",$arrsmstemplate);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_sms_template.tpl");
	SmartyPaginate::disconnect();
	include 'footer.php';

?>
