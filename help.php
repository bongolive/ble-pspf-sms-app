<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('vendor/help.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/aboutus.tpl'))
		{
			$smarty->display('vendor/lang_'.$_SESSION['lang'].'/help.tpl');
		}
		else
		{
			$smarty->display('vendor/help.tpl');
		}
	}
	
	include('footer.php');
?>