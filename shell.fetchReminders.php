<?php
/****************************************************************************************************************
*	File : shell.fetchReminders.php
*	Purpose: Fetch for aactive reminder and update the sms_stores table and inbound_mesages table
*	Author : Leonard Nyirenda
*****************************************************************************************************************/

include_once ('bootstrap.php');
include_once(LIB_DIR . "shell.inc.php");
include(MODEL.'sms/sms.class.php');
include(MODEL.'credit/credit.class.php');
include_once(MODEL . "addressbook/addressbook.class.php");

$objSms = new sms();
$arrSms = $objSms->fetchActiveReminders();
 
$objCredit = new credit();
 
 //finding today day name
$sendDate	= date("Y-m-d H:i:s");// current date
$weekday = date('l', strtotime($sendDate)); 
 
 if($arrSms != false){
 	//echo "Data found now proccessing<br />";
 	foreach($arrSms as $rowReminder){
	
		switch($rowReminder['reminder_type']){
	
			case 'sms':
				//Put days in sms stores into an array
				$repeatDays='';
				unset($repeatDays);
				$repeatDays=explode(',',$rowReminder['repeat_days']);
				if(in_array($weekday, $repeatDays, true)){
					//echo "Reminder sending reminder please wait...<br />";
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					 $arrSmsDetails = $objSms->getSmsBySmsStoreId();
					
					//checking credit balance of the vendor
					$objCredit->userId = $arrSmsDetails['sent_by'];
					$crdBalance = $objCredit->getCreditBalance();
					if($crdBalance >= $arrSmsDetails['credit']){
						//Vendor has enough credit to resend the sms
						//echo "Vendor has enough credit to resend the sms..<br />";
						$arrInboundSms = $objSms->getInboundSmsBySmsStoreId();
						if($arrInboundSms != false){
							//updating sms_store table
							$objSms->userId			= $arrSmsDetails['sent_by'];
							$objSms->status			= 0;
							$objSms->textMassage	= $arrSmsDetails['text_message'];
							$objSms->senderID		= $arrSmsDetails['sender_id'];
							$objSms->credit			= $arrSmsDetails['credit'];
							$objSms->massegeCount	= $arrSmsDetails['message_count'];
							$objSms->massegeLength	= $arrSmsDetails['message_length'];
							$objSms->addressbookId	=	'REMINDER';
							$objSms->contactId	=	'REMINDER';
							$objSms->senderType		= 'VEN';
							//echo "updating sms_stores table please wait...<br />";
							if($objSms->smsPush()){
								$objSms->blockCredit();
								$smsID=$objSms->getSmsStoresID2();
								$objSms->smsStoreID=$smsID;
					
								//updating inbound table
								//echo "Now Updating Inbound sms table <br />";
								foreach($arrInboundSms as $rowInbound){
									$objSms->textMassage	= $rowInbound['short_message'];
									$objSms->massegeCount	= $rowInbound['msg_count'];
									$objSms->massegeLength	= $rowInbound['msg_length'];
									$objSms->mob_no			= $rowInbound['destination_addr'];
									$objSms->senderName = $rowInbound['source_addr'];
									$objSms->insertIntoInboundMessage2();
								}
					
							}
						}
					}
					
					//Updating next send date
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					$objSms->smsId = $rowReminder['id'];
					$objSms->updateNextSendDate();
					$objSms->updateReminderStatus();
				}else{
					//Updating next send date
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					$objSms->smsId = $rowReminder['id'];
					$objSms->updateNextSendDate();
					$objSms->updateReminderStatus();
				}
			break;
			
			case 'birthday':
				//Put days in sms stores into an array
				$repeatDays='';
				unset($repeatDays);
				$repeatDays=explode(',',$rowReminder['repeat_days']);
				if(in_array($weekday, $repeatDays, true)){
					//echo "Sending reminder please wait...<br />";
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					$arrSmsStoreDetails = $objSms->getSmsBySmsStoreId();
					
					//echo "Now fetching info please wait <br />";
					if($arrSmsStoreDetails['contact_id']=='FILE'){
						$objSms->mob_no=$rowReminder['contact_id'];
						$arrSmsDetails = $objSms->getInboundSmsBySmsStoreIdAndMobNo();
					}else{
						$objAddressbook = new addressbook();
						$objAddressbook->contactId=$rowReminder['contact_id'];
						$mobNo=$objAddressbook->getContactMobileNo();
						$objSms->mob_no=$mobNo['mob_no'];
						$arrSmsDetails = $objSms->getInboundSmsBySmsStoreIdAndMobNo();	
					}
					
					
					//checking credit balance of the vendor
					$objCredit->userId = $arrSmsStoreDetails['sent_by'];
					$crdBalance = $objCredit->getCreditBalance();
					//echo $arrSmsDetails['msg_count'];
					if($crdBalance >= $arrSmsDetails['msg_count']){
						//Vendor has enough credit to resend the sms
						//echo "Vendor has enough credit to resend the sms..<br />";
							//updating sms_store table
							$objSms->userId = $arrSmsDetails['user_id'];
							$objSms->status			= 0;
							$objSms->textMassage	= $arrSmsDetails['short_message'];
							$objSms->senderID		= $arrSmsStoreDetails['sender_id'];
							$objSms->credit			= $arrSmsDetails['msg_count'];
							$objSms->massegeCount	= $arrSmsDetails['msg_count'];
							$objSms->massegeLength	= $arrSmsDetails['msg_length'];
							$objSms->addressbookId	=	'REMINDER';
							$objSms->contactId	=	'REMINDER';
							$objSms->senderType		= 'VEN';
							//echo "updating sms_stores table please wait...<br />";
							if($objSms->smsPush()){
								$objSms->blockCredit();
								$smsID=$objSms->getSmsStoresID2();
								$objSms->smsStoreID=$smsID;
					
								//updating inbound table
								//echo "Now Updating Inbound sms table <br />";
									$objSms->mob_no			= $arrSmsDetails['destination_addr'];
									$objSms->senderName = $arrSmsDetails['source_addr'];
									$objSms->insertIntoInboundMessage2();
							}
					}
		
					//Updating next send date
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					$objSms->smsId = $rowReminder['id'];
					$objSms->updateNextSendDate();
					$objSms->updateReminderStatus();
				}else{
					//Updating next send date
					$objSms->smsStoreID = $rowReminder['sms_store_id'];
					$objSms->smsId = $rowReminder['id'];
					$objSms->updateNextSendDate();
					$objSms->updateReminderStatus();
				}
			break;
		} //end switch statement
	}
 }else{
 	echo "No data found now going to sleep...<br />";
 }
?>