<?php 
/****************************************************************************************************************
*	File : ven_update_smsstore.php
*	Purpose: Updating sms_store Tabble To set is_prossed equal to one
*	Author : Leonard Nyirenda
*****************************************************************************************************************/
	include_once ('bootstrap.php');
	//require_once(LIB_DIR.'inc.php');
	include_once(LIB_DIR . "shell.inc.php");
	include(MODEL.'user.class.php');
	include(MODEL.'sms/sms.class.php');
	$debug = false;

	// Infinite loop starts
	while(true) { 
		$objSms = new sms();
		$arraySmsDetails = $objSms -> getSmsDetails();
		$smsCount = count($arraySmsDetails);
	
		if($smsCount > 0){
			foreach($arraySmsDetails as $sms){
				if($sms['contact_id']==="FILE" || $sms['contact_id']==="QUICK" || $sms['contact_id']==="REMINDER"){
					// Not Scheduled SMS
					$objSms ->id =$sms['id'];
					$objSms -> smsProccessed();
				}
			}
		}
		// Infinite loop starts		
	} 
	include_once(LIB_DIR . "close.php");
?>