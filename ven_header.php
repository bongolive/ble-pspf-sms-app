<?php
	ob_start();
	$logout_url   = '';
	$username     = '';
	$welcome      = '';
	$logout       = '';
	$base_url	  = BASE_URL;
	$current_url  = $_SERVER['SCRIPT_NAME'];
	$cur_arr      = explode('/', $current_url);
	$curPage      = $cur_arr[count($cur_arr) - 1]; 
	
	//if($curPage!='ven_smspush.php'){
	//	unset($_SESSION['sms']);	
	//}
	if($curPage!='ven_smspush_quick.php'){
		unset($_SESSION['smsQuick']);	
	}
	if($curPage!='ven_smspush_file.php'){
		unset($_SESSION['smsFile']);	
	}
	
	if($curPage!='ven_import_contacts.php'){
		unset($_SESSION['contact']);	
	}
	if(isset($_SESSION['user']) && $_SESSION['user']!=''){
			 $welcome      = HEADER_WELCOME;
			 $logout       = HEADER_LOGOUT;
			 $username     = $_SESSION['user']['name'];
			 $logout_url   = BASE_URL.'logout.php';
			// $links        = get_menus($url,'menus',$cur_page,'','class=current');
			 $login       = true;
	}else{
		
		 $login       = false;
	}
	//echo $_SERVER['SCRIPT_NAME'];
	//$smarty->assign("links", $links);

	$smarty->assign("curPage", $curPage);
	$smarty->assign("base_url", $base_url);
	$smarty->assign("userType", $_SESSION['user']['user_type']); // assign user's type to tpl file
	$smarty->assign("login", $login);
	$smarty->assign("welcome", $welcome);
	$smarty->assign("logout", $logout);
	$smarty->assign("username", $username);
	$smarty->assign("logout_url", $logout_url);
	$smarty->assign("parent_ven_id", $_SESSION['user']['parent_ven_id']);
	$smarty->assign("access_incoming", $_SESSION['user']['access_incoming']);
	$smarty->assign("access_registrations", $_SESSION['user']['access_registrations']);
	$smarty->assign("reseller", $_SESSION['user']['reseller']);
	$smarty->assign("title", '');
	$smarty->assign("crdedit", getArray('credit_balances','id','
	CAST((credit_balance) as SIGNED INTEGER)
	as  credit_balance'," where user_id = '".$_SESSION['user']['id']."'"));
	$smarty->assign("contacts", countContactOfDefault($_SESSION['user']['id']));
	$smarty->display("vendor/ven_header.tpl");
?>