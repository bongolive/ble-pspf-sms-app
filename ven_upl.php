<?php 
/*
***************************************************************************************************
*	File : ven_smspush.php
*	Purpose: Upload vendor contacts from excel files
*	Author : Leonard nyirenda
****************************************************************************************************
*/
	
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	//These setting will be used by file SMS only when script run and will not make any changes to php.ini file
	ini_set(memory_limit,'250M');
	ini_set(max_execution_time,'900');
	ini_set(max_input_time,'900');
	
	$objAddressbook = new addressbook();
	$objAddressbook->userId = $_SESSION['user']['id'];
	$errorFlag =0;
	
	switch($_REQUEST['action']){
		case 'uploadFile':
				if(isset($_FILES['file']) ){
					$f_detail = pathinfo($_FILES['file']['name']);
					if(strtolower($f_detail['extension'])== 'csv'){
						$arrRecipient = uploadMobilephoneFrom_csv($_FILES['file']['tmp_name']);
					}else if(strtolower($f_detail['extension'])== 'xls'){
						require_once 'classes/exel/PHPExcel/Reader/excel_reader.php';
   						$data = new Spreadsheet_Excel_Reader($_FILES['file']['tmp_name']);

    					for ($i = 1; $i <= $data->sheets[0]['numRows']; $i++) {
							for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
								$arrRecipient[$i-1][$j-1] = $data->sheets[0]['cells'][$i][$j];
        					}
   						}
					}else if(strtolower($f_detail['extension'])== 'txt'){
						$arrRecipient = uploadMobilephoneFrom_txt($_FILES['file']['tmp_name']);	
					}else if(strtolower($f_detail['extension'])== 'xlsx'){
						//$arrRecipient = getXLSorXLSX($_FILES['file']['tmp_name']);
						require_once "classes/exel/PHPExcel/Reader/simplexlsx.class.php";
						$xlsx = new SimpleXLSX($_FILES['file']['tmp_name'] );
	
						list($cols,) = $xlsx->dimension();
	
						$j	=	0;
						foreach( $xlsx->rows() as $k => $r) {
							for( $i = 0; $i < $cols; $i++){
			 					$arrRecipient[$j][$i] = $r[$i];
							}
							$j++;
						}	
					}
				}else if($_SESSION['contact']['recipients'] != ""){
					$arrRecipient = $_SESSION['contact']['recipients'];
				}
				 
				if($arrRecipient != ''){
					//Finding number of column of returned array
					$arrFirstRow	=array_slice($arrRecipient, 0, 1);
					$colCounter=0;
					foreach ($arrFirstRow as $value) {
    					$counter=1;
						foreach ($value as $key) {
    						$colCounter++;
							$arrCols[]	=	"VALUE".$counter;
							$counter++;
						}
					}
					
					$_SESSION['contact']['arrCols']=$arrCols;
					$_SESSION['contact']['cols']=$colCounter;
					//$_SESSION['contact']['fileuploaded']=$_REQUEST['fileuploaded'];
					$_SESSION['contact']['recipients'] = $arrRecipient;
					
					// Taking only ten Rows of an array
					$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
					if(count($arrRecipient)>0){
						$smarty->assign('action','uploadedData');
						$smarty->assign('arrColumn',$arrCols);
						$smarty->assign('arrRecipient',$arrRecipient);
					}else{
						$smarty->assign('action','uploadFile');
						$msg = VEN_SMS_PUSH_MSG_05;
						$errorFlag = 1;
					}
					
				}
		break;
		case 'uploadedData':
			
			if(isset($_REQUEST['headerRow']) && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else if($_SESSION['contact']['headerRow'] ==1 && $_REQUEST['headerRow']==1){
				$headerRow = 1;
			}else{
				$headerRow = 0;
			}
			
			if(isset($_REQUEST['mobNoColumn'])){
				$mobNoColumn	=	$_REQUEST['mobNoColumn'];
			}else if($_SESSION['contact']['mobNoColumn'] != ""){
				$mobNoColumn	=$_SESSION['contact']['mobNoColumn'];
			}
			
			//Finding mobile,fname,lname,email,title nad comment user select
			$counter =0;
			foreach ($mobNoColumn as $value) {
				if($value=='title'){
					$title=$counter;
				}else if($value=='fname'){
					$fname=$counter;
				}else if($value=='lname'){
					$lname=$counter;
				}else if($value=='mobile'){
					$mobile=$counter;
				}else if($value=='email'){
					$email=$counter;
				}else if($value=='comment'){
					$comment=$counter;
				}
				$counter++;
			}
			
			
			
			if(!isset($fname)){
				$smarty->assign('action','uploadedData');
				$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
				$smarty->assign('headerRow',$_REQUEST['headerRow']);
				$smarty->assign('arrRecipient',array_slice($_SESSION['contact']['recipients'], 0, 9));
				$msg = VEN_SMS_PUSH_MSG_07;
				$errorFlag = 1;
			}else if(!isset($mobile)){
				$smarty->assign('action','uploadedData');
				$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
				$smarty->assign('headerRow',$_REQUEST['headerRow']);
				$smarty->assign('arrRecipient',array_slice($_SESSION['contact']['recipients'], 0, 9));
				$msg = VEN_SMS_PUSH_MSG_08;
				$errorFlag = 1;
			}else{
				//assigning data to array
				$_SESSION['contact']['mobNoColumn'] =	$mobNoColumn;
				$_SESSION['contact']['headerRow']	=	$headerRow;
				
				$smarty->assign('action','addressbook');
			}
		break;
		
		case 'addressbook':
			$data = $_SESSION['contact']['recipients'];
			$totalRows = count($data);
			
			$addrBookId = $_REQUEST['addbookId'];
			$_SESSION['contact']['addrBookId']	=  $addrBookId;
			
			//Finding mobile,fname,lname,email,title nad comment user select
			$counter =0;
			foreach ($_SESSION['contact']['mobNoColumn'] as $value) {
				if($value=='title'){
					$title=$counter;
				}else if($value=='fname'){
					$fname=$counter;
				}else if($value=='lname'){
					$lname=$counter;
				}else if($value=='mobile'){
					$mobile=$counter;
				}else if($value=='email'){
					$email=$counter;
				}else if($value=='comment'){
					$comment=$counter;
				}
				$counter++;
			}
			
			
			//Build array for all valid data
			$rowCounter = 0;
			//$arrValidContact=array();
			for ($x=$_SESSION['contact']['headerRow']; $x < $totalRows; $x++){
				//validating Mobile phonr
				$Phone=CutMobileNumber($data[$x][$mobile]);
				if(phoneValidation($Phone) and $Phone!=''){
					//check duplicaate
					foreach($addrBookId as $addbookId){
						if($objAddressbook->CheckDuplicateContact($addbookId,$Phone)){
							$arrDublicate[]	=	$Phone;
						}else{
							// valid mobile phone
							$arrValidContact[][0] =$addbookId;//addressbookid
							$arrValidContact[][1] =$Phone; //mobile phone
			
							//adding title
							if(isset($title)){
								$arrValidContact[][2] =$data[$x][$title];
							}else{
								$arrValidContact[][2] ="";
							}
							
							$arrValidContact[][3] =$data[$x][$fname]; //first name
							
							//adding last name
							if(isset($lname)){
								$arrValidContact[][4] =$data[$x][$lname];
							}else{
								$arrValidContact[][4] ="";
							}
							
							
							//adding email
							if(isset($email)){
								$arrValidContact[][5] =$data[$x][$email];
							}else{
								$arrValidContact[][5] ="";
							}
							
							//adding comment
							if(isset($comment)){
								$arrValidContact[][6] =$data[$x][$comment];
							}else{
								$arrValidContact[][6] ="";
							}
							
						}
					}
				}else{
					$arrInvalid[]	=	$Phone;
				}
				$rowCounter++;
			}
			 
			//unset($_SESSION['contact']['recipients']);
			$_SESSION['contact']['valid']	=	$arrValidContact;
			$_SESSION['contact']['addrBookId'	]=	$addrBookId;
			
			$smarty->assign('TotalContact',count($data));
			$smarty->assign('CountValidContact',count($arrValidContact));
			$smarty->assign('CountInvalidContact',count($arrInvalid));
			$smarty->assign('CountDuplicateContact',count($arrDublicate));
			if(count($arrValidContact)>0){
				$smarty->assign('arrSampleSMS',array_slice($arrValidContact, 0, 5));
			}
			$smarty->assign('action','preview');
		break;
		
		case 'refresh':
			$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 1);
			if($_REQUEST['headerRow']==1){
				foreach ($arrRecipient as $row) {
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= $value;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 1, 10);
				
			}else{
				foreach ($arrRecipient as $row) {
    				$counter = 1;
					$i=0;
					foreach ($row as $value) {
    					$arrCols[$i]	= "VALUE".$counter;
						$counter++;
						$i++;
					}
				}
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
			}
			$smarty->assign('mobNoColumn',$_REQUEST['mobNoColumn']);
			$smarty->assign('headerRow',$_REQUEST['headerRow']);
			$smarty->assign('arrColumn',$arrCols);
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('action','uploadedData');
		
		break;
		
		case 'backTo1':
			$smarty->assign('fileuploaded', $_SESSION['contact']['fileuploaded']);
			$action	= "uploadFile";
			$smarty->assign('action',$action);
		break;
		case 'backTo2':
			if($_SESSION['contact']['headerRow']==1){
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 1, 10);
			}else{
				$arrRecipient = array_slice($_SESSION['contact']['recipients'], 0, 9);
			}
			
			$smarty->assign('arrRecipient',$arrRecipient);
			$smarty->assign('headerRow',$_SESSION['contact']['headerRow']);
			$smarty->assign('arrColumn',$_SESSION['contact']['arrCols']);
			$smarty->assign('action','uploadedData');
		break;		
		case 'cancel':
			unset($_SESSION['contact']);
			$smarty->assign('action','uploadFile');
		break;	
		case 'preview':
			$data	=	$_SESSION['contact']['valid'];
			$totalRows	= count($_SESSION['contact']['valid']);
			$addrBookId	=	$_SESSION['contact']['addrBookId'];
			
			
			for ($x=0; $x < $totalRows; $x++){
				$objAddressbook->addressbookId = $addrBookId;
				$objAddressbook->mob_no =$data[$x][1];
				$objAddressbook->title = $data[$x][2];
				$objAddressbook->fname = $data[$x][3];
				$objAddressbook->lname = $data[$x][4];
				$objAddressbook->email = $data[$x][5];
				$objAddressbook->customs = $data[$x][6];
				$objAddressbook->addContact();			
			}
			
			//Updating Count of Contact to each addressbook
			foreach($addrBookId as $addbkId){
				$objAddressbook->updateCountAddressbook($addbkId);
			}
						
			//msg for success upload
			$msg = VEN_IMPORT_CONTACTS_MSG_2_1. count($data) ;
			unset($_SESSION['contact']);					
			$smarty->assign('action','uploadFile');
		break;
		default:
			unset($_SESSION['contact']);
			$action	="uploadFile";
			$smarty->assign('action',$action);
		break;
	}
	
	//End of case ####
	// addressbook listing
	$arrAddressbook = $objAddressbook->getAddressbookList(false);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('msg',$msg);
	$smarty->display("vendor/ven_upl.tpl");
	
	include 'footer.php';
?>