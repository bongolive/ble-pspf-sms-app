<?php 
	
// On 6th november 2012 leonard update sql statemtent on getAllVendorDetails() so as to sort vendor list in desending order 
	class vendor{
	
		public $userId='';
		public $mobNo;
		public $password;
		public $username;
		public $name;
		public $email;
		public $step;
		public $gender;
		public $phone='';
		public $ageRangeId='';
		public $cityId='';
		public $neighborhoodId='';
		public $occupationId='';
		public $industryId='';
		public $postalAddress='';
		public $physicalAddress='';
		public $organisation='';
		public $organisationCatId='';
		public $vendorType='';
		public $termCondition='';
		public $mob_conf_msg='';
		public $mob_confirmed='';
		public $reminderSent;
		public $start_date;
		public $end_date;
		public $status;
		public $ParentVenId;
		
		public $credit_blocked;
		
	function getAllVendorDetails(){
		$arrVenList = array();
		$sqlCond = " ";
		
		if($this->userId != ''){
			$sqlCond .=" AND  id ='".$this->userId."'";
		}
		
		if($this->vendorType!=''){
			$sqlCond .=" AND  vendor_type ='".$this->vendorType."'";
		}
		
		if($this->username!=''){
			$sqlCond .=" AND username ='".$this->username."'";
		}
		
		if($this->mobNo!=''){
			$sqlCond .=" AND mob_no ='".$this->mobNo."'";
		}
		
		if($this->email!=''){
			$sqlCond .=" AND email ='".$this->email."'";
		}
		
		if($this->start_date !=''){
			if($this->end_date !=''){
			$sqlCond .=" AND created >= '".$this->start_date."' AND created <= '".$this->end_date."'";
			}else{
				$sqlCond .=" AND created = '".$this->start_date."'";
			}
		}
		
		if($this->status != ''){
			if($this->status == 'active'){
				$sqlCond .=" AND active ='1'";
			}else if($this->status == 'not active'){
				$sqlCond .=" AND active ='0'";
			}else if($this->status == 'confirmed'){
				$sqlCond .=" AND confirmed ='1'";
			}else if($this->status == 'not confirmed'){
				$sqlCond .=" AND confirmed ='0'";
			}else if($this->status == 'deleted'){
				$sqlCond .=" AND deleted ='1'";
			}
		}
		//sql for counting records
		$sqlCount = "select id from mst_vendors WHERE id !=''". $sqlCond;
		$result = mysql_query($sqlCount) or mysql_error_show($sqlCount,__FILE__,__LINE__);
		SmartyPaginate::setTotal(mysql_num_rows($result));
		
		//This is sql that return vendor list
		$sql = "select * from mst_vendors WHERE id !=''".$sqlCond;
		$sql= $sql." ORDER BY created DESC   limit ". SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
		$result1 = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		if(mysql_num_rows($result1)>0){
			while($row = mysql_fetch_assoc($result1)){
				array_push($arrVenList,$row);
			}
			return $arrVenList;
		}else{
			return false;
		}
	}
		
		function getVendorDetail(){
		
			//global $myDB;
			$sql = "SELECT v.* , l.location, o.organisation AS organisation_cat, pr.people_range
					FROM mst_vendors AS v
					LEFT JOIN locations AS l ON v.location_id = l.id
					LEFT JOIN organisations AS o ON v.organisation_id = o.id
					LEFT JOIN people_ranges AS pr on pr.id = v.people_range_id
					WHERE v.id= '".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		
		}
		
		public function updateVendorReminderStatus(){
		
			$sql = "update credit_balances set reminder_sent='".$this->reminderSent."' where user_id ='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				return true;
		}
		
		
		public function getVendorCreditBalance(){
		
			$sql = "select * FROM  credit_balances where user_id ='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				$row = mysql_fetch_assoc($result);
				return $row;
		}
		
		public function getVendorDetailsByMobile(){
			$arrVen =array();
			$sql = "select name,mob_no,url FROM  mst_vendors where mob_no ='".$this->mobNo."' limit 1";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				
			while($row = mysql_fetch_assoc($result)){
				$arrVen = $row;
			}
				return $arrVen;
		}
		
		
		public function getAllVendorsWithLowCreditBalances(){
		
			$sql = "select * FROM  credit_balances,mst_vendors where credit_balances.user_id = mst_vendors.id and credit_balances.reminder_sent='0' and (credit_balances.credit_balance-credit_balances.credit_blocked) <= credit_balances.credit_level and active='1' and deleted='0' LIMIT 0, 100 ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				//$row = mysql_fetch_array($result);
				
				return $result;
		}
	
	
	function getVenCreditBalanceByUsernameAndPass(){
			$sql="select  (credit_balances.credit_balance-credit_balances.credit_blocked) as balance from mst_vendors,credit_balances where mst_vendors.id= credit_balances.user_id and mst_vendors.username='".$this->username."' and mst_vendors.password='".$this->password."'";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if(mysql_num_rows($result)>0){
						while($row = mysql_fetch_assoc($result)){
							$Balance=$row['balance'];
							return $Balance;
						}
					}else{
						return 0;
					}
	
	}
	
	function updateVendorCredit_bolcked(){
		$sql="update credit_balances set credit_blocked=(credit_blocked + '".$this->credit_blocked."') where user_id='".$this->userId."'";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if($result ){
			return true;
		}else{
			return false;
		}
	
	}
	
	function updateVendorCreditBalance(){
		$sql="update credit_balances set credit_blocked=(credit_blocked - '".$this->credit_blocked."'),credit_blocked=(credit_blocked-'".$this->credit_blocked."') where user_id='".$this->userId."'";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		if($result ){
			return true;
		}else{
			return false;
		}
	
	}
	
	function getVenSenderIDByUsernamePass(){
		$sql="select senderids.senderid from mst_vendors,senderids where mst_vendors.id=senderids.vendor_id and senderids.status='active' and mst_vendors.username='".$this->username."' and mst_vendors.password='".$this->password."'";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
		while($row = mysql_fetch_assoc($result)){
			$arrSender[] = $row['senderid'];
		}
				
		$arrSender = implode(',',$arrSender);
		return $arrSender;

	}
	
	function getAllSubVendorDetails(){
		$arrVenList = array();
		$sqlCond = " ";
		
		if($this->userId != ''){
			$sqlCond .=" AND  mst_vendors.id ='".$this->userId."'";
		}
		
		if($this->vendorType!=''){
			$sqlCond .=" AND  mst_vendors.vendor_type ='".$this->vendorType."'";
		}
		
		if($this->username!=''){
			$sqlCond .=" AND mst_vendors.username ='".$this->username."'";
		}
		
		if($this->mobNo!=''){
			$sqlCond .=" AND mst_vendors.mob_no ='".$this->mobNo."'";
		}
		
		if($this->email!=''){
			$sqlCond .=" AND mst_vendors.email ='".$this->email."'";
		}
		
		if($this->start_date !=''){
			if($this->end_date !=''){
			$sqlCond .=" AND mst_vendors.created >= '".$this->start_date."' AND mst_vendors.created <= '".$this->end_date."'";
			}else{
				$sqlCond .=" AND mst_vendors.created = '".$this->start_date."'";
			}
		}
		
		if($this->status != ''){
			if($this->status == 'active'){
				$sqlCond .=" AND mst_vendors.active ='1'";
			}else if($this->status == 'not active'){
				$sqlCond .=" AND mst_vendors.active ='0'";
			}else if($this->status == 'confirmed'){
				$sqlCond .=" AND mst_vendors.confirmed ='1'";
			}else if($this->status == 'not confirmed'){
				$sqlCond .=" AND mst_vendors.confirmed ='0'";
			}else if($this->status == 'deleted'){
				$sqlCond .=" AND mst_vendors.deleted ='1'";
			}
		}
		//sql for counting records
		$sqlCount = "select id from mst_vendors WHERE parent_ven_id ='".$this->parentVenId."'". $sqlCond;
		$result = mysql_query($sqlCount) or mysql_error_show($sqlCount,__FILE__,__LINE__);
		SmartyPaginate::setTotal(mysql_num_rows($result));
		
		//This is sql that return vendor list
		$sql = "select mst_vendors.*,(credit_balances.credit_balance - credit_balances.credit_blocked) as credit,credit_balances.id as creditId from mst_vendors left join credit_balances on mst_vendors.id = credit_balances.user_id  WHERE mst_vendors.parent_ven_id ='".$this->parentVenId."'".$sqlCond;
		$sql= $sql." ORDER BY mst_vendors.created DESC   limit ". SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
		$result1 = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		if(mysql_num_rows($result1)>0){
			while($row = mysql_fetch_assoc($result1)){
				array_push($arrVenList,$row);
			}
			return $arrVenList;
		}else{
			return false;
		}
	}
	
	
	function getSubVendorList(){
		$arrVenList=array();
		$sql = "select * from mst_vendors WHERE parent_ven_id ='".$this->parentVenId."'";
		$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		
		if(mysql_num_rows($result)>0){
			while($row = mysql_fetch_assoc($result)){
				array_push($arrVenList,$row);
			}
			return $arrVenList;
		}else{
			return false;
		}
	}
	
}

?>