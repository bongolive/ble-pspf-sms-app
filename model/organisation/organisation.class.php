<?php
	class organisation
	{
		public $id;
		public $organisation;
		
		public $tbl_organisations	= "organisations"; // Multilanguage Fields: organisation.
		public $tbl_multilanguage	= "multilanguage";
		public $lang_default		= "en_us";
		public $lang_current		= "en_us";
		
		public function setLangCurrent($lang_current)
		{
			$this->lang_current = $lang_current;
		}

		function getListOrganisation()
		{
			$sqlPage = "SELECT count(1) AS count FROM ".$this->tbl_organisations." ORDER BY organisation;";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$arrempRanges = array();
			
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "SELECT id,organisation FROM ".$this->tbl_organisations." ORDER BY organisation ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$arrempRanges[] = $row;
				}
			}
			else
			{
				$organisations = array();
				
				$sql = "SELECT id,organisation FROM ".$this->tbl_organisations." ORDER BY organisation ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$IDs[] = $row['id'].'.organisation.'.$this->tbl_organisations;
					
					$organisations[$row['id']] = $row['organisation'];
				}
				
				$IDs = "'".implode("','", $IDs)."'";
				
				$sql = "SELECT content_id,lang,field_content FROM ".$this->tbl_multilanguage."
					WHERE content_id IN (".$IDs.") AND lang='".$this->lang_current."'
					ORDER BY field_content ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$id = explode(".", $row['content_id']);
					
					$row_ = array(
						'id'					=> $id[0],
						'organisation'			=> $row['field_content'],
						'organisation_en_us'	=> $organisations[$id[0]]
					);
					$arrempRanges[] = $row_;
				}
				
				foreach ($arrempRanges as $key => $row)
				{
					$id[$key]					= $row['id'];
					$organisation[$key]			= $row['organisation'];
					$organisation_en_us[$key]	= $row['organisation_en_us'];
				}
				array_multisort($organisation_en_us, SORT_ASC, $id, $organisation, $arrempRanges);
			}
			
			return $arrempRanges;
		}
		
		function addOrganisation($site_languages)
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$content_id = getNewUUID(); // Generation of new id
				
				/* Inserting new value content in default language_BEGIN */
				$sql = "INSERT INTO ".$this->tbl_organisations."(id,organisation,created) VALUES('".$content_id."','".$this->organisation."',NOW())";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows() == 0)
				{
					return false;
				}
				/* Inserting new value content in default language_END */
				
				/* Inserting new value content in all other languages_BEGIN */
				foreach($site_languages as $key => $value)
				{
					if(strcmp($key,$this->lang_default) != 0)
					{
						$sql = "INSERT INTO ".$this->tbl_multilanguage."(content_id,lang,field_content) VALUES('".$content_id.".organisation.".$this->tbl_organisations."','".$key."','".$this->organisation."')";
						$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
						if(mysql_affected_rows() == 0)
						{
							return false;
						}
					}
				}
				/* Inserting new value content in all other languages_END */
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function getDetailOrganisation()
		{
			$sql = "SELECT id,organisation FROM ".$this->tbl_organisations." WHERE id='".$this->id."';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			return $row = mysql_fetch_assoc($result);
		}
		
		function updateOrganisation()
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "UPDATE ".$this->tbl_organisations." SET organisation='".$this->organisation."', modified = NOW() WHERE id='".$this->id."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$sql = "UPDATE ".$this->tbl_multilanguage." SET field_content='".$this->organisation."' WHERE content_id='".$this->id.".organisation.".$this->tbl_organisations."' AND lang='".$this->lang_current."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					$sql = "UPDATE ".$this->tbl_organisations." SET modified = NOW() WHERE id='".$this->id."';";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
		
		function deleteOrganisation()
		{
			// Delete content in all other (except default language) languages
			$sql = "DELETE FROM ".$this->tbl_multilanguage." WHERE id LIKE '".$this->id.".%';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0)
			{
				/* Delete default language content_BEGIN */
				$sql = "DELETE FROM ".$this->tbl_organisations." WHERE id='".$this->id."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
				/* Delete default language content_END */
			}
			else
			{
				return false;
			}
		}
	}
?>