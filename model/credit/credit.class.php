<?php

// On 6th november 2012 leonard update sql statemtent on getRequestedCreditList()   so as to sort credit purchase in desending order 
	class credit{
	
		public $userId;
		
		public $creditRequestId; 
		public $creditRequest; 
		public $creditTotalCost;
		public $creditRequestStatus;
		public $creditRequestPaymentStatus;
		public $startCredit;
		public $endCredit;
		public $creditRate;
		public $creditReqId;
		
		public $crdtSchmId;
		public $vendorName;
		public $vendor_id;
		public $vendor_type;
		public $quantity;
		public $status;
		public $payment_status;
		public $username;
		public $startDate;
		public $endDate;
		public $parentVenId;
			

		function getCrditSchemes(){
			$arrCreditScheme = array();

			$sql = "SELECT * FROM credit_schemes ORDER BY start_range ASC";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrCreditScheme, $row);	
			}
			
			return $arrCreditScheme;

		}

		function getCreditRate(){
			$sql = "SELECT rate FROM credit_schemes WHERE id='".$this->crdtSchmId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		}

		function requestCredit(){
			$sql = "INSERT INTO credit_requests (id,credit_request_id,  	user_id,credit_scheme_id,credit_requested,total_cost,status,payment_status,created) 
			values(UUID(),'".$this->creditReqId."','".$this->userId."','".$this->crdtSchmId."','".$this->creditRequest."','".$this->creditTotalCost."','pending','0',NOW())";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				return true;
			}else{
				return false;
			}
			
		}

		function requestCreditAdm(){

			if (!empty($this->creditRequestStatus)) {
				$stutus = $this->creditRequestStatus;
			}
			else {$status = 'pending';}
			if (!empty($this->creditRequestPaymentStatus)) {
				$payment_stutus = $this->creditRequestPaymentStatus;
			}
			else {$payment_stutus = '0';}
			$sql = "INSERT INTO credit_requests (id,credit_request_id,  	user_id,credit_scheme_id,credit_requested,total_cost,status,payment_status,created) 
			values(UUID(),'".$this->creditReqId."','".$this->userId."','".$this->crdtSchmId."','".$this->creditRequest."','".$this->creditTotalCost."','".$stutus."','".$payment_stutus."',NOW())";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				if ($this->creditRequestStatus=='allocated'){
					
					//testing if user exist in the credit_balance table
					$sqlTest="select * from credit_balances where user_id = '".$this->userId."'";
					$resultTest = mysql_query($sqlTest) or mysql_error_show($sql,__FILE__,__LINE__);
					
					if(mysql_num_rows($resultTest) >0){
							$sqlUpdateCB = "UPDATE credit_balances SET credit_balance = (credit_balance+".$this->creditRequest."), modified=NOW() WHERE  user_id ='".$this->userId."'";
							$resultCB = mysql_query($sqlUpdateCB) or mysql_error_show($sqlUpdateCB,__FILE__,__LINE__);
						
							if(mysql_affected_rows()>0){
						
								return true;
							}
						}else{
							$sqlUpdateCB = "INSERT INTO credit_balances (id,user_id,credit_balance,created) VALUES(UUID(),'".$this->userId."',".$this->creditRequest.",NOW())";
							$resultCB = mysql_query($sqlUpdateCB) or mysql_error_show($sqlUpdateCB,__FILE__,__LINE__);
							if(mysql_affected_rows()>0){
						
								return true;
							}
						}
						
				}
				else {
					return true;
				}
			}else{
				return false;
			}
			
		}

		function getRequestedCreditList(){
			
			$sqlSearch ='';

			$sql_sub ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->vendor_id)){
			
				$sqlSearch .=" AND mv.vendor_id LIKE '%".$this->vendor_id."%' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->crdtSchmId)){
			
				$sqlSearch .=" AND cr.credit_scheme_id='".$this->crdtSchmId."'";
			}

			if(!empty($this->quantity)){
			
				$sqlSearch .=" AND cr.credit_requested='".$this->quantity."'";
			} 
			if(!empty($this->status)){
			
				$sqlSearch .=" AND cr.status LIKE '%".$this->status."%' ";
			}
			if(!empty($this->payment_status)){
			
				$sqlSearch .=" AND cr.payment_status='".$this->payment_status."' ";
			}
			if(!empty($this->startDate) && !empty($this->endDate)){
			
				$sqlSearch .=" AND DATE_FORMAT(cr.created,'%Y-%m-%d') BETWEEN '".$this->startDate."' AND '".$this->endDate."' ";
			}

			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}
			

			$sql = "SELECT cr.id,cr.credit_request_id,mv.name,mv.vendor_id,mv.vendor_type,cs.rate,cr.credit_requested,cr.total_cost,cr.status,cr.payment_status,cr.credit_scheme_id,cr.created,bon.rate as ratebon, bon.id as idbon FROM credit_requests as cr, mst_vendors as mv,credit_schemes AS cs,credit_bonus AS bon WHERE cr.user_id=mv.id AND (bon.id=cr.credit_scheme_id OR cs.id=cr.credit_scheme_id) ".$sqlSearch.$sql_sub."GROUP BY cr.credit_request_id";   

			$sqlPage = "SELECT COUNT(1) AS count FROM (".$sql.") as temp ";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);

			$sql= $sql." ORDER BY  cr.created DESC limit ".
				  SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrRequestedCredit[] = $row;
					}
					mysql_free_result($result);
					return $arrRequestedCredit;
			}else{
				return false;
			}
		
		
		
		}

		function getAddCreditList(){
			
			$sqlSearch ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->vendor_id)){
			
				$sqlSearch .=" AND mv.vendor_id LIKE '%".$this->vendor_id."%' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}

			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}

			$sql = "SELECT  mv.id,mv.name,mv.vendor_id,mv.vendor_type,mv.username FROM mst_vendors as mv WHERE 1 ".$sqlSearch." LIMIT ".
				SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();

			$sqlPage = "SELECT COUNT(1) AS count FROM credit_requests as cr, mst_vendors as mv,credit_schemes AS cs WHERE  cr.user_id=mv.id AND cs.id=cr.credit_scheme_id ".$sqlSearch;
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrRequestedCredit[] = $row;
					}
					mysql_free_result($result);
					return $arrRequestedCredit;
			}else{
				return false;
			}
		
		
		
		}

		function getRequestedCreditDetails(){
			$sql = "SELECT cr.id,cr.user_id,cr.credit_requested,cr.credit_request_id, cr.credit_scheme_id,cr.total_cost,cr.status,cr.payment_status,cr.created,mv.name FROM credit_requests as cr, mst_vendors as mv WHERE cr.id='".$this->creditRequestId."' and cr.user_id=mv.id";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_num_rows($result)>0){

				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		}
		
		function updateRequestedCredit2(){
			$sql = "UPDATE credit_requests SET status ='".$this->creditRequestStatus."',payment_status='".$this->creditRequestPaymentStatus."',credit_requested ='".$this->creditRequest."',credit_scheme_id ='".$this->crdtSchmId."',total_cost ='".$this->creditTotalCost."' WHERE id='".$this->creditRequestId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
		}

		function updateRequestedCredit(){
			$sql = "UPDATE credit_requests SET status ='".$this->creditRequestStatus."',payment_status='".$this->creditRequestPaymentStatus."',credit_requested ='".$this->creditRequest."',credit_scheme_id ='".$this->crdtSchmId."',total_cost ='".$this->creditTotalCost."' WHERE id='".$this->creditRequestId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0){
				if( $this->creditRequestStatus=='allocated'){
						
					$sql = "SELECT * FROM  credit_requests WHERE  id ='".$this->creditRequestId."'";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					$row = mysql_fetch_assoc($result);

					$sqlCB = "SELECT * FROM  credit_balances WHERE  user_id ='".$row['user_id']."'";
					$resultCB = mysql_query($sqlCB) or mysql_error_show($sqlCB,__FILE__,__LINE__);
					
					if(mysql_num_rows($resultCB)>0){
					
						$sqlUpdateCB = "UPDATE credit_balances SET credit_balance = (credit_balance+".$row['credit_requested']."), modified=NOW() WHERE  user_id ='".$row['user_id']."'";
						$resultCB = mysql_query($sqlUpdateCB) or mysql_error_show($sqlUpdateCB,__FILE__,__LINE__);
						
						if(mysql_affected_rows()>0){
						
							return true;
						}
							
					}else{
						
						$sqlUpdateCB = "INSERT INTO credit_balances (id,user_id,credit_balance,created) VALUES(UUID(),'".$row[user_id]."',".$row['credit_requested'].",NOW())";
						$resultCB = mysql_query($sqlUpdateCB) or mysql_error_show($sqlUpdateCB,__FILE__,__LINE__);
						
						if(mysql_affected_rows()>0){
						
							return true;
						}
					
					}
				
				}else{
				
					return true;
				}

			
			}else{

				return false;
			}
		
		}


		function addCreditScheme(){
		
			$sql = "INSERT INTO credit_schemes (id,ven_id,start_range,end_range,rate,created) VALUES(UUID(),'".$this->parentVenId."','".$this->startCredit."',".$this->endCredit.",".$this->creditRate.",NOW())";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0){
			
				return true;
			}else
				return false;
		
		}

		function addCreditBonus(){
		
			$sql = "INSERT INTO credit_bonus (id,rate,created) VALUES(UUID(),'".$this->creditRate."',NOW())";

			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0){
			
				$id = $this->getCrdtBon();
			
				return $id;
			} else
				return false;
		
		}

		function getCrdtSchmDetails(){
		
			
			$sql = "SELECT * FROM  credit_schemes WHERE id='".$this->crdtSchmId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				return mysql_fetch_assoc($result);
			
			}
			
		
		}

		function getCrdtSchm(){
		
			
			$sql = "SELECT id FROM  credit_schemes WHERE rate='".$this->creditRate."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				return mysql_fetch_assoc($result);
			
			}

		}

		function getCrdtBon(){
		
			
			$sql = "SELECT id FROM  credit_bonus WHERE rate='".$this->creditRate."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				return mysql_fetch_assoc($result);
			
			}

		}


		function updateCreditScheme(){
		
			$sql = "UPDATE credit_schemes SET start_range='".$this->startCredit."', end_range='".$this->endCredit."', rate='".$this->creditRate."' WHERE id='".$this->crdtSchmId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){
				return true;
			}else{
				return false;
			}
		}
		function deleteCreditScheme(){
		
			$sql = "DELETE FROM  credit_schemes WHERE id='".$this->crdtSchmId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				return true;
			}else{
				return false;
			}
		
		}

		function requestedCreditStatus(){
		
			$sql = "select SUM(if(credit_requests.status='Pending',1,0)) AS pending,SUM(if(credit_requests.status='rejected',1,0)) AS rejected,SUM(if(credit_requests.status='allocated',1,0)) AS allocated FROM  credit_requests,mst_vendors where credit_requests.user_id=mst_vendors.id and mst_vendors.parent_ven_id = '' and mst_vendors.active=1";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);
		
		}
		
		function requestedSubAccCreditStatus(){
		
			$sql = "select SUM(if(credit_requests.status='Pending',1,0)) AS pending,SUM(if(credit_requests.status='rejected',1,0)) AS rejected,SUM(if(credit_requests.status='allocated',1,0)) AS allocated FROM  credit_requests,mst_vendors where credit_requests.user_id=mst_vendors.id and mst_vendors.parent_ven_id != '' and mst_vendors.active=1";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);
		
		}

		function getCreditBalance(){
		
			$sql = "select * FROM  credit_balances where user_id ='".$this->userId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				$row = mysql_fetch_assoc($result);
				return (int)($row['credit_balance']);
			}else{
				return '0';
			}
			
		
		
		}
	
	
	
		function getCreditsBalanceList(){
			
			$sqlSearch ='';

			$sql_sub ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->vendor_id)){
			
				$sqlSearch .=" AND mv.vendor_id LIKE '%".$this->vendor_id."%' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}
			

			$sql = "SELECT cb.id, mv.name, mv.vendor_id, mv.vendor_type ,cb.credit_balance, cb.credit_blocked FROM credit_balances as cb, mst_vendors as mv WHERE cb.user_id=mv.id".$sqlSearch.$sql_sub;  
			
			$sqlPage = "SELECT COUNT(1) AS count FROM (".$sql.") as temp ";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);

			$sql= $sql." limit ".
				  SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrCreditBalances[] = $row;
					}
					mysql_free_result($result);
					return $arrCreditBalances;
			}else{
				return false;
			}
		
		
		
		}


		function requestedResellerCreditStatus(){
		
			$sql = "select SUM(if(credit_requests.status='pending',1,0)) AS pending,SUM(if(credit_requests.status='rejected',1,0)) AS rejected,SUM(if(credit_requests.status='allocated',1,0)) AS allocated FROM  credit_requests,mst_vendors where credit_requests.user_id=mst_vendors.id and mst_vendors.parent_ven_id='".$this->parentVenId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);
		
		}
		
		function getCreditScheme(){
			 $sql = "Select * from credit_schemes where ven_id='".$this->parentVenId."' and start_range=(
				if(".$this->creditRequest.">(select max(end_range) from credit_schemes where ven_id='".$this->parentVenId."'),
				(select start_range from credit_schemes where  ven_id='".$this->parentVenId."' and end_range=(select max(end_range) from credit_schemes where  ven_id='".$this->parentVenId."')),
				(select min(start_range) from credit_schemes where  ven_id='".$this->parentVenId."' and end_range>=".$this->creditRequest.")))";

				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_num_rows($result)>0){
					$row = mysql_fetch_assoc($result);
					return $row;
				}else{
					return false;
				}
		}

	function insertDefaultCreditBalance(){
			$sqlUpdateCB = "INSERT INTO credit_balances (id,user_id,credit_balance,created) VALUES(UUID(),'".$this->userId."',".$this->creditRequest.",NOW())";
			$resultCB = mysql_query($sqlUpdateCB) or mysql_error_show($sqlUpdateCB,__FILE__,__LINE__);
			if(mysql_affected_rows()>0){
				return true;
			}else{
				return false;
			}
		}
		
		function getRequestedSubVenCreditList(){
			
			$sqlSearch ='';

			$sql_sub ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->crdtSchmId)){
			
				$sqlSearch .=" AND cr.credit_scheme_id='".$this->crdtSchmId."'";
			}

			if(!empty($this->quantity)){
			
				$sqlSearch .=" AND cr.credit_requested='".$this->quantity."'";
			} 
			if(!empty($this->status)){
			
				$sqlSearch .=" AND cr.status = '".$this->status."' ";
			}
			if(!empty($this->payment_status)){
			
				$sqlSearch .=" AND cr.payment_status='".$this->payment_status."' ";
			}
			if(!empty($this->startDate) && !empty($this->endDate)){
			
				$sqlSearch .=" AND DATE_FORMAT(cr.created,'%Y-%m-%d') BETWEEN '".$this->startDate."' AND '".$this->endDate."' ";
			}

			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}
			

			 $sql = "SELECT cr.id,cr.credit_request_id,mv.name,mv.vendor_id,mv.vendor_type,cs.rate,cr.credit_requested,cr.total_cost,cr.status,cr.payment_status,cr.credit_scheme_id,cr.created,bon.rate as ratebon, bon.id as idbon FROM credit_requests as cr, mst_vendors as mv,credit_schemes AS cs,credit_bonus AS bon WHERE mv.parent_ven_id ='".$this->parentVenId."' and cr.user_id=mv.id AND (bon.id=cr.credit_scheme_id OR cs.id=cr.credit_scheme_id) ".$sqlSearch.$sql_sub."GROUP BY cr.credit_request_id";   

			$sqlPage = "SELECT COUNT(1) AS count FROM (".$sql.") as temp ";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);

			$sql= $sql." ORDER BY  cr.created DESC limit ".
				  SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrRequestedCredit[] = $row;
					}
					mysql_free_result($result);
					return $arrRequestedCredit;
			}else{
				return false;
			}
		
		
		
		}
		
		
		
		function getCreditsSubVenBalanceList(){
			
			$sqlSearch =' ';

			$sql_sub =' ';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}
			

			$sql = "SELECT cb.id, mv.name, mv.vendor_id, mv.vendor_type ,cb.credit_balance, cb.credit_blocked FROM credit_balances as cb, mst_vendors as mv WHERE mv.parent_ven_id ='".$this->parentVenId."' and cb.user_id=mv.id".$sqlSearch.$sql_sub;  
			
			$sqlPage = "SELECT COUNT(1) AS count FROM (".$sql.") as temp ";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);

			$sql= $sql." limit ".
				  SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrCreditBalances[] = $row;
					}
					mysql_free_result($result);
					return $arrCreditBalances;
			}else{
				return false;
			}
		
		
		
		}
		
		
		
		
		function getAddCreditSubVenList(){
			
			$sqlSearch ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->vendor_id)){
			
				$sqlSearch .=" AND mv.vendor_id LIKE '%".$this->vendor_id."%' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username ='".$this->username."' ";
			}
			
			if(!empty($this->parentVenId)){
			
				$sqlSearch .=" AND mv.parent_ven_id ='".$this->parentVenId."' ";
			}

			if(!empty($this->userId)){
			
				$sql_sub = "AND mv.id = '".$this->userId."'";
			}

			$sql = "SELECT  mv.id,mv.name,mv.vendor_id,mv.vendor_type,mv.username FROM mst_vendors as mv WHERE 1 ".$sqlSearch." LIMIT ".
				SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();

			$sqlPage = "SELECT COUNT(1) AS count FROM credit_requests as cr, mst_vendors as mv,credit_schemes AS cs WHERE  cr.user_id=mv.id AND cs.id=cr.credit_scheme_id ".$sqlSearch;
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
					while($row = mysql_fetch_assoc($result)){
					
						$arrRequestedCredit[] = $row;
					}
					mysql_free_result($result);
					return $arrRequestedCredit;
			}else{
				return false;
			}
		
		
		}
		
		function getParentVenCXreditBalance( ){
			$sql = "select * FROM  credit_balances where user_id ='".$this->parentVenId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0) {
				$row = mysql_fetch_assoc($result);
				return (int)( $row['credit_balance'] - $row['credit_blocked'] );
			}else{
				return '0';
			}
		}
		
		function removeCreditToParentVendor(){
			$sql="UPDATE credit_balances SET credit_balance=(credit_balance-".$this->creditRequest.") WHERE user_id='".$this->parentVenId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){
				return true;
			}else{
				return false;
			}
		}
		
		function getResellerCrditSchemes(){
			$arrCreditScheme = array();
			$sql = "SELECT * FROM credit_schemes where ven_id='".$this->parentVenId."' ORDER BY start_range ASC";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){
				array_push($arrCreditScheme, $row);	
			}
			return $arrCreditScheme;
		}
	
	}


?>
