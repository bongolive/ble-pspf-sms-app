<?php
	include_once('bootstrap.php');
	include_once(LIB_DIR.'inc.php');
	 
	class Languages
	{
		private $tbl_name = 'languages';
		
		public $lang;
		public $title;
		
		public function __construct($lang='',$title='')
		{
			$this->lang		= $lang;
			$this->title	= $title;
		}
		
/* Set Model Fields_BEGIN * /
		public function setLang($title)
		{
			$this->lang = $lang;
		}
		public function setLang($title)
		{
			$this->title = trim($title);
		}
		/ * Set Model Fields_END * /
		
		/ * Get Model Fields_BEGIN * /
		public function getLang()
		{
			return $this->lang;
		}
		public function getTitle()
		{
			return $this->title;
		}
/ * Get Model Fields_END */
		
		public function add()
		{
			$sql = 'INSERT INTO '.$this->tbl_name.'(lang,title) VALUES("'.$this->lang.'","'.$this->title.'")';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function update($lang,$title)
		{
			$lang	= trim($lang);
			$title	= trim($title);
			
			if((mb_strlen($lang,"utf-8") > 0) && (mb_strlen($title,"utf-8") >0))
			{
				$sql = 'UPDATE '.$this->tbl_name.' SET lang="'.$lang.'", title="'.$title.'" WHERE lang="'.$this->lang.'";';
				
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				
				if(mysql_affected_rows()>0)
				{
					$this->lang		= $lang;
					$this->title	= $title;
				
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}

		public function delete()
		{
			$sql = 'DELETE FROM '.$this->tbl_name.' WHERE lang="'.$this->lang.'";';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				$this->__destruct();
				
				return true;
			}
			else
			{
				$this->__destruct();
				
				return false;
			}
		}
		
		public function getLangs()
		{
			$query = 'SELECT * FROM '.$this->tbl_name.' ORDER BY lang;';
			
			$result = mysql_query($query);
			
			$langs	= array();
			
			while($row = mysql_fetch_assoc($result))
			{
				$langs[$row['lang']] = $row['title'];
			}
			
			return $langs;
		}
		
/*
		public function getLangIds()
		{
			$query = 'SELECT lang FROM '.$this->tbl_name.' ORDER BY lang;';
			
			$result = mysql_query($query);
			
			$lang_ids	= array();
			
			$i = 0;
			while($row = mysql_fetch_assoc($result))
			{
				$lang_ids[$i]	= $row['lang'];
				$i++;
			}
			
			return $lang_ids;
		}
*/
		
		public function __destruct()
		{
			unset($this->lang);
			unset($this->title);
		}
	}
?>