<?php
	include_once('bootstrap.php');
	include_once(LIB_DIR.'inc.php');
	 
	class Multilanguage
	{
		private $tbl_name			= 'multilanguage';
		
		private $tbl_source			= array(
			0 => array(
				'name'		=> 'categories',
				'fields'	=> array('category')
			),
			1 => array(
				'name'		=> 'educations',
				'fields'	=> array('education_level')
			),
			2 => array(
				'name'		=> 'employments',
				'fields'	=> array('employment')
			),
			3 => array(
				'name'		=> 'industries',
				'fields'	=> array('industry')
			),
			4 => array(
				'name'		=> 'occupations',
				'fields'	=> array('occupation')
			),
			5 => array(
				'name'		=> 'organisations',
				'fields'	=> array('organisation')
			)
		);
		
		public $content_id;
		public $lang;
		public $field_content;
		
		public $locale;
		
		public function __construct()
		{
			if(isset($project_vars['locale']))
			{
				$this->locale = $project_vars['locale'];
			}
			else
			{
				$this->locale = setlocale(LC_ALL,'en_US.UTF-8');
			}
			
			$this->setLang(mb_strtolower((mb_substr($this->locale,0,mb_strpos($this->locale,'.',0,'utf8'),'utf8')),'utf8'));
		}
		
		/* Set Model Fields_BEGIN */
		public function setContentId($content_id)
		{
			$this->content_id = $content_id;
		}
		public function setLang($lang)
		{
			$this->lang = trim($lang);
		}
		public function setFiledContent($field_content)
		{
			$this->field_content = trim($field_content);
		}
		/* Set Model Fields_END */
		
		/* Get Model Fields_BEGIN */
		public function getContentId()
		{
			return $this->content_id;
		}
		public function getLang()
		{
			return $this->lang;
		}
		public function getFiledContent()
		{
			return $this->field_content;
		}
		/* Get Model Fields_END */
		
		public function add($content_id,$lang,$field_content)
		{
			$this->setContentId($content_id);
			$this->setLang($lang);
			$this->setFiledContent($field_content);
			
			$sql = 'INSERT INTO '.$this->tbl_name.'(content_id,lang,field_content) VALUES("'.$this->content_id.'","'.$this->lang.'","'.$this->field_content.'")';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function update($content_id,$lang,$fieled_content)
		{
			$this->setContentId($content_id);
			$this->setLang($lang);
			$this->setFiledContent($field_content);
			
			$sql = 'INSERT INTO '.$this->tbl_name.'(content_id,lang,field_content) VALUES('.$this->content_id.','.$this->lang.','.$this->field_content.')';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		public function delete($content_id,$lang)
		{
			$this->setContentId($content_id);
			$this->setLang($lang);
			$this->setFiledContent($field_content);
			
			$sql = 'DELETE FROM '.$this->tbl_name.' WHERE content_id="'.$this->content_id.'" AND lang="'.$this->lang.'";';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				$this->__destruct();
				
				return true;
			}
			else
			{
				$this->__destruct();
				
				return false;
			}
		}
		
		public function deleteByLang($lang)
		{
			$sql = 'DELETE FROM '.$this->tbl_name.' WHERE lang="'.$lang.'";';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				$this->__destruct();
				
				return true;
			}
			else
			{
				$this->__destruct();
				
				return false;
			}
		}
		
		public function get($content_id,$lang)
		{
			$this->setContentId($content_id);
			$this->setLang($lang);
			
			$query = 'SELECT * FROM '.$this->tbl_name.' WHERE content_id="'.$this->content_id.'" AND lang="'.$this->lang.'" LIMIT 1;';
			
			$result = mysql_query($query);

			while($row = mysql_fetch_assoc($result))
			{
				$this->setFiledContent($row['field_content']);
			}
			
			return $this->field_content;
		}
		
		public function changeLangName($lang,$lang_new)
		{
			$this->setLang($lang);
			
			$sql = 'UPDATE '.$this->tbl_name.' SET lang="'.$lang_new.'" WHERE lang="'.$this->lang.'";';
			
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_affected_rows()>0)
			{
				$this->lang		= $lang;
				$this->title	= $title;
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function __destruct()
		{
			unset($content_id);
			unset($lang);
			unset($field_content);
			unset($locale);
		}
		
		public function addNewLanguageContent($lang)
		{
			$this->setLang($lang);
			
			foreach($this->tbl_source as $t_source)
			{
				foreach($t_source['fields'] as $field)
				{
					$query = 'SELECT id,'.$field.' FROM '.$t_source['name'].' ORDER BY '.$field.' ASC;';
					
					$result = mysql_query($query);
					
					while($row = mysql_fetch_assoc($result))
					{
						$this->add($row['id'].'.'.$field.'.'.$t_source['name'], $this->lang, $row[$field]);
					}
					
					mysql_free_result($result);
				}
			}
		}
	}
?>