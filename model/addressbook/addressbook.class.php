<?php

//On 12 November 2012 Leonard update  addContact()and updateContact() functions
//By adding these new fields mob_no2,gender,address,birth_date,optional_one,optional_two,location,city and country
	class addressbook
	{
		public $tbl_addressbooks	= "addressbooks";
		public $tbl_contacts		= "contacts";
		
		public $userId;
		public $addressbook;
		public $description;
		public $addressbookId;
		public $contactId;
		public $totalCount;
		public $title;
		public $fname;
		public $lname;
		public $email;
		public $mob_no;
		
		public $mob_no2;
		public $gender;
		public $birth_date;
		public $address;
		public $optional_one;
		public $optional_two;
		public $area;
		public $city;
		public $country;
		
		
		public $customs;
		public $recInseted=0;
		public $recInvalid=0;
		public $recDuplicate=0;
		
		public function insertAddressbook()
		{
			if(!$this->checkDuplicateAddressbook())
			{
				$this->id = getNewUUID();
				$sql="INSERT INTO  ".$this->tbl_addressbooks." (id,vendor_id,addressbook,description,created)
				VALUES('".$this->id."','".$this->userId."','".$this->addressbook."','".mysql_real_escape_string($this->description)."', NOW());";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows()>0)
				{
					return 1;
				}
				else
					return 0;
			}
			else
				return 2;
		}
		
		public function checkDuplicateAddressbook()
		{
			/*
				if(isset($_SESSION['lang']) && ($_SESSION['lang'] != $this->lang_default))
				{
					//echo '<pre>+'.$_SESSION['lang'].'+</pre>';
					/* Get addressbook id_BEGIN */
						//content_id
						//lang
						//field_content
						/*$sql_pre	= "
							SELECT id FROM ".$this->tbl_addressbooks."
							WHERE vendor_id='".$this->userId."' AND addressbook=(
								SELECT cotent_id FROM ".$this->tbl_multilanguage."
								WHERE field_content='".$this->addressbook."' AND lang='".$_SESSION['lang']."' 
							)
						;";
						echo '<pre>!!'.$sql_pre.'!!</pre>';*/
						//$result_pre	= mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					/* Get addressbook id_BEGIN * /
				}
			*/
			$sql	= "SELECT * FROM ".$this->tbl_addressbooks." WHERE vendor_id='".$this->userId."' AND addressbook='".$this->addressbook."'";
			$result	= mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		function getAddressbookList($limit=true){
			/*
			$arrAddressbook = array();	
			$sql="SELECT * FROM ".$this->tbl_addressbooks." WHERE vendor_id ='".$this->userId."'" ;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows( $result)>0){
				while($row = mysql_fetch_assoc($result)){
					array_push($arrAddressbook,$row);
				}
				return $arrAddressbook;
			}else
				return false;
		*/
			
			if($limit){
			
				$query = "SELECT count(1) as total FROM ".$this->tbl_addressbooks." WHERE vendor_id ='".$this->userId."'";
				$result = mysql_query($query);
				$row = mysql_fetch_assoc($result);
				SmartyPaginate::setTotal($row['total']);
				$limitQuery = sprintf(" LIMIT %d,%d",
				SmartyPaginate::getCurrentIndex(), SmartyPaginate::getLimit());
				
			}
			$query = "SELECT * FROM ".$this->tbl_addressbooks." WHERE vendor_id ='".$this->userId."' ORDER BY addressbook  ".$limitQuery ;
			
			$result = mysql_query($query);

			while ($row = mysql_fetch_assoc($result)) {
				
				$arrAddressbook[] = $row;
			}
			
			
			mysql_free_result($result);
			
			return $arrAddressbook;
		
		}

		function deleteAddressbook(){
			
			global $myDB;
			
			foreach($this->addressbookId as $key=>$addbookId){
			
				$sql = "select addressbook from ".$this->tbl_addressbooks." where id='".$addbookId."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				$row = mysql_fetch_assoc($result);
					if($row['addressbook']=='Default' or $row['addressbook']=='default'){
						$this->addressbookId[$key]="0";
					}
			}
			
			if( count($this->addressbookId) > 0){
				
				$sql="DELETE FROM ".$this->tbl_contacts." WHERE addressbook_id IN ('".implode("','",$this->addressbookId)."')";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				

				$sql="DELETE FROM ".$this->tbl_addressbooks." WHERE id IN ('".implode("','",$this->addressbookId)."') AND vendor_id='".$this->userId."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				
				if(mysql_affected_rows($myDB)>0){
					
					return true;
				}else{
					
					return false;
				}
			}else{
				return false;
			}
			
		}

		function addContact(){
		
			global $myDB;
			$counter=0;
			if(is_array($this->addressbookId)){
				
				foreach($this->addressbookId as $addbookId){
					//$sql="INSERT INTO  ".$this->tbl_contacts."  (id,addressbook_id,mob_no,title,fname,lname,email,customs,created) values(UUID(),'$addbookId','".$this->mob_no."','".$this->title."','".$this->fname."','".$this->lname."','".$this->email."','".$this->customs."',NOW())";
					$sql="INSERT INTO ".$this->tbl_contacts."  SET 
						id=UUID(),
						addressbook_id='".mysql_real_escape_string($addbookId)."',
						mob_no='".mysql_real_escape_string($this->mob_no)."',
						title='".mysql_real_escape_string($this->title)."',
						fname='".mysql_real_escape_string($this->fname)."',
						lname='".mysql_real_escape_string($this->lname)."',
						email='".mysql_real_escape_string($this->email)."',
						mob_no2='".mysql_real_escape_string($this->mob_no2)."',
						gender='".mysql_real_escape_string($this->gender)."',
						birth_date='".mysql_real_escape_string($this->birth_date)."',
						address='".mysql_real_escape_string($this->address)."',
						optional_one='".mysql_real_escape_string($this->optional_one)."',
						optional_two='".mysql_real_escape_string($this->optional_two)."',
						area='".mysql_real_escape_string($this->area)."',
						city='".mysql_real_escape_string($this->city)."',
						country='".mysql_real_escape_string($this->country)."',
						
						created=NOW()";


					$result = mysql_query($sql);	
					if(mysql_affected_rows($myDB)>0){
						$counter +=mysql_affected_rows($myDB);
					}
				}
			
			}
			if($counter>0)
				return true;
			else
				return false;
		
		}
		function updateCountAddressbook($address_id){


			$sqlSelect = "SELECT COUNT(*) AS count FROM ".$this->tbl_contacts." WHERE addressbook_id ='".$address_id."'";
			$resCount = mysql_query($sqlSelect) or mysql_error_show($sqlSelect,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($resCount);


			$sqlUpdate = "UPDATE ".$this->tbl_addressbooks." SET contacts_count ='".$row['count']."' WHERE id ='".$address_id."' ";
			$resUpdate = mysql_query($sqlUpdate) or mysql_error_show($sqlUpdate,__FILE__,__LINE__);
			 
			if(mysql_affected_rows()>0){
					 return 1;
			}else{

				return 0;
			}
			
	
		}
		function getAddressbookName(){
			$sql = "SELECT id,addressbook FROM ".$this->tbl_addressbooks." WHERE id='".$this->addressbookId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$num=mysql_num_rows($result);
			if($num == 1){
				while($row = mysql_fetch_assoc($result)){
				
					return $row; 
					
				}
			}else{
				return false;
			}
		
		}
		
		function getAddressbookID(){
			$sql = "SELECT id FROM addressbooks WHERE  vendor_id = '".$this->userId."' and addressbook='".$this->addressbook."' ";
			$result = mysql_query($sql) ;
			$num = mysql_num_rows($result);
			
			if($num == 1){
				while($row = mysql_fetch_assoc($result)){
				
					return $row; 
					
				}
			}else{
				return false;
			}
		
		}
		
		function getDefaultAddressbook(){
			$sql = "SELECT id FROM ".$this->tbl_addressbooks." WHERE  vendor_id = '".$this->userId."' and addressbook='Default' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)==1){
				while($row = mysql_fetch_assoc($result)){
				
					return $row; 
					
				}
			}else{
				return false;
			}
		
		}

		function getAddressbookDetails($limit=true){
			$sqlCond='';
			
			if(!empty($this->mob_no)){		
				$sqlCond .= " AND mob_no like '%".$this->mob_no."%' ";
			}
			if(!empty($this->fname)){
				$sqlCond .= " AND fname like '%".$this->fname."%' "; 
			}
			if(!empty($this->lname)){
				$sqlCond .= " AND lname like '%".$this->lname."%' "; 
			}
			
			if(!empty($this->email)){
				$sqlCond .= " AND email like '%".$this->email."%' "; 
			}
			
			if(!empty($this->mob_no2)){
				$sqlCond .= " AND mob_no2 like '%".$this->mob_no2."%' "; 
			}
			
			if(!empty($this->optional_one)){
				$sqlCond .= " AND optional_one like '%".$this->optional_one."%' "; 
			}
			
			if(!empty($this->optional_two)){
				$sqlCond .= " AND optional_two like '%".$this->optional_two."%' "; 
			}
			
			if(!empty($this->area)){
				$sqlCond .= " AND area like '%".$this->area."%' "; 
			}
			if(!empty($this->city)){
				$sqlCond .= " AND city like '%".$this->city."%' "; 
			}
			if($limit){
			
				$query = "SELECT count(1) as total FROM ".$this->tbl_contacts." WHERE addressbook_id='".$this->addressbookId."'".$sqlCond;
				$result = mysql_query($query) or mysql_error_show($sql,__FILE__,__LINE__);
				$row = mysql_fetch_array($result, MYSQL_ASSOC);
				SmartyPaginate::setTotal($row['total']);
				$limitQuery = sprintf(" LIMIT %d,%d",
				SmartyPaginate::getCurrentIndex(), SmartyPaginate::getLimit());
				
			}
					



			$arrAddbookDetail= array();
			$sql = "SELECT id,CONCAT_WS(' ',title,fname,lname)AS name,mob_no,email,optional_one,gender,birth_date,mob_no2 FROM ".$this->tbl_contacts." WHERE addressbook_id='".$this->addressbookId."' ".$sqlCond.$limitQuery;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrAddbookDetail,$row);
				
			}
			if(count($arrAddbookDetail>0)){
				mysql_free_result($result);
				return $arrAddbookDetail;
			}else{
				mysql_free_result($result);
				return false;
			}
		
		}

		function deletetContact(){
			
			global $myDB;
			$sql="DELETE FROM ".$this->tbl_contacts." WHERE id IN ('".implode("','",$this->contactId)."')";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);

			if( mysql_affected_rows($myDB)>0){
				$this->updateCountAddressbook($this->addressbookId);
				return true;
			}else{
				return false;
			}
			
		
		}
		
		public function uploadCsv($file){
			global $project_vars;
			$handle = fopen($file, "r");
			$line = "";
			$comma = ",";
			$incr = 0;
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE){
				
				if($incr==0){
					if(count($data)!=5 || ($data[0]!='Mobile' ||  $data[1]!='Title' || $data[2]!='Fname' || $data[3]!='Lname' || $data[4]!='Comments')){
					
						fclose($handle);
						return false;

					
					}
				
				}


				if($incr>0){
					$data[0] = trim($data[0]);
					$phno = substr($data[0],-($project_vars['mob_no_length']));
				
					if(phoneValidation($phno)){
						foreach($this->addressbookId as $addressbook_id){
							$this->inserContactDetails($addressbook_id,$phno,trim($data[1]),trim($data[2]),trim($data[3]),trim($data[4]));	
						}
						$this->recInseted++;
					} else{
						$this->recInvalid++;
					}
				}
				$incr++;
							
			}

			fclose($handle);
			return true;

		}


		function inserContactDetails($addressbook_id,$phno,$title,$fname,$lname,$customs){
		
			global $myDB;
			$sql = "INSERT INTO ".$this->tbl_contacts." (id,addressbook_id,mob_no,title,fname,lname,customs,created) VALUES (UUID(),'".$addressbook_id."','".$phno."','".mysql_real_escape_string($title)."','".mysql_real_escape_string($fname)."','".mysql_real_escape_string($lname)."','".mysql_real_escape_string($customs)."',now())"; 
			$result = @mysql_query($sql); 
			/*
			if( mysql_affected_rows($myDB)>0)
				$this->recInseted++;
			else
				$this->recDuplicate++;
			*/
		
		}

		function getContactDetail(){
		
			$sql = "SELECT * FROM ".$this->tbl_contacts." WHERE id='".$this->contactId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
			
				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		
		
		}
		
		function getContactMobileNo(){
		
			$sql = "SELECT mob_no FROM ".$this->tbl_contacts." WHERE id='".$this->contactId."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				return $row = mysql_fetch_assoc($result);
			}else{
				return false;
			}
		
		
		}
		
		function CheckDuplicateContact($addressbookId,$mobile){
		
			$sql = "SELECT * FROM ".$this->tbl_contacts." WHERE addressbook_id='".$addressbookId."' and mob_no='".$mobile."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
			
				return true;
			}else{
				return false;
			}
		
		
		}

		function updateContact(){
			global $myDB;
			$arrContactId = array();
			$sql = "select c.id from ".$this->tbl_contacts." as c,".$this->tbl_addressbooks." as a where c.addressbook_id=a.id and a.vendor_id='".$this->userId."' and c.mob_no=(SELECT mob_no FROM ".$this->tbl_contacts." where id ='".$this->contactId."')";
			$resultC = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($resultC)>0){
				while($row = mysql_fetch_assoc($resultC)){
					array_push($arrContactId,$row['id']);
				}
			
			}
			if(count($arrContactId)>0){
			
				$contactId = implode("','",$arrContactId);
				unset($sql);
				mysql_free_result($resultC);
			}
		
			$sql="UPDATE ".$this->tbl_contacts."  SET 
			mob_no='".mysql_real_escape_string($this->mob_no)."',
			title='".mysql_real_escape_string($this->title)."',
			fname='".mysql_real_escape_string($this->fname)."',
			lname='".mysql_real_escape_string($this->lname)."',
			email='".mysql_real_escape_string($this->email)."',
			mob_no2='".mysql_real_escape_string($this->mob_no2)."',
			gender='".mysql_real_escape_string($this->gender)."',
			birth_date='".mysql_real_escape_string($this->birth_date)."',
			address='".mysql_real_escape_string($this->address)."',
			optional_one='".mysql_real_escape_string($this->optional_one)."',
			optional_two='".mysql_real_escape_string($this->optional_two)."',
			area='".mysql_real_escape_string($this->area)."',
			city='".mysql_real_escape_string($this->city)."',
			country='".mysql_real_escape_string($this->country)."',
			modified=NOW() where id IN ('".$contactId."')";
			$result = mysql_query($sql);	
			
			if($result)
				return true;
			else
				return false;
		
		}

		function getTotalContacts(){
			$arrMobile = array();
			if(is_array($this->addressbookId)){
				$sqlStr = " IN ('".implode("','",$this->addressbookId)."') ";
				
			}
			$sql = "SELECT distinct mob_no FROM ".$this->tbl_contacts." WHERE addressbook_id ".$sqlStr;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrMobile,$row['mob_no']);
			}
			return $arrMobile;
		
		}

		function getContactsOfAddbook(){
		
			$arrMobile = array();
			if(is_array($this->contactId)){
				$sqlStr = " IN ('".implode("','",$this->contactId)."') ";
				
			}
			$sql = "SELECT distinct mob_no FROM ".$this->tbl_contacts." AS c, ".$this->tbl_addressbooks." AS a WHERE a.id=c.addressbook_id AND a.addressbook='Default' and c.id ".$sqlStr;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrMobile,$row['mob_no']);
			}
			return $arrMobile;
		
		}

		
		function getAllMobiles(){
		
			$arrMob = array();

			if(!empty($this->addressbookId) && !empty($this->contactId)){
			
				$sqlStr = " WHERE addressbook_id IN ('".str_replace(',',"','",$this->addressbookId)."') OR id IN ('".str_replace(',',"','",$this->contactId)."') ";
			}elseif(!empty($this->addressbookId) && empty($this->contactId)){
			
				$sqlStr = " WHERE addressbook_id in ('".str_replace(',',"','",$this->addressbookId)."') ";
			}elseif(empty($this->addressbookId) && !empty($this->contactId)){
			
				$sqlStr = " WHERE id in ('".str_replace(',',"','",$this->contactId)."') ";
			}
			
			$sql = "SELECT DISTINCT id,mob_no FROM ".$this->tbl_contacts."  ".$sqlStr;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrMob,$row);	
			}
			return $arrMob;
		
		}

		function getDefaultContact(){
			
			$arrContact = array();
			$arrContact1 = array();
			$arrContact2 = array();

			$search = '';
			$search1 = '';
			$search2 = '';

			if(!empty($this->mob_no)) {
				$search .= " AND c.mob_no like'%".$this->mob_no."%'";
				$search1 .= "AND c.fname like'%".$this->mob_no."%'";
				$search2 .= "AND c.lname like'%".$this->mob_no."'";
			}

			$sql = "SELECT c.id,c.mob_no,c.fname,c.lname FROM ".$this->tbl_contacts." as c,".$this->tbl_addressbooks." as b where b.vendor_id='".$this->userId."' AND b.addressbook='Default' AND c.addressbook_id = b.id ".$search." order by c.fname "  ;
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrContact,$row);	
			}
			
			$sql1 = "SELECT c.id,c.mob_no,c.fname,c.lname FROM ".$this->tbl_contacts." as c,".$this->tbl_addressbooks." as b where b.vendor_id='".$this->userId."' AND b.addressbook='Default' AND c.addressbook_id = b.id ".$search1." order by c.fname "  ;
			$result1 = mysql_query($sql1) or mysql_error_show($sql1,__FILE__,__LINE__);
			while($row1 = mysql_fetch_assoc($result1)){
			
				array_push($arrContact1,$row1);	
			}
			
			
			$sql2 = "SELECT c.id,c.mob_no,c.fname,c.lname FROM ".$this->tbl_contacts." as c,".$this->tbl_addressbooks." as b where b.vendor_id='".$this->userId."' AND b.addressbook='Default' AND c.addressbook_id = b.id ".$search2." order by c.fname "  ;
			$result2 = mysql_query($sql2) or mysql_error_show($sql2,__FILE__,__LINE__);
			while($row2 = mysql_fetch_assoc($result2)){
			
				array_push($arrContact2,$row2);	
			}
			
			
			return array_merge($arrContact,$arrContact1,$arrContact2);
		}

		function getSelectedAddressbookList($arrSelectAddbook){
			if(is_array($arrSelectAddbook) && count($arrSelectAddbook)){
			
				$addbookId = implode("','",$arrSelectAddbook);
				$query = "SELECT * FROM ".$this->tbl_addressbooks." WHERE vendor_id ='".$this->userId."' AND id IN ('".$addbookId."') ORDER BY addressbook ";
				$result = mysql_query($query);
				while ($row = mysql_fetch_assoc($result)) {
					
					$arrAddressbook[] = $row;
				}
				
				
				mysql_free_result($result);
				
				return $arrAddressbook;
			}
			
		}
		
		function getContactDetailByPhone(){
			$sql = "SELECT contacts.* FROM contacts,addressbooks WHERE contacts.mob_no='".$this->mob_no."' and addressbooks.id=contacts.addressbook_id and addressbooks.vendor_id='".$this->userId."' and contacts.id='".$this->contactId."' order by created ASC limit 1";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_num_rows($result)>0){
				$row = mysql_fetch_assoc($result);
				return $row;
			}else{
				return false;
			}
		
		
		}
	
		function CutMobileNumber($mobile){
			$mobile=trim($mobile);
			if(strlen($mobile) >9){
				return substr($mobile, -9);
			}else{
				return $mobile;
			}
		
		}
		
		
		
		function getSelectedContacList($selectedContactId){
		
			if(is_array($selectedContactId) && count($selectedContactId)){
			
				$contactId = implode("','",$selectedContactId);
			
			}

			$arrContact = array();

			$sql = "SELECT c.id,c.mob_no,c.fname FROM ".$this->tbl_contacts." as c where c.id IN ('".$contactId."') order by c.fname ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){
			
				array_push($arrContact,$row);	
			}
			return $arrContact;
		
		}
		
		
		function addSubscriberContact(){
			global $myDB;
			$mob_no = mysql_real_escape_string($this->mob_no);
			$str ="INSERT INTO $this->tbl_contacts  SET  id=UUID(),        mob_no='$mob_no', issubscriber= 1,created=NOW()";
			$result  = mysql_query($str);
		
			if($result)
			{
		   		$str = "SELECT id FROM $this->tbl_contacts WHERE mob_no='$mob_no' AND issubscriber= 1 ";
		   		$result = mysql_query($str);
			
				while($item = mysql_fetch_array($result) )
				return $item['id'];
			}
			
			return false;
		}
		
		function reportGender(){
			$sql="select contacts.gender as value_x,count(contacts.gender) as value_y from contacts,addressbooks where addressbooks.id=contacts.addressbook_id and addressbooks.vendor_id='$this->userId' and addressbooks.addressbook='Default' group by value_x";
			$result = mysql_query($sql);
			//$row=mysql_fetch_assoc($result);
			return $result;
		}
		
		function reportLocation(){
			$sql="select contacts.area as value_x,count(contacts.area) as value_y from contacts,addressbooks where addressbooks.id=contacts.addressbook_id and addressbooks.vendor_id='$this->userId' and addressbooks.addressbook='Default'  group by value_x";
			$result = mysql_query($sql);
			//$row=mysql_fetch_assoc($result);
			return $result;
		}
		
		function reportAge(){
			$sql="select CEIL(DATEDIFF(NOW(),contacts.birth_date)/365) as value_x,count(contacts.id) as value_y from contacts,addressbooks where addressbooks.id=contacts.addressbook_id and addressbooks.vendor_id='$this->userId' and addressbooks.addressbook='Default' group by value_x";
			$result = mysql_query($sql);
			//$row=mysql_fetch_assoc($result);
			return $result ;
		}
		
		function getBirtday(){
			$sql="select * from contacts,addressbooks where addressbooks.id=contacts.addressbook_id and addressbooks.vendor_id='$this->userId' and DAY(contacts.birth_date)=DAY(NOW()) and month(contacts.birth_date)=month(NOW())";
			
			$result = mysql_query($sql);
			//$row=mysql_fetch_assoc($result);
			return $result;
		}

}


?>
