<?php
	class employment
	{
		public $id;
		public $employment;
		
		public $tbl_employments		= "employments"; // Multilanguage Fields: employment.
		public $tbl_multilanguage	= "multilanguage";
		public $lang_default		= "en_us";
		public $lang_current		= "en_us";
		
		public function setLangCurrent($lang_current)
		{
			$this->lang_current = $lang_current;
		}
		
		function getListEmployment()
		{
			$sqlPage = "SELECT count(1) AS count FROM ".$this->tbl_employments." ORDER BY employment;";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$arrempRanges = array();
			
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "SELECT id,employment FROM ".$this->tbl_employments." ORDER BY employment ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$arrempRanges[] = $row;
				}
			}
			else
			{
				$employments = array();
				
				$sql = "SELECT id,employment FROM ".$this->tbl_employments." ORDER BY employment ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$IDs[] = $row['id'].'.employment.'.$this->tbl_employments;
					
					$employments[$row['id']] = $row['employment'];
				}
				
				$IDs = "'".implode("','", $IDs)."'";
				
				$sql = "SELECT content_id,lang,field_content FROM ".$this->tbl_multilanguage."
					WHERE content_id IN (".$IDs.") AND lang='".$this->lang_current."'
					ORDER BY field_content ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$id = explode(".", $row['content_id']);
					
					$row_ = array(
						'id'				=> $id[0],
						'employment'		=> $row['field_content'],
						'employment_en_us'	=> $employments[$id[0]]
					);
					$arrempRanges[] = $row_;
				}
				
				foreach ($arrempRanges as $key => $row)
				{
					$id[$key]				= $row['id'];
					$employment[$key]			= $row['employment'];
					$employment_en_us[$key]	= $row['employment_en_us'];
				}
				array_multisort($employment_en_us, SORT_ASC, $id, $employment, $arrempRanges);
			}
			
			return $arrempRanges;
		}
		
		function addEmployment($site_languages)
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$content_id = getNewUUID(); // Generation of new id
				
				/* Inserting new value content in default language_BEGIN */
				$sql = "INSERT INTO ".$this->tbl_employments."(id,employment,created) VALUES('".$content_id."','".$this->employment."',NOW())";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows() == 0)
				{
					return false;
				}
				/* Inserting new value content in default language_END */
				
				/* Inserting new value content in all other languages_BEGIN */
				foreach($site_languages as $key => $value)
				{
					if(strcmp($key,$this->lang_default) != 0)
					{
						$sql = "INSERT INTO ".$this->tbl_multilanguage."(content_id,lang,field_content) VALUES('".$content_id.".employment.".$this->tbl_employments."','".$key."','".$this->employment."')";
						$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
						if(mysql_affected_rows() == 0)
						{
							return false;
						}
					}
				}
				/* Inserting new value content in all other languages_END */
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function getDetailEmployment()
		{
			$sql = "SELECT id,employment FROM ".$this->tbl_employments." WHERE id='".$this->id."';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			return $row = mysql_fetch_assoc($result);
		}
		
		function updateEmployment()
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "UPDATE ".$this->tbl_employments." SET employment='".$this->employment."', modified = NOW() WHERE id='".$this->id."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$sql = "UPDATE ".$this->tbl_multilanguage." SET field_content='".$this->employment."' WHERE content_id='".$this->id.".employment.".$this->tbl_employments."' AND lang='".$this->lang_current."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					$sql = "UPDATE ".$this->tbl_employments." SET modified = NOW() WHERE id='".$this->id."';";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
		
		function deleteEmployment()
		{
			// Delete content in all other (except default language) languages
			$sql = "DELETE FROM ".$this->tbl_multilanguage." WHERE id LIKE '".$this->id.".%';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0)
			{
				/* Delete default language content_BEGIN */
				$sql = "DELETE FROM ".$this->tbl_employments." WHERE id='".$this->id."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
				/* Delete default language content_END */
			}
			else
			{
				return false;
			}
		}
	}
?>