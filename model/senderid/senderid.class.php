<?php
// On 6th november 2012 leonard update sql statemtent on getAdmSenderidList() so as to sort senderids in desending order 
	class senderid{
	
		public $id;
		public $senderid;
		public $status;
		public $username;
		public $userid;
		public $login_id;
		public $usertype;
		public $stat;
		public $is_deleted;
		
		public $search_senderid;
		public $url;
		public $counter;
		public $op;
		public $row_page;
		public $email_to;
		public $email_from;
		public $email_subject;
		public $email_sender_name;
		public $email_from_name;
		public $email_username;

		public $vendorName;
		public $vendor_id;
		public $vendor_type;
		public $startDate;
		public $endDate;
		public $parentVenId;
	
		

		function getSenderidList(){
		
			
			$sql = "SELECT * FROM senderids WHERE vendor_id ='".$this->login_id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){

			
			$arrSenderId[] = array(	"id"=>$row['id'],
									"senderid"=>$row['senderid'],
									"status"=>$row['status'],
								);
				
			}
			
			return  $arrSenderId;
			
		}
		
		function getActiveSenderid(){
		
			
			$sql = "SELECT * FROM senderids WHERE vendor_id ='".$this->login_id."' and status='active'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row = mysql_fetch_assoc($result)){

			
			$arrSenderId[] = array(	"id"=>$row['id'],
									"senderid"=>$row['senderid'],
									);
				
			}
			if(count($arrSenderId)>0){
				return  $arrSenderId;
			}else{
				return false;
			}			
		}
		
		function requestSenderName(){
			$sql="select senderid from senderids where vendor_id='".$this->login_id."' and id='".$this->id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			while($row=mysql_fetch_assoc($result)){
				$senderid=$row['senderid'];
			}
			return $senderid;
		
		}


	
		function getSenderidDetails(){
		
			$arrSenderId = array();
			$sql = "SELECT *
						FROM senderids 
						WHERE id='".$this->id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			if($row){
				return $row;
			}else{
				return false;
			}
		}

		function getSenderidDetailsname(){
		
			$arrSenderId = array();
			$sql = "SELECT s.id, s.senderid,s.vendor_id, s.status,s.is_deleted,s.created, s.modified ,ms.username
						FROM senderids as s, mst_vendors as ms
						WHERE ms.id=s.vendor_id AND s.id='".$this->id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			if($row){
				return $row;
			}else{
				return false;
			}
		}

		/*function insertSenderid(){
			
			global $myDB;
			if($this->checkDuplicateSenderid()){
				
				return 2; // duplicate senderid for the end user
			
			}else{
				
				
				$sqlInsert = "INSERT INTO senderids (id,senderid,vendor_id,status,created) VALUES(UUID(),'".$this->senderid."','".$this->login_id."','pending',now())";
				$resultInsert =  mysql_query($sqlInsert) or mysql_error_show($sqlInsert,__FILE__,__LINE__);
				if(mysql_affected_rows($myDB)>0){
					return 1;
				}else
					return 0;

			}
		}*/

		function insertSenderid(){
			
			global $myDB;

			$id = getNewUUID();

			if($this->checkDuplicateSenderid()){
				
				return 2; // duplicate senderid for the end user
			
			}else{
				
			if ($this->status=="") $status = 'pending';
			else $status = $this->status;

				$sqlInsert = "INSERT INTO senderids (id,senderid,vendor_id,status,created) VALUES('".$id."','".$this->senderid."','".$this->login_id."','".$status."',now())";
				$resultInsert =  mysql_query($sqlInsert) or mysql_error_show($sqlInsert,__FILE__,__LINE__);

				if(mysql_affected_rows($myDB)>0){
					return $id;
				}else
					return 0;

			}
		}

		function deleteSenderid(){
			
			
			//$sql    = "UPDATE senderids SET active ='0' WHERE  id='".$this->id."'";
			$sql    = "DELETE FROM senderids WHERE  id='".$this->id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if($result){	
				
				$sql    = "DELETE FROM users_senderids WHERE  senderid_id ='".$this->id."'";
				$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
					return true;

			}else 
				return false;

		}

		function removeSenderid(){
			
				$sql    = "UPDATE senderids SET  is_deleted=$this->is_deleted, status='inactive', modified=NOW() WHERE id='$this->id'";
				$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);

				if($result)
					return 1;
				else 
					return 0;
				
		}

		function editSenderid(){
			
				$sql    = "UPDATE senderids SET  senderid='$this->senderid', modified=NOW() WHERE id='$this->id'";
				$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
					return 1;
				else 
					return 0;
				
		}	

		function updateSenderid(){
			
				$sql    = "UPDATE senderids SET  status='".$this->status."', modified=NOW() WHERE id='".$this->id."'";
				$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
					return 1;
				else 
					return 0;
				
		}	

		function updateSenderName(){
			
				$sql    = "UPDATE mst_vendors SET  username='".$this->username."', modified=NOW() WHERE id='".$this->vendor_id."'";
				$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
					return 1;
				else 
					return 0;
				
		}

		function updateSenderId_edit(){
			
				$sql1    = "UPDATE senderids SET  senderid='".$this->sender_id."', modified=NOW() WHERE id='".$this->id."'";
				$result1 =  mysql_query($sql1) or mysql_error_show($sql1,__FILE__,__LINE__);
				//$sql2   = "UPDATE mst_vendors SET  vendor_id='".$this->sender_id."', modified=NOW() WHERE id='".$this->vendor_id."'";
				//$result2 =  mysql_query($sql2) or mysql_error_show($sql2,__FILE__,__LINE__);
				// This is original condition before Update on 09/09/2012 by leonard nyirenda
				// if($result1 && $result2)
				if($result1)
					return 1;
				else 
					return 0;
				
		}			

		function checkDuplicateSenderid(){
		
			$sql = "SELECT s.id FROM senderids AS s
					WHERE
					s.senderid='".$this->senderid."'
					AND s.vendor_id = '".$this->login_id."'";
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			$row_num = mysql_num_rows($result);
			if($row_num >0)
				return true;
			else 
				return false;
		}


		function getAdmSenderidList(){
			
			$sqlSearch ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->vendor_id)){
			
				$sqlSearch .=" AND mv.vendor_id = '".$this->vendor_id."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->senderid)){
			
				$sqlSearch .=" AND s.senderid LIKE '%".$this->senderid."%' ";
			}
			if(!empty($this->status)){
			
				$sqlSearch .=" AND s.status LIKE '%".$this->status."%' ";
			}
			if(!empty($this->startDate) && !empty($this->endDate)){
			
				$sqlSearch .=" AND s.created BETWEEN '".$this->startDate."' AND '".$this->endDate."' ";
			}
			
			$sqlPage = "SELECT COUNT(1) AS count FROM senderids AS s
					LEFT JOIN mst_vendors AS mv ON s.vendor_id=mv.id WHERE s.is_deleted=0 ".$sqlSearch;
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$sql = "SELECT s.id,mv.id AS user_id,mv.name,mv.vendor_id,mv.vendor_type,s.senderid,DATE_FORMAT(s.created,'%m-%d-%Y') AS created,s.status  FROM senderids AS s
					LEFT JOIN mst_vendors AS mv ON s.vendor_id=mv.id WHERE s.is_deleted=0 ".$sqlSearch." ORDER BY s.created DESC   LIMIT  ".SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_num_rows($result) >0){
				while($row = mysql_fetch_assoc($result)){
					$arrSenderId[] = $row;
				}
				
				return $arrSenderId;
			}else{ 
				return false;
			}
		}

		function requestedCreditStatus(){
		
			$sql = "select SUM(if(status='pending',1,0)) AS pending,SUM(if(status='active',1,0)) AS active,SUM(if(status='inactive',1,0)) AS inactive FROM  senderids";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);
		
		}
		
		function requestedResellerSenderidStatus(){
		
			$sql = "select SUM(if(senderids.status='pending',1,0)) AS pending,SUM(if(senderids.status='active',1,0)) AS active,SUM(if(senderids.status='inactive',1,0)) AS inactive FROM  senderids,mst_vendors where senderids.vendor_id=mst_vendors.id and mst_vendors.parent_ven_id='".$this->parentVenId."' ";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return mysql_fetch_assoc($result);
		
		}
		
		function getSubVenSenderidList(){
			
			$sqlSearch ='';

			if(!empty($this->vendorName)){
			
				$sqlSearch .=" AND mv.id ='".$this->vendorName."' ";
			}

			if(!empty($this->username)){
			
				$sqlSearch .=" AND mv.username = '".$this->username."' ";
			}
			if(!empty($this->vendor_type)){
			
				$sqlSearch .=" AND mv.vendor_type = ".$this->vendor_type." ";
			}
			if(!empty($this->senderid)){
			
				$sqlSearch .=" AND s.senderid LIKE '%".$this->senderid."%' ";
			}
			if(!empty($this->status)){
			
				$sqlSearch .=" AND s.status LIKE '%".$this->status."%' ";
			}
			if(!empty($this->startDate) && !empty($this->endDate)){
			
				$sqlSearch .=" AND s.created BETWEEN '".$this->startDate."' AND '".$this->endDate."' ";
			}
			
			$sqlPage = "SELECT COUNT(1) AS count FROM senderids AS s
					LEFT JOIN mst_vendors AS mv ON s.vendor_id=mv.id WHERE s.is_deleted=0 and mv.parent_ven_id ='".$this->parentVenId."' ".$sqlSearch;
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$sql = "SELECT s.id,mv.id AS user_id,mv.name,mv.vendor_id,mv.username,mv.organisation,mv.vendor_type,s.senderid,DATE_FORMAT(s.created,'%m-%d-%Y') AS created,s.status  FROM senderids AS s
					LEFT JOIN mst_vendors AS mv ON s.vendor_id=mv.id WHERE s.is_deleted=0 and mv.parent_ven_id ='".$this->parentVenId."' ".$sqlSearch." ORDER BY s.created DESC   LIMIT  ".SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
			$result =  mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			if(mysql_num_rows($result) >0){
				while($row = mysql_fetch_assoc($result)){
					$arrSenderId[] = $row;
				}
				
				return $arrSenderId;
			}else{ 
				return false;
			}
		}


	}
	


?>
