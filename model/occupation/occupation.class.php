<?php
	
	class occupation
	{
		public $id;
		public $occupation;
		
		public $tbl_occupations		= "occupations"; // Multilanguage Fields: occupation.
		public $tbl_multilanguage	= "multilanguage";
		public $lang_default		= "en_us";
		public $lang_current		= "en_us";
		
		public function setLangCurrent($lang_current)
		{
			$this->lang_current = $lang_current;
		}
		
		function getListOccupation()
		{
			$sqlPage = "SELECT count(1) AS count FROM ".$this->tbl_occupations." ORDER BY occupation;";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$arrempRanges = array();
			
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "SELECT id,occupation FROM ".$this->tbl_occupations." ORDER BY occupation ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$arrempRanges[] = $row;
				}
			}
			else
			{
				$occupations = array();
				
				$sql = "SELECT id,occupation FROM ".$this->tbl_occupations." ORDER BY occupation ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$IDs[] = $row['id'].'.occupation.'.$this->tbl_occupations;
					
					$occupations[$row['id']] = $row['occupation'];
				}
				
				$IDs = "'".implode("','", $IDs)."'";
				
				$sql = "SELECT content_id,lang,field_content FROM ".$this->tbl_multilanguage."
					WHERE content_id IN (".$IDs.") AND lang='".$this->lang_current."'
					ORDER BY field_content ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$id = explode(".", $row['content_id']);
					
					$row_ = array(
						'id'				=> $id[0],
						'occupation'		=> $row['field_content'],
						'occupation_en_us'	=> $occupations[$id[0]]
					);
					$arrempRanges[] = $row_;
				}
				
				foreach ($arrempRanges as $key => $row)
				{
					$id[$key]				= $row['id'];
					$occupation[$key]			= $row['occupation'];
					$occupation_en_us[$key]	= $row['occupation_en_us'];
				}
				array_multisort($occupation_en_us, SORT_ASC, $id, $occupation, $arrempRanges);
			}
			
			return $arrempRanges;
		}
		
		function addOccupation($site_languages)
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$content_id = getNewUUID(); // Generation of new id
				
				/* Inserting new value content in default language_BEGIN */
				$sql = "INSERT INTO ".$this->tbl_occupations."(id,occupation,created) VALUES('".$content_id."','".$this->occupation."',NOW())";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows() == 0)
				{
					return false;
				}
				/* Inserting new value content in default language_END */
				
				/* Inserting new value content in all other languages_BEGIN */
				foreach($site_languages as $key => $value)
				{
					if(strcmp($key,$this->lang_default) != 0)
					{
						$sql = "INSERT INTO ".$this->tbl_multilanguage."(content_id,lang,field_content) VALUES('".$content_id.".occupation.".$this->tbl_occupations."','".$key."','".$this->occupation."')";
						$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
						if(mysql_affected_rows() == 0)
						{
							return false;
						}
					}
				}
				/* Inserting new value content in all other languages_END */
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		function getDetailOccupation()
		{
			$sql = "SELECT id,occupation FROM ".$this->tbl_occupations." WHERE id='".$this->id."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			return $row = mysql_fetch_assoc($result);
		}
		
		function updateOccupation()
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "UPDATE ".$this->tbl_occupations." SET occupation='".$this->occupation."', modified = NOW() WHERE id='".$this->id."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$sql = "UPDATE ".$this->tbl_multilanguage." SET field_content='".$this->occupation."' WHERE content_id='".$this->id.".occupation.".$this->tbl_occupations."' AND lang='".$this->lang_current."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					$sql = "UPDATE ".$this->tbl_occupations." SET modified = NOW() WHERE id='".$this->id."';";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
		
		function deleteOccupation()
		{
			// Delete content in all other (except default language) languages
			$sql = "DELETE FROM ".$this->tbl_multilanguage." WHERE id LIKE '".$this->id.".%';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0)
			{
				/* Delete default language content_BEGIN */
				$sql = "DELETE FROM ".$this->tbl_occupations." WHERE id='".$this->id."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
				/* Delete default language content_END */
			}
			else
			{
				return false;
			}
		}
	}
?>