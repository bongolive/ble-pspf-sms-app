<?php
class primkeys{
private $table = 'prim_keywords';
public $id_prim_keyword;
public $text_prim_keyword;
public $vendor_id;

function addPrimKey()
{
	$text = $this->normString($this->text_prim_keyword);
	if(!$this->checked($text))
		return 'invalid';
	$str = sprintf("INSERT INTO prim_keywords (text_prim_keywords, mst_vendors_id)VALUES('%s','%s') ",$text,$this->vendor_id);
	mysql_query($str);
	return mysql_insert_id();
}
function removePrimKey()
{
	include(MODEL.'secondkeys.class.php');
	$obj = new secondkeys();
	$obj->prim_keywords_id = $this->id_prim_keyword;
	$result = $obj->getSecondKeyById();
	if(mysql_num_rows($result)!=0)
	{
		return 'invalid';

	}
	$str = "DELETE FROM prim_keywords WHERE id_prim_keywords=$this->id_prim_keyword";
	return  mysql_query($str);
}
function editPrimKey()
{
	$text = $this->normString($this->text_prim_keyword);
	if(!$this->checked($text))
		return 'invalid';
	$str = "UPDATE $this->table SET text_prim_keywords='$text' WHERE id_prim_keywords='$this->id_prim_keyword'";
	return mysql_query($str);

}
function getAllPrim()
{
	return mysql_query("SELECT * FROM prim_keywords");
}
function getPrimByVendor()
{
	$str = sprintf("SELECT id_prim_keywords,text_prim_keywords FROM %s WHERE mst_vendors_id ='%s'",$this->table,$this->vendor_id);
	$result  = mysql_query($str);
	if(mysql_num_rows($result)>0)
	{
		while($item = mysql_fetch_array($result))
		{
			$data[ $item['id_prim_keywords'] ] = $item['text_prim_keywords'];
		}
		return $data;
	}
	return false;
}
function getPrimForCronJob()
{
	$result = mysql_query("SELECT *  FROM $this->table");
	if(mysql_num_rows($result)==0)
		return false;
	while($item = mysql_fetch_array($result) )
	{
		$data[$item['text_prim_keywords']] = ($item['id_prim_keywords']);
	}
	return $data;
}
function get_id_vendor()
{
    $str = "SELECT mst_vendors_id  FROM $this->table WHERE id_prim_keywords=$this->id_prim_keywords";
    $result = mysql_query($str);
	if(mysql_num_rows($result)==0)
		return false;
	while($item = mysql_fetch_array($result) )
	{
		return $item['mst_vendors_id'];
	}
}
function checkPrimKey($text)
{
	$str = sprintf("select * from prim_keywords WHERE UPPER(text_prim_keywords) = UPPER('%s') ",$text);
	$result  = mysql_query($str);
	if(mysql_num_rows($result)!=0)
		return false;
	return true;
}
private function normString($text)
{
	$text = mysql_real_escape_string( trim($text) );
	$text = htmlspecialchars($text);
	return  strtolower($text);
}

private function checked($text)
{
	include(MODEL.'secondkeys.class.php');
	$flag = $this->checkPrimKey($text);
	if(!$flag){
		return $flag;
	}
	else{
	$second = new secondkeys();
	$flag = $second->checkSekondKey($text);
	}
	return $flag;
}
///////////////////////////////////////////////////////////////////////
function get_id_contact_mob_num($num)
{
    $str = "SELECT id FROM  contacts WHERE mob_no= '$num'";
    //print($str);	die();
	$result = mysql_query($str);
	if(mysql_num_rows($result)==0)
	    return false;
	  while($item = mysql_fetch_array($result))
	  	return $item['id'];

}
function get_one_sender_id($id)
{
	$str = "SELECT id FROM senderids WHERE vendor_id ='$id' and status='active'";
	$result =  mysql_query($str);
    if(mysql_num_rows($result)==0)
        return false;
    while( $item = mysql_fetch_array($result) )
        {
            return $item['id'];
        }
}

}//end class

