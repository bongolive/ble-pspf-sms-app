<?php
	class category
	{
		public $id;
		public $category;
		
		public $tbl_categories		= "categories"; // Multilanguage Fields: category.
		public $tbl_multilanguage	= "multilanguage";
		public $lang_default		= "en_us";
		public $lang_current		= "en_us";
		
		public function setLangCurrent($lang_current)
		{
			$this->lang_current = $lang_current;
		}
		
		public function getListCategory()
		{
			$sqlPage = "SELECT count(1) AS count FROM ".$this->tbl_categories." ORDER BY category;";
			$result = mysql_query($sqlPage) or mysql_error_show($sqlPage,__FILE__,__LINE__);
			$row = mysql_fetch_assoc($result);
			SmartyPaginate::setTotal($row['count']);
			
			$arrempRanges = array();
			
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "SELECT id,category FROM ".$this->tbl_categories." ORDER BY category ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$arrempRanges[] = $row;
				}
			}
			else
			{
				$categories = array();
				
				$sql = "SELECT id,category FROM ".$this->tbl_categories." ORDER BY category ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$IDs[] = $row['id'].'.category.'.$this->tbl_categories;
					
					$categories[$row['id']] = $row['category'];
				}
				
				$IDs = "'".implode("','", $IDs)."'";
				
				$sql = "SELECT content_id,lang,field_content FROM ".$this->tbl_multilanguage."
					WHERE content_id IN (".$IDs.") AND lang='".$this->lang_current."'
					ORDER BY field_content ASC LIMIT ".
					SmartyPaginate::getCurrentIndex().",". SmartyPaginate::getLimit();
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				while($row = mysql_fetch_assoc($result))
				{
					$id = explode(".", $row['content_id']);
					
					$row_ = array(
						'id'				=> $id[0],
						'category'			=> $row['field_content'],
						'category_en_us'	=> $categories[$id[0]]
					);
					$arrempRanges[] = $row_;
				}
				
				foreach ($arrempRanges as $key => $row)
				{
					$id[$key]				= $row['id'];
					$category[$key]			= $row['category'];
					$category_en_us[$key]	= $row['category_en_us'];
				}
				array_multisort($category_en_us, SORT_ASC, $id, $category, $arrempRanges);
			}
			
			return $arrempRanges;
		}
		
		public function addCategory($site_languages)
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$content_id = getNewUUID(); // Generation of new id
				
				/* Inserting new value content in default language_BEGIN */
				$sql = "INSERT INTO ".$this->tbl_categories."(id,category,created) VALUES('".$content_id."','".$this->category."',NOW())";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows() == 0)
				{
					return false;
				}
				/* Inserting new value content in default language_END */
				
				/* Inserting new value content in all other languages_BEGIN */
				foreach($site_languages as $key => $value)
				{
					if(strcmp($key,$this->lang_default) != 0)
					{
						$sql = "INSERT INTO ".$this->tbl_multilanguage."(content_id,lang,field_content) VALUES('".$content_id.".category.".$this->tbl_categories."','".$key."','".$this->category."')";
						$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
						if(mysql_affected_rows() == 0)
						{
							return false;
						}
					}
				}
				/* Inserting new value content in all other languages_END */
				
				return true;
			}
			else
			{
				return false;
			}
		}
		
		public function getDetailCategory()
		{
			$sql = "SELECT id,category FROM ".$this->tbl_categories." WHERE id='".$this->id."'";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			
			return $row = mysql_fetch_assoc($result);
		}
		
		public function updateCategory()
		{
			if(strcmp($this->lang_default,$this->lang_current) == 0)
			{
				$sql = "UPDATE ".$this->tbl_categories." SET category='".$this->category."', modified = NOW() WHERE id='".$this->id."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				$sql = "UPDATE ".$this->tbl_multilanguage." SET field_content='".$this->category."' WHERE content_id='".$this->id.".category.".$this->tbl_categories."' AND lang='".$this->lang_current."';";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if($result)
				{
					$sql = "UPDATE ".$this->tbl_categories." SET modified = NOW() WHERE id='".$this->id."';";
					$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
					if($result)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					return false;
				}
			}
		}
		
		public function deleteCategory()
		{
			// Delete content in all other (except default language) languages
			$sql = "DELETE FROM ".$this->tbl_multilanguage." WHERE id LIKE '".$this->id.".%';";
			$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
			if(mysql_affected_rows()>0)
			{
				/* Delete default language content_BEGIN */
				$sql = "DELETE FROM ".$this->tbl_categories." WHERE id='".$this->id."'";
				$result = mysql_query($sql) or mysql_error_show($sql,__FILE__,__LINE__);
				if(mysql_affected_rows()>0)
				{
					return true;
				}
				else
				{
					return false;
				}
				/* Delete default language content_END */
			}
			else
			{
				return false;
			}
		}
	}
?>