<?php
	
	/********************************************
	*	File	: ven_broadcast.php		*
	*	Purpose	: Vendor Boadcast management		*
	*	Author	: Leonard Nyirenda					*
	********************************************/
	//on 12th oct 2012 Leonard nyirenda add new search parameter jobName
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'sms/sms.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$selfUrl = BASE_URL.'ven_broadcast.php';

	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);

	$objUser = new user();
	$objSms  = new sms();
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
		case'search':
			$pagingUrl = $selfUrl.'?mob_no='.$_REQUEST['mob_no']; 
			$pagingUrl.= '&sms_type='.$_REQUEST['sms_type'];
			$pagingUrl.= '&textMessage='.$_REQUEST['textMessage'];
			$pagingUrl.= '&jobName='.$_REQUEST['jobName'];  
			$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
			$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
			$pagingUrl.= '&action='.$_REQUEST['action'];
			SmartyPaginate::setUrl($pagingUrl);
			
			// stting search param
			$objSms->userId 			= $_SESSION['user']['id'];
			$objSms-> mob_no			= $_REQUEST['mob_no']; 
			$objSms-> sms_type			= $_REQUEST['sms_type']; 
			$objSms-> textMessage		= $_REQUEST['textMessage']; 
			$objSms-> jobName			= $_REQUEST['jobName'];
			$objSms-> startDate			= getDateFormat($_REQUEST['startDate'],'Y-m-d');
			$objSms-> endDate			= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
			
			// getting search sms
			$arrSms =  $objSms->getSmsList();
			
		break;
		case'edit':
				$objSms->smsId	= $_REQUEST['sid']; 
				$arrSmsToEdit =  $objSms->getSmsToReschedule();
				$smarty->assign("arrSmsToEdit",$arrSmsToEdit);
			
			break;
		case'update':
				
				$objSms->smsId			= $_REQUEST['sid']; 
				$objSms->scheduleTime	= getDateFormat($_REQUEST['scheduleDate']); 
				$objSms->rescheduleSms();
				
				$action = '';
				$objSms->userId 			= $_SESSION['user']['id'];
				$objSms-> textMessage			= $_REQUEST['textMessage']; 
				$objSms-> startDate			= getDateFormat($_REQUEST['startDate'],'Y-m-d');
				$objSms-> endDate			= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
				// getting search sms
				$arrSms =  $objSms->getSmsList();
			break;
		case'':
			
			$pagingUrl.= '?mob_no='.$_REQUEST['mob_no']; 
			$pagingUrl.= '&sms_type='.$_REQUEST['sms_type'];
			$pagingUrl.= '&textMessage='.$_REQUEST['textMessage']; 
			$pagingUrl.= '&jobName='.$_REQUEST['jobName']; 
			$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
			$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
			$pagingUrl.= '&action='.$_REQUEST['action'];
			SmartyPaginate::setUrl($pagingUrl);
			
			// stting search param
			$objSms->userId 			= $_SESSION['user']['id'];
			$objSms-> mob_no			= $_REQUEST['mob_no']; 
			$objSms-> sms_type			= $_REQUEST['sms_type']; 
			$objSms-> textMessage		= $_REQUEST['textMessage']; 
			$objSms-> jobName			= $_REQUEST['jobName']; 
			$objSms-> startDate			= getDateFormat($_REQUEST['startDate'],'Y-m-d');
			$objSms-> endDate			= getDateFormat($_REQUEST['endDate'],'Y-m-d'); 
			
			// getting search sms
			$arrSms =  $objSms->getSmsList();
			
		break;
		default:
			$action = "";
	}

	$smarty->assign("msg",$msg);
	$smarty->assign("mob_no", $_REQUEST['mob_no']);
	$smarty->assign("sms_type",$_REQUEST['sms_type']);
	$smarty->assign("type_seach",$_REQUEST['textMessage']);
	$smarty->assign("textMessage",$_REQUEST['textMessage']);
	$smarty->assign("jobName",$_REQUEST['jobName']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);

	$smarty->assign("arrVendors",getArray('mst_vendors','id','name',' where active=1 and deleted=0 '));
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("ajaxUrl",BASE_URL.'adm_broadcast_ajax.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrSms",$arrSms);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_broadcast.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
