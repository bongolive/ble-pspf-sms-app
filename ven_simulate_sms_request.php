<?php
/****************************************************************************************************************
*	File : ven_simulate_sms_request.php
*	Purpose: Simulate sms request for a entered mobile nu mber
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'primkeys.class.php');
	include(MODEL.'secondkeys.class.php');
	include(MODEL.'incoming.class.php');
	include_once(LIB_DIR.'smsProcessLib.php');
	
	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$errorFlag='0';
	$objPrimaryKey = new primkeys();
	$objSecondKeyword = new secondkeys();

	$obj = new incoming();
	
	if(isset($_REQUEST['textMessage']) && isset($_REQUEST['mob_no'])){
		if(! phoneValidation($_REQUEST['mob_no'])){
			//Error message for non valid mobile number
			$msg=VEN_SIMULATE_SMS_REQUEST_MSG_01;
			$errorFlag='1';
		}else{
			$arrsms = parsesms($_REQUEST['textMessage']);
	
			if(count($arrsms)>0){
				//search in the database to check if primary keyword is valid
				if($objPrimaryKey->checkPrimKey($arrsms[0]) ==true){
					//primary key word exist
					$obj->message=cleanQuery(strtolower($_REQUEST['textMessage']));
					$obj->phone = str_replace('+','',$_REQUEST['mob_no']);
					$obj->userId = $_SESSION['user']['id'];
					$obj->received = date("Y-m-d h:i:s");
					if($obj->newIncomingSms()){
						$msg=VEN_SIMULATE_SMS_REQUEST_MSG_04;
					}else{
						$msg=VEN_SIMULATE_SMS_REQUEST_MSG_05;	
					}
				}else if($objSecondKeyword->checkSekondKey($arrsms[0])){
					//Keyword found in secondary keyword table
					$obj->message=cleanQuery(strtolower($_REQUEST['textMessage']));
					$obj->phone = str_replace('+','',$_REQUEST['mob_no']);
					$obj->userId = $_SESSION['user']['id'];
					$obj->received = date("Y-m-d h:i:s");
					if($obj->newIncomingSms()){
						$msg=VEN_SIMULATE_SMS_REQUEST_MSG_04;
					}else{
						$msg=VEN_SIMULATE_SMS_REQUEST_MSG_05;	
					}
				}else{
					//Key word not exist
					$msg=VEN_SIMULATE_SMS_REQUEST_MSG_03;
					$errorFlag='1';
				}
	
			}else{
				//error message if keyword is empty
				$msg=VEN_SIMULATE_SMS_REQUEST_MSG_02;
				$errorFlag='1';
			}

		}
	
	}
	
	if($errorFlag=='1'){
		$smarty->assign('textMessage',$_REQUEST['textMessage']);
		$smarty->assign('mobNo',$_REQUEST['mob_no']);
	}
	
	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('msg',$msg);
	$smarty->display("templates/vendor/ven_simulate_sms_request.tpl");
	
	include 'footer.php';
	
	
?>