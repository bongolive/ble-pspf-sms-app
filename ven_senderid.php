<?php
/****************************************************************************************************************
*	File : ven_senderid.php
*	Purpose: listing of sender Id details & request for new one
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'senderid/senderid.class.php');
	include(MODEL.'user.class.php');
	require_once 'swift/lib/swift_required.php';

	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$errorFlag = 0;
	$objSenderid = new senderid();
	$objSenderid->login_id = $_SESSION['user']['id'];
	
	$objUser = new user();
	$objUser->userId = $_SESSION['user']['id'];
	$vendorDetail = $objUser->getVendorDetail();
	$objUser->parentVenId = $vendorDetail['parent_ven_id'];
	
	if($_SESSION['user']['parent_ven_id'] != ''){
		$resellerDetail = $objUser->getResellerDetail();
	}else{
		$resellerDetail['name'] ='Admin';
		$resellerDetail['email'] ='pspf@pspf-tz.org';
	}
	
	if(isset($_REQUEST['action']) && $_REQUEST['action']=='add'){
	//	var_dump(senderidValidation($_REQUEST['senderid']));
		if(!isset($_REQUEST['senderid']) || $_REQUEST['senderid']==""){
			
			$msg = VEN_SENDERID_MSG_1;
			$errorFlag = 1;
		
		}else{
				if(senderidValidation($_REQUEST['senderid'])){
					$objSenderid->parentVenId = $_SESSION['user']['parent_ven_id'];
					$objSenderid->senderid = $_REQUEST['senderid'];
					$status = $objSenderid->insertSenderid();
					
					if(strlen($status)>25){
					
						$msg = VEN_SENDERID_MSG_2;
						
							//sendind mail
							$smarty->assign("parentVenId", $_SESSION['user']['parent_ven_id']);
							$smarty->assign('status', 'active');
							$smarty->assign('resellerDetails', $resellerDetail);
							$smarty->assign("vendorDetail", $vendorDetail);
							$smarty->assign("senderName", $_REQUEST['senderid']);
							$smarty->assign("requestDate", date("Y-m-d H:i:s"));
							$smarty->assign('allowUrl',BASE_URL.'ven_manage_senderid.php?status=active&senderid='.$_REQUEST['senderid'].'&vendorId='.$_SESSION['user']['id'].'&action=confirm&id='.urlencode($status));
							$smarty->assign('rejectUrl',BASE_URL.'ven_manage_senderid.php?status=inactive&senderid='.$_REQUEST['senderid'].'&vendorId='.$_SESSION['user']['id'].'&action=confirm&id='.urlencode($status));
					
							try{
								if($project_vars["smtp_auth"]===true){
									$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
									->setUsername($project_vars["smtp_username"])
									->setPassword($project_vars["smtp_password"]);
								}else{
						
									$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
								}

								$mailer = Swift_Mailer::newInstance($transport);

								$body = $smarty->fetch('mail_template/mail_ven_sendername.tpl');

								$message = Swift_Message::newInstance('Sendername Request')
								->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
								->setTo(array($vendorDetail['email']=>$vendorDetail['name']))
								->setBody($body );
								$message->setContentType("text/html");

								//Send the message
								$result = $mailer->send($message);
						} catch (Exception $e){
					
							$msg = SUB_REGISTER_MSG_08.$e->getMessage();
							$errorFlag = 1;
						}	
								
								
								
								
						try{	
							if($project_vars["smtp_auth"]===true){
									$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
									->setUsername($project_vars["smtp_username"])
									->setPassword($project_vars["smtp_password"]);
								}else{
						
									$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
								}

								$mailer = Swift_Mailer::newInstance($transport);

								$body = $smarty->fetch('mail_template/mail_adm_sendername.tpl');

								$message = Swift_Message::newInstance('Sendername Request')
								->setFrom(array($vendorDetail['email']=>$vendorDetail['name']))
								->setTo(array($resellerDetail['email'] =>$resellerDetail['name']))
								->setBody($body );
								$message->setContentType("text/html");

								//Send the message
								$result = $mailer->send($message);
							} catch (Exception $e){
					
								$msg = SUB_REGISTER_MSG_08.$e->getMessage();
								$errorFlag = 1;
							}	
								
						
						
					}elseif($status==2){
						$msg = VEN_SENDERID_MSG_3;
						$errorFlag = 1;

					}elseif($status==0){
						$msg = VEN_SENDERID_MSG_4;
						$errorFlag = 1;
					}
				}else{
				
					$msg = VEN_SENDERID_MSG_5;
					$errorFlag = 1;
				}

				if($errorFlag == 1){
					$smarty->assign("senderid",$senderid);		
				}
		}
	}
	
	$arrSenderid = $objSenderid-> getSenderidList();

	
	$smarty->assign("msg",$msg);
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("arrSenderid",$arrSenderid);
	$smarty->display("vendor/ven_senderid.tpl");
	
	include 'footer.php';
?>