<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	$smarty->assign('js','ven_coupone.js');
	include('ven_header.php');
	include(MODEL.'user.class.php');
	include(MODEL.'sms/sms.class.php');
	include(MODEL.'senderid/senderid.class.php');
	include(MODEL.'addressbook/addressbook.class.php');
	include(MODEL.'credit/credit.class.php');
	include(MODEL.'usubscribers.class.php');
 	include(MODEL.'primkeys.class.php');
	
	$smarty->assign("arrVendors",getArray('mst_vendors','id','name',' where active=1 and deleted=0 '));
	$loginValidation = check_session($_SESSION['user'],'VEN','vendor/index.php');

	//number of subscribers to keyword
	$mobileCount = $_POST['count_user_subscriber'];
	
	//calculate credit balance
	$arr=getArray('credit_balances','id','CAST((credit_balance-credit_blocked) as SIGNED INTEGER) as  credit_balance'," where user_id = '".$_SESSION['user']['id']."'");
	$creditBalance = $arr[0]['credit_balance'];
	
	//does broadcaster have valid keywords
	$obj = new primkeys();
	$obj->vendor_id=$_SESSION['user']['id'];
	$f = $obj->getPrimByVendor();
	
	//no valid keywords
	if($f==false)
	{
		$msg = VEN_SMS_PUSH_MSG_02;//change this
		$errorFlag = 1;
//		header("location:ven_smspush.php");
	}

	$smarty->assign('PrimKeyArray',$f);
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}

	//broadcaster senderids
	$senderid  = new senderid();
	$senderid->login_id = $_SESSION['user']['id'];
	$senders = $senderid->getActiveSenderid();
 	$smarty->assign('myArray',$senders);


	//does broadcaster have enough credit?
	if($creditBalance < $mobileCount){
			$msg = VEN_SMS_PUSH_MSG_02;
			$errorFlag = 1;
	}

	//create sms broadcast object
	$objSms = new sms();
	
	switch($_REQUEST['action']){
			case 'smspush':
			if($_REQUEST['scheduleDate'] && $_REQUEST['scheduleDate'] !==""){

				if(strtotime($_REQUEST['scheduleDate'])< strtotime(date('Y-m-d H:i:s'))){

					$msg = VEN_SMS_PUSH_MSG_01;
					$errorFlag = 1;
				}
			}
			if($errorFlag == 0 ){

				$str=str_replace(chr(13), '', $_REQUEST['textMessage']);	
				$smsCount = ceil(strlen($str)/160);

				$creditNeeded =  $smsCount * $_POST['count_user_subscriber'];
		/*		if(is_array($_REQUEST['addressbooks'])){
					$_SESSION['sms']['addressbooks']	= array_unique($_REQUEST['addressbooks']);
				}
				if(is_array($_REQUEST['contacts'])){
					$_SESSION['sms']['contacts']		= array_unique($_REQUEST['contacts']);
				}
			*/	$_SESSION['sms1']['textMessage']		= $_REQUEST['textMessage'];
				$_SESSION['sms1']['senderid']		= $_REQUEST['senderid'];
				$_SESSION['sms1']['scheduleDate']	= $_REQUEST['scheduleDate'];
				$_SESSION['sms1']['creditNeeded']	= $creditNeeded;
				$_SESSION['sms1']['smsCount']		= $smsCount;//*$_POST['count_user_subscriber'];
                $_SESSION['sms1']['primkey'] 		= $_POST['prim_key_id'];

				$smarty->assign('creditNeeded',$creditNeeded);
				$smarty->assign('smsCount',$smsCount);
				$smarty->assign('mobileCount',$_POST['count_user_subscriber']);
				$smarty->assign('creditBalance',$creditBalance);//$creditBalance);
				$smarty->assign('smsconf','smsconf');
			}else{
				$smarty->assign('arrSelectedAddbooks',$arrSelectedAddbooks);
				$smarty->assign('arrSelectedContacts',$arrSelectedContacts);
				$smarty->assign('textMessage', $_REQUEST['textMessage']);
				$smarty->assign('senderid', $_REQUEST['senderid']);
				$smarty->assign('scheduleDate', $_REQUEST['scheduleDate']);

			}
             break;
			case 'smsconf':
                    if(isset($_SESSION['sms1']))
                        {
					$objSms->textMassage	= $_SESSION['sms1']['textMessage'];
					$objSms->senderID		= $_SESSION['sms1']['senderid'];//$_SESSION['user']['id'];
					$objSms->credit			= $_SESSION['sms1']['smsCount'];//$_SESSION['sms1']['creditNeeded'];
					$objSms->massegeCount	= $_SESSION['sms1']['smsCount'];
					$objSms->massegeLength	= strlen($_SESSION['sms1']['textMessage']);
					$objSms->senderType		= 'VEN';
					$objSms->userId       	= $_SESSION['user']['id'];//new addition

					if(isset($_SESSION['sms1']['scheduleDate']) && $_SESSION['sms1']['scheduleDate']!=''){
						$objSms->isScheduled	= 1;
						$objSms->scheduleTime	= getDateFormat($_SESSION['sms1']['scheduleDate']);
					}
                  $obj =  new usubscriber();
				  print_r($_POST['prim_key_id']);
                  $obj->prim_key_id =$_SESSION['sms1']['primkey'];
                  $nums = $obj->get_num_users();
				  			
                  for($i=0; $i<sizeof($nums);$i++)
                  {
                       $objSms->contactId = $obj->get_id_contact_mob_num($nums[$i]);
						if($objSms->smsPush())
						{
	   				  		//block credits for sms to be sent
		               		$objSms->blockCredit();
						}

                  }

                unset($_SESSION['sms1']);
            }

            /*
					if($objSms->smsPush()){
						//$objSms->blockCredit();
						if(isset($_SESSION['sms']['scheduleDate']) && $_SESSION['sms']['scheduleDate']!=''){

							$msg = VEN_SMS_PUSH_MSG_03;

						}else{
							$msg = VEN_SMS_PUSH_MSG_04;
						}

					}
*/

			break;
			case'backTo1':
				$smarty->assign('arrSelectedAddbooks',$arrSelectedAddbooks);
				$smarty->assign('arrSelectedContacts',$arrSelectedContacts);
				$smarty->assign('textMessage',$_SESSION['sms1']['textMessage']);
				$smarty->assign('senderid',$_SESSION['sms']['senderid']);
				$smarty->assign('scheduleDate',$_SESSION['sms']['scheduleDate']);

			break;
			default:
			unset($_SESSION['sms1']);
			break;


	}

	$smarty->assign('errorFlag',$errorFlag);
	$smarty->assign('msg',$msg);
	$smarty->assign('arrAddressbook',$arrAddressbook);
	$smarty->assign('arrSenderid',$arrSenderid);
	$smarty->assign('mob_no_length',$project_vars['mob_no_length']);
	$smarty->assign('fname_length',$project_vars['fname_length']);
	$smarty->assign('Iname_length',$project_vars['lname_length']);

	$smarty->display("vendor/ven_coupone.tpl");

	include 'footer.php';

