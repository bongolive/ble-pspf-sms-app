<?php
	
	/********************************************
	*	File	: vendor_incoming.php		*
	*	Purpose	: Vendor Incoming SMS Management		*
	*	Author	: Leonard Nyirenda						*
	********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'sms/sms.class.php');
	require_once(MODEL.'incoming.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}
	$selfUrl = BASE_URL.'ven_incoming.php';
	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);
	$objUser = new user();
	$objIncoming  = new incoming();
	$objIncoming->userId = $_SESSION['user']['id'];
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";
	switch($action){
		case'search':
			// setting pagin url			
			$pagingUrl = $selfUrl.'?vendorName='.$_SESSION['user']['id'];
			//$pagingUrl.= '&broadcaster_type='.$_REQUEST['broadcaster_type'];
			$pagingUrl.= '&mob_no='.formatMobno($_REQUEST['mob_no']); 
			$pagingUrl.= '&status='.$_REQUEST['status'];
			$pagingUrl.= '&textMessage='.$_REQUEST['textMessage']; 
			$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
			$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
			$pagingUrl.= '&action='.$_REQUEST['action'];
			$pagingUrl.= '&keyword='.$_REQUEST['keyword'];
			$pagingUrl.= '&checkno='.$_REQUEST['checkno'];
			SmartyPaginate::setUrl($pagingUrl);
			// stting search param
			$objIncoming->userId = $_SESSION['user']['id'];
			$objIncoming-> keyword = $_REQUEST['keyword'];
			$objIncoming-> checkno = $_REQUEST['checkno'];
			$objIncoming-> vendorName = $_SESSION['user']['id'];
			$objIncoming-> status	= $_REQUEST['status']; 
			$objIncoming-> mob_no		= formatMobno($_REQUEST['mob_no']);  
			$objIncoming-> textMessage	= $_REQUEST['textMessage']; 
			$objIncoming-> startDate	= getDateFormat($_REQUEST['startDate']);
			$objIncoming-> endDate		= getDateFormat($_REQUEST['endDate']); 
			// getting search sms
			$arrSms =  $objIncoming->getIncomingSmsList();
		break;
		case 'edit':
			$objIncoming-> sms_in_id = $_REQUEST['sms_in_id'];
			$arrSms =  $objIncoming->getIncomingSmsList();
		break;
		case 'update':
			$objIncoming-> sms_in_id = $_REQUEST['sms_in_id'];
			$objIncoming-> textMessage	= $_REQUEST['textMessage2'];
			$objIncoming-> status	= $_REQUEST['status2'];
			$result=$objIncoming->updateEditedIncomingSms();
			$action = "search";	
			$objIncoming-> sms_in_id = '';
			$objIncoming-> textMessage	= '';
			$objIncoming-> status	= '';
			$arrSms =  $objIncoming->getIncomingSmsList();  
		break;
		default:
			$action = "search";	
			$arrSms =  $objIncoming->getIncomingSmsList();
	}
	$smarty->assign("msg",$msg);
	$smarty->assign("vendorName",$_REQUEST['vendorName']);
	$smarty->assign("status",$_REQUEST['status']);
	$smarty->assign("mob_no", $_REQUEST['mob_no']);
	$smarty->assign("textMessage",$_REQUEST['textMessage']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	$smarty->assign("keyword", $_REQUEST['keyword']);
	$smarty->assign("checkno", $_REQUEST['checkno']);
	$smarty->assign("arrKeyWord",getArray('prim_keywords','id_prim_keywords','text_prim_keywords'));
	$smarty->assign("arrVendors",getArray('mst_vendors','id','name',' where active=1 and deleted=0'));
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("ajaxUrl",BASE_URL.'ven_incoming_ajax.php');
	$smarty->assign("action",$action);
	$smarty->assign("arrSms",$arrSms);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_incoming.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
