<?php
/********************************************
	*	File	: rcv_incoming.php				*
	*	Purpose	: Recieve Incoming sms for PSPF and save the sms to the databese	*
	*	Author	: leonard nyirenda						*
*********************************************/
include_once ('bootstrap.php');
require_once(LIB_DIR.'inc.php');
include_once(LIB_DIR.'smsProcessLib.php');

if(isset($_REQUEST['message']) && isset($_REQUEST['from'])){
	//$data[] = $_POST['data'];
	$sms_text = trim(strtolower(urldecode($_REQUEST['message'])));
	$sms_text = strtolower($sms_text );
	$sms_text = cleanQuery($sms_text);

	$sms_sender = str_replace('+','',urldecode($_REQUEST['from']));
	$sms_sender = trim($sms_sender);
	
	$phone_receive = trim(urldecode($_REQUEST['phone_receive']));

	$date = date("Y-m-d h:i:s");
	$apikey = $project_vars['bulk_gw_apikey'];

	//store received SMS into database
	$sms_in_query = "insert into text_in_log set phone='".cleanQuery($sms_sender)."', message='".cleanQuery($sms_text)."',phone_receive='".cleanQuery($phone_receive)."' ,received='".$date."', status='received',ven_id='".cleanQuery($apikey)."'";
	$sms_in_query_result = mysql_query($sms_in_query) or mysql_error_show($sql,__FILE__,__LINE__);
	echo "Ok";
}else{
	echo "Fail";
}
?>