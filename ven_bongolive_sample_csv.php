<?php
/****************************************************************************************************************
*	File   : ven_bongolive_sample_csv.php
*	Purpose: Download sample CSV file format for uploading contacts
*	Author : Akhilesh
*****************************************************************************************************************/ 
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	include(MODEL.'addressbook/addressbook.class.php');
	


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	$dirPath	= "download_file/";
	$fileName	= "bongolive_sample.csv";

	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Length: " . filesize($dirPath.$fileName));
	header("Content-type: text/x-csv");
	header("Content-Disposition: attachment; filename=$fileName");
	 
	ob_clean();
	flush();
	readfile($dirPath.$fileName);
	
	require_once(LIB_DIR.'close.php');
	exit;
?>