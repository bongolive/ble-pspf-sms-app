<?php
	
	/********************************************
	*	File	: ven_manage_credits_request.php				*
	*	Purpose	: Vendor crdit  management for sub vendor *
	*	Author	: Leonard Nyirenda						*
	*********************************************/

	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('ven_header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'credit/credit.class.php');
	include(MODEL.'vendors/vendor.class.php');
	include(CLASSES.'SmartyPaginate.class.php');
	require_once 'swift/lib/swift_required.php';


	$loginValidation = check_session($_SESSION['user'],'VEN',$project_vars["login_failed_url"]);
	
	foreach($_REQUEST as $key=>$value){
		$_REQUEST[$key] = trim($value);
	}


	SmartyPaginate::connect();
	SmartyPaginate::setLimit($project_vars['row_per_page']);
	
	if($_SESSION['user']['parent_ven_id']==''){
		(int) $remoteCredit = getRemoteCreditBalance();
		(int) $resCredit = getTotalCreditOfAllVendors();
		$resp = updateLocalCopyCreditBalance($_SESSION['user']['id'],$remoteCredit - $resCredit['credit']);
	} 
	
	$objUser = new user();
	$objUser->parentVenId = $_SESSION['user']['id'];
	if($_SESSION['user']['id'] != ''){
		$resellerDetail = $objUser->getResellerDetail();
	}else{
		$resellerDetail['name'] ='Admin';
		$resellerDetail['email'] ='pspf@pspf-tz.org';
	}
	$smarty->assign('resellerDetails', $resellerDetail);
	$smarty->assign("parentVenId", $_SESSION['user']['id']);
	
	$objVendor = new vendor();
	$objVendor->parentVenId=$_SESSION['user']['id'];
	$arrVendors=$objVendor->getSubVendorList();
	
	$objCredit = new credit();
	
	$selfUrl = BASE_URL.'ven_manage_credits_request.php';
	$pagingUrl = $selfUrl.'?vendorName='.$_REQUEST['vendorName'];
	$pagingUrl.= '&username='.$_REQUEST['username'];
	$pagingUrl.= '&vendor_type='.$_REQUEST['vendor_type'];
	$pagingUrl.= '&rate='.$_REQUEST['rate']; 
	$pagingUrl.= '&quantity='.$_REQUEST['quantity']; 
	$pagingUrl.= '&payment_status='.$_REQUEST['payment_status']; 
	$pagingUrl.= '&status='.$_REQUEST['status']; 
	$pagingUrl.= '&startDate='.$_REQUEST['startDate']; 
	$pagingUrl.= '&endDate='.$_REQUEST['endDate'];
	SmartyPaginate::setUrl($pagingUrl);

	$objCredit->parentVenId=$_SESSION['user']['id'];
	$objCredit->vendorName		= $_REQUEST['vendorName'];
	$objCredit->username		= $_REQUEST['username'];
	$objCredit->vendor_type		= $_REQUEST['vendor_type'];
	$objCredit->crdtSchmId		= $_REQUEST['rate']; 
	$objCredit->quantity		= $_REQUEST['quantity'];
	$objCredit->status		= $_REQUEST['status'];
	$objCredit->payment_status	= $_REQUEST['payment_status'];
	$objCredit->startDate		= getDateFormat($_REQUEST['startDate'],'Y-m-d');
	$objCredit->endDate		= getDateFormat($_REQUEST['endDate'],'Y-m-d');

	$objCreditRequest =  $objCredit->getRequestedSubVenCreditList();
	
	
	$action = isset($_REQUEST['action'])?$_REQUEST['action']:"";

	switch($action){
	
		case'edit':
			$objCredit->creditRequestId = $_REQUEST['id'];
			$arrCreditDetails = $objCredit->getRequestedCreditDetails();
			$smarty->assign('arrCreditScheme',getArray('credit_schemes','id','rate',' ORDER BY rate ASC'));
			$smarty->assign('arrCreditBonus',getArray('credit_bonus','id','rate',' ORDER BY rate ASC'));
			
		break;
		case'update':
			//$crdtSchem = explode("###",$_REQUEST['crdtSchmId']);
			$objCredit->creditRate = $_REQUEST['crdtSchmId'];

			$tmp_rate = $objCredit->getCrdtSchm();

			if ($tmp_rate){
			
				$objCredit->crdtSchmId 	= $tmp_rate['id'];	

			}

			else {

				$bon = $objCredit->getCrdtBon();
				
				if($bon){

					$objCredit->crdtSchmId 	= $bon['id'];

				} else {

					$sch = $objCredit->addCreditBonus();
			
					$objCredit->crdtSchmId 	= $sch['id'];

				}

			}	
			$objCredit->creditRequestId 			= $_REQUEST['id'];
			$objCredit->creditRequestStatus 		= $_REQUEST['status'];
			$objCredit->creditRequestPaymentStatus 		= $_REQUEST['payment_status'];
			$objCredit->creditRequest 			= $_REQUEST['crditRequested'];
	
			$objCredit->creditTotalCost 			= (intval($_REQUEST['crdtSchmId'])*intval($_REQUEST['crditRequested']));
			$arrCreditDetails = $objCredit->getRequestedCreditDetails();
			
			$objUser->userId = $_REQUEST['userId'];
			$userDetail = $objUser-> getUserDetailVen();
			
			$objCredit->parentVenId=$_SESSION['user']['id'];
			$parentVenCreditBal = $objCredit->getParentVenCXreditBalance();
			$crdtRequested== $_REQUEST['crditRequested'];
			
			$objVendor = new vendor();
			$objVendor->userId = $_REQUEST['userId'];
			$rowBalance = $objVendor->getVendorCreditBalance();
			$venCreditBalance=$rowBalance['credit_balance']-$rowBalance['credit_blocked'];
			
			$objUser->parentVenId = $userDetail['parent_ven_id'];
			$resellerDetail = $objUser->getResellerDetail();
			
			$objVendor->parentVenId=$userDetail['parent_ven_id'];
			$arrVendors=$objVendor->getSubVendorList();
			
			
			
			
			
			switch($_REQUEST['payment_status']){
				case '0': $pay = 'Not paid';break;
				case '1': $pay = 'Paid';break;
				case '2': $pay = 'Partially Paid';break;
				case '3': $pay = 'Receipt Sent';break;
				case '4': $pay = 'Bonus';break;
			}

			if($_REQUEST['status'] == 'allocated'){
				if($parentVenCreditBal < $_REQUEST['crditRequested']){
					$status=false;
					$errorFlag=1;
					$msg="Credid not added to a User because you do not have enough credit in your account";
				}else{
					$status = $objCredit-> updateRequestedCredit();
					$objCredit->removeCreditToParentVendor();
					$msg="Credit updated successfully";
				}
			}else{
				 $objCredit-> updateRequestedCredit2();
				 $status=false;
			}
			
				if($status) {
					//updating Credit balance reminder
					$objVendor = new vendor();
					$objVendor->userId = $_REQUEST['userId'];
					$rowBalance = $objVendor->getVendorCreditBalance();
					if(($rowBalance['credit_balance']-$rowBalance['credit_blocked']) > $rowBalance['credit_level']){
						$objVendor->reminderSent = 0;
						$objVendor->updateVendorReminderStatus();
					}

					/*if (($_REQUEST['payment_status']!=$_REQUEST['old_payment_status'] || $_REQUEST['status']!=$_REQUEST['old_status']) && $project_vars['mail_send']===true){	*/
					$smarty->assign('req_id',$arrCreditDetails['credit_request_id']);

					if ((($_REQUEST['payment_status']==1 && $_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) || $_REQUEST['status']!=$_REQUEST['old_status']) && $project_vars['mail_send']===true){
						//$tmp_rate = explode('###', $_REQUEST['crdtSchmId']);
						//$objCredit->crdtSchmId = $tmp_rate[0];
						//$rate = $objCredit->getCrdtSchmDetails();

						
						$smarty->assign('quantity', $_REQUEST['crditRequested']);
						//$smarty->assign('rate', $rate['rate']);
						$smarty->assign('rate', $_REQUEST['crdtSchmId']);
						//$smarty->assign('total', $_REQUEST['crditRequested']*$rate['rate']);
						$smarty->assign('total', $_REQUEST['crditRequested']*$_REQUEST['crdtSchmId']);
						$smarty->assign('status', $_REQUEST['status']);
						$smarty->assign('PayStatus', $pay);
									
						//if ($_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) {
						if ($_REQUEST['payment_status']==1 && $_REQUEST['payment_status']!=$_REQUEST['old_payment_status']) {
							$smarty->assign('payment_true', true);
							$smarty->assign('newPayStatus', $pay);
						}
						if ($_REQUEST['status']!=$_REQUEST['old_status']) {
							$smarty->assign('status_true', true);
							$smarty->assign('newStatus', $_REQUEST['status']);
						}

						try{
							if($project_vars["smtp_auth"]===true){
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
								->setUsername($project_vars["smtp_username"])
								->setPassword($project_vars["smtp_password"]);
							}else{
						
								$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
							}

							$mailer = Swift_Mailer::newInstance($transport);

							$body = $smarty->fetch('mail_template/mail_change_status.tpl');

							//var_dump($body);

							$message = Swift_Message::newInstance('Status of Purchase Request')
							->setFrom(array($resellerDetail['email'] =>$resellerDetail['name']))
							->setTo(array($userDetail['email']=>$userDetail['name']))
							->setBody($body );
							$message->setContentType("text/html");

							//Send the message
							$result = $mailer->send($message);
						} catch (Exception $e){
							$msg = SUB_REGISTER_MSG_08.$e->getMessage();
							$errorFlag = 1;
						}
						try{
							if($project_vars['mob_confirmation']==true){		
								$massege = str_rand($project_vars["random_mob_conf_length"],$project_vars["random_string_type"]);
								smsSendWithCurl($userDetail['mob_no'],str_replace('MESSAGE',$massege,$project_vars['register_message']),$project_vars['sms_api_senderid']);
								$objUser->mob_conf_msg = $massege;
								$objUser->insertSubMobConfMsg();
							}
						}catch(Exception $objE){
							$msg = SUB_REGISTER_MSG_09.$objE->getMessage();
							$errorFlag = 1;
						}

					}
				}			

			if($status && $_REQUEST['status'] == 'allocated'){
				$msg = "Credits allocated to vendors";
			}elseif($status && $_REQUEST['status'] == 'rejected'){
				$msg = "Credits request rejected for vendors";
			}else{
				$msg = "Credits request rejected because you do not have enough credit in your account";
			}

			$action = "";
			
		break;
		default:
			$action = "";
	
	
	}
	
	$smarty->assign("payment_status", $_GET['payment_status']);
	
	$smarty->assign("msg",$msg);
	$smarty->assign("errorFlag",$errorFlag);
	$smarty->assign("vendorName",$_REQUEST['vendorName']);
	$smarty->assign("username",$_REQUEST['username']);
	$smarty->assign("vendor_type", $_REQUEST['vendor_type']);
	$smarty->assign("rate", $_REQUEST['rate']); 
	$smarty->assign("quantity",$_REQUEST['quantity']);
	$smarty->assign("status",$_GET['status']);
	$smarty->assign("startDate", $_REQUEST['startDate']);
	$smarty->assign("endDate", $_REQUEST['endDate']);
	$smarty->assign('ajaxUrl',BASE_URL.'credit_ajax_adm.php');

	$smarty->assign("arrVendors",$arrVendors);
	$smarty->assign("arrCrdtSchm",getArray('credit_schemes','id','rate',' where active=1 '));
	$smarty->assign("selfUrl",$selfUrl);
	$smarty->assign("action",$action); 
	$smarty->assign("arrCreditDetails",$arrCreditDetails);
	$smarty->assign("objCreditRequest",$objCreditRequest);
	SmartyPaginate::assign($smarty);
	$smarty->display("vendor/ven_manage_credit_requested.tpl");
	SmartyPaginate::disconnect();
		
	include 'footer.php';

?>
