<?php
/****************************************************************************************************************
*	File : simulate_interval_request.php
*	Purpose: Simulate sms request for testing purpose
*	Author : Leonard Nyirenda
*****************************************************************************************************************/ 

	
	$count=0;
	define ("URL_API_DOMAIN", "http://localhost/pspf/");
	while($count<1000){
		$text=urlencode("test leonard nyirenda ".$count);
		$sender="255757447737";
		$posturl = URL_API_DOMAIN."rcv_incoming.php?sender=$sender&text=$text";
    	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $posturl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_exec($ch);
		$count++;
	}
	
?>