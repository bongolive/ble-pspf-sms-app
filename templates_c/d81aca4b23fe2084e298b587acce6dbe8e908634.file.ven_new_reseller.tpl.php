<?php /* Smarty version Smarty3-RC3, created on 2013-05-10 17:19:31
         compiled from "C:\xampp\htdocs\pspf\templates/vendor/ven_new_reseller.tpl" */ ?>
<?php /*%%SmartyHeaderCode:25894518d01f310c423-47393928%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd81aca4b23fe2084e298b587acce6dbe8e908634' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/vendor/ven_new_reseller.tpl',
      1 => 1364997918,
    ),
  ),
  'nocache_hash' => '25894518d01f310c423-47393928',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">

	<div class="column2-ex-left-2 column2_contents">
    <h1>New Client </h1>
    <div style="clear: both;">
<div id="errorDiv" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg"<?php }else{ ?> class="sucess_msg"<?php }?>
<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block;" <?php }else{ ?> style="display:none;"<?php }?>><?php echo $_smarty_tpl->getVariable('msg')->value;?>
</div>

<form name="venRegister" method="POST" action="" enctype="multipart/form-data" onsubmit="javascript: venFormValidation(); return false;">
    <?php if ($_smarty_tpl->getVariable('step')->value=='1'||$_smarty_tpl->getVariable('step')->value==''||$_smarty_tpl->getVariable('step')->value=='0'){?>
        <table width="100%" cellspacing="10">
            <tr>
                <td width="24%">Username<span class="star_col">*</span></td>
                <td width="76%"><span class="form_dinatrea standard-input">
				<input type="text" name="username" id="username" value="" onkeyup="usernameValidation();"></span>
			  <span id="usernameDiv" class="form_error"></span></td>
            </tr>
            <tr>
                <td>Password<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input"><input type="password" name="password" id="password" onkeyup="passValidation();"  value="<?php echo $_smarty_tpl->getVariable('password')->value;?>
"></span><span id="passwordDiv" class="form_error"></span></td>
            </tr>
            <tr>
                <td><?php echo @CONFIRM_PASSWORD;?>
<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input"><input type="password" name="conf_password" id="conf_password" onkeyup="confPassValidation();"  value="<?php echo $_smarty_tpl->getVariable('password')->value;?>
"></span><span id="cpasswordDiv" class="form_error"></span></td>
            </tr>
	    <tr>
                <td><?php echo @ACCOUNT_TYPE;?>
<span class="star_col">*</span></td>
                <td><span class="form_dinatrea standard-input">	
			<select name="accType" id="accType">
			<option value=""><?php echo @SELECT;?>
</option>
			<option value="1" <?php if ($_smarty_tpl->getVariable('accType')->value=='1'){?>selected<?php }?>><?php echo @INDIVIDUAL;?>
</option>	
			<option value="2" <?php if ($_smarty_tpl->getVariable('accType')->value=='2'){?>selected<?php }?>><?php echo @ORGANIZATION;?>
</option>	
			</select></span>
		<span id="accTypeDiv" class="form_error"></span></td>
          </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <input type="submit" name="next1" id="next1" value="<?php echo @NEXT;?>
" class="editbtn">
                </td>
			</tr>
			<!-- <tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 			
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/van_stp-0.png" />
                </td>
			</tr> -->          
        </table>
    <?php }elseif($_smarty_tpl->getVariable('step')->value=='2'){?>
        <?php if ($_smarty_tpl->getVariable('accType')->value=='1'){?>
		<table width="100%" cellspacing="10">
		    <tr>
			<td width="45%"><?php echo @NAME;?>
 <span class="star_col">*</span></td>
			<td width="55%"><span class="form_dinatrea standard-input">
			  <input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
" ></span>
			  <span id="nameDiv" class="form_error"></span>			  </td>
	      </tr>
		    <tr>
			<td>
				<?php echo @MOBILE_NUMBER;?>
 <span class="star_col">*</span><br/><span class="mobexmp">(Ex. 784845785)</span>			</td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" value="<?php echo $_smarty_tpl->getVariable('mob_no')->value;?>
" onkeyup="mobileValidation();" maxlength="<?php echo $_smarty_tpl->getVariable('mobMaxlength')->value+1;?>
"></span><span id="mobDiv" class="form_error"></span></td>
		    </tr>
		    <tr>
			<td><?php echo @EMAIL;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->getVariable('email')->value;?>
" onkeyup="emailValidation();" ></span>
			<span id="emailDiv" class="form_error"></span></td>
		    </tr>
		    <!-- this option is not required for individual vendor
			<tr>
			<td>Number of employees</td>
			<td><span class="form_dinatrea standard-input">
				<select name="peopleRange" id="peopleRange" >
				<option value="">Select</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['name'] = 'pplRange';
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrPeopleRanges')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['id']==$_smarty_tpl->getVariable('peopleRange')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['people_range'];?>
</option>
				<?php endfor; endif; ?>
				</select></span>

			<span id="emailDiv" class="form_error"></span></td>

		    </tr>
			-->
		    <tr>
			<td><?php echo @POSTAL_ADDRESS;?>
</td>
			<td><span class="form_dinatrea standard-input"><textarea name="postalAddress" id="postalAddress" ><?php echo $_smarty_tpl->getVariable('postalAddress')->value;?>
</textarea></span>			</td>
		    </tr>
		    
		     <tr>
		       <td><?php echo @COUNTRY;?>
<span class="star_col">*</span></td>
		       <td><span class="form_dinatrea standard-input">
			    <select name="country" id="country" onchange="refreshpage();" >
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['country']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['name'] = 'country';
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrCountry')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country_code'];?>
" <?php if ($_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country_code']==$_smarty_tpl->getVariable('country')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country'];?>
</option>
				<?php endfor; endif; ?>
			    </select>
				</span>
				<span id="countryDiv" class="form_error"></span></td>
		       <td></td>
		       <td></td>
          </tr>
		     <tr>
			<td><?php echo @LOCATION;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
			    <select name="location" id="location" >
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['location']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['name'] = 'location';
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrLocation')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['id']==$_smarty_tpl->getVariable('location')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['location'];?>
</option>
				<?php endfor; endif; ?>
			    </select>
				</span>
				<span id="locationDiv" class="form_error"></span></td>
			<td></td>
		    </tr>
		<!--	<tr>
				<td>Support Email </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="support_email" id="support_email" value="<?php echo $_smarty_tpl->getVariable('arrVendorDetail')->value['support_email'];?>
" onkeyup="support_emailValidation();" ></span>
				<span id="support_emailDiv" class="form_error"></span></td>
			 </tr>
			 <tr>
				<td>Logo</td>
				<td><span class="form_dinatrea standard-input" style="width:250px">
            <input type="file" name="file" id="file" value="<?php echo $_smarty_tpl->getVariable('fileuploaded')->value;?>
" class="form_dinatrea standard-input"/></span>
            <span id="fileDiv" class="form_error"></span></td>
			  </tr> -->
		    <tr style="display:none;">
			<td><?php echo @ACCEPT_BONGOLIVE;?>
 <a  class="content-link" href="javascript: void(0)" 
				onclick="openTermToRead('<?php echo $_smarty_tpl->getVariable('BASE_URL')->value;?>
<?php if ($_smarty_tpl->getVariable('terms_of_use')->value==1){?>lang_<?php echo $_SESSION['lang'];?>
/<?php }?>terms_of_use.html'); return false;"><?php echo @TERMS_OF_USE;?>
<a> <span class="star_col">*</span></td>
			<td>
			    <span style="width:20px; display:block;float:left;"><input type="checkbox" name="termCondition" id="termCondition" value="1" checked="checked" ></span>
			<span id="tncDiv" class="form_error"></span></td>
		    </tr>
			
		    <tr>
			<td>
			    <input type="submit" name="back" id="back" value="<?php echo @BACK;?>
" class="editbtn">			</td>
			<td>
			    <input type="submit" name="next2" id="next2" value="<?php echo @SUBMIT;?>
" class="editbtn">			</td>
		    </tr>
				<tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/van_stp-1.png" />                </td>
			</tr>  
		</table>
	<?php }elseif($_smarty_tpl->getVariable('accType')->value=='2'){?>

		<table width="100%" cellspacing="10">
		    <tr>
			<td width="45%"><?php echo @ORGANIZATION_NAME;?>
 <span class="star_col">*</span></td>
			<td width="55%"><span class="form_dinatrea standard-input">
			  <input type="text" name="organisation" id="organisation" value="<?php echo $_smarty_tpl->getVariable('organisation')->value;?>
" ></span>
			  <span id="organisationDiv" class="form_error"></span>			  </td>
			</tr>
		    <tr>
			<td><?php echo @ORGANIZATION_CATEGORY;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"> <select name="organisationCat" id="organisationCat" >
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['name'] = 'onganisation';
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrOrganisationCat')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['onganisation']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrOrganisationCat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['onganisation']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('arrOrganisationCat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['onganisation']['index']]['id']==$_smarty_tpl->getVariable('organisationCat')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrOrganisationCat')->value[$_smarty_tpl->getVariable('smarty')->value['section']['onganisation']['index']]['organisation'];?>
</option>
				<?php endfor; endif; ?>
			    </select></span>
			    <span id="organisationCatDiv" class="form_error"></span>			</td>
		    </tr>
		    <tr>
			<td><?php echo @REPRESENTATIVE_NAME;?>
<span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="name" id="name" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
"/></span>
			<span id="nameDiv" class="form_error"></span>			</td>
		    </tr>
		    <tr>
			<td><?php echo @MOBILE_NUMBER;?>
 <span class="star_col">*</span> <br/> <span class="mobexmp">(Ex. 784845785)</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" value="<?php echo $_smarty_tpl->getVariable('mob_no')->value;?>
" onkeyup="mobileValidation();" maxlength="<?php echo $_smarty_tpl->getVariable('mobMaxlength')->value;?>
"></span><span id="mobDiv" class="form_error"></span></td>
		    </tr>
		    <tr>
				<td><?php echo @LANDLINE_NUMBER;?>
 <span class="star_col"></span> <br/><span class="mobexmp">(Ex. 0222180995)</span></td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="phone" id="phone" value="<?php echo $_smarty_tpl->getVariable('phone')->value;?>
" maxlength="<?php echo $_smarty_tpl->getVariable('phoneMaxlength')->value;?>
" onkeyup="javascript: phneValidation(this.value);"/></span>
				<span id="phoneDiv" class="form_error"></span>				</td>
		    </tr>
		     <tr>
			<td><?php echo @EMAIL;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->getVariable('email')->value;?>
" onkeyup="emailValidation();" ></span><span id="emailDiv" class="form_error"></span></td>
		    </tr>
		    <tr>
			<td><?php echo @NUMBER_OF_EMPLOYEES;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
				<select name="peopleRange" id="peopleRange" >
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['name'] = 'pplRange';
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrPeopleRanges')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['pplRange']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['id']==$_smarty_tpl->getVariable('peopleRange')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrPeopleRanges')->value[$_smarty_tpl->getVariable('smarty')->value['section']['pplRange']['index']]['people_range'];?>
</option>
				<?php endfor; endif; ?>
				</select></span>

			<span id="peopleRangeDiv" class="form_error"></span></td>
		    </tr>
		     <tr>
			<td><?php echo @PHYSICAL_ADDRESS;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input"><textarea name="physicalAddress" id="physicalAddress" ><?php echo $_smarty_tpl->getVariable('physicalAddress')->value;?>
</textarea></span>
			<span id="physicalAddressDiv" class="form_error"></span>			</td>
		    </tr>
		     <tr>
			<td><?php echo @POSTAL_ADDRESS;?>
</td>
			<td><span class="form_dinatrea standard-input"><textarea name="postalAddress" id="postalAddress" ><?php echo $_smarty_tpl->getVariable('postalAddress')->value;?>
</textarea></span>			</td>
		    </tr>
		     <tr>
		       <td><?php echo @COUNTRY;?>
<span class="star_col">*</span></td>
		       <td><span class="form_dinatrea standard-input">
			    <select name="country" id="country"  onchange="refreshpage();">
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['country']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['name'] = 'country';
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrCountry')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['country']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['country']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['country']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country_code'];?>
" <?php if ($_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country_code']==$_smarty_tpl->getVariable('country')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['country']['index']]['country'];?>
</option>
				<?php endfor; endif; ?>
			    </select>
				</span>
				<span id="countryDiv" class="form_error"></span></td>
		       <td></td>
          </tr>
		     <tr>
			<td><?php echo @LOCATION;?>
 <span class="star_col">*</span></td>
			<td><span class="form_dinatrea standard-input">
			    <select name="location" id="location" >
				<option value=""><?php echo @SELECT;?>
</option>
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['location']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['name'] = 'location';
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrLocation')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['location']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['location']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['location']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['id']==$_smarty_tpl->getVariable('location')->value){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrLocation')->value[$_smarty_tpl->getVariable('smarty')->value['section']['location']['index']]['location'];?>
</option>
				<?php endfor; endif; ?>
			    </select>
				</span>
				<span id="locationDiv" class="form_error"></span>			</td>
			<td></td>
		    </tr>
		<!--	<tr>
				<td>Support Email </td>
				<td><span class="form_dinatrea standard-input"><input type="text" name="support_email" id="support_email" value="<?php echo $_smarty_tpl->getVariable('arrVendorDetail')->value['support_email'];?>
" onkeyup="support_emailValidation();" ></span>
				<span id="support_emailDiv" class="form_error"></span></td>
			 </tr>
			 <tr>
				<td>Logo</td>
				<td><span class="form_dinatrea standard-input" style="width:250px">
            <input type="file" name="file" id="file" value="<?php echo $_smarty_tpl->getVariable('fileuploaded')->value;?>
" class="form_dinatrea standard-input"/></span>
            <span id="fileDiv" class="form_error"></span></td>
			  </tr> -->
		    <tr style="display:none">
			<td><?php echo @ACCEPT_BONGOLIVE;?>
 <a  class="content-link" href="javascript: void(0)" 
				onclick="openTermToRead('<?php echo $_smarty_tpl->getVariable('BASE_URL')->value;?>
<?php if ($_smarty_tpl->getVariable('terms_of_use')->value==1){?>lang_<?php echo $_SESSION['lang'];?>
/<?php }?>terms_of_use.html'); return false;"><?php echo @TERMS_OF_USE;?>
<a> <span class="star_col">*</span>			</td>
			<td>
			    <span style="width:20px; display:block;float:left;"><input type="checkbox" name="termCondition" id="termCondition" value="1" checked="checked" ></span>
			<span id="tncDiv" class="form_error"></span></td>
		    </tr>
			
		    <tr>
			<td>
			    <input type="submit" name="back" id="back" value="<?php echo @BACK;?>
" class="editbtn">			</td>
			<td>
			    <input type="submit" name="next2" id="next2" value="<?php echo @SUBMIT;?>
" class="editbtn">			</td>
		    </tr>
				<tr>	
				<td colspan="2">&nbsp;</td>
			</tr> 
			<tr>	
				<td>&nbsp;</td>
				 <td align="left">
                   <img alt="0%" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/van_stp-1.png" />                </td>
			</tr>  
		</table>

	<?php }?>
	 <input type="hidden" name="accType" id="accType" value="<?php echo $_smarty_tpl->getVariable('accType')->value;?>
">
     <?php }elseif($_smarty_tpl->getVariable('step')->value=='3'){?>

	<table width="100%" cellspacing="10">
            <tr>
                <td width="33%"><?php echo @MOBILE_VERIFICATION;?>
</td>
                <td width="67%"></td>
            </tr>
            
            <tr>
                <td><?php echo @ENTER_VERIFICATION_CODE;?>
</td>
                <td><span class="form_dinatrea standard-input">
                    <input type="text" name="mob_conf_msg" id="mob_conf_msg" value="<?php echo $_smarty_tpl->getVariable('mobConfMsg')->value;?>
"></span>
                </td>
            </tr>
            <tr>
                <td>
                   
                </td>
                <td>
                    <input type="submit" name="next4" id="next4" value="<?php echo @CONFIRM;?>
" class="editbtn">
                </td>
            </tr>				
      </table>
 <?php }elseif($_smarty_tpl->getVariable('step')->value=='4'){?>
	 <table width="100%" cellspacing="10">
		<tr>	
			<td>&nbsp;</td>
			<td align="left">
				<img alt="0%" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/van_stp-2.png" />
			</td>
		</tr>  
	</table>

    <?php }?>
	<input type="hidden" name="step" id="step" value="<?php echo $_smarty_tpl->getVariable('step')->value;?>
">
	<span class="form_dinatrea standard-input">
	<input type="hidden" name="userId" id="userId" value="<?php echo $_smarty_tpl->getVariable('userId')->value;?>
">
	</span>
	<input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('action')->value;?>
">
</form>

</div>
</div>
      <div class="clear"></div>
</div>


    
    <script type="text/javascript">
		//<!--
            
            $("#back").click(function(){
                
                var step = parseInt($("#step").val())-2;
                $("#step").val(step);
                $("#action").val('edit');
                document.subRegister.submit();
            });
			
			//$(document).ready(
			//function(){
				//var str = $("#errorDiv").html();
				//alert(str.length);
			//	if(str.length<=0){
			//		$("#errorDiv").hide();
			//	}else{
			//		$("#errorDiv").show();
			//	}
			//}
			
		//	);

		
        //-->
		
		function refreshpage(){
			document.venRegister.step.value='refresh';
			
			if(validateCountryCode(document.venRegister.mob_no.value)){
				$("#countryDiv").html("");
				document.venRegister.submit();
			}else{
				$("#countryDiv").html("Mobile number Entered above does not belong to the country selected");
			}
		}
		
		
		function validateCountryCode(mobile){
  			var len = mobile.length;
			var countryCode=document.venRegister.country.value;
    		if((mobile.substr(0,1) == '7' || mobile.substr(0,1) == '6') && len == 9){
				return true;
			}else if((mobile.substr(0,2) == '07' || mobile.substr(0,2) == '06') && len == 10){
       			return true;
    		}else if(len == 12 && (countryCode==mobile.substr(0,3))){
      			return true;
    		}else if (len == 13 && (countryCode==mobile.substr(1,4))){
        		return true;
    		}else {
      			return false;
   			 }
 		 }
		 
		 
		 function support_emailValidation(){
			var pattern =/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,3})$/;
			var email = $("#support_email").val();
			if(email ==""){
				$("#support_emailDiv").html(msg_01);
				return 0;
			}else if(!email.match(pattern)){
				//$("#emailDiv").html('<img src="'+imagePath+'error_icon.gif" >');
				$("#support_emailDiv").html(msg_01);
				return 0;
			}else{
				$("#support_emailDiv").html('<img src="'+imagePath+'tick_icon.gif" >');
				return true;
			}
		}
		
     </script>


