<?php /* Smarty version Smarty3-RC3, created on 2012-10-25 11:44:13
         compiled from "C:\xampp\htdocs\pspf\templates/faq.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205815088fbddcb8778-97535871%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4dec6385a5b77bca829917635bb2e7300924f820' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/faq.tpl',
      1 => 1300914000,
    ),
  ),
  'nocache_hash' => '205815088fbddcb8778-97535871',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2-old">
		 <h1><?php echo @FAQ_;?>
</h1>
		<br />
		<h2>Group Messaging</h2>
		<ol type="1">
			<li><a href="#gm1">Will my business contact list ever be shared with anyone else?</a></li>
			<li><a href="#gm2">How much does it cost to send a sms? </a></li>
			<li><a href="#gm3">How many sms&lsquo; can I send at once?</a></li>
			<li><a href="#gm4">From who will the recipient receive the sms?</a></li>
			<li><a href="#gm5">I am a broadcaster, how do I know that the sms was received?</a></li>
		</ol>
		
			<br />
		<h2>Targeted SMS Advertising</h2>
		<ol type="1">
			<li><a href="#t1">Do you send sms adverts to a large random list of numbers?</a></li>
			<li><a href="#t2">I&lsquo;m unsure how to write a sms adverts, can you help?</a></li>
		</ol>
	
		<br />
		<h2>General</h2>
		<ol type="1">
			<li><a href="#g1">How does your system work?</a></li>
			<li><a href="#g2">Do I need to install any software on my computer?</a></li>
			<li><a href="#g3">How can I pay Bongo Live!?</a></li>
		</ol>	
	
	<br />
		<h2>Group Messaging</h2>
	
			<ol type="1">
			<li><b><a name="gm1">Will my business contact list ever be shared with anyone else?</a></b></li>
				<p>No, Bongo Live! takes the utmost care that your list of contacts is securely stored and only your sms adverts are sent to your customers. You can read our privacy policy for more details.</p>
			<li><b><a name="gm2">How much does it cost to send a sms? </a></b></li>
			<p>You can learn more about our rates on the <a href="pricing.php">pricing page.</a></p>
			
			<li><b><a name="gm3">How many sms' can I send at once?</a></b></li>
			<p>There is no theoretical limit on the number of sms you can send from our system in one go. We have send thousands of messages at once. </p>
			
			<li><b><a name="gm4">From who will the recipient receive the sms?</a></b></li>
			<p>Subscribers will receive sms deals from sender names &lsquo;Bongo Live!&rsquo; </p>
			<p>Broadcasters are able to request various sender names </p>
			
			<li><b><a name="gm5">I am a broadcaster, how do I know that the sms was received?</a></b>?</li>
			<p>Bongo Live! continuously monitors its sms traffic and receives delivery reports for all message sent. </p>		
		</ol>
	
	<br />
		<h2>Targeted SMS Advertising</h2>
		<ol type="1">
			<li><b><a name="t1">Do you send sms adverts to a large random list of numbers?</a></b></li>
				<p>Blasting out messages to a random set of number does not ensure you will get a response. An sms advert about a women&lsquo;s beauty parlor could be received by a 50 year old man, now that doesn&lsquo;t make good business sense does it?</p>
			<li><b><a name="t2">I'm unsure how to write a sms adverts, can you help?</a></b></li>
			<p>Our team of marketing consultants will gladly assist you in creating well crafted sms adverts that match your marketing objectives as well as show your brand. For example, a men&lsquo;s clothing boutique in Msasani would want to target men living in the shop&lsquo;s locality who are interested in fashion. </a></p>
		</ol>
		
	<br />
		<h2>General</h2>
		<ol type="1">
			<li><b><a name="g1">How does your system work?</a></b></li>
				<p>Bongo Live!&lsquo;s uses a proprietary system to send SMS adverts for each of its clients.  As a subscriber to our sms deals our system finds deals that match your profile and interests and sends them to you. Broadcasters are able to send sms to any of their groups and contacts by signing in and using our tailored web platform. </p>
			<li><b><a name="g2">Do I need to install any software on my computer?</a></b></li>
			<p>You do not need to install any special software. Almost all computers come with web browers such as Internet Explorer, Firefox and Google Chrome that can be used to access our services through our website at www.bongolive.co.tz</a></p>
			<li><b><a name="g3">How can I pay Bongo Live!?</a></b></li>
			<p>We accept payments through ZAP, MPESA or credit transfer. ZAP &ndash; 0784 845784.  MPESA &ndash; 0763218047. 
	The mobile number you pay us using should be registered with us for your business. Cash payments in person are also accepted.</a></p>
		</ol>
			
	  </div>
      <div class="column2-ex-right-2"> &nbsp; </div>
      <div class="clear"></div>
    </div>