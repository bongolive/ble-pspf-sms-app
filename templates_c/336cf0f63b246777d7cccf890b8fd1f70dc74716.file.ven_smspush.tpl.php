<?php /* Smarty version Smarty3-RC3, created on 2016-03-03 10:28:14
         compiled from "/var/www/html/pspf/templates/vendor/ven_smspush.tpl" */ ?>
<?php /*%%SmartyHeaderCode:41901615156d7e78e98b576-41184572%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '336cf0f63b246777d7cccf890b8fd1f70dc74716' => 
    array (
      0 => '/var/www/html/pspf/templates/vendor/ven_smspush.tpl',
      1 => 1456990090,
    ),
  ),
  'nocache_hash' => '41901615156d7e78e98b576-41184572',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_wordwrap')) include '/var/www/html/pspf/smarty/plugins/modifier.wordwrap.php';
?><!--header start-->
<!--header end-->
<style type="text/css">
<!--
.hidden { display: none; }
.unhidden { display: block; }

.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 165% /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
.spinnerDiv {
		position: absolute;
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 40%; 
	    left:50%; 
	    text-align:center;
	    z-index:1236;
	    overflow: auto;
	   /* width: 100px;  width of the spinner gif */
	   /* height: 102px; hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
	/*.search-box form input[type="text"]*/
@charset "utf-8";
/* CSS Document */

*{
	margin:0;
	padding:0;
}

.clr{
   clear:both;
}
/*-------------------wrapper------------------*/
#wrapper{ 
   
    margin: 0;
    padding: 0; margin-bottom:25px;

}

.center-wrapper{
   margin:0 auto;
    width:620px;
}

 
/*-------------------header------------------*/ 
#header-page{
  float:left;
  width:700px;
  margin-left:21px;
}

.top-navi2{
  float:left;
  width:348px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color:#E9CFEC;
}

.top-navi3{
  float:left;
  width:495px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
}

.top-navi4{
  float:left;
  width:631px;
  height:30px;
  margin-left:21px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
}

.top-navi{
  float:left;
  width:480px;
  height:30px;
 /* background-image:url(images/top-navi-bg.png);
  background-repeat:repeat-x; */
 background-color: #E9CFEC;
  margin-top:10px;
}

.top-navi ul{
 list-style:none;
}



.content{
   width:850px;
   height:120px;
   border:1px solid #bbbbbb;
   margin-top:100px;
   margin-left:21px; 
   border-radius: 9px 9px 9px 9px;
}


.content ul{
  list-style:none;
}

.content ul li{
border: 1px solid #DDDDDD;
    float: left;
    margin-left: 17px;
    margin-top: 20px;
    padding: 4px 10px;
    text-align: center;}


.one{ 
   color: #000;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.two{ 
   color: #cc0000;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.three{ 
   color: #339900;
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

.four{ 
    font-family: arial;
    font-size: 15px;
    line-height: 24px;
    text-decoration: none;
}

span.total{ 
   	color: #000;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}

span.invalid{ 
   color: #cc0000;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
    font-weight:bold;
}

span.valid{ 
   color: #339900;
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}

span.smscount{ 
    font-family: arial;
    font-size: 20px;
    line-height: 24px;
    text-decoration: none;
	 font-weight:bold;
}


.clear{
	 clear:both;
}

.smsPreview{
   width:850px;
   height:auto;
   border:1px solid #bbbbbb;
   margin-top:50px;
   margin-left:21px;
   padding-bottom: 10px; border-radius: 9px 9px 9px 9px; padding-bottom: 30px;
}


.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 680px}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 830px;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
	padding-left: 5px;
    padding-right: 5px;
    text-align: left;
    width: 370px;
	text-align: left;
		}


.row-2{
 padding-right: 5px;
 text-align: left;
 width:120px;
 text-align: left;
}

.row-3{
 width:100px;
 padding-right: 5px;
    text-align: center;
}

.row-4{
 width:120px;
 padding-right: 5px;
    text-align: center;
}

.row-5{
 padding-right: 5px;
    text-align: center;
    width: 90px;
}

.row-6{
 width:120px;
 text-align: left;
 padding:5px;
}

.para{
  
border: 1px solid #ABADB3;
    color: #000000 !important;
    margin-top: 2px;
    padding: 0 2px;
    width: 350px;}

.footer-top{
    border: 1px solid #BBBBBB;
    height: 90px;
    margin-top: 40px;
	margin-left:21px;
    position: relative;
    width: 850px; border-radius: 9px 9px 9px 9px; 
}

.footer-bottom{
   float:left;
   width:700px;
   margin-top:40px;
   margin-left:2px;
}

.footer-bottom3{
   float:left;
   width:850px;
   margin-top:40px;
   margin-left:20px;
}


.left-bottom{
   float:left;
   width:200px;
}


.right-bottom{
     float: right;
    width: 196px;
}


.back{
   float:right;
   margin-left:20px;
}

.summy{
 margin-left: 15px;
    margin-top: -15px;
}


.center-job{
 margin-left: 15px;
    margin-top: -12px;
}


.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}

.title
{
color: #555555;
font-size: 20px;
margin-left: 13px;
margin-top: 10px;
}


.content-sec {
    border: 3px;
    height: 208px;
    margin-top: 8px;
    padding: 9px;
    width: 733px;
}

.second-nav li
{
display: inline;
    float: left;
    font-family: arial;
    font-size: 14px;
    font-weight: bold;
    line-height: 30px;
    list-style-image: none;
    list-style-position: outside;
    margin-right: 0px;
	margin-left:21px; 
	color: #AAAAAA;
	}

.second-nav ul
{
margin-top:10px;
}

.second-nav li.first-option
{
background-image: url("templates/assets/images/filesms_arrow.png");
    background-repeat: no-repeat;
    color: #FFFFFF;
    height: 31px;
    padding-left: 21px;
    width: 151px;}
	
.second-nav li.first-option2
{
background-image: url("templates/assets/images/filesms_arrow.png");
    background-repeat: no-repeat;
    color: #FFFFFF;
    height: 31px;
	padding-left: 21px;
    width: 151px;}

.btm-content
{
border: 1px solid #ccccd2;
height:auto;
margin-left: 21px;
margin-top: 40px;
width: 700px;border-radius: 9px 9px 9px 9px;
}

.btm-help
{
border: 1px solid #ccccd2;
height:auto;
margin-left: 21px;
margin-top: 40px;
width: 200px;border-radius: 9px 9px 9px 9px;
background-color:#f8f8f8;
}

.file-upload{

background-image: url("templates/assets/images/select_contacts.png");
background-repeat: no-repeat;
height: 28px;
margin-left: 14px;
margin-top: -12px;
position: absolute;
width: 124px;
}

.file-create-message{

background-image: url("templates/assets/images/file_create_message.png");
background-repeat: no-repeat;
height: 28px;
margin-left: 14px;
margin-top: -12px;
position: absolute;
width: 150px;
}

.instrution
{
color: #CCCCD2;
    font-family: arial;
    font-size: 13px;
    font-weight: bold;
    margin-left: 18px;
    margin-top: 28px;}

.upload
{

margin-left: 18px;
    margin-top: 12px;
}

.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 430px;
   height: 270px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}
-->
</style>
<div id="spinner" class="spinner" style="display:none; vertical-align:middle;">
	<div id="spinner2" class="spinnerDiv">
	    <img id="img-spinner" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/ajax-loader.gif" alt="Loading"/>
	</div>
</div>
<div id="container">
<form name="smsPushForm" id="smsPushForm" method="post" action="">
<input name="action" type="hidden" id="action" value="<?php echo $_smarty_tpl->getVariable('action')->value;?>
" />
<div class="column2-ex-left-2 column2_contents">
	<h1 style="margin-bottom:30px;"><?php echo @COMPOSE_SMS;?>
</h1>
  <div id="errorDiv" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg" <?php }else{ ?> class="sucess_msg_2" <?php }?>
	<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block; margin-bottom:20px; margin-left:21px;" <?php }else{ ?> style="display:none; margin-left:21px;" <?php }?>> <?php echo $_smarty_tpl->getVariable('msg')->value;?>
	</div>
	
<!--  Content For Uploading Data start -->
 <?php if ($_smarty_tpl->getVariable('action')->value=='selectContact'){?>
 
 <!--  Menu start here  -->
 <div class="second-nav"> 
     <ul>
    	<li  class="first-option">1. Contacts/Groups</li>
    	<li >2.Create message</li>
    	<li> 3.Process/Preview </li>
   </ul>
</div>
	
<!--  Menu end here  -->
 
 <table border="0" style="margin-top:50px;">
  <tr>
    <td><div class="btm-content">
 	<div class="file-upload"> </div>
	
 	<table width="695" border="0" style="margin-top:40px; margin-left:10px; margin-bottom:40px;">
      <tr>
		<td><b><?php echo @SELECTED_GROUPS;?>
</b></td>
		<td></td>
		<td><b><?php echo @GROUPS;?>
</b></td>
	</tr>
	  <tr>
        <td>
			<span class="standard-input"  style="width:150px;">
				<select multiple id="addressbooks" name="addressbooks[]" style="height:100px; width:300px;">
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['name'] = 'addressbookSelected';
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSelectedAddbooks')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbookSelected']['total']);
?>
					<option selected value="<?php echo $_smarty_tpl->getVariable('arrSelectedAddbooks')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbookSelected']['index']]['id'];?>
">
					<?php echo $_smarty_tpl->getVariable('arrSelectedAddbooks')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbookSelected']['index']]['addressbook'];?>
 : 
					<?php echo $_smarty_tpl->getVariable('arrSelectedAddbooks')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbookSelected']['index']]['contacts_count'];?>
 </option>
					<?php endfor; endif; ?>
				 </select>
			</span>		</td>
        <td>
			<div align="center"><a href="#" id="add"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/arrow_left.png" ></a>
			  <br />
			  <br />
		                    <a href="#" id="remove"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/arrow_right.png"></a> </div></td>
        <td>
			<span class="standard-input"  style="width:150px;">
				<select multiple id="addBook" name="addBook[]" style="height:100px; width:300px;">
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['name'] = 'addressbook';
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrAddressbook')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total']);
?>
					<option value="<?php echo $_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['id'];?>
"><?php echo $_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['addressbook'];?>
 : <?php echo $_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['contacts_count'];?>
 </option>
					<?php endfor; endif; ?>
				</select>
			</span>		</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td></td>
        <td>
			<div id="menu_search2" style='cursor:pointer;padding-top:10px;padding-bottom:10px; display:inline; float:left;'></div>
			<div style="width:150px;float:left; display:inline; padding-top:10px; padding-bottom:10px; margin-right:10px;">
				<span class="standard-input" style="margin-right:3px;">
					<input type="text" name="mob_no" id="mob_no" maxlength="<?php echo $_smarty_tpl->getVariable('mob_no_length')->value;?>
" placeholder="Search Contact" value="">
				</span></div>
				
				<div style="width:150px;float:left; display:inline; padding-top:10px; padding-bottom:10px;"><input type="button" name="getContact" id="getContact" value="<?php echo @SEARCH;?>
" 
						onclick="javascript: getContactList();" class="editbtn" style="width:105px; display:inline; margin-left:3px;" ></div>
			
			
				<div id='search_select' onclick='search_hide();'> 
					<ul>
						<li class='inp_search'><input type='radio' name='type_search' id='search1' onchange='search_hide();' checked><?php echo @MOBILE_NUMBER;?>
</input></li>
						<li class='inp_search'><input type='radio' name='type_search' id='search2' onchange='search_hide();'><?php echo @FIRST_NAME;?>
</input></li>
						<li class='inp_search'><input type='radio' name='type_search' id='search3' onchange='search_hide();'><?php echo @LAST_NAME;?>
</input></li>
					</ul>
				</div>			</td>
      </tr>
      <tr>
        <td><b><?php echo @SELECTED_CONTACTS;?>
</b></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
			<span class="standard-input"  style="width:150px;">
				<select multiple id="contacts" name="contacts[]" style="height:100px; width:300px;">
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['name'] = 'selectedContacts';
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSelectedContacts')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['selectedContacts']['total']);
?>
					<option selected value="<?php echo $_smarty_tpl->getVariable('arrSelectedContacts')->value[$_smarty_tpl->getVariable('smarty')->value['section']['selectedContacts']['index']]['id'];?>
"><?php echo $_smarty_tpl->getVariable('arrSelectedContacts')->value[$_smarty_tpl->getVariable('smarty')->value['section']['selectedContacts']['index']]['mob_no'];?>
 : <?php echo $_smarty_tpl->getVariable('arrSelectedContacts')->value[$_smarty_tpl->getVariable('smarty')->value['section']['selectedContacts']['index']]['fname'];?>
 </option>
					<?php endfor; endif; ?>							 
					</select>
			</span>		</td>
        <td><div align="center"><a href="#" id="addContact"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/arrow_left.png" ></a>
          <br />
          <br />
          <a href="#" id="removeContact"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/arrow_right.png"></a>
        </div></td>
        <td><span class="standard-input"  style="width:150px;">
				<select multiple id="defaultBook" name="defaultBook[]" style="height:100px; width:300px;">
				</select>
			</span></td>
      </tr>
    </table>
	
	
	
	<div class="footer-bottom">
       <p align="left" class="left-bottom">  </p>
      <p class="right-bottom"><input type="image" src="templates/assets/images/filesms_next.png" align="left"  class="back" onclick="return validatSmspushVen();" />
      </p>
         <div class="clear"></div>
  </div>
  </div></td>
    <td valign="top">
	<div id="btm-help" class="btm-help">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;"><?php echo @VEN_COTENTHELP_GROUPSMS_STEP_01;?>
</p>
	
	</div></td>
  </tr>
</table>

 
 
 <?php }?>
<!--  Content For Selecting Addressbook/contacts  -->
	
	

  
  
  <!--  Content For  Creat Message start -->
  <?php if ($_smarty_tpl->getVariable('action')->value=='createMessage'){?>
  <script type="text/javascript">
		$(document).ready(function ()
   {
      $("#btnShowSimple").click(function (e)
      {
         ShowDialog(false);
         e.preventDefault();
      });

      $("#btnShowModal").click(function (e)
      {
         ShowDialog(true);
         e.preventDefault();
      });

      $("#btnClose").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
         
		 var errorFlag=0;
		 //if($("#repeat").val()==''){
		 //	$("#repeatErrorDiv").html("Please Select This Field");
		//	errorFlag=1;
		 //}
		 
		if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
		   	if($("#dayStart").val()==""){
				$("#startDateDiv").html("Please select Day to start");
				errorFlag=1; 
			}else if($("#monthStart").val()==""){
				$("#startDateDiv").html("Please select Month to start");
				errorFlag=1; 
			}else if($("#yearStart").val()==""){
				$("#startDateDiv").html("Please Select Year to start");
				errorFlag=1;
			}else{
				var d1=new Date();
				var d2=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				var d3=new Date(d2);
				if(d3<d1){
					$("#startDateDiv").html("Start Date Should be greater than now");
					errorFlag=1;
				}else{
					$("#scheduleDate").val(d2);
					$("#startDateDiv").html("");
					errorFlag=0;
				}
			}
		 }
		 
		 
		 
		 if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   	if($("#day").val()==""){
				$("#birthdayDiv").html("Please select Day");
				errorFlag=1; 
			}else if($("#month").val()==""){
				$("#birthdayDiv").html("Please select Month");
				errorFlag=1; 
			}else if($("#year").val()==""){
				$("#birthdayDiv").html("Please Select Year");
				errorFlag=1;
			}else{
				var d4=new Date();
				var d5=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				var d6=new Date(d5);
				var startDate=$("#scheduleDate").val();
				var d7=new Date(startDate);
				if(d6<d4){
					$("#birthdayDiv").html("End Date Should be greater than now");
					errorFlag=1;
				}else if(d6<d7){
					$("#birthdayDiv").html("End Date Should be greater than Start Date");
					errorFlag=1;
				}else{
						$("#birthdayDiv").html("");
						errorFlag=0;
				}
			}
		 }
		 
		 
		 if(errorFlag==1){
		 	return false;
		 }else{
		 	$("#repeatErrorDiv").html("");
			$("#birthdayDiv").html("");
			$("#startDateDiv").html("");
			
			//Assign new data to hidden controls on a page
			$("#repeat2").val($("#repeat").val());
			$("#repeat_every2").val($("#repeat_every").val());
			var start_date='';
			
			var rType = document.smsPushForm.elements["reminder_type[]"];
			for(i=0;i<rType.length;i++)
			{
 				if(document.smsPushForm.reminder_type[i].checked){
					var mkk=document.smsPushForm.reminder_type[i].value;
					$("#reminder_type2").val(mkk);
				}
			}
			
			
			
			if($("#reminder_type2").val()=='sms'){
				if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
					start_date=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				
					if(document.getElementById("advOptionDiv").style.display=='none'){
						$("#advOptionDiv").show();
						$("#scheduleDate").attr("disabled",false);
						$("#advOptionTd").html('- '+msg_26);
					}
					$("#scheduleDate").val(start_date);
				
		 		}else{
					start_date="Current Time.";
					$("#scheduleDate").val('');	
				}	
			}else{
				$("#scheduleDate").val('');
				$("#birthdayfield").val($("#startDateColumn").val());
				start_date ="Pick from "+$("#startDateColumn").val()+' Field';	
			}
				
			
			
			if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
				var end_date=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				$("#end_date").val(end_date);
		 	}
			
			var expression='';
			if($("#repeat").val()=='WEEK'){
				var rpDays = document.smsPushForm.elements["repeat_days[]"];
				for(i=0;i<rpDays.length;i++)
				{
 					if(document.smsPushForm.repeat_days[i].checked){
						expression = expression+document.smsPushForm.repeat_days[i].value+',';
					}
				}
				
				if(expression.length >0){
				 expression = expression.substring(0, expression.length - 1);
				 $("#repeat_days2").val(expression);
				}
			}else if($("#repeat").val()=='MONTH'){
				expression='Monthly on a day.';
			}else if($("#repeat").val()=='YEAR'){
				expression='Yearly on a day.';
			}else if($("#repeat").val()=='DAY'){
				expression='Daily.';
			}
			
			//Writting Summary for User preview
			if($("#repeat").val()==''){
				$("#scheduleDate").val('');
				var summary= 'Never Repeat.';
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}
		 }

		 
		 $("#repeatSummary").html(summary);
		 
		 //var brand = $("#brands input:radio:checked").val();
         //$("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

   });

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   }
   
   function showStartDate(){
   		$("#smsStartDateDiv").show();
		$("#birthdayStartDateDiv").hide();	
   }
   
   function hideStartDate(){
   		$("#birthdayStartDateDiv").show();
		$("#smsStartDateDiv").hide();	
   }
   
    function pickRepeat(){
   	if($("#repeat").val()=="DAY"){
		$("#repeatDiv").html("Day");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="WEEK"){
		$("#repeatDiv").html("Week");
		$("#repeatDaysRow").show();
		$("#repeat_every").val(1);
		$("#repeat_every").attr('disabled', true);
	}else if($("#repeat").val()=="MONTH"){
		$("#repeatDiv").html("Month");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="YEAR"){
		$("#repeatDiv").html("Year");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else{
		$("#repeatDiv").html("");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}
	
   }
   
   
   function changeNumberOfRepeat(){
   		if($("#repeat_every").val()>1){
			if($("#repeat").val()=="DAY"){
				$("#repeatDiv").html("Days");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="WEEK"){
				$("#repeatDiv").html("Weeks");
				document.getElementById(repeatDaysRow).style.display="block";
			}else if($("#repeat").val()=="MONTH"){
				$("#repeatDiv").html("Months");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="YEAR"){
				$("#repeatDiv").html("Years");
				document.getElementById(repeatDaysRow).style.display="none";
			}else{
				$("#repeatDiv").html("");
				document.getElementById(repeatDaysRow).style.display="none";
			}
		}
   }	
	</script>
  <!--  Menu start here  -->        
<div class="second-nav"> 
     <ul>
    	<div class="top-navi2">
			<li>1. Contacts/Groups</li>
    		<li class="first-option2">2.Create message</li>
		</div>
    	<li> 3.Process/Preview </li>
   	 </ul>
</div>
<!--  Menu end here  -->
  
  
  
  <table border="0" style="margin-top:50px;">
  <tr>
    <td valign="top"><div class="btm-content">
 <div class="file-create-message"> </div>
 
 
 
 
 
 
 
 <div id="overlay" class="web_dialog_overlay"></div>
   
<div id="dialog" class="web_dialog">
	<div id="output"></div>
   <div style="text-align:right; margin-right:10px; margin-top:10px; display:inline; float:right;"><a href="#" id="btnClose">Close</a></div>
   <div style="float:left; display:inline;"><h2 style="margin-left:20px;">Advanced Settings</h2></div>
   <table width="440" border="0" align="center" cellpadding="10" cellspacing="10" style="margin-left:20px;">
 <tr>
 	<td width="110">Reminder Type</td>
	<td width="330">
		<label><input name="reminder_type[]" id="reminder_type" type="radio" value="sms" checked  onclick="showStartDate();" />SMS Reminder</label>
		<label style="margin-left:15px;"><input name="reminder_type[]" id="reminder_type" type="radio" value="birthday" onclick="hideStartDate();" />Use Contact Dates</label>
	</td>
 </tr>
  <tr>
    <td valign="top" >Repeat</td>
    <td ><div>
	
	<select name="repeat" id="repeat" onchange="pickRepeat();"style="border:2px solid #ccc;" >
		<option value="">Never</option>
		<option value="DAY">Daily</option>
		<option value="WEEK">Weekly</option>
		<option value="MONTH">Monthly</option>
		<option value="YEAR">Yearly</option>
	</select>

	</div> <span id="repeatErrorDiv" class="form_error"></span></td>
  </tr>
  <tr>
    <td valign="top">Repeat Every</td>
    <td valign="bottom">
	<select id="repeat_every" name="repeat_every" onchange="changeNumberOfRepeat();" style="border:2px solid #ccc;" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
	</select>
	<label id="repeatDiv"></label>
	
	</td>
  </tr>
  <tr id="repeatDaysRow" style="display:none;">
    <td valign="top">Repeat Days</td>
    <td>
		<div>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Sunday" />S</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Monday" />M</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Tuesday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Wednesday" />W</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Thursday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Friday" />F</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Saturday" />S</label>
		</div>
	</td>
  </tr>
  <tr id="startDateRow">
  	<td>Start Date</td>
	<td>
		<div id="smsStartDateDiv" style="display:block;">
		<select name="dayStart" id="dayStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['name'] = 'arrDay2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'] = is_array($_loop=31) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total']);
?> 
							<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration'];?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration']==$_smarty_tpl->getVariable('day')->value){?> selected<?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration'];?>
</option>
							<?php endfor; endif; ?>
						</select>

				  
				  
				    	 <select name="monthStart" id="monthStart" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" <?php if ($_smarty_tpl->getVariable('month')->value==1){?> selected<?php }?>>Jan</option>
							<option value="2" <?php if ($_smarty_tpl->getVariable('month')->value==2){?> selected<?php }?>>Feb</option>
							<option value="3" <?php if ($_smarty_tpl->getVariable('month')->value==3){?> selected<?php }?>>Mar</option>
							<option value="4" <?php if ($_smarty_tpl->getVariable('month')->value==4){?> selected<?php }?>>Apr</option>
							<option value="5" <?php if ($_smarty_tpl->getVariable('month')->value==5){?> selected<?php }?>>May</option>
							<option value="6" <?php if ($_smarty_tpl->getVariable('month')->value==6){?> selected<?php }?>>Jun</option>
							<option value="7" <?php if ($_smarty_tpl->getVariable('month')->value==7){?> selected<?php }?>>Jul</option>
							<option value="8" <?php if ($_smarty_tpl->getVariable('month')->value==8){?> selected<?php }?>>Aug</option>
							<option value="9" <?php if ($_smarty_tpl->getVariable('month')->value==9){?> selected<?php }?>>Sep</option>
							<option value="10" <?php if ($_smarty_tpl->getVariable('month')->value==10){?> selected<?php }?>>Oct</option>
							<option value="11" <?php if ($_smarty_tpl->getVariable('month')->value==11){?> selected<?php }?>>Nov</option>
							<option value="12" <?php if ($_smarty_tpl->getVariable('month')->value==12){?> selected<?php }?>>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="yearStart" id="yearStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['name'] = 'arrYear2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'] = is_array($_loop=10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total']);
?> 
							<option value="<?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration']);?>
" <?php if ($_smarty_tpl->getVariable('year')->value==($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration'])){?> selected<?php }?>><?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration']);?>
</option>
							<?php endfor; endif; ?>
						</select>
						 
					
						 <select name="hourStart" id="hourStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['name'] = 'fooStart';
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total']);
?> 
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['fooStart']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['fooStart']['index'];?>
</option>
							<?php endfor; endif; ?> 
						 </select>
					
					 
					 
					<!--	 <select name="minutesStart" id="minutesStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['name'] = 'foo2Start';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] = is_array($_loop=60) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total']);
?>
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2Start']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2Start']['index'];?>
</option>
							<?php endfor; endif; ?>
						 </select> -->
					 </div>
					 <div id="birthdayStartDateDiv" style="display:none;">
					 	<select name="startDateColumn" id="startDateColumn" style="border:2px solid #ccc; width:120px;">
							<option value="birth_date"><?php echo @BIRTH_DATE;?>
</option>
							<option value="optional_one"><?php echo @OPTIONAL_ONE;?>
</option>
			  				<option value="optional_two"><?php echo @OPTIONAL_TWO;?>
</option>
						</select>
					 </div>	 
						 
						 <span id="startDateDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td valign="top">End Date</td>
    <td>
	
	
				    	 <select name="day" id="day" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['name'] = 'arrDay';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'] = is_array($_loop=31) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total']);
?> 
							<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration']==$_smarty_tpl->getVariable('day')->value){?> selected<?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
</option>
							<?php endfor; endif; ?>
						</select>

				  
				  
				    	 <select name="month" id="month" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" <?php if ($_smarty_tpl->getVariable('month')->value==1){?> selected<?php }?>>Jan</option>
							<option value="2" <?php if ($_smarty_tpl->getVariable('month')->value==2){?> selected<?php }?>>Feb</option>
							<option value="3" <?php if ($_smarty_tpl->getVariable('month')->value==3){?> selected<?php }?>>Mar</option>
							<option value="4" <?php if ($_smarty_tpl->getVariable('month')->value==4){?> selected<?php }?>>Apr</option>
							<option value="5" <?php if ($_smarty_tpl->getVariable('month')->value==5){?> selected<?php }?>>May</option>
							<option value="6" <?php if ($_smarty_tpl->getVariable('month')->value==6){?> selected<?php }?>>Jun</option>
							<option value="7" <?php if ($_smarty_tpl->getVariable('month')->value==7){?> selected<?php }?>>Jul</option>
							<option value="8" <?php if ($_smarty_tpl->getVariable('month')->value==8){?> selected<?php }?>>Aug</option>
							<option value="9" <?php if ($_smarty_tpl->getVariable('month')->value==9){?> selected<?php }?>>Sep</option>
							<option value="10" <?php if ($_smarty_tpl->getVariable('month')->value==10){?> selected<?php }?>>Oct</option>
							<option value="11" <?php if ($_smarty_tpl->getVariable('month')->value==11){?> selected<?php }?>>Nov</option>
							<option value="12" <?php if ($_smarty_tpl->getVariable('month')->value==12){?> selected<?php }?>>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="year" id="year" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['name'] = 'arrYear';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'] = is_array($_loop=10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total']);
?> 
							<option value="<?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
" <?php if ($_smarty_tpl->getVariable('year')->value==($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration'])){?> selected<?php }?>><?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
</option>
							<?php endfor; endif; ?>
						</select>
						 
					
						 <select name="hour" id="hour" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['name'] = 'foo';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total']);
?> 
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo']['index'];?>
</option>
							<?php endfor; endif; ?> 
						 </select>
					
					 
					 
						<!-- <select name="minutes" id="minutes" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['name'] = 'foo2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] = is_array($_loop=60) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total']);
?>
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2']['index'];?>
</option>
							<?php endfor; endif; ?>
						 </select>  -->
					 		 
						 
						 <span id="birthdayDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input id="btnSubmit" type="button" value="Ok" style="border:0px solid #93278F; width:100px; height:31px; background:url(templates/assets/images/btn_bg.png) no-repeat left; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif; font-size:1.2em; vertical-align:text-top; text-align:center; color:#FFFFFF;" /></td>
  </tr>
</table>

</div>
 
 
 
 
 
 
 
 
 
 <table width="600" border="0" cellpadding="10" cellspacing=4" style="margin-top:40px; margin-left:10px; margin-bottom:20px;">
   <tr>
     <td><?php echo @SENDER_NAME;?>
:</td>
     <td><div align="left" style="width:40px; margin-bottom:4px;"><span class="form_dinatrea standard-input">
            <select name="senderid" id="senderid" class="select">
               <option value=""><?php echo @SELECT;?>
</option>                             
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['name'] = 'senderid';
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSenderid')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total']);
?>                    
                 <option value="<?php echo $_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('senderid')->value==$_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['id']){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['senderid'];?>
</option>                    
				<?php endfor; endif; ?>    
             </select>
          <span id="senderidDiv" class="form_error"></span></div><img alt="Help" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/filesms_What-this.png" width="20" height="20" id="senderidtip" title="Content for senderid help goes here"><a href="JavaScript:newPopup('popup_senderid.php');" style=" margin-left:20px;"><?php echo @REQUEST_NEW;?>
</a>
 </td>
   </tr>
   <tr>
     <td></td>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td><?php echo @VEN_MENUTOP_BROADCAST_SMS_8;?>
</td>
     <td>
	 	<div id="smstemplate"  style="width:40px"><span class="form_dinatrea standard-input">
			<select name="templatesms" id="smstemplate" onchange="addsmstemplate();" style="margin-right:20px;">
               <option value=""><?php echo @SELECT;?>
</option> 
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['name'] = 'sms';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrsmstemplate')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total']);
?>
				<option value="<?php echo $_smarty_tpl->getVariable('arrsmstemplate')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['message'];?>
"><?php echo $_smarty_tpl->getVariable('arrsmstemplate')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['sms_title'];?>
</option>    
				<?php endfor; endif; ?>
			</select>
		</span></div><img alt="Help" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/filesms_What-this.png" width="20" height="20" id="smsTemplatetip" title="Content of Help for sms templates goes here"><a href="JavaScript:newPopup('ven_sms_template.php');" style=" margin-left:20px;"><?php echo @VEN_MENUTOP_BROADCAST_SMS_9;?>
</a></td>
   </tr>
   <tr>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td><?php echo @PLACE_HOLDER;?>
:</td>
     <td>
	 	<span class="form_dinatrea standard-input" style="margin-bottom:5px;">
            <select name="placeHolder" id="PlaceHolder" class="select" onchange="addPlaceholder();">
              <option value=""><?php echo @SELECT;?>
</option>
			  <option value="mobile"><?php echo @MOBILE_NUMBER;?>
</option>
			  <option value="first_name"><?php echo @FIRST_NAME;?>
</option>
			  <option value="last_name"><?php echo @LAST_NAME;?>
</option>
			  <option value="title"><?php echo @TITLE;?>
</option>
			  <option value="birth_date"><?php echo @BIRTH_DATE;?>
</option>
			  <option value="gender"><?php echo @GENDER_;?>
</option>
			  <option value="email" ><?php echo @EMAIL;?>
</option>
			  <option value="optional_one"><?php echo @OPTIONAL_ONE;?>
</option>
			  <option value="optional_two"><?php echo @OPTIONAL_TWO;?>
</option>
			  <option value="area">Area</option>
			  <option value="city"><?php echo @CITY;?>
</option>
			  <option value="country"><?php echo @COUNTRY;?>
</option> 				   
            </select>
        </span><img alt="Help" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/filesms_What-this.png" width="20" height="20" id="placeholderTooltip" title="Content for Placeholder tip" style="float:left; margin-left:1px;">	 </td>
   </tr>
   <tr>
     <td><input name="repeat2" type="hidden" id="repeat2" value="" />
        <input name="repeat_every2" type="hidden" id="repeat_every2" value="" />
        <input name="repeat_days2" type="hidden" id="repeat_days2" value="" />
        <input name="end_date" type="hidden" id="end_date" value="" />
        <input name="reminder_type2" type="hidden" id="reminder_type2" value="" />
        <input name="birthdayfield" type="hidden" id="birthdayfield" value="" /></td>
     <td>&nbsp;</td>
   </tr>
     <tr>
         <td valign='top'><?php echo @MESSAGE_BODY;?>
:</td>
         <td><textarea name="textMessage" id="textMessage" cols="40" rows="6" class="textarea"><?php echo $_smarty_tpl->getVariable('textMessage')->value;?>
</textarea>
             <span id="textMessageDiv" class="form_error" style="width:300px"></span>	 </td>
     </tr>
     <tr>
         <td>&nbsp;</td>
         <td><div style="width:100%; float:left;">Msgs. / Chars. Remaining<span id="charCount">( 0 ) 0</span></div>
             <div style="width:70%; float:left;" id="messageCount"></div></td>
     </tr>
     <tr>
     <tr><td><strong>Message Type :</strong> </td>
         <td><input type="radio" name="m_type" checked value="m_text">Text
             <input type="radio" name="m_type" value="m_unicode">Unicode</td></tr>
     <tr>
         <td>&nbsp;</td>
         <td>&nbsp;</td>
     </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
   <tr>
      <td valign="top" width="200"><div align="left"><a id="advOptionTd" style="cursor:pointer; font-weight:bold;" onclick="javascript: showHideAdvOption();"> <?php if ($_smarty_tpl->getVariable('scheduleDate')->value&&$_smarty_tpl->getVariable('scheduleDate')->value!=''){?> - <?php echo @SCHEDULE_DATE;?>
 <?php }else{ ?>
                + <?php echo @SCHEDULE_DATE;?>
 <?php }?> </a></div>	  </td>
      <td><div id="advOptionDiv" <?php if ($_smarty_tpl->getVariable('scheduleDate')->value&&$_smarty_tpl->getVariable('scheduleDate')->value!=''){?>style="display:block" <?php }else{ ?>style="display:none"<?php }?>> <span class="form_dinatrea standard-input">
          <input type="text" name="scheduleDate" id="scheduleDate" value="<?php echo $_smarty_tpl->getVariable('scheduleDate')->value;?>
" /></span> </div>	  </td>
    </tr>
	
	<tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
	<tr>
      <td valign="top"><div id="btnShowModal" title="Click Me to view advanced Settings" style="cursor:pointer; font-weight:bold;">+ Custom Reminders</div></td>
      <td><div id="repeatSummary"></div></td>
    </tr>
 </table>
</div>
  <div class="footer-bottom">
       <p align="left" class="left-bottom"><img src="templates/assets/images/filesms_cancel-button.png" onclick="canceljob();" /></p>
       <p class="right-bottom">
	   <input type="image" src="templates/assets/images/filesms_next.png" align="left"  class="back"  onclick="return validatSmspushVen();" />  <img src="templates/assets/images/filesms_back.png" align="left" class="back" onclick="backtoone();" />
    </p>
         <div class="clear"></div>
  </div></td>
    <td valign="top"><div id="btm-help" class="btm-help">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px;  font-size:12px;"><?php echo @VEN_COTENTHELP_GROUPSMS_STEP_02;?>
</p>
	
	</div></td>
  </tr>
</table>

  
  
  
  
<?php }?>  
  <!--  Content For  Creat Message end -->
  
  
  
  
  <!--  Content For  proccess/preview start -->
  
  <?php if ($_smarty_tpl->getVariable('action')->value=='SMSpreview'){?>
  
  <!--  Menu start here  -->
<div class="second-nav"> 
     <ul>
    	<div class="top-navi3">
			<li>1. Contacts/Groups</li>
    		<li>2.Create message</li>
    		<li  class="first-option2"> 3.Process/Preview </li>
		</div>
   </ul>
</div>
<!--  Menu end here  -->
  
  
  
  <div class="content">
        <a href="#"><img src="templates/assets/images/filesms_summy.png" class="summy"/></a>
        <ul> 
            <li class="one"><?php echo @TOTAL_RECORDS;?>
<br /><span class="total"><?php echo $_smarty_tpl->getVariable('TotalContact')->value;?>
</span></li>
            <li class="two"><?php echo @INVALID_RECORDS;?>
<br /><span  class="invalid"><?php echo $_smarty_tpl->getVariable('CountInvalidContact')->value;?>
</span></li>
			<li class="two">Duplicate Records<br />
		  <span  class="invalid"><?php echo $_smarty_tpl->getVariable('CountDuplicateContact')->value;?>
</span></li>
            <li class="three"><?php echo @VALID_RECORDS;?>
<br /><span  class="valid"><?php echo $_smarty_tpl->getVariable('CountValidContact')->value;?>
</span></li>
			<li class="four"><?php echo @CREDITS_BALANCE;?>
<br /><span  class="smscount"><?php echo $_smarty_tpl->getVariable('credit')->value;?>
</span></li>
			<li class="four"><?php echo @CREDITS_COSTS;?>
<br /><span  class="smscount"><?php echo $_smarty_tpl->getVariable('smsCount')->value;?>
</span></li>
        </ul>   
  </div>
  
  <div class="smsPreview">
      <img src="templates/assets/images/filesms_center.png" class="center-job" />
       
	   
	  <?php if ($_smarty_tpl->getVariable('repeat')->value=='sms'){?>
	   <table border="0" style="margin-top:20px;" width="680">                        
            <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;"> 
				 <td class="row-1"><strong><?php echo @TEXT_MESSAGE;?>
</strong></td>
				 <td class="row-2"><strong><?php echo @RECIPIENTS;?>
</strong></td>  
                 <td class="row-2"><strong><?php echo @SENDER_NAME;?>
</strong></td> 
				 <td class="row-3"><strong><?php echo @CHARACTER_COUNT;?>
</strong></td> 
				 <td class="row-4"><strong><?php echo @SMS_COUNT;?>
</strong></td> 
			</tr>                                                    
                  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['name'] = 'sample';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSampleSMS')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total']);
?>
            <?php if ($_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][0]!=''){?>
			<tr class="row-body"> 
					<td class="row-1"><?php echo smarty_modifier_wordwrap($_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][0],40,"<br />\n");?>
</td>
					<td class="row-2"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][1];?>
</td>  
					<td class="row-2"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][2];?>
</td> 
					<td class="row-3"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][3];?>
</td>  
					<td class="row-4"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][4];?>
</td>   
			</tr>
			<?php }?>
				<?php endfor; endif; ?>     
       </table>
	   <?php }else{ ?>
	   		<table border="0" style="margin-top:20px;" width="680">                        
            <tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;"> 
				 <td class="row-1"><strong><?php echo @TEXT_MESSAGE;?>
</strong></td>
				 <td class="row-2"><strong><?php echo @RECIPIENTS;?>
</strong></td>  
                 <td class="row-2"><strong><?php echo @SENDER_NAME;?>
</strong></td> 
				 <td class="row-3"><strong><?php echo @CHARACTER_COUNT;?>
</strong></td> 
				 <td class="row-4"><strong><?php echo @SMS_COUNT;?>
</strong></td>
				 <td class="row-5"><strong><?php echo @BIRTH_DATE;?>
</strong></td> 
			</tr>                                                    
                  <?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['name'] = 'sample';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSampleSMS')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sample']['total']);
?>
            <?php if ($_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][0]!=''){?>
			<tr class="row-body"> 
					<td class="row-1"><?php echo smarty_modifier_wordwrap($_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][0],40,"<br />\n");?>
</td>
					<td class="row-2"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][1];?>
</td>  
					<td class="row-2"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][2];?>
</td> 
					<td class="row-3"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][3];?>
</td>  
					<td class="row-4"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][4];?>
</td>
					<td class="row-5"><?php echo $_smarty_tpl->getVariable('arrSampleSMS')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sample']['index']][5];?>
</td>   
			</tr>
			<?php }?>
				<?php endfor; endif; ?>     
       </table>
	   <?php }?>
  </div>
             
             
      <div class="footer-top">
           <div style="margin:20px 0 20px 20px;">
             <img src="templates/assets/images/filesms_save-job.png" class="save-job"/>
             <table width="500" border="0">
                  <tr>
                    <td width="150"><?php echo @JOB_NAME;?>
:</td>
                    <td width="300"><span class="form_dinatrea standard-input">
                      <input type="text" name="jobname" id="jobname" value="<?php echo $_smarty_tpl->getVariable('jobname')->value;?>
" /></td>
                  </tr>
             </table>
       	</div>
          <div class="clear"></div>
     </div>       
        
        
      <div class="footer-bottom3">
       <p align="left" class="left-bottom"><img src="templates/assets/images/filesms_cancel-button.png" onclick="canceljob();" /></p>
       <p class="right-bottom">
	   <?php if ($_smarty_tpl->getVariable('CountValidContact')->value!=0){?>
       <?php if ($_smarty_tpl->getVariable('smsCount')->value<=$_smarty_tpl->getVariable('credit')->value){?>
	   <input type="image" src="templates/assets/images/file_send.png" align="left" class="back" onclick="SendForm();"/> 
	   <?php }?> 
	   <?php }?>
	   <img src="templates/assets/images/filesms_back.png" align="left" class="back" onclick="backtotwo();" />
        </p>
         <div class="clear"></div>
  </div>  
			 
  
  
  <?php }?>
  <!--  Content For  proccess/preview start -->
  </div>
<div class="clear"></div>
</form>
</div>

    
    <script type="text/javascript">
	//<!--
		 
		 function backtotwo(){
		 	document.smsPushForm.action.value='backTo2';
			if(document.smsPushForm.action.value=='backTo2'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function backtoone(){
		 	document.smsPushForm.action.value='backTo1';
			if(document.smsPushForm.action.value=='backTo1'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function refreshPage(){
		 	document.smsPushForm.action.value='refresh';
			if(document.smsPushForm.action.value=='refresh'){
				$('#spinner').show();
				document.smsPushForm.submit();
			}
		 }
		 
		 function canceljob(){
		 	document.smsPushForm.action.value='cancel';
			var con = confirm("Are You Sure ?");
			if (con ==true)
  			{
  				if(document.smsPushForm.action.value=='cancel'){
					$('#spinner').show();
					document.smsPushForm.submit();
				}
			}
		 }
		 
		 function SendForm(){
			$('#spinner').show();
			document.smsPushForm.submit();
		}
		
		function addPlaceholder(){
			var plceholder = document.smsPushForm.placeHolder.value;
			plceholder	='{'+plceholder+'}';
			insertAtCursor(document.smsPushForm.textMessage, plceholder);
			
		}
		
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
	function insertAtCursor(myField, myValue) {
		//IE support
		if (document.selection) {
			myField.focus();
			sel = document.selection.createRange();
			sel.text = myValue;
		}
		//MOZILLA/NETSCAPE support
		else if (myField.selectionStart || myField.selectionStart == '0') {
			var startPos = myField.selectionStart;
			var endPos = myField.selectionEnd;
			myField.value = myField.value.substring(0, startPos)
+ myValue
+ myField.value.substring(endPos, myField.value.length);
		} else {
			myField.value += myValue;
		}
	}

function getContactList(){

			var mob_no= $("#mob_no").val();
			if($('#search1').is(":checked")){
				var search_sel = 1;
			}
			else if($('#search2').is(":checked")){
				var search_sel = 2;
			}
			else{
				var search_sel = 3;
			}
			$("#defaultBook").html('');

			$.ajax({ 
				
				url: "ven_smspush_ajax.php", 
				type: "POST",                         
				data: "mob_no="+mob_no+"&search="+search_sel+"&action=getDefaultContact",            
				success: function (response) {                
					
					$("#defaultBook").html(response);
					
				}
			});
			
		}
		
		$().ready(function() {
			$('#add').click(function() {
				return !$('#addBook option:selected').remove().appendTo('#addressbooks');
			});
			$('#remove').click(function() {
				return !$('#addressbooks option:selected').remove().appendTo('#addBook');
			});
			$('#addContact').click(function() {
				
				return !$('#defaultBook option:selected').remove().appendTo('#contacts');
			});
			$('#removeContact').click(function() {
				return !$('#contacts option:selected').remove().appendTo('#defaultBook');
			});
		});

function validatSmspushVen(){
	var errorFlag = 0;	
	if($("#action").val()=='selectContact'){
		if($("#addressbooks option").html()==null &&  $("#contacts option").html()==null){
			$("#errorDiv").html("<?php echo @SELECT_GROUP_OR_CONTACT;?>
")
			$("#errorDiv").addClass('error_msg');
			$("#errorDiv").show('slow');	
			errorFlag = 1;
			return false;
		}else{	
			$('#addressbooks option').each(function(i) {
				$(this).attr("selected", "selected");
			});

			$('#contacts option').each(function(i) {
				$(this).attr("selected", "selected");
			});

			$("#errorDiv").removeClass();
			$("#errorDiv").hide('slow');
		}
	}//end of first step
	
	
	if($("#action").val()=='createMessage'){

        if(  $("input[type='radio'][name='m_type']:checked").val() == "m_unicode"){
            $("#textMessageDiv").html("<?php echo @ENTER_TEXT_MASSAGE_ONLY;?>
");
            alert("<?php echo @ENTER_TEXT_MASSAGE_ONLY;?>
");
            return false;
        }

        if($("#senderid").val()==''){
			$("#senderidDiv").html("<?php echo @SELECT_SENDER_NAME;?>
");
			errorFlag = 1;
		}else{
			$("#senderidDiv").html("");
		}
		
		
		if($("#textMessage").val()==''){
			$("#textMessageDiv").html("<?php echo @ENTER_TEXT_MASSAGE;?>
");
			errorFlag = 1;
		}else{
			$("#textMessageDiv").html("");
		}
	}
	
	
	
	if(errorFlag == 0){
		$('#spinner').show();
		document.smsPushForm.submit();
	}else{
		return false;
	}
}

function addsmstemplate() {
	document.smsPushForm.textMessage.value=document.smsPushForm.smstemplate.value;
}

        //-->
     </script>
	 
	 
	 <script type="text/javascript">
	$(document).ready(function(){
	    $("#spinner").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
	    });
	 
	     });
		 
		 
		  window.onload=function() {
 			$("#myBalance").html("<?php echo $_smarty_tpl->getVariable('venCreditBalance')->value;?>
");
		}
	</script>


