<?php /* Smarty version Smarty3-RC3, created on 2015-08-15 14:09:15
         compiled from "/var/www/html/pspf/templates/vendor/ven_dashboard.tpl" */ ?>
<?php /*%%SmartyHeaderCode:205167228855cf1ddb6826c9-13244182%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9e62749d1cd848033d8d558e916157392fc0a6eb' => 
    array (
      0 => '/var/www/html/pspf/templates/vendor/ven_dashboard.tpl',
      1 => 1364972934,
    ),
  ),
  'nocache_hash' => '205167228855cf1ddb6826c9-13244182',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_date_format')) include '/var/www/html/pspf/smarty/plugins/modifier.date_format.php';
?><style>
.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 800px; /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	
.spinnerDiv {
		position: absolute;
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 40%; 
	    left:50%; 
	    text-align:center;
	    z-index:1236;
	    overflow: auto;
	   /* width: 100px;  width of the spinner gif */
	   /* height: 102px; hight of the spinner gif +2px to fix IE8 issue */ 
	}
	.footer-top{
    border: 1px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 330px;
	border-radius: 9px 9px 9px 9px; 
}

.save-job{
margin-top: -32px;
    position: absolute;
    z-index: 1;
}
.clear{
	 clear:both;
}
</style>
<div id="spinner" class="spinner" style="display:none; vertical-align:middle;">
	<div id="spinner2" class="spinnerDiv">
	    <img id="img-spinner" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/ajax-loader.gif" alt="Loading"/>
	</div>
</div>
<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1><?php echo @VEN_DASHBOARD;?>
</h1>
	
	
	<table border="1"> 
 <tr><td valign="top">
	
	
	
	
	
	<table align="center" border="0" cellpadding="10" cellspacing="10" style=" width:auto;">
  <tr>
    <td><a href="ven_addressbook.php"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/address-book.png" alt="Manage Address Book" width="165" height="113" border="0" /></a></td>
    <td><a href="ven_smspush.php"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/groupsms.png" alt="Group SMS" width="165" height="113" border="0"/></a></td>
    <td><a href="ven_smspush_quick.php"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/quicksms.png" alt="Quick SMS" width="165" height="113" border="0"/></a></td>
    <td><a href="ven_smspush_file.php"><img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/filesms.png" alt="File SMS" width="165" height="113" border="0" /></a></td>
  </tr>
</table>


<form action="" method="post" name="dashboard"> 
  
  
  
  <?php if ($_smarty_tpl->getVariable('reseller')->value==1){?>
 <table width="100%" border="0">
  <tr>
    <td>
		<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
 		<table align="center" width="225" cellpadding="0" cellspacing="10">
			<tr>
			  <td colspan="2" class="content-link_green">Purchase Requests:</td>
			</tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td width="58" class="content-link_Black">Status</td>
			<td width="208" class="content-link_Black" align="right">Count</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Pending</td><td align="right"><?php echo $_smarty_tpl->getVariable('creditStat')->value['pending'];?>
</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Allocated</td><td align="right"><?php echo $_smarty_tpl->getVariable('creditStat')->value['allocated'];?>
</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Rejected</td><td align="right"><?php echo $_smarty_tpl->getVariable('creditStat')->value['rejected'];?>
</td></tr>
		</table>
  </div>
  <div class="clear"></div>
</div> 
	</td>
    <td>
		<div class="footer-top">
   <div style="margin:20px 0 20px 20px;">
 		<table align="center" width="225" cellpadding="0" cellspacing="10">
			<tr><td colspan="2" class="content-link_green">Sender IDs:</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td class="content-link_Black">Status</td><td class="content-link_Black" align="right">Count</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Pending</td><td align="right"><?php echo $_smarty_tpl->getVariable('senderidStat')->value['pending'];?>
</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Active</td><td align="right"><?php echo $_smarty_tpl->getVariable('senderidStat')->value['active'];?>
</td></tr>
			<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
			<tr><td>Inactive</td><td align="right"><?php echo $_smarty_tpl->getVariable('senderidStat')->value['inactive'];?>
</td></tr>
		</table>
  </div>
  <div class="clear"></div>
</div>
	</td>
  </tr>
</table>

 <?php }?>

  
  
  
  
  
  
  
  
  <table width="600" border="0">
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><h2> SMS TRAFFIC </h2></td>
    </tr>
  </table>
  <div id="outerDiv" style="width:700px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
  <table width="603" border="0" style="margin-left:40px;">
  <tr>
    <td width="115">&nbsp;</td>
    <td width="478">&nbsp;</td>
  </tr>
  <tr>
    <td>Select Period : </td>
    <td><span class="form_dinatrea standard-input" style=" line-height:8px;">
	<select name="period" id="period" onchange="SendForm();">
      <option value="" <?php if ($_smarty_tpl->getVariable('period')->value==''){?> selected <?php }?>>Select</option>
      <option value="week" <?php if ($_smarty_tpl->getVariable('period')->value=='week'){?> selected <?php }?>>Past Seven Days</option>
      <option value="month" <?php if ($_smarty_tpl->getVariable('period')->value=='month'){?> selected <?php }?>>Current Month</option>
      <option value="year" <?php if ($_smarty_tpl->getVariable('period')->value=='year'){?> selected <?php }?>>Current Year</option>
    </select>
	</span>    </td>
  </tr>
</table>

<?php if ($_smarty_tpl->getVariable('expression')->value!=''){?>
<div id="chart" style="margin-top:20px; margin-bottom:20px;"></div>
</div>
<?php }else{ ?>
	<div style="margin-top:20px; margin-bottom:20px; margin-left:40px;">
	<?php if ($_smarty_tpl->getVariable('period')->value=='year'){?>
	No Message Sent in current year
	<?php }elseif($_smarty_tpl->getVariable('period')->value=='month'){?>
	No Message Sent in current month
	<?php }else{ ?>
	No Message Sent in past seven days
	<?php }?>
	</div>
	</div>
<?php }?>
</form>





</td>
<td valign="top">
<div id="reminderDiv" style=" margin-left:10px; margin-top:10px; width:220px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
<table width="220" border="0" cellpadding="5" cellspacing="10" style="background-color:#f8f8f8; border-radius: 9px 9px 9px 9px;">
  <tr>
    <td><strong>Group Name </strong></td>
    <td><strong>Last Sent Date </strong></td>
  </tr>
  <tr class="TR_spc"><td colspan="2" class="TD_spc"></td></tr>
  <?php if ($_smarty_tpl->getVariable('arrSms')->value){?>
		<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['name'] = 'sms';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSms')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total']);
?>	
  <tr>
    <td><?php echo $_smarty_tpl->getVariable('arrSms')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['addressbook'];?>

	</td>
    <td><div align="center"><?php echo smarty_modifier_date_format($_smarty_tpl->getVariable('arrSms')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['sent_time'],"%A %e %B %Y");?>
</div></td>
  </tr>
  <tr class="TR_spc"><td colspan="2" class="TD_spc"></td></tr>
  <?php endfor; endif; ?>
  <?php }else{ ?>
  <tr>
    <td colspan="2">No SMS Found</td>
    </tr>
  <?php }?>
</table>

</div> 
</td>
</table>

	
</div>
      <div class="clear"></div>
  </div>

  <script>
function renderChart() {

var data = d3.csv.parse(d3.select('#csv').text());
var valueLabelWidth = 60; // space reserved for value labels (right)
var barHeight = 20; // height of one bar
var barLabelWidth = 100; // space reserved for bar labels
var barLabelPadding = 5; // padding between bar and bar labels (left)
var gridLabelHeight = 30; // space reserved for gridline labels
var gridChartOffset = 3; // space between start of grid and first bar
var maxBarWidth = 420; // width of the bar with the max value
 
// accessor functions 
var barLabel = function(d) { return d['Name']; };
var barValue = function(d) { return parseFloat(d['Population (mill)']); };
 
// scales
var yScale = d3.scale.ordinal().domain(d3.range(0, data.length)).rangeBands([0, data.length * barHeight]);
var y = function(d, i) { return yScale(i); };
var yText = function(d, i) { return y(d, i) + yScale.rangeBand() / 2; };
var x = d3.scale.linear().domain([0, d3.max(data, barValue)]).range([0, maxBarWidth]);
// svg container element
var chart = d3.select('#chart').append("svg")
  .attr('width', maxBarWidth + barLabelWidth + valueLabelWidth)
  .attr('height', gridLabelHeight + gridChartOffset + data.length * barHeight);
// grid line labels
var gridContainer = chart.append('g')
  .attr('transform', 'translate(' + barLabelWidth + ',' + gridLabelHeight + ')'); 
gridContainer.selectAll("text").data(x.ticks(5)).enter().append("text")
  .attr("x", x)
  .attr("dy", -3)
  .attr("text-anchor", "middle")
  .text(String);
// vertical grid lines
gridContainer.selectAll("line").data(x.ticks(5)).enter().append("line")
  .attr("x1", x)
  .attr("x2", x)
  .attr("y1", 0)
  .attr("y2", yScale.rangeExtent()[1] + gridChartOffset)
  .style("stroke", "#ccc");
// bar labels
var labelsContainer = chart.append('g')
  .attr('transform', 'translate(' + (barLabelWidth - barLabelPadding) + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
labelsContainer.selectAll('text').data(data).enter().append('text')
  .attr('y', yText)
  .attr('stroke', 'none')
  .attr('fill', 'black')
  .attr("dy", ".35em") // vertical-align: middle
  .attr('text-anchor', 'end')
  .text(barLabel);
// bars
var barsContainer = chart.append('g')
  .attr('transform', 'translate(' + barLabelWidth + ',' + (gridLabelHeight + gridChartOffset) + ')'); 
barsContainer.selectAll("rect").data(data).enter().append("rect")
  .attr('y', y)
  .attr('height', yScale.rangeBand())
  .attr('width', function(d) { return x(barValue(d)); })
  .attr('stroke', 'white')
  .attr('fill', '#993399');
// bar value labels
barsContainer.selectAll("text").data(data).enter().append("text")
  .attr("x", function(d) { return x(barValue(d)); })
  .attr("y", yText)
  .attr("dx", 3) // padding-left
  .attr("dy", ".35em") // vertical-align: middle
  .attr("text-anchor", "start") // text-align: right
  .attr("fill", "black")
  .attr("stroke", "none")
  .text(function(d) { return d3.round(barValue(d), 2); });
// start line
barsContainer.append("line")
  .attr("y1", -gridChartOffset)
  .attr("y2", yScale.rangeExtent()[1] + gridChartOffset)
  .style("stroke", "#000");
  
<?php if ($_smarty_tpl->getVariable('expression')->value!=''){?>
barsContainer.append("text")
    .attr("class", "x label")
    .attr("text-anchor", "end")
    .attr("x", 240)
    .attr("y", -20)
    .text("Volume of sms");
	
<?php }?>
 
 //svg.append("text")
  //  .attr("class", "y label")
  //  .attr("text-anchor", "end")
  //  .attr("y", 6)
  //  .attr("dy", ".75em")
  //  .attr("transform", "rotate(-90)")
  //  .text("life expectancy (years)");

}
    </script>
    <script id="csv" type="text/csv">Name,Population (mill)
<?php echo $_smarty_tpl->getVariable('expression')->value;?>
</script>
    <script>
	function SendForm(){
	$('#spinner').show();
	document.dashboard.submit();
}
	renderChart();</script>
