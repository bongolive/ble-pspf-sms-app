<?php /* Smarty version Smarty3-RC3, created on 2013-10-18 16:25:49
         compiled from "C:\xampp\htdocs\pspf\templates/vendor/ven_manage_broadcast_log.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20222518d536eb32d78-57159554%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '92f9f3791e606790c725f7a45855864cabaad5ff' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/vendor/ven_manage_broadcast_log.tpl',
      1 => 1367316006,
    ),
  ),
  'nocache_hash' => '20222518d536eb32d78-57159554',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
    <h1>SMS Log Report </h1>
	<div style="width:850px;">
	<div id="errorDiv"></div>
	
	<form name="broccastLogForm" action="<?php echo $_smarty_tpl->getVariable('selfUrl')->value;?>
" method="POST">
		<table cellspacing="10" width="600">	
			<tr>
				<td><strong>Client  Name:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<select name="broadcaster" id="broadcaster">
					<option value="">Select</option>
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['name'] = 'broadcaster';
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSmsSender')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['broadcaster']['total']);
?>
					<option value="<?php echo $_smarty_tpl->getVariable('arrSmsSender')->value[$_smarty_tpl->getVariable('smarty')->value['section']['broadcaster']['index']]['id'];?>
" 
					<?php if ($_smarty_tpl->getVariable('arrSmsSender')->value[$_smarty_tpl->getVariable('smarty')->value['section']['broadcaster']['index']]['id']==$_smarty_tpl->getVariable('broadcaster')->value){?>selected<?php }?>
					><?php echo $_smarty_tpl->getVariable('arrSmsSender')->value[$_smarty_tpl->getVariable('smarty')->value['section']['broadcaster']['index']]['username'];?>
 (<?php echo $_smarty_tpl->getVariable('arrSmsSender')->value[$_smarty_tpl->getVariable('smarty')->value['section']['broadcaster']['index']]['name'];?>
)</option>
					<?php endfor; endif; ?>
					</select></span>
				</td>				
			</tr>
			
			<tr>
				<td><strong>Start Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="startDate" id="startDate" value="<?php echo $_smarty_tpl->getVariable('startDate')->value;?>
"></span>
				</td>
			</tr>
			<tr>
				<td><strong>End Date:</strong></td>
				<td><span class="form_dinatrea standard-input">
					<input type="text" name="endDate" id="endDate" value="<?php echo $_smarty_tpl->getVariable('endDate')->value;?>
">	</span>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="submit" name="dndLog" id="dndLog" value="Download Log" class="green_btnbg">
					<input type="hidden" name="action" id="action" value="dnd">
				</td>
			</tr>
		</table>
	</form>	
	
</div>

</div>

<script type="text/javascript">
	//<!--

		$(function() {
		    $('#startDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$(function() {
		    $('#endDate').datepicker({
			duration: '',
			showTime: false,
			time24h: true,
			constrainInput: false
		     });
		});

		function selectBroadcatedrType(brdType){
			
			if(brdType==3){
				$("#vendorName").val("");
				$("#vendorName").attr("disabled",true);
				
			}else{
				
				$("#vendorName").removeAttr("disabled");
				getVendorByUserType(brdType,$("#ajaxUrl").val());
			}

		}
		
		
        //-->
     </script>