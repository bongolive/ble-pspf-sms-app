<?php /* Smarty version Smarty3-RC3, created on 2015-06-05 12:29:11
         compiled from "/var/www/html/bongosource/pspf/templates/vendor/ven_smspush_quick.tpl" */ ?>
<?php /*%%SmartyHeaderCode:42370863655716be76ce9d0-66853054%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9dc6b284cbe992a2d391c271528cd0dcc7907ce9' => 
    array (
      0 => '/var/www/html/bongosource/pspf/templates/vendor/ven_smspush_quick.tpl',
      1 => 1365669488,
    ),
  ),
  'nocache_hash' => '42370863655716be76ce9d0-66853054',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!--header start-->
<!--header end-->
<style type="text/css">
<!--
.hidden { display: none; }
.unhidden { display: block; }

.spinner {
		position: absolute; 
		background-color: grey;
		opacity:0.7;
		filter:alpha(opacity=70); /* For IE8 and earlier */ 
		top: 0%; 
	    left: 0%; 
	    text-align:center;
	    z-index:1234;
	    overflow: auto;
	    width: 100%; /* width of the spinner gif */
	    height: 130% /*hight of the spinner gif +2px to fix IE8 issue */ 
	}
	.web_dialog_overlay
{
   position: fixed;
   top: 0;
   right: 0;
   bottom: 0;
   left: 0;
   height: 100%;
   width: 100%;
   margin: 0;
   padding: 0;
   background: #000000;
   opacity: .15;
   filter: alpha(opacity=15);
   -moz-opacity: .15;
   z-index: 101;
   display: none;
}
.web_dialog
{
   display: none;
   position: fixed;
   width: 450px;
   height: 250px;
   top: 50%;
   left: 50%;
   margin-left: -190px;
   margin-top: -100px;
   background-color: #ffffff;
   border: 2px solid #336699;
   padding: 0px;
   z-index: 102;
   font-family: Verdana;
   font-size: 10pt;
}-->
</style>
<script type="text/javascript">
		$(document).ready(function ()
   {
      $("#btnShowSimple").click(function (e)
      {
         ShowDialog(false);
         e.preventDefault();
      });

      $("#btnShowModal").click(function (e)
      {
         ShowDialog(true);
         e.preventDefault();
      });

      $("#btnClose").click(function (e)
      {
         HideDialog();
         e.preventDefault();
      });

      $("#btnSubmit").click(function (e)
      {
         
		 var errorFlag=0;
		 //if($("#repeat").val()==''){
		 //	$("#repeatErrorDiv").html("Please Select This Field");
		//	errorFlag=1;
		 //}
		 
		  if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
		   	if($("#dayStart").val()==""){
				$("#startDateDiv").html("Please select Day to start");
				errorFlag=1; 
			}else if($("#monthStart").val()==""){
				$("#startDateDiv").html("Please select Month to start");
				errorFlag=1; 
			}else if($("#yearStart").val()==""){
				$("#startDateDiv").html("Please Select Year to start");
				errorFlag=1;
			}else{
				var d1=new Date();
				var d2=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				var d3=new Date(d2);
				if(d3<d1){
					$("#startDateDiv").html("Start Date Should be greater than now");
					errorFlag=1;
				}else{
					$("#scheduleDate").val(d2);
					$("#startDateDiv").html("");
					errorFlag=0;
				}
			}
		 }
		 
		 
		 
		 if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   	if($("#day").val()==""){
				$("#birthdayDiv").html("Please select Day");
				errorFlag=1; 
			}else if($("#month").val()==""){
				$("#birthdayDiv").html("Please select Month");
				errorFlag=1; 
			}else if($("#year").val()==""){
				$("#birthdayDiv").html("Please Select Year");
				errorFlag=1;
			}else{
				var d4=new Date();
				var d5=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				var d6=new Date(d5);
				var startDate=$("#scheduleDate").val();
				var d7=new Date(startDate);
				if(d6<d4){
					$("#birthdayDiv").html("End Date Should be greater than now");
					errorFlag=1;
				}else if(d6<d7){
					$("#birthdayDiv").html("End Date Should be greater than Start Date");
					errorFlag=1;
				}else{
						$("#birthdayDiv").html("");
						errorFlag=0;
				}
			}
		 }
		 
		
		 
		 
		 if(errorFlag==1){
		 	return false;
		 }else{
		 	$("#repeatErrorDiv").html("");
			$("#birthdayDiv").html("");
			$("#startDateDiv").html("");
			
			//Assign new data to hidden controls on a page
			$("#repeat2").val($("#repeat").val());
			$("#repeat_every2").val($("#repeat_every").val());
			var start_date='';
			
			if($("#dayStart").val()!=""  || $("#monthStart").val()!="" || $("#yearStart").val()!=""){
				start_date=$("#monthStart").val()+'/'+$("#dayStart").val()+'/'+$("#yearStart").val()+' '+$("#hourStart").val()+':00:00';
				
				if(document.getElementById("advOptionDiv").style.display=='none'){
					$("#advOptionDiv").show();
					$("#scheduleDate").attr("disabled",false);
					$("#advOptionTd").html('- '+msg_26);
				}
				$("#scheduleDate").val(start_date);
				
		 	}else{
				start_date="Current Time.";	
			}
			
			if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
				var end_date=$("#month").val()+'/'+$("#day").val()+'/'+$("#year").val()+' '+$("#hour").val()+':00:00';
				$("#end_date").val(end_date);
		 	}
			
			var expression='';
			if($("#repeat").val()=='WEEK'){
				var rpDays = document.smsPushForm.elements["repeat_days[]"];
				for(i=0;i<rpDays.length;i++)
				{
 					if(document.smsPushForm.repeat_days[i].checked){
						expression = expression+document.smsPushForm.repeat_days[i].value+',';
					}
				}
				
				if(expression.length >0){
				 expression = expression.substring(0, expression.length - 1);
				 $("#repeat_days2").val(expression);
				}
			}else if($("#repeat").val()=='MONTH'){
				expression='Monthly on a day.';
			}else if($("#repeat").val()=='YEAR'){
				expression='Yearly on a day.';
			}else if($("#repeat").val()=='DAY'){
				expression='Daily.';
			}
			
			//Writting Summary for User preview
			if($("#repeat").val()==''){
				var summary= 'Never Repeat.';
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()!='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() !=''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+$("#end_date").val();
			}else if($("#repeat").val()=='WEEK' && $("#end_date").val() ==''){
				var summary= 'Repeat Every:  '+$("#repeat_every").val()+' '+$("#repeatDiv").html()+'<br />Repeat Days:  '+$("#repeat_days2").val()+' <br />Start Date: '+start_date+'<br />End date:  '+'Never';
			}
		 }

		 
		 $("#repeatSummary").html(summary);
		 
		 //var brand = $("#brands input:radio:checked").val();
         //$("#output").html("<b>Your favorite mobile brand: </b>" + brand);
         HideDialog();
         e.preventDefault();
      });

   });

   function ShowDialog(modal)
   {
      $("#overlay").show();
      $("#dialog").fadeIn(300);

      if (modal)
      {
         $("#overlay").unbind("click");
      }
      else
      {
         $("#overlay").click(function (e)
         {
            HideDialog();
         });
      }
   }

   function HideDialog()
   {
      $("#overlay").hide();
      $("#dialog").fadeOut(300);
   }
   
    function pickRepeat(){
   	if($("#repeat").val()=="DAY"){
		$("#repeatDiv").html("Day");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="WEEK"){
		$("#repeatDiv").html("Week");
		$("#repeatDaysRow").show();
		$("#repeat_every").val(1);
		$("#repeat_every").attr('disabled', true);
	}else if($("#repeat").val()=="MONTH"){
		$("#repeatDiv").html("Month");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else if($("#repeat").val()=="YEAR"){
		$("#repeatDiv").html("Year");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}else{
		$("#repeatDiv").html("");
		$("#repeatDaysRow").hide();
		$("#repeat_every").attr('disabled', false);
	}
	
   }
   
   
   function changeNumberOfRepeat(){
   		if($("#repeat_every").val()>1){
			if($("#repeat").val()=="DAY"){
				$("#repeatDiv").html("Days");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="WEEK"){
				$("#repeatDiv").html("Weeks");
				document.getElementById(repeatDaysRow).style.display="block";
			}else if($("#repeat").val()=="MONTH"){
				$("#repeatDiv").html("Months");
				document.getElementById(repeatDaysRow).style.display="none";
			}else if($("#repeat").val()=="YEAR"){
				$("#repeatDiv").html("Years");
				document.getElementById(repeatDaysRow).style.display="none";
			}else{
				$("#repeatDiv").html("");
				document.getElementById(repeatDaysRow).style.display="none";
			}
		}
   }	
	</script>


<div id="spinner" class="spinner" style="display:none;">
	    <img id="img-spinner" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/ajax-loader.gif" alt="Loading" style="margin-top:25%"/>
</div>
<div id="container">
<form name="smsPushForm" id="smsPushForm" method="post" action="" enctype="multipart/form-data" onsubmit="return validatSmspushVen();">




 <div id="overlay" class="web_dialog_overlay"></div>
   
<div id="dialog" class="web_dialog">
	<div id="output"></div>
   <div style="text-align:right; margin-right:10px; margin-top:10px; display:inline; float:right;"><a href="#" id="btnClose">Close</a></div>
   <div style="float:left; display:inline;"><h2 style="margin-left:20px;">Advanced Settings</h2></div>
   <table width="450" border="0" align="center" cellpadding="10" cellspacing="10" style="margin-left:20px;">
  <tr>
    <td valign="top" width="90">Repeat</td>
    <td width="360"><div>
	
	<select name="repeat" id="repeat" onchange="pickRepeat();"style="border:2px solid #ccc;" >
		<option value="">Never</option>
		<option value="DAY">Daily</option>
		<option value="WEEK">Weekly</option>
		<option value="MONTH">Monthly</option>
		<option value="YEAR">Yearly</option>
	</select>

	</div> <span id="repeatErrorDiv" class="form_error"></span></td>
  </tr>
  <tr>
    <td valign="top">Repeat Every</td>
    <td valign="bottom">
	<select id="repeat_every" name="repeat_every" onchange="changeNumberOfRepeat();" style="border:2px solid #ccc;" >
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
		<option value="13">13</option>
		<option value="14">14</option>
		<option value="15">15</option>
		<option value="16">16</option>
		<option value="17">17</option>
		<option value="18">18</option>
		<option value="19">19</option>
		<option value="20">20</option>
		<option value="21">21</option>
		<option value="22">22</option>
		<option value="23">23</option>
		<option value="24">24</option>
		<option value="25">25</option>
		<option value="26">26</option>
		<option value="27">27</option>
		<option value="28">28</option>
		<option value="29">29</option>
		<option value="30">30</option>
	</select>
	<label id="repeatDiv"></label>
	
	</td>
  </tr>
  <tr id="repeatDaysRow" style="display:none;">
    <td valign="top">Repeat Days</td>
    <td>
		<div>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Sunday" />S</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Monday" />M</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Tuesday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Wednesday" />W</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Thursday" />T</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Friday" />F</label>
			<label style="margin-right:3px;"><input type="checkbox" id="repeat_days" name="repeat_days[]" value="Saturday" />S</label>
		</div>
	</td>
  </tr>
  <tr>
  	<td>Start Date</td>
	<td>
		<select name="dayStart" id="dayStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['name'] = 'arrDay2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'] = is_array($_loop=31) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay2']['total']);
?> 
							<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration'];?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration']==$_smarty_tpl->getVariable('day')->value){?> selected<?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay2']['iteration'];?>
</option>
							<?php endfor; endif; ?>
						</select>

				  
				  
				    	 <select name="monthStart" id="monthStart" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" <?php if ($_smarty_tpl->getVariable('month')->value==1){?> selected<?php }?>>Jan</option>
							<option value="2" <?php if ($_smarty_tpl->getVariable('month')->value==2){?> selected<?php }?>>Feb</option>
							<option value="3" <?php if ($_smarty_tpl->getVariable('month')->value==3){?> selected<?php }?>>Mar</option>
							<option value="4" <?php if ($_smarty_tpl->getVariable('month')->value==4){?> selected<?php }?>>Apr</option>
							<option value="5" <?php if ($_smarty_tpl->getVariable('month')->value==5){?> selected<?php }?>>May</option>
							<option value="6" <?php if ($_smarty_tpl->getVariable('month')->value==6){?> selected<?php }?>>Jun</option>
							<option value="7" <?php if ($_smarty_tpl->getVariable('month')->value==7){?> selected<?php }?>>Jul</option>
							<option value="8" <?php if ($_smarty_tpl->getVariable('month')->value==8){?> selected<?php }?>>Aug</option>
							<option value="9" <?php if ($_smarty_tpl->getVariable('month')->value==9){?> selected<?php }?>>Sep</option>
							<option value="10" <?php if ($_smarty_tpl->getVariable('month')->value==10){?> selected<?php }?>>Oct</option>
							<option value="11" <?php if ($_smarty_tpl->getVariable('month')->value==11){?> selected<?php }?>>Nov</option>
							<option value="12" <?php if ($_smarty_tpl->getVariable('month')->value==12){?> selected<?php }?>>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="yearStart" id="yearStart" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['name'] = 'arrYear2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'] = is_array($_loop=10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear2']['total']);
?> 
							<option value="<?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration']);?>
" <?php if ($_smarty_tpl->getVariable('year')->value==($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration'])){?> selected<?php }?>><?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear2']['iteration']);?>
</option>
							<?php endfor; endif; ?>
						</select>
						 
					
						 <select name="hourStart" id="hourStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['name'] = 'fooStart';
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['fooStart']['total']);
?> 
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['fooStart']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['fooStart']['index'];?>
</option>
							<?php endfor; endif; ?> 
						 </select>
					
					 
					 
						<!-- <select name="minutesStart" id="minutesStart" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['name'] = 'foo2Start';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] = is_array($_loop=60) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2Start']['total']);
?>
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2Start']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2Start']['index'];?>
</option>
							<?php endfor; endif; ?>
						 </select> -->
					 		 
						 
						 <span id="startDateDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td valign="top">End Date</td>
    <td>
	
	
				    	 <select name="day" id="day" style="border:2px solid #ccc; width:60px;">
							<option value="">Day:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['name'] = 'arrDay';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'] = is_array($_loop=31) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total']);
?> 
							<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration']==$_smarty_tpl->getVariable('day')->value){?> selected<?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
</option>
							<?php endfor; endif; ?>
						</select>

				  
				  
				    	 <select name="month" id="month" style="border:2px solid #ccc; width:65px;">
						 	<option value="">Month:</option>
							<option value="1" <?php if ($_smarty_tpl->getVariable('month')->value==1){?> selected<?php }?>>Jan</option>
							<option value="2" <?php if ($_smarty_tpl->getVariable('month')->value==2){?> selected<?php }?>>Feb</option>
							<option value="3" <?php if ($_smarty_tpl->getVariable('month')->value==3){?> selected<?php }?>>Mar</option>
							<option value="4" <?php if ($_smarty_tpl->getVariable('month')->value==4){?> selected<?php }?>>Apr</option>
							<option value="5" <?php if ($_smarty_tpl->getVariable('month')->value==5){?> selected<?php }?>>May</option>
							<option value="6" <?php if ($_smarty_tpl->getVariable('month')->value==6){?> selected<?php }?>>Jun</option>
							<option value="7" <?php if ($_smarty_tpl->getVariable('month')->value==7){?> selected<?php }?>>Jul</option>
							<option value="8" <?php if ($_smarty_tpl->getVariable('month')->value==8){?> selected<?php }?>>Aug</option>
							<option value="9" <?php if ($_smarty_tpl->getVariable('month')->value==9){?> selected<?php }?>>Sep</option>
							<option value="10" <?php if ($_smarty_tpl->getVariable('month')->value==10){?> selected<?php }?>>Oct</option>
							<option value="11" <?php if ($_smarty_tpl->getVariable('month')->value==11){?> selected<?php }?>>Nov</option>
							<option value="12" <?php if ($_smarty_tpl->getVariable('month')->value==12){?> selected<?php }?>>Dec</option>
						</select>
				  	 
				  
				  
				     <select name="year" id="year" style="border:2px solid #ccc; width:60px;">
							<option value="">Year:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['name'] = 'arrYear';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'] = is_array($_loop=10) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total']);
?> 
							<option value="<?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
" <?php if ($_smarty_tpl->getVariable('year')->value==($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration'])){?> selected<?php }?>><?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
</option>
							<?php endfor; endif; ?>
						</select>
						 
					
						 <select name="hour" id="hour" style="border:2px solid #ccc; width:50px;">
						 	<option value="">HH:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['name'] = 'foo';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] = is_array($_loop=24) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo']['total']);
?> 
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo']['index'];?>
</option>
							<?php endfor; endif; ?> 
						 </select>
					
					 
					 
						<!-- <select name="minutes" id="minutes" style="border:2px solid #ccc; width:50px;">
						 	<option value="">MM:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['name'] = 'foo2';
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] = is_array($_loop=60) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['foo2']['total']);
?>
								<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2']['index'];?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['foo2']['index'];?>
</option>
							<?php endfor; endif; ?>
						 </select> -->
					 		 
						 
						 <span id="birthdayDiv" class="form_error"></span>
	
	</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input id="btnSubmit" type="button" value="Ok" style="border:0px solid #93278F; width:100px; height:31px; background:url(templates/assets/images/btn_bg.png) no-repeat left; font-family:"Lucida Grande", "Helvetica Neue", Helvetica, Arial, Verdana, sans-serif; font-size:1.2em; vertical-align:text-top; text-align:center; color:#FFFFFF;" /></td>
  </tr>
</table>

</div>







  <div class="column2-ex-left-2 column2_contents">
	<h1><?php echo @COMPOSE_QUICK_SMS;?>
</h1>
	<div id="errorDiv" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg" <?php }else{ ?> class="sucess_msg_2" <?php }?>
	<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block;" <?php }else{ ?> style="display:none;" <?php }?>> <?php echo $_smarty_tpl->getVariable('msg')->value;?>
	</div>
  
  <?php if (!$_smarty_tpl->getVariable('smsconf')->value){?>
  
  <table border="0">
  <tr>
    <td valign="top"><div style=" margin-top:10px; width:600px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
	<table width="600" border="0" cellpadding="0" cellspacing="10" style="background:#f6f6f6; border-radius: 9px 9px 9px 9px;">
    <tr>
      <td width="200" valign="top"><strong><?php echo @ENTER_NUMBERS;?>
:</strong>	  </td>
      <td width="350"><div style="font-size:10px"><?php echo @RECIPIENT_DISCRIPTION;?>
</div>
          <textarea name="recipients" id="recipients" rows="5"  class="textarea"><?php echo $_smarty_tpl->getVariable('arrRecipients')->value;?>
</textarea>
          <span id="recipientsDiv" class="form_error" style="width:350px"></span> <?php if ($_smarty_tpl->getVariable('MobError')->value==1){?>
        <script type="text/javascript">
		$("#recipientsDiv").html("Recepient should be a mobile number");
	</script>
        <?php }?>
        <div style="width:350px">
          <div id="recepientCount" style="margin-left:10px; margin-top:0"><?php echo @COUNTER;?>
: 0/50</div>
        </div></td>
    </tr>
    <tr>
      <td width="200" valign="top"><strong><?php echo @VEN_MENUTOP_BROADCAST_SMS_8;?>
</strong></td>
      <td width="350"><div id="smstemplate"  style="width:40px"><span class="form_dinatrea standard-input">
          <select name="templatesms" id="smstemplate" onchange="addsmstemplate();" style="margin-right:20px;">
            <option value=""><?php echo @SELECT;?>
</option>
            
					      
				<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['name'] = 'sms';
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrsmstemplate')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['sms']['total']);
?>
				
					      
            <option value="<?php echo $_smarty_tpl->getVariable('arrsmstemplate')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['message'];?>
"><?php echo $_smarty_tpl->getVariable('arrsmstemplate')->value[$_smarty_tpl->getVariable('smarty')->value['section']['sms']['index']]['sms_title'];?>
</option>
            
					      
				<?php endfor; endif; ?>
			  
				        
          </select>
        </span></div>
      <!--  <a href="JavaScript:newPopup('ven_sms_template.php');" style=" margin-left:20px;"><?php echo @VEN_MENUTOP_BROADCAST_SMS_9;?>
</a> --> </td>
    </tr>
    <tr>
      <td width="200" valign="top"><strong><?php echo @MESSAGE_BODY;?>
:</strong></td>
      <td width="350"><textarea name="textMessage"  rows="4" class="textarea" id="textMessage"><?php echo $_smarty_tpl->getVariable('textMessage')->value;?>
</textarea>
          <span id="textMessageDiv" class="form_error" style="width:350px"></span>
        <div style="float:left;">Characters<span id="charCount" style="margin-left:10px">0</span>/160</div>
        <div style=" margin-left:60px; float:left;" id="messageCount">&nbsp;</div></td>
    </tr>
    <tr>
      <td width="200" valign="top"><strong><?php echo @SENDER_NAME;?>
:
        <input name="repeat2" type="hidden" id="repeat2" value="" />
        <input name="repeat_every2" type="hidden" id="repeat_every2" value="" />
        <input name="repeat_days2" type="hidden" id="repeat_days2" value="" />
        <input name="end_date" type="hidden" id="end_date" value="" />
        <input name="startDate" type="hidden" id="startDate" value="" />
      </strong></td>
      <td align="left" valign="bottom">
          <div align="left" style="width:40px;"><span class="form_dinatrea standard-input">
            <select name="senderid" id="senderid" class="select">
              <option value=""><?php echo @SELECT;?>
</option>                             
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['name'] = 'senderid';
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrSenderid')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['senderid']['total']);
?>                
              <option value="<?php echo $_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['id'];?>
" <?php if ($_smarty_tpl->getVariable('senderid')->value==$_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['id']){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrSenderid')->value[$_smarty_tpl->getVariable('smarty')->value['section']['senderid']['index']]['senderid'];?>
</option>
                       				<?php endfor; endif; ?>
            </select>
            <span id="senderidDiv" class="form_error"></span> </div> <!-- <a href="JavaScript:newPopup('ven_senderid.php');" style=" margin-left:20px;"><?php echo @REQUEST_NEW;?>
</a> --> </td>
    </tr>
    <tr>
      <td width="200" valign="top"><div align="left"><a id="advOptionTd" style="cursor:pointer; font-weight:bold;" onclick="javascript: showHideAdvOption();"> <?php if ($_smarty_tpl->getVariable('scheduleDate')->value&&$_smarty_tpl->getVariable('scheduleDate')->value!=''){?>
        - <?php echo @SCHEDULE_DATE;?>

        <?php }else{ ?>
        + <?php echo @SCHEDULE_DATE;?>

        <?php }?></a></div></td>
      <td width="350"><div id="advOptionDiv" <?php if ($_smarty_tpl->getVariable('scheduleDate')->value&&$_smarty_tpl->getVariable('scheduleDate')->value!=''){?>style="display:block" <?php }else{ ?>style="display:none"<?php }?>> <span class="form_dinatrea standard-input">
              <input type="text" name="scheduleDate" id="scheduleDate" value="<?php echo $_smarty_tpl->getVariable('scheduleDate')->value;?>
" />
      </div></td>
    </tr>
    <tr>
      <td valign="top"><div id="btnShowModal" title="Click Me to view advanced Settings" style="cursor:pointer; font-weight:bold;">+ Custom Reminder</div></td>
      <td><div id="repeatSummary"></div></td>
    </tr>
    <tr>
      <td width="200" valign="top">&nbsp;</td>
      <td><input type="submit" name="send" id="send" value="<?php echo @SEND;?>
" class="editbtn" />
          <input type="hidden" name="action" id="action" value="smspush" /></td>
    </tr>
  </table>
	</div></td>
    <td valign="top"><div style=" margin-left:10px; margin-top:10px; width:320px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px; background:#f6f6f6;">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;"><?php echo @VEN_COTENTHELP_QUICKSMS_STEP_01;?>
</p>
	</div></td>
  </tr>
</table>
   <?php }else{ ?>
  	<table border="0">
  <tr>
    <td valign="top"><div style=" margin-top:10px; width:600px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px;">
		<table align="center" width="600" cellpadding="0" cellspacing="6" style="background-color:#f8f8f8; border-radius: 9px 9px 9px 9px;">
				<tr>
					<td><?php echo @SMS_COUNT;?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('smsCount')->value;?>
</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><?php echo @TOTAL_CONTACTS;?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('mobileCount')->value;?>
</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><?php echo @CREDITS_BALANCE;?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('creditBalance')->value;?>
</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td><?php echo @CREDITS_COSTS;?>
</td>
					<td><?php echo $_smarty_tpl->getVariable('creditNeeded')->value;?>
</td>
				</tr>
				<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
				<tr>
					<td>
						
					</td>
					<td>
					
					</td>
				</tr>

  </table>
  </div>
  <table width="600" border="0" style="margin-top:10px;">
  <tr>
    <td width="300" align="left"><input type="button" name="back" id="back" value="Back"  onclick="javascript: backTo(1)" class="editbtn"></td>
    <td width="300" align="right"><input type="submit" name="conf" id="conf" value="<?php echo @SEND;?>
" <?php if ($_smarty_tpl->getVariable('mobileCount')->value<1||$_smarty_tpl->getVariable('creditBalance')->value<$_smarty_tpl->getVariable('mobileCount')->value){?>disabled  class="redbtn"<?php }else{ ?>class="editbtn" <?php }?> >
					<input type="hidden" name="action" id="action" value="smsconf" ></td>
  </tr>
</table>

  </td>
    <td valign="top"><div style=" margin-left:10px; margin-top:10px; width:320px; border:1px solid #bbbbbb; border-radius: 9px 9px 9px 9px; background:#f6f6f6;">
	<h2 style="margin-left:3px;">Help</h2>
	<p style="margin-left:3px; font-size:12px;"><?php echo @VEN_COTENTHELP_QUICKSMS_STEP_02;?>
</p>
	</div></td>
  </tr>
</table>

	
	
  	<?php }?>
  </div>
<div class="clear"></div>
</form>
</div>

    
    <script type="text/javascript">
	//<!--
		 function unhide(divID) {
 			var item = document.getElementById(divID);
 			if (item) {
				 item.className=(item.className=='hidden')?'unhidden':'hidden';
			 }
		 }
		 
		 function addsmstemplate() {
		 	document.smsPushForm.textMessage.value=document.smsPushForm.smstemplate.value;
		 }
		 
		 function backTo(val){
			
			$("#action").val('backTo'+val);
			$('#spinner').show();
			document.smsPushForm.submit();	
		}
		 
		 function SendForm(){
			$('#spinner').show();
			document.smsPushForm.submit();
		}
		
		
		function validatSmspushVen(){
			var phoneregex=/^\+?[0-9]{9,12}$/;
			var errorFlag = 0;	
			if($("#action").val()!='smsconf'){
				if($("#recipients").val()==''){
					$("#recipientsDiv").html("<?php echo @ENTER_RECIPIENT;?>
");
					errorFlag = 1;
					return false;
				}else if($("#recipients").val()!=''){
					$("#recipientsDiv").html("");
					var lines = $("#recipients").val().split('\n');
					lines = lines.filter(Number)
					for(var i = 0;i < lines.length;i++){
    					//code here using lines[i] which will give you each line
						
						if(lines.length  <= 50){
							if(! lines[i].match(phoneregex)){
								$("#recipientsDiv").html("<?php echo @SUB_REGISTER_MSG_03;?>
");
								errorFlag = 1;
								return false;
							}else if(lines[i].length<9){
								$("#recipientsDiv").html("<?php echo @SUB_REGISTER_MSG_03;?>
");
								errorFlag = 1;
								return false;
							}else if(lines[i].length>13){
								$("#recipientsDiv").html("<?php echo @SUB_REGISTER_MSG_03;?>
");
								errorFlag = 1;
								return false;
							}
						}else{
							$("#recipientsDiv").html("<?php echo @MAXIMUM_NUMBER_RECIPIENTS;?>
");
							errorFlag = 1;
							return false;
						}
					}
				}
				
				if($("#textMessage").val()==''){
				
					$("#textMessageDiv").html("<?php echo @ENTER_TEXT_MASSAGE;?>
");
					errorFlag = 1;
					return false;
				}else{
					$("#textMessageDiv").html("");
				}
				
				if($("#senderid").val()==''){
					$("#senderidDiv").html("<?php echo @SELECT_SENDER_NAME;?>
");
					errorFlag = 1;
					return false;
				}else{
					$("#senderidDiv").html("");
				}
			}

			if(errorFlag == 0){
				$('#spinner').show();
			   document.smsPushForm.submit();
			}
		
		}
		
		$(function() {
		    $('#scheduleDate').datepicker({
			duration: '',
			showTime: true,
			time24h: true,
			constrainInput: false
		     });
		});
		
		
		$('#recipients').keyup(function() {
        var count= 1;
		var chars= 12;
		var v = $(this).val();
        var vl = v.replace(/(\r\n|\n|\r)/gm,"").length;
		var phoneregex=/^\+?[0-9]{9,12}$/;
	
	//counting Valid lines of recipient
	var Nolines = $("#recipients").val().split('\n');
	Nolines = Nolines.filter(Number)
	document.getElementById("recepientCount").innerHTML = "<?php echo @COUNTER;?>
:  "+Nolines.length;
	
	
	var lines = $("#recipients").val().split('\n');
	lines = lines.filter(Number)

	for(var i = 0;i < lines.length;i++){
		
		if(lines.length  <= 50){
			if(! checkMobile(lines[i])){
				$("#recipientsDiv").html("<?php echo @SUB_REGISTER_MSG_03;?>
");
			}else if(! lines[i].match(phoneregex)){
				$("#recipientsDiv").html("<?php echo @SUB_REGISTER_MSG_03;?>
");
			}else{
				$("#recipientsDiv").html("");
			}
		}else{
			$("#recipientsDiv").html("<?php echo @MAXIMUM_NUMBER_RECIPIENTS;?>
");
			return false;
		}
		
	}

});
 
 function checkMobile(mobile){
  var len = mobile.length;
    if((mobile.substr(0,1) == '7' || mobile.substr(0,1) == '6') && len == 9){
		return true;
	}else if((mobile.substr(0,2) == '07' || mobile.substr(0,2) == '06') && len == 10){
       return true;
    }else if(len == 12 && (mobile.substr(0,1) != "+") && (mobile.substr(0,1) != "0")){
      return true;
    }else if (len == 13 && (mobile.substr(0,1) == "+")){
        return true;
    }else{
      return false;
    }
  }

        //-->
     </script>
	 
	 
	 <script type="text/javascript">
	$(document).ready(function(){
	    $("#spinner").bind("ajaxSend", function() {
	        $(this).show();
	    }).bind("ajaxStop", function() {
	        $(this).hide();
	    }).bind("ajaxError", function() {
	        $(this).hide();
	    });
	 
	     });
		 
		 window.onload=function() {
 			$("#myBalance").html("<?php echo $_smarty_tpl->getVariable('venCreditBalance')->value;?>
");
		}
	</script>
	
	

