<?php /* Smarty version Smarty3-RC3, created on 2012-10-19 15:47:17
         compiled from "templates/vendor/ven_simulate_sms_request.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2180950814bd558a4d0-61071351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b899b13b77667c09dd46688054380ca871c1231f' => 
    array (
      0 => 'templates/vendor/ven_simulate_sms_request.tpl',
      1 => 1350650695,
    ),
  ),
  'nocache_hash' => '2180950814bd558a4d0-61071351',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
	<div class="line01"></div>
  <div class="column2-ex-left-2-old">
		<h1><?php echo @VEN_SIMULATE_SMS_REQUEST;?>
</h1>
	    <div id="msg" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg" <?php }else{ ?> class="sucess_msg_2" <?php }?>
<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block;" <?php }else{ ?> style="display:none;" <?php }?>><?php echo $_smarty_tpl->getVariable('msg')->value;?>
</div>
		<form name="smsRequest" method="post" action="" onsubmit="return validateSmsRequest();">
		<table border="0">
          <tr>
            <td valign="top" width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
		  <tr>
            <td width="150"><strong><?php echo @MOBILE_NUMBER;?>
</strong></td>
            <td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" maxlength="13" value="<?php echo $_smarty_tpl->getVariable('mobNo')->value;?>
" onkeyup="javascript: mobileValidation();"></span><span id="mobDiv" class="form_error"></span></td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td valign="top" width="150"><div align="left"><strong><?php echo @MESSAGE_BODY;?>
:</strong></div></td>
            <td><textarea name="textMessage" cols="40"  rows="4" class="textarea" id="textMessage"><?php echo $_smarty_tpl->getVariable('textMessage')->value;?>
</textarea>
			<span id="textMessageDiv" class="form_error" style="width:500px;"></span>
			</td>
          </tr>
          <tr>
            <td width="150"><strong><?php echo @COUNTER;?>
:</strong></td>
            <td><div style="width:30%; float:left;"><span id="charCount">0</span>/160</div>
				<div style="width:70%; float:left;" id="messageCount">&nbsp;</div></td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="150">&nbsp;</td>
            <td><input type="submit" name="send" id="send" value="<?php echo @SEND;?>
" class="editbtn"></td>
          </tr>
        </table>
		<br/>
        </form>
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>

    <script type="text/javascript">
		function validateSmsRequest(){
			if(!mobileValidation()){
                    return false; 
            }else if($("#textMessage").val()==''){
				$("#textMessageDiv").html("<?php echo @ENTER_TEXT_MASSAGE;?>
");
				return false;
			}else{
				$("#textMessageDiv").html("");
				$("#mobDiv").html("");
				return true;
			}
		
		}
	</script>
