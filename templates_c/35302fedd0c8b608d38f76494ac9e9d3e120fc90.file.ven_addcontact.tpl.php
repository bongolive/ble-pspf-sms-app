<?php /* Smarty version Smarty3-RC3, created on 2013-10-18 16:45:47
         compiled from "C:\xampp\htdocs\pspf\templates/vendor/ven_addcontact.tpl" */ ?>
<?php /*%%SmartyHeaderCode:27194518b8a5eae6a45-84224878%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35302fedd0c8b608d38f76494ac9e9d3e120fc90' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/vendor/ven_addcontact.tpl',
      1 => 1368099418,
    ),
  ),
  'nocache_hash' => '27194518b8a5eae6a45-84224878',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<style type="text/css">
.table-body{
float: left;
    margin: 20px;
    text-align: center;
	margin-left:5px;
    width: 100%;}

.row-body{
border-bottom: 1px solid #DDDDDD;
    color: #000000;
    display: block;
    width: 100%;}
    
    .row-body.first{
	background-color: #EEEEEE;
}

    .row-body.first .row-1{
background-color: #FFFFFF;
    float: left;
    height: 24px;}
    
.row-1{
    width: 70px;
	text-align: left;
		}


.row-2{
 width:300px;
 text-align: left;
}

.footer-top{
    border: 0px solid #BBBBBB;
    height: auto;
    margin-top: 20px;
	margin-left:2px;
    position: relative;
    width: 100%; border-radius: 9px 9px 9px 9px; 
}
.clear{
	 clear:both;
}
</style>
<div id="container">
	<div class="column2-ex-left-2 column2_contents">
	<h1><?php echo @ADD_CONTACT;?>
</h1>
<div id="msg" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg" <?php }else{ ?> class="sucess_msg_2" <?php }?>
<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block;" <?php }else{ ?> style="display:none;" <?php }?>><?php echo $_smarty_tpl->getVariable('msg')->value;?>
</div>

<table align="center" cellpadding="0" cellspacing="0">
<tr><td>&nbsp;</td></tr>
	</tr>
	<tr>
        <!--td><?php $_template = new Smarty_Internal_Template("vendor/ven_left.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?></td-->
        <td>  
             
		<form name="addContactForm" method="POST" action="" onsubmit="javascript: validationAddContact(); return false;">
			
			<table align="center" width="600" cellpadding="0" cellspacing="10" >
				<tr>
					<td><?php echo @MOBILE_NUMBER;?>
 <span class="star_col">*</span> <br/></td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="mob_no" id="mob_no" maxlength="<?php echo $_smarty_tpl->getVariable('mob_no_length')->value+1;?>
" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['mob_no'];?>
" onkeyup="javascript: mobileValidation();"></span><span id="mobDiv" class="form_error"></span></td>
				</tr>
				<tr>
					<td><?php echo @FIRST_NAME;?>
</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="fname" id="fname" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['fname'];?>
"></span><span id="fnameDiv" class="form_error"></span></td>
				</tr>
				<tr>
					<td><?php echo @LAST_NAME;?>
</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="lname" id="lname" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['lname'];?>
"></span></td>
				</tr>
				<tr>
					<td width="140"><?php echo @TITLE;?>
:</td>
					<td width="428"><span class="form_dinatrea standard-input">
					<select name="title" id="title" >
					<option value="" ><?php echo @SELECT;?>
</option>	
					<option value="Mr" <?php if ($_smarty_tpl->getVariable('arrContactDetails')->value['title']=='Mr'){?>selected<?php }?>><?php echo @MR;?>
</option>
					<option value="Mrs" <?php if ($_smarty_tpl->getVariable('arrContactDetails')->value['title']=='Mrs'){?>selected<?php }?>><?php echo @MRS;?>
</option>
					<option value="Miss" <?php if ($_smarty_tpl->getVariable('arrContactDetails')->value['title']=='Miss'){?>selected<?php }?> ><?php echo @MISS;?>
</option>
				  </select></span><span id="titleDiv" class="form_error"></span></td>
				</tr>
				<tr>
				  <td><?php echo @BIRTH_DATE;?>
</td>
				  <td>
				  
				  <span class="form_dinatrea standard-input" style="width:65px; margin-right:10px;">
				    	 <select name="day" id="day">
							<option value="">Day:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['name'] = 'arrDay';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'] = is_array($_loop=31) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrDay']['total']);
?> 
							<option value="<?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
" <?php if ($_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration']==$_smarty_tpl->getVariable('day')->value){?> selected<?php }?>><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['arrDay']['iteration'];?>
</option>
							<?php endfor; endif; ?>
						</select>
				  </span>
				  
				  <span class="form_dinatrea standard-input" style="width:80px; margin-right:10px; margin-left:0px">
				    	 <select name="month" id="month">
						 	<option value="">Month:</option>
							<option value="1" <?php if ($_smarty_tpl->getVariable('month')->value==1){?> selected<?php }?>>Jan</option>
							<option value="2" <?php if ($_smarty_tpl->getVariable('month')->value==2){?> selected<?php }?>>Feb</option>
							<option value="3" <?php if ($_smarty_tpl->getVariable('month')->value==3){?> selected<?php }?>>Mar</option>
							<option value="4" <?php if ($_smarty_tpl->getVariable('month')->value==4){?> selected<?php }?>>Apr</option>
							<option value="5" <?php if ($_smarty_tpl->getVariable('month')->value==5){?> selected<?php }?>>May</option>
							<option value="6" <?php if ($_smarty_tpl->getVariable('month')->value==6){?> selected<?php }?>>Jun</option>
							<option value="7" <?php if ($_smarty_tpl->getVariable('month')->value==7){?> selected<?php }?>>Jul</option>
							<option value="8" <?php if ($_smarty_tpl->getVariable('month')->value==8){?> selected<?php }?>>Aug</option>
							<option value="9" <?php if ($_smarty_tpl->getVariable('month')->value==9){?> selected<?php }?>>Sep</option>
							<option value="10" <?php if ($_smarty_tpl->getVariable('month')->value==10){?> selected<?php }?>>Oct</option>
							<option value="11" <?php if ($_smarty_tpl->getVariable('month')->value==11){?> selected<?php }?>>Nov</option>
							<option value="12" <?php if ($_smarty_tpl->getVariable('month')->value==12){?> selected<?php }?>>Dec</option>
						</select>
				  	 </span>
				  
				  <span class="form_dinatrea standard-input" style="width:70px; margin-right:5px;">
				     <select name="year" id="year">
							<option value="">Year:</option>
							<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['name'] = 'arrYear';
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'] = is_array($_loop=80) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['arrYear']['total']);
?> 
							<option value="<?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
" <?php if ($_smarty_tpl->getVariable('year')->value==($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration'])){?> selected<?php }?>><?php echo ($_smarty_tpl->getVariable('currentYear')->value-$_smarty_tpl->getVariable('smarty')->value['section']['arrYear']['iteration']);?>
</option>
							<?php endfor; endif; ?>
						</select>
				 		 </span><span id="birthdayDiv" class="form_error"></span>
				  
				  </td>
			  </tr>
				<tr>
				  <td><?php echo @GENDER_;?>
</td>
				  <td><span class="form_dinatrea standard-input">
				    <select name="gender" id="gender" >
                      <option value="" ><?php echo @SELECT;?>
</option>
                      <option value="Male" <?php if ($_smarty_tpl->getVariable('arrContactDetails')->value['gender']=='Male'){?> selected<?php }?> ><?php echo @MALE;?>
</option>
                      <option value="Female" <?php if ($_smarty_tpl->getVariable('arrContactDetails')->value['gender']=='Female'){?> selected<?php }?> ><?php echo @FEMALE;?>
</option>
                    </select>
				  </span></td>
			  </tr>
				<tr>
				  <td><?php echo @MOBILE_NUMBER2;?>
</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="mob_no2" id="mob_no2" maxlength="<?php echo $_smarty_tpl->getVariable('mob_no_length')->value+1;?>
" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['mob_no2'];?>
" onkeyup="javascript: mobile2Validation();" />
				  </span><span id="mob2Div" class="form_error"></span></td>
			  </tr>
				<!--
				<tr>
				  <td><?php echo @POSTAL_ADDRESS;?>
</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="address" id="address" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['address'];?>
" />
				  </span></td>
			  </tr>
			  -->
				<tr>
					<td><?php echo @EMAIL;?>
</td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="email" id="email" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['email'];?>
" onkeyup="javascript: emailValidation();"></span><span id="emailDiv" class="form_error"></span></td>
				</tr>
				<!--
				<tr>
					<td><?php echo @COMMENTS;?>
 </td>
					<td><span class="form_dinatrea standard-input"><input type="text" name="customs" id="customs" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['customs'];?>
"></span></td>
				</tr> -->
				<tr>
				  <td><?php echo @COUNTRY;?>
</td>
				  <td><span class="form_dinatrea standard-input">
				    <select name="country" id="country">
						<option value="" ><?php echo @SELECT;?>
</option>	
						<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['name'] = 'countries';
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrCountry')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['countries']['total']);
?>
						<option value="<?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['countries']['index']]['country'];?>
" <?php if ($_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['countries']['index']]['country']==$_smarty_tpl->getVariable('arrContactDetails')->value['country']){?>selected<?php }?>><?php echo $_smarty_tpl->getVariable('arrCountry')->value[$_smarty_tpl->getVariable('smarty')->value['section']['countries']['index']]['country'];?>
</option>
						<?php endfor; endif; ?>
					</select>
				  </span></td>
			  </tr>
				<tr>
				  <td><?php echo @CITY;?>
</td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="city" id="city" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['city'];?>
" />
				  </span></td>
			  </tr>
				<tr>
				  <td><?php echo @AREA;?>
<input name="address" id="address" type="hidden" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['address'];?>
" /><input name="customs" id="customs" type="hidden" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['customs'];?>
" /></td>
				  <td><span class="form_dinatrea standard-input">
				    <input type="text" name="area" id="area" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['area'];?>
" />
				  </span></td>
			  </tr>
				
				<tr>
				  <td><?php echo @OPTIONAL_ONE;?>
</td>
				  <td><span class="form_dinatrea standard-input"><input type="text" name="optional_one" id="optional_one" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['optional_one'];?>
"></span></td>
			  </tr>
				<tr>
				  <td><?php echo @OPTIONAL_TWO;?>
</td>
				  <td><span class="form_dinatrea standard-input"><input type="text" name="optional_two" id="optional_two" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['optional_two'];?>
"></span></td>
			  </tr>
				</table>
				<?php if ($_smarty_tpl->getVariable('action')->value!='update'){?>
					
                   
                    <table style="margin-top:20px; margin-right:10px;" id="tblAddContact">
					<tr class="row-body" style="background-color:#E5E5E5; font-weight:bold;">
						<td class="row-1"><input type="checkbox" name="all" id="all" onclick="javascript: checkAllGroup('all','tblAddContact','defaultAddbook');">&nbsp;<?php echo @ALL;?>
</td>
						<td class="row-2"><?php echo @GROUP_NAME;?>
</td>
					
					</tr>
					<tr class="TR_spc"><td height="14" colspan="6" class="TD_spc"></td></tr>
					<?php unset($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['name'] = 'addressbook';
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'] = is_array($_loop=$_smarty_tpl->getVariable('arrAddressbook')->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['addressbook']['total']);
?>
					<tr class="row-body">
						<td class="row-1"><input  type="checkbox" name="addbookId[]" <?php if ($_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['addressbook']=='Default'){?>id="defaultAddbook"
						checked
						onclick="javascript: keepCheckAddbook(this.id);"
						<?php }else{ ?>id="addbookId" <?php }?> value="<?php echo $_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['id'];?>
"></td>
						<td class="row-2"><?php echo $_smarty_tpl->getVariable('arrAddressbook')->value[$_smarty_tpl->getVariable('smarty')->value['section']['addressbook']['index']]['addressbook'];?>
</td>
					</tr>
					<tr class="TR_spc"><td colspan="6" class="TD_spc"></td></tr>
					<?php endfor; endif; ?>
					</table>
                    
				<?php }?>
				<table align="center" width="600" cellpadding="0" cellspacing="10" >
				<tr>
					<td></td>
					<td>
					<input type="submit" name="save" id="save" value="<?php echo @SAVE;?>
" class="editbtn">
					<input type="hidden" name="action" id="action" value="<?php echo $_smarty_tpl->getVariable('action')->value;?>
">
					<input type="hidden" name="bid" id="bid" value="<?php echo $_smarty_tpl->getVariable('arrContactDetails')->value['addressbook_id'];?>
">
					<input type="hidden" name="cid" id="cid" value="<?php echo $_smarty_tpl->getVariable('cid')->value;?>
">

					</td>
				</tr>
				    
			</table>
            
             
					
	</form>
            <!-- middle body end-->
        </td>
    </tr>
</table>
  </div>
      <div class="clear"></div>
  </div>

	<script type="text/javascript">
	<!--
	

	function validationAddContact(){

            var addbookError = 0;
            var mobError = 0;
            var emailError = 0;
            var errorFlag= 0;
			if($("#action").val()=='add'){
				if($("#defaultAddbook").attr("checked")==true){
					addbookError = 1;
				}else if(document.addContactForm.addbookId.length>1){
					for (var x = 0; x < document.addContactForm.addbookId.length; x++)
					{
						if(document.addContactForm.addbookId[x].checked == true){
						
							addbookError = 1;
						}
						
					}
				}else{
					
					if($("#defaultAddbook").attr("checked")==true){
						
						addbookError = 1;
					
					}else{

						if($("#addbookId").attr("checked")==true){

							addbookError = 1;
						}
					}

				}
				if(addbookError !=1){

					$("#msg").removeClass();
					$("#msg").addClass("error_msg");
					$("#msg").html("<?php echo @SELECT_ANY_GROUP;?>
");
					$("#msg").show();
					errorFlag=1; 
				
				}else{
					
					$("#msg").removeClass();
					$("#msg").html("");
					$("#msg").hide();
					
				}
			}
			
			 if(!mobileValidation()){
                    errorFlag=1; 
            
            }
			//if($("#fname").val()==""){
            //	$("#fnameDiv").html("<?php echo @FIRST_NAME_REQUIRED;?>
")
            //    errorFlag=1; 
           // }else{
			//	$("#fnameDiv").html('')
           // }
		   
		   if($("#day").val()!=""  || $("#month").val()!="" || $("#year").val()!=""){
		   		if($("#day").val()==""){
					$("#birthdayDiv").html("Day of your birtday is required");
					errorFlag=1; 
				}else if($("#month").val()==""){
					$("#birthdayDiv").html("Month of your birthday is required");
					errorFlag=1; 
				}else if($("#year").val()==""){
					$("#birthdayDiv").html("Year of your birthday is required");
					errorFlag=1; 
				}
		   }else{
				$("#birthdayDiv").html("");
			}
		   

            if($("#email").val()!=""){
            
                if(!emailValidation()){
                    errorFlag=1; 
                }
            }
			
			if($("#mob_no2").val()!=""){
            
                if(!mobile2Validation()){
                    errorFlag=1; 
                }
            }
			
            if(errorFlag!=1){
            
                document.addContactForm.submit();
            }


	}
	
	
	
	$(function() {
		    $('#birth_date').datepicker({
			duration: '',
			showTime: false,
			time24h: false,
			constrainInput: false
		     });
		});
		
		
	function mobile2Validation(){

	var pattern =/^\+?[0-9]{8,12}$/;

	var mob =$("#mob_no2").val();
    if(mob.substr(0,1) == '7' || mob.substr(0,1) == '6'){
		mob_no_length = 9;
	}else if(mob.substr(0,1) == '0'){
       	mob_no_length = 10;
    }else if(mob.substr(0,1) == '+'){
        mob_no_length = 13;
    }else {
      	mob_no_length = 12;
    }
	
	if(mob==""){
		$("#mob2Div").html("");
		return 0;
	}else if (!mob.match(pattern)) {

		$("#mob2Div").html(msg_07);
		return 0;
	}else if(mob.length != mob_no_length){

		$("#mob2Div").html(msg_09_1+mob_no_length+msg_09_2);
		return 0;
	}else{

		$("#mob2Div").html('<img src="'+imagePath+'tick_icon.gif" >');
		return true;
	}



}
        
	-->	
	</script>
	
