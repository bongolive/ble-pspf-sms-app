<?php /* Smarty version Smarty3-RC3, created on 2015-08-15 14:09:01
         compiled from "/var/www/html/pspf/templates/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:214745776355cf1dcd47b839-34747229%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3d8411e4b6510a191be45fd88a1170bbf6d030b7' => 
    array (
      0 => '/var/www/html/pspf/templates/header.tpl',
      1 => 1368450678,
    ),
  ),
  'nocache_hash' => '214745776355cf1dcd47b839-34747229',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>
	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo @SUB_TITLE;?>
</title>
		<link href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/style.css" rel="stylesheet" type="text/css" />
		<!--<link href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/stylesheet_revisions.css" rel="stylesheet" type="text/css" />-->
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/fader.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/tab.css" type="text/css" media="screen" />
		<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/ddsmoothmenu.css" />
		<script type="text/javascript">
			var assetPath = '<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
';

		</script>
		<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery-1.4.2.min.js" type="text/javascript"></script>
		<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/png-transparent.js" language="javascript" type="text/javascript"></script>
		<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery.anythingfader.js" type="text/javascript"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/ddsmoothmenu.js"></script>
				
		<script type="text/javascript">
			// ddeclaration of image path global for javascript
			var imagePath = '<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/';
		</script>
		<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/common_functions_lang_<?php echo $_SESSION['lang'];?>
.js" type="text/javascript"></script>
		<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/common_functions.js" type="text/javascript"></script>
		<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery-ui-1.7.2.custom.min.js"></script>
		<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/timepicker.js"></script>
        <link rel="icon" type="image/ico" href="favicon.ico">
</head>
<body>
<div id="container-top">
	<div class="logo"><a href="index.php"><img alt="Bongo Live logo" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/logo.png" /></a></div>
	<div id="header_topright">
		<?php $_template = new Smarty_Internal_Template("langs.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

		<div style="text-align:right;width:650px; margin:0; height:20px;"></div>
		
	</div>
	<div class="clear"></div>