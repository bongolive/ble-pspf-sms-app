<?php /* Smarty version Smarty3-RC3, created on 2015-06-05 12:47:55
         compiled from "/var/www/html/bongosource/pspf/templates/vendor/ven_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17369988335571704bc93581-77575987%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '61addd931d2e58dfaa19ffd1a45b2cd47e0498ba' => 
    array (
      0 => '/var/www/html/bongosource/pspf/templates/vendor/ven_header.tpl',
      1 => 1433497630,
    ),
  ),
  'nocache_hash' => '17369988335571704bc93581-77575987',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!--	<meta name="description" content="Bongo Live! is a mobile services company that seeks to empower businesses, NGOs, government and consumers. Our services include group/bulk SMS, targeted sms advertising, SMS suverys, SMS raffles, SMS voting and custom mobile and sms solutions."/>

	<meta name="keywords" content="Bulk, group, sms, marketing, advertising, ads, adverts, applications, integration, voting, raffle, survey, opinions, sales, on demand, mass, contacts, address book, Dar es Salaam, Arusha, Zanzibar, Mwanza, Tanga, Morogoro, Mbeya, Iringa, Moshi, Tanzania, East Africa, greetings, emergency, alerts, restaurants, business, bars, nightclub, relgious, church, mosque, NGO, non governmental organization, school, college, univertisyt, institute, event planners, weddings, functions, seminars, audience" /> -->



	<title><?php echo @VEN_HEADER_TITLE;?>
</title>



	<link href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/style.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/fader.css" type="text/css" media="screen" />

	<link rel="stylesheet" type="text/css" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/ddsmoothmenu.css" />



	<script type="text/javascript">

		var assetPath = '<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
';

	</script>
	
	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery-1.4.2.min.js" type="text/javascript"></script>

	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/png-transparent.js" language="javascript" type="text/javascript"></script>

	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery.anythingfader.js" type="text/javascript"></script>
	
	<link type="text/css" rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/graph.css">
	
	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/d3.v2.js"></script>
	

	<script type="text/javascript">

		/*

			$(document).ready(function(){

				//Default Action

				$(".tab_content").hide(); //Hide all content

				$("ul.tabs li:first").addClass("active").show(); //Activate first tab

				$(".tab_content:first").show(); //Show first tab content



				//On Click Event

				$("ul.tabs li").click(function(){

					$("ul.tabs li").removeClass("active"); //Remove any "active" class

					$(this).addClass("active"); //Add "active" class to selected tab

					$(".tab_content").hide(); //Hide all tab content

					var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

					$(activeTab).fadeIn(); //Fade in the active content

					return false;

				});

			});

		*/

		$(document).ready(function(){

			//On Click Event

			$("ul.tabs li").click(function(){

				$("ul.tabs li").removeClass("active"); //Remove any "active" class

				$(this).addClass("active"); //Add "active" class to selected tab

				$(".tab_content").hide(); //Hide all tab content

				var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

				$(activeTab).fadeIn(); //Fade in the active content

				return false;

			});

		});

	</script>

	<script type="text/javascript">

		function formatText(index, panel){

			return index + "";

		}



		$(function (){

			$('.anythingFader').anythingFader({

				autoPlay: true,                 // This turns off the entire FUNCTIONALY, not just if it starts running or not.

				delay: 8000,                    // How long between slide transitions in AutoPlay mode

				startStopped: false,            // If autoPlay is on, this can force it to start stopped

				animationTime: 1000,             // How long the slide transition takes

				hashTags: true,                 // Should links change the hashtag in the URL?

				buildNavigation: true,          // If true, builds and list of anchor links to link to each slide

				pauseOnHover: true,             // If true, and autoPlay is enabled, the show will pause on hover

				startText: "",                // Start text

				stopText: "",               // Stop text

				navigationFormatter: formatText   // Details at the top of the file on this use (advanced use)

			});



			$("#slide-jump").click(function(){

				$('.anythingFader').anythingFader(6);

			});

		});

	</script>


	<script type="text/javascript">

		// ddeclaration of image path global for javascript

		var imagePath = '<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/';

	</script>



	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/common_functions_lang_<?php echo $_SESSION['lang'];?>
.js" type="text/javascript"></script>

	<script src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/common_functions.js" type="text/javascript"></script>

<?php if (($_smarty_tpl->getVariable('css')->value)){?>

    	<link type="text/css" href="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/<?php echo $_smarty_tpl->getVariable('css')->value;?>
" rel="stylesheet" />

<?php }?>
	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/jquery-ui-1.7.2.custom.min.js"></script>
	<link type="text/css" href="css/ui-lightness/jquery-ui-1.7.2.custom.css" rel="stylesheet" />

	<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/timepicker.js"></script>

<?php if (($_smarty_tpl->getVariable('js')->value)){?>

<script type="text/javascript" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
js/<?php echo $_smarty_tpl->getVariable('js')->value;?>
"></script>

<?php }?>



	<!--[if IE]>

		<style type="text/css" media="all">@import "<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
css/ie6_tab.css";</style>

	<![endif]-->
<link rel="icon" type="image/ico" href="favicon.ico">
</head>



<body>

<div id="container-top">

  <div class="logo"><a href="index.php"><img alt="PSPFlogo" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/logo.png" /></a></div>

  <div class="nav-sign-in">

  	<?php $_template = new Smarty_Internal_Template("langs.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate();?><?php $_template->updateParentVariables(0);?><?php unset($_template);?>

	<?php if ($_smarty_tpl->getVariable('login')->value){?>

		<ul>

		  <li id="user-id"><?php echo $_smarty_tpl->getVariable('welcome')->value;?>
 <?php echo $_smarty_tpl->getVariable('username')->value;?>
,</li>

		  <li></li>

		  <li><a href="<?php echo $_smarty_tpl->getVariable('logout_url')->value;?>
"><?php echo $_smarty_tpl->getVariable('logout')->value;?>
</a></li>

		</ul>

	<?php }?>

  </div>

  <div id="subpage_main-menu">

  </div>

  <div id="main-info">

    <table width="100%" border="0" cellspacing="0">

      <tr>

        <td class="main-info-title01"><?php echo @SMS_BALANCE;?>
:</td>

        <td class="main-info-sms-balance"><div id="myBalance"><?php echo $_smarty_tpl->getVariable('crdedit')->value[0]['credit_balance'];?>
</div></td>

        <td class="main-info-contacts"><?php echo @CONTACTS;?>
</td>

	<td class="main-info-sms-balance"><?php echo $_smarty_tpl->getVariable('contacts')->value;?>
</td>

      </tr>

    </table>

  </div>

  <div class="clear"></div>

  <p id="user-role"><?php echo @VEN_BROADCASTER;?>
</p>

  <div class="topmenu">

    <ul class="primary">

      <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_dashboard.php'){?>
			class="active-topmenu current item-eight-a"

			<?php }else{ ?>  class="item-eight"

			<?php }?> style="width:90px;">

			<a href="ven_dashboard.php"><?php echo @VEN_DASHBOARD;?>
</a>

        <ul class="secondary">

		  <li><a href=""></a></li>

          <li><a href=""></a></li>

          <li><a href=""></a></li>

		   <!-- <li><a href="dnld_unsaved_contact.php">Download Unsaved Contacts</a></li> -->

        </ul>

      </li>
	  <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_addressbook.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_addbook_details.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_addcontact.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_import_contacts.php'||$_smarty_tpl->getVariable('curPage')->value=='dnld_unsaved_contact.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_contacts_analytics.php'){?>

			class="active-topmenu  item-nine-a current"

			<?php }else{ ?>  class="item-nine"

			<?php }?> style="width:150px;">

			<a href="ven_addressbook.php"><?php echo @VEN_MENUTOP_DASHBOARD_4;?>
</a>

        <ul class="secondary">

		  <li><a href="ven_addressbook.php"><?php echo @VEN_MENUTOP_DASHBOARD_1;?>
</a></li>

          <li><a href="ven_addcontact.php"><?php echo @VEN_MENUTOP_DASHBOARD_2;?>
</a></li>

          <li><a href="ven_import_contacts.php"><?php echo @VEN_MENUTOP_DASHBOARD_3;?>
</a></li>
		  <li><a href="ven_contacts_analytics.php"><?php echo @VEN_MENUTOP_DASHBOARD_5;?>
</a></li>

		   <!-- <li><a href="dnld_unsaved_contact.php">Download Unsaved Contacts</a></li> -->

        </ul>

      </li>

      <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_senderid.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_word_keys.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_credit_requested.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_credit_purchage.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_smspush.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_sms_template.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_smspush_quick.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_smspush_file.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_reminders.php'){?>

			class="active-topmenu current item-two-a"

		  <?php }else{ ?>

			class="item-two"

		  <?php }?>  style="width:110px;"><a href="ven_smspush.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS;?>
</a>

        <ul class="secondary">

			<li><a href="ven_smspush.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_1;?>
</a></li>
			
			<li><a href="ven_smspush_quick.php"><?php echo @COMPOSE_QUICK_SMS;?>
</a></li>
			
			<li><a href="ven_smspush_file.php"><?php echo @COMPOSE_FILE_SMS;?>
</a></li>

		 	<li><a href="ven_senderid.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_2;?>
</a></li>

            <li><a href='ven_credit_purchage.php'><?php echo @VEN_MENUTOP_PURCHASE_SMS_1;?>
</a></li>
			<?php if ($_smarty_tpl->getVariable('parent_ven_id')->value==''){?>
        	<li><a href="ven_word_keys.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_6;?>
</a></li>
			<?php }?>
			<li><a href="ven_sms_template.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_8;?>
</a></li>

			<li><a href="ven_reminders.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_10;?>
</a></li>
        </ul>

      </li>

	  
	<?php if ($_smarty_tpl->getVariable('parent_ven_id')->value==''||$_smarty_tpl->getVariable('access_registrations')->value=='1'){?>
	  

	  <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_incoming_reg.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_incoming_reg_log_report.php'){?>class="active-topmenu current item-three-a" <?php }else{ ?> class="item-three"<?php }?>><a href="ven_incoming_reg.php"><?php echo @MEMBER_REGISTRATION;?>
</a>

        <ul class="secondary">
		 <li><a href="ven_incoming_reg.php"><?php echo @VEN_MENUTOP_INCOMING_REG;?>
</a></li>
         <li><a href="ven_incoming_reg_log_report.php"><?php echo @VEN_MENUTOP_INCOMING_REG_LOG;?>
</a></li>
          <li></li>

        </ul>

      </li>
      
      <?php }?>

		<li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_broadcast.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_log_report.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_incoming_log_report.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_incoming.php'){?>class="active-topmenu current item-one-a" <?php }else{ ?> class="item-one"<?php }?> style="width:90px;"><a href="ven_incoming.php"><?php echo @VEN_MENUTOP_REPORT;?>
</a>

        <ul class="secondary">
          	<?php if ($_smarty_tpl->getVariable('parent_ven_id')->value==''||$_smarty_tpl->getVariable('access_incoming')->value=='1'){?>
            <li><a href="ven_incoming.php"><?php echo @VEN_MENUTOP_INCOMING;?>
</a></li>
			<li><a href="ven_incoming_log_report.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_7;?>
</a></li>
            <?php }?>
			<li><a href="ven_broadcast.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_3;?>
</a></li>
			<li><a href="ven_log_report.php"><?php echo @VEN_MENUTOP_BROADCAST_SMS_4;?>
</a></li>
        </ul>

      </li>
      
      <?php if ($_smarty_tpl->getVariable('reseller')->value==1){?>
	  <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_manage_sub_vendor.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_new_reseller.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_manage_broadcast.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_manage_broadcast_log.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_credits_schemes.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_manage_credits_request.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_manage_senderid.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_add_credit.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_add_senderid.php'){?>

			class="active-topmenu current item-two-a"

		  <?php }else{ ?>

			class="item-two"

		  <?php }?> style="width:80px;"><a href="ven_manage_sub_vendor.php">Users</a>

        <ul class="secondary">
			<li><a href="ven_manage_sub_vendor.php">Manage Client </a></li>
			<li><a href="ven_manage_credits_request.php">Credit</a></li>
			<!-- <li><a href="ven_manage_credits_balances.php">Credit Balance</a></li> -->
			<li><a href="ven_manage_senderid.php">Sende rNames</a></li>
		 	<li><a href="ven_manage_broadcast.php">History Report</a></li>
            <li><a href="ven_manage_broadcast_log.php">SMS Log Report</a></li>
			<li><a href="ven_credits_schemes.php">Credit Scheme</a></li>
        </ul>

      </li>
		<?php }?>
      
      

	   <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='ven_profile.php'||$_smarty_tpl->getVariable('curPage')->value=='ven_changepassword.php'){?>class="active-topmenu current item-five-a" <?php }else{ ?> class="item-five"<?php }?>  style="width:80px;"><a href="ven_profile.php" style="margin-left:0px;"><?php echo @VEN_MENUTOP_PROFILE;?>
</a>

       <ul class="secondary">

          <li><a href="ven_profile.php"><?php echo @VEN_MENUTOP_PROFILE;?>
</a></li>

          <li><a href="ven_changepassword.php"><?php echo @VEN_MENUTOP_CHANGE_PASSWORD;?>
</a></li>

          <li></li>

        </ul>

      </li>

	  <li <?php if ($_smarty_tpl->getVariable('curPage')->value=='help.php'){?>class="active-topmenu current item-four-a" <?php }else{ ?> class="item-four"<?php }?>  style="width:80px;">

	  <a href="help.php"><a href="help.php"><?php echo @VEN_MENUTOP_HELP;?>
</a>

        <ul class="secondary">

          <li></li>

          <li></li>

          <li></li>

        </ul>

      </li>



    </ul>

  </div>



  <div class="clear"></div>

</div>



