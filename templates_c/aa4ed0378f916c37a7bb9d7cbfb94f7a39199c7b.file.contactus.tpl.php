<?php /* Smarty version Smarty3-RC3, created on 2012-10-25 11:28:03
         compiled from "C:\xampp\htdocs\pspf\templates/contactus.tpl" */ ?>
<?php /*%%SmartyHeaderCode:155445088f8132855a9-83094338%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'aa4ed0378f916c37a7bb9d7cbfb94f7a39199c7b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/contactus.tpl',
      1 => 1300914000,
    ),
  ),
  'nocache_hash' => '155445088f8132855a9-83094338',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
      <div class="line01"></div>
      <div class="column2-ex-left-2">
    <h1><?php echo @CONTACT_US;?>
</h1>
    <?php echo @CONTACT_US_PREINFO;?>

    <!--Form Begins-->
    <form name="contactusForm" id="contactusForm" method="POST" action="" onsubmit="contactusValidation();return false;">
          <table cellspacing="10px">
		  <tr>
              <td colspan="4"><div id="errorDiv" <?php if ($_smarty_tpl->getVariable('errorFlag')->value=='1'){?> class="error_msg" <?php }else{ ?> class="sucess_msg" <?php }?>
<?php if ($_smarty_tpl->getVariable('msg')->value){?> style="display:block;" <?php }else{ ?> style="display:none;" <?php }?>><?php echo $_smarty_tpl->getVariable('msg')->value;?>
</div> </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_name"> <?php echo @NAME;?>
<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="name" name="name" value="<?php echo $_smarty_tpl->getVariable('name')->value;?>
"  /> </span>

			  <span id="nameDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_email"><?php echo @EMAIL_ADDRESS;?>
<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="email" name="email" value="<?php echo $_smarty_tpl->getVariable('email')->value;?>
"  /> </span>
			  <span id="emailDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_ex_field2"><?php echo @MOBILE_NUMBER;?>
<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="mob_no" name="mob_no" value="<?php echo $_smarty_tpl->getVariable('mob_no')->value;?>
"  /> </span>
			  <span id="mobDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_ex_field3"><?php echo @BUSINESS_NAME;?>
</label></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" id="business_name" name="business_name" value="<?php echo $_smarty_tpl->getVariable('business_name')->value;?>
"  /> </span></td>
        </tr>
        <tr>
              <td class="ta-right"><label for="si_contact_subject"><?php echo @SUBJECT;?>
<span class="star_col">*</span></label></td>
              <td>
			  <span class="form_dinatrea standard-input">

			  <input style="text-align:left;" type="text" id="subject" name="subject" value="<?php echo $_smarty_tpl->getVariable('subject')->value;?>
"  /> </span>
			  <span id="subjectDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td class="ta-right top">
				<label for="si_contact_message"> <?php echo @MESSAGE;?>
<span class="star_col">*</span></label>
			  </td>
              <td><span class="form_dinatrea standard-input" style="width:350px;" >
				<textarea style="text-align:left;" id="message" name="message"  cols="40" rows="10"><?php echo $_smarty_tpl->getVariable('message')->value;?>
</textarea> </span>
				<span id="messageDiv" class="form_error"></span>
			  </td>
        </tr>
        <tr>
              <td><label for="si_contact_captcha_code"><?php echo @CAPTCHA_CODE;?>
<span class="star_col">*</span></label></td>
              <td>
				 <!-- <img id="si_image_ctf" style="padding-bottom:10px; float:left; border-style:none; margin:0;" src="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/securimage_show_medium.php?sid=dd11e0638ece111518369b9539ca3559" alt="CAPTCHA Image" title="CAPTCHA Image" /> -->
				 <img id="si_image_ctf" style="padding-bottom:10px; float:left; border-style:none; margin:0;" src="captcha.php" alt="CAPTCHA Image" title="CAPTCHA Image" />
				  <!--
				  <a href="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/securimage_play.php" title="CAPTCHA Audio"> 
				  <img src="http://wordpress.bongo-live.com/wp-content/plugins/si-contact-form/captcha-secureimage/images/audio_icon.gif" alt="CAPTCHA Audio" style="padding-top:2px; vertical-align:top; float:left; border-style:none; margin:0;" onclick="this.blur()" />
				  </a>
				  -->
				  &nbsp; 
				  <a href="#" title="Refresh Image" onclick="document.getElementById('si_image_ctf').src = 'captcha.php?' + Math.random(); return false">
				  <img src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/refresh.gif" alt="Refresh Image" style="vertical-align:top; float:left; border-style:none; margin:0;" onclick="this.blur()" />
				  </a>
			  </td>
				
            </tr>
        <tr>
              <td></td>
              <td>
			  <span class="form_dinatrea standard-input">
			  <input style="text-align:left;" type="text" value="" name="security_code" id="security_code"  size="9" />
			   </span>
			  <span id="captchaDiv" class="form_error"><?php echo $_smarty_tpl->getVariable('msgSecCode')->value;?>
 </span>
			  </td>
		</tr>
        <tr>
              <td>
			  
				<input type="button" name="resset" id="rest" value="<?php echo @RESET;?>
" class="editbtn"/>
			  </td>
              <td>
				<input type="submit" name="submit" id="submit" value="<?php echo @SUBMIT;?>
" class="editbtn"/>
				<input type="hidden" name="action" value="send" />
			  </td>
            </tr>
      </table>
          <div class="clear"></div>
        </form>
    
    <!-- Form Ends --> 
  </div>
      <div class="column2-ex-right-2"> &nbsp;</div>
      <div class="clear"></div>
    </div>

    
    <script type="text/javascript">
		//<!--
            
		function contactusValidation(){
			
			var error=0;
			var name;
			var errorMob;
			var errorEmail;
			var subject;
			var message;
			var captcha;
			
			errorMob = mobileValidation();
			errorEmail = emailValidation();
			
			
			if($("#name").val()==""){
		
				$("#nameDiv").html("<?php echo @ENTER_NAME;?>
");
				name=false;
			}else{
				$("#nameDiv").html("");
				name=true;
			}

			if($("#subject").val()==""){
				$("#subjectDiv").html("<?php echo @ENTER_SUBJECT;?>
");
				subject=false;
			}else{
				$("#subjectDiv").html("");
				subject=true;
			}

			if($("#message").val()==""){
				$("#messageDiv").html("<?php echo @ENTER_MESSAGE;?>
");
				message=false;
			}else{
				$("#messageDiv").html("");
				message=true;
			}

			if($("#security_code").val()==""){
				$("#captchaDiv").html("<?php echo @ENTER_CAPTCHA_CODE;?>
");
				captcha=false;
			}else{
				$("#captchaDiv").html("");
				captcha=true;
			}


			
			if(!errorEmail  || !errorMob || !name || !message || !subject || !captcha){
					error=1;
			}
			
			
			if(error !=1){
				document.contactusForm.submit();
			}
		
		
		}



        //-->
     </script>


