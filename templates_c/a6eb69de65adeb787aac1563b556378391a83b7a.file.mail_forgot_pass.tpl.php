<?php /* Smarty version Smarty3-RC3, created on 2012-10-25 11:14:11
         compiled from "C:\xampp\htdocs\pspf\templates/mail_template/mail_forgot_pass.tpl" */ ?>
<?php /*%%SmartyHeaderCode:105385088f4d3d866a1-85593726%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a6eb69de65adeb787aac1563b556378391a83b7a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/mail_template/mail_forgot_pass.tpl',
      1 => 1346234266,
    ),
  ),
  'nocache_hash' => '105385088f4d3d866a1-85593726',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
index.php"><img alt="Bongo Live" src="<?php echo $_smarty_tpl->getVariable('BASE_URL_HTTP_ASSETS')->value;?>
images/logo.png" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:10px 0px 10px 0px;">Password Reset</h1>
		<br />

		<p>The password for your account has been reset.</p>
		<p>Temporary password: <strong><?php echo $_smarty_tpl->getVariable('field2_val')->value;?>
</strong></p>

		<p>For security reasons please login immediately with your new password <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
index.php">here</a> and change your password.</p>
			
		<p>If you have any questions you can browse through the frequently asked questions <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
faq.php">FAQ</a> page or you can get in touch with us using the <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
contactus.php">contact us</a> page.</p>

		<p> Bongo Live Enterprise Ltd - Your Message, Your Audience. <br />
		  7th Floor, Ibrahim Manzil, Above Kearley Travels <br />
		  Zanaki St, Dar es Salaam, Tanzania<br />
		  Tel: +255 22 2129275
		  .<br />
		  Mob: +255 688 121252 </p><br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;">

		  <p style="float:left;	margin:15px 0px 0px 0px;">2012 &copy; Bongo Inc. All Rights Reserved</p>
		  <div class="nav-bottom" width="60%">
		   
		   <table border="0" align="right">
				<tr><td collspan="9" style="height:10px;"></td></tr>
			   <tr>
				<td><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
aboutus.php" style="color:#FFF;text-decoration:none;">About Us</a></td>
				<td>|</td>
				<td><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
privacy.php" style="color:#FFF;text-decoration:none;">Privacy</a></td>
				<td>|</td>
				<td><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
terms_of_use.php" style="color:#FFF;text-decoration:none;">Terms Of Use</a></td>
				<td>|</td>
				<td><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
faq.php" style="color:#FFF;text-decoration:none;">FAQ</a></td>
				<td>|</td>
				<td><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
contactus.php" style="color:#FFF;text-decoration:none;">Contact Us</a></td>
			   </tr>
		   </table>

		</div>
		  <div class="clear"></div>
		</div>

</body>
</html>
