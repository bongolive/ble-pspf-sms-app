<?php /* Smarty version Smarty3-RC3, created on 2013-05-10 23:17:13
         compiled from "C:\xampp\htdocs\pspf\templates/vendor/lang_sw_tz/help.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19030518d55c9ce4423-25141121%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0ca2b8e15782700391d46cc3c165b87bfd31ed6e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/vendor/lang_sw_tz/help.tpl',
      1 => 1300986000,
    ),
  ),
  'nocache_hash' => '19030518d55c9ce4423-25141121',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1><?php echo @VEN_MENUTOP_HELP;?>
</h1>
	
	<ol type="1">
		<li><a href="#1">Ni aina gani za mafaili mnazokubali ili kuingiza majina ya watu?</a></li>
		<li><a href="#2">Je, faili lenye namba ninazoingiza linatakiwa kuwa katika fomati maalum?</a></li>
		<li><a href="#3">Nitanunuaje salio la sms toka Bongo Live?</a></li>
		<li><a href="#4">Inachukua muda gani kwa salio la sms zilizonunuliwa kuonekana kwenye akaunti yangu?</a></li>
		<li><a href="#5">Nifanyeje ili kubadili neno langu la siri?</a></li>
		<li><a href="#6">Nifanyeje ili kubadili namba yangu ya simu ya mkononi?</a></li>
		<li><a href="#7">Jina la mtumaji ni nini?</a></li>
		<li><a href="#8">Nifanyeje ili kuomba jina la mtumaji?</a></li>
		<li><a href="#9">Ni majina yapi ya watumaji naweza kuomba?</a></li>
		<li><a href="#10">Inachukua muda gani kwa maombi yangu ya jina la mtumaji kukubaliwa?</a></li>
		<li><a href="#11">Nifanyeje ili kuongeza jina la mtu wangu?</a></li>
		<li><a href="#12">Nifanyeje ili kuhariri jina la mtu wangu?</a></li>
		<li><a href="#13">Nifanyeje ili kuanzisha kundi jipya?</a></li>
		<li><a href="#14">Nifanyeje ili kufuta kundi la watu?</a></li>
		<li><a href="#15">Nifanyeje ili kutuma sms?</a></li>
		<li><a href="#16">Nataka kununua sms zaidi ya 25,000. Tunaweza kujadiliana bei yake?</a></li>
		<li><a href="#17">Je, kuna tofauti kati ya akaunti ya ‘broadcaster’ binafsi na shirika?</a></li>
		<li><a href="#18">Naweza kuongeza majina mangapi kwenye mfumo?</a></li>
	</ol>
	<br />
		<ol type="1">
		<li><b><a name="1">Ni aina gani za mafaili mnazokubali ili kuingiza majina ya watu?</a></b></li>
			<p>Kwa sasa tunakubali tu mafaili aina ya .CSV. Unaweza kubadili faili la ekseli liwe katika fomati hii kwa kufanya FILE> SAVE AS> (kisha chagua . CSV toka kwenye aina za mafaili zitakazotokea)</p>
            
		<li><b><a name="2">Je, faili lenye namba ninazoingiza linatakiwa kuwa katika fomati maalum?</a></b></li>
		<p>Ndiyo. Fomati inaweza kunakiliwa toka kwenye faili la mfumo lililoko kwenye ukurasa wa Dashboard>Ukurasa was Import Contacts.</p>
        
		<li><b><a name="3">Nitanunuaje salio la sms toka Bongo Live?</a></b></li>
		<p>Ili kununua salio la sms, kliki kwenye ‘link’ ya ‘Nunua SMS’. Jaza fomu kwa kuingiza idadi ya salio la sms unayotaka kununua. Bei yake itatokea yenyewe. Kliki ‘Tuma’ ili kuthibitisha maombi yako ya manunuzi. Utaonyeshwa namba ya maombi ya manunuzi na maelezo ya jinsi ya kulipia (kupitia ZAP, MPESA, fedha taslimu). Mara malipo yatakapopokelewa na kuthibitishwa, mwakilishi wa Bongo Live ataidhinisha kuingizwa kwa salio la sms kwenye akaunti yako. Mteja anawajibika kwa gharama zozote za kibiasahara.</p>
	
    	<li><b><a name="4">Inachukua muda gani kwa salio la sms zilizonunuliwa kuonekana kwenye akaunti yangu?</a></b></li>
		<p>Mara malipo yatakapopokelewa, itachukua kati ya saa 2 – 3 kwa salio kuonekana. Tafadhali, tupe hadi saa 6.</p>
        
		<li><b><a name="5">Nifanyeje ili kubadili neno langu la siri?</a></b>?</li>
		<p>Kliki kwenye ‘link’ ya ‘Badili Neno la Siri’ mara baada ya kuingia kwenye akaunti yako. Utatakiwa utaje neno lako la siri la zamani na jipya. Kisha kliki ‘Tuma’ ili kuthibitisha badiliko. Utapokea uthibitisho wa neno lako jipya la siri kupitia sms na baruapepe.</p>
		
        <li><b><a name="6">Nifanyeje ili kubadili namba yangu ya simu ya mkononi?</a></b></li>
		<p>Kliki kwenye ‘link’ ya ‘Badili Namba ya Simu’. Kisha utatakiwa kuingiza namba mpya ya simu ya mkononi.</p>
		
        <li><b><a name="7">Jina la Mtumaji ni nini?</a></b></li>
		<p>Jina la mtumaji ni herufi au tarakimu zinazoonekana kama ‘Kutoka kwa:’ katika meseji yako ya sms. Hii inaweza kuwa ama herufi 11 au namba ya simu ya mkononi katika fomati inayokubalika kimataifa ambayo ina urefu wa tarakimu 14 Mfano 255784234357. </p>
		
        <li><b><a name="8">Nifanyeje ili kuomba Jina la Mtumaji?</a></b></li>
		<p>Ili kuomba jina mtumaji, kliki kwenye ‘link’ ya ‘Jina la Mtumaji’. Utatakiwa kuingiza jina lako la mtumaji  uliloomba, pamoja na sababu ya kuliomba.</p>
		
        <li><b><a name="9">Ni majina yapi ya watumaji naweza kuomba?</a></b></li>
		<p>Unaweza kuomba jina la mtumaji ambalo si la shirika, mtu binafsi, bidhaa au jina la kibiashara ambalo si lako. Namba za simu zitakazoombwa zitaidhinishwa tu iwapo zinahusiana na akaunti yako ya utumaji sms. Tunajitahidi kuzuia matumizi ya kiudanganyifu ya majina ya watumaji.</p>
		
        <li><b><a name="10">Inachukua muda gani kwa maombi yangu ya jina la mtumaji kukubaliwa?</a></b></li>
		<p>Mara ombi kuhusu ‘Jina la Mtumaji’ linapopokelewa, kwa kawaida litashughulikiwa ndani ya saa 2 – 3. Tafadhali tupe hadi saa 6.</p>
		
        <li><b><a name="11">Nifanyeje ili kuongeza jina la mtu wangu?</a></b></li>
		<p>Ili kuingiza jina la mtu, tembelea skrini ya Dashboard>Ongeza Jina Jipya. Jaza , kisha kliki ‘Save’. Kinachotakiwa tu ni namba ya simu ya mkononi.</p>
		
        <li><b><a name="12">Nifanyeje ili kuhariri jina la mtu wangu?</a></b></li>
		<p>Ili kuhariri taarifa za mtu wako, tembelea ukurasa wa ‘Dashboard’ kisha kliki kwenye ‘Shughulikia Majina’  kwa ajili ya kundi liliko jina unalotaka kuhariri. Mara unapolifikia, kliki kwenye ‘Hariri’ ili kufanya mabadiliko yoyote. Kumbuka ku-save mara baada ya kukamilisha uhariri wako.</p>
		
        <li><b><a name="13">Nifanyeje ili kuanzisha kundi jipya?</a></b></li>
		<p>Kuanzisha kundi jipya ni rahisi tu. Tembelea ukurasa wa ‘Dashboard’, kisha ingiza jina la kundi lako jipya kwenye fomu, halafu kliki ‘Tuma’.</p>
		
        <li><b><a name="14">Nifanyeje ili kufuta kundi la watu?</a></b></li>
		<p>Kufuta kundi nako ni rahisi pia. Tembelea ukurasa wa ‘Dashboard’. Chagua kundi unalotaka kufuta toka kwenye orodha ya makundi. Kliki ‘Futa’, kisha thibitisha kufuta.</p>
		
        <li><b><a name="15">Nifanyeje ili kutuma sms?</a></b></li>
		<p>Ili kutuma SMS ni lazima kwanza uhakikishe mambo kadhaa:</p>
		<ol type="A">
		<li>Una Salio la kutosha la SMS kwa ajili ya kutuma kwenye kundi ulilokusudia? Kama sivyo, tembelea ukurasa wa ‘Nunua SMS’ ili kuanza utaratibu.</li>
		<li>Una jina halali la mtumaji lililoidhinishwa na utawala? Kama sivyo, tembelea ukurasa wa ‘Broadcast>Shughulikia Majina ya mtumaji’ ili kuomba jina la mtumaji.</li>
		<li>Mara hatua za hapo juu zitakapokamilika, tembelea ukurasa wa Broadcast>Andika SMS</li>
		    <ol type="1">
			<li>Chagua makundi unayotaka kuwatumia SMS, ingiza meseji za sms, chagua jina la mtumaji.</li>
			<li>Kama unataka kupangilia sms zako zije ziende kwa walengwa hapo baadaye, unaweza pia kuchagua tarehe na muda.</li>
			<li>Kliki ‘Tuma’ kisha thibitisha utumaji.</li>
			</ol>
		</ol>
		</p>
		
        <li><b><a name="16">Nataka kununua sms zaidi ya 25,000. Tunaweza kujadiliana bei yake?</a></b></li>
		<p>Kama unataka kununua sms nyingi kwa mara moja, tutafurahi sana kujadiliana nawe kuhusu viwango bora vya bei. Tafadhali tupigie kupitia 0688 121 252 kwa maelezo zaidi au wasiliana nasi kupitia fomu ya mawasiliano.</p>
		
        <li><b><a name="17">Je, kuna tofauti kati ya akaunti ya ‘broadcaster’ binafsi na shirika?</a></b></li>
		<p> Hakuna tofauti kati ya aina hizi mbili za akaunti. Tunazitofautisha tu ili tuweze kuwahudumia wateja wetu vizuri.</p>
		
        <li><b><a name="18">Naweza kuongeza majina mangapi kwenye mfumo?</a></b></li>
		<p>Kimsingi hakuna kikomo cha idadi ya watu unaoweza kuwaongeza kwenye mfumo.</p>
	</ol>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>