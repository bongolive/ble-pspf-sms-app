<?php /* Smarty version Smarty3-RC3, created on 2013-05-13 10:53:02
         compiled from "C:\xampp\htdocs\pspf\templates/mail_template/mail_ven_register_conf_for_reseller.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3133551909bde6a4479-40341578%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '976c2ee7c388067de2f5a10b3b5e556ac72e454b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/mail_template/mail_ven_register_conf_for_reseller.tpl',
      1 => 1365167130,
    ),
  ),
  'nocache_hash' => '3133551909bde6a4479-40341578',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
index.php"><img alt="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['name'];?>
" src="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['logo'];?>
" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:10px 0px 10px 0px;">Client Registration</h1>
		<br />

		<p>Your new account has been setup .</p>
		<p>You can now communicate easily and broadcast your sms to any number of contacts with our simple easy to use web platform. </p>
		<p>Your login details:</p>
		<p>URL : <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
ven_login.php"><?php echo $_smarty_tpl->getVariable('base_url')->value;?>
ven_login.php</a></p>
		<p><?php echo $_smarty_tpl->getVariable('field1')->value;?>
 : <?php echo $_smarty_tpl->getVariable('field1_val')->value;?>
</p>
		<p><?php echo $_smarty_tpl->getVariable('field2')->value;?>
 : <?php echo $_smarty_tpl->getVariable('field2_val')->value;?>
</p>
		<p>If you forget your password it can be retrieved on the <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
ven_login.php">home page</a></p>
		<?php if ($_smarty_tpl->getVariable('activationLink')->value===true){?>
		<p>Before you can sign into your account you must activate your account by clicking on the link below. If the link is not active please copy and paste the link into your browser&#39;s address bar. </p>
		<p><?php echo $_smarty_tpl->getVariable('activateUrl')->value;?>
</p>
		<?php }?>
		<p>You can sign in on the <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
ven_login.php">here</a></p>
		<p>Once registered you will be able to sign into your account and do the following:
			<ul>
				<li>Add, import and edit contacts</li>
				<li>Organize contacts into groups</li>
				<li>Purchase SMS credits</li>
				<li>Manage sender names</li>
				<li>Broadcast SMS to groups and contacts</li>
				<li>Schedule SMS broadcasts for the future.</li>
				<li>Access our help page to learn how to do things.</li>
			</ul>

		<p>If you have any questions you can get in touch with us at address below.</p>

		<p> <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['organisation'];?>
- Your Message, Your Audience. <br />
		  <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['physical_address'];?>
<br />
		  Tel: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_phone'];?>

		  .<br />
		  Mob: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['mob_no'];?>
<br />
		  Email: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_email'];?>

		<p><br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;">

		  <p style="float:left;	margin:15px 0px 0px 0px;"></p>
		  <div class="nav-bottom" width="60%"></div>
		  <div class="clear"></div>
		</div>

</body>
</html>
