<?php /* Smarty version Smarty3-RC3, created on 2014-08-25 15:56:04
         compiled from "C:\xampp\htdocs\pspf\templates/mail_template/mail_change_pass.tpl" */ ?>
<?php /*%%SmartyHeaderCode:8017518e5181e605b5-47932108%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e502d5157202862e80a2b3b26ac4c07950cd964a' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/mail_template/mail_change_pass.tpl',
      1 => 1368279808,
    ),
  ),
  'nocache_hash' => '8017518e5181e605b5-47932108',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
index.php"><img alt="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['name'];?>
" src="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['logo'];?>
" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:10px 0px 10px 0px;">Password Reset</h1>
		<br />

		<p>The password for your account has been changed successfully.</p>
		
		<p>Your new password : <strong><?php echo $_smarty_tpl->getVariable('newPassword')->value;?>
</strong></p>

		<?php if ($_smarty_tpl->getVariable('parentVenId')->value==''){?>
        <p>If you did not change your password or if have any other questions, you can get in touch with us using the  <a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
contactus.php">Contact Us</a> page.</p>
		<p> PSPF. <br />
		  PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street <br />
		  Dar es Salaam, Tanzania<br />
		  Tel: +255 222120912
		  .<br />
		  E-Mail: pspf@pspf-tz.org </p>
		<?php }else{ ?>
		<p> <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['organisation'];?>
 <br />
		  <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['physical_address'];?>
<br />
		  Tel: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_phone'];?>

		  .<br />
		  Mob: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['mob_no'];?>
<br />
		  Email: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_email'];?>

		<p> 
		<?php }?><br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;"></div>

</body>
</html>
