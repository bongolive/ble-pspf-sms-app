<?php /* Smarty version Smarty3-RC3, created on 2012-10-25 11:45:56
         compiled from "C:\xampp\htdocs\pspf\templates/privacy.tpl" */ ?>
<?php /*%%SmartyHeaderCode:209715088fc44b26363-17424979%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0d5c786e86d927ad3bd0d4116d12c4f4c8398d18' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/privacy.tpl',
      1 => 1300914000,
    ),
  ),
  'nocache_hash' => '209715088fc44b26363-17424979',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="container">
	<div class="line01"></div>
		<div class="column2-ex-left-2-old">
		<h1><?php echo @PRIVACY_POLICY;?>
</h1>
	
	<h2>PLEASE READ THIS AGREEMENT CAREFULLY</h2>
	
	<p>At Bongo Live we are committed to protecting your privacy and strive to make our site a safe environment for everyone who visits and uses Bongo Live services. The purpose of this policy statement is to share with you the following:</p>
	
	<p>1.	How we gather information about our users
2.	How we use the information gathered about our users
3.	Protection of Database Information
4.	Reviewing/Changing Your Information
5.	Limits on Our Abilities
6.	Complaints Procedure
</p>
	
	<h2>1. How we gather information about our users</h2>
		<p>We will never collect sensitive information about you without your explicit consent. When you register with Bongo Live as a broadcaster or subscriber by filling out and submitting the registration form on the Bongo Live web site, an account will be created for you automatically. </p>
		<p>When you visit the Bongo Live web site, our web servers may use technology to track the behavior patterns of visitors to our site by collecting number of visits, average time spent, page views, the name of the domain you used to access the Internet and the web site you came from and visit next. </p>
		
	<h2>2. How we use the information gathered about our users</h2>
		<p>Your personal information will not be used or disclosed for purposes other than those set out in this Bongo Live Privacy Policy. We will not sell or rent personally identifiable information provided by you to us. We use your information to keep track of the Bongo Live services you have signed up to receive and to statistically analyze site usage, to improve our content and product offerings and to customize our site's content and layout. We do this in an attempt to improve our site and better tailor it to meet our users' needs. We use information in the file we maintain about you, and other information we obtain from your current and past activities on the site, to resolve disputes, troubleshoot problems and enforce our Terms and Conditions. Bongo Live may use any e-mail address, mobile phone number, physical address that you provide to us to communicate with you from time to time regarding administrative notices, account information, updates and upgrades to Bongo Live and other Bongo Live products that become available. In addition, we must comply with court orders, information requests from government agencies and regulators and other legal and regulatory processes that may require the disclosure of your personally identifiable information.</p>
		
		<p>If Bongo Live or all of Bongo Live assets are acquired, we may transfer customer information, including your personal information provided to us as part of the registration process, to the acquirer. Otherwise, we will not use or share your personal information with any third party except to our service providers as required to operate Bongo Live.</p>
		
		<h2>3. Protection of Database Information</h2>
		
		<p>Any information sent to Bongo Live for text messaging and or storage purposes will remain the property of the account holder, which Bongo Live will hold securely in accordance with our internal security policy and the law. Bongo Live will take reasonable measures to protect your personal information with security safeguards appropriate to the sensitivity of the information, through the use of technological measures (e.g., firewalls, passwords, encryption) and training of employees. Bongo Live will at no time collect or redistribute this information without your consent, except where legally required to do so by law.</p>
		
			<h2>4. Reviewing/Changing Your Information</h2>
		<p>It is Bongo Live policy that just as a user subscribes to our service, they have the right to unsubscribe. When a user unsubscribes from our service, they will no longer receive any messages unless they were to subscribe again. Bongo Live is not responsible for the messages sent by our clients or customers that are using Bongo Live products or services to send messages. It is the broadcasters sole responsibility to acquire necessary permission to send messages to their recipients.</p>
		<p>If at any time you would like to change any information in your account, login from the home page. Here you can change or update your information.</p>
		
		<h2>5. Limits on Our Abilities</h2>
		<p>Although your privacy is very important to us, due to the existing legal and technical environment, we cannot ensure that your personally identifiable information will not be disclosed to third parties in ways not described in this policy. Additionally, we can (and you authorize us to) disclose any information about you to private entities, law enforcement or other government officials, as we, in our sole discretion, believe necessary or appropriate to address or resolve inquiries or problems.</p>
		
	  <h2>6. Complaints Procedure</h2>
		<p>If you have any complaints relating to any aspect of our service, please email us at contact (at) bongolive.co.tz. Please state clearly the nature of the complaint. We will acknowledge your complaint within 5 working days. You will also be assigned a contact name that will be responsible for keeping you informed of the progress of any complaint.</p>
<p>Bongo Live may reserve the right to change this online privacy policy from time to time at its discretion. Changes will be posted in the privacy policy web page and will be effective upon posting. Your continued use of Bongo Live after changes to this online privacy policy have been posted is deemed acceptance of those changes. It is your sole responsibility to monitor the privacy policy to determine whether any such changes have been posted. This privacy policy is subject to and applicable to all privacy laws.</p>
<p>You acknowledge that acceptance of this privacy policy is a condition to your use of our services and you agree to be bound by all of its terms and conditions</p>
<p>If you have further questions about our privacy policy or its implementation, please contact us at contact (at) bongolive.co.tz
</p>
		
		</div>
	<div class="column2-ex-right-2"> &nbsp;</div>
	<div class="clear"></div>
</div>