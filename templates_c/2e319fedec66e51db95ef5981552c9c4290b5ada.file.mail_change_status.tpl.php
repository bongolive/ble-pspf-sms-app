<?php /* Smarty version Smarty3-RC3, created on 2015-03-10 14:38:30
         compiled from "C:\xampp\htdocs\pspf\templates/mail_template/mail_change_status.tpl" */ ?>
<?php /*%%SmartyHeaderCode:31965518e7484225285-65949554%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2e319fedec66e51db95ef5981552c9c4290b5ada' => 
    array (
      0 => 'C:\\xampp\\htdocs\\pspf\\templates/mail_template/mail_change_status.tpl',
      1 => 1368279796,
    ),
  ),
  'nocache_hash' => '31965518e7484225285-65949554',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	</head>
	<body>
	
		<div id="container-top">
		  <div class="logo"><a href="<?php echo $_smarty_tpl->getVariable('base_url')->value;?>
index.php"><img alt="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['name'];?>
" src="<?php echo $_smarty_tpl->getVariable('resellerDetails')->value['logo'];?>
" border="0" /></a></div>
		  <div class="clear"></div>
		</div>
		<div id="container">
		  <div class="line01"></div>
		  <div class="column2-ex-left-2">
		  <div class="maincontent">
		<h1 style="font-size:20px;color:#93278F;margin:40px 0px 10px 0px;">Payment Status</h1>
		<br />

		<p>Credit request id : <strong><?php echo $_smarty_tpl->getVariable('req_id')->value;?>
</strong></p>

		<?php if (($_smarty_tpl->getVariable('status_true')->value=='true')){?>

			<p>The status for purchase requests has been changed successfully.</p>


		<?php }?>

		<?php if (($_smarty_tpl->getVariable('payment_true')->value=='true')){?>

			<p>The payment status for purchase requests has been changed successfully.</p>
		
		<?php }?>

			<p>Credit Requested: <strong><?php echo $_smarty_tpl->getVariable('quantity')->value;?>
</strong> </p>

			<p>Rate: <strong><?php echo $_smarty_tpl->getVariable('rate')->value;?>
</strong> </p>

			<p>Total Cost: <strong><?php echo $_smarty_tpl->getVariable('total')->value;?>
</strong> </p>
			
			<p>Status: <strong><?php echo $_smarty_tpl->getVariable('status')->value;?>
</strong> </p>

			<p>Payment Status: <strong><?php echo $_smarty_tpl->getVariable('PayStatus')->value;?>
</strong> </p>

		<?php if ($_smarty_tpl->getVariable('parentVenId')->value==''){?>
		<p> PSPF. <br />
		  PSPF HOUSE - Shorter Tower 6 - 13 floor Between Ohio and Kibo Street <br />
		  Dar es Salaam, Tanzania<br />
		  Tel: +255 222120912
		  .<br />
		  E-Mail: pspf@pspf-tz.org </p>
		<?php }else{ ?>
		<p> <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['organisation'];?>
 <br />
		  <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['physical_address'];?>
<br />
		  Tel: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_phone'];?>

		  .<br />
		  Mob: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['mob_no'];?>
<br />
		  Email: <?php echo $_smarty_tpl->getVariable('resellerDetails')->value['support_email'];?>

		<p> 
		<?php }?> <br/>
		</div>
		</div>
		  <div class="clear"></div>
		</div>

		<div style="width:96%;margin:20px auto 0px;background:#3B9F3E;
		background-repeat:repeat-x;	height:46px;color:#FFF;font-size:14px;padding:0px 10px 0px 10px;	position:relative;"></div>

</body>
</html>
