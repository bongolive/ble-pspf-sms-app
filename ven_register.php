<?php
	
	/**********************************************
	*	File	: ven_register.php				  *
	*	Purpose	: Registration of clients,vendors *
	*             or broadcasters				  *
	*	Author	: Akhilesh	
	*	15 September 2012 Leonard Update Error msg For sending Email To appear successful regardless of email sent to vendor or not					  *
	**********************************************/
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	require_once(MODEL.'user.class.php');
	require_once(MODEL.'location/location.class.php');
	require_once 'swift/lib/swift_required.php';
	
	$objUser = new user();

	$step = $_REQUEST['step'];
	$action = $_REQUEST['action'];
	$errorFlag = 0;
	$refresh=0;
	//print_r( $_REQUEST);	
	if(isset($_POST['userId']) && $_POST['userId'] != ''){
		
		$objUser-> userId = $_POST['userId'];
		$userDetail = $objUser-> getVendorDetail();
	}
	
	switch($step){
		default:
			$step = '1';
			$action = 'add';
			unset($_SESSION['ven_reg']);
		break;
		case'0':
			if($action=='edit'){
			
				$smarty->assign('username',$userDetail['username']);
				$smarty->assign('email',$userDetail['email']);
				$smarty->assign('accType',$userDetail['vendor_type']);
				$smarty->assign('userId',$userDetail['id']);
			
				$action = 'update';
				$step = '1';
		
			}
			
		break;

		case'1':
			$objUser->username = $_POST['username'];
			$objUser->password = $_POST['password'];
			$objUser->vendor_id= str_rand($project_vars["random_vendorId_length"],$project_vars["random_vendorId_strtype"]);
			$objUser->vendorType = $_POST['accType'];
			$objUser->step = $_POST['step'];
			
			if(isset($action) && $action=='add'){
				if(isset($_REQUEST['password']) && $_REQUEST['password']!=''){
					$_SESSION['ven_reg']['pass']=$_POST['password'];	
				}
				$userId = $objUser->registerVenStep1Insert();
			
			}elseif(isset($action) && $action=='update'){
				if(isset($_REQUEST['password']) && $_REQUEST['password']!=''){
					$_SESSION['ven_reg']['pass']=$_POST['password'];
				}
				$userId = $objUser->registerVenStep1Upadate();
			
			}
			if($userId==2){
			
				$msg = VEN_REGISTER_MSG_01;
				$errorFlag = 1;
				$smarty->assign('username',$_POST['username']);
				$smarty->assign('password',$_POST['password']);
				$smarty->assign('accType',$_POST['vendor_type']);
				$action = 'add';
				$step = '1';

			
			}else{
				$smarty->assign('userId', $_POST['userId']);
				$smarty->assign('name',$userDetail['name']);
				$smarty->assign('gender',$userDetail['gender']);
				$smarty->assign('ageRange',$userDetail['age_range_id']);
				$smarty->assign('location',$userDetail['location_id']);
				$smarty->assign('neighborhood',$userDetail['neighborhood_id']);
				$smarty->assign("arrCountry",getArray('countries','country_code','country'));
				
				$action = 'update';
				$accType = $_REQUEST['accType'];
				$step = '2';
			}
		break;
		case 'refresh':
			
				$smarty->assign('userId', $_POST['userId']);
				$smarty->assign('name',$_POST['name']);
				$smarty->assign('mob_no',$_POST['mob_no']);
				$smarty->assign('email',$_POST['email']);
				$smarty->assign('postalAddress',$_POST['postalAddress']);
				$smarty->assign('neighborhood',$_POST['neighborhood_id']);
				$smarty->assign("country",$_POST['country']);
				
				$refresh=1;
				$action = 'update';
				$accType = $_REQUEST['accType'];
				$smarty->assign('accType',$_POST['accType']);
				$step = '2';
		
		break;
		case'2':
			
			if($action =='update' || $action =='add'){
				if($accType==1){
				
					$objUser->userId	= $_POST['userId'];
					$objUser->name		= $_POST['name'];
					$objUser->mobNo		= formatMobno($_POST['mob_no']);
					$objUser->email		= $_POST['email'];
					//$objUser->peopleRangeId	= $_POST['peopleRange'];
					$objUser->postalAddress = $_POST['postalAddress'];
					$objUser->locationId	= $_POST['location'];
					$objUser->termCondition = $_POST['termCondition'];
					$objUser->confirmed = 1;
					$objUser->step		= $_POST['step'];

				}elseif($accType==2){
					
					$objUser->userId			= $_POST['userId'];
					$objUser->name				= $_POST['name'];
					$objUser->mobNo				= formatMobno($_POST['mob_no']);
					$objUser->phone				= $_POST['phone'];
					$objUser->organisation		= $_POST['organisation'];
					$objUser->organisationCatId	= $_POST['organisationCat'];
					$objUser->email				= $_POST['email'];
					$objUser->peopleRangeId		= $_POST['peopleRange'];
					$objUser->postalAddress		= $_POST['postalAddress'];
					$objUser->physicalAddress	= $_POST['physicalAddress'];
					$objUser->locationId		= $_POST['location'];
					$objUser->termCondition		= $_POST['termCondition'];
					$objUser->step				= $_POST['step'];
					$objUser->confirmed = 1;
				}
					
				$status = $objUser->registerVenStep2Update();
				if($status ==2){
					
					$msg = VEN_REGISTER_MSG_02;
					$errorFlag = 1;
					$smarty->assign('userId',$_POST['userId']);
					$smarty->assign('name',$_POST['name']);
					$smarty->assign('mob_no',$_POST['mob_no']);
					$smarty->assign('phone',$_POST['phone']);
					$smarty->assign('organisation',$_POST['organisation']);
					$smarty->assign('organisationCat',$_POST['organisationCat']);
					$smarty->assign('email',$_POST['email']);
					$smarty->assign('peopleRange',$_POST['peopleRange']);
					$smarty->assign('postalAddress',$_POST['postalAddress']);
					$smarty->assign('physicalAddress',$_POST['physicalAddress']);
					$smarty->assign('location',$_POST['location']);

					
					$action = 'update';
					$accType = $_REQUEST['accType'];
					$step = '2';
					
				}elseif($status!=2 && $status!=false){

					$objUser-> userId = $_POST['userId'];
					$userDetail = $objUser-> getVendorDetail();
		
					$objUser->createDefaultSenderName(); // create a default sender name as mobile no of broadcaster
					$objUser->createDefaultAddbook(); // create a "Default" group for broadcaster
					
					if($project_vars['ven_account_activation_email']!==true){
						
						//$objUser->createDefaultSenderName(); // create a default sender name as mobile no of broadcaster
						//$objUser->createDefaultAddbook(); // create a "Default" group for broadcaster
						
						if($project_vars['mob_confirmation']==false){
							$msg = VEN_REGISTER_MSG_03;
							$errorFlag = 0;
						}else{
							$msg = VEN_REGISTER_MSG_04;
							$errorFlag = 0;
						
						}
					}else{
						if($project_vars['mob_confirmation']==false){
							$msg = VEN_REGISTER_MSG_05;
							$errorFlag = 0;
						}else{
							$msg = VEN_REGISTER_MSG_06;
							$errorFlag = 0;
						
						}
					}
					try{
						if($project_vars["smtp_auth"]===true){
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"])
							->setUsername($project_vars["smtp_username"])
							->setPassword($project_vars["smtp_password"]);
						}else{
						
							$transport = Swift_SmtpTransport::newInstance($project_vars["smtp_host"], $project_vars["smtp_port"]);
						}

						$mailer = Swift_Mailer::newInstance($transport);

						//Create a message
						$smarty->assign('field1','Username');
						$smarty->assign('field1_val',$userDetail['username']);
						$smarty->assign('field2','Password');
						$smarty->assign('field2_val',$_SESSION['ven_reg']['pass']);
						//$smarty->assign('activationLink',$project_vars['ven_account_activation_email']);
						//$smarty->assign('activateUrl',BASE_URL.'activate_acnt.php?userType=VEN&id='.urlencode($userDetail['id']));
						$body = $smarty->fetch('mail_template/mail_ven_register_conf.tpl');
						
						
						
						$message = Swift_Message::newInstance($project_vars['acct_activation_msg'])
						->setFrom(array($project_vars["smtp_username"] => 'admin'))
						->setTo(array($userDetail["email"]=> $userDetail['name']))
						->setBody($body );
						$message->setContentType("text/html");

						//Send the message
						$result = $mailer->send($message);
					} catch (Exception $e){
					
						$msg = VEN_REGISTER_MSG_05;
							$errorFlag = 0;
						//$msg = VEN_REGISTER_MSG_03.VEN_REGISTER_MSG_07;
						//$errorFlag = 1;
					}
					try{	
						if($project_vars['mob_confirmation']==true){
							$massege = str_rand($project_vars["random_mob_conf_length"],$project_vars["random_string_type"]);
							smsSendWithCurl($objUser->mobNo,str_replace('MESSAGE',$massege,$project_vars['register_message']),$project_vars['sms_api_senderid']);
							$objUser->mob_conf_msg = $massege;
							$objUser->insertVenMobConfMsg();
						}
					}catch(Exception $objE){
						$msg = VEN_REGISTER_MSG_05;
						$errorFlag = 0;
						//$msg = VEN_REGISTER_MSG_08.$objE->getMessage();;
						//$errorFlag = 1;
					
					}

					$smarty->assign('userId', $_POST['userId']);
					if($project_vars['mob_confirmation']==true){
						$action = 'update';
						$step = '3';
					}else{
						$step = '4';
					}
				}
			}
			
			
		break;
		case'3':
			if($action =='update' || $action == 'add'){
				$objUser->userId = $_POST['userId'];
				$objUser->mob_conf_msg = $_POST['mob_conf_msg'];
				$objUser->mob_confirmed = '1';
				$satus = $objUser->registerVenStep3Update();
				if($satus==1){
					$objUser->createDefaultSenderName(); // create a default sender name as mobile no of broadcaster
					$objUser->createDefaultAddbook(); // create a "Default" group for broadcaster
				
					$msg = VEN_REGISTER_MSG_09;
					$errorFlag = 0;
					$step = '4';
				}elseif($satus==2){
				
					$msg = VEN_REGISTER_MSG_10;
					$errorFlag = 1;
					$step = '3';
					$smarty->assign('mobConfMsg',$objUser->mob_conf_msg);

				}elseif($satus==0){
				
					$msg = VEN_REGISTER_MSG_11;
					$errorFlag = 1;
					$step = '3';
					$smarty->assign('mobConfMsg',$objUser->mob_conf_msg);
				}
			}
			
		break;
		
	
	}

	
	$smarty->assign("arrCountry",getArray('countries','country_code','country'));
	
	if($refresh !=1){
		//$smarty->assign("arrLocation",getArray('locations','id','location'));
	}else{
		$objLocation= new location();
		$objLocation->country_code = $_POST['country'];
		$arrLoct=$objLocation->getCountryLocations();
		$smarty->assign("arrLocation",$arrLoct);
		
	}
	
	$smarty->assign("arrAgeRange",getArray('age_ranges','id','age_range'));
	//$smarty->assign("arrOccupation",getArray('occupations','id','occupation'));
	//$smarty->assign("arrIndustry",getArray('industries','id','industry'));
	//$smarty->assign("arrCategory",getArray('categories','id','category'));
	$smarty->assign("arrOrganisationCat",getArray('organisations','id','organisation',' ORDER BY organisation',$_SESSION['lang']));
	$smarty->assign("arrPeopleRanges",getArray('people_ranges','id','people_range'));
	$smarty->assign("action",$action);
	$smarty->assign("userId",$userId);
	$smarty->assign("accType",$accType);
	$smarty->assign("step",$step);
	$smarty->assign("msg",$msg);
	$smarty->assign("mobMaxlength",$project_vars['mob_no_length']);
	$smarty->assign("phoneMaxlength",$project_vars['phone_length']);
	$smarty->assign("errorFlag",$errorFlag);
	
	/* Check correct including of file '/terms_of_use.html' in template '/templates/vendor/ven_register.tpl' _BEGIN */
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->assign('terms_of_use',0);
	}
	else
	{
		if(file_exists('./lang_'.$_SESSION['lang'].'/terms_of_use.html'))
		{
			$smarty->assign('terms_of_use',1);
		}
		else
		{
			$smarty->assign('terms_of_use',0);
		}
	}
	/* Check correct including of file '/terms_of_use.html' in template '/templates/subscribers/sub_register.tpl' _END */
	
	$smarty->display("vendor/ven_register.tpl");

	include 'footer.php';

?>