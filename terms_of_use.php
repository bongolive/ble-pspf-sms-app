<?php
	include_once ('bootstrap.php');
	require_once(LIB_DIR.'inc.php');
	include('header.php');
	
	if(strcmp($_SESSION['lang'],'en_us') == 0)
	{
		$smarty->display('terms_of_use.tpl');
	}
	else
	{
		if(file_exists('./templates/lang_'.$_SESSION['lang'].'/terms_of_use.tpl'))
		{
			$smarty->display('lang_'.$_SESSION['lang'].'/terms_of_use.tpl');
		}
		else
		{
			$smarty->display('terms_of_use.tpl');
		}
	}
	
	include('footer.php');
?>